/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

// get defaults assetExts array
const defaultAssetExts = require('metro-config/src/defaults/defaults').assetExts;
// get defaults assetExts array
const defaultSource = require('metro-config/src/defaults/defaults').sourceExts;
module.exports = (async () => {
  return {
    transformer: {
      babelTransformerPath: require.resolve('react-native-svg-transformer'),
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: false,
        },
      }),
    },
    resolver: {
      assetExts: defaultAssetExts.filter(ext => ext !== 'svg'),
      sourceExts: [...defaultSource, 'svg'],
    },
  };
})();

// module.exports = {
//   transformer: {
//     getTransformOptions: async () => ({
//       transform: {
//         experimentalImportSupport: false,
//         inlineRequires: false,
//       },
//     }),
//   },
// };
