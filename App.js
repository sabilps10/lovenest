import React, {useEffect} from 'react';
import {View, StyleSheet, YellowBox} from 'react-native';
import {ApolloProvider} from 'react-apollo';
import client from './src/apollo';
import Main from './src/screens/Main';
import Navigation from './src/navigations';
import {Provider} from 'react-redux';
import Store from './src/redux/store';
import SplashScreen from 'react-native-splash-screen';
import FCM from './src/utils/Notifications/FCMService';

const {subTopic, onMessage, registerAppIOSWithFCM} = FCM;

YellowBox.ignoreWarnings(['Warning: ReactNative.createElement', '']);
console.disableYellowBox = true;

const App = props => {
  console.log('APP ROOT CLIENT APOLLO: ', client);
  useEffect(() => {
    SplashScreen.hide();

    return () => SplashScreen.hide();
  }, []);

  useEffect(() => {
    subTopic();
    return () => subTopic();
  }, []);

  return (
    <ApolloProvider client={client}>
      <Provider store={Store}>
        <Navigation {...props}>
          <View style={styles.container}>
            <Main {...props} />
          </View>
        </Navigation>
      </Provider>
    </ApolloProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;
