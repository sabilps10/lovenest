import {Platform} from 'react-native';
import modeStatus from './modeStatus';

const developmentTopics = Platform.OS === 'ios' ? 'development' : 'development';

const productionTopics =
  Platform.OS === 'ios'
    ? 'sg.com.lovenest.customer'
    : 'sg.com.lovenest.android';

const topics = modeStatus === 'dev' ? developmentTopics : productionTopics;

export default topics;
