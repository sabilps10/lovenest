// We dont use tipsy stripe anymore, will be remove for that libs
import modeStatus from '../modeStatus';

const publishKey = '';
const devPublishKey = '';

const keyUsed = modeStatus === 'dev' ? devPublishKey : publishKey;
export default keyUsed;
