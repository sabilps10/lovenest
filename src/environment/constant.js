import modeStatus from './modeStatus';

const developmentServer =
  'https://staging-dot-apollo-dot-love-nest-233803.appspot.com/';
const productionServer = 'https://apollo-dot-love-nest-233803.appspot.com';
// const hotFixServer = `https://staging-hotfix-dot-apollo-dot-love-nest-233803.appspot.com`

const URI = modeStatus === 'dev' ? developmentServer : productionServer;

export default URI;
