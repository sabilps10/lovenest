import modeStatus from './modeStatus';

const developmentTopics = 'common.development';

const productionTopics = 'common.production';

const common = modeStatus === 'dev' ? developmentTopics : productionTopics;

export default common;
