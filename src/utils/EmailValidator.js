const emailValidator = email => {
  return new Promise((resolve, reject) => {
    try {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const result = re.test(String(email).toLowerCase());
      if (result) {
        resolve(true);
      } else {
        resolve(false);
      }
    } catch (error) {
      reject(false);
    }
  });
};

export default emailValidator;
