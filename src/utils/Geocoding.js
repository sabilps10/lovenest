import axios from 'axios';
import APIKEY from '../environment/MapAPI';

class Geocoding {
  detailLocation = address => {
    return new Promise(async (resolve, reject) => {
      try {
        const dataAddress = address;
        const parsingAddress = dataAddress.replace('#', '');
        const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${parsingAddress}&key=${APIKEY}`;
        const getGeocode = await axios.get(geocodeUrl);
        console.log('getGeocode: ', getGeocode.data.results[0]);
        if (getGeocode.data.results.length > 0) {
          const {geometry} = getGeocode.data.results[0];
          resolve(geometry);
        }
      } catch (error) {
        console.log('reject error: ', error);
        reject(error);
      }
    });
  };
}

const GeoLocation = new Geocoding();

export default GeoLocation;
