import axios from 'axios';
import APIKEY from '../environment/MapAPI';

class Geocoding {
  detailLocation = address => {
    return new Promise(async (resolve, reject) => {
      try {
        if (address !== '' || address) {
          const dataAddress = address;
          const parsingAddress = dataAddress.replace('#', '');
          const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${parsingAddress}&key=${APIKEY}`;
          const getGeocode = await axios.get(geocodeUrl);
          console.log('getGeocode: ', getGeocode);
          if (getGeocode?.data?.results?.length > 0) {
            const {geometry} = getGeocode.data.results[0];
            const {location} = geometry;
            const {lat, lng} = location;
            const dataGeo = getGeocode.data.results[0];
            const length = dataGeo?.address_components?.length
              ? dataGeo.address_components.length
              : [];
            const result = {
              postalCode: dataGeo?.address_components[length - 1]?.long_name,
              country: dataGeo?.address_components[length - 2]?.long_name,
              city: dataGeo?.address_components[length - 3]?.long_name,
              lat,
              lng,
              address: dataGeo?.formatted_address,
            };
            resolve(result);
          } else {
            throw new Error('address not found');
          }
        }
      } catch (error) {
        console.log('reject error: ', error);
        reject(error);
      }
    });
  };
}

const GeoLocation = new Geocoding();

export default GeoLocation;
