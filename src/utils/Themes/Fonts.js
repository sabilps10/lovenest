import React from 'react';
import {Platform} from 'react-native';

export const FontSize = {
  superTiny: 9,
  tiny: 10,
  small: 12,
  regular: 14,
  exregular: 16,
  large: 18,
  extraLarge: 21,
  superExtraLarge: 24,
  topTabFontSize: 13.4,
  title: 45,
};

export const IconSize = {
  iSmall: 25,
  iMedium: 35,
  iLarge: 45,
};

export const FontType = {
  thin: Platform?.OS === 'ios' ? 'Gotham-Light' : 'GothamLight',
  book: Platform?.OS === 'ios' ? 'Gotham-Book' : 'GothamBook',
  bookItalic: Platform?.OS === 'ios' ? 'Gotham-BookItalic' : 'GothamBookItalic',
  italiano: Platform?.OS === 'ios' ? 'Italianno-Regular' : 'Italiano-Reguler',
  medium: Platform?.OS === 'ios' ? 'Gotham-Medium' : 'GothamMedium',
  bold: Platform?.OS === 'ios' ? 'Gotham-Bold' : 'GothamBold',
  tabFontBold:
    Platform?.OS === 'ios' ? 'Montserrat-SemiBold' : 'Montserrat-SemiBold',
  tabFontMedium: Platform?.OS === 'ios' ? 'GothamMedium' : 'GothamMedium',
  handStyle: Platform?.OS === 'ios' ? 'Gloriant' : 'Gloriant',
  handStyle1:
    Platform?.OS === 'ios' ? 'KaushanScript-Regular' : 'KaushanScript-Regular',
  BaskervilleBold:
    Platform?.OS === 'ios' ? 'BaskervilleBT-Bold' : 'BaskervilleBoldBT',
  Baskerville: Platform?.OS === 'ios' ? 'Baskerville-Normal' : 'baskvl',
};

export const CanvasFontStyle = {
  PatrickHandSCRegular:
    Platform?.OS === 'ios' ? 'PatrickHandSC-Regular' : 'PatrickHandSC-Regular',
  KleeOneSemiBold:
    Platform?.OS === 'ios' ? 'KleeOne-SemiBold' : 'KleeOne-SemiBold',
  OleoScriptRegular:
    Platform?.OS === 'ios' ? 'OleoScript-Regular' : 'OleoScript-Regular',
  RedressedRegular:
    Platform?.OS === 'ios' ? 'Redressed-Regular' : 'Redressed-Regular',
  MeriendaRegular:
    Platform?.OS === 'ios' ? 'Merienda-Regular_0_wt' : 'Merienda-Regular_0_wt',
  DMSansItalic: Platform?.OS === 'ios' ? 'DMSans-Italic' : 'DMSans-Italic',
  DMSansBold: Platform?.OS === 'ios' ? 'DMSans-Bold' : 'DMSans-Bold',
  DMSansRegular: Platform?.OS === 'ios' ? 'DMSans-Regular' : 'DMSans-Regular',
  CinzelVariableFontWght:
    Platform?.OS === 'ios' ? 'Cinzel-Regular' : 'Cinzel-Regular',
  CourgetteRegular:
    Platform?.OS === 'ios' ? 'Courgette-Regular' : 'Courgette-Regular',
};

export const arrayCanvasFont = [
  {
    id: 1,
    name: 'PatrickHandSC',
    scriptName: CanvasFontStyle?.PatrickHandSCRegular,
  },
  {
    id: 2,
    name: 'KleeOneSemiBold',
    scriptName: CanvasFontStyle?.KleeOneSemiBold,
  },
  {
    id: 3,
    name: 'OleoScriptRegular',
    scriptName: CanvasFontStyle?.OleoScriptRegular,
  },
  {
    id: 4,
    name: 'RedressedRegular',
    scriptName: CanvasFontStyle?.RedressedRegular,
  },
  {
    id: 5,
    name: 'MeriendaRegular',
    scriptName: CanvasFontStyle?.MeriendaRegular,
  },
  {
    id: 6,
    name: 'DMSansItalic',
    scriptName: CanvasFontStyle?.DMSansItalic,
  },
  {
    id: 7,
    name: 'DMSansBold',
    scriptName: CanvasFontStyle?.DMSansBold,
  },
  {
    id: 8,
    name: 'DMSansRegular',
    scriptName: CanvasFontStyle?.DMSansRegular,
  },
  {
    id: 9,
    name: 'CinzelVariableFontWght',
    scriptName: CanvasFontStyle?.CinzelVariableFontWght,
  },
  {
    id: 10,
    name: 'CourgetteRegular',
    scriptName: CanvasFontStyle?.CourgetteRegular,
  },
];
