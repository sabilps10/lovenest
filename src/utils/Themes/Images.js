// Appointment Card
import appIconSent from '../../static/images/AppointmentIcon/arrow.png';
import appIconReschedule from '../../static/images/AppointmentIcon/broken_heart.png';
import appIconApproved from '../../static/images/AppointmentIcon/check_green.png';
import appIconHavenBook from '../../static/images/AppointmentIcon/exclamation_mark.png';

// new icon for Appointment
import rescheduledIcon from '../../static/images/AppointmentIcon/NewIcons/rescheduled.png';
import confirmedIcon from '../../static/images/AppointmentIcon/NewIcons/confirmed.png';
import haventbookIcon from '../../static/images/AppointmentIcon/NewIcons/haventBook.png';
import sentIcon from '../../static/images/AppointmentIcon/NewIcons/sent.png';

// Bottom Navigation Icon Active
import bottomNavActiveHome from '../../static/images/BottomNavIcon/ActiveIcon/homePink.png';
import bottomNavActiveOrder from '../../static/images/BottomNavIcon/ActiveIcon/ordersPink.png';
import bottomNavActivePayment from '../../static/images/BottomNavIcon/ActiveIcon/payIconPink.png';
import bottomNavActiveMore from '../../static/images/BottomNavIcon/ActiveIcon/morePink.png';
import bottomNavActiveAppointment from '../../static/images/BottomNavIcon/ActiveIcon/appointmentsPink.png';
import bottomNavActiveEinvitation from '../../static/images/BottomNavIcon/ActiveIcon/einvitationPink.png';

// Bottom Navigation Icon Inactive
import bottomNavInactiveHome from '../../static/images/BottomNavIcon/InActiveIcon/homeBlack.png';
import bottomNavInactiveOrder from '../../static/images/BottomNavIcon/InActiveIcon/ordersBlack.png';
import bottomNavInactivePayment from '../../static/images/BottomNavIcon/InActiveIcon/payIconBlack.png';
import bottomNavInactiveMore from '../../static/images/BottomNavIcon/InActiveIcon/moreBlack.png';
import bottomNavInactiveAppointment from '../../static/images/BottomNavIcon/InActiveIcon/appointmentsBlack.png';
import bottomNavInactiveEinvitation from '../../static/images/BottomNavIcon/InActiveIcon/einvitationBlack.png';

// Credit Card Icon
import amex from '../../static/images/CreditCard/amex.png';
import discover from '../../static/images/CreditCard/discover.png';
import mastercard from '../../static/images/CreditCard/mastercard.png';
import visa from '../../static/images/CreditCard/visa.png';
import unknownCard from '../../static/images/CreditCard/stp_card_unknown.png';
import paymentHeaderBG from '../../static/images/CreditCard/headerPayment.png';

// Welcome Screen Image
import lovenestText from '../../static/images/OnBoarding/mainLogoLN.png';
import welcomeBackground from '../../static/images/OnBoarding/starting.png';
import onBoarding0 from '../../static/images/OnBoarding/onboarding0.png';
import onBoarding1 from '../../static/images/OnBoarding/onboarding1.png';
import onBoarding2 from '../../static/images/OnBoarding/onboarding2.png';
import onBoarding3 from '../../static/images/OnBoarding/onboarding3.png';
import hand from '../../static/images/OnBoarding/hand.png';
import shield from '../../static/images/OnBoarding/shield.png';
import bannerMainAccount from '../../static/images/OnBoarding/bannerMainAccount.png';
import bannerPartnerAccount from '../../static/images/OnBoarding/bannerPartnerAccount.png';

// Profile
import userIcon from '../../static/images/Profile/user.png';
import cameraIcon from '../../static/images/Profile/camera.png';

// Top Navigation Icon Active
import TopNavActiveBridal from '../../static/images/TopNavIcon/Active/BridalPink.png';
import TopNavActiveCafe from '../../static/images/TopNavIcon/Active/CafePink.png';
import TopNavActiveEvent from '../../static/images/TopNavIcon/Active/EventPink.png';
import TopNavActiveInteriorDesign from '../../static/images/TopNavIcon/Active/InteriorDesignPink.png';
import TopNavActiveJewellery from '../../static/images/TopNavIcon/Active/JewelleryPink.png';
import TopNavActiveSmartHome from '../../static/images/TopNavIcon/Active/SmartHomePink.png';
import TopNavActiveToday from '../../static/images/TopNavIcon/Active/TodayPink.png';
import TopNavActiveVenue from '../../static/images/TopNavIcon/Active/VenuePink.png';

// Top Navigation Icon InActive
import TopNavInactiveBridal from '../../static/images/TopNavIcon/Inactive/BridalBlack.png';
import TopNavInactiveCafe from '../../static/images/TopNavIcon/Inactive/CafeBlack.png';
import TopNavInactiveEvent from '../../static/images/TopNavIcon/Inactive/EventBlack.png';
import TopNavInactiveInteriorDesign from '../../static/images/TopNavIcon/Inactive/InteriorDesignBlack.png';
import TopNavInactiveJewellery from '../../static/images/TopNavIcon/Inactive/JewelleryBlack.png';
import TopNavInactiveSmartHome from '../../static/images/TopNavIcon/Inactive/SmartHomeBlack.png';
import TopNavInactiveToday from '../../static/images/TopNavIcon/Inactive/TodayBlack.png';
import TopNavInactiveVenue from '../../static/images/TopNavIcon/Inactive/VenueBlack.png';

// QR Code
import QRCodeFrame from '../../static/images/QCode/QR-overlay.png';
import QRCodePointViewFrame from '../../static/images/QCode/qr-code.png';
import QRFrame from '../../static/images/QCode/QRFrame.png';
import QRBG from '../../static/images/QCode/bgQR.png';

// Common Image
import bellNotificationIcon from '../../static/images/bell.png';
import toasterGearFailed from '../../static/images/gearFailed.png';
import toasterGearSuccess from '../../static/images/gearToaster.png';
import failedGear from '../../static/images/failedGear.png';
import successGear from '../../static/images/successGear.png';
import lnLogoNotif from '../../static/images/ln_logo_notif.png';
import lnTextBlack from '../../static/images/mainLogoLNBlack.png';
import lnTextHome from '../../static/images/homeLovenestText.png';
import homeTextLogin from '../../static/images/homeTextLogin.png';
import loveCrown from '../../static/images/lovecrown.png';
import BigLNLogo from '../../static/images/BigLNLogo.png';

// Char Image
import charCar from '../../static/images/CharImages/car.png';
import charComingSoon from '../../static/images/CharImages/coming_soon.png';
import charFullBook from '../../static/images/CharImages/fullbook.png';
import charInCompleteAppointment from '../../static/images/CharImages/incomplete_appointment.png';
import charNoNotification from '../../static/images/CharImages/no_notification.png';
import charNoOrder from '../../static/images/CharImages/no_orders.png';
import charNoCafe from '../../static/images/CharImages/noCafe.png';
import charNoPaymentLogin from '../../static/images/CharImages/noPaymentLogin.png';
import charOfferNotAvailable from '../../static/images/CharImages/offer_not_available.png';
import charOrderbag from '../../static/images/CharImages/orderBag.png';
import charNoEvent from '../../static/images/CharImages/charNoEvent.png';
import charHandBag from '../../static/images/handBag.png';
import charChekcedTimeline from '../../static/images/checkedTimeline.png';
import charAdditionalItems from '../../static/images/additionalItems.png';
import charGreyCheckedTimeLine from '../../static/images/greyCheckTimeLine.png';
import charGreenCheckedTimeLine from '../../static/images/CheckedGreen.png';
import charMerchantAbout from '../../static/images/aboutMerchant.png';
import charBridalBadge from '../../static/images/bridalBadge.png';
import charGoldtag from '../../static/images/goldTag.png';
import charCupStar from '../../static/images/cupStar.png';
import charLeftShuriken from '../../static/images/left-shuriken.png';
import charRightShuriken from '../../static/images/right-shuriken.png';
import charCommingSoonNew from '../../static/images/CharImages/commingsoon.png';
import charEmptyFlorist from '../../static/images/CharImages/charEmptyFloirst.png';
import charDelivery from '../../static/images/Delivery.png';
import charSelfPickup from '../../static/images/SelfCollect.png';
import charTheWeddingOf from '../../static/images/theWedddingOf.png';
import charEmptyFloristPromotion from '../../static/images/Florist/emptyPromotion.png';
import charIconReview from '../../static/images/Florist/charReview.png';
import charIconAbout from '../../static/images/Florist/charStore.png';
import charFilterIcon from '../../static/images/Florist/charFilterIcon.png';
import charFilterIconRed from '../../static/images/Florist/charFilterIconRed.png';
import charCategoryIcon from '../../static/images/Florist/categoryIcon.png';
import charMainGreenStoreIcon from '../../static/images/Florist/MainGreenStoreIcon.png';
import charNoPortfolio from '../../static/images/Florist/charNoPortfolio.png';
import charFloristBGMerchant from '../../static/images/Florist/floristBGMerchant.png';
import charRatingStar from '../../static/images/ratingStar.png';
import charCreateInvitation from '../../static/images/createImage.png';
import charMyInvitation from '../../static/images/myinvitationImage.png';
import charMyRSVP from '../../static/images/myrsvpImage.png';
import charBridalCovers from '../../static/images/bridalCovers.png';
import charSpiteGreen from '../../static/images/spiteGreen.png';
import charSpiteRed from '../../static/images/spiteRed.png';
import charNoteGreen from '../../static/images/noteGreen.png';
import charNoteRed from '../../static/images/noteRed.png';
import charJewelleryBanner from '../../static/images/jewelery1.png';
import charEmptyTable from '../../static/images/emptyTable.png';
import charTableIconRed from '../../static/images/tableIconRed.png';
import charLoveAndWinIcon from '../../static/images/loveandwinIcon.png';
import charLoveAndWinBg from '../../static/images/loveandwinbg.png';
import charRibbonTopTime from '../../static/images/ribbonTopTimer.png';
import charRibbonDateDraw from '../../static/images/ribbonDateDraw.png';
import charRibbonToWin from '../../static/images/ribbonToWin.png';
import charCameraButton from '../../static/images/cameraButton.png';
import charChanceCard from '../../static/images/chanceCard.png';
import charChanceCardRibbon from '../../static/images/chanceCardRibbon.png';
import charLoveAndWinHostoryIcon from '../../static/images/loveandwiniconhistory.png';
import charLoveWinEmpty from '../../static/images/lovewinempty.png';

// Social Media Image
import facebook from '../../static/images/socialMedia/facebook.png';
import youtube from '../../static/images/socialMedia/youtube.png';
import instagram from '../../static/images/socialMedia/instagram.png';
import pinterest from '../../static/images/socialMedia/pinterest.png';
import globe from '../../static/images/socialMedia/globe.png';

// Home Menu LN V2.0
import BridalIcon from '../../static/images/HomeMenu/BridalIcon.png';
import JewelleryIcon from '../../static/images/HomeMenu/JewelleryIcon.png';
import FloristIcon from '../../static/images/HomeMenu/FloristIcon.png';
import VenueIcon from '../../static/images/HomeMenu/VenueIcon.png';
import SmartHomeIcon from '../../static/images/HomeMenu/SmartHomeIcon.png';
import SuitIcon from '../../static/images/HomeMenu/SuitIcon.png';
import PhotographyIcon from '../../static/images/HomeMenu/PhotographyIcon.png';
import InteriorDesignIcon from '../../static/images/HomeMenu/InteriorDesignIcon.png';

// Notification Icon V2
// News Tab Notification Icon
import ActivityIcon from '../../static/images/NotificationIcon/News/activity.png';
import EventIcon from '../../static/images/NotificationIcon/News/event.png';
import InfoIcon from '../../static/images/NotificationIcon/News/info.png';
import PromoIcon from '../../static/images/NotificationIcon/News/promo.png';
// Appointment Tab Notification Icon
import ConfirmedIcon from '../../static/images/NotificationIcon/Appointments/confirmed.png';
import RescheduledIcon from '../../static/images/NotificationIcon/Appointments/rescheduled.png';
import ReminderIcon from '../../static/images/NotificationIcon/Appointments/reminder.png';
import UpdateIcon from '../../static/images/NotificationIcon/Appointments/update.png';

// New Notif Big Icon
import BigIconNewsPromo from '../../static/images/NotificationIcon/NewsBigIcon/Promo.png';
import BigIconNewsEvent from '../../static/images/NotificationIcon/NewsBigIcon/Event.png';
import BigIconNewsOrder from '../../static/images/NotificationIcon/NewsBigIcon/Order.png';

// Bridal Badge
import blueBadge from '../../static/images/BadgeMerchant/badgeBlue.png';
import pinkBadge from '../../static/images/BadgeMerchant/badgeRed.png';
import crownBadge from '../../static/images/BadgeMerchant/crown.png';

// Florist Order Stepper
import orderConfirmedRed from '../../static/images/FloristOrderStepper/orderConfirmed.png';
import orderAccesptedRed from '../../static/images/FloristOrderStepper/orderAccepted.png';
import orderShippedRed from '../../static/images/FloristOrderStepper/orderShipped.png';
import preparingYourOrderRed from '../../static/images/FloristOrderStepper/preparingYourOrder.png';
import orderConfirmedGrey from '../../static/images/FloristOrderStepper/orderConfirmedGrey.png';
import orderAccesptedGrey from '../../static/images/FloristOrderStepper/orderAcceptedGrey.png';
import orderShippedGrey from '../../static/images/FloristOrderStepper/orderShippedGrey.png';
import preparingYourOrderGrey from '../../static/images/FloristOrderStepper/preparingYourOrderGrey.png';

import floristLogo from '../../static/images/Florist/FleursLogo.png';
import floristExplore from '../../static/images/Florist/ExploreFLorist.png';
import floristGiftSet from '../../static/images/Florist/FloristGiftSet.png';
import floristDomes from '../../static/images/Florist/FlowerDomes.png';

import venueWhiteIcon from '../../static/images/VenueAndHotel/venueWhiteIcon.png';
import venueBlackIcon from '../../static/images/VenueAndHotel/venueBlackIcon.png';
import hotelWhiteIcon from '../../static/images/VenueAndHotel/hotelWhiteIcon.png';
import hotelBlackIcon from '../../static/images/VenueAndHotel/hotelBlackIcon.png';
import venueImg from '../../static/images/VenueAndHotel/venueImg.png';
import hotelImg from '../../static/images/VenueAndHotel/hotelImg.png';
import venueBg from '../../static/images/VenueAndHotel/venueBg.png';

import boxTable from '../../static/images/boxTable.png';
import circleTable from '../../static/images/circleTable.png';
import retangleTable from '../../static/images/retangleTable.png';

// New Home Menu
import BridalNewMenu from '../../static/images/NewMainMenuHome/BridalMenu.png';
import JewelleryMenu from '../../static/images/NewMainMenuHome/JewelleryMenu.png';
import FloristMenu from '../../static/images/NewMainMenuHome/FloristMenu.png';
import VenueMenu from '../../static/images/NewMainMenuHome/VenueMenu.png';
import SmartHomeMenu from '../../static/images/NewMainMenuHome/SmartHomeMenu.png';
import SuitMenu from '../../static/images/NewMainMenuHome/SuitMenu.png';
import PhotographyMenu from '../../static/images/NewMainMenuHome/PhotographyMenu.png';
import InteriorDesignMenu from '../../static/images/NewMainMenuHome/InteriorDesignMenu.png';
import EditorChoiceAwardsMenu from '../../static/images/NewMainMenuHome/EditorChoiceAwardsMenu.png';
import PremiumVendorsMenu from '../../static/images/NewMainMenuHome/PremiumVendorsMenu.png';
import BeautyWellnessMenu from '../../static/images/NewMainMenuHome/BeautyWellnessMenu.png';
import BeveragesAndWinesMenu from '../../static/images/NewMainMenuHome/BeveragesAndWinesMenu.png';
import ChineseWeddingTraditionalMenu from '../../static/images/NewMainMenuHome/ChineseWeddingTraditionalMenu.png';
import CarRentalMenu from '../../static/images/NewMainMenuHome/CarRentalMenu.png';
import FavoursAndStationariesMenu from '../../static/images/NewMainMenuHome/FavoursAndStationariesMenu.png';
import TravelAndHoneyMoonMenu from '../../static/images/NewMainMenuHome/TravelAndHoneyMoonMenu.png';
import CateresMenu from '../../static/images/NewMainMenuHome/CateresMenu.png';
import EmceesMenu from '../../static/images/NewMainMenuHome/EmceesMenu.png';
import HairAndMakeUpMenu from '../../static/images/NewMainMenuHome/HairAndMakeUpMenu.png';
import PhotoAndVideoMenu from '../../static/images/NewMainMenuHome/PhotoAndVideoMenu.png';

export const NewHomeMenu = {
  BridalNewMenu,
  JewelleryMenu,
  FloristMenu,
  VenueMenu,
  SmartHomeMenu,
  SuitMenu,
  PhotographyMenu,
  InteriorDesignMenu,
  EditorChoiceAwardsMenu,
  PremiumVendorsMenu,
  BeautyWellnessMenu,
  BeveragesAndWinesMenu,
  ChineseWeddingTraditionalMenu,
  CarRentalMenu,
  FavoursAndStationariesMenu,
  TravelAndHoneyMoonMenu,
  CateresMenu,
  EmceesMenu,
  HairAndMakeUpMenu,
  PhotoAndVideoMenu,
};

export const tableIcon = {
  boxTable,
  circleTable,
  retangleTable,
};

export const VenueAndHotelImages = {
  venueWhiteIcon,
  venueBlackIcon,
  hotelWhiteIcon,
  hotelBlackIcon,
  venueImg,
  hotelImg,
  venueBg,
};

export const DummyFloristImage = {
  floristLogo,
  floristExplore,
  floristGiftSet,
  floristDomes,
};

export const OrderStepper = {
  orderConfirmedRed,
  orderAccesptedRed,
  orderShippedRed,
  preparingYourOrderRed,
  orderConfirmedGrey,
  orderAccesptedGrey,
  orderShippedGrey,
  preparingYourOrderGrey,
};

export const BigIconNotification = {
  BigIconNewsPromo,
  BigIconNewsEvent,
  BigIconNewsOrder,
};

export const NotificationIcon = {
  ActivityIcon,
  EventIcon,
  InfoIcon,
  PromoIcon,
  ConfirmedIcon,
  RescheduledIcon,
  ReminderIcon,
  UpdateIcon,
};

export const HomeMenuIcon = {
  BridalIcon,
  JewelleryIcon,
  FloristIcon,
  VenueIcon,
  SmartHomeIcon,
  SuitIcon,
  PhotographyIcon,
  InteriorDesignIcon,
};

export const socialMediaIcon = {
  facebook,
  youtube,
  globe,
  pinterest,
  instagram,
};

export const appIcon = {
  appIconApproved,
  appIconHavenBook,
  appIconReschedule,
  appIconSent,
  rescheduledIcon,
  confirmedIcon,
  haventbookIcon,
  sentIcon,
};

export const bottomNavActive = {
  bottomNavActiveHome,
  bottomNavActiveMore,
  bottomNavActiveOrder,
  bottomNavActivePayment,
  bottomNavActiveAppointment,
  bottomNavActiveEinvitation,
};

export const bottomNavInactive = {
  bottomNavInactiveHome,
  bottomNavInactiveOrder,
  bottomNavInactivePayment,
  bottomNavInactiveAppointment,
  bottomNavInactiveMore,
  bottomNavInactiveEinvitation,
};

export const creditCard = {
  mastercard,
  visa,
  amex,
  discover,
  unknownCard,
  paymentHeaderBG,
};

export const onBoarding = {
  lovenestText,
  welcomeBackground,
  onBoarding0,
  onBoarding1,
  onBoarding2,
  onBoarding3,
  hand,
  shield,
  bannerMainAccount,
  bannerPartnerAccount,
};

export const profileIcon = {
  userIcon,
  cameraIcon,
};

export const topNavigationActive = {
  TopNavActiveToday,
  TopNavActiveEvent,
  TopNavActiveBridal,
  TopNavActiveJewellery,
  TopNavActiveInteriorDesign,
  TopNavActiveVenue,
  TopNavActiveSmartHome,
  TopNavActiveCafe,
};

export const topNavigationInactive = {
  TopNavInactiveToday,
  TopNavInactiveEvent,
  TopNavInactiveBridal,
  TopNavInactiveJewellery,
  TopNavInactiveVenue,
  TopNavInactiveInteriorDesign,
  TopNavInactiveSmartHome,
  TopNavInactiveCafe,
};

export const QRCode = {
  QRCodeFrame,
  QRCodePointViewFrame,
  QRFrame,
  QRBG,
};

export const commonImage = {
  bellNotificationIcon,
  toasterGearFailed,
  toasterGearSuccess,
  failedGear,
  successGear,
  lnLogoNotif,
  lnTextBlack,
  lnTextHome,
  homeTextLogin,
  loveCrown,
  BigLNLogo,
};

export const charImage = {
  charCar,
  charComingSoon,
  charFullBook,
  charInCompleteAppointment,
  charNoNotification,
  charNoOrder,
  charNoCafe,
  charNoPaymentLogin,
  charOfferNotAvailable,
  charOrderbag,
  charNoEvent,
  charHandBag,
  charChekcedTimeline,
  charAdditionalItems,
  charGreyCheckedTimeLine,
  charGreenCheckedTimeLine,
  charMerchantAbout,
  charBridalBadge,
  charGoldtag,
  charLeftShuriken,
  charRightShuriken,
  charCommingSoonNew,
  charEmptyFlorist,
  charDelivery,
  charSelfPickup,
  charTheWeddingOf,
  charCupStar,
  charEmptyFloristPromotion,
  charIconReview,
  charIconAbout,
  charFilterIcon,
  charFilterIconRed,
  charCategoryIcon,
  charMainGreenStoreIcon,
  charNoPortfolio,
  charFloristBGMerchant,
  charRatingStar,
  charCreateInvitation,
  charMyInvitation,
  charMyRSVP,
  charBridalCovers,
  charSpiteGreen,
  charSpiteRed,
  charNoteGreen,
  charNoteRed,
  charJewelleryBanner,
  charEmptyTable,
  charTableIconRed,
  charLoveAndWinIcon,
  charLoveAndWinBg,
  charRibbonTopTime,
  charRibbonDateDraw,
  charRibbonToWin,
  charCameraButton,
  charChanceCard,
  charChanceCardRibbon,
  charLoveAndWinHostoryIcon,
  charLoveWinEmpty,
};

export const BridalBadge = {
  blueBadge,
  pinkBadge,
  crownBadge,
};
