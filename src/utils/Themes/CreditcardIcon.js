export const creditCardIcon = {
  visaIcon: require('../../static/images/CreditCard/visa.png'),
  masterCardIcon: require('../../static/images/CreditCard/mastercard.png'),
  discoverIcon: require('../../static/images/CreditCard/discover.png'),
  amexIcon: require('../../static/images/CreditCard/amex.png'),
  unknownIcon: require('../../components/CreditCards/icons/stp_card_unknown.png'),
};

export const creditCardIntialNumber = {
  visa: 4,
  masterCard: 5,
  discover: 6,
  amex: 3,
};
