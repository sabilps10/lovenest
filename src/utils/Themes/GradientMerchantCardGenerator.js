const gradientColor = ['#FBF1E4', '#FFEDED', '#FACFC7', '#F6F6F6'];

const generatorColors =
  gradientColor[Math.floor(Math.random() * gradientColor.length)];

export default generatorColors;
