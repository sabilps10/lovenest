const ColorGradients = {
  dark: 'rgba(30,30,30,0.85)',
  darkLess: 'rgba(0,0,0,0.71)',
  veryDarkLess: 'rgba(255,255,255,0.16)',
  noMoreDark: 'rgba(255,255,255,0.0)',
};

export default ColorGradients;
