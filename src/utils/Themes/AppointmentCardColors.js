const cardColors = {
  yellow: '#FFF6E4',
  yellowBold: '#EDBC5D',
  red: '#FFE5E8',
  redBold: '#E17676',
  grey: '#EDEAEA',
  greyBold: '#979595',
  green: '#DAF2E9',
  greenBold: '#67BA9D',
};

export default cardColors;
