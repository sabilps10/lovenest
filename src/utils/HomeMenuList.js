import {HomeMenuIcon, NewHomeMenu} from './Themes/Images';

const {
  BridalNewMenu,
  JewelleryMenu,
  FloristMenu,
  VenueMenu,
  SmartHomeMenu,
  SuitMenu,
  PhotographyMenu,
  InteriorDesignMenu,
  EditorChoiceAwardsMenu,
  PremiumVendorsMenu,
  BeautyWellnessMenu,
  BeveragesAndWinesMenu,
  ChineseWeddingTraditionalMenu,
  CarRentalMenu,
  FavoursAndStationariesMenu,
  TravelAndHoneyMoonMenu,
  CateresMenu,
  EmceesMenu,
  HairAndMakeUpMenu,
  PhotoAndVideoMenu,
} = NewHomeMenu;

const list1 = [
  {
    id: 1,
    menuName: 'Bridal',
    menu: 'Bridal',
    icon: BridalNewMenu,
  },
  {
    id: 2,
    menuName: 'Jewellery',
    menu: 'Jewellery',
    icon: JewelleryMenu,
  },
  {
    id: 3,
    menuName: 'Venue',
    menu: 'Venue',
    icon: VenueMenu,
  },
  {
    id: 4,
    menuName: 'Homebotic',
    menu: 'Smart Home',
    icon: SmartHomeMenu,
  },
  {
    id: 5,
    menuName: 'Suit',
    menu: 'Suit',
    icon: SuitMenu,
  },
  {
    id: 6,
    menuName: 'Nest Lab',
    menu: 'Interior Design',
    icon: InteriorDesignMenu,
  },
  {
    id: 7,
    menuName: 'Florist',
    menu: 'Florist',
    icon: FloristMenu,
  },
  {
    id: 8,
    menuName: 'Photography',
    menu: 'Photography',
    icon: PhotographyMenu,
  },
];

const list2 = [
  {
    id: 1,
    menuName: 'Editor Choice Award',
    menu: 'Editor Choice Award',
    icon: EditorChoiceAwardsMenu,
  },
  {
    id: 2,
    menuName: 'Beauty Wellness',
    menu: 'Beauty Wellness',
    icon: BeautyWellnessMenu,
  },
  {
    id: 3,
    menuName: 'Chinese Wedding',
    menu: 'Chinese Wedding Traditional',
    icon: ChineseWeddingTraditionalMenu,
  },
  {
    id: 4,
    menuName: 'Favour & Stationaries',
    menu: 'Favour & Stationaries',
    icon: FavoursAndStationariesMenu,
  },
  {
    id: 5,
    menuName: 'Premium Vendors',
    menu: 'Premium Vendors',
    icon: PremiumVendorsMenu,
  },
  {
    id: 6,
    menuName: 'Beverages & Wines',
    menu: 'Beverages & Wines',
    icon: BeveragesAndWinesMenu,
  },
  {
    id: 7,
    menuName: 'Car\nRental',
    menu: 'Car Rental',
    icon: CarRentalMenu,
  },
  {
    id: 8,
    menuName: 'Travel & Honeymoon',
    menu: 'Travel & Honeymoon',
    icon: TravelAndHoneyMoonMenu,
  },
];

const list3 = [
  {
    id: 1,
    menuName: 'Cateres',
    menu: 'Cateres',
    icon: CateresMenu,
  },
  {
    id: 2,
    menuName: 'Emcees',
    menu: 'Emcees',
    icon: EmceesMenu,
  },
  {
    id: 3,
    menuName: 'Hair & Make Up',
    menu: 'Hair & Make Up',
    icon: HairAndMakeUpMenu,
  },
  {
    id: 4,
    menuName: 'Photo & Videos',
    menu: 'Photo & Videos',
    icon: PhotoAndVideoMenu,
  },
];

export const NewMenuList = {
  list1,
  list2,
  list3,
};

// const {
//   BridalIcon,
//   JewelleryIcon,
//   FloristIcon,
//   VenueIcon,
//   SmartHomeIcon,
//   SuitIcon,
//   PhotographyIcon,
//   InteriorDesignIcon,
// } = HomeMenuIcon;

// export const MenuList = [
//   {
//     id: 1,
//     name: 'Bridal',
//     tag: 'Bridal',
//     icon: BridalIcon,
//   },
//   {
//     id: 2,
//     name: 'Jewellery',
//     tag: 'Jewellery',
//     icon: JewelleryIcon,
//   },
//   {
//     id: 4,
//     name: 'Venue',
//     tag: 'Venue',
//     icon: VenueIcon,
//   },
//   {
//     id: 5,
//     name: 'Homebotic',
//     tag: 'Smart Home',
//     icon: SmartHomeIcon,
//   },
//   {
//     id: 6,
//     name: 'Suit',
//     tag: 'Suit',
//     icon: SuitIcon,
//   },
//   {
//     id: 8,
//     name: 'Nest Lab',
//     tag: 'Interior Design',
//     icon: InteriorDesignIcon,
//   },
//   {
//     id: 3,
//     name: 'Fleurs',
//     tag: 'Florist',
//     icon: FloristIcon,
//   },
//   {
//     id: 7,
//     name: 'Photography',
//     tag: 'Photography',
//     icon: PhotographyIcon,
//   },
// ];
