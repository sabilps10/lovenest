import axios from 'axios';
import APIKEY from '../environment/MapAPI';

class GeocodeWithLatLng {
  getLocation = (lat, lng) => {
    return new Promise(async (resolve, reject) => {
      try {
        const uri = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${APIKEY}`;
        const getDetailLocation = await axios.get(uri);

        if (getDetailLocation.data.results.length > 0) {
          const detailLocation = getDetailLocation.data.results[1];
          if (detailLocation) {
            const {geometry} = detailLocation;
            console.log('geometry: ', geometry);
            if (geometry) {
              const latitude = geometry.location.lat;
              const longitude = geometry.location.lng;
              const address = detailLocation.formatted_address;

              const summary = {
                lat: latitude,
                lng: longitude,
                viewport: geometry.viewport,
                address,
              };

              resolve(summary);
            }
          }
        }
      } catch (error) {
        console.log('GeocodeWithLatLng: ', error);
        reject(error);
      }
    });
  };
}

const dataLocation = new GeocodeWithLatLng();

export default dataLocation;
