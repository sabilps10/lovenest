const termAndCondition = [
  {
    title: `Over View`,
    data: [
      {
        data: `We are Love Nest Holding Pte Ltd. ("Love Nest") and we own and operate this website ("Site") at lovenest.com.sg. Throughout the Site, the terms "we", "us" and "our" refer to lovenest. lovenest offers this website, including, without any limitation, mobile applications, web applications and any related services including notifications, electronic mails (collectively, the "Services") and any information, data, or other content or materials appearing or otherwise made accessible on or through the Services (collectively, the "Content") from this Site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated herein.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `By visiting our Site, registering an account with us, providing us with your details which includes but not limited to email address, purchasing something from us and/or using our Services, you agree to be bound by the following terms and conditions ("Terms of Use", "Terms"), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Use apply to all users of the Site, including without limitation to users who are browsers, vendors, customers, merchants, and/ or contributors of content.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `The headings used in this agreement are included for convenience only and will not limit or otherwise affect the interpretation and definitions of the Terms.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Please read these Terms of Use carefully before accessing or using our Site, as you agree to be bound by these Terms of Use upon doing so. These terms govern your access to and use of the Services of lovenest. If you do not agree to all the terms and conditions of this agreement, then you may not access and/or use the Site, Services and/or Content. If these Terms of Use are considered an offer, acceptance is expressly limited to these Terms of Use.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Any new features or tools which are added to the current store shall also be subject to the Terms of Use. You can review the most current version of the Terms of Use at any time on this page. We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Use by posting updates and/or changes to our Site. It is your responsibility to check these Terms of Use regularly for changes. If you do not agree to the Terms of Use, please do not access or use our Site, Services and/or Content. Your continued use of or access to our Site, Services and/or Content following the posting of any amended Terms of Use constitutes acceptance of those Terms of Use.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We do not warrant that the materials on the Site are appropriate or available for use in locations outside Singapore. Accessing the Site from territories where its contents are illegal or unlawful is prohibited. If you choose to access the Site from elsewhere, you do so on your own initiative and are responsible for compliance with local laws. Love Nest disclaims all liability resulting therefrom.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `General and Condition`,
    data: [
      {
        data: `You represent and warrant that if you are registering for the Services as an individual, you are at least 13 years old, and if you are 18 years old or older, that you are fully capable and competent to enter into and to comply with these Terms, or if you are between 13 and 18 years old, that you have permission of a legal parent or guardian who has agreed to assume fully your obligations under these Terms as if such obligations are owed by the said parent or guardian. If you are registering for the Services for and on behalf on an entity that is not a natural person, you represent and warrant that you are authorized to enter into this agreement and bind the entity to these Terms. We reserve the right, at our sole discretion, to refuse to offer the Services to any person or entity and to change our eligibility criteria at any time. You may not create or use any account with us for any person or entity other than yourself except if you are authorized to do so for and on behalf of such person or entity.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Your access and use of the Services shall be at your own discretion and risk, and you shall be solely responsible for safeguarding and maintaining the confidentiality of your Account, as defined below, including your username and password and all activities in connection with or that occur on or through your Account, as defined below.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Services, violate any laws in your jurisdiction (including but not limited to copyright laws).`,
        subData: null,
        nextPage: false,
      },
      {
        data: `While using the Site, you must not transmit including but not limited to, any worms, viruses and/or any code of a destructive nature.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `A breach or violation of any of the Terms of Use will result in an immediate termination of the Services without further reference to you. We reserve the right, at our sole discretion, to refuse service to anyone for any or for no reason at any time.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Please note that credit card information is always encrypted during transfer over networks.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Accuracy, Completeness And Timelines Of Information`,
    data: [
      {
        data: `We are not responsible for the accuracy of information on this Site, complete or current. The material on this Site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or timelier sources of information. Any reliance on the material on this Site is at your own risk.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `This Site may contain certain historical information provided for your reference only and may not be current and up-to-date. We reserve the right to modify the contents of this Site at any time, but we have no obligation to update any information on our Site. You acknowledge and understand that it is your responsibility to monitor any changes to our Site.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Modifications To The Provision Of Services And Prices`,
    data: [
      {
        data: `We reserve the right, at our sole discretion and at any time to modify the prices of our products without further notice to you.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We reserve the right, at our sole discretion and at any time to modify or discontinue the Services (or any part or content thereof) without further notice to you.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We disclaim all liability to you or to any third-party for any changes to the Services, including but limited to modification, price change, suspension or discontinuance of the Services.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Account Information`,
    data: [
      {
        data: `To access certain parts of the Site, Services and/or Content, you will be required to register for an account with Love Nest ("Account"). In so doing, you will be required to input your Phone Number.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `It is your responsibility to safeguard and maintain the confidentiality of your Login ID and Password. You agree and undertake to: `,
        subData: [
          {
            sub: `You are solely responsible for safeguarding and maintaining the confidentiality of your Phone Number. You agree not to`,
          },
          {
            sub: `share or permit others to use your Phone Number and Email or`,
          },
          {
            sub: `assign or transfer your Account to any other person or entity.`,
          },
        ],
        nextPage: false,
      },
      {
        data: `You shall be bound by and responsible for all communications and online activity transmitted or conducted through the use of your Account.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We have no responsibility or liability for any loss, damage, cost, expenses, or liabilities arising as a result of or in connection with the wrongful or fraudulent use of your Account. If you are aware of any actual or suspected unauthorized use(s) of your Account, loss, theft, or unauthorized disclosure of your OTP, please notify us immediately at: contact@lovenest.com.sg.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You acknowledge and agree that each household or delivery address may only register one (1) Account. Multiple Account(s) registering the same delivery address may be permitted at our sole discretion.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Please provide accurate, complete, and up-to-date information required for your Account. You may at any time change or update your Account information by updating your information under My Account.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Where a delivery address is associated with multiple Account(s) without our written authorization, or fraudulent or wrongful use of an Account is detected or suspected, we reserve the right at our sole discretion, without liability, and without prejudice to our other rights and remedies under this Agreement or at law, to immediately:`,
        subData: [
          {
            sub: `cancel any orders placed through such Account(s);`,
          },
          {
            sub: `cancel or invalidate any credits or discount vouchers, rewards or codes awarded to or used by such Account(s);`,
          },
          {
            sub: `prohibit such Account(s) or persons from participating in any promotions, contests or surveys of Love Nest; and/or`,
          },
          {
            sub: `merge, suspend and/or terminate such Account(s).`,
          },
        ],
        nextPage: false,
      },
    ],
  },
  {
    title: `Disclaimer Of Warranties; Limitation Of Liability`,
    data: [
      {
        data: `We do not guarantee, represent or warrant that your use of our Site, Services and/or Content will be uninterrupted, timely, secure or error-free.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We do not warrant that the results that may be obtained from the use of our Site, Services and/or Content will be accurate or reliable.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You agree that from time to time we may remove our Site, Services and/or Content for indefinite periods of time or cancel our Site, Services and/or Content at any time, without notice to you.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You agree that your use of, or inability to use, our Site, Services and/or Content is at your own risk. Our Site, Services and/or Content and all products and services delivered to you through our Site, Services and/or Content are (except as expressly stated by us) provided 'as is' and 'as available' for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `In no case shall Love Nest, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation for any lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. As some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall only be limited to the extent permitted by law.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Optional Tools`,
    data: [
      {
        data: `We may provide you with access to third-party tools over which we do not monitor or have any control over whatsoever.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You acknowledge and agree that we provide access to such tools 'as is' and 'as available' without any warranties, representations or conditions of any kind and without any endorsement. We are not liable for, including but not limited to, any loss and/or damage suffered by you whatsoever arising from or relating to your use of optional third-party tools.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Any use by you of optional tools offered through the Site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We may also, in the future, offer new services and/or features through the Site (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Use.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Third party links`,
    data: [
      {
        data: `Third-party links on our Site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please ensure that you review the third-party's policies and practices and make sure you understand them before you engage in any transaction.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Any complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `User Comments, Feedback And Other Submissions`,
    data: [
      {
        data: `If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We are and shall be under no obligation, including but not limited to, to:`,
        subData: [
          {
            sub: `maintain any comments in confidence`,
          },
          {
            sub: `pay compensation for any comments; or`,
          },
          {
            sub: `respond to any comments.`,
          },
        ],
        nextPage: false,
      },
      {
        data: `We may, but are under no obligation to, monitor, edit or remove content that we determine, in our sole discretion, is unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Use.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `By sending and/or posting your comments, you agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You agree to not use a false e-mail address and phone number, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Personal Information`,
    data: [
      {
        data: `Your submission of personal information through the store is governed by our Privacy Policy. To view our Privacy Policy`,
        subData: null,
        nextPage: true,
      },
    ],
  },
  {
    title: `Accuracy Of Billing And Account Information`,
    data: [
      {
        data: `In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Errors, Inaccuracies And Omissions`,
    data: [
      {
        data: `Occasionally there may be information on our Site, in the Services and/or Content that contain typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Site, Services, Content or on any related website is inaccurate at any time without prior notice to you.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We undertake no obligation to update, amend or clarify information in the Site, Services, Content or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Site, Services, Content or on any related website should be taken to indicate that all information in the Site, Services, Content or on any related website has been modified or updated.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Prohibited Uses`,
    data: [
      {
        data: `In addition to other prohibitions as set forth in the Terms of Use, you are prohibited from using the Site, Services or Content:`,
        subData: [
          {
            sub: `for any unlawful purpose;`,
          },
          {
            sub: `to solicit others to perform or participate in any unlawful acts;`,
          },
          {
            sub: `to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances;`,
          },
          {
            sub: `to infringe upon or violate our intellectual property rights or the intellectual property rights of others;`,
          },
          {
            sub: `to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability;`,
          },
          {
            sub: `to submit false or misleading information;`,
          },
          {
            sub: `to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet;`,
          },
          {
            sub: `to collect or track the personal information of others;`,
          },
          {
            sub: `to spam, phish, pharm, pretext, spider, crawl, or scrape;`,
          },
          {
            sub: `for any obscene or immoral purpose; or`,
          },
          {
            sub: `to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet.`,
          },
        ],
        nextPage: false,
      },
    ],
  },
  {
    title: `Indemnification`,
    data: [
      {
        data: `You agree to indemnify, defend and hold harmless Love Nest and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Use or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Severability`,
    data: [
      {
        data: `In the event that any provision of these Terms of Use is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Use, such determination shall not affect the validity and enforceability of any other remaining provisions.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Termination`,
    data: [
      {
        data: `The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `These Terms of Use are effective unless and until terminated by us.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Your sole and exclusive right and remedy against us in the case of dissatisfaction with the Services or any other grievance shall be your termination and discontinuation of access to or use of the Services or by notifying us that you no longer wish to use our Services.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `If we are of the opinion that you have failed, or we suspect that you have failed, to comply with any term or provision of these Terms of Use, we also may terminate this agreement at any time without notice to you and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Promotion`,
    data: [
      {
        data: `Vouchers as released by Love Nest are valid only on the Site or mobile application.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `In order to use the vouchers, voucher codes must be entered at the checkout page. If you do not do so, we are unable to apply the voucher to your purchase.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Vouchers codes are to be used for a one time purchase only.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We reserve the right to cancel or modify orders, or revoke the use of voucher codes, where the vouchers are used in the following circumstances:`,
        subData: [
          {
            sub: `suspicious or fraudulent voucher use;`,
          },
          {
            sub: `voucher abuse, including vouchers redeemed using multiple accounts or multiple checkouts associated with the same customer or group of customers; and/ or`,
          },
          {
            sub: `vouchers used in bad faith (including resold vouchers, or use of vouchers on orders made and intended for resale).`,
          },
        ],
        nextPage: false,
      },
      {
        data: `Additional terms and conditions may be specified in relation to specific voucher codes (for example, duration, discount amount and products covered), and will govern the use and redemption of those vouchers in addition to these terms.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Vouchers are not exchangeable for cash. Love Nest reserves the right to change these terms or cancel any promotions at any time without notice.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Entire Agreement`,
    data: [
      {
        data: `The failure of us to exercise or enforce any right or provision of these Terms of Use shall not constitute a waiver of such right or provision.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `These Terms of Use and any policies or operating rules posted by us on this site or in respect to the Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Use).`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Governing Law`,
    data: [
      {
        data: `These Terms of Use and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of Singapore.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
];

export default termAndCondition;
