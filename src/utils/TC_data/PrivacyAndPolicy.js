const privacyAndPolicy = [
  {
    title: `Introduction`,
    data: [
      {
        data: `LOVE NEST HOLDING PTE. LTD. and our subsidiaries, affiliates, related corporations, associated companies, service providers and agents (“Love Nest”) take privacy very seriously. We ask that you read this privacy policy (the “Privacy Policy”) carefully as it contains important information about: (i) what to expect when Love Nest collects personal data (“Personal Data”) about you and how Love Nest will use your Personal Data; and (ii) how you may use personal data of our shopper and delivery personals (collectively, known as shopper and delivery bees). This Privacy Policy is provided in connection with [the Personal Data Protection Act (Act 26 of 2012) of Singapore (the “PDPA”)], and governed by and construed in accordance with [Singapore] law.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `This Privacy Policy applies to information we collect on our website lovenest.com.sg, and our mobile app ‘lovenest’ (collectively, the “Site”) about:`,
        subData: [
          {
            sub: `visitors to the Site;`,
          },
          {
            sub: `users who have registered an account with us through the Site;`,
          },
          {
            sub: `users who have provided their email addresses to us through the Site; and`,
          },
          {
            sub: `customers who have purchased something from us through the Site.`,
          },
        ],
        nextPage: false,
      },
    ],
  },
  {
    title: `Acceptance Of Privacy Policy`,
    data: [
      {
        data: `This Privacy Policy describes how your Personal Data is collected, used, shared, stored and protected by Love Nest, and how you may use the personal data of our shopper and delivery bees. By continuing to use the Site and/or by accessing and using our Services (as defined in our Terms of Use), you agree to be bound by the terms of this Privacy Policy which is subject to our Terms of Use and you give your consent for the collection, use and disclosure of your Personal Data and agree to use the personal data of our shopper and delivery bees in accordance with this Privacy Policy.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Collection Of Information`,
    data: [
      {
        data: `We may obtain data about you when you visit the Site. We may monitor your use of this Site through the use of cookies and similar tracking devices. For example, we may monitor the number of times you visit our Site or which other sites and/or web-pages you visit. This information helps us to build a profile of visitors to our Site and our users. Such data will be aggregated or statistical, which means that we will not be able to identify you individually.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We may collect and store your Personal Data including but not limited to the following:`,
        subData: [
          {
            sub: `personally identifiable information (e.g. your name, mailing address, e-mail address, phone number, password to access our Site, credit or debit card information, etc.) which you voluntarily provide for the Purposes (as defined below) in connection with your use or access of the Services including, but not limited to, when you create an account with us, make payments, upload, post, display or otherwise transmit any content on or through the Services, communicate with another person or entity through the Services or contact us by e-mail or telephone; and`,
          },
          {
            sub: `non-personally identifiable information (e.g. the URL you last visited or next access, your browser type, your IP address, etc.) which is collected automatically from you when you interact with the Site (as defined in our Terms of Use) through the use of software or other automated processes that the Site uses.`,
          },
        ],
        nextPage: false,
      },
      {
        data: `You may receive personal data of our shopper and delivery bees when they contact you to verify certain transactional details (such as your specific requirements about the products or substitutes, or confirmation of your delivery address).`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We will only keep Personal Data for as long as we are either required to by law, for our business, or as is relevant for the purposes for which it was collected. You agree to delete our shopper or delivery bees’ personal data once your order has been delivered or if you have cancelled your order. However, please do not hesitate to contact customer service should you have any questions about your order or concerns regarding any shopper and delivery bee.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Use Of Information`,
    data: [
      {
        data: `We may use your Personal Data for some or all of the following purposes and/or other purposes as notified to you or as permitted by law (the “Purposes”):`,
        subData: [
          {
            sub: `to process your registration for our Services;`,
          },
          {
            sub: `to process orders and payment transactions made by you;`,
          },
          {
            sub: `to respond to enquiries or requests from you and provide customer service;`,
          },
          {
            sub: `to send you information, notices, updates or to otherwise contact you when necessary;`,
          },
          {
            sub: `to help us provide, maintain, develop, test, enhance and personalize our Services to you;`,
          },
          {
            sub: `to diagnose and resolve any problems with the Services;`,
          },
          {
            sub: `to detect or investigate any prohibited, illegal, unauthorized or fraudulent activities;`,
          },
          {
            sub: `to monitor and analyze user activities and demographic data including trends and usage of the Services;`,
          },
          {
            sub: `to generate and/or track anonymous analytics data;`,
          },
          {
            sub: `to back up our systems and allow for disaster recovery;`,
          },
          {
            sub: `to protect the rights, property or personal safety of our staff or the public;`,
          },
          {
            sub: `to facilitate direct business-to-business sales with our partners;`,
          },
          {
            sub: `any other purposes in connection with the Services which we may notify to you or to further your interaction with us; and/or`,
          },
          {
            sub: `to comply with any law, court order or other legal compulsion.`,
          },
        ],
        nextPage: false,
      },
      {
        data: `Specifically, when you provide us with Personal Data to, including but not limited to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Furthermore, we may, from time to time, use your Personal Data to contact you to inform you about our latest products and services, promotional offers and other marketing information. You consent to being contacted for such purposes unless you opt out of receiving such information by contacting us at contact@lovenest.com.sg or sending us a letter addressed to Love Nest at City Gate 371, #02-16 Beach Rd, Singapore 199597.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You may use personal data of our shopper and delivery bees only to facilitate arrangements relating to the specific order.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Disclosure Of Information`,
    data: [
      {
        data: `We will only share your Personal Data with parties with whom we have a contractual or other relationship (unless otherwise agreed by you) including but not limited to, third parties that require such information as is reasonably necessary for the Purposes. Such third parties may include the service providers, contractors, consultant, agents and/or representatives who provide services to the Company in connection with the business and/or operation.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `The parties with whom we share your Personal Data will be under reasonable confidentiality terms not to use your personally identifiable information for any purpose not specified in this Privacy Policy. Notwithstanding the foregoing, we do not control and shall not be responsible or liable in any way for the collection, use and protection of your Personal Data by any party with whom we share your Personal Data including any businesses, other entities and/or third-party service providers that we may conduct direct business-to-business sales with. Where you require us to provide Personal Data to any third party, it shall be your responsibility to read the privacy policy of such parties before providing any Personal Data to them.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `You may not disclose personal data of our shopper and delivery bees to any third parties except to facilitate delivery of the products that you have ordered (such as to a family member who will receive the products on your behalf).`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Information To Third-party Services`,
    data: [
      {
        data: `Once you leave our Site or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our Site’s Terms of Use.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Specifically, once a link is clicked and you are directed away from our Site, we will no longer be responsible for the privacy practices of other sites and encourage you to read their privacy statements.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Security`,
    data: [
      {
        data: `To protect your Personal Data, we take commercially reasonable precautions, maintain appropriate safeguards and follow industry practices with respect to security and privacy of information, to minimise risk of loss, misuse or unauthorised alteration of such information under our control. Notwithstanding the foregoing, while we strive to protect your Personal Data, we cannot guarantee the security or privacy of such information, or that such information will not be accessed, disclosed, altered or destroyed by third parties without our permission, due to factors beyond our control.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `All credit card information provided to us is encrypted using secure socket layer technology (SSL) and stored with an AES-256 encryption. We follow all PCI-DSS requirements and implement additional generally accepted industry standards to reasonably protect and safeguard all credit card information provided to us.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Withdrawal, Access And Correction Of Your Personal Data`,
    data: [
      {
        data: `Should you wish to withdraw your consent to use of your Personal Data or obtain access to or make corrections to your Personal Data records, please give Love Nest reasonable notice by sending a written request to contact@lovenest.com.sg or to Love Nest at 2 City Gate 371, #02-16 Beach Rd, Singapore 199597. We reserve the right to charge a reasonable administrative fee for retrieving your personal data records.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Love Nest will cease to collect, use or disclose the Personal Data within 7 working days upon receipt of such withdrawal request unless otherwise permitted or required by the PDPA or other applicable laws and regulations.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Any complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `Please note that if you withdraw your consent to any or all use of your Personal Data, depending on the nature of your request, Love Nest may not be in a position to continue to provide Services to you and this may result in the termination of any contractual relationship in place. Love Nest’s legal rights and remedies in such event are expressly reserved.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `We also want to ensure that your Personal Data is accurate and up to date. You must only submit to us information which is accurate and not misleading. If any of the information that you have provided to Love Nest changes and/or if you wish to cancel your registration, please let us know by sending an email to contact@lovenest.com.sg or by sending a letter to Love Nest at City Gate 371, #02-16 Beach Rd, Singapore 199597.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Cookies`,
    data: [
      {
        data: `We may use cookies, web beacons and other similar tracking technology in connection with your access to or use of the Site. Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior information. The information is used to track visitor use of the Site and to compile statistical reports on Site activity and allow us to improve the Site. Cookies also enable you to use or access certain features or services of our Site or Service, including the shopping cart and check-out features. It may be possible for you to disable some but not all cookies through your device or browser settings, but doing so may affect your ability to access or use the Site.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Age of Consent`,
    data: [
      {
        data: `By using this Site, you represent that you are at least the age of majority in your country, state or province of residence, or that you are the age of majority in your country, state or province of residence and you have given us your consent to allow any of your minor dependents to use this Site.
`,
        subData: null,
        nextPage: true,
      },
    ],
  },
  {
    title: `Changes To This Privacy Policy`,
    data: [
      {
        data: `We reserve the right, at our sole discretion, to modify or update this Privacy Policy at any time and all changes will take effect immediately upon posting. If we make material changes to this Privacy Policy, we will notify you via the Site that it has been updated.`,
        subData: null,
        nextPage: false,
      },
      {
        data: `If Love Nest is acquired or merged with another company, your information may be transferred to the new owners to enable the continued provision of Services in accordance with the Terms of Use and this Privacy Policy.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
  {
    title: `Questions And Contact Information`,
    data: [
      {
        data: `If you have any questions regarding this Privacy Policy or you would like to register a complaint, kindly contact our Privacy Compliance Officer at contact@lovenest.com.sg or send a letter addressed to the Privacy Compliance Offer, Love Nest at City Gate 371, #02-16 Beach Rd, Singapore 199597.`,
        subData: null,
        nextPage: false,
      },
    ],
  },
];

export default privacyAndPolicy;
