import axios from 'axios';
import APIKEY from '../environment/MapAPI';

class GeocodeWithLatLng {
  getLocation = (lat, lng) => {
    return new Promise(async (resolve, reject) => {
      try {
        const uri = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${APIKEY}`;
        const getGeocode = await axios.get(uri);
        console.log('getGeocode: ', getGeocode.data.results[0]);
        if (getGeocode.data.results.length > 0) {
          const {geometry} = getGeocode.data.results[0];
          const {location} = geometry;
          const {lat, lng} = location;
          const dataGeo = getGeocode.data.results[0];
          const length = dataGeo?.address_components?.length
            ? dataGeo.address_components.length
            : [];
          const result = {
            postalCode: dataGeo?.address_components[length - 1]?.long_name,
            country: dataGeo?.address_components[length - 2]?.long_name,
            city: dataGeo?.address_components[length - 3]?.long_name,
            lat,
            lng,
            address: dataGeo?.formatted_address,
          };
          resolve(result);
        }
      } catch (error) {
        console.log('GeocodeWithLatLng: ', error);
        reject(error);
      }
    });
  };
}

const dataLocation = new GeocodeWithLatLng();

export default dataLocation;
