import {Platform, Linking} from 'react-native';
import PushNotification, {Importance} from 'react-native-push-notification';
import messaging from '@react-native-firebase/messaging';
import Topics from '../../environment/firebaseSubscriptionNotification';
import CommonChannel from '../../environment/localPushNotificationChannel';
import {CommonActions} from '@react-navigation/native';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

// Query Graphql
import SaveTokenToServer from '../../graphql/queries/saveTokenNotification';

var temporaryData;

// set notification requirement
const pushNotifConfigure = () => {
  try {
    PushNotification.configure({
      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      largeIcon: 'ic_icon',
      smallIcon: 'ic_icon',

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

// (optional) Called when Token is generated (iOS and Android)
const pushNotifOnRegister = async () => {
  try {
    await PushNotification.configure({
      onRegister: FCMTOKEN => {
        console.log('pushNotifOnRegister On Register: ', FCMTOKEN);
      },
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

// (required) Called when a remote is received or opened, or local notification is opened
const pushNotifOnNotification = (client, navigation) => {
  try {
    PushNotification.configure({
      onNotification: notification => {
        console.log(
          'pushNotifOnNotification Config onNotification: ',
          notification,
        );
        notification?.id !== undefined
          ? (temporaryData = notification)
          : (temporaryData = temporaryData);
        if (Platform.OS === 'ios') {
          if (notification) {
            console.log(
              'IOS pushNotifOnNotification notification: ',
              notification,
            );
            if (
              notification.foreground === true &&
              notification.userInteraction === true
            ) {
              console.log('MASUK TAPPED BRO: ', {
                foreground: notification.foreground,
                userInteraction: notification.userInteraction,
                temporaryData,
              });
              // (required) Called when a remote is received or opened, or local notification is opened
              notification.finish(PushNotificationIOS.FetchResult.NoData);
              onTappedLocalNotif(
                client,
                navigation,
                temporaryData ? temporaryData : notification,
              );
            } else {
              // Non Tapped
              console.log('Notification: ', notification);
            }
          }
        } else {
          // Android Handler
          console.log('Notification Android Android Handler: ', notification);
          if (notification?.foreground && notification?.userInteraction) {
            // Handle tapped when app in foregroudn and notif tapped
            onTappedLocalNotif(client, navigation, notification);
          } else if (
            !notification?.foreground &&
            notification?.userInteraction
          ) {
            // handle when app killed and notif bar tapped
            onTappedLocalNotif(client, navigation, notification);
          } else {
            //  No nothing
          }
        }
      },
      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: notification => {
        console.log('Config onAction:', notification);
        // process the action
        if (notification) {
          if (Platform.OS === 'ios') {
            // iOS
          } else {
            // Android
          }
        }
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: err => {
        console.error('error onRegistrationError', err);
      },
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

// (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
const pushNotifOnAction = () => {
  try {
    PushNotification.configure({
      onAction: notification => {
        console.log('pushNotifOnAction Config onAction:', notification);
        // process the action
        if (notification) {
          if (Platform.OS === 'ios') {
            // iOS
          } else {
            // Android
          }
        }
      },
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

// (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
const pushNotifOnRegistrationError = () => {
  try {
    PushNotification.configure({
      onRegistrationError: error => {
        console.log(
          'pushNotifOnRegistrationError Config onRegistrationError : ',
          error,
        );
      },
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

const onTappedLocalNotif = (client, navigation, notification) => {
  try {
    console.log('onTappedLocalNotif: ', notification);
    if (Platform.OS === 'ios') {
      const {data} = notification;
      const {id, notifId, type} = data;
      if (type === 'Appointment') {
        // To Appointment Detail Screen
        navigation.dispatch(
          CommonActions.navigate({
            name: 'AppointmentDetail',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
            },
          }),
        );
      } else if (type === 'Event') {
        // To Order Detail for Event
        navigation.dispatch(
          CommonActions.navigate({
            name: 'OrderDetail',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
              orderType: data.orderType,
            },
          }),
        );
      } else if (type === 'Order') {
        // To Order Detail for Bridal
        if (data?.orderId) {
          // for Photo Album Progress
          navigation.dispatch(
            CommonActions.navigate({
              name: 'PhotoAlbumProgress',
              params: {
                orderId: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              },
            }),
          );
        } else {
          navigation.dispatch(
            CommonActions.navigate({
              name: 'OrderDetail',
              params: {
                id: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              },
            }),
          );
        }
      } else if (
        data?.type === 'NEWSEVENT' ||
        data?.type === 'NEWSPROMO' ||
        data?.type === 'NEWS'
      ) {
        navigation.dispatch(
          CommonActions.navigate({
            name: 'Notification',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
            },
          }),
        );
      }
    } else {
      console.log('Android on Tap: ', notification);
      const {data} = notification;
      const {id, notifId, type} = data;
      if (type === 'Appointment') {
        navigation.dispatch(
          CommonActions.navigate({
            name: 'AppointmentDetail',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
            },
          }),
        );
      } else if (type === 'Event') {
        // To Order Detail for Event
        navigation.dispatch(
          CommonActions.navigate({
            name: 'OrderDetail',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
              orderType: data.orderType,
            },
          }),
        );
      } else if (type === 'Order') {
        // To Order Detail for Bridal
        if (data?.orderId) {
          // for Photo Album Progress
          navigation.dispatch(
            CommonActions.navigate({
              name: 'PhotoAlbumProgress',
              params: {
                orderId: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              },
            }),
          );
        } else {
          navigation.dispatch(
            CommonActions.navigate({
              name: 'OrderDetail',
              params: {
                id: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              },
            }),
          );
        }
      } else if (
        data?.type === 'NEWSEVENT' ||
        data?.type === 'NEWSPROMO' ||
        data?.type === 'NEWS'
      ) {
        navigation.dispatch(
          CommonActions.navigate({
            name: 'Notification',
            params: {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
            },
          }),
        );
      }
    }
  } catch (error) {
    console.log('Error onTappedLocalNotif: ', error);
  }
};

const registerAppIOSWithFCM = async () => {
  try {
    await messaging().registerDeviceForRemoteMessages();
    await messaging().setAutoInitEnabled(true);
  } catch (error) {
    console.log('Error: ', error);
  }
};

const onRegister = (client, navigation) => {
  try {
    console.log('masuk sini');
    checkPermission(client, navigation);
  } catch (error) {
    console.log('Error: ', error);
  }
};

const checkPermission = async (client, navigation) => {
  console.log('checkPermission: >>>>> BRO');
  try {
    await messaging()
      .hasPermission()
      .then(async enabled => {
        console.log('PERMISSION NOTIF BRO: ', enabled);
        if (enabled) {
          await getFCMToken(client, navigation);
        } else {
          await requestPermission(client, navigation);
        }
      })
      .catch(error => {
        console.log('FCM Error Check Permission', error);
      });
  } catch (error) {
    console.log('Error: ', error);
  }
};

const requestPermission = async (client, navigation) => {
  try {
    await messaging()
      .requestPermission()
      .then(async () => {
        await getFCMToken(client, navigation);
      })
      .catch(error => {
        console.log('Error Request Permission: ', error);
      });
  } catch (error) {
    console.log('Error: ', error);
  }
};

const getFCMToken = (client, navigation) => {
  try {
    console.log('MASUK KE GETFCMTOKEN');
    messaging()
      .getToken()
      .then(fcmToken => {
        console.log('FCM TOKEN: ', fcmToken);
        if (fcmToken) {
          client
            .query({
              query: SaveTokenToServer,
              variables: {
                token: fcmToken,
              },
              ssr: false,
              fetchPolicy: 'no-cache',
            })
            .then(async response => {
              console.log('Response Saving FCM Token: ', response);
              await pushNotifConfigure(client, navigation);
              await pushNotifOnRegister(client, navigation);
              await pushNotifOnNotification(client, navigation);
            })
            .catch(error => {
              console.log('Error Saving FCM Token: ', error);
            });
        }
      })
      .catch(error => {
        console.log('getFCMToken Error: ', error);
      });
  } catch (error) {
    console.log('Error getFCMToken: ', error);
  }
};

// this work when app killed, and notif bar tapped (only android)
// when app running , but in background
const onNotificationOpenedApp = async (client, navigation) => {
  try {
    // when app running , but in background
    await messaging().onNotificationOpenedApp(remoteMessage => {
      console.log('FCM onNotificationOpenedApp: ', remoteMessage);
      const {data} = remoteMessage;
      const {id, notifId, type} = data;

      if (type === 'Appointment') {
        // To Appointment Detail Screen
        navigation.navigate('Appointments', {
          screen: 'AppointmentDetail',
          params: {
            id: parseInt(id, 10),
            notifId: parseInt(notifId, 10),
            type,
          },
        });
      } else if (type === 'Event') {
        // To Order Detail for Event
        navigation.navigate('OrderDetail', {
          id: parseInt(id, 10),
          notifId: parseInt(notifId, 10),
          type,
          orderType: data.orderType,
        });
      } else if (type === 'Order') {
        console.log('MASUK SINI BAMBANG: ', remoteMessage);
        if (data?.orderId) {
          // To Order Photo ALbum Progress
          navigation.navigate('PhotoAlbumProgress', {
            orderId: parseInt(id, 10),
            notifId: parseInt(notifId, 10),
            type,
          });
        } else {
          // To Order Detail for Bridal
          navigation.navigate('OrderDetail', {
            id: parseInt(id, 10),
            notifId: parseInt(notifId, 10),
            type,
          });
        }
      } else if (
        data?.type === 'NEWSEVENT' ||
        data?.type === 'NEWSPROMO' ||
        data?.type === 'NEWS'
      ) {
        console.log('MASUK SINII IOS Notification Notif');
        const url = 'lovenest://MainHome/Homes/Notification';
        Linking.canOpenURL(url).then(supported => {
          if (supported) {
            Linking.openURL(url);
          } else {
            console.log("Don't know how to open URI: " + this.props.url);
          }
        });
      }
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

// this is when app killed and tap the notif bar (iOS only run this)
// is happen when app is opened from quit state
const getInitialNotification = async (client, navigation) => {
  try {
    // is happen when app is opened from quit state
    await messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        console.log('FCM getInitialNotification: ', remoteMessage);
        if (remoteMessage) {
          const {data} = remoteMessage;
          const {id, notifId, type} = data;

          if (type === 'Appointment') {
            // To Appointment Detail Screen
            navigation.navigate('Appointments', {
              screen: 'AppointmentDetail',
              params: {
                id: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              },
            });
          } else if (type === 'Event') {
            // To Order Detail for Event
            navigation.navigate('OrderDetail', {
              id: parseInt(id, 10),
              notifId: parseInt(notifId, 10),
              type,
              orderType: data.orderType,
            });
          } else if (type === 'Order') {
            if (data?.orderId) {
              // To Order Photo ALbum Progress
              navigation.navigate('PhotoAlbumProgress', {
                orderId: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              });
            } else {
              // To Order Detail for Bridal
              navigation.navigate('OrderDetail', {
                id: parseInt(id, 10),
                notifId: parseInt(notifId, 10),
                type,
              });
            }
          } else if (
            data?.type === 'NEWSEVENT' ||
            data?.type === 'NEWSPROMO' ||
            data?.type === 'NEWS'
          ) {
            console.log('MASUK SINII IOS Notification Notif');
            const url = 'lovenest://MainHome/Homes/Notification';
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + this.props.url);
              }
            });
          }
        }
      })
      .catch(error => {
        console.log('Error FCM getInitialNotification: ', error);
      });
  } catch (error) {
    console.log('error: ', error);
  }
};

// (ios/android)
// trigerred foregorund message
const onMessage = async () => {
  try {
    // trigerred foregorund message
    await messaging().onMessage(async remoteMessage => {
      console.log('FCM onMessage: ', remoteMessage);
      temporaryData = undefined;

      if (
        remoteMessage?.data?.type === 'Appointment' ||
        remoteMessage?.data?.type === 'Event' ||
        remoteMessage?.data?.type === 'Order'
      ) {
        // Common Notification
        if (Platform?.OS === 'android') {
          // Android Common
          if (remoteMessage) {
            await PushNotification.localNotification({
              autoCancel: true,
              channelId: CommonChannel,
              title: remoteMessage?.notification?.title,
              message: remoteMessage?.notification?.body,
              data: remoteMessage?.data,
              priority: 'high',
              largeIcon: 'ic_launcher',
              smallIcon: 'ic_notification',
            });
          }
        } else {
          // IOS Common
          if (remoteMessage) {
            const {notification, data = {}} = remoteMessage;
            temporaryData = remoteMessage;
            const {title, body, id} = notification;
            await PushNotification.localNotification({
              autoCancel: true,
              channelId: CommonChannel,
              largeIcon: 'ic_launcher',
              smallIcon: 'ic_notification',
              title,
              message: body,
              data,
              priority: 'high',
            });
          }
        }
      } else if (
        remoteMessage?.data?.type === 'NEWSEVENT' ||
        remoteMessage?.data?.type === 'NEWSPROMO' ||
        remoteMessage?.data?.type === 'NEWS'
      ) {
        // Blast Notif
        console.log('BLAZZZZ', remoteMessage);
        if (Platform?.OS === 'android') {
          if (remoteMessage) {
            await PushNotification.localNotification({
              autoCancel: true,
              channelId: Topics,
              title: remoteMessage?.notification?.title,
              message: remoteMessage?.notification?.body,
              data: remoteMessage?.data,
              priority: 'high',
              largeIcon: 'ic_launcher',
              smallIcon: 'ic_notification',
            });
          }
        } else {
          if (remoteMessage) {
            temporaryData = remoteMessage;
            const {notification} = remoteMessage;
            temporaryData = remoteMessage;
            const {title, body, id} = notification;
            await PushNotification.localNotification({
              autoCancel: true,
              channelId: Topics,
              largeIcon: 'ic_launcher',
              smallIcon: 'ic_notification',
              title,
              message: body,
              data: {
                ...remoteMessage?.data,
                specialNotif: true,
              },
              priority: 'high',
            });
          }
        }
      } else {
        console.log('Not Running Push Notification');
      }
    });
  } catch (error) {
    console.log('Error: ', error);
  }
};

const onRefreshToken = () => {
  return new Promise(async resolve => {
    try {
      await messaging().onTokenRefresh(newFCMToken => {
        if (newFCMToken) {
          console.log('FCM NEW TOKEN: ', newFCMToken);
          resolve(newFCMToken);
        } else {
          resolve(null);
        }
      });
    } catch (error) {
      console.log('Error: ', error);
      resolve(null);
    }
  });
};

const subTopic = async () => {
  try {
    await PushNotification.getChannels(channelID => {
      console.log('Channel ID Local IOS/Android: ', channelID);
    });

    // Common Notif Register
    await PushNotification.createChannel(
      {
        channelId: CommonChannel, // (required)
        channelName: CommonChannel, // (required)
        channelDescription: 'for common notif only', // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      created => console.log(`createChannel Common returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
    );

    // Subscription Topic Remote FCM
    await messaging()
      .subscribeToTopic(Topics)
      .then(async response => {
        console.log('Sub TOPIC: ', response);
        // This is for Blast Notif
      })
      .catch(error => {
        console.log('Error: ', error);
      });
  } catch (error) {
    console.log('Error: ', error);
  }
};

const remoteMessage = async (client, navigation) => {
  const register = await messaging().isAutoInitEnabled();
  console.log('REGISTER BRUU: ', register);
};

const unRegister = () => {
  // this.messsageListener();
  PushNotification.unregister();
};

export default {
  pushNotifConfigure,
  pushNotifOnRegister,
  pushNotifOnNotification,
  pushNotifOnAction,
  pushNotifOnRegistrationError,
  onTappedLocalNotif,
  registerAppIOSWithFCM,
  onRegister,
  checkPermission,
  requestPermission,
  getFCMToken,
  onNotificationOpenedApp,
  getInitialNotification,
  onMessage,
  onRefreshToken,
  subTopic,
  unRegister,
  remoteMessage,
};
