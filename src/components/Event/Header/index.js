import React from 'react';
import {Dimensions, Animated, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {Left, Body, Right} from 'native-base';
import {hasNotch} from 'react-native-device-info';

const {black, white, greyLine} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const HeaderEvent = props => {
  const {headerOpacity, name, headerHeight} = props;
  return (
    <Animated.View
      style={{
        width: Dimensions.get('window').width,
        position: 'absolute',
        top: 0,
        zIndex: 2,
        opacity: headerOpacity,
        backgroundColor: white,
        height: headerHeight,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
      }}>
      <Left style={{flex: 0.2}} />
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <Animated.Text
          numberOfLines={1}
          style={{
            top: hasNotch() ? 15.5 : 14.5,
            fontFamily: medium,
            fontSize: regular,
            color: black,
          }}>
          {name}
        </Animated.Text>
      </Body>
      <Right style={{flex: 0.2}} />
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(HeaderEvent);

export default props => <Wrapper {...props} />;
