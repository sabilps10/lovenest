import React from 'react';
import {Animated, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AnimatedAsyncImage from '../../Image/AnimatedAsyncImage';

const AimatedHeaderImage = props => {
  console.log('HeaderImage Props: ', props);
  const {containerStyle, imageStyle, imageSource, placeholderColor} = props;
  return (
    <Animated.View
      style={{
        ...containerStyle,
      }}>
      <AnimatedAsyncImage
        resizeMode={'cover'}
        style={{
          ...imageStyle,
        }}
        loaderStyle={{
          width: Dimensions.get('window').width / 7,
          height: Dimensions.get('window').width / 7,
        }}
        source={imageSource}
        placeholderColor={placeholderColor}
      />
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(AimatedHeaderImage);

export default props => <Wrapper {...props} />;
