import React, {useState} from 'react';
import {Text, View, Dimensions, TouchableOpacity, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';
import Colors from '../../../utils/Themes/Colors';
import moment from 'moment';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import ModalRSVP from '../Modal/ModalRSVP';
import ModalLogin from '../Modal/ModalLogin';
import AsyncStorage from '@react-native-community/async-storage';

// authKey
import AuthKey from '../../../utils/AsyncstorageDataStructure';

// Ribbon
import RibbonGift from '../Ribbon/ribbonGift';
import RibbonPrice from '../Ribbon/ribbonPrice';

const {asyncToken} = AuthKey;
const {medium, book} = FontType;
const {greyLine, white, black, mainGreen, headerBorderBottom} = Colors;
const AnimatedCard = Animated.createAnimatedComponent(Card);
const {width, height} = Dimensions.get('window');

const CardEvent = props => {
  console.log('CardEvent Props: ', props);
  const {data, navigation} = props;
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const [showModalRSVP, setShowModalRSVP] = useState(false);
  const [showModalLogin, setShowModalLogin] = useState(false);

  const showRSVP = async () => {
    try {
      const checkAuth = await AsyncStorage.getItem(asyncToken);
      console.log('checkAuth: ', checkAuth);
      if (checkAuth) {
        setShowModalRSVP(true);
      } else {
        setShowModalLogin(true);
      }
    } catch (error) {
      setShowModalLogin(false);
      setShowModalRSVP(false);
    }
  };

  const closeModal = () => {
    setShowModalLogin(false);
    setShowModalRSVP(false);
  };

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (data) {
    const logoSource =
      data.eventBannerDynamicURL === null
        ? {uri: data.eventBannerURL}
        : {uri: `${data.eventBannerDynamicURL}=h500`};
    const startDate = moment.utc(data.date).format('MMM DD');
    const endDate = moment.utc(data.endDate).format('MMM DD');
    const isSameDate = moment.utc(data.date).isSame(data.endDate);
    const eventName = data.name;
    const eventId = data.id;
    return (
      <>
        <ModalLogin
          closeModal={closeModal}
          status={showModalLogin}
          navigation={navigation}
        />
        <ModalRSVP
          data={data}
          closeModal={closeModal}
          status={showModalRSVP}
          eventName={eventName}
          eventId={eventId}
          navigation={navigation}
        />
        <TouchableOpacity
          style={{marginRight: 10}}
          onPress={() => {
            navigation.navigate('EventDetail', {id: eventId});
          }}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            style={{borderRadius: 4, transform: [{scale}], width: width / 1.8}}>
            <CoverImage logoSource={logoSource} />
            <DateAndTitle
              startDate={startDate}
              endDate={endDate}
              isSameDate={isSameDate}
              eventName={eventName}
            />
            <GlobalRibbon data={data} />
            <Summary data={data} />
            <ButtonRSVP data={data} showRSVP={showRSVP} />
          </AnimatedCard>
        </TouchableOpacity>
      </>
    );
  } else {
    return null;
  }
};

export const ButtonRSVP = props => {
  console.log('ButtonRSVP Props: ', props);
  const {showRSVP} = props;

  return (
    <CardItem
      style={{
        paddingTop: 0,
        width: '100%',
        backgroundColor: 'transparent',
      }}>
      <Button
        disabled={props?.data?.isFullBooked ? true : false}
        onPress={() => showRSVP()}
        style={{
          borderRadius: 4,
          backgroundColor: props?.data?.isFullBooked
            ? headerBorderBottom
            : mainGreen,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            color: props?.data?.isFullBooked ? greyLine : white,
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          RSVP
        </Text>
      </Button>
    </CardItem>
  );
};

export const Summary = props => {
  const {data} = props;

  return (
    <CardItem
      style={{
        paddingTop: 0,
        width: '100%',
        backgroundColor: 'transparent',
      }}>
      <Text
        style={{
          fontFamily: book,
          color: black,
          fontSize: RFPercentage(1.3),
          letterSpacing: 0.3,
          lineHeight: 18,
        }}
        numberOfLines={2}>
        {data?.summary}
      </Text>
    </CardItem>
  );
};

export const GlobalRibbon = props => {
  const {data} = props;
  return (
    <CardItem
      style={{
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 0,
        paddingBottom: 8,
        width: '100%',
        backgroundColor: 'transparent',
      }}>
      <RibbonPrice data={data} />
      {data.freeGift ? <RibbonGift data={data} /> : null}
    </CardItem>
  );
};

export const DateAndTitle = props => {
  const {startDate, endDate, eventName, isSameDate} = props;

  return (
    <CardItem
      style={{
        width: '100%',
        backgroundColor: 'transparent',
        paddingLeft: 10,
        paddingRight: 10,
      }}>
      <View
        style={{
          flex: isSameDate ? 0.2 : 0.5,
          flexDirection: 'row',
          height: '100%',
          marginRight: isSameDate ? 5 : 8,
        }}>
        <View style={{flex: 1}}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: medium,
              fontSize: RFPercentage(1.2),
              color: black,
              lineHeight: 18,
            }}>
            {startDate.toLocaleUpperCase()}
          </Text>
        </View>
        {isSameDate ? null : (
          <>
            <View
              style={{
                flex: 0.5,
                flexDirection: 'row',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.2),
                  color: black,
                  lineHeight: 18,
                }}>
                -
              </Text>
            </View>
            <View style={{flex: 1}}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.2),
                  color: black,
                  lineHeight: 18,
                }}>
                {endDate.toUpperCase()}
              </Text>
            </View>
          </>
        )}
      </View>
      <View
        style={{
          height: '100%',
          width: 1.5,
          backgroundColor: greyLine,
          marginRight: 8,
        }}
      />
      <View
        style={{
          flex: 1,
          height: '100%',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'flex-start',
        }}>
        <Text
          numberOfLines={2}
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.3),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          {eventName}
        </Text>
      </View>
    </CardItem>
  );
};

export const CoverImage = props => {
  const {logoSource} = props;

  return (
    <CardItem cardBody>
      <AsyncImage
        resizeMode={'cover'}
        style={{
          width: '100%',
          height: height / 7.5,
          borderTopLeftRadius: 4,
          borderTopRightRadius: 4,
        }}
        loaderStyle={{
          width: Dimensions.get('window').width / 7,
          height: Dimensions.get('window').width / 7,
        }}
        source={logoSource}
        placeholderColor={greyLine}
      />
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(CardEvent);

export default props => <Wrapper {...props} />;
