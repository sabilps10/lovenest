import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import ElepsisDescription from '../EventDescription/elepsisDescription';
import NonElepsisDescription from '../EventDescription/nonElepsisDescription';
import {FontType} from '../../../utils/Themes/Fonts';

const {black, lightSalmon, mainRed} = Colors;
const {medium} = FontType;

const DescriptionEvent = props => {
  console.log('DescriptionEvent Props: ', props);
  const {
    description,
    seeMore,
    setSeeMore,
    descriptionLength,
    setDescriptionLength,
    fit,
  } = props;
  return (
    <CardItem style={{width: '100%'}}>
      <View style={{flex: fit ? 0.1 : 0.15, height: '100%'}}>
        <Icon
          type="Feather"
          name="clipboard"
          style={{
            top: 6,
            fontSize: fit ? RFPercentage(2.2) : RFPercentage(1.8),
            color: black,
          }}
        />
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}>
        {!seeMore ? (
          <>
            <ElepsisDescription
              description={description}
              descriptionLength={length => setDescriptionLength(length)}
            />
            {descriptionLength > 1 ? (
              <Text style={{bottom: 31}}>....</Text>
            ) : null}
          </>
        ) : (
          <NonElepsisDescription description={description} />
        )}
        {descriptionLength > 1 ? (
          <TouchableOpacity
            onPress={() => setSeeMore()}
            style={{
              height: 30,
              // bottom: seeMore ? 70 : 30,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainRed,
                letterSpacing: 0.3,
                lineHeight: 25,
              }}>
              {seeMore ? 'See Less' : 'See More'}
            </Text>
          </TouchableOpacity>
        ) : null}
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(DescriptionEvent);

export default props => <Wrapper {...props} />;
