import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';

const {black} = Colors;
const {book} = FontType;

const ScheduleEvent = props => {
  console.log('ScheduleEvent Props: ', props);
  const {
    isSameDate,
    startDateEvent,
    endDateEvent,
    startTimeEvent,
    endTimeEvent,
    fit,
  } = props;

  const renderTimeEvent = () => {
    if (isSameDate && startTimeEvent === endTimeEvent) {
      return (
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {`${startTimeEvent}`}
        </Text>
      );
    } else {
      return (
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {`${startTimeEvent} - ${endTimeEvent}`}
        </Text>
      );
    }
  };
  return (
    <CardItem style={{width: '100%'}}>
      <View style={{flex: fit ? 0.1 : 0.15, height: '100%'}}>
        <Icon
          type="Feather"
          name="clock"
          style={{
            top: 6,
            fontSize: fit ? RFPercentage(2.2) : RFPercentage(1.8),
            color: black,
          }}
        />
      </View>
      <View style={{flex: 1, flexDirection: 'column'}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {isSameDate
            ? `${startDateEvent}`
            : `${startDateEvent} - ${endDateEvent}`}
        </Text>
        {/* <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {isSameDate
            ? `${startTimeEvent}`
            : `${startTimeEvent} - ${endTimeEvent}`}
        </Text> */}
        {renderTimeEvent()}
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(ScheduleEvent);

export default props => <Wrapper {...props} />;
