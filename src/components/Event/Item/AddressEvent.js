import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';

const {black} = Colors;
const {book} = FontType;

const AddressEvent = props => {
  console.log('AddressEvent Props: ', props);
  const {venueName, address1, address2, country, postCode, fit} = props;
  return (
    <CardItem style={{width: '100%'}}>
      <View style={{flex: fit ? 0.1 : 0.15, height: '100%'}}>
        <Icon
          type="Feather"
          name="map-pin"
          style={{
            top: 6,
            fontSize: fit ? RFPercentage(2.2) : RFPercentage(1.8),
            color: black,
          }}
        />
      </View>
      <View style={{flex: 1, flexDirection: 'column'}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {venueName} {'\n'}
          {address1 !== null ? address1 : address2}, {country} {postCode}
        </Text>
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(AddressEvent);

export default props => <Wrapper {...props} />;
