import React from 'react';
import {Text, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';

const AnimatedCardItem = Animated.createAnimatedComponent(CardItem);
const {medium} = FontType;
const {black} = Colors;

const TitleEvent = props => {
  console.log('TitleEvent Props: ', props);
  const {title} = props;
  return (
    <AnimatedCardItem>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(2.1),
          color: black,
          letterSpacing: 0.3,
          lineHeight: 25,
        }}>
        {title}
      </Text>
    </AnimatedCardItem>
  );
};

const Wrapper = compose(withApollo)(TitleEvent);

export default props => <Wrapper {...props} />;
