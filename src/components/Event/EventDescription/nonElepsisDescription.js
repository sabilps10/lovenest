import React from 'react';
import {Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import HTML from 'react-native-render-html';

const {black} = Colors;
const {book} = FontType;

const NonElepsisDescription = props => {
  console.log('NonElepsisDescription Props: ', props);
  const {description, smallText} = props;
  return (
    <>
      <HTML
        ignoredTags={[]}
        html={description}
        containerStyle={{top: -15}}
        baseFontStyle={{
          fontFamily: book,
          fontSize: smallText ? RFPercentage(1.6) : RFPercentage(1.8),
          color: black,
          letterSpacing: 0.3,
          lineHeight: 25,
        }}
        imagesInitialDimensions={{width: 100, height: 100}}
        imagesMaxWidth={Dimensions.get('window').width / 1.3}
      />
    </>
  );
};

const Wrapper = compose(withApollo)(NonElepsisDescription);

export default props => <Wrapper {...props} />;
