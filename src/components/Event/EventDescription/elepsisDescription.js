import React from 'react';
import {Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import HTML from 'react-native-render-html';

const {black} = Colors;
const {book} = FontType;

const ElepsisDescription = props => {
  console.log('Elepsis Description Props: ', props);
  const {description, descriptionLength} = props;
  return (
    <>
      <HTML
        ignoredTags={[]}
        html={description}
        containerStyle={{top: -13.5}}
        baseFontStyle={{
          fontFamily: book,
          fontSize: RFPercentage(1.7),
          color: black,
          letterSpacing: 0.3,
          lineHeight: 20,
        }}
        imagesInitialDimensions={{width: 100, height: 100}}
        imagesMaxWidth={Dimensions.get('window').width / 1.3}
        onParsed={(dom, RNElements) => {
          console.log('dom: ', dom);
          console.log('RNElements: ', RNElements);
          descriptionLength(RNElements.length);
          return RNElements.splice(0, 1);
        }}
      />
    </>
  );
};

const Wrapper = compose(withApollo)(ElepsisDescription);

export default props => <Wrapper {...props} />;
