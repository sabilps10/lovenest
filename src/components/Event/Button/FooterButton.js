import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Footer, FooterTab} from 'native-base';
import RibbonPrice from '../Ribbon/bigRibbonPrice';
import RibbonGift from '../Ribbon/bigRibbonGift';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {white, lightSalmon, mainGreen, headerBorderBottom, greyLine} = Colors;
const {medium} = FontType;

const FooterButton = props => {
  console.log('FooterButton Props: ', props);
  const {detail, showRSVP, freeGift} = props;
  return (
    <Footer>
      <FooterTab
        style={{
          backgroundColor: white,
          width: '100%',
        }}>
        <View
          style={{
            flex: 1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <RibbonPrice isEventDetail={true} data={detail} />
          {freeGift ? <RibbonGift isEventDetail={true} data={detail} /> : null}
        </View>
        <View
          style={{
            flex: 0.4,
            backgroundColor: props?.detail?.isFullBooked
              ? headerBorderBottom
              : mainGreen,
          }}>
          <TouchableOpacity
            disabled={props?.detail?.isFullBooked ? true : false}
            onPress={() => showRSVP()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2),
                color: props?.detail?.isFullBooked ? greyLine : white,
                letterSpacing: 0.3,
              }}>
              RSVP
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(FooterButton);

export default props => <Wrapper {...props} />;
