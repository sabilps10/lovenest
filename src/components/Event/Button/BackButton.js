import React from 'react';
import {TouchableOpacity, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

const {white, black} = Colors;
const AnimatedTouch = Animated.createAnimatedComponent(TouchableOpacity);

const BackButton = props => {
  console.log('BackButton Props: ', props);
  const {buttonPosition, onPress} = props;
  return (
    <AnimatedTouch
      onPress={() => onPress()}
      style={{
        height: 32,
        width: 32,
        borderRadius: 32 / 2,
        position: 'absolute',
        zIndex: 3,
        left: 15,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        transform: [{translateY: buttonPosition}],
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{
          right: 1,
          fontSize: 20,
          color: black,
        }}
      />
    </AnimatedTouch>
  );
};

const Wrapper = compose(withApollo)(BackButton);

export default props => <Wrapper {...props} />;
