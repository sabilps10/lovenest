import React from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  Dimensions,
  Animated,
  Linking,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../Image/AsyncImage';
import {Card, CardItem, Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor, lightSalmon, transparent} = Colors;

let initialIndex = 0;

class FeaturedEventSlider extends React.Component {
  sliderRef = React.createRef();
  timeId = null;
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.stopAutoPlay();
    if (this.props.data) {
      this.startAutoPlay();
    }
  }

  componentWillUnmount() {
    this.stopAutoPlay();
  }

  startAutoPlay = () => {
    if (this.props.timer) {
      this.timerId = setInterval(() => {
        this.goNextIndex();
      }, this.props.timer);
    }
  };

  stopAutoPlay = () => {
    if (this.timerId) {
      clearInterval();
      this.timerId = null;
      initialIndex = 0;
    }
  };

  goNextIndex = () => {
    if (this.sliderRef) {
      if (this.sliderRef.current) {
        if (this.sliderRef.current.scrollToIndex) {
          if (initialIndex >= this.props.data.length - 1) {
            initialIndex = 0;
            this.sliderRef.current.scrollToIndex({
              animated: true,
              index: initialIndex,
            });
          } else {
            this.sliderRef.current.scrollToIndex({
              animated: true,
              index: (initialIndex += 1),
            });
          }
        }
      }
    }
  };

  openLink = link => {
    Linking.canOpenURL(link).then(supported => {
      if (supported) {
        Linking.openURL(link);
      } else {
        console.log(`Don't know how to open URI: ${link}`);
      }
    });
  };

  render() {
    console.log('Slider Banner Props: ', this.props);
    const {data, navigation} = this.props;
    const {scrollX} = this.state;
    if (data === undefined || data === null) {
      return null;
    } else {
      return (
        <>
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: this.state.scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            ref={this.sliderRef}
            data={this.props.data}
            initialScrollIndex={0}
            onScrollToIndexFailed={info => {
              const wait = new Promise(resolve => setTimeout(resolve, 500));
              wait.then(() => {
                this.sliderRef.current?.scrollToIndex({
                  index: 0,
                  animated: true,
                });
              });
            }}
            showsHorizontalScrollIndicator={false}
            horizontal
            scrollEnabled
            pagingEnabled
            keyExtractor={(_, index) => `${String(index)} Sliders`}
            renderItem={({item, _}) => {
              const logoSource =
                item.eventBannerDynamicURL === null
                  ? {uri: item.eventBannerURL}
                  : {uri: `${item.eventBannerDynamicURL}=h500`};
              const {id} = item;
              return (
                <Card
                  transparent
                  style={{
                    ...this.props.containerStyle,
                    marginTop: 0,
                    marginLeft: 0,
                    marginRight: 0,
                  }}>
                  <CardItem
                    button
                    onPress={() => {
                      navigation.navigate('EventDetail', {id});
                    }}
                    cardBody
                    style={{backgroundColor: 'transparent'}}>
                    <AsyncImage
                      resizeMode={'cover'}
                      style={{
                        ...this.props.imageStyle,
                      }}
                      loaderStyle={{
                        ...this.props.imageLoaderStyle,
                      }}
                      source={logoSource}
                      placeholderColor={greyLine}
                    />
                  </CardItem>
                </Card>
              );
            }}
          />
          {this.props.showIndicator ? (
            <Card transparent style={{marginBottom: 0}}>
              <CardItem style={{backgroundColor: transparent}}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  {this.props.data.map((_, idx) => {
                    let color = Animated.divide(
                      scrollX,
                      Dimensions.get('window').width,
                    ).interpolate({
                      inputRange: [idx - 1, idx, idx + 1],
                      outputRange: [greyLine, lightSalmon, greyLine],
                      extrapolate: 'clamp',
                    });
                    return (
                      <>
                        {this.props.data.length > 7 ? (
                          <Animated.View
                            key={String(idx)}
                            style={{
                              height: 7,
                              width: 7,
                              backgroundColor: color,
                              margin: 3,
                              borderRadius: 5,
                            }}
                          />
                        ) : (
                          <Animated.View
                            key={String(idx)}
                            style={{
                              height: 7,
                              width: 7,
                              backgroundColor: color,
                              margin: 4,
                              borderRadius: 5,
                            }}
                          />
                        )}
                      </>
                    );
                  })}
                </View>
              </CardItem>
            </Card>
          ) : null}
        </>
      );
    }
  }
}

const Wrapper = compose(withApollo)(FeaturedEventSlider);

export default props => <Wrapper {...props} />;
