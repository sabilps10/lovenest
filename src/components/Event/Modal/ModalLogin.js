import React, {useState} from 'react';
import {
  Text,
  Image,
  View,
  Modal,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Button,
  Icon,
  Footer,
  FooterTab,
  CardItem,
  Card,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import {CommonActions} from '@react-navigation/native';
import {charImage} from '../../../utils/Themes/Images';

const {charNoEvent} = charImage;
const {overlayDim, white, black, greyLine, lightSalmon, mainGreen} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

const ModalLogin = props => {
  const {status, closeModal, navigation} = props;
  return (
    <>
      <Modal
        transparent
        statusBarTranslucent
        visible={status}
        animated
        animationType="fade">
        <View style={{flex: 1, backgroundColor: overlayDim}}>
          <TouchableOpacity
            onPress={() => closeModal()}
            style={{flex: 1, backgroundColor: 'transparent'}}
          />
          <View style={{width: '100%', backgroundColor: 'white'}}>
            <Card
              style={{
                zIndex: 99,
                marginLeft: 0,
                marginRight: 0,
                bottom: 20,
                borderTopRightRadius: 25,
                borderTopLeftRadius: 25,
              }}>
              <CardItem>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      borderRadius: 25,
                      backgroundColor: mainGreen,
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: 'white',
                        letterSpacing: 0.3,
                        lineHeight: 18,
                      }}>
                      Please Login or Register to RSVP
                    </Text>
                  </View>
                </View>
                <View style={{flex: 0.5}}>
                  <Button
                    onPress={() => closeModal()}
                    transparent
                    style={{
                      alignSelf: 'flex-end',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="x"
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 24,
                        color: black,
                      }}
                    />
                  </Button>
                </View>
              </CardItem>
            </Card>
            <Card transparent>
              <CardItem cardBody style={{width: '100%', paddingTop: 0}}>
                <Image
                  source={charNoEvent}
                  style={{width: '100%', height: height / 3.5}}
                  resizeMethod="resize"
                  resizeMode="contain"
                />
              </CardItem>
              <CardItem>
                <Button
                  onPress={() => {
                    try {
                      closeModal();
                      navigation.navigate('Login', {showXIcon: true});
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                  style={{
                    borderRadius: 4,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: mainGreen,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: white,
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    Login
                  </Text>
                </Button>
              </CardItem>
            </Card>
          </View>
        </View>

        {/* <Container style={{backgroundColor: 'transparent'}}>
          <View style={{flex: 1}}>
            <TouchableOpacity
              onPress={() => closeModal()}
              style={{flex: 3, backgroundColor: overlayDim}}
            />
            <View
              style={{
                flex: 1,
                backgroundColor: white,
                flexDirection: 'column',
              }}>
              <View
                style={{
                  width: '100%',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                <TouchableOpacity
                  onPress={() => closeModal()}
                  style={{
                    left: 5,
                    width: 45,
                    height: 45,
                    borderRadius: 45 / 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{fontSize: RFPercentage(2.5), color: black}}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  top: 30,
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                <Text
                  numberOfLines={2}
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(2),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Please login or register to RSVP
                </Text>
              </View>
            </View>
          </View>
          <Footer
            style={{
              borderWidth: 0,
              borderColor: white,
              backgroundColor: white,
              elevation: 0,
              shadowOpacity: 0,
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <FooterTab style={{backgroundColor: white}}>
              <Button
                onPress={() => {
                  try {
                    closeModal();
                    navigation.navigate('Login', {showXIcon: true})
                    // navigation.dispatch(
                    //   CommonActions.reset({
                    //     routes: [
                    //       {
                    //         name: 'Login',
                    //         params: {showXIcon: true},
                    //       },
                    //     ],
                    //   }),
                    // );
                  } catch (error) {
                    console.log('Error: ', error);
                  }
                }}
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  marginBottom: 10,
                  width: '100%',
                  height: 48,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: mainGreen,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                  }}>
                  Login
                </Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container> */}
      </Modal>
    </>
  );
};

const Wrapper = compose(withApollo)(ModalLogin);

export default props => <Wrapper {...props} />;
