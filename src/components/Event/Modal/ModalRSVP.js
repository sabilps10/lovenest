import React, {useState, useCallback, useEffect} from 'react';
import {
  Text,
  View,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button, Icon, CardItem, Card} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import REGISTER_EVENT_MUTATION from '../../../graphql/mutations/registerEvent';

const {
  overlayDim,
  white,
  black,
  greyLine,
  mainGreen,
  mainRed,
  headerBorderBottom,
} = Colors;
const {medium, book} = FontType;

const ModalRSVP = props => {
  console.log('Modal RSVP: ', props);
  const {status, closeModal, eventName, eventId, client, navigation} = props;

  const [count, setCount] = useState(1);
  const [isLoadingRSVP, setIsLoadingRSVP] = useState(false);
  const [isErrorRSVP, setIsErrorRSVP] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [listTime, setListTime] = useState([]);

  useEffect(() => {
    fetch();
  }, []);

  const selectedTimeSlot = async index => {
    try {
      const manipulated = await Promise.all(
        listTime?.map((d, i) => {
          if (index === i) {
            return {
              ...d,
              isSelected: true,
            };
          } else {
            return {
              ...d,
              isSelected: false,
            };
          }
        }),
      );

      if (manipulated?.length === listTime?.length) {
        await setListTime([...manipulated]);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetch = async () => {
    try {
      if (props?.data?.timeslot) {
        await setListTime([...props?.data?.timeslot]);
        await setIsLoading(false);
      } else {
        await setIsLoading(false);
      }
    } catch (error) {
      await setIsLoading(false);
    }
  };

  const checkingSelectedTimeSlot = list => {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await Promise.all(
          list
            ?.map((d, i) => {
              if (d?.isSelected) {
                return {
                  id: d?.id,
                };
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );

        if (result) {
          resolve(result);
        } else {
          resolve([]);
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  const onRSVP = async () => {
    try {
      await setIsLoadingRSVP(true);
      await setIsErrorRSVP(false);

      const checking = await checkingSelectedTimeSlot(listTime);
      console.log('checking >>>>> ', checking);

      if (listTime?.length === 0) {
        // for event without time slot
        await client
          .mutate({
            mutation: REGISTER_EVENT_MUTATION,
            variables: {
              eventId: parseInt(eventId, 10),
              quantity: parseInt(count, 10),
              // timeslotId: checking[0]?.id,
            },
          })
          .then(async response => {
            console.log('Response RSVP: ', response);
            const {data, errors} = response;
            const {registerEvent} = data;
            const {data: registerDetail, error} = registerEvent;

            if (errors === undefined) {
              if (error) {
                await setIsErrorRSVP(true);
                await setIsLoadingRSVP(false);

                await setTimeout(async () => {
                  await setIsErrorRSVP(false);
                  await setIsLoadingRSVP(false);
                }, 1000);
              } else {
                const {id} = registerDetail;
                if (id) {
                  await setIsErrorRSVP(false);
                  await setIsLoadingRSVP(false);
                  await closeModal();
                  navigation.navigate('OrderDetail', {
                    id,
                    successRegisterEvent: true,
                  });
                } else {
                  await setIsErrorRSVP(true);
                  await setIsLoadingRSVP(false);
                  await setTimeout(async () => {
                    await setIsErrorRSVP(false);
                    await setIsLoadingRSVP(false);
                  }, 1000);
                }
              }
            } else {
              await setIsErrorRSVP(true);
              await setIsLoadingRSVP(false);
              await setTimeout(async () => {
                await setIsErrorRSVP(false);
                await setIsLoadingRSVP(false);
              }, 1000);
            }
          })
          .catch(error => {
            throw new Error(error);
          });
      } else {
        // Event has time slot
        if (checking?.length > 0) {
          console.log('WUCAUUU TIME SLOT EVENT RSVP: ', {
            eventId: parseInt(eventId, 10),
            quantity: parseInt(count, 10),
            timeslotId: checking[0]?.id,
          });
          await client
            .mutate({
              mutation: REGISTER_EVENT_MUTATION,
              variables: {
                eventId: parseInt(eventId, 10),
                quantity: parseInt(count, 10),
                timeslotId: checking[0]?.id,
              },
            })
            .then(async response => {
              console.log('Response RSVP: ', response);
              const {data, errors} = response;
              const {registerEvent} = data;
              const {data: registerDetail, error} = registerEvent;

              if (errors === undefined) {
                if (error) {
                  await setIsErrorRSVP(true);
                  await setIsLoadingRSVP(false);

                  await setTimeout(async () => {
                    await setIsErrorRSVP(false);
                    await setIsLoadingRSVP(false);
                  }, 1000);
                } else {
                  const {id} = registerDetail;
                  if (id) {
                    await setIsErrorRSVP(false);
                    await setIsLoadingRSVP(false);
                    await closeModal();
                    navigation.navigate('OrderDetail', {
                      id,
                      successRegisterEvent: true,
                    });
                  } else {
                    await setIsErrorRSVP(true);
                    await setIsLoadingRSVP(false);
                    await setTimeout(async () => {
                      await setIsErrorRSVP(false);
                      await setIsLoadingRSVP(false);
                    }, 1000);
                  }
                }
              } else {
                await setIsErrorRSVP(true);
                await setIsLoadingRSVP(false);
                await setTimeout(async () => {
                  await setIsErrorRSVP(false);
                  await setIsLoadingRSVP(false);
                }, 1000);
              }
            })
            .catch(error => {
              throw new Error(error);
            });
        } else {
          await setIsErrorRSVP(true);
          await setIsLoadingRSVP(false);

          await setTimeout(async () => {
            await setIsErrorRSVP(false);
            await setIsLoadingRSVP(false);
          }, 1000);
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorRSVP(true);
      await setIsLoadingRSVP(false);
      await setTimeout(async () => {
        await setIsErrorRSVP(false);
        await setIsLoadingRSVP(false);
      }, 1000);
    }
  };

  return (
    <>
      <Modal
        transparent
        statusBarTranslucent
        visible={status}
        animated
        animationType="fade">
        <View style={{flex: 1, backgroundColor: overlayDim}}>
          <TouchableOpacity
            onPress={() => closeModal()}
            style={{flex: 1, backgroundColor: 'transparent'}}
          />
          <View
            style={{
              width: '100%',
              backgroundColor: 'white',
              borderTopRightRadius: 25,
              borderTopLeftRadius: 25,
            }}>
            <HeaderBar closeModal={() => closeModal()} />
            <Card
              transparent
              style={{
                marginLeft: 0,
                marginRight: 0,
              }}>
              {isErrorRSVP ? (
                <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                  <CardItem>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: mainRed,
                        fontSize: RFPercentage(1.5),
                        letterSpacing: 0.3,
                      }}>
                      Failed To RSVP!
                    </Text>
                  </CardItem>
                </Card>
              ) : null}
              <EventName eventName={eventName} />
              <NumberAttendees />
              <TicketButton
                count={count}
                increase={() => {
                  setCount(count + 1);
                }}
                decrease={() => {
                  if (count !== 1) {
                    setCount(count - 1);
                  }
                }}
              />
              <TimeSlot
                isLoading={isLoading}
                data={listTime}
                selectedTimeSlot={index => selectedTimeSlot(index)}
              />
              <ButtonRSVP isLoadingRSVP={isLoadingRSVP} onRSVP={onRSVP} />
            </Card>
          </View>
        </View>
      </Modal>
    </>
  );
};

export const TimeSlot = props => {
  console.log('TimeSlot Props: >>> ', props);
  const [isExpand, setIsExpand] = useState(false);

  const toggle = async () => {
    try {
      await setIsExpand(!isExpand);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = useCallback((item, index) => {
    return `${item.id}`;
  }, []);

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity
          onPress={() => props?.selectedTimeSlot(index)}
          style={{
            padding: 1,
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: headerBorderBottom,
          }}>
          <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </View>
          {item?.isSelected ? (
            <Icon
              type="Feather"
              name="check"
              style={{left: 12, fontSize: 25, color: 'green'}}
            />
          ) : null}
        </TouchableOpacity>
      );
    },
    [props?.data],
  );

  if (props?.isLoading) {
    return null;
  } else {
    if (props?.data?.length === 0) {
      return null;
    } else {
      return (
        <Card
          transparent
          style={{
            paddingLeft: 13,
            paddingRight: 13,
            marginLeft: 0,
            marginRight: 0,
          }}>
          <CardItem
            style={{
              width: '100%',
              backgroundColor: 'transparent',
              paddingTop: 0,
            }}>
            <Text
              style={{
                right: 10,
                fontFamily: book,
                fontSize: RFPercentage(1.6),
                color: greyLine,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              Time Slot :
            </Text>
          </CardItem>
          <CardItem
            style={{
              width: '100%',
              borderWidth: 1,
              borderColor: greyLine,
              borderRadius: 4,
            }}>
            <View style={{width: '100%'}}>
              <TouchableOpacity
                onPress={toggle}
                style={{
                  flexDirection: 'row',
                  width: '100%',
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.6),
                      color: black,
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    Select Time Slot
                  </Text>
                </View>
                <View
                  style={{
                    left: 5,
                    width: 35,
                    height: 35,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name={isExpand ? 'chevron-up' : 'chevron-down'}
                    style={{left: 5, ontSize: 25, color: 'grey'}}
                  />
                </View>
              </TouchableOpacity>
              {isExpand ? (
                <FlatList
                  ListHeaderComponent={() => {
                    return (
                      <View
                        style={{
                          marginTop: 10,
                          marginBottom: 10,
                          width: '100%',
                          height: 1,
                          borderBottomColor: headerBorderBottom,
                          borderBottomWidth: 1,
                        }}
                      />
                    );
                  }}
                  data={props?.data}
                  extraData={props?.data}
                  keyExtractor={keyExt}
                  renderItem={renderItem}
                />
              ) : null}
            </View>
          </CardItem>
        </Card>
      );
    }
  }
};

export const ButtonRSVP = props => {
  const {isLoadingRSVP, onRSVP} = props;

  return (
    <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
      <Button
        onPress={() => {
          onRSVP();
        }}
        style={{
          width: '100%',
          borderRadius: 4,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: mainGreen,
          flexDirection: 'row',
        }}>
        {isLoadingRSVP ? (
          <ActivityIndicator
            size="large"
            animating={isLoadingRSVP}
            color={white}
            style={{marginRight: 10}}
          />
        ) : null}
        <Text
          style={{
            fontFamily: medium,
            letterSpacing: 0.3,
            lineHeight: 18,
            fontSize: RFPercentage(1.8),
            color: white,
          }}>
          RSVP
        </Text>
      </Button>
    </CardItem>
  );
};

export const TicketButton = props => {
  const {count, increase, decrease} = props;

  return (
    <CardItem style={{width: '100%'}}>
      <View
        style={{
          wisth: '100%',
          borderWidth: 1,
          borderColor: greyLine,
          borderRadius: 4,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{flex: 1}}>
          <Button
            onPress={() => decrease()}
            transparent
            style={{
              alignSelf: 'flex-start',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="minus"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.5),
              color: black,
            }}>
            {count}
          </Text>
        </View>
        <View style={{flex: 1}}>
          <Button
            onPress={() => increase()}
            transparent
            style={{
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="plus"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </View>
      </View>
    </CardItem>
  );
};

export const NumberAttendees = props => {
  return (
    <CardItem
      style={{
        width: '100%',
        backgroundColor: 'transparent',
        paddingTop: 0,
      }}>
      <Text
        style={{
          top: 5,
          fontFamily: book,
          fontSize: RFPercentage(1.6),
          color: greyLine,
          letterSpacing: 0.3,
          lineHeight: 18,
        }}>
        Number of attendees :
      </Text>
    </CardItem>
  );
};

export const EventName = props => {
  const {eventName} = props;

  return (
    <CardItem
      style={{
        width: '100%',
        backgroundColor: 'transparent',
        paddingTop: 0,
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(2),
          color: black,
          letterSpacing: 0.3,
          lineHeight: 18,
        }}>
        {eventName}
      </Text>
    </CardItem>
  );
};

export const HeaderBar = props => {
  const {closeModal} = props;

  return (
    <Card
      style={{
        zIndex: 99,
        marginLeft: 0,
        marginRight: 0,
        bottom: 20,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
      }}>
      <CardItem>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              borderRadius: 25,
              backgroundColor: mainGreen,
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 5,
              paddingBottom: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: 'white',
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              Book The Event
            </Text>
          </View>
        </View>
        <View style={{flex: 0.5}}>
          <Button
            onPress={() => closeModal()}
            transparent
            style={{
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </View>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(ModalRSVP);

export default props => <Wrapper {...props} />;
