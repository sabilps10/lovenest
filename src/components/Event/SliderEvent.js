import React from 'react';
import {Text, FlatList, Dimensions, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import CardEvent from './Card/CardEvent';
import BigCardEvent from './Card/BigCardEvent';
import {Card, CardItem, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {transparent, mainRed} = Colors;
const {medium} = FontType;

const SliderEvent = props => {
  console.log('SliderEvent Props: ', props);
  const {data, navigation, client} = props;
  if (data) {
    return (
      <>
        {data.length === 0 ? null : (
          <Card transparent style={{marginTop: 0}}>
            <CardItem
              style={{
                backgroundColor: transparent,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2.2),
                  color: 'black',
                  letterSpacing: 0.3,
                }}>
                Upcoming Events
              </Text>
              {data.length === 0 || data.length === 1 ? null : (
                <TouchableOpacity
                  style={{left: 25}}
                  onPress={() => {
                    navigation.navigate('UpcomingEvent');
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.9),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      See all
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{
                        marginLeft: 5,
                        fontSize: RFPercentage(2),
                        color: mainRed,
                      }}
                    />
                  </View>
                </TouchableOpacity>
              )}
            </CardItem>
          </Card>
        )}
        {data.length === 0 ? null : data.length === 1 ? (
          <View
            style={{
              width: '100%',
              paddingLeft: 15,
              paddingRight: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <BigCardEvent
              data={data[0]}
              navigation={navigation}
              client={client}
            />
          </View>
        ) : (
          <FlatList
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              paddingLeft: 15,
              paddingRight: 5,
              marginBottom: 10,
            }}
            snapToInterval={Dimensions.get('window').width / 1.5}
            pagingEnabled
            horizontal={true}
            scrollEnabled
            data={data}
            keyExtractor={(_, index) => String(index) + 'Container'}
            renderItem={({item}) => {
              return (
                <CardEvent
                  data={item}
                  navigation={navigation}
                  client={client}
                />
              );
            }}
          />
        )}
      </>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(SliderEvent);

export default props => <Wrapper {...props} />;
