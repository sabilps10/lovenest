import React from 'react';
import {Text, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts';
import {hasNotch} from 'react-native-device-info';

const {medium} = FontType;
const {greyLine, black} = Colors;

const RibbonPrice = props => {
  const {data} = props;
  const {promoPrice, retailPrice} = data;
  return (
    <>
      <View style={{flexDirection: 'row', marginRight: 5}}>
        <View
          style={{
            minHeight: 20,
            maxHeight: 20,
            borderWidth: 1,
            borderColor: greyLine,
            borderRightWidth: 0,
            flexDirection: 'row',
            flexWrap: 'wrap',
            paddingLeft: 2,
            paddingRight: 15,
          }}>
          <View
            style={{
              flexWrap: 'wrap',
              maxWidth: 15,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 1,
            }}>
            <Icon
              type="Feather"
              name="tag"
              style={{top: 0.5, fontSize: RFPercentage(1.5), color: black}}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {promoPrice === 0 ? (
              <Text
                style={{
                  left: Platform.OS === 'android' ? 0.9 : hasNotch() ? 2 : 1,
                  top:
                    Platform.OS === 'android'
                      ? hasNotch()
                        ? -0.5
                        : 1
                      : hasNotch()
                      ? 1
                      : 1.9,
                  fontFamily: medium,
                  color: '#D47271',
                  fontSize: RFPercentage(1.15),
                  fontWeight: 'bold',
                }}>
                FREE
              </Text>
            ) : (
              <Text style={{top: 1}}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.15),
                    textDecorationLine: 'line-through',
                  }}>
                  ${retailPrice}
                </Text>
                <Text
                  style={{
                    fontFamily: medium,
                    color: '#D47271',
                    fontSize: RFPercentage(1.15),
                    fontWeight: 'bold',
                  }}>
                  {' FREE'}
                </Text>
              </Text>
            )}
          </View>
          <View
            style={{
              top: 2,
              position: 'absolute',
              right: -6,
              borderLeftWidth: 1,
              borderLeftColor: greyLine,
              borderBottomWidth: 1,
              borderBottomColor: greyLine,
              transform: [{rotate: '45deg'}],
              minHeight: 13,
              maxHeight: 13,
              minWidth: 13,
            }}
          />
        </View>
      </View>
    </>
  );
};

const Wrapper = compose(withApollo)(RibbonPrice);

export default props => <Wrapper {...props} />;
