import React from 'react';
import {Text, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts';
import {hasNotch} from 'react-native-device-info';
const {medium} = FontType;
const {greyLine, black} = Colors;

const BigRibbonPrice = props => {
  const {data, isEventDetail} = props;
  const {promoPrice, retailPrice} = data;
  return (
    <>
      <View style={{flexDirection: 'row', marginRight: 10}}>
        <View
          style={{
            minHeight: 25,
            maxHeight: 25,
            borderWidth: 1,
            borderColor: greyLine,
            borderRightWidth: 0,
            flexDirection: 'row',
            paddingLeft: 10,
            paddingRight: 15,
          }}>
          <View
            style={{
              left: -5,
              flexWrap: 'wrap',
              maxWidth: 20,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 1,
            }}>
            <Icon
              type="Feather"
              name="tag"
              style={{top: 1, fontSize: RFPercentage(2), color: black}}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {promoPrice === 0 ? (
              <Text
                style={
                  isEventDetail
                    ? {
                        marginLeft: 5,
                        left: -5,
                        top: -0.5,
                        color: '#D47271',
                        fontSize: RFPercentage(1.6),
                        fontWeight: 'bold',
                        fontFamily: medium,
                      }
                    : {
                        left: -5,
                        top: -0.5,
                        color: '#D47271',
                        fontSize: RFPercentage(1.6),
                        fontWeight: 'bold',
                        fontFamily: medium,
                      }
                }>
                FREE
              </Text>
            ) : (
              <Text style={{left: isEventDetail ? -2.5 : -3.5, top: -0.5}}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.6),
                    textDecorationLine: 'line-through',
                  }}>
                  ${retailPrice}
                </Text>
                <Text
                  style={{
                    fontFamily: medium,
                    color: '#D47271',
                    fontSize: RFPercentage(1.6),
                    fontWeight: 'bold',
                  }}>
                  {' FREE'}
                </Text>
              </Text>
            )}
          </View>
          <View
            style={{
              top: 3.5,
              position: 'absolute',
              right: -6,
              borderLeftWidth: 1,
              borderLeftColor: greyLine,
              borderBottomWidth: 1,
              borderBottomColor: greyLine,
              transform: [{rotate: '45 deg'}],
              minHeight: 16,
              maxHeight: 16,
              minWidth: 16,
            }}
          />
        </View>
      </View>
    </>
  );
};

const Wrapper = compose(withApollo)(BigRibbonPrice);

export default props => <Wrapper {...props} />;
