import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts/';

const {medium} = FontType;
const {greyLine} = Colors;

const RibbonGift = () => {
  return (
    <>
      <View style={{flexDirection: 'row'}}>
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: greyLine,
            borderLeftWidth: 0,
            minHeight: 20,
            maxHeight: 20,
            alignItems: 'center',
            backgroundColor: '#F9D5CC',
            paddingRight: 2,
          }}>
          <View
            style={{
              position: 'absolute',
              left: -7,
              borderLeftWidth: 1,
              borderLeftColor: greyLine,
              borderBottomWidth: 1,
              borderBottomColor: greyLine,
              transform: [{rotate: '45deg'}],
              minHeight: 13.5,
              maxHeight: 13.5,
              minWidth: 13.5,
              backgroundColor: '#F9D5CC',
            }}
          />
          <View
            style={{
              left: -1,
              bottom: 0.5,
              borderWidth: 0,
              flexWrap: 'wrap',
              maxWidth: 20,
              paddingRight: 1.5,
            }}>
            <Icon
              type="MaterialIcons"
              name="star-border"
              style={{fontSize: RFPercentage(1.8)}}
            />
          </View>
          <View style={{paddingTop: 0.5}}>
            <Text
              style={{
                fontFamily: medium,
                color: 'black',
                bottom: 0.2,
                left: -4.5,
                fontSize: RFPercentage(1.15),
              }}>
              GIFT AVAILABLE
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const Wrapper = compose(withApollo)(RibbonGift);

export default props => <Wrapper {...props} />;
