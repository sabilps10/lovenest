import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts';

const {medium} = FontType;

const {greyLine} = Colors;

const BigRibbonGift = props => {
  const {isEventDetail} = props;
  return (
    <>
      <View style={{flexDirection: 'row'}}>
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: greyLine,
            borderLeftWidth: 0,
            minHeight: 25,
            maxHeight: 25,
            alignItems: 'center',
            backgroundColor: '#F9D5CC',
            paddingRight: 2,
          }}>
          <View
            style={{
              position: 'absolute',
              left: -9,
              borderLeftWidth: 1,
              borderLeftColor: greyLine,
              borderBottomWidth: 1,
              borderBottomColor: greyLine,
              transform: [{rotate: '45deg'}],
              minHeight: 17,
              maxHeight: 17,
              minWidth: 17,
              backgroundColor: '#F9D5CC',
            }}
          />
          <View
            style={
              isEventDetail
                ? {
                    left: -5,
                    bottom: 0.5,
                    borderWidth: 0,
                    flexWrap: 'wrap',
                    maxWidth: 25,
                    paddingRight: 1.5,
                  }
                : {
                    left: -1,
                    bottom: 0.5,
                    borderWidth: 0,
                    flexWrap: 'wrap',
                    maxWidth: 25,
                    paddingRight: 1.5,
                  }
            }>
            <Icon
              type="MaterialIcons"
              name="star-border"
              style={{fontSize: RFPercentage(2)}}
            />
          </View>
          <View style={{paddingTop: 0.5}}>
            <Text
              style={{
                fontFamily: medium,
                color: 'black',
                bottom: 0.2,
                left: isEventDetail ? -5 : -8.5,
                fontSize: RFPercentage(1.6),
              }}>
              GIFT AVAILABLE
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const Wrapper = compose(withApollo)(BigRibbonGift);

export default props => <Wrapper {...props} />;
