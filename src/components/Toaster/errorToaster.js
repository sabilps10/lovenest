import React, {useState, useEffect} from 'react';
import {Animated, View, Text} from 'react-native';
import {commonImage} from '../../utils/Themes/Images';

const {toasterGearFailed} = commonImage;

const ErrorToaster = props => {
  const {text} = props;
  const [opacity] = useState(new Animated.Value(0));
  let [toasterText, setToasterText] = useState('');

  useEffect(() => {
    loadFunc();
    updateText();
  });

  const updateText = () => {
    if (text) {
      toasterText = text;
      setToasterText(toasterText);
    }
  };

  const loadFunc = () => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true,
      }),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 3000,
        useNativeDriver: true,
      }),
    ]).start();
  };

  return (
    <View
      style={{
        position: 'absolute',
        top: 10,
        width: '100%',
        zIndex: 99999,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Animated.View
        onLoad={loadFunc}
        style={{
          paddingTop: 6,
          paddingLeft: 20,
          paddingRigt: 20,
          flexWrap: 'wrap',
          opacity: opacity,
          backgroundColor: 'rgb(84,84,84)',
          minHeight: 32,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          flexDirection: 'row',
          transform: [
            {
              scale: opacity.interpolate({
                inputRange: [0, 1],
                outputRange: [0.85, 1],
              }),
            },
          ],
        }}>
        <Animated.Image
          onLoad={loadFunc}
          source={toasterGearFailed}
          style={{
            width: 18,
            height: 18,
            left: -10,
            opacity: opacity,
            transform: [
              {
                scale: opacity.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0.85, 1],
                }),
              },
            ],
          }}
        />
        <Text
          style={{
            fontFamily: 'Gotham-Medium',
            fontSize: 11,
            color: '#ffffff',
            right: 5,
          }}>
          {text}
        </Text>
      </Animated.View>
    </View>
  );
};

export default ErrorToaster;
