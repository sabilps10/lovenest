import React, {useState, useEffect} from 'react';
import {Text, Image, StyleSheet, Dimensions} from 'react-native';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../utils/Themes/Images';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const {charInCompleteAppointment} = charImage;
const {regular, extraLarge} = FontSize;
const {medium, book} = FontType;
const {black} = Colors;
const {width, height} = Dimensions.get('window');

/**
 *
 * @param {titleText, subText} props
 */

const NoTransactions = props => {
  const {titleText, subText} = props;

  let [title, setTitle] = useState('');
  let [subTitle, setStubTitle] = useState('');

  useEffect(() => {
    settingTitle(titleText);
    settingSubTitle(subText);
  });

  const settingTitle = value => {
    title = value;
    setTitle(title);
  };

  const settingSubTitle = value => {
    subTitle = value;
    setStubTitle(subTitle);
  };

  return (
    <Card
      transparent
      style={{
        elevation: 0,
        shadowOpacity: 0,
        marginLeft: 0,
        marginRight: 0,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 1.1,
        height: height / 1.6,
      }}>
      <CardItem>
        <Image
          source={charInCompleteAppointment}
          style={{
            // flex: 1,
            width: width / 1.1,
            height: height / 3,
          }}
          resizeMode="contain"
        />
      </CardItem>
      <CardItem style={{flexDirection: 'column', paddingBottom: 10}}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: medium,
            // fontSize: extraLarge,
            fontSize: RFPercentage(2.2),
            color: black,
            lineHeight: 25,
            letterSpacing: 0.4,
            marginBottom: 15,
          }}>
          {title}
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            // fontSize: regular,
            fontSize: RFPercentage(1.8),
            color: black,
            lineHeight: 20,
            letterSpacing: 0.3,
          }}>
          {subTitle}
        </Text>
      </CardItem>

      {/* <CardItem
        style={{
          backgroundColor: 'transparent',
        }}>
        <View style={{width: '100%'}}>
          <Button text="Login" onPressed={() => onPress()} />
        </View>
      </CardItem> */}
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    height: height / 1.3,
  },
  image: {
    // width: Dimensions.get('window').width - 25,
    // height: 232,
    // aspectRatio: (Dimensions.get('window').width - 25) / 232,
    // bottom: 15,
    width: width / 1.1,
    height: height / 3,
  },
  cardItemTitle: {paddingTop: 26, flexDirection: 'column'},
  textTitle: {
    textAlign: 'center',
    marginBottom: 15,
    fontFamily: medium,
    // fontSize: extraLarge,
    fontSize: RFValue(18),
    color: black,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
  textSubTitle: {
    textAlign: 'center',
    fontFamily: book,
    // fontSize: regular,
    fontSize: RFValue(13),
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
  },
});

export default NoTransactions;
