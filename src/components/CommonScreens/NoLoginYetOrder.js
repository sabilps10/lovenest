import React, {useState, useEffect} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../utils/Themes/Images';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import Button from '../Button/onBoardingButton';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const {charOrderbag} = charImage;
const {regular, extraLarge} = FontSize;
const {medium, book} = FontType;
const {black} = Colors;
const {width, height} = Dimensions.get('window');

const NoLoginYetOrder = props => {
  const {titleText, subText, onPress} = props;
  let [title, setTitle] = useState('');
  let [text, setText] = useState('');

  useEffect(() => {
    setTitleAndText(titleText, subText);
  });

  const setTitleAndText = (propsTitle, propsText) => {
    title = propsTitle;
    setTitle(title);
    text = propsText;
    setText(text);
  };

  return (
    <Card
      transparent
      style={{
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        elevation: 0,
        shadowOpacity: 0,
        marginLeft: 0,
        marginRight: 0,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Image
        source={charOrderbag}
        style={{
          // width: Dimensions.get('window').width - 25,
          // height: 232,
          // aspectRatio: (Dimensions.get('window').width - 25) / 232,
          width: width / 1.1,
          height: height / 3,
          bottom: 35,
        }}
      />
      <CardItem style={{flexDirection: 'column', paddingBottom: 10}}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: medium,
            // fontSize: extraLarge,
            fontSize: RFPercentage(2.2),
            color: black,
            lineHeight: 25,
            letterSpacing: 0.4,
            marginBottom: 15,
          }}>
          {title}
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            // fontSize: regular,
            fontSize: RFPercentage(1.8),
            color: black,
            lineHeight: 20,
            letterSpacing: 0.3,
          }}>
          {text}
        </Text>
      </CardItem>

      <CardItem
        style={{
          backgroundColor: 'transparent',
        }}>
        <Button text="Login" onPressed={() => onPress()} />
      </CardItem>
    </Card>
  );
};

export default NoLoginYetOrder;
