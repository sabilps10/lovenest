import React, {useState, useEffect} from 'react';
import {Text, Image, StyleSheet, Dimensions} from 'react-native';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../utils/Themes/Images';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const {charNoPaymentLogin} = charImage;
const {regular, extraLarge} = FontSize;
const {medium, book} = FontType;
const {black} = Colors;
const {width, height} = Dimensions.get('window');

/**
 *
 * @param {titleText, subText} props
 */

const NoTransactionPayment = props => {
  const {titleText, subText} = props;

  let [title, setTitle] = useState('');
  let [subTitle, setStubTitle] = useState('');

  useEffect(() => {
    settingTitle(titleText);
    settingSubTitle(subText);
  }, [title, subTitle]);

  const settingTitle = value => {
    title = value;
    setTitle(title);
  };

  const settingSubTitle = value => {
    subTitle = value;
    setStubTitle(subTitle);
  };

  return (
    <Card transparent style={styles.card}>
      <Image source={charNoPaymentLogin} style={styles.image} />
      <CardItem style={styles.cardItemTitle}>
        <Text style={styles.textTitle}>{title}</Text>
        <Text style={styles.textSubTitle}>{subTitle}</Text>
      </CardItem>
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  image: {
    // width: Dimensions.get('window').width - 35,
    // height: 245,
    // aspectRatio: (Dimensions.get('window').width - 35) / 245,
    // bottom: 15,
    width: width / 1.1,
    height: height / 3,
    bottom: 35,
  },
  cardItemTitle: {flexDirection: 'column', paddingBottom: 10},
  textTitle: {
    marginBottom: 15,
    fontFamily: medium,
    // fontSize: extraLarge,
    fontSize: RFPercentage(2.2),
    color: black,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
  textSubTitle: {
    textAlign: 'center',
    fontFamily: book,
    // fontSize: regular,
    fontSize: RFPercentage(1.8),
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
  },
});

export default NoTransactionPayment;
