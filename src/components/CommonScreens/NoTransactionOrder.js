import React, {useState, useEffect} from 'react';
import {Text, Image, StyleSheet, Dimensions} from 'react-native';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../utils/Themes/Images';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const {charOrderbag} = charImage;
const {regular, extraLarge} = FontSize;
const {medium, book} = FontType;
const {black} = Colors;
const {width, height} = Dimensions.get('window');

/**
 *
 * @param {titleText, subText} props
 */

const NoTransactionOrder = props => {
  const {titleText, subText} = props;

  let [title, setTitle] = useState('');
  let [subTitle, setStubTitle] = useState('');

  useEffect(() => {
    settingTitle(titleText);
    settingSubTitle(subText);
  }, [title, subTitle]);

  const settingTitle = value => {
    title = value;
    setTitle(title);
  };

  const settingSubTitle = value => {
    subTitle = value;
    setStubTitle(subTitle);
  };

  return (
    <Card transparent style={styles.card}>
      <Image source={charOrderbag} style={styles.image} />
      <CardItem style={styles.cardItemTitle}>
        <Text style={styles.textTitle}>{title}</Text>
        <Text style={styles.textSubTitle}>{subTitle}</Text>
      </CardItem>
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  image: {
    // width: Dimensions.get('window').width - 25,
    // height: 232,
    // aspectRatio: (Dimensions.get('window').width - 25) / 232,
    width: width / 1.1,
    height: height / 3,
    bottom: 45,
  },
  cardItemTitle: {paddingTop: 26, flexDirection: 'column'},
  textTitle: {
    marginBottom: 15,
    fontFamily: medium,
    // fontSize: extraLarge,
    fontSize: RFPercentage(2.2),
    color: black,
    lineHeight: 25,
    letterSpacing: 0.4,
    bottom: 25,
  },
  textSubTitle: {
    textAlign: 'center',
    fontFamily: book,
    // fontSize: regular,
    fontSize: RFPercentage(1.8),
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
    bottom: 25,
  },
});

export default NoTransactionOrder;
