import React, {useState, useEffect} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../utils/Themes/Images';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import Button from '../Button/onBoardingButton';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

const {charInCompleteAppointment} = charImage;
const {regular, extraLarge} = FontSize;
const {medium, book} = FontType;
const {black} = Colors;
const {height, width} = Dimensions.get('window');

const NoLoginYet = props => {
  const {titleText, subText, onPress} = props;
  let [title, setTitle] = useState('');
  let [text, setText] = useState('');

  useEffect(() => {
    setTitleAndText(titleText, subText);
  });

  const setTitleAndText = (propsTitle, propsText) => {
    title = propsTitle;
    setTitle(title);
    text = propsText;
    setText(text);
  };

  return (
    <Card
      transparent
      style={{
        elevation: 0,
        shadowOpacity: 0,
        marginLeft: 0,
        marginRight: 0,
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: height / 1.3,
      }}>
      <CardItem>
        <Image
          source={charInCompleteAppointment}
          style={{
            // flex: 1,
            width: width / 1.1,
            height: height / 3,
          }}
          resizeMode="contain"
        />
      </CardItem>
      <CardItem style={{flexDirection: 'column', paddingBottom: 10}}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: medium,
            // fontSize: extraLarge,
            fontSize: RFPercentage(2.2),
            color: black,
            lineHeight: 25,
            letterSpacing: 0.4,
            marginBottom: 15,
          }}>
          {title}
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            // fontSize: regular,
            fontSize: RFPercentage(1.8),
            color: black,
            lineHeight: 20,
            letterSpacing: 0.3,
          }}>
          {text}
        </Text>
      </CardItem>

      <CardItem
        style={{
          backgroundColor: 'transparent',
        }}>
        <View style={{width: '100%'}}>
          <Button text="Login" onPressed={() => onPress()} />
        </View>
      </CardItem>
    </Card>
  );
};

export default NoLoginYet;
