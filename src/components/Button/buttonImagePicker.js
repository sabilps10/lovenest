import React from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import {Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import Loader from '../Loader/JustLoader';

const {white, black} = Colors;

const buttonImagePicker = props => {
  const {onPress, isLoading} = props;
  return (
    <Button
      onPress={() => {
        onPress();
      }}
      rounded
      activeOpacity={0.7}
      active
      style={{
        ...styles.button,
      }}>
      {isLoading ? (
        <View style={{width: 35, height: 35}}>
          <Loader />
        </View>
      ) : (
        <Icon type="Feather" name="camera" style={styles.icon} />
      )}
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    bottom: 35,
    left: 40,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    backgroundColor: white,
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    left: 1,
    top: Platform.OS === 'android' ? 3 : 1,
    textAlign: 'center',
    width: 25,
    height: 25,
    fontSize: 17,
    color: black,
    padding: 0,
  },
});

export default buttonImagePicker;
