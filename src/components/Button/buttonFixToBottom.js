import React, {useState, useEffect} from 'react';
import {Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Footer, Button, FooterTab} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {lightSalmon, white, mainGreen} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const ButtonFixToBottom = props => {
  const {text, onPress, isLoading, disable} = props;

  let [ButtonText, setText] = useState('');

  useEffect(() => {
    settingText(text);
  });

  const settingText = value => {
    ButtonText = value;
    setText(ButtonText);
  };

  return (
    <Footer
      style={{backgroundColor: 'transparent', elevation: 0, shadowOpacity: 0}}>
      <FooterTab style={{backgroundColor: mainGreen}}>
        {disable ? (
          <TouchableOpacity
            activeOpacity={disable ? 0 : 0.5}
            onPress={() => console.log('masuk ke disable button')}
            style={{
              // backgroundColor: lightSalmon,
              flexDirection: 'row',
              minHeight: 50,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {isLoading ? (
              <ActivityIndicator
                color="white"
                size="large"
                style={{marginRight: 10}}
              />
            ) : null}
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: white,
                lineHeight: 15,
              }}>
              {ButtonText}
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            activeOpacity={disable ? 0 : 0.5}
            onPress={() => {
              console.log('masuk ke enable button');
              onPress();
            }}
            style={{
              // backgroundColor: lightSalmon,
              flexDirection: 'row',
              minHeight: 50,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {isLoading ? (
              <ActivityIndicator
                color="white"
                size="large"
                style={{marginRight: 10}}
              />
            ) : null}
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: white,
                lineHeight: 15,
              }}>
              {ButtonText}
            </Text>
          </TouchableOpacity>
        )}
      </FooterTab>
    </Footer>
  );
};

export default ButtonFixToBottom;
