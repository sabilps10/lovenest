import React from 'react';
import {TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';

const {black} = Colors;

const CircleButton = props => {
  const {navigation, resetProductFilter} = props;
  return (
    <TouchableOpacity
      onPress={async () => {
        await resetProductFilter();
        navigation.goBack(null);
      }}
      style={{
        position: 'absolute',
        top: 43,
        left: 15,
        zIndex: 10,
        backgroundColor: 'white',
        width: 32,
        height: 32,
        borderRadius: 32 / 2,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{fontSize: 20, color: black}}
      />
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(CircleButton);

export default props => <Wrapper {...props} />;
