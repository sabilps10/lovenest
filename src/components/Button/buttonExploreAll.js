import React from 'react';
import {Text, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Button} from 'native-base';

const {lightSalmon, white} = Colors;
const {medium} = FontType;
const AnimatedButton = Animated.createAnimatedComponent(Button);
const AnimatedCard = Animated.createAnimatedComponent(Card);

const ButtonExploreAll = props => {
  console.log('DailyCalendar Props: ', props);
  const {onPress} = props;
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <AnimatedCard
      transparent
      style={{
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        transform: [{scale: scale}],
      }}>
      <CardItem style={{paddingLeft: 1, paddingRight: 1, paddingTop: 5}}>
        <AnimatedButton
          activeOpacity={1}
          onPress={() => onPress()}
          onPressIn={pressIn}
          onPressOut={pressOut}
          style={{
            width: '100%',
            height: 45,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: lightSalmon,
            borderWidth: 1,
            elevation: 0,
            shadowOpacity: 0,
            backgroundColor: white,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: lightSalmon,
              letterSpacing: 0.3,
            }}>
            EXPLORE ALL
          </Text>
        </AnimatedButton>
      </CardItem>
    </AnimatedCard>
  );
};

const Wrapper = compose(withApollo)(ButtonExploreAll);

export default props => <Wrapper {...props} />;
