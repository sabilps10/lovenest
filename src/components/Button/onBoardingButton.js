import React from 'react';
import {Text, ActivityIndicator, StyleSheet} from 'react-native';
import {Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {white, transparent, black, mainGreen} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const onBoardingButton = props => {
  const {text, isLoading, onPressed, transparentType, fullyTransparent} = props;

  return (
    <React.Fragment>
      <Button
        disabled={fullyTransparent ? true : false}
        activeOpacity={0.6}
        style={{
          ...styles.touchButton,
          borderRadius: 2,
          backgroundColor: transparentType ? transparent : mainGreen,
        }}
        onPress={() => onPressed()}>
        {isLoading ? (
          <ActivityIndicator size="large" color={white} />
        ) : (
          <Text
            style={{
              ...styles.text,
              color: fullyTransparent
                ? 'transparent'
                : transparentType
                ? black
                : white,
            }}>
            {text}
          </Text>
        )}
      </Button>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: regular,
    fontFamily: medium,
    lineHeight: 15,
  },
  touchButton: {
    elevation: 0,
    shadowOpacity: 0,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 48,
  },
});

export default onBoardingButton;
