import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {lightSalmon, transparent, mainRed} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const buttonText = props => {
  console.log('Button Text Props: ', props);
  const {onPress, text} = props;
  return (
    <Button
      style={styles.button}
      onPress={() => {
        onPress();
      }}>
      <Text style={styles.text}>{text}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: transparent,
    width: 80,
    elevation: 0,
    shadowOpacity: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontFamily: medium,
    fontSize: regular,
    color: mainRed,
    lineHeight: 15,
  },
});

export default buttonText;