import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';

const {mainRed} = Colors;
const {medium} = FontType;

const Unfilter = props => {
  const {name, parentIndex, checkList, index, showIcon} = props;

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => {
        checkList(parentIndex, index);
      }}
      style={{
        marginBottom: 10,
        marginRight: 10,
        borderRadius: 25,
        backgroundColor: '#FFEDED',
        borderWidth: 1,
        borderColor: mainRed,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
      }}>
      {showIcon ? (
        <Icon
          type="Feather"
          name="users"
          style={{fontSize: RFPercentage(2), color: mainRed}}
        />
      ) : null}
      <Text
        style={
          showIcon
            ? {
                right: 5,
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
                lineHeight: 18,
              }
            : {
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
                lineHeight: 18,
              }
        }>
        {name}
      </Text>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(Unfilter);

export default props => <Wrapper {...props} />;
