import React, {useState, useEffect} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import Loader from '../../components/Loader/JustLoader';

const {transparent} = Colors;

const ButtonSocialMedia = props => {
  const {iconType, onPress} = props;

  let [iconName, setIconName] = useState('');

  useEffect(() => {
    if (iconType !== undefined) {
      setIcon(iconType);
    }
  });

  const setIcon = icon => {
    iconName = icon;
    setIconName(iconName);
  };

  if (iconName === '') {
    return (
      <View style={styles.loaderContainer}>
        <Loader />
      </View>
    );
  }

  return (
    <Button onPress={() => onPress()} style={styles.container}>
      <Image source={iconName} style={styles.image} />
    </Button>
  );
};

const styles = StyleSheet.create({
  loaderContainer: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    elevation: 0,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: transparent,
  },
  image: {width: 32, height: 32},
});

export default ButtonSocialMedia;
