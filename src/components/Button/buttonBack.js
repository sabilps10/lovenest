import React from 'react';
import {StyleSheet} from 'react-native';
import {Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';

const {black, transparent} = Colors;

const ButtonBack = props => {
  const {onPress, iconX} = props;
  return (
    <Button
      style={styles.button}
      onPress={() => {
        onPress();
      }}>
      <Icon
        type="Feather"
        name={iconX ? 'x' : 'chevron-left'}
        style={styles.icon}
      />
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: transparent,
    shadowOpacity: 0,
    elevation: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {fontSize: 25, color: black},
});

export default ButtonBack;
