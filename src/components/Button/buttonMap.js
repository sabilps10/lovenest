import React from 'react';
import {StyleSheet, Platform} from 'react-native';
import {Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';

const {black, white} = Colors;

const ButtonMap = props => {
  const {onPress} = props;

  return (
    <Button onPress={() => onPress()} style={styles.button}>
      <Icon type="Feather" name="navigation" style={styles.icon} />
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    backgroundColor: white,
    position: 'absolute',
    bottom: -10,
    height: 42,
    width: 42,
    borderRadius: 42 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: black,
    top: Platform.OS === 'ios' ? 11 : 14,
    left: Platform.OS === 'ios' ? 11 : 12,
    fontSize: 18,
    width: 42,
    height: 42,
  },
});

export default ButtonMap;
