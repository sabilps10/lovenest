import React from 'react';
import {Text, StatusBar} from 'react-native';
import {Container, Card, CardItem, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {transparent, newContainerColor, lightBlueGrey} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

/**
 *
 * @param {onPress, message} props
 */

const NotLoginYet = props => {
  const {onPress, message} = props;
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <StatusBar barStyle="dark-content" backgroundColor={transparent} translucent={false} />
      <Card
        transparent
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <CardItem
          style={{
            width: '100%',
            backgroundColor: transparent,
            // flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Button
            onPress={() => onPress()}
            style={{
              elevation: 0,
              borderRadius: 0,
              bottom: 30,
              backgroundColor: transparent,
              borderWidth: 1,
              borderColor: lightBlueGrey,
              width: '40%',
              height: 45,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: lightBlueGrey,
              }}>
              {message}
            </Text>
          </Button>
        </CardItem>
      </Card>
    </Container>
  );
};

export default NotLoginYet;
