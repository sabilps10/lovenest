import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import _ from 'lodash';
import counrtyCodeList from '../../../static/international_phone_number';
import Colors from '../../../utils/Themes/Colors';
import {Card, CardItem, Left, Body, Right, Icon, Button} from 'native-base';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';

const {
  overlayDim,
  black,
  veryLightPinkTwo,
  white,
  brownGrey,
  transparent,
} = Colors;
const {extraLarge, regular} = FontSize;
const {medium, book} = FontType;

class countryCodeListModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisibility: false,
      countryCodeList: counrtyCodeList,
      filteredCountryCodeList: [],
      searchText: '',
      isSearching: false,
    };
  }

  getValueOfParentModal = props => {
    const {modalVisibility} = props;
    if (modalVisibility !== undefined) {
      this.setState(prevState => ({
        ...prevState,
        modalVisibility,
        countryCodeList: counrtyCodeList,
        filteredCountryCodeList: [],
        searchText: '',
        isSearching: false,
      }));
    }
  };

  componentDidMount() {
    const {props} = this;
    this.getValueOfParentModal(props);
  }

  componentDidUpdate(prevProps, prevState) {
    const {props} = this;
    const {modalVisibility} = props;
    const {modalVisibility: oldModalVisibility} = prevProps;

    if (oldModalVisibility !== modalVisibility) {
      this.getValueOfParentModal(props);
    }
  }

  closeModal = () => {
    try {
      const {props} = this;
      const {closeModal: closingModal} = props;

      this.setState(
        prevState => ({
          ...prevState,
          modalVisibility: false,
        }),
        () => {
          closingModal();
        },
      );
    } catch (error) {
      console.log('Closing modal Error: ', error);
    }
  };

  search = e => {
    this.setState(
      prevState => ({
        ...prevState,
        isSearching: true,
        searchText: e,
      }),
      () => {
        const {searchText} = this.state;
        this.debouncing(searchText);
      },
    );
  };

  debouncing = _.debounce(searchText => {
    if (searchText === '') {
      const {countryCodeList} = this.state;
      this.setState(prevState => ({
        ...prevState,
        countryCodeList,
        filteredCountryCodeList: countryCodeList,
      }));
    } else {
      const {countryCodeList: countryCodeListState} = this.state;

      const filtered = countryCodeListState.filter((data, index) => {
        return (
          data.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1 ||
          data.dial_code.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
        );
      });

      if (filtered !== undefined) {
        this.setState(
          prevState => ({
            ...prevState,
            filteredCountryCodeList: filtered,
          }),
          () => {
            this.setState(prevState => ({
              ...prevState,
              isSearching: false,
            }));
          },
        );
      }
    }
  }, 1000);

  selectListCountry = data => {
    const {closeModal} = this.props;
    closeModal(data);
  };

  render() {
    const {state} = this;
    const {
      modalVisibility,
      countryCodeList,
      searchText,
      isSearching,
      filteredCountryCodeList,
    } = state;
    return (
      <React.Fragment>
        <StatusBar animated backgroundColor={overlayDim} />
        <Modal animationType="slide" transparent visible={modalVisibility}>
          <View style={styles.container}>
            <View style={styles.container}>
              <TouchableOpacity
                onPress={this.closeModal}
                style={styles.secondSectionModal}
              />
            </View>
            <View style={styles.secondContainer}>
              <Card transparent style={{paddingBottom: 80, paddingTop: 20}}>
                <CardItem>
                  <Text style={styles.title}>Select country code</Text>
                </CardItem>
                <CardItem>
                  <View style={styles.textInputCardItem}>
                    <View style={styles.iconContainer}>
                      <Icon type="Feather" name="search" style={styles.icon} />
                    </View>
                    <View style={{flexBasis: '90%'}}>
                      <TextInput
                        value={searchText}
                        placeholder="Type country code or country name"
                        onChangeText={e => this.search(e)}
                        style={styles.textInputSearch}
                      />
                    </View>
                  </View>
                </CardItem>
                <CardItem>
                  <Text style={styles.subTitle}>All countries</Text>
                </CardItem>
                {isSearching && searchText !== '' ? (
                  <CardItem style={styles.isSearching}>
                    <ActivityIndicator size="large" />
                  </CardItem>
                ) : (
                  <FlatList
                    data={
                      filteredCountryCodeList.length !== 0
                        ? filteredCountryCodeList
                        : countryCodeList
                    }
                    contentContainerStyle={{
                      paddingBottom: 80,
                    }}
                    initialNumToRender={30}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {
                      const {name, flag, dial_code} = item;
                      return (
                        <CardItem style={styles.cardItemFlastlist}>
                          <Button
                            activeOpacity={0.9}
                            onPress={() => this.selectListCountry(item)}
                            style={styles.listItem}>
                            <Left style={styles.left}>
                              <Text style={styles.flag}>{flag}</Text>
                            </Left>
                            <Body style={styles.body}>
                              <Text style={styles.countryName}>{name}</Text>
                            </Body>
                            <Right style={styles.right}>
                              <Text style={styles.dialCode}>{dial_code}</Text>
                            </Right>
                          </Button>
                        </CardItem>
                      );
                    }}
                  />
                )}
              </Card>
            </View>
          </View>
        </Modal>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1},
  secondContainer: {flex: 5, backgroundColor: white},
  title: {
    fontFamily: medium,
    fontSize: extraLarge,
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
  },
  subTitle: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  icon: {left: 5, fontSize: 15, color: brownGrey},
  iconContainer: {
    flexBasis: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  secondSectionModal: {
    backgroundColor: overlayDim,
    height: '100%',
    width: '100%',
  },
  textInputSearch: {
    padding: 3,
    height: 36,
    width: '100%',
  },
  textInputCardItem: {
    backgroundColor: veryLightPinkTwo,
    justifyContent: 'center',
    width: '100%',
    height: 36,
    flexDirection: 'row',
  },
  flag: {
    fontSize: 25,
  },
  countryName: {
    fontFamily: book,
    fontSize: regular,
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
  },
  left: {
    flexBasis: '10%',
  },
  body: {
    flexBasis: '70%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  right: {
    flexBasis: '10%',
  },
  listItem: {
    width: '100%',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: brownGrey,
    paddingBottom: 8,
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: white,
  },
  cardItemFlastlist: {backgroundColor: transparent},
  isSearching: {width: '100%', justifyContent: 'center', alignItems: 'center'},
});

export default countryCodeListModal;
