import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  TextInput,
  Modal,
  Dimensions,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {hasNotch} from 'react-native-device-info';

const {black, white, mainRed, mainGreen, greyLine, overlayDim} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

export const DateAndTimePickupForm = props => {
  const {
    type,
    disabled,
    isError,
    title,
    value,
    onChange,
    placeholder,
    dateFormat,
  } = props;

  const [showIOSDatePicker, setShowIOSDatePicker] = useState(false);
  const [showAndroidDatePicker, setShowAndroidDatePicker] = useState(false);

  const [showIOSTimePicker, setShowIOSTimePicker] = useState(false);
  const [showAndroidTimePicker, setShowAndroidTimePicker] = useState(false);

  const onOpenPicker = async () => {
    try {
      if (Platform.OS === 'ios') {
        if (type === 'Date') {
          await setShowIOSDatePicker(true);
        } else if (type === 'Time') {
          await setShowIOSTimePicker(true);
        } else {
          // Do Nothing
        }
      } else {
        if (type === 'Date') {
          await setShowAndroidDatePicker(true);
        } else if (type === 'Time') {
          await setShowAndroidTimePicker(true);
        } else {
          // Do Nothing
        }
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <>
      <CardItem>
        <View style={{width: '100%'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: mainRed}}>*</Text>
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
            {disabled ? (
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: medium,
                  color: greyLine,
                  fontSize: RFPercentage(1.5),
                  letterSpacing: 0.3,
                  fontStyle: 'italic',
                }}>
                (please select date first!)
              </Text>
            ) : null}
          </View>
          <View style={{marginTop: 5, paddingLeft: 7, paddingRight: 7}}>
            <TouchableOpacity
              disabled={disabled}
              onPress={onOpenPicker}
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <Text
                style={{
                  color: value === '' ? greyLine : black,
                  fontSize: RFPercentage(1.85),
                  letterSpacing: 0.3,
                }}>
                {value === ''
                  ? placeholder
                  : moment(value)
                      .utc()
                      .local()
                      .format(type === 'Date' ? 'ddd, DD MMM YYYY' : 'hh:mm A')}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 10,
              borderBottomColor: isError ? mainRed : greyLine,
              borderBottomWidth: 1,
              width: '100%',
              height: 0.5,
            }}
          />
        </View>
      </CardItem>
      {/* IOS Date Picker */}
      {Platform.OS === 'ios' ? (
        showIOSDatePicker ? (
          <Modal visible={showIOSDatePicker} transparent animationType="fade">
            <DatePickerIOS
              cancel={() => setShowIOSDatePicker(false)}
              pickerMode={type === 'Date' ? 'date' : 'time'}
              theDate={value === '' ? new Date() : value}
              onChange={async e => {
                try {
                  console.log('ANU JANCUK: ', e);
                  await onChange(e);
                  await setShowIOSDatePicker(false);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
            />
          </Modal>
        ) : null
      ) : null}

      {/* IOS Time Picker */}
      {Platform.OS === 'ios' ? (
        showIOSTimePicker ? (
          <Modal visible={showIOSTimePicker} transparent animationType="fade">
            <TimePickerIOS
              cancel={() => setShowIOSTimePicker(false)}
              pickerMode={type === 'Date' ? 'date' : 'time'}
              theDate={value === '' ? new Date() : value}
              onChange={async e => {
                try {
                  console.log('ANU JANCUK: ', e);
                  await onChange(e);
                  await setShowIOSTimePicker(false);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
            />
          </Modal>
        ) : null
      ) : null}

      {/* Android Date Picker */}
      {Platform.OS === 'android' ? (
        showAndroidDatePicker ? (
          <DatePickerAndroid
            cancel={async () => await setShowAndroidDatePicker(false)}
            pickerMode={type === 'Date' ? 'date' : 'time'}
            show={showAndroidDatePicker}
            theDate={value === '' ? new Date() : value}
            onChange={async e => {
              try {
                await setShowAndroidDatePicker(false);
                await onChange(e);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
        ) : null
      ) : null}

      {/* Android Time Picker */}
      {Platform.OS === 'android' ? (
        showAndroidTimePicker ? (
          <DatePickerAndroid
            cancel={async () => await setShowAndroidTimePicker(false)}
            pickerMode={type === 'Date' ? 'date' : 'time'}
            show={showAndroidTimePicker}
            theDate={value === '' ? new Date() : value}
            onChange={async e => {
              try {
                await setShowAndroidTimePicker(false);
                await onChange(e);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
        ) : null
      ) : null}
    </>
  );
};

export const TimePickerAndroid = props => {
  const {show, theTime, pickerMode, onChange: onChangeTime, cancel} = props;
  console.log('theDate Date Picker Android: ', theTime);

  const [time, setTime] = useState(theTime);
  const [mode, setMode] = useState(pickerMode);
  const [showup, setShowUp] = useState(false);

  useEffect(() => {
    settingShow(show);
  }, []);

  const settingShow = value => {
    setShowUp(value);
  };

  const onChange = (event, selectedTime) => {
    console.log('Event Date Picker Android: ', event);
    console.log('SelectedDate Date Picker Android: ', selectedTime);
    if (event.type === 'set') {
      onChangeTime(selectedTime);
      settingShow(false);
    } else {
      cancel();
      settingShow(false);
    }
  };

  return (
    <View>
      {showup ? (
        <DateTimePicker
          testID="dateTimePicker"
          value={theTime === '' ? new Date() : new Date(time)}
          mode={pickerMode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </View>
  );
};

export const DatePickerAndroid = props => {
  const {show, theDate, pickerMode, onChange: onChangeDate, cancel} = props;
  console.log('theDate Date Picker Android: ', theDate);

  const [date, setDate] = useState(theDate);
  const [mode, setMode] = useState(pickerMode);
  const [showup, setShowUp] = useState(false);

  useEffect(() => {
    settingShow(show);
  }, []);

  const settingShow = value => {
    setShowUp(value);
  };

  const onChange = (event, selectedDate) => {
    console.log('Event Date Picker Android: ', event);
    console.log('SelectedDate Date Picker Android: ', selectedDate);
    if (event.type === 'set') {
      onChangeDate(selectedDate);
      settingShow(false);
    } else {
      cancel();
      settingShow(false);
    }
  };

  return (
    <View>
      {showup ? (
        <DateTimePicker
          testID="dateTimePicker"
          minimumDate={new Date()}
          value={theDate === '' ? new Date() : new Date(date)}
          mode={pickerMode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </View>
  );
};

export const TimePickerIOS = props => {
  console.log('KONTOL: ', props);
  const {theDate, pickerMode, onChange, cancel} = props;

  const [date, setDate] = useState(theDate === '' ? new Date() : theDate);

  const onChangeDate = (event, selectedDate) => {
    console.log('IOS Date Picker Event: ', event);
    console.log('IOS Date Picker SelectedDate: ', selectedDate);
    setDate(selectedDate);
  };

  const ok = () => {
    onChange(date === '' ? new Date() : date);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: overlayDim,
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          cancel();
        }}
        style={{flex: 2, backgroundColor: 'transparent'}}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: greyLine,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: '#f8f8f8',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => cancel()}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => {
                ok();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: '100%', backgroundColor: white}}>
          <DateTimePicker
            // style={
            //   hasNotch()
            //     ? {
            //         left: 50,
            //         position: 'relative',
            //         width: width,
            //       }
            //     : {
            //         left: 20,
            //         position: 'relative',
            //         width: width,
            //       }
            // }
            testID="dateTimePicker"
            value={date === '' ? new Date() : new Date(date)}
            mode={pickerMode}
            is24Hour={true}
            display='spinner'
            onChange={onChangeDate}
          />
        </View>
      </View>
    </View>
  );
};

export const DatePickerIOS = props => {
  const {theDate, pickerMode, onChange, cancel} = props;

  const [date, setDate] = useState(theDate === '' ? new Date() : theDate);

  const onChangeDate = (event, selectedDate) => {
    console.log('IOS Date Picker Event: ', event);
    console.log('IOS Date Picker SelectedDate: ', selectedDate);
    setDate(selectedDate);
  };

  const ok = () => {
    onChange(date === '' ? new Date() : date);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: overlayDim,
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          cancel();
        }}
        style={{flex: 2, backgroundColor: 'transparent'}}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: greyLine,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: '#f8f8f8',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => cancel()}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => {
                ok();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: width, backgroundColor: white}}>
          <DateTimePicker
            // style={
            //   hasNotch()
            //     ? {
            //         left: 50,
            //         position: 'relative',
            //         width: width,
            //       }
            //     : {
            //         left: 20,
            //         position: 'relative',
            //         width: width,
            //       }
            // }
            testID="dateTimePicker"
            value={date === '' ? new Date() : new Date(date)}
            mode={pickerMode}
            is24Hour={true}
            display="spinner"
            onChange={onChangeDate}
          />
        </View>
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(DateAndTimePickupForm);

export default props => <Wrapper {...props} />;
