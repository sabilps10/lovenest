import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  TextInput,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, white, mainRed, mainGreen, greyLine} = Colors;
const {medium, book} = FontType;

export const Form = props => {
  const {
    isError,
    required,
    value,
    onChangeText,
    title,
    placeholder,
    multiline,
    numberOfLines,
    isLoading,
    type, //email or free text
  } = props;

  const [isInCorrectEmail, setIsInCorrectEmail] = useState(false);

  useEffect(() => {
    if (type === 'Email') {
      validate(value);
    }
  }, [value, type]);

  const validate = async emailText => {
    try {
      let reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

      const testEmailText = await reg.test(emailText);
      if (!testEmailText) {
        // Error Format fo Email
        await setIsInCorrectEmail(true);
      } else {
        // Correct Format of email
        await setIsInCorrectEmail(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View style={{flexDirection: 'row', width: '100%'}}>
          {required ? <Text style={{color: mainRed}}>*</Text> : null}
          {!required ? (
            <Text
              style={{
                left: 3,
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {`${title}`}
            </Text>
          ) : (
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          )}
          {isLoading ? (
            <View style={{marginLeft: 10, flexDirection: 'row'}}>
              <Text
                style={{
                  fontSize: RFPercentage(1.5),
                  color: greyLine,
                }}>
                {'updating...'}
              </Text>
              <ActivityIndicator
                size="small"
                color={mainGreen}
                style={{marginHorizontal: 5, bottom: 5}}
              />
            </View>
          ) : null}
        </View>
        <View style={{marginTop: 5}}>
          <TextInput
            keyboardType={title === 'Contact Number' ? 'phone-pad' : 'default'}
            autoCorrect={false}
            numberOfLines={numberOfLines}
            multiline={numberOfLines === 1 ? false : multiline}
            style={
              Platform.OS === 'android'
                ? {
                    textAlignVertical: 'top',
                    borderBottomWidth: 1,
                    borderBottomColor: isError ? mainRed : greyLine,
                    backgroundColor: '#ffffff',
                    fontFamily: book,
                    fontSize: RFPercentage(1.8),
                    lineHeight: 18,
                  }
                : {
                    textAlignVertical: 'top',
                    borderBottomWidth: 1,
                    borderBottomColor: isError ? mainRed : greyLine,
                    backgroundColor: '#ffffff',
                    fontFamily: book,
                    paddingLeft: 5,
                    paddingBottom: 10,
                    fontSize: RFPercentage(1.8),
                    lineHeight: 18,
                  }
            }
            value={value}
            placeholder={placeholder}
            onChangeText={async e => {
              try {
                const result = type === 'Email' ? e.toLowerCase() : e;
                await onChangeText(result);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
          {isInCorrectEmail && type === 'Email' ? (
            <Text
              style={{
                left: 5,
                marginVertical: 2,
                fontFamily: book,
                fontSize: RFPercentage(1.3),
                color: mainRed,
                fontStyle: 'italic',
                letterSpacing: 0.3,
              }}>
              wrong email format!
            </Text>
          ) : null}
        </View>
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(Form);

export default props => <Wrapper {...props} />;
