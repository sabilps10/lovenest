import React, {useState} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  TextInput,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import TextInputPhonePicker from '../Input/TextinputPhonePickerWithoutLabel';

const {black, white, mainRed, mainGreen, greyLine} = Colors;
const {medium, book} = FontType;
import ModalCountryCode from '../../components/Modal/onBoardingModal/countryCodeListModal';

const PhoneNumberInput = props => {
  console.log('DailyCalendar Props: ', props);
  const {
    title,
    required,
    value,
    placeholder,
    countryCode,
    onChange,
    onChangeCountryCode,
  } = props;

  const [country, setCountry] = useState('+62');
  const [showModalCountryCode, setShowModalCountryCode] = useState(false);

  const openModalCountryCode = () => {
    try {
      setShowModalCountryCode(true);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <>
      <CardItem
        style={{
          paddingLeft: 0,
          paddingRight: 0,
          flexDirection: 'column',
          paddingBottom: 0,
        }}>
        <View
          style={{
            width: '100%',
            paddingLeft: 15,
            paddingRight: 15,
            flexDirection: 'row',
          }}>
          {required ? (
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              *
            </Text>
          ) : null}
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
        </View>
        <TextInputPhonePicker
          title={title}
          value={value}
          placeholder={placeholder}
          countryCode={countryCode}
          modalVisibility={showModalCountryCode}
          openModal={() => {
            openModalCountryCode(true);
          }}
          onChange={e => {
            onChange(e);
          }}
        />
      </CardItem>
      <ModalCountryCode
        modalVisibility={showModalCountryCode}
        closeModal={async e => {
          if (e) {
            const {dial_code} = e;
            await setCountry(dial_code);
            await onChangeCountryCode(dial_code);
            await setShowModalCountryCode(false);
          } else {
            await setShowModalCountryCode(false);
          }
        }}
      />
    </>
  );
};

const Wrapper = compose(withApollo)(PhoneNumberInput);

export default props => <Wrapper {...props} />;
