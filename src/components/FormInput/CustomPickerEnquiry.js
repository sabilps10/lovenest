import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  TextInput,
  Modal,
  Dimensions,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

const {black, white, mainRed, mainGreen, greyLine, overlayDim} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

export const DateAndTimePickupForm = props => {
  const {
    type,
    disabled,
    isError,
    title,
    value,
    onChange,
    placeholder,
    data,
  } = props;

  const [showList, setShowList] = useState(false);

  const onOpenPicker = () => {
    try {
      setShowList(true);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <>
      <CardItem>
        <View style={{width: '100%'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: mainRed}}>*</Text>
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
            {disabled ? (
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: medium,
                  color: greyLine,
                  fontSize: RFPercentage(1.5),
                  letterSpacing: 0.3,
                  fontStyle: 'italic',
                }}>
                (please select date first!)
              </Text>
            ) : null}
          </View>
          <View style={{marginTop: 5, paddingLeft: 7, paddingRight: 7}}>
            <TouchableOpacity
              disabled={disabled}
              onPress={onOpenPicker}
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <Text
                style={{
                  color: value === '' ? greyLine : black,
                  fontSize: RFPercentage(1.85),
                  letterSpacing: 0.3,
                }}>
                {value === '' ? placeholder : value}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 10,
              borderBottomColor: isError ? mainRed : greyLine,
              borderBottomWidth: 1,
              width: '100%',
              height: 0.5,
            }}
          />
        </View>
      </CardItem>
      <Modal transparent animationType="fade" visible={showList}>
        <View style={{flex: 1, backgroundColor: overlayDim}}>
          <TouchableOpacity
            onPress={() => setShowList(false)}
            style={{flex: 1}}
          />
          <View style={{backgroundColor: white, flex: 1.5}}>
            <Card
              style={{
                marginTop: 0,
                marginLeft: 0,
                marginRight: 0,
                bottom: 10,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    borderRadius: 25,
                    padding: 10,
                    paddingTop: 5,
                    paddingBottom: 5,
                    backgroundColor: mainGreen,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: white,
                      letterSpacing: 0.3,
                    }}>
                    {title}
                  </Text>
                </View>
                <View>
                  <Button
                    onPress={() => setShowList(false)}
                    transparent
                    style={{
                      alignSelf: 'flex-end',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="x"
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 24,
                        color: black,
                      }}
                    />
                  </Button>
                </View>
              </CardItem>
            </Card>
            <FlatList
              contentContainerStyle={{paddingBottom: 10}}
              data={data}
              extraData={data}
              keyExtractor={(item, index) => `${index} List`}
              renderItem={({item, index}) => {
                return (
                  <Card transparent style={{marginTop: 0}}>
                    <CardItem
                      button
                      onPress={async () => {
                        try {
                          await onChange(item.value);
                          await setShowList(false);
                        } catch (error) {
                          console.log('Error: ', error);
                        }
                      }}
                      style={{paddingBottom: 0}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.5),
                          color: mainGreen,
                          letterSpacing: 0.3,
                        }}>
                        {item.label}
                      </Text>
                    </CardItem>
                    <CardItem>
                      <View
                        style={{
                          width: '100%',
                          borderBottomColor: greyLine,
                          borderBottomWidth: 1,
                          height: 1,
                        }}
                      />
                    </CardItem>
                  </Card>
                );
              }}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

const Wrapper = compose(withApollo)(DateAndTimePickupForm);

export default props => <Wrapper {...props} />;
