import React, {useState, useCallback} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import {Card, CardItem} from 'native-base';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  NotificationIcon,
  commonImage,
  BigIconNotification,
} from '../../../utils/Themes/Images';
import Dash from 'react-native-dash';

const {lnLogoNotif} = commonImage;
const {BigIconNewsEvent, BigIconNewsPromo} = BigIconNotification;
const {InfoIcon, PromoIcon} = NotificationIcon;
const {regular} = FontSize;
const {medium, book} = FontType;
const {black, greyLine, mainRed} = Colors;
const {width, height} = Dimensions.get('window');

const CardPromo = props => {
  console.log('Props Card Promo: ', props);
  const {item} = props;

  const numOfLines = 3;
  const [textShown, setTextShown] = useState(false); // remain text
  const [lengthMore, setLengtMore] = useState(false); // read more or less
  const toggleNumberOfLines = () => {
    // hide or show text
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(e => {
    setLengtMore(e.nativeEvent.lines.length >= 3);
  }, []);

  const handleURL = url => {
    try {
      if (url !== '' || url) {
        Linking.canOpenURL(url).then(supported => {
          if (supported) {
            Linking.openURL(url);
          } else {
            console.log(`Don't know how to open URI: ${url}`);
          }
        });
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <TouchableOpacity
      onPress={() => {
        console.log('PROPS CARD PROMO PRESSED: ', props)
        handleURL(item?.update?.promotion?.promoLinkURL)
      }}>
      <Card
        transparent
        style={{marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0}}>
        <CardItem
          header
          style={{
            backgroundColor: 'transparent',
            width: '100%',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 5,
            paddingBottom: 15,
          }}>
          <Image
            source={PromoIcon}
            style={{width: width / 25, height: height / 25, marginRight: 5}}
            resizeMode="contain"
          />
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: greyLine,
              letterSpacing: 0.3,
            }}>
            Promo
          </Text>
          <View
            style={{
              height: '50%',
              width: 2,
              borderRightWidth: 1,
              borderRightColor: greyLine,
              marginHorizontal: 10,
            }}
          />
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: greyLine,
              letterSpacing: 0.3,
            }}>
            {moment(new Date(item.createdOn))
              .utc()
              .local()
              .fromNow()}
          </Text>
        </CardItem>
        {item?.update?.promotion ? (
          <CardItem
            style={{
              paddingTop: 0,
              backgroundColor: 'transparent',
              paddingBottom: 15,
            }}>
            <View
              style={{
                width: width / 1.1,
                height: height / 4.5,
                overflow: 'hidden',
                borderRadius: 4,
              }}>
              <Image
                source={
                  item?.update?.promotion?.promoDynamicURL
                    ? {uri: item?.update?.promotion?.promoDynamicURL}
                    : {uri: item?.update?.promotion?.promoImageURL}
                }
                style={{flex: 1}}
                resizeMethod="resize"
                resizeMode="cover"
              />
            </View>
          </CardItem>
        ) : null}
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <View style={{flex: 1, height: '100%'}}>
            <Text
              style={{
                fontFamily: medium,
                lineHeight: 18,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                marginBottom: 10,
              }}>
              {item?.update?.title}
            </Text>
            <Text
              onTextLayout={onTextLayout}
              numberOfLines={textShown ? undefined : numOfLines}
              style={{
                fontFamily: book,
                lineHeight: 18,
                fontSize: RFPercentage(1.5),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.update?.description}
            </Text>
            {lengthMore ? (
              <Text
                onPress={toggleNumberOfLines}
                style={{
                  lineHeight: 21,
                  marginTop: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                {textShown ? 'Read less' : 'Read more...'}
              </Text>
            ) : null}
          </View>
          <View
            style={{
              flex: 0.3,
              height: '100%',
              justifyContent: 'flex-end',
              alignItems: 'flex-start',
              flexDirection: 'row',
            }}>
            <View style={{width: width / 9, height: height / 17}}>
              <Image
                source={BigIconNewsPromo}
                style={{width: '100%', height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
        <CardItem style={{backgroundColor: 'transparent'}}>
          <Dash
            style={{flexDirection: 'row', width: '100%', height: 1}}
            dashColor={greyLine}
            dashThickness={0.9}
          />
        </CardItem>
      </Card>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(CardPromo);

export default props => <Wrapper {...props} />;
