import React from 'react';
import {Text, View, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {medium, book} = FontType;
const {black, mainRed, white} = Colors;
const {width, height} = Dimensions.get('window');

const FilterList = props => {
  console.log('DailyCalendar Props: ', props);
  const {list, onChange, filterText} = props;
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
      horizontal
      data={list}
      extraData={list}
      keyExtractor={item => `${item.id} Filter`}
      renderItem={({item, index}) => {
        return (
          <TouchableOpacity
            onPress={() => onChange(item.name)}
            style={{
              marginHorizontal: 8,
              paddingTop: 8,
              paddingBottom: 8,
              paddingLeft: 10,
              paddingRight: 10,
              borderRadius: 25,
              borderWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: filterText === item.name ? mainRed : white,
              borderColor: mainRed,
            }}>
            <Text
              style={{
                color: filterText === item.name ? white : mainRed,
                fontFamily: medium,
                fontSize: RFPercentage(1.5),
                letterSpacing: 0.3,
              }}>
              {item.name}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const Wrapper = compose(withApollo)(FilterList);

export default props => <Wrapper {...props} />;
