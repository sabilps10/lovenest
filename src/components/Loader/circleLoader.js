import React from 'react';
import {View} from 'react-native';
import LottieView from 'lottie-react-native';
import loaderAnimation from '../../utils/Lottie/loader.json';

export default class circleLoader extends React.Component {
  render() {
    return (
      <View style={{width: 100, height: 100}}>
        <LottieView source={loaderAnimation} autoPlay loop />
      </View>
    );
  }
}
