import React from 'react';
import LottieView from 'lottie-react-native';
import loaderAnimation from '../../utils/Lottie/loader.json';

export default class circleLoader extends React.Component {
  render() {
    return <LottieView source={loaderAnimation} autoPlay loop />;
  }
}
