import React from 'react';
import {View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import MerchantShimmer from '../Event/EventListShimmer';
import Colors from '../../../utils/Themes/Colors';
import {Container} from 'native-base';

const {newContainerColor} = Colors;
const shimmerSize = [1, 2, 3, 4, 5];

const MerchantShimmerComponent = () => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          paddingTop: 15,
          paddingBottom: 15,
          paddingLeft: 15,
          paddingRight: 15,
        }}>
        {shimmerSize.map((_, i) => {
          return <MerchantShimmer key={`${String(i)} shimmer`} />;
        })}
      </View>
    </Container>
  );
};

const Wrapper = compose(withApollo)(MerchantShimmerComponent);

export default props => <Wrapper {...props} />;
