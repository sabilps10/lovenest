import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import ContentLoader, {Rect} from 'react-content-loader/native';

const {greyLine, newContainerColor} = Colors;

const ProductShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={520}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="0" y="0" rx="5" ry="5" width="30%" height="20" />
      <Rect x="0" y="40" rx="5" ry="5" width="45%" height="188" />
      <Rect x="190" y="40" rx="5" ry="5" width="45%" height="188" />
      <Rect x="0" y="256" rx="5" ry="5" width="45%" height="188" />
      <Rect x="190" y="256" rx="5" ry="5" width="45%" height="188" />
    </ContentLoader>
  );
};

const Wrapper = compose(withApollo)(ProductShimmer);

export default props => <Wrapper {...props} />;
