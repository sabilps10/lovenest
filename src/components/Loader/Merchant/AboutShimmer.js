import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ContentLoader, {Rect, Circle, Path} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const AboutShimmer = props => {
  return (
    <ContentLoader
      speed={1}
      width={'100%'}
      height={450}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="0" y="0" rx="3" ry="3" width="60%" height="6" />
      <Rect x="0" y="30" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="42" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="54" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="66" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="78" rx="3" ry="3" width="100%" height="6" />

      <Rect x="0" y="102" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="114" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="126" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="138" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="150" rx="3" ry="3" width="100%" height="6" />

      <Rect x="0" y="174" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="186" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="198" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="210" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="222" rx="3" ry="3" width="100%" height="6" />

      <Rect x="0" y="246" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="258" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="270" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="282" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="294" rx="3" ry="3" width="100%" height="6" />

      <Rect x="0" y="318" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="330" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="342" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="354" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="366" rx="3" ry="3" width="100%" height="6" />

      <Rect x="0" y="390" rx="3" ry="3" width="90%" height="6" />
      <Rect x="0" y="402" rx="3" ry="3" width="100%" height="6" />
      <Rect x="0" y="414" rx="3" ry="3" width="80%" height="6" />
      <Rect x="0" y="426" rx="3" ry="3" width="70%" height="6" />
      <Rect x="0" y="438" rx="3" ry="3" width="100%" height="6" />
    </ContentLoader>
  );
};

const Wrapper = compose(withApollo)(AboutShimmer);

export default props => <Wrapper {...props} />;
