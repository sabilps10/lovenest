import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ContentLoader, {Rect} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const SpecialOfferShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={1000}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="18" y="15" rx="5" ry="5" width="90%" height="160" />
      <Rect x="18" y="190" rx="5" ry="5" width="90%" height="160" />
      <Rect x="18" y="368" rx="5" ry="5" width="90%" height="160" />
      <Rect x="18" y="543" rx="5" ry="5" width="90%" height="160" />
      <Rect x="18" y="718" rx="5" ry="5" width="90%" height="160" />
    </ContentLoader>
  );
};

const Wrapper = compose(withApollo)(SpecialOfferShimmer);

export default props => <Wrapper {...props} />;
