import React from 'react';
import {View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import MerchantShimmer from '../Event/EventListShimmer';
import Colors from '../../../utils/Themes/Colors';
import {Container} from 'native-base';
import ContentLoader, {Rect} from 'react-content-loader/native';

const {newContainerColor, greyLine} = Colors;

export const ShimmerBanner = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={800}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="0" y="0" rx="5" ry="5" width="100%" height="188" />
      <Rect x="15" y="200" rx="5" ry="5" width="100" height="25" />
      <Rect x="120" y="200" rx="5" ry="5" width="100" height="25" />
      <Rect x="225" y="200" rx="5" ry="5" width="100" height="25" />
      <Rect x="335" y="200" rx="5" ry="5" width="100" height="25" />
      <Rect x="15" y="240" rx="5" ry="5" width="90%" height="150" />
      <Rect x="15" y="415" rx="5" ry="5" width="90%" height="150" />
      <Rect x="15" y="590" rx="5" ry="5" width="90%" height="150" />
    </ContentLoader>
  );
};

const MerchantDetailShimmerComponent = () => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View
        style={{
          flex: 1,
        }}>
        <ShimmerBanner />
      </View>
    </Container>
  );
};

const Wrapper = compose(withApollo)(MerchantDetailShimmerComponent);

export default props => <Wrapper {...props} />;
