import React from 'react';
import {Text, View} from 'react-native';
import {Container, Content} from 'native-base';
import Loader from './circleLoader';
import Colors from '../../utils/Themes/Colors';

const {white} = Colors;

const LoaderWithContainer = () => {
  return (
    <Container style={{backgroundColor: white}}>
      <Content
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </Content>
    </Container>
  );
};

export default LoaderWithContainer;
