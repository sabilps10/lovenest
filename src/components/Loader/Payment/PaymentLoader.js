import React from 'react';
import {StatusBar, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import Loader from '../../Loader/circleLoader';
import Colors from '../../../utils/Themes/Colors';
import DefaultHeader from '../../Header/Payments/DefaultHeader';

const {newContainerColor} = Colors;

const PaymentLoader = props => {
  console.log('PaymentLoader Props: ', props);
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <DefaultHeader Title="Payment" />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    </Container>
  );
};

const Wrapper = compose(withApollo)(PaymentLoader);

export default props => <Wrapper {...props} />;
