import React from 'react';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const BannerShimmer = props => {
  return (
    <ContentLoader
      speed={1}
      width={'100%'}
      height={230}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      style={{width: '100%'}}
      {...props}>
      <Rect x="0" y="0" rx="0" ry="0" width="100%" height="180" />
    </ContentLoader>
  );
};

export default BannerShimmer;
