import React from 'react';
import ContentLoader, {Rect} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const EventListShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={210}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="0" y="0" rx="5" ry="5" width="100%" height="188" />
    </ContentLoader>
  );
};

export default EventListShimmer;
