import React from 'react';
import ContentLoader, {Rect} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const SpecialOfferShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={200}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="15" y="10" rx="10" ry="10" width="150" height="15" />
      <Rect x="15" y="45" rx="8" ry="8" width="250" height="140" />
      <Rect x="290" y="45" rx="8" ry="8" width="250" height="140" />
    </ContentLoader>
  );
};

export default SpecialOfferShimmer;
