import React from 'react';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';
import {Dimensions} from 'react-native';

const {width} = Dimensions?.get('screen');
const {greyLine, newContainerColor} = Colors;

const MenuShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={900}
      backgroundColor={Colors?.lightBlueGrey}>
      <Rect x="15" y="10" rx="5" ry="5" width="70%" height="60" />
      <Rect x="15" y="80" rx="5" ry="5" width="70%" height="10" />
    </ContentLoader>
  );
};

export default MenuShimmer;
