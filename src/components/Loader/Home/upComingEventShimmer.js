import React from 'react';
import ContentLoader, {Rect} from 'react-content-loader/native';
import Colors from '../../../utils/Themes/Colors';

const {greyLine, newContainerColor} = Colors;

const UpComingEventShimmer = props => {
  return (
    <ContentLoader
      width={'100%'}
      height={240}
      speed={1}
      backgroundColor={greyLine}
      foregroundColor={newContainerColor}
      {...props}>
      <Rect x="15" y="0" rx="10" ry="10" width="150" height="15" />
      <Rect x="15" y="35" rx="8" ry="8" width="150" height="190" />
      <Rect x="190" y="35" rx="8" ry="8" width="150" height="190" />
      <Rect x="365" y="35" rx="8" ry="8" width="150" height="190" />
    </ContentLoader>
  );
};

export default UpComingEventShimmer;
