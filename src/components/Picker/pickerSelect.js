import React from 'react';
import {Platform, StyleSheet} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black} = Colors;
const {regular} = FontSize;
const {book} = FontType;

const Picker = props => {
  const {
    items,
    placeholder,
    useNativeAndroidPickerStyle,
    onChange,
    disabled,
  } = props;
  return (
    <RNPickerSelect
      disabled={!disabled ? false : true}
      useNativeAndroidPickerStyle={useNativeAndroidPickerStyle ? true : false}
      style={{...styles}}
      placeholder={placeholder}
      onValueChange={value => onChange(value)}
      items={items}
    />
  );
};

const styles = StyleSheet.create({
  placeholder: {
    left: Platform.OS === 'android' ? -7 : 1,
    fontSize: RFPercentage(1.7),
  },
  inputAndroid: {
    left: Platform.OS === 'android' ? -7 : 1,
    fontFamily: book,
    // fontSize: regular,
    fontSize: RFPercentage(1.7),
    color: black,
  },
  inputIOS: {
    left: Platform.OS === 'android' ? -7 : 1,
    fontFamily: book,
    // fontSize: regular,
    fontSize: RFPercentage(1.7),
    color: black,
  },
});

export default Picker;
