import React, {useState, useEffect} from 'react';
import {View, Text, Modal, Platform, TouchableOpacity} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import moment from 'moment';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

const {
  veryLightPinkTwo,
  offWhite,
  mainRed,
  mainGreen,
  white,
  black,
  greyLine,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;

const DateTimeAndPicker = props => {
  const {value, mode, display, done, cancel, minDate, openStatus} = props;
  const [state, setState] = useState({
    date: new Date(),
    updatedDate: value,
  });

  const [show, setShow] = useState(false);

  useEffect(() => {
    settingShowAndroid(openStatus);
    settingState();
  }, []);

  const settingState = () => {
    let propData = state;
    propData.updatedDate = value;
    setState(propData);
  };

  const settingShowAndroid = e => {
    setShow(e);
  };

  const updateDate = selectedDate => {
    const getUnixTimeStamp = moment(selectedDate).valueOf();
    state.updatedDate = new Date(getUnixTimeStamp);
    setState(state);
  };

  const updateDateAndroid = (e, selectedDate) => {
    if (e.type === 'set') {
      const newSetOfDate = new Date(e.nativeEvent.timestamp);
      state.updatedDate = newSetOfDate;
      setState(state);
      settingShowAndroid(false);
      done(state.updatedDate === '' ? state.date : state.updatedDate);
    } else {
      cancel();
    }
  };

  if (Platform.OS === 'ios') {
    return (
      <Modal visible={true} transparent animationType="fade">
        <View style={{flex: 1, backgroundColor: overlayDim}}>
          <TouchableOpacity
            style={{flex: 4}}
            onPress={() => {
              cancel();
            }}
          />
          <View
            style={{
              flex: 2,
              flexDirection: 'column',
              backgroundColor: veryLightPinkTwo,
            }}>
            {/* <View
              style={{
                height: 35,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: offWhite,
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    cancel();
                  }}>
                  <Text
                    style={{
                      fontWeight: '300',
                      fontSize: regular,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  onPress={() =>
                    done(
                      state.updatedDate === '' ? state.date : state.updatedDate,
                    )
                  }>
                  <Text
                    style={{
                      color: '#147EFB',
                      fontWeight: '500',
                      fontSize: regular,
                    }}>
                    Done
                  </Text>
                </TouchableOpacity>
              </View>
            </View> */}
            <View
              style={{
                width: '100%',
                backgroundColor: '#f8f8f8',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  padding: 7,
                }}>
                <TouchableOpacity
                  onPress={() => cancel()}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 7,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: mainRed,
                      fontSize: RFPercentage(1.8),
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                  padding: 7,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    done(
                      state.updatedDate === '' ? state.date : state.updatedDate,
                    );
                  }}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 7,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: mainRed,
                      fontSize: RFPercentage(1.8),
                    }}>
                    OK
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{backgroundColor: veryLightPinkTwo}}>
              <DateTimePicker
                // style={{
                //   position: 'relative',
                //   left: hasNotch() ? 50 : 20,
                //   bottom: hasNotch() ? 0 : 15,
                // }}
                minimumDate={minDate}
                value={
                  state.updatedDate === '' ? state.date : state.updatedDate
                }
                mode={mode}
                display={display}
                onChange={(event, selectedDate) => {
                  updateDate(selectedDate);
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  } else {
    return (
      <>
        {show ? (
          <DateTimePicker
            minimumDate={minDate}
            value={state.updatedDate === '' ? state.date : state.updatedDate}
            mode={mode}
            display={display}
            onChange={(event, selectedDate) => {
              console.log('Event: ', event);
              console.log('SelectedDate: ', selectedDate);
              updateDateAndroid(event, selectedDate);
            }}
          />
        ) : null}
      </>
    );
  }
};

export default DateTimeAndPicker;
