import React from 'react';
import {StyleSheet, Dimensions, View, Text, ScrollView} from 'react-native';
import Video from 'react-native-video';

export const VideoPlayer = props => {
  return (
    <View style={styles.container}>
      <Video
        source={props?.url}
        style={styles.video}
        controls={true}
        resizeMode={'cover'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  video: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * (9 / 14.6),
    backgroundColor: 'black',
  },
  text: {
    marginTop: 30,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: 'justify',
  },
});
