import React, {useState} from 'react';
import {View, Image, Platform, Animated} from 'react-native';
import Loader from '../Loader/JustLoader';

/**
 *
 * @param {style, source, placeholderColor, loaderStyle} props
 */

const AnimatedAsyncImage = props => {
  // console.log('Async Image Props: ', props);
  const {style, source, placeholderColor, loaderStyle, resizeMode} = props;
  let [loaded, setLoaded] = useState(false);

  const onLoad = () => {
    setLoaded(true);
  };

  return (
    <Animated.View style={style}>
      {Platform.OS === 'ios' ? (
        <Animated.Image
          source={
            source.uri === undefined ? source : {...source, cache: 'default'}
          }
          style={[
            style,
            {
              position: 'absolute',
              // resizeMode: 'contain',
            },
          ]}
          onLoad={() => onLoad()}
          resizeMethod="auto"
          resizeMode={resizeMode ? resizeMode : 'cover'}
        />
      ) : (
        <Animated.Image
          source={source}
          style={[
            style,
            {
              position: 'absolute',
              // resizeMode: 'contain',
            },
          ]}
          onLoad={() => onLoad()}
          resizeMethod="auto"
          resizeMode={resizeMode ? resizeMode : 'cover'}
        />
      )}
      {!loaded && (
        <Animated.View
          style={[
            style,
            {
              backgroundColor: placeholderColor,
              position: 'absolute',
              justifyContent: 'center',
              alignItems: 'center',
            },
          ]}>
          <View style={loaderStyle}>
            <Loader />
          </View>
        </Animated.View>
      )}
    </Animated.View>
  );
};

export default AnimatedAsyncImage;
