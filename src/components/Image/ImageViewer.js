/* eslint-disable no-unused-vars */
/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {
  Text,
  View,
  ToastAndroid,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Modal,
  StatusBar,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Icon} from 'native-base';
import {withApollo} from 'react-apollo';
import {connect} from 'react-redux';
import compose from 'lodash/fp/compose';
import ImageViewer from 'react-native-image-zoom-viewer';
// import CameraRoll from '@react-native-community/cameraroll';
// import RNFetchBlob from 'rn-fetch-blob';
import Colors from '../../utils/Themes/Colors';

const {lightSalmon, transparent, overlayDim, mainRed} = Colors;
const {height, width} = Dimensions.get('window');

class ImageViewerSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageList: [],
      open: false,
      index: 0,
      renderImage: '',
      toasterDownloading: false,
      toasterSuccessDownloading: false,
    };
  }

  async componentDidMount() {
    const imageList = await Promise.all(
      this.props.imageData.map((data, index) => {
        return {
          id: (index += 1),
          url: data,
        };
      }),
    );
    this.setState(prevState => ({
      ...prevState,
      open: this.props.openStatus,
      imageList,
    }));
  }

  async componentWillReceiveProps(nextProps) {
    const imageList = await Promise.all(
      nextProps.imageData.map((data, index) => {
        return {
          id: (index += 1),
          url: data,
        };
      }),
    );
    this.setState(prevState => ({
      ...prevState,
      open: nextProps.openStatus,
      imageList,
    }));
  }

  close = () => {
    this.setState(
      prevState => ({
        ...prevState,
        open: false,
      }),
      () => {
        this.props.closeImageViewer(false);
      },
    );
  };

  next = () => {
    const {imageList} = this.state;
    let {index} = this.state;
    index += 1;

    if (index >= imageList.length) {
      this.setState(prevState => ({
        ...prevState,
        index: 0,
      }));
    } else {
      console.log('INDEXnya ', index);
      this.setState(prevState => ({
        ...prevState,
        index,
      }));
    }
  };

  prev = () => {
    const {imageList} = this.state;
    let {index} = this.state;
    index -= 1;

    if (index < 0) {
      const lenghtImage = imageList.length - 1;
      this.setState(prevState => ({
        ...prevState,
        index: lenghtImage,
      }));
    } else {
      console.log('INDEXnya ', index);
      this.setState(prevState => ({
        ...prevState,
        index,
      }));
    }
  };

  //   saveCameraRollAndroid = async url => {
  //     try {
  //       // Ask Permission
  //       const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
  //         title: 'Cool Photo App Camera Permission',
  //         message: 'Cool Photo App needs access to your camera ' + 'so you can take awesome pictures.',
  //         buttonNeutral: 'Ask Me Later',
  //         buttonNegative: 'Cancel',
  //         buttonPositive: 'OK',
  //       });

  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         RNFetchBlob.config({
  //           fileCache: true,
  //           appendExt: 'jpg',
  //         })
  //           .fetch('GET', url)
  //           .then(res => {
  //             console.log('Android Res: ', res);
  //             CameraRoll.saveToCameraRoll(res.path())

  //               .then(res => {
  //                 console.log('save', res);
  //                 ToastAndroid.show('Image saved Successfully.', ToastAndroid.SHORT);
  //               })
  //               .catch(error => {
  //                 console.log('error: fetch blob ', error);
  //                 ToastAndroid.show('Ops! Operation Failed', ToastAndroid.SHORT);
  //               });
  //           });
  //       } else {
  //         ToastAndroid.show('Dont have permission!', ToastAndroid.SHORT);
  //       }
  //     } catch (error) {
  //       ToastAndroid.show("Can't download the attatchment!", ToastAndroid.SHORT);
  //     }
  //   };

  //   saveToCameraRollIOS = async url => {
  //     try {
  //       console.log('URL SAVE CAMERA ROLL: ', url);
  //       this.setState(prevState => ({
  //         ...prevState,
  //         toasterDownloading: true,
  //       }));
  //       CameraRoll.saveToCameraRoll(url)
  //         .then(res => {
  //           console.log('Res: iOS: ', res);

  //           if (res) {
  //             this.setState(prevState => ({
  //               ...prevState,
  //               toasterDownloading: false,
  //               toasterSuccessDownloading: true,
  //             }));

  //             setTimeout(() => {
  //               this.setState(prevState => ({
  //                 ...prevState,
  //                 toasterDownloading: false,
  //                 toasterSuccessDownloading: false,
  //               }));
  //             }, 3000);
  //           } else {
  //             this.setState(prevState => ({
  //               ...prevState,
  //               toasterDownloading: false,
  //             }));
  //           }
  //         })
  //         .catch(error => {
  //           console.log('iOS error: ', error);

  //           this.setState(prevState => ({
  //             ...prevState,
  //             toasterDownloading: false,
  //           }));
  //         });
  //     } catch (error) {
  //       console.log('Error: ', error);
  //       this.setState(prevState => ({
  //         ...prevState,
  //         toasterDownloading: false,
  //       }));
  //     }
  //   };

  render() {
    console.log('PROPS bro: ', this.props);
    const {download} = this.props;
    return (
      <React.Fragment>
        <StatusBar
          translucent={false}
          backgroundColor={this.props.openStatus ? 'black' : 'white'}
          animated
          barStyle={this.props.openStatus ? 'light-content' : 'dark-content'}
        />
        <Modal visible={this.state.open} transparent style={{flex: 1}}>
          {this.state.toasterSuccessDownloading ? (
            <View
              style={{
                backgroundColor: 'transparent',
                flex: 1,
                borderWidth: 1,
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: 999,
              }}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 4,
                  borderColor: lightSalmon,
                  backgroundColor: 'white',
                  width: 200,
                  height: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                }}>
                <Icon
                  type="Feather"
                  name="check"
                  style={{fontSize: 35, color: lightSalmon}}
                />
                <Text style={{fontSize: 14, color: 'black'}}>
                  Download Success!
                </Text>
              </View>
            </View>
          ) : null}
          {this.state.toasterDownloading ? (
            <View
              style={{
                backgroundColor: overlayDim,
                flex: 1,
                borderWidth: 1,
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: 999,
              }}>
              <ActivityIndicator size="large" color={lightSalmon} />
            </View>
          ) : null}
          <TouchableOpacity onPress={this.close} style={styles.backButton}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={styles.iconBackButton}
            />
          </TouchableOpacity>
          {/* {download ? (
            <TouchableOpacity
              onPress={() => {
                if (Platform.OS === 'android') {
                  this.saveCameraRollAndroid(this.state.imageList[this.state.index].url);
                } else {
                  this.saveToCameraRollIOS(this.state.imageList[this.state.index].url);
                }
              }}
              style={styles.rightButton}
            >
              <Icon type="Feather" name="download" style={styles.iconBackButton} />
            </TouchableOpacity>
          ) : null} */}
          <ImageViewer
            enableSwipeDown
            onSwipeDown={() => this.close()}
            style={{width, height}}
            index={this.state.index}
            saveToLocalByLongPress={false}
            imageUrls={this.state.imageList}
            renderArrowLeft={() => {
              if (this.state.imageList.length === 1) {
                return null;
              } else {
                return (
                  <TouchableOpacity
                    onPress={this.prev}
                    style={styles.backArrow}>
                    <Icon
                      type="Feather"
                      name="chevron-left"
                      style={styles.iconArrow}
                    />
                  </TouchableOpacity>
                );
              }
            }}
            renderArrowRight={() => {
              if (this.state.imageList.length === 1) {
                return null;
              } else {
                return (
                  <TouchableOpacity
                    onPress={this.next}
                    style={styles.nextArrow}>
                    <Icon
                      type="Feather"
                      name="chevron-right"
                      style={styles.iconArrow}
                    />
                  </TouchableOpacity>
                );
              }
            }}
            renderImage={() => {
              return (
                <Image
                  source={{
                    uri: `${this.state.imageList[this.state.index].url}`,
                  }}
                  style={{flex: 1, width: null, height}}
                />
              );
            }}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  backButton: {
    backgroundColor: transparent,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 48 / 2,
    height: 48,
    width: 48,
    position: 'absolute',
    left: 0,
    top: 30,
    zIndex: 99,
  },
  rightButton: {
    backgroundColor: transparent,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 48 / 2,
    height: 48,
    width: 48,
    position: 'absolute',
    right: 0,
    top: 30,
    zIndex: 99,
  },
  iconBackButton: {fontSize: 24, color: 'white'},
  iconArrow: {fontSize: 24, color: mainRed},
  backArrow: {
    elevation: 1,
    opacity: 0.7,
    backgroundColor: 'white',
    width: 38,
    height: 38,
    borderRadius: 38 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    left: 5,
  },
  nextArrow: {
    elevation: 1,
    opacity: 0.7,
    backgroundColor: 'white',
    width: 38,
    height: 38,
    borderRadius: 38 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    right: 5,
  },
});

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ImageViewerSlider);

const Wrapper = compose(withApollo)(ConnectedComponent);

export default props => {
  return <Wrapper {...props} />;
};
