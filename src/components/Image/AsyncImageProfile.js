import React, {useState} from 'react';
import {View, Image, Platform} from 'react-native';
import Loader from '../Loader/JustLoader';
import {profileIcon} from '../../utils/Themes/Images';
const {userIcon} = profileIcon;

/**
 *
 * @param {style, source, placeholderColor, loaderStyle} props
 */

const AsyncImagProfile = props => {
  console.log('Async Image Profile Props: ', props);
  const {style, source, placeholderColor, loaderStyle, resizeMode} = props;
  let [loaded, setLoaded] = useState(false);

  const onLoad = () => {
    setLoaded(true);
  };

  return (
    <View style={style}>
      {Platform.OS === 'ios' ? (
        <Image
          source={
            source.uri === undefined || source.uri === '' || source.uri === null
              ? userIcon
              : {...source, cache: 'default'}
          }
          style={[
            style,
            {
              position: 'absolute',
              // resizeMode: 'contain',
            },
          ]}
          onLoad={() => onLoad()}
          resizeMethod="auto"
          resizeMode={resizeMode ? resizeMode : 'cover'}
        />
      ) : (
        <Image
          source={
            source.uri === undefined || source.uri === '' || source.uri === null
              ? userIcon
              : {...source}
          }
          style={[
            style,
            {
              position: 'absolute',
              // resizeMode: 'contain',
            },
          ]}
          onLoad={() => onLoad()}
          resizeMethod="auto"
          resizeMode={resizeMode ? resizeMode : 'cover'}
        />
      )}
      {!loaded && (
        <View
          style={[
            style,
            {
              backgroundColor: placeholderColor,
              position: 'absolute',
              justifyContent: 'center',
              alignItems: 'center',
            },
          ]}>
          <View style={loaderStyle}>
            <Loader />
          </View>
        </View>
      )}
    </View>
  );
};

export default AsyncImagProfile;
