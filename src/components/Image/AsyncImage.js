import React, {useState, useCallback, useEffect} from 'react';
import {View, Image, Platform} from 'react-native';
import {Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors'

/**
 *
 * @param {style, source, placeholderColor, loaderStyle} props
 */

const AsyncImage = props => {
  // console.log('Async Image Props: ', props);
  const {style, source, placeholderColor, loaderStyle, resizeMode} = props;

  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  return (
    <View style={style}>
      <Image
        source={
          source?.uri === undefined
            ? source
            : Platform?.OS === 'ios'
            ? {uri: source.uri, cache: 'default'}
            : source
        }
        style={[
          style,
          {
            position: 'absolute',
            // resizeMode: 'contain',
          },
        ]}
        resizeMode={resizeMode ? resizeMode : 'cover'}
        resizeMethod="auto"
        onLoadStart={() => {
          // console.log('Masuk Load Start');
          setIsLoading(true);
        }}
        onLoad={() => {
          // console.log('Masuk Load Process');
          setIsLoading(true);
        }}
        onLoadEnd={() => {
          // console.log('Masuk Load End');
          setIsLoading(false);
        }}
      />
      {isLoading && (
        <View
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            // backgroundColor: '#dddddd',
            backgroundColor: Colors?.lightBlueGrey,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {/* <Icon
            type="Feather"
            name="image"
            style={{fontSize: 25, color: 'gray'}}
          /> */}
        </View>
      )}
    </View>
  );
};

export default AsyncImage;
