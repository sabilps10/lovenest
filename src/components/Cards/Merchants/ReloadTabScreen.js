import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {Container, Content, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {newContainerColor, lightSalmon, white} = Colors;
const {medium} = FontType;

const ReloadTabScreen = props => {
  console.log('ReloadTabScreen Props: ', props);
  const {heights, reload, onLayout} = props;
  return (
    <Container style={{backgroundColor: newContainerColor, height: heights}}>
      <Content
        onLayout={onLayout}
        contentContainerStyle={{
          padding: 15,
          paddingTop: 25,
          paddingBottom: 25,
        }}>
        <View
          style={{
            height: heights,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Button
            onPress={reload}
            style={{
              minWidth: '35%',
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: white,
              borderRadius: 25,
              backgroundColor: lightSalmon,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <View>
              <Text
                style={{
                  left: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                Reload
              </Text>
            </View>
            <Icon
              type="Feather"
              name="rotate-ccw"
              style={{left: 15, fontSize: RFPercentage(2.3), color: white}}
            />
          </Button>
        </View>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(ReloadTabScreen);

export default props => <Wrapper {...props} />;
