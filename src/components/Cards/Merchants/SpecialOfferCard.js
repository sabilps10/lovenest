import React from 'react';
import {Dimensions, Animated, Linking, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {Card, CardItem} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';
import {Platform} from 'react-native';
import {hasNotch} from 'react-native-device-info';

const {width, height} = Dimensions.get('window');
const {newContainerColor} = Colors;
const AnimatedCard = Animated.createAnimatedComponent(Card);

const specialOfferWidth = Dimensions.get('window').width / 1.1;
const specialOfferHeight =
  Platform.OS === 'ios'
    ? hasNotch()
      ? Dimensions.get('window').height / 5
      : Dimensions.get('window').height / 4
    : Dimensions.get('window').height / 4;

const SpecialOfferCard = props => {
  const {source, disabled, promoLinkURL, navigation, item} = props;
  const id = item?.id;
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const openLink = () => {
    Linking.canOpenURL(promoLinkURL).then(supported => {
      if (supported) {
        Linking.openURL(promoLinkURL);
      } else {
        console.log(`Don't know how to open URI: ${promoLinkURL}`);
      }
    });
  };

  return (
    <TouchableOpacity
      onPress={() => {
        // openLink()
        try {
          navigation.navigate('SpecialOfferDetail', {id});
        } catch (error) {
          console.log('Error: ', error);
        }
      }}
      // disabled={disabled}
      activeOpacity={1}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        // transparent
        style={{
          width: specialOfferWidth,
          // height: specialOfferHeight,
          marginBottom: 15,
          transform: [{scale: scale}],
          borderRadius: 4,
        }}>
        <CardItem
          cardBody
          style={{
            // width: specialOfferWidth,
            // height: specialOfferHeight,
            backgroundColor: 'transparent',
          }}>
          <AsyncImage
            style={{
              width: specialOfferWidth,
              height: specialOfferHeight,
              borderRadius: 4,
              // aspectRatio: specialOfferWidth / specialOfferHeight,
            }}
            source={source}
            placeholderColor={newContainerColor}
            loaderStyle={{
              width: width / 7,
              height: height / 7,
            }}
            resizeMode="cover"
          />
        </CardItem>
      </AnimatedCard>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(SpecialOfferCard);

export default props => <Wrapper {...props} />;
