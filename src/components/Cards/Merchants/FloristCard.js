import React, {useRef} from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  View,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {Card, CardItem, Button} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';

const {white, greyLine, mainGreen, black, newContainerColor, transparent} = Colors;
const {book, medium} = FontType;

// Card
const {width, height} = Dimensions.get('window');
const cardWidth = width / 1.1;
const cardHeight = 245;
const imageWidth = width / 8.5;
const imageHeight = height / 18;
const AnimatedCard = Animated.createAnimatedComponent(Card);

const FloristCard = props => {
  const {onPress, title, tag, source, iconSource} = props;
  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <TouchableOpacity
      style={{marginBottom: 10}}
      activeOpacity={1}
      onPress={() => {
        onPress();
      }}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        style={{
          minHeight: cardHeight,
          maxHeight: cardHeight,
          width: cardWidth,
          borderRadius: 4,
          // borderWidth: 0.5,
          // borderColor: greyLine,
          marginBottom: 10,
          transform: [{scale: scale}],
          elevation: 0,
          // borderWidth: 1.5,
          shadowOpacity: 0,
        }}>
        <CardItem cardBody style={{backgroundColor: transparent}}>
          <AsyncImage
            style={{
              flex: 1,
              width: '100%',
              height: 169.44,
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
            }}
            loaderStyle={{width: 60 / 1.5, height: 60 / 1.5}}
            source={source}
            placeholderColor={newContainerColor}
          />
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexDirection: 'row',
            flexWrap: 'wrap',
            backgroundColor: transparent,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              height: '100%',
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                paddingRight: 15,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                {title}
              </Text>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                {tag}
              </Text>
            </View>
          </View>
          <View style={{flex: 0.5}}>
            <Button
              onPress={() => {
                onPress();
              }}
              style={{
                width: width / 4,
                height: height / 25,
                backgroundColor: mainGreen,
                borderRadius: 0,
                elevation: 0,
                shadowOpacity: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                Visit Now
              </Text>
            </Button>
          </View>
        </CardItem>
      </AnimatedCard>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(FloristCard);

export default props => <Wrapper {...props} />;
