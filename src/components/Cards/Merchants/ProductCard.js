import React, {useMemo} from 'react';
import {Text, View, Dimensions, Animated, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncImage from '../../Image/AsyncImage';
import {Card, CardItem} from 'native-base';

const {width, height} = Dimensions.get('window');
const {greyLine, headerBorderBottom, newContainerColor, black} = Colors;
const {book, medium} = FontType;
const AnimatedCard = Animated.createAnimatedComponent(Card);

const ProductCard = props => {
  const {source, productId, merchantId, name, navigation, label} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const goToDetail = () => {
    if (label === 'Product') {
      navigation.push('ProductDetail', {productId, merchantId});
    } else {
      // go to Accessories Detail
      navigation.push('AccessoriesDetail', {productId, merchantId});
    }
  };

  // const RenderCard = () =>
  //   useMemo(() => {
  //     return (
  //       <TouchableOpacity
  //         activeOpacity={1}
  //         onPress={() => goToDetail()}
  //         onPressIn={pressIn}
  //         onPressOut={pressOut}>
  //         <AnimatedCard
  //           style={{
  //             width: width / 2.33,
  //             // height: height / 3,
  //             borderWidth: 1,
  //             borderColor: greyLine,
  //             elevation: 0,
  //             shadowOpacity: 0,
  //             borderRadius: 0,
  //             transform: [{scale: scale}],
  //           }}>
  //           <CardItem
  //             cardBody
  //             style={{
  //               width: width / 2.35,
  //               height: height / 2.8,
  //               backgroundColor: 'transparent',
  //             }}>
  //             <AsyncImage
  //               style={{
  //                 width: width / 2.35,
  //                 height: height / 2.8,
  //                 aspectRatio: width / 2.35 / (height / 2.8),
  //               }}
  //               source={source}
  //               placeholderColor={newContainerColor}
  //               loaderStyle={{
  //                 width: width / 7,
  //                 height: height / 7,
  //               }}
  //               resizeMode="cover"
  //             />
  //           </CardItem>
  //           <CardItem
  //             style={{
  //               backgroundColor: 'transparent',
  //               paddingLeft: 10,
  //               paddingRight: 10,
  //             }}>
  //             <Text
  //               style={{
  //                 fontFamily: book,
  //                 fontSize: RFPercentage(1.9),
  //                 color: black,
  //                 letterSpacing: 0.3,
  //               }}
  //               numberOfLines={1}>
  //               {name}
  //             </Text>
  //           </CardItem>
  //         </AnimatedCard>
  //       </TouchableOpacity>
  //     );
  //   }, [source]);

  // source, productId, merchantId, name

  // return RenderCard();

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => goToDetail()}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        style={{
          width: width / 2.2,
          marginLeft: 5,
          marginRight: 5,
          marginTop: 5,
          marginBottom: 5,
          borderWidth: 1,
          borderRadius: 4,
          borderColor: headerBorderBottom,
          transform: [{scale: scale}],
        }}>
        <CardItem
          cardBody
          style={{
            // width: '100%',
            // height: height / 3,
            backgroundColor: 'transparent',
            overflow: 'hidden'
          }}>
          <AsyncImage
            style={{
              flex: 1,
              width: '100%',
              height: height / 3.33,
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
            }}
            loaderStyle={{
              width: width / 7,
              height: height / 7,
            }}
            source={source}
            placeholderColor={newContainerColor}
            resizeMode="cover"
          />
        </CardItem>
        <CardItem
          style={{
            backgroundColor: 'transparent',
            paddingLeft: 10,
            paddingRight: 10,
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}
            numberOfLines={1}>
            {name}
          </Text>
        </CardItem>
      </AnimatedCard>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(ProductCard);

export default props => <Wrapper {...props} />;
