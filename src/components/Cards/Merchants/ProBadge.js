import React from 'react';
import {Dimensions, Text, View, Image,} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {BridalBadge} from '../../../utils/Themes/Images';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {crownBadge} = BridalBadge;
const {book, medium} = FontType;
const {black, white} = Colors;
const {width, height} = Dimensions.get('window');

const ProBadge = props => {
  console.log('ProBadge Props: ', props);
  const {title} = props;
  return (
    <LinearGradient
      colors={['#ebdcb0', '#CAB77C', '#CAB77C']}
      style={{
        minWidth: width / 2.7,
        height: height / 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
        // flexWrap: 'wrap',
      }}>
      <Image
        source={crownBadge}
        style={{width: width / 25, height: height / 25, marginRight: 5}}
        resizeMode="contain"
      />
      <Text
        style={{fontFamily: medium, color: white, fontSize: RFPercentage(1.3)}}>
        {title}
      </Text>
    </LinearGradient>
  );
};

const Wrapper = compose(withApollo)(ProBadge);

export default props => <Wrapper {...props} />;
