import React, {useMemo} from 'react';
import {Text, View, Dimensions, Animated, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncImage from '../../Image/AsyncImage';

const {black, greyLine, newContainerColor, transparent} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');
const AnimatedCard = Animated.createAnimatedComponent(Card);

const HighlightBridal = props => {
  console.log('MASUK SINI BOSSSS HighlightBridal: ', props);
  const {item, label, navigation} = props;
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  const {featuredImageDynamicURL, featuredImageURL, name, bridalTheme} = item;
  const source = featuredImageDynamicURL
    ? {uri: `${featuredImageDynamicURL}=h500`}
    : {uri: featuredImageURL};

  const RenderCard = () =>
    useMemo(() => {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            navigation.navigate('HighlightDetail', {
              productId: item.id,
              merchantId: item.merchantDetails.id,
            });
          }}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            style={{
              width: width / 1.1,
              borderRadius: 4,
              marginBottom: 15,
              transform: [{scale}],
            }}>
            <CardItem cardBody style={{backgroundColor: transparent}}>
              <AsyncImage
                style={{
                  width: width / 1.1,
                  height: height / 4,
                  aspectRatio: width / 1.1 / (height / 4),
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}
                source={source}
                placeholderColor={newContainerColor}
                loaderStyle={{
                  width: width / 7,
                  height: height / 7,
                }}
                resizeMode="cover"
              />
            </CardItem>
            <CardItem
              style={{
                backgroundColor: transparent,
                marginBottom: 0,
                paddingBottom: 0,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {name}
              </Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                backgroundColor: transparent,
                flexDirection: 'row',
                // flexWrap: 'wrap',
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  // flexWrap: 'wrap',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  height: '100%',
                }}>
                <View style={{flex: 0.1}}>
                  <Icon
                    type="Feather"
                    name="tag"
                    style={{top: -1, fontSize: RFPercentage(1.6), color: black}}
                  />
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Text
                    numberOfLines={1}
                    style={{
                      right: 10,
                      fontFamily: book,
                      fontSize: RFPercentage(1.6),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {bridalTheme}
                  </Text>
                </View>
              </View>
            </CardItem>
          </AnimatedCard>
        </TouchableOpacity>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(HighlightBridal);

export default props => <Wrapper {...props} />;
