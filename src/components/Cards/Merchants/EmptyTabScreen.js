import React from 'react';
import {Text, View, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {charImage} from '../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {newContainerColor, white, black} = Colors;
const {book} = FontType;
const {charCar} = charImage;

const EpmtyTabScreen = props => {
  console.log('EpmtyTabScreen Props: ', props);
  const {heights, onLayout, message} = props;

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <Content
        onLayout={onLayout}
        contentContainerStyle={{
          padding: 15,
          paddingTop: 25,
          paddingBottom: 25,
        }}>
        <Card transparent style={{width: width / 1.1}}>
          <CardItem
            cardBody
            style={{
              width: width / 1.1,
              height: height / 2.9,
            }}>
            <AsyncImage
              style={{
                width: width / 1.1,
                height: height / 2.9,
                aspectRatio: width / 1.1 / (height / 2.9),
              }}
              source={charCar}
              placeholderColor={newContainerColor}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              resizeMode="cover"
            />
          </CardItem>
          <CardItem
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: width / 1.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: book,
                  color: black,
                  fontSize: RFPercentage(1.8),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                  lineHeight: 18,
                }}>
                {message}
              </Text>
            </View>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(EpmtyTabScreen);

export default props => <Wrapper {...props} />;
