import React from 'react';
import {Text, View, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {FontType} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Button} from 'native-base';

const {medium} = FontType;
const {white, black, mainGreen} = Colors;
const {width, height} = Dimensions.get('window');

const LeftSide = props => {
  const {title, tag, onPress, color} = props;
  return (
    <LinearGradient
      start={{x: 0.0, y: 0.0}}
      end={{x: 0.66, y: 0.0}}
      locations={[0.75, 0.99]}
      colors={[color, 'rgba(255, 255, 255, 0.1)']}
      style={{
        flex: 1,
        zIndex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderRadius: 4,
      }}>
      <View
        style={{
          flex: 1,
          padding: 15,
          paddingTop: 20,
          height: '100%',
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
        }}>
        <View
          style={{
            flexDirection: 'column',
            width: '100%',
            paddingRight: 10,
          }}>
          <Text
            style={{
              marginBottom: 5,
              fontFamily: medium,
              fontSize: RFPercentage(1.9),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {title}
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.5),
              color: '#999999',
              letterSpacing: 0.3,
              lineHeight: 15,
            }}>
            {tag}
          </Text>
        </View>
        <Button
          onPress={onPress}
          style={{
            width: width / 4,
            height: height / 25,
            backgroundColor: mainGreen,
            borderRadius: 0,
            elevation: 0,
            shadowOpacity: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: white,
              letterSpacing: 0.3,
            }}>
            Book Now
          </Text>
        </Button>
      </View>
      <View style={{flex: 1}} />
    </LinearGradient>
  );
};

const Wrapper = compose(withApollo)(LeftSide);

export default props => <Wrapper {...props} />;
