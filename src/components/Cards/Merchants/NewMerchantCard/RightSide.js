import React from 'react';
import {View, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsycImage from '../../../Image/AsyncImage';

const {width, height} = Dimensions.get('window');

export const RightSide = props => {
  const {source} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        flex: 1,
        position: 'absolute',
        backgroundColor: 'transparent',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 0,
      }}>
      {/* <View style={{flex: 1, height: '100%', width: '100%'}} /> */}
      <View style={{flex: 1, height: '100%', width: '100%'}}>
        <AsycImage
          source={source}
          style={{flex: 1, width: '100%', height: '100%', borderRadius: 4}}
          placeholderColor={'white'}
          loaderStyle={{width: width / 4.5, height: height / 7.5}}
          resizeMode="cover"
        />
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(RightSide);

export default props => <Wrapper {...props} />;
