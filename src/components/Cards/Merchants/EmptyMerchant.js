import React from 'react';
import {Text, View, Image, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import {charImage} from '../../../utils/Themes/Images';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {charNoCafe} = charImage;
const {black} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const cardWidth = width / 1.1;
const cardHeight = height / 1.46;

const EmptyMerchant = props => {
  console.log('Empty Merchant Props: ', props);
  return (
    <Card
      style={{
        width: cardWidth,
        height: cardHeight,
        marginLeft: 0,
        marginRight: 0,
        elevation: 0,
        shadowOpacity: 0,
        alignItems: 'center',
      }}>
      <Image
        source={charNoCafe}
        style={{
          bottom: 15,
          width: width / 1.2,
          height: height / 1.5,
          aspectRatio: width / 1.2 / (height / 1.5),
        }}
        resizeMode="contain"
      />
      <CardItem
        footer
        style={{
          position: 'absolute',
          bottom: 25,
          width: '100%',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Text
          style={{
            marginBottom: 10,
            textAlign: 'center',
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Coming Soon
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          We'll notify you when we launch.
        </Text>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(EmptyMerchant);

export default props => <Wrapper {...props} />;
