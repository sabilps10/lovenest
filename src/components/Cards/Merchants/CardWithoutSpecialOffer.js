import React, {useMemo} from 'react';
import {Text, View, Dimensions, Animated, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem, Card} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import AsyncImage from '../../Image/AsyncImage';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {greyLine, transparent, newContainerColor, black} = Colors;
const {book, medium} = FontType;

const cardWidth = Dimensions.get('window').width / 1.1;
const cardHeight = 245;

const imageWidth = Dimensions.get('window').width / 7.5;
const imageHeight = Dimensions.get('window').height / 16.5;

const AnimatedCard = Animated.createAnimatedComponent(Card);

const CardWithoutSpecialOffer = props => {
  const {
    id,
    logoSource,
    iconSource,
    name,
    tagLine,
    navigation,
    serviceType,
  } = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const CardMerchant = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('MerchantDetail', {serviceType, id});
        }}
        activeOpacity={1}
        onPressIn={pressIn}
        onPressOut={pressOut}>
        <AnimatedCard
          style={{
            minHeight: cardHeight,
            maxHeight: cardHeight,
            width: cardWidth,
            borderRadius: 4,
            borderWidth: 0.5,
            borderColor: greyLine,
            marginBottom: 15,
            transform: [{scale: scale}],
          }}>
          <CardItem cardBody style={{backgroundColor: transparent}}>
            <AsyncImage
              style={{
                flex: 1,
                width: '100%',
                height: 169.44,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}
              loaderStyle={{width: 60 / 1.5, height: 60 / 1.5}}
              source={logoSource}
              placeholderColor={newContainerColor}
            />
          </CardItem>
          <CardItem
            style={{
              width: '100%',
              flexDirection: 'row',
              flexWrap: 'wrap',
              backgroundColor: transparent,
            }}>
            <View style={{flex: 0.2}}>
              <View>
                <AsyncImage
                  style={{
                    aspectRatio: imageWidth / imageHeight,
                    width: imageWidth,
                    height: imageHeight,
                    borderWidth: 0.5,
                    borderColor: greyLine,
                  }}
                  loaderStyle={{width: 43 / 1.5, height: 45 / 1.5}}
                  source={iconSource}
                  placeholderColor={newContainerColor}
                />
              </View>
            </View>
            <View
              style={{
                flex: 1,
                paddingLeft: 10,
                flexDirection: 'row',
                flexWrap: 'wrap',
                height: '100%',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 25,
                  }}>
                  {name}
                </Text>
                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 25,
                  }}>
                  {tagLine}
                </Text>
              </View>
            </View>
          </CardItem>
        </AnimatedCard>
      </TouchableOpacity>
    );
  };

  if (logoSource && iconSource && name && tagLine) {
    return CardMerchant();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(CardWithoutSpecialOffer);

export default props => <Wrapper {...props} />;
