import React, {useMemo} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Card, CardItem, Button, Icon} from 'native-base';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import moment from 'moment';
import Dash from 'react-native-dash';
import {charImage} from '../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');
const {charHandBag} = charImage;
const {small, regular} = FontSize;
const {book, medium, bold} = FontType;
const {black, brownCream, greyLine, mainRed, mainGreen} = Colors;

export const ImageProductRental = props => {
  const {inventory} = props;
  console.log('Inventory: ', inventory);
  const {product} = inventory;
  const {featuredImageDynamicURL, featuredImageURL} = product;

  if (
    featuredImageDynamicURL === null ||
    (featuredImageDynamicURL === undefined &&
      featuredImageURL === null &&
      featuredImageURL === undefined)
  ) {
    return null;
  } else {
    if (
      featuredImageDynamicURL === null ||
      featuredImageDynamicURL === undefined
    ) {
      return (
        <Image
          source={{uri: featuredImageURL}}
          style={{flex: 1, width: null, height: 168}}
        />
      );
    } else {
      return (
        <Image
          source={{uri: featuredImageDynamicURL}}
          style={{flex: 1, width: null, height: 168}}
        />
      );
    }
  }
};

export const ProductRentals = props => {
  const {productRentals} = props;

  if (productRentals === null || productRentals === undefined) {
    return null;
  } else {
    if (productRentals.length === 0) {
      return null;
    } else {
      return (
        <FlatList
          data={productRentals}
          keyExtractor={(item, index) => `${index}ProductRentals`}
          renderItem={({item}) => {
            return (
              <View
                style={{
                  width: '100%',
                  backgroundColor: 'transparent',
                  padding: 17,
                }}>
                <View
                  style={{
                    width: '99%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    minHeight: 169,
                    maxHeight: 169,
                  }}>
                  <View
                    style={{
                      minHeight: 169,
                      maxHeight: 169,
                      minWidth: 111,
                      maxWidth: 111,
                      borderWidth: 0.5,
                      borderColor: greyLine,
                      backgroundColor: greyLine,
                    }}>
                    <ImageProductRental inventory={item.inventory} />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      minHeight: 169,
                      maxHeight: 169,
                      padding: 10,
                      flexDirection: 'column',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'column'}}>
                      <Text
                        style={{
                          fontFamily: bold,
                          fontSize: small,
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: small,
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item.inventory.product.name}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'column'}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: small,
                          color: greyLine,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        Rental Date
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: small,
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>{`${moment(item.startDate).format(
                        'DD MMMM YYYY',
                      )} - ${moment(item.endDate).format(
                        'DD MMMM YYYY',
                      )}`}</Text>
                    </View>
                    <View style={{flexDirection: 'column', bottom: 0}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: small,
                          color: greyLine,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        Qty
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: small,
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item.quantity}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            );
          }}
        />
      );
    }
  }
};

export const ItemList = props => {
  const {itemLists} = props;

  if (!itemLists || itemLists?.length === 0) {
    return null;
  } else {
    return (
      <FlatList
        data={itemLists}
        extraData={itemLists}
        keyExtractor={(item, index) => `${index}ItemLists`}
        renderItem={({item, index}) => {
          console.log('Super Item For Product rental: ', item);
          if (item?.packageItemName.toLowerCase() === 'photos & albums') {
            return (
              <>
                <CardItem
                  style={{
                    flexDirection: 'column',
                    backgroundColor: 'transparent',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexWrap: 'wrap',
                    }}>
                    <View style={{flexBasis: '48%'}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        {item.packageItemName}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexBasis: '48%',
                        height: '100%',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        Qty: {item.quantity}
                      </Text>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item.description.replace(/<br\s*[\/]?>/gi, '\n')}
                  </Text>
                </CardItem>
                <CardItem>
                  <TouchableOpacity
                    onPress={() => {
                      props?.navigation.navigate('PhotoAlbumProgress', {
                        orderId: props?.orderDetail?.id,
                        itemId: item?.id,
                      });
                    }}
                    style={{
                      padding: 10,
                      backgroundColor: mainGreen,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: 'white',
                        letterSpacing: 0.3,
                      }}>
                      See the progress
                    </Text>
                  </TouchableOpacity>
                </CardItem>
                {item.productRentals === undefined ||
                item.productRentals === null ? null : item.productRentals
                    .length === undefined ? null : item.productRentals
                    .length === 0 ? null : (
                  <>
                    <CardItem
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                      }}>
                      <View style={{paddingRight: 10}}>
                        <View
                          style={{
                            backgroundColor: 'white',
                            borderRadius: 25,
                            paddingLeft: 10,
                            paddingRight: 10,
                            paddingTop: 7,
                            paddingBottom: 7,
                            borderWidth: 1,
                            borderColor: mainRed,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontFamily: medium,
                              fontSize: small,
                              color: mainRed,
                              letterSpacing: 0.3,
                            }}>
                            {item.packageItemName} Product(s)
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          borderBottomWidth: 1,
                          borderBottomColor: mainRed,
                        }}
                      />
                    </CardItem>
                    <ProductRentals productRentals={item.productRentals} />
                  </>
                )}
                {index === itemLists.length - 1 ? null : (
                  <CardItem>
                    <Dash
                      dashColor={greyLine}
                      dashThickness={0.5}
                      style={{
                        marginTop: 10,
                        width: '100%',
                        height: 1,
                      }}
                    />
                  </CardItem>
                )}
              </>
            );
          } else {
            return (
              <>
                <CardItem
                  style={{
                    flexDirection: 'column',
                    backgroundColor: 'transparent',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexWrap: 'wrap',
                    }}>
                    <View style={{flexBasis: '48%'}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        {item.packageItemName}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexBasis: '48%',
                        height: '100%',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        Qty: {item.quantity}
                      </Text>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item.description.replace(/<br\s*[\/]?>/gi, '\n')}
                  </Text>
                </CardItem>
                {item.productRentals === undefined ||
                item.productRentals === null ? null : item.productRentals
                    .length === undefined ? null : item.productRentals
                    .length === 0 ? null : (
                  <>
                    <CardItem
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                      }}>
                      <View style={{paddingRight: 10}}>
                        <View
                          style={{
                            backgroundColor: 'white',
                            borderRadius: 25,
                            paddingLeft: 10,
                            paddingRight: 10,
                            paddingTop: 7,
                            paddingBottom: 7,
                            borderWidth: 1,
                            borderColor: mainRed,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontFamily: medium,
                              fontSize: small,
                              color: mainRed,
                              letterSpacing: 0.3,
                            }}>
                            {item.packageItemName} Product(s)
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          borderBottomWidth: 1,
                          borderBottomColor: mainRed,
                        }}
                      />
                    </CardItem>
                    <ProductRentals productRentals={item.productRentals} />
                  </>
                )}
                {index === itemLists.length - 1 ? null : (
                  <CardItem>
                    <Dash
                      dashColor={greyLine}
                      dashThickness={0.5}
                      style={{
                        marginTop: 10,
                        width: '100%',
                        height: 1,
                      }}
                    />
                  </CardItem>
                )}
              </>
            );
          }
        }}
      />
    );
  }
};

export const PackageLists = props => {
  const {packageLists, setCollapsePackageList} = props;
  return (
    <FlatList
      data={packageLists ? (packageLists?.length > 0 ? packageLists : []) : []}
      extraData={
        packageLists ? (packageLists?.length > 0 ? packageLists : []) : []
      }
      keyExtractor={(item, index) => `${index}packageListSide`}
      renderItem={({item, index}) => {
        return (
          <Card
            style={{
              marginTop: 0,
              marginBottom: 15,
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
              borderBottomColor: greyLine,
              borderBottomWidth: 0.5,
              borderRadius: 4,
            }}>
            <CardItem
              style={{
                width: '100%',
                backgroundColor: 'transparent',
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <View
                  style={{
                    flex: 0.26,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Image
                    source={charHandBag}
                    style={{width: width / 8, height: height / 15}}
                    resizeMode="contain"
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: '#999999',
                      letterSpacing: 0.3,
                    }}>
                    PACKAGE
                  </Text>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: bold,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item.name}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0.2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    onPress={() => {
                      // set collapse
                      setCollapsePackageList(index);
                    }}
                    transparent
                    style={{
                      backgroundColor: 'transparent',
                      alignSelf: 'center',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name={item.collapse ? 'chevron-up' : 'chevron-down'}
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 24,
                        color: '#757575',
                      }}
                    />
                  </Button>
                </View>
              </View>
            </CardItem>
            {item.collapse ? (
              <CardItem>
                <Dash
                  dashColor={greyLine}
                  dashThickness={0.8}
                  style={{width: '100%', height: 0.5}}
                />
              </CardItem>
            ) : null}
            {item.collapse ? (
              <ItemList
                orderDetail={props?.orderDetail}
                itemLists={item.itemLists}
                navigation={props?.navigation}
              />
            ) : null}
          </Card>
        );
      }}
    />
  );
};

const OrderPackageList = props => {
  const {orderProps, setCollapsePackageList, singlePreview} = props;
  console.log('OrderPackageList Props: ', orderProps);
  if (orderProps?.packageLists?.length > 0) {
    const {packageLists} = orderProps;
    const RenderList = () => {
      return useMemo(() => {
        return (
          <Card
            transparent
            style={{
              marginLeft: 15,
              marginRight: 15,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <CardItem style={{paddingLeft: 0, paddingRight: 0}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: singlePreview
                    ? RFPercentage(1.7)
                    : RFPercentage(1.8),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                {singlePreview ? 'GOWN/SUIT PACKAGE' : 'ORDER LIST(S)'}
              </Text>
            </CardItem>
            <PackageLists
              orderDetail={props?.orderProps}
              navigation={props?.navigation}
              packageLists={packageLists}
              setCollapsePackageList={index => setCollapsePackageList(index)}
            />
          </Card>
        );
      }, [orderProps, setCollapsePackageList]);
    };

    return RenderList();
  } else {
    return null;
  }
};

export default OrderPackageList;
