import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Card, CardItem} from 'native-base';
import CardColors from '../../../utils/Themes/AppointmentCardColors';
import {appIcon} from '../../../utils/Themes/Images';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import AsyncImage from '../../Image/AsyncImage';

const {
  appIconReschedule,
  appIconApproved,
  appIconHavenBook,
  appIconSent,
} = appIcon;
const {black, white, newContainerColor} = Colors;
const {small, regular} = FontSize;
const {medium, bold, book} = FontType;

const {
  yellow,
  yellowBold,
  red,
  redBold,
  grey,
  greyBold,
  green,
  greenBold,
} = CardColors;

/* Props
  cardType,
  label,
  activity,
  body,
  footer,
  onPress
*/

const SmallCard = props => {
  const {cardType, label, activity, body, footer, onPress} = props;

  let [type, setType] = useState('');
  let [labelText, setLabel] = useState('');
  let [activityText, setActivity] = useState('');
  let [bodyText, setBody] = useState('');
  let [footerText, setFooter] = useState('');
  let [colorHeader, setColorHeader] = useState(newContainerColor);
  let [colorActivity, setColorActivity] = useState(newContainerColor);
  let [iconHeader, setIconHeader] = useState(newContainerColor);

  useEffect(() => {
    settingType(cardType);
    settingColor(cardType);
    settingLabel(label);
    settingActivity(activity);
    settingBody(body);
    settingFooter(footer);
  }, []);

  const settingType = value => {
    type = value;
    setType(type);
  };

  const settingLabel = value => {
    labelText = value;
    setLabel(labelText);
  };

  const settingActivity = value => {
    activityText = value;
    setActivity(activityText);
  };

  const settingBody = value => {
    bodyText = value;
    setBody(bodyText);
  };

  const settingFooter = value => {
    footerText = value;
    setFooter(footerText);
  };

  const settingColor = value => {
    if (value === "Haven't Book") {
      colorHeader = yellow;
      setColorHeader(colorHeader);

      colorActivity = yellowBold;
      setColorActivity(colorActivity);

      iconHeader = appIconHavenBook;
      setIconHeader(iconHeader);
    } else if (value === 'Sent') {
      colorHeader = grey;
      setColorHeader(colorHeader);

      colorActivity = greyBold;
      setColorActivity(colorActivity);

      iconHeader = appIconSent;
      setIconHeader(iconHeader);
    } else if (value === 'Reschedule') {
      colorHeader = red;
      setColorHeader(colorHeader);

      colorActivity = redBold;
      setColorActivity(colorActivity);

      iconHeader = appIconReschedule;
      setIconHeader(iconHeader);
    } else {
      colorHeader = green;
      setColorHeader(colorHeader);

      colorActivity = greenBold;
      setColorActivity(colorActivity);

      iconHeader = appIconApproved;
      setIconHeader(iconHeader);
    }
  };

  return (
    <>
      <TouchableOpacity
        onPress={() => onPress()}
        style={{
          borderWidth: 1,
          minHeight: 200,
          maxHeight: 200,
          minWidth: '47.5%',
          maxWidth: '47.5%',
          borderColor: '#E7E7E7',
        }}>
        <View style={{flexDirection: 'column', width: '100%', height: '100%'}}>
          <View
            style={{
              minHeight: 50,
              maxHeight: 50,
              width: '100%',
              backgroundColor: colorHeader,
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <AsyncImage
              style={styles.iconType}
              loaderStyle={{width: 24 / 2, height: 24 / 2}}
              source={iconHeader}
              placeholderColor={newContainerColor}
            />
            <Text style={{left: 10, color: colorActivity, ...styles.labelText}}>
              {labelText}
            </Text>
          </View>
          <View style={{width: '100%', flexDirection: 'column', padding: 8}}>
            <View
              style={{
                ...styles.viewActivity,
                backgroundColor: colorActivity,
              }}>
              <Text style={styles.activityText}>{activityText}</Text>
            </View>
            <View style={styles.viewBody}>
              <Text style={styles.bodyText}>{bodyText}</Text>
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              bottom: 5,
              width: '100%',
              justifyContent: 'center',
              paddingLeft: 8,
              paddingRight: 8,
            }}>
            <Text style={styles.footerText}>With {footerText}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  labelText: {
    fontFamily: medium,
    fontSize: small,
    lineHeight: 18,
    letterSpacing: 0.26,
  },
  activityText: {
    fontFamily: bold,
    fontSize: small,
    color: white,
    lineHeight: 18,
    letterSpacing: 0.26,
  },
  bodyText: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  footerText: {
    fontFamily: book,
    fontSize: small,
    color: black,
    lineHeight: 15,
    letterSpacing: 0.2,
  },
  touch: {
    flexWrap: 'wrap',
    width: '48%',
    height: 200,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 15,
  },
  card: {
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    width: '100%',
    height: '99%',
    borderWidth: 1,
    elevation: 0,
    shadowOpacity: 0,
  },
  cardItemHeader: {
    width: '100%',
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'row',
  },
  iconType: {width: 24, height: 24},
  cardItemActivityAndbody: {
    paddingLeft: 7,
    paddingRight: 7,
    flexDirection: 'column',
    paddingTop: 9,
  },
  viewActivity: {
    minHeight: 18,
    width: '100%',
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 3,
    paddingBottom: 3,
  },
  viewBody: {
    width: '100%',
    paddingTop: 10,
    paddingLeft: 3,
    paddingRight: 3,
  },
  cardItemFooter: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 30,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default SmallCard;
