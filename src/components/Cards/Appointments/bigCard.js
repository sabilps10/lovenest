import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import {Card, CardItem, Left, Body} from 'native-base';
import CardColors from '../../../utils/Themes/AppointmentCardColors';
import Colors from '../../../utils/Themes/Colors';
import {appIcon} from '../../../utils/Themes/Images';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import AsyncImage from '../../Image/AsyncImage';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {
  black,
  newContainerColor,
  white,
  offWhite,
  greyLine,
  superGrey,
  overlayRed,
  mainRed,
} = Colors;
const {small, regular, large} = FontSize;
const {bold, medium, book, bookItalic} = FontType;
const {width, height} = Dimensions.get('window');
const {
  yellow,
  yellowBold,
  red,
  redBold,
  grey,
  greyBold,
  green,
  greenBold,
} = CardColors;
const {
  appIconReschedule,
  appIconApproved,
  appIconHavenBook,
  appIconSent,
  rescheduledIcon,
  haventbookIcon,
  confirmedIcon,
  sentIcon,
} = appIcon;

const yellowMessage =
  'You have yet to book an appointment for this item. Tap the button below to begin.';
const greyMessage =
  'Your appointment request was sent. You will be updated for confirmation.';

const yellowStatus = "Haven't Book";
const redStatus = 'Reschedule';
const greyStatus = 'Sent';
const greenStatus = 'Confirmed';

/**
 *
 * @param {appointmentData, cardStatus} props
 */

const BigCard = props => {
  const {appointmentData, cardStatus} = props;

  let [isLoading, setIsLoading] = useState(true);
  let [appData, setAppData] = useState(null);
  let [status, setCardStatus] = useState(null);
  let [cardColor, setCardColor] = useState(newContainerColor);
  let [cardColorActivity, setCardColorActivity] = useState(newContainerColor);
  let [iconHeader, setIconHeader] = useState('');

  useEffect(() => {
    settingAppData(appointmentData);
    settingCardStatus(cardStatus);
    settingCardColor(cardStatus);
  }, [isLoading, cardStatus]);

  const settingIsLoading = () => {
    isLoading = false;
    setIsLoading(isLoading);
  };

  const settingAppData = value => {
    appData = value;
    setAppData(appData);
  };

  const settingCardStatus = value => {
    status = value;
    setCardStatus(status);
  };

  const settingCardColor = value => {
    console.log('VALUE COLOR: ', value);
    if (value === yellowStatus) {
      cardColor = yellow;
      setCardColor(cardColor);

      cardColorActivity = yellowBold;
      setCardColorActivity(cardColorActivity);

      iconHeader = haventbookIcon;
      setIconHeader(iconHeader);

      settingIsLoading();
    } else if (value === redStatus) {
      cardColor = red;
      setCardColor(cardColor);

      cardColorActivity = redBold;
      setCardColorActivity(cardColorActivity);

      iconHeader = rescheduledIcon;
      setIconHeader(iconHeader);
      settingIsLoading();
    } else if (value === greyStatus) {
      cardColor = grey;
      setCardColor(cardColor);

      cardColorActivity = greyBold;
      setCardColorActivity(cardColorActivity);

      iconHeader = sentIcon;
      setIconHeader(iconHeader);
      settingIsLoading();
    } else if (value === greenStatus) {
      cardColor = green;
      setCardColor(cardColor);

      cardColorActivity = greenBold;
      setCardColorActivity(cardColorActivity);

      iconHeader = confirmedIcon;
      setIconHeader(iconHeader);
      settingIsLoading();
    }
  };

  console.log('App Data : >>> ', appData);

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} />
    );
  }

  return (
    <Card transparent style={UpperCardStyle.container}>
      <CardItem style={{paddingTop: 0}}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            borderBottomColor: greyLine,
            borderBottomWidth: 0.5,
            paddingBottom: 5,
          }}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              {status} Appointment
            </Text>
          </View>
          <View
            style={{
              flex: 0.25,
              alignItems: 'flex-end',
              justifyContent: 'center',
            }}>
            <Image
              source={iconHeader}
              style={{top: -3, width: width / 7, height: height / 18}}
              resizeMode="contain"
            />
          </View>
        </View>
      </CardItem>
      <CardItem style={{paddingBottom: 0}}>
        <View style={{width: '100%', flexDirection: 'column'}}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.7),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            APPOINTMENT ACTIVITY
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginVertical: 5,
            }}>
            {appData?.activity ? appData.activity : 'N/A'}
          </Text>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.7),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            With {appData.merchantDetails.name}
          </Text>
        </View>
      </CardItem>
      {status === yellowStatus ? (
        <CardItem style={UpperCardStyle.message}>
          <View
            style={{
              backgroundColor: '#FAF8F4',
              flexDirection: 'row',
              padding: 10,
            }}>
            <Text>
              <Text
                style={{
                  ...UpperCardStyle.messageText,
                  color: mainRed,
                  fontStyle: 'italic',
                }}>
                You have yet to book an appointment for this item. Tap the
              </Text>
              <Text
                style={{
                  ...UpperCardStyle.messageText,
                  color: mainRed,
                  fontFamily: bold,
                }}>
                {' '}
                Book Appointment
              </Text>
              <Text
                style={{
                  ...UpperCardStyle.messageText,
                  color: mainRed,
                  fontStyle: 'italic',
                }}>
                {' '}
                button to begin
              </Text>
            </Text>
          </View>
        </CardItem>
      ) : status === greyStatus ? (
        <CardItem style={UpperCardStyle.message}>
          <Text style={{...UpperCardStyle.messageText, color: black}}>
            {greyMessage}
          </Text>
        </CardItem>
      ) : status === redStatus ? (
        <CardItem
          style={{...UpperCardStyle.message, marginTop: 0, paddingBottom: 15}}>
          <View
            style={{backgroundColor: overlayRed, padding: 10, width: '100%'}}>
            <Text>
              <Text
                style={{
                  ...UpperCardStyle.messageText,
                  color: mainRed,
                  fontStyle: 'italic',
                }}>
                The appointment request on{' '}
              </Text>
              <Text style={{fontFamily: bold, color: mainRed}}>
                {appData.date}
              </Text>
              <Text style={{fontFamily: bold, color: mainRed}}>
                , {appData.time}
              </Text>
              <Text
                style={{
                  ...UpperCardStyle.messageText,
                  color: mainRed,
                  fontStyle: 'italic',
                }}>
                , has been rejected for the reason below. Tap the button below
                to rebook another date.
              </Text>
            </Text>
          </View>
        </CardItem>
      ) : status === greenStatus ? (
        <CardItem style={{...UpperCardStyle.message, paddingBottom: 0}}>
          <View style={{width: '100%', flexDirection: 'column'}}>
            <Text
              style={{
                lineHeight: 25,
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: superGrey,
                letterSpacing: 0.3,
              }}>
              DETAILS
            </Text>
            <Text style={{...UpperCardStyle.messageText, color: black}}>
              {appData.date === null || appData.date === undefined
                ? ''
                : appData.date}{' '}
              |{' '}
              {appData.timeslot === undefined || appData.timeslot === null
                ? ''
                : appData.timeslot}{' '}
              {'\n'}
              <Text>
                at{' '}
                <Text
                  style={{fontWeight: 'bold', textDecorationLine: 'underline'}}>
                  {appData.serviceAddress === null ||
                  (appData.serviceAddress === undefined &&
                    appData.merchantDetails.address === null) ||
                  (appData.merchantDetails.address === undefined &&
                    appData.merchantDetails.addresses === null) ||
                  appData.merchantDetails.addresses === undefined ||
                  appData.merchantDetails.addresses.lenght === 0
                    ? 'City Gate, 371 Beach Road, #02-16, Singapore 199597'
                    : appData.serviceAddress !== null
                    ? appData.serviceAddress.address
                    : appData.merchantDetails.addresses === null ||
                      appData.merchantDetails.addresses === undefined ||
                      appData.merchantDetails.addresses.length === 0
                    ? appData.merchantDetails.address
                    : appData.merchantDetails.addresses[0].address}
                </Text>
              </Text>
              {'\n'}
              <Text>{`T: +${
                appData?.merchantDetails?.addresses?.length > 0
                  ? appData.merchantDetails.addresses[0].phone
                  : appData.merchantDetails.phone
              }`}</Text>
            </Text>
          </View>
        </CardItem>
      ) : null}
      {/* Reject or Reschedule Reason */}
      {status === redStatus ? (
        <>
          {appData?.rejectReason ? (
            appData.rejectReason === '' ? null : (
              <CardItem
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  paddingTop: 0,
                  paddingBottom: 15,
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 1,
                    marginBottom: 30,
                  }}
                />
                <View style={{width: '100%', marginBottom: 10}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: superGrey,
                      letterSpacing: 0.3,
                    }}>
                    REASON FOR RESCHEDULE
                  </Text>
                </View>
                <View style={{width: '100%'}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                      lineHeight: 20,
                    }}>
                    {appData?.rejectReason
                      ? appData.rejectReason === ''
                        ? ''
                        : appData.rejectReason
                      : ''}
                  </Text>
                </View>
              </CardItem>
            )
          ) : null}
        </>
      ) : null}
      {/* Service Request */}
      {appData.serviceRequest === undefined ||
      appData.serviceRequest === null ? null : (
        <CardItem
          footer
          style={{
            flexDirection: 'column',
            width: '100%',
            paddingTop: 0,
            paddingBottom: 0,
          }}>
          <View
            style={{
              width: '100%',
              height: 1,
              marginBottom: 30,
            }}
          />
          <View style={{width: '100%', marginBottom: 10}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: superGrey,
                letterSpacing: 0.3,
              }}>
              SERVICE REQUEST
            </Text>
          </View>
          <View style={{width: '100%'}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {appData.serviceRequest}
            </Text>
          </View>
        </CardItem>
      )}
      <CardItem style={{paddingTop: 0, paddingBottom: 0}}>
        <View
          style={{
            width: '100%',
            height: 1,
            borderBottomColor: greyLine,
            borderBottomWidth: 0.5,
          }}
        />
      </CardItem>
    </Card>
  );
};

const UpperCardStyle = StyleSheet.create({
  container: {marginLeft: 0, marginRight: 0, elevation: 0, shadowOpacity: 0},
  Cardheader: {width: '100%'},
  left: {flexBasis: '10%'},
  body: {flexBasis: '90%', justifyContent: 'center'},
  headerText: {
    fontFamily: medium,
    fontSize: regular,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  viewActivity: {minHeight: 25, justifyContent: 'center', padding: 7},
  textActivity: {
    fontFamily: bold,
    fontSize: regular,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  boldText: {
    fontFamily: medium,
    fontSize: large,
    lineHeight: 25,
    letterSpacing: 0.39,
  },
  cardItemMerchantName: {paddingTop: 0, paddingBottom: 0},
  merchantName: {
    fontFamily: book,
    fontSize: small,
    lineHeight: 25,
    letterSpacing: 0.26,
  },
  message: {paddingTop: 20, paddingBottom: 20},
  messageText: {
    fontFamily: book,
    fontSize: RFPercentage(1.8),
    lineHeight: 25,
    letterSpacing: 0.3,
  },
});

export default BigCard;
