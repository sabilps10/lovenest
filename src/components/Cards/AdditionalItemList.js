import React, {useMemo} from 'react';
import {Text, View, FlatList, Dimensions, Image} from 'react-native';
import {Card, CardItem, Button, Icon} from 'native-base';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import Dash from 'react-native-dash';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {charAdditionalItems} = charImage;
const {regular, exregular} = FontSize;
const {book, medium, bold} = FontType;
const {black, brownCream, greyLine} = Colors;

export const ItemList = props => {
  const {additionalItems, setCollapseAdditionalItem} = props;
  return (
    <FlatList
      data={additionalItems}
      keyExtractor={(item, index) => `${String(index)} Additional Items`}
      renderItem={({item, index}) => {
        return (
          <Card
            style={{
              marginTop: 0,
              marginBottom: 15,
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
              borderBottomColor: greyLine,
              borderBottomWidth: 0.5,
              borderRadius: 4,
            }}>
            <CardItem
              style={{
                width: '100%',
                backgroundColor: 'transparent',
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <View
                  style={{
                    flex: 0.26,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Image
                    source={charAdditionalItems}
                    style={{width: width / 8, height: height / 15}}
                    resizeMode="contain"
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: '#999999',
                      letterSpacing: 0.3,
                    }}>
                    ITEM
                  </Text>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: bold,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item.name}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0.2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    onPress={() => {
                      setCollapseAdditionalItem(index);
                    }}
                    transparent
                    style={{
                      backgroundColor: 'transparent',
                      alignSelf: 'center',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name={item.collapse ? 'chevron-up' : 'chevron-down'}
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 24,
                        color: '#757575',
                      }}
                    />
                  </Button>
                </View>
              </View>
            </CardItem>
            {/* Detail Items */}
            {item.collapse ? <DetailItems item={item} index={index} /> : null}
          </Card>
        );
      }}
    />
  );
};

export const DetailItems = props => {
  const {item} = props;
  return (
    <>
      <CardItem
        style={{flexDirection: 'column', backgroundColor: 'transparent'}}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexBasis: '100%',
              height: '100%',
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: black,
                letterSpacing: 0.3,
              }}>
              Qty:{' '}
              <Text style={{fontFamily: medium, color: black}}>
                {item.quantity}
              </Text>
            </Text>
          </View>
        </View>
      </CardItem>
      <CardItem style={{backgroundColor: 'transparent'}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
          }}>
          {item.description.replace(/<br\s*[\/]?>/gi, '\n')}
        </Text>
      </CardItem>
    </>
  );
};

const AdditionalItemList = props => {
  console.log('AdditionalItemList Props >>>> ', props);
  const {orderProps, setCollapseAdditionalItem, singlePreview} = props;
  const RenderList = () => {
    return useMemo(() => {
      return (
        <>
          {orderProps?.additionalItems?.length > 0 ? (
            <Card
              transparent
              style={{
                marginLeft: 15,
                marginRight: 15,
                shadowOpacity: 0,
                elevation: 0,
              }}>
              <CardItem
                style={{
                  backgroundColor: 'white',
                  paddingLeft: 0,
                  paddingRight: 0,
                }}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: singlePreview
                      ? RFPercentage(1.7)
                      : RFPercentage(1.7),
                    color: '#999999',
                    letterSpacing: 0.3,
                  }}>
                  ADDITIONAL ITEM(S)
                </Text>
              </CardItem>
              <ItemList
                additionalItems={orderProps.additionalItems}
                setCollapseAdditionalItem={e => setCollapseAdditionalItem(e)}
              />
            </Card>
          ) : null}
        </>
      );
    }, [orderProps, setCollapseAdditionalItem]);
  };

  if (orderProps?.additionalItems?.length > 0) {
    return RenderList();
  } else {
    return null;
  }
};

export default AdditionalItemList;
