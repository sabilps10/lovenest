import React, {useState} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Linking,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import GeoCodeWithLangLng from '../../../utils/GeocodeWithLatLng';
import {Card, CardItem, Icon} from 'native-base';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');
const {black, brownCream} = Colors;
const {medium, book} = FontType;
const {regular} = FontSize;

// Common Sceens
import Map from '../../../components/Map/MapWithNavigationButton';
import TitleEvent from '../../../components/Event/Item/TitleEvent';
import ScheduleEvent from '../../../components/Event/Item/ScheduleEvent';
import AddressEvent from '../../../components/Event/Item/AddressEvent';
import DescriptionEvent from '../../../components/Event/Item/DescriptionEvent';

const EventDetail = props => {
  console.log('EventDetail Props: ', props);
  const {orderDetailData} = props;

  const [seeMore, setSeeMore] = useState(false);
  const [descriptionLength, setDescriptionLength] = useState(0);

  if (orderDetailData === null || orderDetailData === undefined) {
    return null;
  } else {
    if (
      orderDetailData.eventDetails === null ||
      orderDetailData.eventDetails === undefined
    ) {
      return null;
    } else {
      const {eventDetails, ticketQuantity: attendee, total} = orderDetailData;
      const {
        name,
        latitude,
        longitude,
        date,
        endDate,
        startTime,
        endTime,
        venueName,
        country,
        description,
        address1,
        address2,
        postCode,
      } = eventDetails;

      // const startDateEvent = moment(new Date(date)).format('DD MMMM YYYY');
      // const endDateEvent = moment(new Date(endDate)).format('DD MMMM YYYY');
      // const checkingIsSameDate = moment(startDateEvent).isSame(endDateEvent);

      const startDateEvent = moment
        .utc(date)
        .local()
        .format('ddd, DD MMM YYYY');
      const endDateEvent = moment
        .utc(endDate)
        .local()
        .format('ddd, DD MMM YYYY');
      const isSameDate = moment(startDateEvent).local().isSame(endDateEvent);

      const startTimeEvent = moment.utc(startTime).local().format('hh:mm a');
      const endTimeEvent = moment.utc(endTime).local().format('hh:mm a');

      const openAddressToMap = async (lat, lng) => {
        try {
          const aspectRatio = width / height;
          const getDeltaData = await GeoCodeWithLangLng.getLocation(
            latitude,
            longitude,
          );
          console.log('getDeltaData: ', getDeltaData);

          if (getDeltaData) {
            const {viewport} = getDeltaData;
            const {northeast, southwest} = viewport;
            const {southlat} = southwest;
            const {northlat} = northeast;
            const latDelta = northlat - southlat;
            const lngDelta = latDelta * aspectRatio;
            const coordinate = {
              latitude,
              longitude,
              latitudeDelta: latDelta,
              longitudeDelta: lngDelta,
            };

            const url = Platform.select({
              ios: `https://www.google.com/maps/search/?api=1&query=${venueName}&center=${coordinate.latitude},${coordinate.longitude}`,
              android: `geo:${coordinate.latitude},${coordinate.longitude}?q=${venueName}`,
            });

            Linking.canOpenURL(url)
              .then(supported => {
                if (!supported) {
                  const browser_url = `https://www.google.de/maps/@${coordinate.latitude},${coordinate.longitude}?q=${venueName}`;
                  return Linking.openURL(browser_url);
                }
                return Linking.openURL(url);
              })
              .catch(err => console.log('error', err));
          }
        } catch (error) {
          console.log('Error get address event: ', error);
        }
      };

      return (
        <>
          <Card transparent style={{marginTop: -10}}>
            <CardItem>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.9),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                PAYMENT DETAILS
              </Text>
            </CardItem>
            <CardItem
              style={{
                paddingTop: 0,
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Total
              </Text>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                $ {total}
              </Text>
            </CardItem>
            <CardItem
              style={{
                paddingTop: 0,
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Attendee
              </Text>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                {attendee}
              </Text>
            </CardItem>
            {orderDetailData?.selectedTimeslot?.name ? (
              <CardItem
                style={{
                  paddingTop: 0,
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.9),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  Time Slot
                </Text>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.9),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  {orderDetailData?.selectedTimeslot?.name
                    ? orderDetailData?.selectedTimeslot?.name
                    : ''}
                </Text>
              </CardItem>
            ) : null}
          </Card>
          <Card
            style={{
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
              marginTop: 2,
            }}>
            <CardItem style={{paddingTop: 15, paddingBottom: 0}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.8),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                EVENT DETAILS
              </Text>
            </CardItem>
            <TitleEvent title={name} />
            <ScheduleEvent
              fit={true}
              isSameDate={isSameDate}
              startDateEvent={startDateEvent}
              endDateEvent={endDateEvent}
              startTimeEvent={startTimeEvent}
              endTimeEvent={endTimeEvent}
            />
            <AddressEvent
              fit={true}
              venueName={venueName}
              address1={address1}
              address2={address2}
              country={country}
              postCode={postCode}
            />
            <DescriptionEvent
              fit={true}
              descriptionLength={descriptionLength}
              description={description}
              seeMore={seeMore}
              setSeeMore={() => setSeeMore(!seeMore)}
              setDescriptionLength={length => setDescriptionLength(length)}
            />
            <Map
              withDirectionText={true}
              forMapAddresses={`${venueName}, ${country}`}
              onNavigate={() => openAddressToMap()}
            />
          </Card>
        </>
      );
    }
  }
};

const Wrapper = compose(withApollo)(EventDetail);

export default props => <Wrapper {...props} />;
