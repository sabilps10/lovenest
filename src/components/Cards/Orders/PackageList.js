import React from 'react';
import {Text, FlatList, View, Image, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Dash from 'react-native-dash';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';

const {black, darkGrey, greyLine} = Colors;
const {small, regular} = FontSize;
const {book, medium} = FontType;
const {charHandBag} = charImage;
const {width, height} = Dimensions.get('window');

const PackageList = props => {
  const {orderType, itemList} = props;
  if (orderType) {
    if (orderType === 'Bridal') {
      return (
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                PACKAGE
              </Text>
              <FlatList
                data={itemList?.packageLists ? itemList.packageLists : []}
                extraData={itemList?.packageLists ? itemList.packageLists : []}
                keyExtractor={(_, index) => `${String(index)} ParentBridal`}
                listKey={(_, index) => `${String(index)} ParentBridal`}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                        alignItems: 'center',
                        marginVertical: 2.5,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {item?.name ? item.name : ''}
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item?.totalItem
                          ? item.totalItem > 1
                            ? ` (${item.totalItem} Items)`
                            : ` (${item.totalItem} Item)`
                          : null}
                      </Text>
                    </View>
                  );
                }}
              />
            </View>
            <View style={{flex: 0.3}}>
              <Image
                source={charHandBag}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  width: width / 10,
                  height: height / 17,
                  aspectRatio: 1.2,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
      );
    } else if (orderType === 'Hotel') {
      return (
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                PACKAGE
              </Text>
              <FlatList
                data={itemList?.packageLists ? itemList.packageLists : []}
                extraData={itemList?.packageLists ? itemList.packageLists : []}
                keyExtractor={(_, index) => `${String(index)} ParentHotel`}
                listKey={(_, index) => `${String(index)} ParentHotel`}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                        alignItems: 'center',
                        marginVertical: 2.5,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {item?.name ? item.name : ''}
                      </Text>
                      {/* <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item?.totalItem
                          ? item.totalItem > 1
                            ? ` (${item.totalItem} Items)`
                            : ` (${item.totalItem} Item)`
                          : null}
                      </Text> */}
                    </View>
                  );
                }}
              />
            </View>
            <View style={{flex: 0.3}}>
              <Image
                source={charHandBag}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  width: width / 10,
                  height: height / 17,
                  aspectRatio: 1.2,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
      );
    } else if (orderType === 'Venue') {
      return (
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                PACKAGE
              </Text>
              <FlatList
                data={itemList?.packageLists ? itemList.packageLists : []}
                extraData={itemList?.packageLists ? itemList.packageLists : []}
                keyExtractor={(_, index) => `${String(index)} ParentVenue`}
                listKey={(_, index) => `${String(index)} ParentVenue`}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                        alignItems: 'center',
                        marginVertical: 2.5,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {item?.name ? item.name : ''}
                      </Text>
                      {/* <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item?.totalItem
                          ? item.totalItem > 1
                            ? ` (${item.totalItem} Items)`
                            : ` (${item.totalItem} Item)`
                          : null}
                      </Text> */}
                    </View>
                  );
                }}
              />
            </View>
            <View style={{flex: 0.3}}>
              <Image
                source={charHandBag}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  width: width / 10,
                  height: height / 17,
                  aspectRatio: 1.2,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
      );
    } else if (orderType === 'Event') {
      return (
        <CardItem
          style={{
            paddingTop: 0,
            backgroundColor: 'transparent',
          }}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                ITEM
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: 2.5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  {itemList?.name ? itemList.name : ''}
                </Text>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.6),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  {itemList?.ticketQuantity
                    ? itemList.ticketQuantity > 1
                      ? ` (${itemList.ticketQuantity} Items)`
                      : ` (${itemList.ticketQuantity} Item)`
                    : null}
                </Text>
              </View>
            </View>
            <View style={{flex: 0.3}}>
              <Image
                source={charHandBag}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  width: width / 10,
                  height: height / 17,
                  aspectRatio: 1.2,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
      );
    } else if (orderType === 'Florist') {
      // console.log('ITEM LIST FLORIST: ', itemList);
      if (itemList?.orderStatus === 'Cart') {
        // this to hide order florist white status still in chart (no paid yet)
        return null;
      } else {
        return (
          <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
            <View
              style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: '#999999',
                    letterSpacing: 0.3,
                    lineHeight: 25,
                  }}>
                  ITEM(s)
                </Text>
                <FlatList
                  data={itemList?.productsOrder ? itemList.productsOrder : []}
                  extraData={
                    itemList?.productsOrder ? itemList.productsOrder : []
                  }
                  keyExtractor={(_, index) => `${String(index)} ParentInterior`}
                  listKey={(_, index) => `${String(index)} ParentInterior`}
                  renderItem={({item, index}) => {
                    return (
                      <View
                        style={{
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                          width: '100%',
                          alignItems: 'center',
                          marginVertical: 2.5,
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.6),
                            color: black,
                            letterSpacing: 0.3,
                            lineHeight: 18,
                          }}>
                          {item?.name ? item.name : ''}
                        </Text>
                        <Text
                          style={{
                            fontFamily: book,
                            fontSize: RFPercentage(1.6),
                            color: black,
                            letterSpacing: 0.3,
                            lineHeight: 18,
                          }}>
                          {item?.orderQty
                            ? item.orderQty > 1
                              ? ` (${item.orderQty} Items)`
                              : ` (${item.orderQty} Item)`
                            : null}
                        </Text>
                      </View>
                    );
                  }}
                />
              </View>
              <View style={{flex: 0.3}}>
                <Image
                  source={charHandBag}
                  style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    width: width / 10,
                    height: height / 17,
                    aspectRatio: 1.2,
                  }}
                  resizeMode="contain"
                />
              </View>
            </View>
          </CardItem>
        );
      }
    } else {
      return (
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: '#999999',
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                PACKAGE
              </Text>
              <FlatList
                data={
                  itemList?.interiorPackageList
                    ? itemList.interiorPackageList
                    : []
                }
                extraData={
                  itemList?.interiorPackageList
                    ? itemList.interiorPackageList
                    : []
                }
                keyExtractor={(_, index) => `${String(index)} ParentInterior`}
                listKey={(_, index) => `${String(index)} ParentInterior`}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        width: '100%',
                        alignItems: 'center',
                        marginVertical: 2.5,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {item?.name ? item.name : ''}
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.6),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item?.totalItem
                          ? item.totalItem > 1
                            ? ` (${item.totalItem} Items)`
                            : ` (${item.totalItem} Item)`
                          : null}
                      </Text>
                    </View>
                  );
                }}
              />
            </View>
            <View style={{flex: 0.3}}>
              <Image
                source={charHandBag}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  width: width / 10,
                  height: height / 17,
                  aspectRatio: 1.2,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
        </CardItem>
      );
    }
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(PackageList);

export default props => <Wrapper {...props} />;
