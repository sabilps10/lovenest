import React from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import moment from 'moment';
import AsyncImage from '../../Image/AsyncImage';
import {commonImage} from '../../../utils/Themes/Images';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import PackageList from './PackageList';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, greyLine, newContainerColor} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;
const {lnLogoNotif} = commonImage;

export const Spacer = () => {
  return (
    <CardItem style={{width: '100%', paddingTop: 0}}>
      <View
        style={{
          width: '100%',
          height: 1,
          borderBottomWidth: 0.5,
          borderBottomColor: greyLine,
        }}
      />
    </CardItem>
  );
};

const OrderList = props => {
  const {orderListItem, navigation} = props;
  return (
    <FlatList
      extraData={orderListItem}
      data={orderListItem}
      keyExtractor={(item, index) => `${String(index)} Order Item 2`}
      listKey={(item, index) => `${String(index)} Order Item 2`}
      renderItem={({item}) => {
        console.log('ITEM ORDER LIST: ', item);
        if (item?.orderType === 'Florist' && item?.orderStatus === 'Cart') {
          return null;
        } else {
          return (
            <>
              <TouchableOpacity
                activeOpacity={0.3}
                onPress={() => {
                  console.log('OrderList ITEM ID: ', item.id);
                  console.log('PROPS ANU: ', props);
                  navigation.navigate('OrderDetail', {
                    id: item.id,
                    successRegisterEvent: false,
                  });
                }}>
                <Card
                  style={{
                    marginLeft: 15,
                    marginRight: 15,
                    shadowOpacity: 0,
                    elevation: 0,
                    borderRadius: 4,
                  }}>
                  <HeaderCard item={item} />
                  <Spacer />
                  <PackageList orderType={item.orderType} itemList={item} />
                </Card>
              </TouchableOpacity>
            </>
          );
        }
      }}
    />
  );
};

export const HeaderCard = props => {
  const {item} = props;
  return (
    <CardItem
      style={{
        backgroundColor: 'transparent',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
      }}>
      <View style={{flexDirection: 'row', flexWrap: 'wrap', flexBasis: '50%'}}>
        <View
          style={{
            borderRightColor: greyLine,
            borderRightWidth:
              item?.orderType === 'Event'
                ? 0
                : item?.orderType?.length > 14
                ? 0
                : 1,
            paddingRight: 5,
            marginRight: 5,
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: '#757575',
              lineHeight: 20,
            }}>
            {item?.orderType ? item.orderType : ''}
          </Text>
        </View>
        <View style={{flexWrap: 'wrap'}}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: '#757575',
              lineHeight: 20,
            }}>
            {item?.orderType === 'Florist'
              ? 'Lovenest'
              : item?.merchant?.name
              ? item.merchant.name
              : ''}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexBasis: '50%',
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingTop: 5,
          paddingBottom: 5,
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: '#757575',
            lineHeight: 20,
          }}>
          {item?.number ? item.number : ''}
        </Text>
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(OrderList);

export default props => <Wrapper {...props} />;
