import React, {useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import {charImage} from '../../../utils/Themes/Images';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import Dash from 'react-native-dash';
import AsyncImage from '../../../components/Image/AsyncImage';

const {black, greyLine, mainRed} = Colors;
const {book, medium, bold, small} = FontType;
const {width, height} = Dimensions.get('window');
const {charHandBag} = charImage;

const InteriorPackageList = props => {
  console.log('InteriorPackageList Props: ', props);
  const {orderDetailData, setCollapseInteriorPackageList} = props;

  const RenderList = () => {
    return useMemo(() => {
      return (
        <>
          {orderDetailData?.interiorPackageList?.length > 0 ? (
            <Card
              transparent
              style={{
                marginLeft: 15,
                marginRight: 15,
                elevation: 0,
                shadowOpacity: 0,
              }}>
              <CardItem style={{paddingLeft: 0, paddingRight: 0}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.8),
                    color: '#999999',
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  ORDER LIST(S)
                </Text>
              </CardItem>
              <PackageList
                {...props}
                packageList={orderDetailData}
                setCollapseInteriorPackageList={e =>
                  setCollapseInteriorPackageList(e)
                }
              />
            </Card>
          ) : null}
        </>
      );
    }, [orderDetailData, setCollapseInteriorPackageList]);
  };

  if (orderDetailData) {
    if (orderDetailData?.interiorPackageList) {
      return RenderList();
    } else {
      return null;
    }
  } else {
    return null;
  }
};

export const PackageList = props => {
  const {packageList, setCollapseInteriorPackageList} = props;
  const keyExt = (item, index) => {
    return `${item.id} Parent`;
  };
  return (
    <FlatList
      data={
        packageList?.interiorPackageList?.length > 0
          ? packageList.interiorPackageList
          : []
      }
      extra={
        packageList?.interiorPackageList?.length > 0
          ? packageList.interiorPackageList
          : []
      }
      keyExtractor={keyExt}
      listKey={keyExt}
      renderItem={({item, index}) => {
        return (
          <Card
            style={{
              marginTop: 0,
              marginBottom: 15,
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
              borderBottomColor: greyLine,
              borderBottomWidth: 0.5,
              borderRadius: 4,
            }}>
            <CardItem
              style={{
                width: '100%',
                backgroundColor: 'transparent',
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <View
                  style={{
                    flex: 0.26,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Image
                    source={charHandBag}
                    style={{width: width / 8, height: height / 15}}
                    resizeMode="contain"
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: book,
                      fontSize: RFPercentage(1.7),
                      color: '#999999',
                      letterSpacing: 0.3,
                    }}>
                    PACKAGE
                  </Text>
                  <Text
                    style={{
                      lineHeight: 20,
                      fontFamily: bold,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item.name}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0.2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    onPress={() => {
                      // set collapse
                      setCollapseInteriorPackageList(index);
                    }}
                    transparent
                    style={{
                      backgroundColor: 'transparent',
                      alignSelf: 'center',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name={item.collapse ? 'chevron-up' : 'chevron-down'}
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 24,
                        color: '#757575',
                      }}
                    />
                  </Button>
                </View>
              </View>
            </CardItem>
            {item.collapse ? (
              <ItemList
                {...props}
                interiorProduct={
                  item?.interiorOrderProducts?.length > 0
                    ? item.interiorOrderProducts
                    : []
                }
                itemLists={item?.itemLists?.length > 0 ? item.itemLists : []}
              />
            ) : null}
          </Card>
        );
      }}
    />
  );
};

export const ItemList = props => {
  const {itemLists, interiorProduct} = props;
  const keyExt = (item, index) => `${item.id} Child Item Lists`;
  if (itemLists?.length > 0) {
    return (
      <FlatList
        data={itemLists}
        extraData={itemLists}
        keyExtractor={keyExt}
        listKey={keyExt}
        renderItem={({item, index}) => {
          const {packageItemDescription} = item;
          return (
            <>
              <CardItem>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: '#999999',
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  Description
                </Text>
              </CardItem>
              <CardItem style={{paddingTop: 0}}>
                <Text style={{fontFamily: medium, fontSize: RFPercentage(1.7), color: black, letterSpacing: 0.3, lineHeight: 18}}>{packageItemDescription}</Text>
              </CardItem>
              <InteriorProductList
                {...props}
                product={interiorProduct}
                itemLists={itemLists}
              />
              {index === itemLists.length - 1 ? null : (
                <CardItem>
                  <Dash
                    dashThickness={0.5}
                    dashColor={greyLine}
                    style={{width: '100%', height: 1}}
                  />
                </CardItem>
              )}
            </>
          );
        }}
      />
    );
  } else {
    return null;
  }
};

export const InteriorProductList = props => {
  const {product} = props;
  const keyExt = (item, index) => `${item.id} Item Product Interior`;
  if (product?.length > 0) {
    return (
      <Card transparent style={{marginLeft: 0, marginRight: 0}}>
        <CardItem
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
          }}>
          <View style={{paddingRight: 10}}>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 25,
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 7,
                paddingBottom: 7,
                borderWidth: 1,
                borderColor: mainRed,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                Product(s)
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              borderBottomWidth: 1,
              borderBottomColor: mainRed,
            }}
          />
        </CardItem>
        <FlatList
          data={product}
          extraData={product}
          listKey={keyExt}
          keyExtractor={keyExt}
          renderItem={({item, index}) => {
            return (
              <>
                <CardItem>
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {item?.name ? item.name : 'N/A'}
                      </Text>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.7),
                          color: black,
                          letterSpacing: 0.3,
                          marginTop: 5,
                        }}>
                        Qty:{' '}
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.7),
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {item?.quantity ? item.quantity : 'N/A'}
                        </Text>
                      </Text>
                    </View>
                    {item?.attachment ? (
                      <View
                        style={{
                          flex: 0.8,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                          alignItems: 'flex-start',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            const source = item?.attachment?.dynamicUrl
                              ? item.attachment.dynamicUrl
                              : item.attachment.url;
                            props.navigation.navigate('ProductAttachment', {
                              images: [{url: source}],
                            });
                          }}
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-start',
                          }}>
                          <Text
                            style={{
                              fontFamily: medium,
                              fontSize: RFPercentage(1.5),
                              color: mainRed,
                              letterSpacing: 0.3,
                              lineHeight: 20,
                            }}>
                            See Attachment
                          </Text>
                          <Icon
                            type="Feather"
                            name="arrow-right"
                            style={{fontSize: 24, color: mainRed}}
                          />
                        </TouchableOpacity>
                      </View>
                    ) : null}
                  </View>
                </CardItem>
              </>
            );
          }}
        />
      </Card>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(InteriorPackageList);

export default props => <Wrapper {...props} />;
