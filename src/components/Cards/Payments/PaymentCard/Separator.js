import React from 'react';
import {View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';

const {greyLine} = Colors;

const Separator = () => {
  return (
    <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
      <View
        style={{
          width: '100%',
          height: 1,
          borderBottomWidth: 0.5,
          borderBottomColor: greyLine,
        }}
      />
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(Separator);

export default props => <Wrapper {...props} />;
