import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {CardItem, Icon} from 'native-base';

const {mainRed} = Colors;
const {medium} = FontType;

const SmallButtonMakePayment = props => {
  console.log('SmallButtonMakePayment Props: ', props);
  const {onPress} = props;
  return (
    <CardItem
      style={{
        backgroundColor: 'transparent',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
      }}>
      <TouchableOpacity onPress={() => onPress()}>
        <View
          style={{
            padding: 5,
            borderWidth: 1,
            borderColor: mainRed,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              marginLeft: 5,
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: mainRed,
              letterSpacing: 0,
              lineHeight: 20,
            }}>
            Make Payment
          </Text>
          <View
            style={{
              marginHorizontal: 5,
              width: 20,
              height: 20,
              borderRadius: 20 / 2,
              borderWidth: 1,
              borderColor: mainRed,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="arrow-right"
              style={{
                top: 1,
                left: 10,
                fontSize: 15,
                color: mainRed,
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(SmallButtonMakePayment);

export default props => <Wrapper {...props} />;
