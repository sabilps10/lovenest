import React from 'react';
import {Text} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import Dash from 'react-native-dash';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';

const {black, greyLine} = Colors;
const {book, medium} = FontType;

const DetailPaymentCard = props => {
  const {total, paid, unPaid} = props;

  return (
    <>
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingTop: 5,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          UNPAID
        </Text>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          $ {unPaid}
        </Text>
      </CardItem>
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingTop: 0,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          PAID
        </Text>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          $ {paid}
        </Text>
      </CardItem>
      <CardItem style={{paddingTop: 0}}>
        <Dash
          dashColor={greyLine}
          dashThickness={1}
          style={{width: '100%', height: 1}}
        />
      </CardItem>
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingTop: 0,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          TOTAL
        </Text>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          $ {total}
        </Text>
      </CardItem>
    </>
  );
};

const Wrapper = compose(withApollo)(DetailPaymentCard);

export default props => <Wrapper {...props} />;
