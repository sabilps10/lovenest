import React from 'react';
import {Text, View, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import AsyncImage from '../../../Image/AsyncImage';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../../utils/Themes/Fonts';

const {width, height} = Dimensions.get('window');
const {newContainerColor, black} = Colors;
const {medium, book} = FontType;

const HeaderCardLabel = props => {
  const {logoSource, merchant, order} = props;
  return (
    <CardItem
      style={{
        backgroundColor: 'transparent',
        alignItems: 'flex-start',
      }}>
      <View style={{flex: 0.2}}>
        <AsyncImage
          style={{
            width: width / 9,
            height: height / 18,
            aspectRatio: 83 / 86.86,
          }}
          loaderStyle={{width: 83 / 1.5, height: 86.86 / 1.5}}
          source={logoSource}
          placeholderColor={newContainerColor}
        />
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View style={{flexDirection: 'row', width: '100%'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {merchant?.name ? merchant.name : 'N/A'}
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginHorizontal: 5,
            }}>
            |
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {merchant?.serviceType ? merchant.serviceType : 'N/A'}
          </Text>
        </View>
        <Text
          style={{
            marginTop: 2.5,
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {order?.number ? order.number : 'N/A'}
        </Text>
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(HeaderCardLabel);

export default props => <Wrapper {...props} />;
