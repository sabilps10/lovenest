import React from 'react';
import {Animated, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card} from 'native-base';
import {commonImage} from '../../../../utils/Themes/Images';

const {lnLogoNotif} = commonImage;
const AnimatedCard = Animated.createAnimatedComponent(Card);

// Small Component
import HeaderCardLabel from './HeaderCardLabel';
import Separator from './Separator';
import DetailPaymentCard from './DetailPaymentCard';
import SmallButtonMakePayment from './SmallButtonMakePayment';

const CardPayment = props => {
  const {item, navigation, fullWidth} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const {merchant, order} = item;
  const unPaid = item?.balance ? item.balance.toFixed(2) : 'N/A';
  const paid = item?.totalPaid ? item.totalPaid.toFixed(2) : 'N/A';
  const total = item?.order?.total ? item.order.total.toFixed(2) : 'N/A';
  const logoSource =
    merchant === null || merchant === undefined
      ? {lnLogoNotif}
      : merchant.logoImageDynamicUrl === null ||
        merchant.logoImageDynamicUrl === undefined
      ? {uri: merchant.logoImageUrl}
      : {uri: `${merchant.logoImageDynamicUrl}=h500`};

  const goToMakePayment = () => {
    try {
      navigation.navigate('MakePayment', {
        paymentDetail: item,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToPaymentDetail = () => {
    try {
      navigation.navigate('PaymentDetails', {
        paymentDetail: item,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <TouchableOpacity
      disabled={fullWidth ? true : false}
      activeOpacity={1}
      onPress={() => goToPaymentDetail()}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        transparent={fullWidth ? true : false}
        style={{
          transform: [{scale: scale}],
          marginLeft: fullWidth ? 0 : 15,
          marginRight: fullWidth ? 0 : 15,
          borderRadius: 4,
          marginBottom: 20,
        }}>
        <HeaderCardLabel
          navigation={navigation}
          logoSource={logoSource}
          merchant={merchant}
          order={order}
          item={item}
        />
        <Separator />
        <DetailPaymentCard total={total} paid={paid} unPaid={unPaid} />
        {fullWidth ? null : (
          <SmallButtonMakePayment onPress={() => goToMakePayment()} />
        )}
      </AnimatedCard>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(CardPayment);

export default props => <Wrapper {...props} />;
