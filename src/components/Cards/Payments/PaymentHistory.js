import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import PaymentHistoryTimeLine from '../Payments/TimeLine/PaymentHistoryTimeLine';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {greyLine} = Colors;
const {regular} = FontSize;
const {book} = FontType;

const PaymentHistory = props => {
  console.log('PaymentHistory Props: ', props);
  const {
    singlePreview,
    navigation,
    orderDetailData,
    showTextLastPayment,
  } = props;

  if (orderDetailData === undefined || orderDetailData === null) {
    return null;
  } else {
    if (
      orderDetailData.paymentList === undefined ||
      orderDetailData.paymentList === null
    ) {
      return null;
    } else {
      const {paymentList} = orderDetailData;
      if (paymentList === undefined || paymentList === null) {
        return null;
      } else {
        if (paymentList.length === 0) {
          return null;
        } else {
          const goToPaymentHistory = () => {
            try {
              navigation.navigate('PaymentHistory', {
                data: orderDetailData,
              });
            } catch (error) {
              console.log('Error Go To Payment Details');
            }
          };
          return (
            <Card
              transparent
              style={{
                marginTop: 15,
                marginLeft: 0,
                marginRight: 0,
                elevation: 0,
                shadowOpacity: 0,
              }}>
              <CardItem>
                <View style={{flex: 2}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.8),
                      lineHeight: 20,
                      letterSpacing: 0.3,
                      color: '#999999',
                    }}>
                    {showTextLastPayment ? 'LAST PAYMENT' : 'PAYMENT HISTORY'}
                  </Text>
                </View>
              </CardItem>
              <PaymentHistoryTimeLine
                {...props}
                goToPaymentHistory={goToPaymentHistory}
                singlePreview={singlePreview ? true : false}
                paymentList={paymentList}
              />
              <CardItem style={{width: '100%', marginTop: 20}}>
                <View
                  style={{
                    width: '100%',
                    height: 0.5,
                    borderWidth: 0.2,
                    borderColor: greyLine,
                  }}
                />
              </CardItem>
            </Card>
          );
        }
      }
    }
  }
};

const Wrapper = compose(withApollo)(PaymentHistory);

export default props => <Wrapper {...props} />;
