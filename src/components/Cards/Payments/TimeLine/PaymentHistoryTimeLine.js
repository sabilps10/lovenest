import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button, Card, CardItem, Icon} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../../utils/Themes/Fonts';
import moment from 'moment';
import {charImage} from '../../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {
  transparent,
  black,
  greyLine,
  lightSalmon,
  white,
  disablePaymentText,
  refundBgColor,
} = Colors;
const {charChekcedTimeline} = charImage;
const {regular, small} = FontSize;
const {book, medium} = FontType;

export const CircleIcon = props => {
  const {singlePreview} = props;
  return (
    <View
      style={{
        top: singlePreview ? 10 : 3,
        left: singlePreview ? -8 : -5,
        width: 18,
        height: 18,
        borderRadius: 18 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: lightSalmon,
      }}>
      {/* <Icon
        type="Feather"
        name="check"
        style={{top: 1, right: -12, color: white, fontSize: 10}}
      /> */}
      <Image
        source={charChekcedTimeline}
        style={{width: width / 18, height: height / 18}}
        resizeMode="contain"
      />
    </View>
  );
};

export const TimeLine = props => {
  const {
    singlePreview,
    paymentListLength,
    paymentItem,
    paymentIndex,
    goToPaymentHistory,
  } = props;
  return (
    <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <CircleIcon singlePreview={singlePreview} />
        <View
          style={{
            top: 7,
            left: -5,
            width: 1,
            flex: 1,
            borderWidth: 0.5,
            borderColor:
              paymentIndex === paymentListLength - 1
                ? transparent
                : paymentListLength === 1 && singlePreview
                ? transparent
                : paymentListLength === 1 && !singlePreview
                ? transparent
                : lightSalmon,
          }}
        />
      </View>
      <View style={{flex: 1}}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: disablePaymentText,
              fontFamily: book,
              fontSize: small,
              letterSpacing: 0.3,
              lineHeight: 21,
            }}>
            {paymentItem.refund === null || paymentItem.refund === undefined
              ? paymentItem.paidDate
              : paymentItem.refund.createdOn === undefined ||
                paymentItem.refund.createdOn === null
              ? ''
              : moment(paymentItem.refund.createdOn).format('DD MMMM YYYY')}
          </Text>
          {singlePreview ? (
            <Button
              onPress={goToPaymentHistory}
              transparent
              style={{
                alignSelf: 'center',
                paddingTop: 0,
                paddingBottom: 0,
                height: 35,
                width: 35,
                justifyContent: 'center',
              }}>
              <Icon
                type="Feather"
                name="chevron-right"
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 24,
                  color: '#999999',
                }}
              />
            </Button>
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
            alignItems: 'center',
            paddingTop: 5,
            paddingBottom: 5,
          }}>
          <Text
            style={{
              marginRight: 5,
              fontFamily: medium,
              fontSize: small,
              letterSpacing: 0.3,
              lineHeight: 21,
            }}>
            {paymentItem.paymentNumber}
          </Text>
          {paymentItem.refund === null ? null : (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 2,
                paddingBottom: 2,
                paddingLeft: 5,
                paddingRight: 5,
                borderRadius: 20,
                backgroundColor: refundBgColor,
                flexWrap: 'wrap',
              }}>
              <Text style={{color: disablePaymentText, fontSize: small}}>
                Refunded
              </Text>
            </View>
          )}
        </View>
        <Text
          style={{
            marginBottom: 5,
            fontFamily: medium,
            fontSize: small,
            letterSpacing: 0.3,
            lineHeight: 21,
          }}>
          {paymentItem.preAmount === null || paymentItem.preAmount === undefined
            ? ''
            : `$ ${paymentItem.preAmount.toFixed(2)}`}
          {paymentItem.paymentType === null ||
          paymentItem.paymentType === undefined
            ? ''
            : ` - ${paymentItem.paymentType}`}
        </Text>
        <Text
          style={{
            marginBottom: 5,
            fontFamily: book,
            fontSize: small,
            letterSpacing: 0.3,
            lineHeight: 21,
          }}>
          {paymentItem.refund === null || paymentItem.refund === undefined
            ? `${paymentItem.description}`
            : `${paymentItem.refund.reason}`}
        </Text>
        {paymentItem.refund === null ? null : (
          <Text
            style={{
              fontFamily: book,
              fontSize: small,
              letterSpacing: 0.3,
              lineHeight: 21,
              marginBottom: 5,
              color: disablePaymentText,
            }}>
            {paymentItem.refund.amount === null ||
            paymentItem.refund.amount === undefined
              ? ''
              : `Refund amount $ ${paymentItem.refund.amount.toFixed(2)}`}{' '}
            {paymentItem.refund.type === null ||
            paymentItem.refund.type === undefined
              ? ''
              : `- ${paymentItem.refund.type}`}
          </Text>
        )}
        <View style={{width: '100%', marginBottom: 15}} />
      </View>
    </CardItem>
  );
};

const PaymentHistoryTimeLine = props => {
  console.log('PaymentHistoryTimeLine: ', props);
  const {paymentList, singlePreview, goToPaymentHistory} = props;
  const stylingCard = singlePreview
    ? {
        borderColor: '#FCE4E5',
        borderWidth: 1.5,
        borderRadius: 4,
        marginLeft: 15,
        marginRight: 15,
        paddingTop: 15,
        paddingBottom: 0,
        backgroundColor: '#FFEDED',
      }
    : {
        marginLeft: 0,
        marginRight: 0,
      };

  if (paymentList === undefined || paymentList === null) {
    return null;
  } else {
    const paymentData = singlePreview ? paymentList.slice(0, 1) : paymentList;
    const paymentListLength = paymentData.length;
    return (
      <FlatList
        contentContainerStyle={{...stylingCard}}
        data={paymentData}
        keyExtractor={(item, index) => `${String(index)} Payment List Item`}
        listKey={(item, index) => `${String(index)} Payment List Item`}
        renderItem={({item, index}) => {
          return (
            <TimeLine
              {...props}
              goToPaymentHistory={goToPaymentHistory}
              singlePreview={singlePreview}
              paymentListLength={paymentListLength}
              paymentItem={item}
              paymentIndex={index}
            />
          );
        }}
      />
    );
  }
};

const Wrapper = compose(withApollo)(PaymentHistoryTimeLine);

export default props => <Wrapper {...props} />;
