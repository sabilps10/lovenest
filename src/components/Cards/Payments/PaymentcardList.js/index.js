import React, {useMemo} from 'react';
import {Text, View, FlatList, RefreshControl} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import CardPayment from '../PaymentCard';

const PaymentCardList = props => {
  console.log('PaymentCardList >>>>> ', props);
  const {
    paymentTabRef,
    paymentList,
    pullToRefresh,
    isPullRefresh,
    pageSize,
    totalCount,
    settingIsLoadMore,
    isLoadMore,
    onChangeOpacity,
  } = props;

  const RenderPaymentList = () =>
    useMemo(() => {
      return (
        <FlatList
          onScrollBeginDrag={() => onChangeOpacity(false)}
          onScrollEndDrag={() => onChangeOpacity(true)}
          ref={paymentTabRef}
          refreshControl={
            <RefreshControl
              onRefresh={() => pullToRefresh()}
              refreshing={isPullRefresh}
            />
          }
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            if (pageSize <= totalCount) {
              settingIsLoadMore(true);
            } else {
              settingIsLoadMore(false);
            }
          }}
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <View
                  style={{
                    width: '100%',
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 5,
                  }}>
                  <Text>Load More...</Text>
                </View>
              );
            } else {
              return null;
            }
          }}
          contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
          data={paymentList}
          extraData={paymentList}
          keyExtractor={(item, index) => `${index} Payment List`}
          listKey={(item, index) => `${index} Payment List`}
          renderItem={({item}) => {
            return <CardPayment {...props} item={item} />;
          }}
        />
      );
    }, [isLoadMore, isPullRefresh, paymentList]);

  if (paymentList) {
    return RenderPaymentList();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(PaymentCardList);

export default props => <Wrapper {...props} />;
