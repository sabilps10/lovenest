import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Dash from 'react-native-dash';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, greyLine} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;

const style = StyleSheet.create({
  card: {
    marginTop: 5,
    marginLeft: 0,
    marginRight: 0,
    elevation: 0,
    shadowOpacity: 0,
  },
  cardDetail: {
    paddingBottom: 0,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 1,
    borderBottomWidth: 0.5,
    borderBottomColor: greyLine,
  },
  label: {
    fontFamily: book,
    fontSize: RFPercentage(1.8),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: '#999999',
  },
  totalLabel: {
    fontFamily: medium,
    fontSize: RFPercentage(1.8),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: black,
  },
  commonLabel: {
    fontFamily: book,
    fontSize: RFPercentage(1.8),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: black,
  },
});

const PaymentDetail = props => {
  console.log('Payment Detail Card Props: ', props);
  const {orderDetailData, label} = props;

  if (orderDetailData === null || orderDetailData === undefined) {
    return null;
  } else {
    if (
      orderDetailData.orderType === null ||
      orderDetailData.orderType === undefined
    ) {
      return null;
    } else {
      const {orderType} = orderDetailData;
      if (orderType === 'Bridal') {
        return (
          <PaymentDetailsCard label={label} orderDetailData={orderDetailData} />
        );
      } else if (orderType === 'Interior Design') {
        return (
          <PaymentDetailsCard label={label} orderDetailData={orderDetailData} />
        );
      } else if (orderType === 'Venue') {
        return (
          <PaymentDetailsCard label={label} orderDetailData={orderDetailData} />
        );
      } else if (orderType === 'Hotel') {
        return (
          <PaymentDetailsCard label={label} orderDetailData={orderDetailData} />
        );
      } else {
        return null;
      }
    }
  }
};

export const PaymentDetailsCard = props => {
  const {orderDetailData, label} = props;
  const {total, subTotal, gstTax, totalPaid, balance} = orderDetailData;
  return (
    <Card transparent style={style.card}>
      <CardItem style={{paddingBottom: 0, marginBottom: 0}}>
        <Text style={style.label}>{label}</Text>
      </CardItem>
      <CardItem style={style.cardDetail}>
        <Text style={style.totalLabel}>Total</Text>
        <Text style={style.totalLabel}>
          {total === undefined || total === null ? '' : `$ ${total.toFixed(2)}`}
        </Text>
      </CardItem>
      <CardItem style={style.cardDetail}>
        <Text style={style.commonLabel}>Subtotal</Text>
        <Text style={style.commonLabel}>
          {subTotal === undefined || subTotal === null
            ? ''
            : `$ ${subTotal.toFixed(2)}`}
        </Text>
      </CardItem>
      <CardItem style={style.cardDetail}>
        <Text style={style.commonLabel}>GST @ 7%</Text>
        <Text style={style.commonLabel}>
          {gstTax === undefined || gstTax === null
            ? ''
            : `$ ${gstTax.toFixed(2)}`}
        </Text>
      </CardItem>
      <CardItem>
        {/* <View style={style.line} /> */}
        <Dash
          dashColor={greyLine}
          dashThickness={0.8}
          style={{width: '100%', height: 0.5}}
        />
      </CardItem>
      <CardItem style={{...style.cardDetail, paddingTop: 0}}>
        <Text style={style.commonLabel}>Paid</Text>
        <Text style={style.commonLabel}>
          {totalPaid === null || totalPaid === undefined
            ? ''
            : `$ ${totalPaid.toFixed(2)}`}
        </Text>
      </CardItem>
      <CardItem style={{...style.cardDetail, paddingBottom: 10}}>
        <Text style={style.commonLabel}>Balance</Text>
        <Text style={style.commonLabel}>
          {balance === null || balance === undefined
            ? ''
            : `$ ${balance.toFixed(2)}`}
        </Text>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(PaymentDetail);

export default props => <Wrapper {...props} />;
