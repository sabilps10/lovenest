import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {commonImage} from '../../../utils/Themes/Images';
import AsyncImage from '../../Image/AsyncImage';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import moment from 'moment';
import {Card, CardItem} from 'native-base';

const {lnLogoNotif} = commonImage;
const {black, greyLine, newContainerColor} = Colors;
const {regular} = FontSize;
const {book} = FontType;

const OrderDetailHeader = props => {
  console.log('OrderDetailHeader Props: ', props);
  const {orderDetailData} = props;

  if (orderDetailData === null) {
    return null;
  } else {
    const logoSource =
      orderDetailData.orderType === 'Bridal' ||
      orderDetailData.orderType === 'Interior Design' ||
      orderDetailData.orderType === 'Venue' ||
      orderDetailData.orderType === 'Hotel' ||
      orderDetailData.orderType === 'Florist'
        ? orderDetailData.merchant !== null || undefined
          ? orderDetailData.merchant.logoImageDynamicUrl === null &&
            orderDetailData.merchant.logoImageUrl === null
            ? lnLogoNotif
            : orderDetailData.merchant.logoImageDynamicUrl === null
            ? {uri: orderDetailData.merchant.logoImageUrl}
            : {uri: orderDetailData.merchant.logoImageDynamicUrl}
          : lnLogoNotif
        : orderDetailData.eventDetails !== null || undefined
        ? orderDetailData.eventDetails.eventBannerDynamicURL === null &&
          orderDetailData.eventDetails.eventBannerURL === null
          ? lnLogoNotif
          : orderDetailData.eventDetails.eventBannerDynamicURL === null
          ? {uri: orderDetailData.eventDetails.eventBannerURL}
          : {uri: orderDetailData.eventDetails.eventBannerDynamicURL}
        : lnLogoNotif;

    const name = orderDetailData?.merchant?.name
      ? orderDetailData.merchant.name
      : '';

    return (
      <Card
        transparent
        style={{
          marginTop: 15,
          marginLeft: 0,
          marginRight: 0,
          shadowOpacity: 0,
          elevation: 0,
        }}>
        <CardItem>
          <View
            style={{
              marginRight: 10,
              width: 43,
              height: 45,
            }}>
            <AsyncImage
              style={{
                width: 42.5,
                height: 44,
                aspectRatio: 42.5 / 44,
              }}
              loaderStyle={{width: 43 / 1.5, height: 45 / 1.5}}
              source={logoSource}
              placeholderColor={newContainerColor}
            />
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              height: '100%',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: regular,
                color: '#999999',
                letterSpacing: 0.3,
              }}>
              {orderDetailData.orderType === null
                ? ''
                : orderDetailData.orderType.toUpperCase()}
            </Text>
            <Text
              style={{
                lineHeight: 25,
                fontFamily: book,
                fontSize: regular,
                color: black,
                letterSpacing: 0.3,
              }}>
              {name}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              flexDirection: 'column',
              justifyContent: 'flex-start',
              alignItems: 'flex-end',
              height: '100%',
            }}>
            <Text
              style={{
                textAlign: 'right',
                fontFamily: book,
                fontSize: regular,
                color: '#999999',
                letterSpacing: 0.3,
              }}>
              {orderDetailData.createdOn === null ||
              orderDetailData.createdOn === undefined
                ? ''
                : moment(orderDetailData.createdOn).format('DD MMM YYYY')}
            </Text>
          </View>
        </CardItem>
        <CardItem>
          <View
            style={{
              width: '100%',
              height: 0.5,
              borderWidth: 0.2,
              borderColor: greyLine,
            }}
          />
        </CardItem>
      </Card>
    );
  }
};

const Wrapper = compose(withApollo)(OrderDetailHeader);

export default props => <Wrapper {...props} />;
