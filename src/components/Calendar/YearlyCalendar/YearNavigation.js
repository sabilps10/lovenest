import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {CardItem, Left, Body, Right, Icon} from 'native-base';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black} = Colors;
const {medium} = FontType;

const YearNavigation = props => {
  const {year, backButton, rightButton} = props;
  const currentYear = moment().format('YYYY');
  return (
    <CardItem>
      <Left
        style={{
          flex: 1,
        }}>
        {parseInt(year, 10) <= parseInt(currentYear, 10) ? null : (
          <TouchableOpacity
            style={{
              width: '100%',
              height: 30,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}
            onPress={() => backButton()}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{fontSize: 20, color: black}}
            />
          </TouchableOpacity>
        )}
      </Left>
      <Body
        style={{
          flex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.9),
            color: black,
            letterSpacing: 0.3,
          }}>
          {year}
        </Text>
      </Body>
      <Right
        style={{
          flex: 1,
        }}>
        <TouchableOpacity
          onPress={() => rightButton()}
          style={{
            width: '100%',
            height: 30,
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}>
          <Icon
            type="Feather"
            name="chevron-right"
            style={{fontSize: 20, color: black}}
          />
        </TouchableOpacity>
      </Right>
    </CardItem>
  );
};

export default YearNavigation;
