import React from 'react';
import {Text, View, Platform} from 'react-native';
import {Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import LinearGradient from 'react-native-linear-gradient';

const {lightSalmon, disableCalendar} = Colors;
const {book} = FontType;
const {small} = FontSize;

const Legend = ({footer}) => {
  return (
    <>
      <CardItem style={{flexDirection: 'column'}}>
        <View
          style={{
            paddingLeft: 100,
            paddingRight: 100,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* <View
              style={{
                minWidth: 17,
                maxWidth: 17,
                minHeight: 17,
                maxHeight: 17,
                backgroundColor: lightSalmon,
                borderRadius: 4,
                marginBottom: 10,
              }}
            /> */}
            {Platform.OS === 'android' ? (
              <LinearGradient
                start={{x: 0.0, y: 1.8}}
                end={{x: 1.1, y: 0.0}}
                locations={[0, 0.5, 0.78]}
                colors={['#fa878c', '#fa878c', '#fcc0c3']}
                style={{
                  minWidth: 17,
                  maxWidth: 17,
                  minHeight: 17,
                  maxHeight: 17,
                  margin: 1,
                  borderRadius: 4,
                }}
              />
            ) : (
              <LinearGradient
                start={{x: 0.15, y: 1.15}}
                end={{x: 0.95, y: 0.19}}
                locations={[0.6, 0.3, 0.2]}
                // colors={['#fa878c', '#fa878c', '#fa878c', '#fcc0c3']}
                colors={['#fa878c', '#fa878c', '#fa878c', '#fcc0c3']}
                style={{
                  minWidth: 17,
                  maxWidth: 17,
                  minHeight: 17,
                  maxHeight: 17,
                  margin: 1,
                  borderRadius: 4,
                  marginBottom: 10,
                }}
              />
            )}
            <Text style={{fontFamily: book, fontSize: small, color: '#545454'}}>
              Available
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                minWidth: 17,
                maxWidth: 17,
                minHeight: 17,
                maxHeight: 17,
                backgroundColor: disableCalendar,
                borderRadius: 4,
                marginBottom: 10,
              }}
            />
            <Text style={{fontFamily: book, fontSize: small, color: '#545454'}}>
              Unavailable
            </Text>
          </View>
        </View>
      </CardItem>
      {footer ? (
        <CardItem
          style={{
            marginTop: 32,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontFamily: book, fontSize: small, color: '#545454'}}>
            Choose your appointment date
          </Text>
        </CardItem>
      ) : null}
    </>
  );
};

export default Legend;
