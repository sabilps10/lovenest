import React from 'react';
import {Text, View, FlatList, Platform} from 'react-native';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import LinearGradient from 'react-native-linear-gradient';

const {white, disableCalendar, lightSalmon} = Colors;

const DateInMonth = props => {
  const {dateInMonth, weddingDate} = props;
  const marriageDate = moment(weddingDate).format('DD-MMMM-YYYY');
  const monthStart = moment(dateInMonth.monthYear)
    .startOf('month')
    .format('DD-MMMM-YYYY');
  return (
    <FlatList
      data={dateInMonth.datesInMonth}
      keyExtractor={(item, index) => 'Date' + index.toString()}
      listKey={(item, index) => 'Date' + index.toString()}
      columnWrapperStyle={{justifyContent: 'center', alignItems: 'center'}}
      numColumns={7}
      renderItem={({item, index}) => {
        const theDate = moment(item.date).format('DD-MMMM-YYYY');
        const currentDate = moment(new Date()).format('DD-MMMM-YYYY');
        const isSame = moment(theDate).isSame(monthStart, 'month');
        const isBefore = moment(item.date).isBefore(currentDate);
        const fullBooked = item.isFullBooked;
        const isSameDay = moment(theDate).isSame(currentDate);
        const isExceedWeddingDate = moment(theDate).isBefore(marriageDate);
        if (!isSame) {
          // white ----> not same month
          return (
            <View
              style={{
                minWidth: 17,
                maxWidth: 17,
                minHeight: 17,
                maxHeight: 17,
                backgroundColor: white,
                margin: 1,
                borderRadius: 4,
              }}
            />
          );
        } else {
          // same month ---> check full book or not
          if (fullBooked) {
            // grey box
            return (
              <View
                style={{
                  minWidth: 17,
                  maxWidth: 17,
                  minHeight: 17,
                  maxHeight: 17,
                  backgroundColor: disableCalendar,
                  margin: 1,
                  borderRadius: 4,
                }}
              />
            );
          } else {
            if (isBefore) {
              // date already pass
              return (
                <View
                  style={{
                    minWidth: 17,
                    maxWidth: 17,
                    minHeight: 17,
                    maxHeight: 17,
                    backgroundColor: disableCalendar,
                    margin: 1,
                    borderRadius: 4,
                  }}
                />
              );
            } else if (isSameDay) {
              // same date ----> grey
              return (
                <View
                  style={{
                    minWidth: 17,
                    maxWidth: 17,
                    minHeight: 17,
                    maxHeight: 17,
                    backgroundColor: disableCalendar,
                    margin: 1,
                    borderRadius: 4,
                  }}
                />
              );
            } else if (!isExceedWeddingDate) {
              return (
                <View
                  style={{
                    minWidth: 17,
                    maxWidth: 17,
                    minHeight: 17,
                    maxHeight: 17,
                    backgroundColor: disableCalendar,
                    margin: 1,
                    borderRadius: 4,
                  }}
                />
              );
            } else {
              return (
                <>
                  {Platform.OS === 'android' ? (
                    <LinearGradient
                      start={{x: 0.0, y: 1.8}}
                      end={{x: 1.1, y: 0.0}}
                      locations={[0, 0.5, 0.78]}
                      colors={['#fa878c', '#fa878c', '#fcc0c3']}
                      style={{
                        minWidth: 17,
                        maxWidth: 17,
                        minHeight: 17,
                        maxHeight: 17,
                        margin: 1,
                        borderRadius: 4,
                      }}
                    />
                  ) : (
                    <LinearGradient
                      start={{x: 0.15, y: 1.15}}
                      end={{x: 0.95, y: 0.19}}
                      locations={[0.6, 0.3, 0.2]}
                      colors={['#fa878c', '#fa878c', '#fa878c', '#fcc0c3']}
                      style={{
                        minWidth: 17,
                        maxWidth: 17,
                        minHeight: 17,
                        maxHeight: 17,
                        margin: 1,
                        borderRadius: 4,
                      }}
                    />
                  )}
                </>
              );
            }
          }
        }
      }}
    />
  );
};

export default DateInMonth;
