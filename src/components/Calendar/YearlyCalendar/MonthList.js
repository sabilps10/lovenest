import React, {useMemo} from 'react';
import {Text, View, FlatList, TouchableOpacity, Animated, Dimensions} from 'react-native';
import DateInMonth from './DateInMonth';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import moment from 'moment';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

const AnimatedFlatlist = Animated.createAnimatedComponent(FlatList);
const {black} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const { height } = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

const MonthList = props => {
  // console.log('Props MonthList: ', props);
  const {
    appointmentId,
    opacity,
    weddingDate,
    calendar,
    navigation,
    itemName,
    activity,
  } = props;
  // console.log('weddingDate > ', weddingDate);
  const currentMonth = moment().format('DD-MMMM-YYYY');

  const goToMonthlyCalendar = index => {
    navigation.navigate('MonthlyCalendar', {
      appointmentId,
      index,
      calendar,
      weddingDate,
      itemName,
      activity,
    });
  };

  const keyExt = (item, _) => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderAnimatedFlatlist = () =>
    useMemo(() => {
      return (
        <AnimatedFlatlist
          style={{
            opacity,
          }}
          data={calendar}
          keyExtractor={keyExt}
          getItemLayout={getItemLayout}
          numColumns={2}
          contentContainerStyle={{
            justifyContent: 'space-between',
          }}
          columnWrapperStyle={{
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
          renderItem={({item, index}) => {
            // console.log('item monthly: ', item);
            const monthYearCalendar = moment(item.monthYear).format(
              'DD-MMMM-YYYY',
            );
            const isBeforeMonth = moment(monthYearCalendar).isBefore(
              currentMonth,
              'M',
            );
            const isAfterMonth = moment(monthYearCalendar).isAfter(
              weddingDate,
              'M',
            );
            // console.log(`${monthYearCalendar}`, {
            //   isBefore: isBeforeMonth,
            //   isAfter: isAfterMonth,
            // });
            return (
              <View
                style={{
                  flexDirection: 'column',
                  flexBasis: '50%',
                  height: 150,
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    padding: 5,
                    paddingLeft: 35,
                    paddingRight: 15,
                  }}>
                  <Text
                    style={{
                      top: 10,
                      marginBottom: 10,
                      fontFamily: medium,
                    }}>
                    {item.month}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    disabled={
                      isBeforeMonth && !isAfterMonth
                        ? true
                        : !isBeforeMonth && isAfterMonth
                        ? true
                        : false
                    }
                    onPress={() => {
                      goToMonthlyCalendar(index);
                    }}
                    style={{
                      flexDirection: 'column',
                      flexWrap: 'wrap',
                    }}>
                    <DateInMonth dateInMonth={item} weddingDate={weddingDate} />
                  </TouchableOpacity>
                </View>
              </View>
            );
          }}
        />
      );
    }, [calendar]);

  if (calendar) {
    return RenderAnimatedFlatlist();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(MonthList);

export default props => <Wrapper {...props} />;
