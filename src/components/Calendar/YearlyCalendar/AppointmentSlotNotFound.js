import React from 'react';
import {Text, View} from 'react-native';
import {Container, Content, Button} from 'native-base';
import ErrorToaster from '../../Toaster/errorToaster';
import Colors from '../../../utils/Themes/Colors';

const {newContainerColor} = Colors;

const AppointmentSlotNotFound = props => {
  const {onPress} = props;
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <ErrorToaster text={'Slot not available for this appointment!'} />
      <Content
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View>
          <Button
            style={{
              backgroundColor: 'transparent',
              borderWidth: 1,
              borderColor: '#d2d2d2',
              width: 100,
              height: 40,
              borderRadius: 5,
              elevation: 0,
              shadowOpacity: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => onPress()}>
            <Text style={{color: '#d2d2d2'}}>Reload</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

export default AppointmentSlotNotFound;
