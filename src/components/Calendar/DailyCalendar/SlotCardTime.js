import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import {CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {regular} = FontSize;
const {book, medium} = FontType;
const {transparent, greyLine, black} = Colors;
const red = '#FFEDED';

const SlotCardTime = props => {
  console.log('SlotCardTime: ', props);
  const {
    time,
    item,
    parentIndex,
    index,
    onPressed,
    slotsLeft,
    weddingDate,
    date,
    isCollapse,
  } = props;

  const parsingSelectedDate = moment(date).format('DD-MMMM-YYYY');
  const parsingWeddingDate = moment(weddingDate).format('DD-MMMM-YYYY');
  const currentDate = moment().format('DD-MMMM-YYYY');
  const isBefore = moment(parsingSelectedDate).isBefore(currentDate);
  const isSame = moment(parsingSelectedDate).isSame(currentDate);
  const isExceedWeddingDate = moment(parsingSelectedDate).isAfter(
    parsingWeddingDate,
  );

  const isDisable = isBefore
    ? true
    : isSame
    ? true
    : isExceedWeddingDate
    ? true
    : slotsLeft === 0
    ? true
    : item.availableSlot === 0
    ? true
    : item.availableSlot > 1
    ? false
    : false;

  const slotAvail = isBefore
    ? 'Full Booked'
    : isSame
    ? 'Full Booked'
    : isExceedWeddingDate
    ? 'Full Booked'
    : slotsLeft === 0
    ? 'Full Booked'
    : item.availableSlot === 0
    ? 'Full Booked'
    : item.availableSlot > 1
    ? `${item.availableSlot} Slots Left`
    : `${item.availableSlot} Slot Left`;
  return (
    <CardItem
      disabled={isDisable}
      button
      onPress={() => onPressed(parentIndex, index)}
      style={{
        paddingBottom: 0,
        backgroundColor: item.selected ? red : transparent,
      }}>
      <View style={{flexDirection: 'column', width: '100%', height: 40}}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                top: 7,
                fontFamily: item.selected ? medium : book,
                fontSize: RFPercentage(1.7),
                color: isDisable ? greyLine : black,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {item.timeLabel}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <Text
              style={{
                top: 7,
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: greyLine,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {slotAvail}
            </Text>
            {item.selected ? (
              <Icon
                type="Feather"
                name="check"
                style={{top: 2, left: 10, fontSize: 17, color: black}}
              />
            ) : null}
          </View>
        </View>
        {index === time.length - 1 ? null : (
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              width: '100%',
              height: 1,
              borderBottomColor: greyLine,
              borderBottomWidth: 0.5,
            }}
          />
        )}
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(SlotCardTime);

export default props => <Wrapper {...props} />;
