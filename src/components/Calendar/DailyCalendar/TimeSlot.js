import React, {useState, useEffect, useMemo} from 'react';
import {View, FlatList, Dimensions} from 'react-native';
import {Card, Container} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import AppointmentSlotByDateQuery from '../../../graphql/queries/getAppointmentSlotByDate';
import LoaderOnly from '../../Loader/circleLoader';
import AppointmentSlotNotFound from '../YearlyCalendar/AppointmentSlotNotFound';
import ErrorScreen from '../../ErrorScreens/NotLoginYet';
import SlotCardTitle from './SlotCardTitle';
import Slots from './Slots';

const {height} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

export const CardTimeSlot = props => {
  console.log('CardTimeSlot >>> ', props);
  const {slots, date, weddingDate, selectingTimeSlot, onCollapse} = props;

  const keyExt = (_, index) => `${index} Label`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderSlots = () =>
    useMemo(() => {
      return (
        <FlatList
          contentContainerStyle={{paddingTop: 15, paddingBottom: 5}}
          scrollEnabled
          extraData={slots}
          data={slots}
          keyExtractor={keyExt}
          getItemLayout={getItemLayout}
          listKey={(item, index) => `${index} Label`}
          renderItem={({item, index}) => {
            console.log('ANU SLOT Collapes: ', item);
            return (
              <Card
                style={{
                  borderRadius: 4,
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 15,
                  elevation: 0,
                  shadowOpacity: 0,
                }}>
                <SlotCardTitle
                  onCollapse={i => onCollapse(i)}
                  item={item}
                  index={index}
                  date={date}
                  weddingDate={weddingDate}
                />
                {item.collapse ? (
                  <Slots
                    isCollapse={item.collapse}
                    date={date}
                    weddingDate={weddingDate}
                    slotsLeft={item.availableSlots}
                    time={item.time}
                    parentIndex={index}
                    onSelected={(parentIndex, idx) =>
                      selectingTimeSlot(parentIndex, idx)
                    }
                  />
                ) : null}
              </Card>
            );
          }}
        />
      );
    }, [slots]);

  if (slots) {
    return RenderSlots();
  } else {
    return null;
  }
};

const TimeSlot = props => {
  console.log('Props Time Slot: ', props);

  const {
    navigation,
    client,
    indexes,
    calendar,
    appointmentId,
    weddingDate,
    settingSelectedTimeSlot,
  } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [slots, setSlots] = useState([]);
  const [connectionError, setConnectionError] = useState(false);
  const [errorStatus, setErrorStatus] = useState('');
  const [date, setDate] = useState('');

  useEffect(() => {
    if (
      indexes !== undefined &&
      calendar !== undefined &&
      appointmentId !== undefined
    ) {
      fetchTimeSlot(indexes, calendar, appointmentId);
    }
    const subscribe = navigation.addListener('tabPress', tabPressProps => {
      console.log('TAB PRESS TIME SLOT: ', tabPressProps);
      tabPressProps.preventDefault();
      if (
        indexes !== undefined &&
        calendar !== undefined &&
        appointmentId !== undefined
      ) {
        fetchTimeSlot(indexes, calendar, appointmentId);
      }
    });
    return subscribe;
  }, [navigation, indexes, isLoading]);

  const fetchTimeSlot = (i, c) => {
    client
      .query({
        query: AppointmentSlotByDateQuery,
        variables: {
          date: moment(c[i].date).format('DD MMM YYYY'),
          appointmentId,
        },
        fetchPolicy: 'no-cache',
        notifyOnNetworkStatusChange: true,
      })
      .then(async response => {
        console.log('Response: ', response);
        const {data} = response;
        const {getAppointmentSlotByDate, errors} = data;
        const {date: dateResponse, timeslot, error} = getAppointmentSlotByDate;
        if (errors === undefined) {
          if (error === null) {
            const updatingTimeSlotData = await Promise.all(
              timeslot.map(async d => {
                const timeLoop = await d.time.map(t => {
                  return {
                    ...t,
                    selected: false,
                  };
                });
                if (timeLoop.length === d.time.length) {
                  return {
                    ...d,
                    collapse: false,
                    time: timeLoop,
                  };
                }
              }),
            );
            // console.log('SLUUUTTTTYYYY: ', updatingTimeSlotData);
            if (updatingTimeSlotData.length === timeslot.length) {
              setDate(dateResponse);
              setSlots([...updatingTimeSlotData]);
              setErrorStatus('');
              setConnectionError(false);
              setIsLoading(false);
            }
          } else {
            setErrorStatus(error);
            setIsLoading(false);
            setConnectionError(false);
          }
        } else {
          setIsLoading(false);
          setConnectionError(true);
        }
      })
      .catch(error => {
        console.log('Error: ', error);
        setErrorStatus('Time slot not found!');
        setIsLoading(false);
        setConnectionError(false);
      });
  };

  const onCollapse = async index => {
    try {
      let oldData = slots;
      oldData[index].collapse = !oldData[index].collapse;

      await setSlots([...oldData]);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const selectingTimeSlot = async (parentIndex, index) => {
    console.log('parentIndex: ', parentIndex);
    console.log('index: ', index);
    console.log('Slots: ', slots);
    console.log(
      'SELECTED TIME SLOTS VALUE: ',
      slots[parentIndex].time[index].value,
    );
    try {
      let resetSlots = await Promise.all(
        slots.map(async d => {
          const resetTime = await d.time.map(t => {
            return {
              ...t,
              selected: false,
            };
          });

          if (resetTime.length === d.time.length) {
            return {
              ...d,
              time: resetTime,
            };
          }
        }),
      );

      if (resetSlots.length === slots.length) {
        // console.log('resetSlots: ', resetSlots);
        let oldSlots = resetSlots;
        oldSlots[parentIndex].time[index].selected = true;
        // console.log('NEW SLOTS: ', oldSlots);
        setSlots(oldSlots);
        settingSelectedTimeSlot(oldSlots[parentIndex].time[index].value);
      }
    } catch (error) {
      console.log('Error selectingTimeSlot: ', error);
    }
  };

  if (isLoading && !connectionError) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <LoaderOnly />
      </View>
    );
  } else if (!isLoading && connectionError) {
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          fetchTimeSlot(indexes, calendar);
        }}
      />
    );
  } else if (!isLoading && errorStatus !== '') {
    return (
      <AppointmentSlotNotFound
        onPress={() => {
          fetchTimeSlot(indexes, calendar);
        }}
      />
    );
  }

  return (
    <Container style={{backgroundColor: 'white'}}>
      <CardTimeSlot
        slots={slots}
        date={date}
        weddingDate={weddingDate}
        onCollapse={onCollapse}
        selectingTimeSlot={(parentIndex, idx) =>
          selectingTimeSlot(parentIndex, idx)
        }
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(TimeSlot);

export default props => <Wrapper {...props} />;
