import React, {useMemo} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';

const {black, lightSalmon, white, veryLightPinkTwo, mainRed} = Colors;
const {regular, small} = FontSize;
const {book, medium} = FontType;

const TabBar = tabProps => {
  const {
    state,
    descriptors,
    navigation: tabNavigation,
    tabRef,
    setIndex,
    indexes,
  } = tabProps;
  const keyExt = (_, index) => `${index}tabBar`;

  const RenderTabList = () =>
    useMemo(() => {
      return (
        <View style={styles.container}>
          <FlatList
            ref={tabRef}
            getItemLayout={(data, index) => {
              // Max 5 items visibles at once
              return {
                length: Dimensions.get('window').width / 5,
                offset: (Dimensions.get('window').width / 5) * index,
                index,
              };
            }}
            snapToAlignment={'center'}
            snapToInterval={Dimensions.get('window').width / 5}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            data={state.routes}
            keyExtractor={keyExt}
            listKey={keyExt}
            renderItem={({item: routes, index}) => {
              const {options} = descriptors[routes.key];
              const label =
                options.tabBarLabel !== undefined
                  ? options.tabBarLabel
                  : options.title !== undefined
                  ? options.title
                  : moment(routes.name).format('ddd, DD MMM YYYY');
              const isFocused = indexes === index;

              const onPress = async () => {
                const event = tabNavigation.emit({
                  type: 'tabPress',
                  target: moment(routes.name).format('ddd, DD MMM YYYY'),
                  canPreventDefault: true,
                });

                if (!isFocused && !event.defaultPrevented) {
                  await setIndex(index);
                  tabRef.current.scrollToIndex({
                    animated: true,
                    index,
                    viewOffset: Dimensions.get('window').width / 2.5,
                  });
                  tabNavigation.navigate(
                    moment(routes.name).format('ddd, DD MMM YYYY'),
                    {index: index},
                  );
                }
              };

              const onLongPress = () => {
                tabNavigation.emit({
                  type: 'tabLongPress',
                  target: moment(routes.name).format('ddd, DD MMM YYYY'),
                });
              };
              return (
                <>
                  <TouchableOpacity
                    key={String(index) + 'TabBar'}
                    accessibilityRole="button"
                    accessibilityStates={isFocused ? ['selected'] : []}
                    accessibilityLabel={options.tabBarAccessibilityLabel}
                    testID={options.tabBarTestID}
                    onPress={() => onPress()}
                    onLongPress={() => onLongPress()}
                    style={{
                      ...styles.buttonTab,
                      borderBottomColor: isFocused ? mainRed : veryLightPinkTwo,
                      borderBottomWidth: isFocused ? 4 : 0,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        ...styles.text,
                        fontFamily: isFocused ? medium : medium,
                        fontWeight: isFocused ? 'bold' : '500',
                        color: isFocused ? mainRed : black,
                      }}>
                      {moment(label).format('D')}
                    </Text>
                    <Text
                      style={{
                        ...styles.dayText,
                        fontFamily: isFocused ? book : book,
                        color: isFocused ? mainRed : black,
                      }}>
                      {moment(label).format('ddd')}
                    </Text>
                  </TouchableOpacity>
                </>
              );
            }}
          />
        </View>
      );
    }, [state.routes]);

  if (state.routes) {
    return RenderTabList();
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    width: Dimensions.get('window').width,
    backgroundColor: 'white',
    borderBottomColor: veryLightPinkTwo,
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderTopColor: white,
  },
  buttonTab: {
    height: '100%',
    width: Dimensions.get('window').width / 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: regular,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  dayText: {
    fontSize: small,
    lineHeight: 25,
    letterSpacing: 0.3,
  },
});

const Wrapper = compose(withApollo)(TabBar);

export default props => <Wrapper {...props} />;
