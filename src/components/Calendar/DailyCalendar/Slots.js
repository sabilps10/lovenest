import React from 'react';
import {FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import SlotCardTime from './SlotCardTime';

const Slots = props => {
  const {time, onSelected, parentIndex, slotsLeft, weddingDate, date, isCollapse} = props;
  const onSelectingIndexTimeSlot = (lab, idx) => {
    onSelected(lab, idx);
  };
  return (
    <FlatList
      data={time}
      extraData={time}
      keyExtractor={(item, index) => `${index}TimeSlot`}
      renderItem={({item, index}) => {
        return (
          <SlotCardTime
            isCollapse={isCollapse}
            date={date}
            time={time}
            weddingDate={weddingDate}
            slotsLeft={slotsLeft}
            item={item}
            parentIndex={parentIndex}
            index={index}
            onPressed={(parentIndexCB, indx) =>
              onSelectingIndexTimeSlot(parentIndexCB, indx)
            }
          />
        );
      }}
    />
  );
};

const Wrapper = compose(withApollo)(Slots);

export default props => <Wrapper {...props} />;
