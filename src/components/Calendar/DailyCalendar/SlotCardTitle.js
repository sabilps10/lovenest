import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import {CardItem, Button, Icon} from 'native-base';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {greyLine, black, lightSalmon} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

const SlotCardTitle = props => {
  const {item, date, weddingDate, onCollapse, index} = props;
  const currentDate = moment().format('DD-MMMM-YYYY');
  const parsingDate = moment(date).format('DD-MMMM-YYYY');
  const parsingWeddingDate = moment(weddingDate).format('DD-MMMM-YYYY');
  const isBefore = moment(parsingDate).isBefore(currentDate);
  const isSame = moment(parsingDate).isSame(currentDate);
  const isExceedWeddingDate = moment(parsingDate).isAfter(parsingWeddingDate);

  const slotAvail = isBefore
    ? 'Full Booked'
    : isSame
    ? 'Full Booked'
    : isExceedWeddingDate
    ? 'Full Booked'
    : item.availableSlots === 0
    ? 'Full Booked'
    : item.availableSlots > 1
    ? `${item.availableSlots} Slots Available`
    : `${item.availableSlots} Slot Available`;

  const backgroundColor = isBefore
    ? greyLine
    : isSame
    ? greyLine
    : isExceedWeddingDate
    ? greyLine
    : item.availableSlots === 0
    ? greyLine
    : black;
  const textColor = isBefore
    ? greyLine
    : isSame
    ? greyLine
    : isExceedWeddingDate
    ? greyLine
    : item.availableSlots === 0
    ? greyLine
    : black;
  return (
    <CardItem style={{paddingBottom: 0, backgroundColor: 'transparent'}}>
      <View style={{flexDirection: 'column', width: '100%', height: 40}}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            {/* <View
              style={{
                top: 7,
                width: 11,
                height: 11,
                marginRight: 10,
                backgroundColor: backgroundColor,
              }}
            /> */}
            <Text
              style={{
                // top: 8,
                top: -1,
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: textColor,
                letterSpacing: 0.3,
              }}>
              {item.label}
            </Text>
            <Text
              style={{
                // top: 8,
                fontStyle: 'italic',
                marginLeft: 5,
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: '#999999',
                letterSpacing: 0.3,
              }}>
              ({slotAvail})
            </Text>
          </View>
          <View
            style={{
              flex: 0.25,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            {/* <Text
              style={{
                top: 8,
                fontFamily: book,
                fontSize: regular,
                color: textColor,
                letterSpacing: 0.3,
              }}>
              {slotAvail}
            </Text> */}
            <Button
              disabled={backgroundColor === greyLine ? true : false}
              onPress={() => onCollapse(index)}
              style={{
                elevation: 0,
                alignItems: 'center',
                paddingTop: 0,
                paddingBottom: 0,
                height: 35,
                width: 35,
                justifyContent: 'center',
                backgroundColor: 'transparent',
              }}>
              <Icon
                type="Feather"
                name={item.collapse ? 'chevron-up' : 'chevron-down'}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 24,
                  color: backgroundColor,
                }}
              />
            </Button>
          </View>
        </View>
        {/* <View
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            height: 1,
            borderBottomColor: greyLine,
            borderBottomWidth: 0.5,
          }}
        /> */}
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(SlotCardTitle);

export default props => <Wrapper {...props} />;
