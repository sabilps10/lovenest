import React, {useState, useEffect} from 'react';
import {Text, Dimensions, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Map from './index';
import {Card, CardItem, Icon, Button} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import Loader from '../../components/Loader/JustLoader';

const {book} = FontType;
const {black, white, greyLine} = Colors;

const rightStyle = {
  position: 'absolute',
  top: -20,
  right: 15,
  maxWidth: 42,
  minWidth: 42,
  maxHeight: 42,
  minHeight: 42,
  backgroundColor: white,
  borderRadius: 42 / 2,
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,
  elevation: 3,
  justifyContent: 'center',
  alignItems: 'center',
};

const normalStyle = {
  position: 'absolute',
  top: -20,
  left: '50%',
  maxWidth: 42,
  minWidth: 42,
  maxHeight: 42,
  minHeight: 42,
  backgroundColor: white,
  borderRadius: 42 / 2,
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,
  elevation: 3,
  justifyContent: 'center',
  alignItems: 'center',
};

const MapWithNavigationButton = props => {
  console.log('MapWithNavigationButton Props: ', props);
  const {
    forMapAddresses,
    onNavigate,
    withDirectionText,
    disableContainerStyle,
    buttonOnTheRight,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [address, setAddress] = useState('');

  useEffect(() => {
    settingAddress();
  }, []);

  const settingAddress = async () => {
    if (forMapAddresses) {
      await setAddress(forMapAddresses);
      await setIsLoading(false);
    }
  };

  if (forMapAddresses) {
    if (isLoading) {
      return (
        <View
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#f8f8f8',
          }}>
          {/* <View
            style={{
              width: 100,
              height: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Loader />
          </View> */}
          <Text style={{fontSize: 14, letterSpacing: 0.3, color: greyLine}}>
            Loading Map ...
          </Text>
        </View>
      );
    } else {
      return (
        <Card
          transparent
          style={
            disableContainerStyle
              ? {
                  marginLeft: 0,
                  marginRight: 0,
                  shadowOpacity: 0,
                  elevation: 0,
                  marginTop: 0,
                  paddingTop: 0,
                }
              : {
                  marginLeft: 0,
                  marginRight: 0,
                  shadowOpacity: 0,
                  elevation: 0,
                }
          }>
          <CardItem
            style={{
              backgroundColor: 'transparent',
              paddingLeft: 0,
              paddingRight: 0,
              paddingTop: 0,
              paddingBottom: 0,
            }}>
            <Map forMapAddress={address} />
          </CardItem>
          <CardItem style={{flexDirection: 'column', width: '100%'}}>
            <Button
              onPress={() => onNavigate()}
              style={buttonOnTheRight ? {...rightStyle} : {...normalStyle}}>
              <Icon
                type="Feather"
                name="navigation"
                style={{
                  marginRight: 0,
                  marginLeft: 0,
                  marginTop: 0,
                  marginBottom: 0,
                  fontSize: 21,
                  color: black,
                }}
              />
            </Button>
            {withDirectionText ? (
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                  marginTop: 20,
                }}>
                Get Directions
              </Text>
            ) : null}
          </CardItem>
        </Card>
      );
    }
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(MapWithNavigationButton);

export default props => <Wrapper {...props} />;
