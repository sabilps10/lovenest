import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geocoding from '../../utils/Geocoding';
import Loader from '../../components/Loader/JustLoader';
import Colors from '../../utils/Themes/Colors';

const {width, height} = Dimensions.get('window');
const {greyLine, white} = Colors;

const Maps = props => {
  const {forMapAddress, width: propWidth, height: propsHeight} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [address, setAddresses] = useState('');
  const [coordinate, setCoordinate] = useState({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  });

  useEffect(() => {
    setAddress();
  }, [forMapAddress]);

  const setAddress = async () => {
    try {
      if (forMapAddress) {
        await setAddresses(forMapAddress);
        await getLatLngLocationFromAddress();
        await setIsLoading(false);
      }
    } catch (error) {
      await setIsLoading(false);
    }
  };

  const getLatLngLocationFromAddress = async () => {
    try {
      const getLocationDetail = await Geocoding.detailLocation(forMapAddress);
      if (getLocationDetail) {
        const ASPECT_RATIO = width / height;

        const {lat} = getLocationDetail.location;
        const {lng} = getLocationDetail.location;
        const latDelta =
          getLocationDetail.viewport.northeast.lat -
          getLocationDetail.viewport.southwest.lat;
        const lngDelta = latDelta * ASPECT_RATIO;

        const coordinates = {
          latitude: lat,
          longitude: lng,
          latitudeDelta: latDelta,
          longitudeDelta: lngDelta,
        };
        // console.log('coordinate: ', coordinates);
        if (coordinate) {
          await setCoordinate({...coordinates});
        }
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (forMapAddress) {
    if (isLoading) {
      return (
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              height: 150,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#f8f8f8',
            }}>
            {/* <View
              style={{
                width: 100,
                height: 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Loader />
            </View> */}
            <Text style={{fontSize: 14, letterSpacing: 0.3, color: greyLine}}>
              Loading Map ...
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1}}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={{
              height: propsHeight ? propsHeight : 150,
              width: propWidth ? propWidth : '100%',
            }}
            region={coordinate}>
            <Marker
              coordinate={{
                latitude: coordinate.latitude,
                longitude: coordinate.longitude,
              }}
              // title="lovenest"
              // description="lovenest HQ"
            />
          </MapView>
        </View>
      );
    }
  } else {
    return null;
  }
};

export default Maps;
