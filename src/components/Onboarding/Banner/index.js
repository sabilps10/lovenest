import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  Animated,
  Dimensions,
  FlatList,
  Platform,
} from 'react-native';
import {onBoarding} from '../../../utils/Themes/Images';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import {hasNotch} from 'react-native-device-info';

const {bold} = FontType;
const {greyLine, mainGreen} = Colors;
const {width, height} = Dimensions.get('window');
const {onBoarding1, onBoarding2, onBoarding3} = onBoarding;

const bannerImageList = [
  {
    id: 1,
    url: onBoarding1,
    tagLine: 'True love stories never have endings.',
  },
  {
    id: 2,
    url: onBoarding2,
    tagLine: `The highest happiness on earth 
is the happiness of marriage.`,
  },
  {
    id: 3,
    url: onBoarding3,
    tagLine: `Orchestrate your wedding dream,
all in one place`,
  },
];

let initialIndex = 0;

export default class OnBoardingBanner extends Component {
  sliderRef = React.createRef();
  timeId = null;
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.stopAutoPlay();
    this.startAutoPlay();
  }

  componentWillUnmount() {
    this.stopAutoPlay();
  }

  startAutoPlay = () => {
    if (this.props?.timer) {
      this.timerId = setInterval(() => {
        this.goNextIndex();
      }, this.props.timer);
    }
  };

  stopAutoPlay = () => {
    if (this.timerId) {
      clearInterval();
      this.timerId = null;
      initialIndex = 0;
    }
  };

  goNextIndex = () => {
    if (this.sliderRef?.current?.scrollToIndex) {
      if (initialIndex >= bannerImageList.length - 1) {
        initialIndex = 0;
        this.sliderRef.current.scrollToIndex({
          animated: true,
          index: initialIndex,
        });
      } else {
        this.sliderRef.current.scrollToIndex({
          animated: true,
          index: (initialIndex += 1),
        });
      }
    }
  };

  keyExt = (item, index) => `${item.id} Banners`;

  render() {
    const {scrollX} = this.state;
    return (
      <>
        <FlatList
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.state.scrollX,
                  },
                },
              },
            ],
            {useNativeDriver: false},
          )}
          bounces={false}
          ref={this.sliderRef}
          data={bannerImageList}
          extraData={bannerImageList}
          keyExtractor={this.keyExt}
          listKey={this.keyExt}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEnabled
          pagingEnabled
          renderItem={({item, index}) => {
            return (
              <View style={{width, height}}>
                <LinearGradient
                  colors={['transparent', '#FFD8D8']}
                  style={{
                    zIndex: 2,
                    width,
                    height,
                    position: 'absolute',
                    bottom: Platform.OS === 'ios' ? 0 : -60,
                    left: 0,
                    right: 0,
                    backgroundColor: 'transparent',
                  }}
                />
                <View
                  style={{
                    zIndex: 3,
                    position: 'absolute',
                    bottom:
                      Platform.OS === 'ios'
                        ? hasNotch()
                          ? 120
                          : 120
                        : hasNotch()
                        ? 80
                        : 120,
                    left: 0,
                    right: 0,
                    width,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    paddingLeft: 35,
                    paddingRight: 35,
                  }}>
                  <Text
                    style={{
                      lineHeight: 20,
                      textAlign: 'center',
                      fontFamily: bold,
                      color: mainGreen,
                      fontSize: RFPercentage(1.7),
                      letterSpacing: 0.25,
                    }}>
                    {item.tagLine}
                  </Text>
                </View>
                <View
                  style={{
                    zIndex: 1,
                    width,
                    height,
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                  }}>
                  <Image
                    source={item.url}
                    style={{
                      width,
                      height: Platform.OS === 'ios' ? height : height + 30,
                      aspectRatio: width / height,
                    }}
                    resizeMode="cover"
                  />
                </View>
              </View>
            );
          }}
        />
        <View
          style={{
            position: 'absolute',
            bottom: Platform.OS === 'ios' ? (hasNotch() ? 80 : 70) : 70,
            left: 0,
            right: 0,
            backgroundColor: 'transparent',
            width,
            height: 40,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {bannerImageList.map((_, idx) => {
            let color = Animated.divide(
              scrollX,
              Dimensions.get('window').width,
            ).interpolate({
              inputRange: [idx - 1, idx, idx + 1],
              outputRange: [greyLine, mainGreen, greyLine],
              extrapolate: 'clamp',
            });
            return (
              <>
                {bannerImageList.length > 7 ? (
                  <Animated.View
                    key={String(idx)}
                    style={{
                      height: 10,
                      width: 10,
                      backgroundColor: color,
                      margin: 3,
                      borderRadius: 5,
                      borderWidth: 1,
                      borderColor: mainGreen,
                    }}
                  />
                ) : (
                  <Animated.View
                    key={String(idx)}
                    style={{
                      height: 10,
                      width: 10,
                      backgroundColor: color,
                      margin: 4,
                      borderRadius: 5,
                      borderWidth: 1,
                      borderColor: mainGreen,
                    }}
                  />
                )}
              </>
            );
          })}
        </View>
      </>
    );
  }
}
