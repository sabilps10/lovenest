import React from 'react';
import {Text, StatusBar} from 'react-native';
import {Header, Left, Body, Right} from 'native-base';
import ButtonBack from '../../Button/buttonBack';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';

const {black, greyLine, white} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const YearlyCalendarHeader = props => {
  const {popStacking, title, iconX} = props;
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={white} />
      <Header
        androidStatusBarColor={white}
        iosBarStyle="dark-content"
        style={{
          paddingLeft: 0,
          paddingRight: 0,
          backgroundColor: white,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          elevation: 0,
          shadowOpacity: 0,
        }}>
        <Left
          style={{
            flexBasis: '20%',
            left: -10,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ButtonBack
            {...props}
            onPress={() => popStacking()}
            iconX={iconX ? true : false}
          />
        </Left>
        <Body
          style={{
            flexBasis: '60%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            numberOfLines={1}
            style={{
              fontFamily: medium,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
            }}>
            Appointment: {title}
          </Text>
        </Body>
        <Right style={{flexBasis: '20%'}} />
      </Header>
    </>
  );
};

export default YearlyCalendarHeader;
