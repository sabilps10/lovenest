import React from 'react';
import {Text, StatusBar, Dimensions, Image, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';

const {medium, book} = FontType;
const {white, black, greyLine} = Colors;

const Headers = props => {
  const {title, buttonLeft, buttonRight, showRightButton} = props;
  return (
    <>
      <Header
        translucent={false}
        iosBarStyle="dark-content"
        androidStatusBarColor="white"
        style={{
          backgroundColor: white,
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <Left
          style={{
            flex: 0.15,
          }}>
          <Button
            onPress={buttonLeft}
            style={{
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: white,
              alignSelf: 'flex-start',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={
                Platform.OS === 'android'
                  ? {
                      right: 5,
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: black,
                    }
                  : {
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: black,
                    }
              }
            />
          </Button>
        </Left>
        <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 18,
              textAlign: 'center',
            }}>
            {title}
          </Text>
        </Body>
        <Right style={{flex: 0.15}}>
          {showRightButton ? (
            <Button
              onPress={buttonRight}
              style={{
                elevation: 0,
                shadowOpacity: 0,
                backgroundColor: white,
                alignSelf: 'flex-end',
                paddingTop: 0,
                paddingBottom: 0,
                height: 35,
                width: 35,
                justifyContent: 'center',
              }}>
              <Icon
                type="Feather"
                name="home"
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 24,
                  color: black,
                }}
              />
            </Button>
          ) : null}
        </Right>
      </Header>
    </>
  );
};

const Wrapper = compose(withApollo)(Headers);

export default props => <Wrapper {...props} />;
