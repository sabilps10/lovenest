import React from 'react';
import {Text, View, Dimensions, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AnimatedAsyncImage from '../../Image/AnimatedAsyncImage';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';

const {newContainerColor, white, greyLine, black} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

const HeaderImage = props => {
  const {
    name,
    tagline,
    imageSource,
    logoSource,
    containerStyle,
    imageBannerStyle,
  } = props;
  return (
    <Animated.View style={{...containerStyle}}>
      <AnimatedAsyncImage
        resizeMode={'cover'}
        style={{...imageBannerStyle}}
        loaderStyle={{
          width: width / 7,
          height: height / 7,
        }}
        source={imageSource}
        placeholderColor={newContainerColor}
      />
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
          paddingLeft: 15,
          paddingRight: 15,
          paddingTop: 5,
          paddingBottom: 5,
          backgroundColor: white,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <View style={{flex: 0.3, maxHeight: height / 15}}>
          <View style={{position: 'absolute', bottom: 5}}>
            <AnimatedAsyncImage
              resizeMode={'cover'}
              style={{
                width: width / 5.5,
                height: height / 10,
                aspectRatio: width / 5.5 / (height / 10),
                borderWidth: 0.5,
                borderColor: greyLine,
                backgroundColor: white,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={newContainerColor}
            />
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            minHeight: height / 15,
            maxHeight: height / 15,
            paddingRight: 5,
            paddingBottom: 5,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
            }}>
            {name}
          </Text>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
            }}>
            {tagline}
          </Text>
        </View>
      </View>
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(HeaderImage);

export default props => <Wrapper {...props} />;
