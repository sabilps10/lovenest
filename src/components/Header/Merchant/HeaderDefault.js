import React from 'react';
import {Text, View, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Icon, Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';

const {black, white, greyLine} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const HeaderDefault = props => {
  const {navigation, resetProductFilter} = props;
  return (
    <>
      <StatusBar
        translucent={false}
        backgroundColor="white"
        barStyle="dark-content"
      />
      <Header
        androidStatusBarColor={white}
        iosBarStyle="dark-content"
        style={{
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          backgroundColor: white,
        }}>
        <Left style={{flex: 0.2}}>
          <Button
            transparent
            onPress={async () => {
              await resetProductFilter();
              navigation.goBack(null);
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{fontSize: 24, color: black}}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
            }}>
            Merchant Detail
          </Text>
        </Body>
        <Right style={{flex: 0.2}} />
      </Header>
    </>
  );
};

const Wrapper = compose(withApollo)(HeaderDefault);

export default props => <Wrapper {...props} />;
