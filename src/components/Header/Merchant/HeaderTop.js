import React from 'react';
import {Text, Dimensions, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Left, Body, Right} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width} = Dimensions.get('window');
const {white, black, greyLine} = Colors;
const {medium} = FontType;

const HeaderTop = props => {
  const {name, opacity, headerHeight} = props;
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        zIndex: 5,
        height: headerHeight,
        width: width,
        backgroundColor: white,
        flexDirection: 'row',
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
        opacity,
      }}>
      <Left style={{flex: 0.2, height: '100%'}} />
      <Body
        style={{
          flex: 1,
          height: '100%',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <Text
          numberOfLines={1}
          style={{
            bottom: 15,
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {name}
        </Text>
      </Body>
      <Right style={{flex: 0.2, height: '100%'}} />
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(HeaderTop);

export default props => <Wrapper {...props} />;
