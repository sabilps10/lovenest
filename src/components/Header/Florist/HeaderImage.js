import React from 'react';
import {Text, Dimensions, Image, View, Animated, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import ProBadge from '../../../components/Cards/Merchants/ProBadge';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';
import {charImage} from '../../../utils/Themes/Images';

const {white, superGrey} = Colors;
const {book} = FontType;
const {width, height} = Dimensions.get('window');
const {charFloristBGMerchant} = charImage;

// Global Height
import {HEADER_IMAGE_HEIGHT} from '../../../screens/Menu/Florist/Utils';

const HeaderImage = props => {
  const {logoSource, tagline} = props;

  return (
    <Animated.View
      style={{
        width,
        height: HEADER_IMAGE_HEIGHT,
        backgroundColor: white,
        flexDirection: 'column',
      }}>
      <Animated.View
        style={{
          paddingTop:
            Platform.OS === 'ios'
              ? hasNotch()
                ? 90
                : 60
              : hasNotch()
              ? 90
              : 60,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={charFloristBGMerchant}
          style={{
            bottom: 10,
            position: 'absolute',
            zIndex: -1,
            width,
            height: height / 3.1,
          }}
          resizeMode="cover"
        />
        <View
          style={{
            minWidth: width / 2.5,
            maxWidth: width / 2.5,
            minHeight: height / 12.5,
            maxHeight: height / 12.5,
            backgroundColor: 'transparent',
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={logoSource}
            style={{
              width: '100%',
              height: height / 12.5,
              borderRadius: 4,
            }}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              top: 10,
              textAlign: 'center',
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: superGrey,
              letterSpacing: 0.3,
              fontStyle: 'italic',
            }}>
            {tagline}
          </Text>
        </View>
        <View style={{marginVertical: 10, marginTop: 20}}>
          <ProBadge title={'Pro Florist Merchant'} />
        </View>
      </Animated.View>
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(HeaderImage);

export default props => <Wrapper {...props} />;
