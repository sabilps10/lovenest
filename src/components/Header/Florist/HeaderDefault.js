import React from 'react';
import {TouchableOpacity, Dimensions, View, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {Header, Left, Body, Right, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Global Height
import {HEADER_DEFAULT_HEIGHT} from '../../../screens/Menu/Florist/Utils';

// Animated Custom Component
const AnimatedHeader = Animated.createAnimatedComponent(Header);
const AnimatedTouchableOpacity = Animated.createAnimatedComponent(
  TouchableOpacity,
);
const AnimatedIcon = new Animated.createAnimatedComponent(Icon);

const {black, white, headerBorderBottom} = Colors;
const {medium} = FontType;
const {width, height} = Dimensions.get('window');

const HeaderDefault = props => {
  const {
    opacity,
    goBack,
    goToAbout,
    goToFilter,
    goToHome,
    goToCart,
    buttonElevation,
    buttonElevationIOS,
    activeTab,
  } = props;
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 5,
      }}>
      <View
        style={{
          zIndex: 6,
          height: height / 8.7,
          width,
          flexDirection: 'row',
          position: 'absolute',
          top: 0,
          paddingBottom: hasNotch() ? 5 : 10,
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <Left
          style={{
            paddingLeft: 5,
            flex: 0.35,
            alignSelf: 'flex-end',
          }}>
          <AnimatedTouchableOpacity
            onPress={goBack}
            style={{
              width: 30,
              height: 30,
              justifyContent: 'center',
              alignItems: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: buttonElevationIOS,
              shadowRadius: 3.84,
              elevation: buttonElevation,
              borderRadius: 30 / 2,
              backgroundColor: white,
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </AnimatedTouchableOpacity>
        </Left>
        <Body
          style={{
            flex: 1,
            alignSelf: 'flex-end',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 5,
            paddingTop: hasNotch() ? 2 : 5,
          }}>
          <Animated.Text
            style={{
              opacity,
              textAlign: 'center',
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            Fleurs
          </Animated.Text>
        </Body>
        <Right
          style={{
            flex: 0.35,
            alignSelf: 'flex-end',
            paddingRight: 5,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingLeft: 10,
          }}>
          {activeTab === 'Home' || activeTab === 'Products' ? (
            <AnimatedTouchableOpacity
              onPress={() => {
                try {
                  if (activeTab === 'Home') {
                    // Go To Rating
                  } else if (activeTab === 'Products') {
                    // Go To Filter Screen
                    goToFilter();
                  } else {
                    // Do nothing
                  }
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              style={{
                width: 30,
                height: 30,
                alignSelf: 'flex-end',
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowRadius: 1.41,
                shadowOpacity: buttonElevationIOS,
                elevation: buttonElevation,
                borderRadius: 30 / 2,
                backgroundColor: white,
              }}>
              <AnimatedIcon
                type="Feather"
                name={
                  activeTab === 'Home'
                    ? 'edit'
                    : activeTab === 'Products'
                    ? 'sliders'
                    : 'home'
                }
                style={
                  activeTab === 'Products'
                    ? {
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 15,
                        color: black,
                        transform: [{rotate: '90deg'}],
                      }
                    : {
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 15,
                        color: black,
                      }
                }
              />
            </AnimatedTouchableOpacity>
          ) : (
            <AnimatedTouchableOpacity
              disabled
              style={{
                width: 30,
                height: 30,
                alignSelf: 'flex-end',
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowRadius: 1.41,
                shadowOpacity: 0,
                elevation: 0,
                borderRadius: 30 / 2,
                backgroundColor: white,
              }}>
              <Icon
                type="Feather"
                name={
                  activeTab === 'Home'
                    ? 'edit'
                    : activeTab === 'Products'
                    ? 'sliders'
                    : 'home'
                }
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 15,
                  color: white,
                }}
              />
            </AnimatedTouchableOpacity>
          )}
          <AnimatedTouchableOpacity
            onPress={() => {
              if (activeTab === 'Home') {
                // Go to About
                goToAbout();
              } else if (activeTab === 'Products') {
                // Go To cart
                goToCart();
              } else {
                // Go To Home
                goToHome();
              }
            }}
            style={{
              width: 30,
              height: 30,
              alignSelf: 'flex-end',
              justifyContent: 'center',
              alignItems: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowRadius: 1.41,
              shadowOpacity: buttonElevationIOS,
              elevation: buttonElevation,
              borderRadius: 30 / 2,
              backgroundColor: white,
            }}>
            <Icon
              type={
                activeTab === 'Promotions' ||
                activeTab === 'Portfolios' ||
                activeTab === 'Products'
                  ? 'Feather'
                  : 'MaterialCommunityIcons'
              }
              name={
                activeTab === 'Promotions' || activeTab === 'Portfolios'
                  ? 'home'
                  : activeTab === 'Products'
                  ? 'shopping-bag'
                  : 'store'
              }
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 20,
                color: black,
              }}
            />
          </AnimatedTouchableOpacity>
        </Right>
      </View>
      <Animated.View
        style={{
          opacity,
        }}>
        <AnimatedHeader
          translucent
          iosBarStyle="dark-content"
          androidStatusBarColor="transparent"
          style={{
            elevation: 0,
            shadowOpacity: 0,
            width,
            height: HEADER_DEFAULT_HEIGHT,
            backgroundColor: white,
            borderBottomWidth: 1,
            borderBottomColor: headerBorderBottom,
            paddingBottom: 10,
          }}
        />
      </Animated.View>
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(HeaderDefault);

export default props => <Wrapper {...props} />;
