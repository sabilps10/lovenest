import React from 'react';
import {Text, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {Header, Left, Body, Right, Icon, Button} from 'native-base';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';

const {white, greyLine, black} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const HeaderPayment = props => {
  const {leftButton, Title, rightButton} = props;

  return (
    <>
      <StatusBar
        translucent={false}
        barStyle="dark-content"
        backgroundColor="white"
        animated
      />
      <Header
        translucent={false}
        iosBarStyle={'dark-content'}
        androidStatusBarColor={white}
        style={{
          backgroundColor: white,
          shadowOpacity: 0,
          elevation: 0,
          borderBottomWidth: 0.5,
          borderBottomColor: greyLine,
        }}>
        <Left style={{flex: 0.3}}>
          <Button transparent onPress={() => leftButton()}>
            <Icon
              type="MaterialCommunityIcons"
              name="history"
              style={{fontSize: 25, color: black}}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
            {Title}
          </Text>
        </Body>
        <Right style={{flex: 0.3}}>
          <Button transparent onPress={() => rightButton()}>
            <Icon
              type="Feather"
              name="credit-card"
              style={{fontSize: 25, color: black}}
            />
          </Button>
        </Right>
      </Header>
    </>
  );
};

const Wrapper = compose(withApollo)(HeaderPayment);

export default props => <Wrapper {...props} />;
