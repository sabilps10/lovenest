import React from 'react';
import {Text, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';

const {white, greyLine, black} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const DefaultHeader = props => {
  const {Title} = props;
  return (
    <>
      <StatusBar translucent={false} backgroundColor="white" animated />
      <Header
        translucent={false}
        iosBarStyle="dark-content"
        androidStatusBarColor={white}
        style={{
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          elevation: 0,
          shadowOpacity: 0,
          backgroundColor: 'white',
        }}>
        <Left style={{flex: 0.3}} />
        <Body
          style={{
            flex: 1,
            minHeight: 40,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
            {Title}
          </Text>
        </Body>
        <Right style={{flex: 0.3}} />
      </Header>
    </>
  );
};

const Wrapper = compose(withApollo)(DefaultHeader);

export default props => <Wrapper {...props} />;
