import React from 'react';
import {
  View,
  FlatList,
  Dimensions,
  Animated,
  Linking,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../Image/AsyncImage';
import {Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

const AnimatedCard = Animated.createAnimatedComponent(Card);

const {greyLine, white} = Colors;

let initialIndex = 0;

export const CardSliders = props => {
  const {link, openLink, imageStyle, imageLoaderStyle, logoSource} = props;
  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPressIn={pressIn}
      onPressOut={pressOut}
      disabled={link ? false : true}
      onPress={() => {
        openLink();
      }}>
      <AnimatedCard
        transparent
        style={{
          right: Platform.OS === 'android' ? 2 : 0,
          marginTop: 0,
          marginLeft: 0,
          marginRight: 0,
          paddingLeft: 15,
          paddingRight: 15,
          width: Dimensions.get('window').width,
          transform: [{scale: scale}],
        }}>
        <CardItem
          cardBody
          style={{
            backgroundColor: 'transparent',
            borderRadius: 4,
            elevation: 2,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.25,
            shadowRadius: 2,
          }}>
          <AsyncImage
            resizeMode="cover"
            style={{
              ...imageStyle,
            }}
            loaderStyle={{
              ...imageLoaderStyle,
            }}
            source={logoSource}
            placeholderColor={'#f8f8f8'}
          />
        </CardItem>
      </AnimatedCard>
    </TouchableOpacity>
  );
};

class Slider extends React.Component {
  sliderRef = React.createRef();
  timeId = null;
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  componentDidMount() {
    console.log('PROPS BANNER: ', this.props);
    this.stopAutoPlay();
    if (this.props.data) {
      this.startAutoPlay();
    }
  }

  componentWillUnmount() {
    this.stopAutoPlay();
  }

  startAutoPlay = () => {
    if (this.props.timer) {
      this.timerId = setInterval(() => {
        this.goNextIndex();
      }, this.props.timer);
    }
  };

  stopAutoPlay = () => {
    if (this.timerId) {
      clearInterval();
      this.timerId = null;
      initialIndex = 0;
    }
  };

  goNextIndex = () => {
    if (this.sliderRef) {
      if (this.sliderRef.current) {
        if (this.sliderRef.current.scrollToIndex) {
          if (initialIndex >= this.props.data.length - 1) {
            initialIndex = 0;
            this.sliderRef.current.scrollToIndex({
              animated: true,
              index: initialIndex,
            });
          } else {
            this.sliderRef.current.scrollToIndex({
              animated: true,
              index: (initialIndex += 1),
            });
          }
        }
      }
    }
  };

  openLink = link => {
    Linking.canOpenURL(link).then(supported => {
      if (supported) {
        Linking.openURL(link);
      } else {
        console.log(`Don't know how to open URI: ${link}`);
      }
    });
  };

  render() {
    console.log('Slider Banner Props: ', this.props);
    const {data} = this.props;
    const {scrollX} = this.state;
    if (data === undefined || data === null) {
      return null;
    } else {
      return (
        <>
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: this.state.scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            ref={this.sliderRef}
            data={this.props.data}
            initialScrollIndex={0}
            onScrollToIndexFailed={() => {
              const wait = new Promise(resolve => setTimeout(resolve, 500));
              wait.then(() => {
                this.sliderRef.current?.scrollToIndex({
                  index: 0,
                  animated: true,
                });
              });
            }}
            showsHorizontalScrollIndicator={false}
            horizontal
            scrollEnabled
            pagingEnabled
            keyExtractor={(_, index) => `${String(index)} Sliders`}
            renderItem={({item}) => {
              const logoSource =
                item.dynamicUrl === null
                  ? {uri: item.url}
                  : {uri: `${item.dynamicUrl}=h500`};
              const {link} = item;
              return (
                <CardSliders
                  link={link}
                  openLink={() => {
                    try {
                      if (link === 'Florist' || link === 'florist') {
                        this.props.navigation?.navigate('Florist');
                      } else {
                        this.openLink(link);
                      }
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                  logoSource={logoSource}
                  imageLoaderStyle={{...this.props.imageLoaderStyle}}
                  imageStyle={{...this.props.imageStyle}}
                />
              );
            }}
          />
          {this.props.showIndicator ? (
            <View
              style={{
                ...this.props.indicatorStyle,
              }}>
              {this.props.data.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });

                let widths = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [7, 20, 7],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {this.props.data.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: widths,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 5,
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 3.84,

                          elevation: 5,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: widths,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 5,
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 3.84,

                          elevation: 5,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
          ) : null}
        </>
      );
    }
  }
}

const Wrapper = compose(withApollo)(Slider);

export default props => <Wrapper {...props} />;
