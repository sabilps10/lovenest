import React from 'react';
import {View, Dimensions, Image, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {commonImage} from '../../../utils/Themes/Images';
import ButtonSeeAllProduct from './ButtonSeeAllProduct';
import CardProduct from '../../Cards/Merchants/ProductCard';

const {loveCrown} = commonImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const PublicProduct = props => {
  const {navigation, publicProducts} = props;
  return (
    <View
      style={{
        position: 'relative',
        top: -100,
        width: widthScreen,
        height: heightScreen / 2.3,
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0.25}}
        end={{x: 1.9, y: 5}}
        locations={[0.0, 0.2, 0.6]}
        colors={['#FFEDED', 'white', 'white']}
        style={{flex: 1, zIndex: 1}}
      />
      <Image
        source={loveCrown}
        style={{
          position: 'absolute',
          top: 10,
          left: 0,
          zIndex: 2,
          width: 43,
          height: 54,
        }}
        resizeMode="contain"
      />
      <ButtonSeeAllProduct {...props} />
      <View
        style={{
          width: '100%',
          position: 'absolute',
          top: 45,
          zIndex: 4,
        }}>
        <FlatList
          legacyImplementation
          disableVirtualization
          numColumns={2}
          contentContainerStyle={{
            padding: 15,
            paddingBottom: 15,
            paddingTop: 0,
          }}
          columnWrapperStyle={{
            justifyContent: 'space-between',
          }}
          data={publicProducts}
          extraData={publicProducts}
          keyExtractor={item => String(item.id)}
          renderItem={({item}) => {
            const {
              featuredImageDynamicURL,
              featuredImageURL,
              name,
              id: productId,
              merchantDetails,
            } = item;
            const source =
              featuredImageDynamicURL === null
                ? {uri: featuredImageURL}
                : {uri: `${featuredImageDynamicURL}=h500`};
            const {id} = merchantDetails;
            return (
              <CardProduct
                label={'Product'}
                name={name}
                source={source}
                merchantId={id}
                productId={productId}
                navigation={navigation}
              />
            );
          }}
        />
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(PublicProduct);

export default props => <Wrapper {...props} />;
