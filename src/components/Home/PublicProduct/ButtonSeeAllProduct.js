import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';

const {black, mainRed, white} = Colors;
const {medium} = FontType;

const ButtonSeeAllProduct = props => {
  const {navigation, disableButton, title, related} = props;

  const goToProducts = () => {
    try {
      // prepare params id and serviceType
      navigation.navigate('Products', {id: null, serviceType: 'Bridal'});
    } catch (error) {
      console.log('Error go to products: ', error);
    }
  };

  return (
    <View
      style={{
        paddingLeft: 15,
        paddingRight: 15,
        position: 'absolute',
        top: 15,
        zIndex: 3,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <View>
        <Text
          style={{
            fontFamily: medium,
            color: related ? white : black,
            fontSize: RFPercentage(1.8),
          }}>
          {title ? title : 'Graze your wedding looks'}
        </Text>
      </View>
      {disableButton ? null : (
        <TouchableOpacity onPress={() => goToProducts()}>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}>
            <Text
              style={{
                left: -10,
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              See All
            </Text>
            <Icon
              type="Feather"
              name="arrow-right"
              style={{
                right: 5,
                marginLeft: 5,
                fontSize: 18,
                color: mainRed,
              }}
            />
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

const Wrapper = compose(withApollo)(ButtonSeeAllProduct);

export default props => <Wrapper {...props} />;
