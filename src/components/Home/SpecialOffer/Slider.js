import React from 'react';
import {Text, FlatList, Dimensions, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import CardSpecialOffer from '../../Home/SpecialOffer/CardSpecialOffer';

const {black, transparent, mainRed} = Colors;
const {medium} = FontType;

const SliderSpecialOffer = props => {
  console.log('SliderSpecialOffer Props: ', props);
  const {data, totalCount, navigation} = props;
  if (data) {
    return (
      <>
        {data.length === 0 ? null : (
          <Card transparent>
            <CardItem
              style={{
                backgroundColor: transparent,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2.2),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Special Offers
              </Text>
              {totalCount === 0 || totalCount <= 5 ? null : (
                <TouchableOpacity
                  style={{left: 25}}
                  onPress={() => {
                    navigation.navigate('SpecialOffer');
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.9),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      See all
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{
                        marginLeft: 5,
                        fontSize: RFPercentage(2),
                        color: mainRed,
                      }}
                    />
                  </View>
                </TouchableOpacity>
              )}
            </CardItem>
          </Card>
        )}
        <FlatList
          snapToInterval={Dimensions.get('window').width / 1.37}
          decelerationRate={'normal'}
          snapToAlignment={'center'}
          contentContainerStyle={{
            paddingLeft: 16.5,
            paddingRight: 5,
            marginBottom: 10,
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={data}
          keyExtractor={(_, index) => `${String(index)} Special Offer`}
          renderItem={({item}) => {
            return <CardSpecialOffer navigation={navigation} data={item} />;
          }}
        />
      </>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(SliderSpecialOffer);

export default props => <Wrapper {...props} />;
