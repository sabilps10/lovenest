import React from 'react';
import {
  Dimensions,
  TouchableOpacity,
  Animated,
  Linking,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';
import Colors from '../../../utils/Themes/Colors';
import {hasNotch} from 'react-native-device-info';

const {greyLine, newContainerColor} = Colors;
const AnimatedCard = Animated.createAnimatedComponent(Card);
const {width, height} = Dimensions.get('window');

const CardSpeicalOffer = props => {
  console.log('CardSpeicalOffer Props: ', props);
  const {data, navigation} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (data) {
    const {promoLinkURL} = data;
    const logoSource =
      data.promoImageDynamicURL === null
        ? {uri: data.promoImageURL}
        : {uri: `${data.promoImageDynamicURL}=h500`};

    // This function disable since promo detail screen
    // const openLink = () => {
    //   Linking.canOpenURL(promoLinkURL).then(supported => {
    //     if (supported) {
    //       Linking.openURL(promoLinkURL);
    //     } else {
    //       console.log(`Don't know how to open URI: ${promoLinkURL}`);
    //     }
    //   });
    // };
    return (
      <>
        <TouchableOpacity
          onPress={() => {
            // openLink()
            try {
              navigation.navigate('SpecialOfferDetail', {id: data?.id});
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          // disabled={promoLinkURL ? false : true}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            transparent
            style={{
              minWidth: width / 1.41,
              maxWidth: width / 1.41,
              // height: 135,
              borderRadius: 4,
              marginRight: 15,
              shadowOpacity: 0,
              elevation: 0,
              borderWidth: 0.5,
              borderColor: greyLine,
              transform: [{scale: scale}],
            }}>
            <CardItem
              cardBody
              style={{
                // width: '100%',
                // height: 135.67,
                backgroundColor: 'transparent',
              }}>
              <AsyncImage
                resizeMode={'cover'}
                style={{
                  // flex: 1,
                  width: width / 1.41,
                  height:
                    Platform.OS === 'ios'
                      ? hasNotch()
                        ? height / 6.2
                        : height / 5.2
                      : height / 5.2,
                  borderRadius: 4,
                }}
                loaderStyle={{
                  width: width / 7,
                  height: height / 7,
                }}
                source={logoSource}
                placeholderColor={greyLine}
              />
            </CardItem>
          </AnimatedCard>
        </TouchableOpacity>
      </>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(CardSpeicalOffer);

export default props => <Wrapper {...props} />;
