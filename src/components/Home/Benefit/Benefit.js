import React from 'react';
import {Text, View, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import CardBenefit from './CardBenefit';

const {black, transparent, mainRed} = Colors;
const {medium} = FontType;

const Benefit = props => {
  console.log('Benefit Props: ', props);
  const {data, navigation, totalCount} = props;

  if (data) {
    return (
      <>
        {data.length === 0 ? null : (
          <Card transparent>
            <CardItem
              style={{
                backgroundColor: transparent,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2.2),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Love Nest Benefits
              </Text>
              {totalCount === 0 || totalCount <= 4 ? null : (
                <TouchableOpacity
                  style={{left: 25}}
                  onPress={() => {
                    navigation.navigate('Benefits');
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.9),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      See all
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{
                        marginLeft: 5,
                        fontSize: RFPercentage(2),
                        color: mainRed,
                      }}
                    />
                  </View>
                </TouchableOpacity>
              )}
            </CardItem>
          </Card>
        )}
        <FlatList
          contentContainerStyle={{
            paddingLeft: 15,
            paddingRight: 10,
            marginBottom: 80,
          }}
          numColumns={2}
          columnWrapperStyle={{
            marginBottom: 8,
            justifyContent: 'space-between',
          }}
          data={data}
          listKey={(_, index) => `${String(index)} LN Benefits`}
          renderItem={({item, index}) => {
            return <CardBenefit data={item} />;
          }}
        />
      </>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(Benefit);

export default props => <Wrapper {...props} />;
