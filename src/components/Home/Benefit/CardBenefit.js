import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Animated,
  Linking,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import AsyncImage from '../../Image/AsyncImage';
import Colors from '../../../utils/Themes/Colors';

const {newContainerColor, greyLine} = Colors;
const AnimatedCard = Animated.createAnimatedComponent(Card);

const CardBenefit = props => {
  console.log('CardBenefit Props: ', props);
  const {data} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (data) {
    const {link} = data;
    const logoSource =
      data.dynamicUrl === null
        ? {uri: data.url}
        : {uri: `${data.dynamicUrl}=h500`};

    const openLink = () => {
      Linking.canOpenURL(link).then(supported => {
        if (supported) {
          Linking.openURL(link);
        } else {
          console.log(`Don't know how to open URI: ${link}`);
        }
      });
    };
    return (
      <TouchableOpacity
        onPress={() => openLink()}
        disabled={link ? false : true}
        activeOpacity={1}
        onPressIn={pressIn}
        onPressOut={pressOut}>
        <AnimatedCard
          transparent
          style={{
            minWidth: Dimensions.get('window').width / 2.35,
            maxWidth: Dimensions.get('window').width / 2.35,
            height: 163,
            borderRadius: 4,
            marginRight: 10,
            shadowOpacity: 0,
            elevation: 0,
            borderWidth: 0.5,
            borderColor: greyLine,
            transform: [{scale: scale}],
          }}>
          <CardItem
            cardBody
            style={{
              width: '100%',
              height: 163,
              backgroundColor: greyLine,
            }}>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                flex: 1,
                width: '100%',
                height: 163,
                borderRadius: 4,
              }}
              loaderStyle={{
                width: Dimensions.get('window').width / 7,
                height: Dimensions.get('window').width / 7,
              }}
              source={logoSource}
              placeholderColor={greyLine}
            />
          </CardItem>
        </AnimatedCard>
      </TouchableOpacity>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(CardBenefit);

export default props => <Wrapper {...props} />;
