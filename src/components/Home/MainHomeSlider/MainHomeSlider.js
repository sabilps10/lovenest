import React from 'react';
import {View, Dimensions, ActivityIndicator} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import BannerSlider from '../../Home/Banner/Slider';
import Colors from '../../../utils/Themes/Colors';
import {hasNotch} from 'react-native-device-info';
import {Platform} from 'react-native';

const {mainGreen} = Colors;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const HomeMainSliderCard = props => {
  const {isLoadingBanner, isErrorBanner, banners, navigation} = props;
  return (
    <View
      style={{
        width: widthScreen,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {isLoadingBanner ? (
        // <View
        //   style={{
        //     width: '100%',
        //     height: heightScreen / 4,
        //     justifyContent: 'center',
        //     alignItems: 'center',
        //   }}>
        //   <ActivityIndicator
        //     style={{bottom: 15}}
        //     animating={isLoadingBanner}
        //     size="large"
        //     color={mainGreen}
        //   />
        // </View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 15,
            bottom: 5,
          }}>
          <View
            style={{
              borderRadius: 3,
              width: '100%',
              height: widthScreen * 0.45,
              backgroundColor: Colors?.lightBlueGrey,
            }}
          />
        </View>
      ) : isErrorBanner ? null : (
        <BannerSlider
          navigation={navigation}
          showIndicator={true}
          imageStyle={{
            flex: 1,
            width: '100%',
            height: widthScreen * 0.45,
            backgroundColor: Colors?.lightBlueGrey,
            // Platform.OS === 'ios'
            //   ? hasNotch()
            //     ? heightScreen / 4.5
            //     : heightScreen / 4
            //   : hasNotch()
            //   ? heightScreen / 4.1
            //   : heightScreen / 4,
            borderRadius: 4,
          }}
          imageLoaderStyle={{
            width: '100%',
            height: widthScreen * 0.45,
            // borderWidth: 1,
          }}
          data={banners}
          indicatorStyle={{
            left: 30,
            top: -28,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
          }}
          timer={3000}
        />
      )}
    </View>
  );
};

const Wrapper = compose(withApollo)(HomeMainSliderCard);

export default props => <Wrapper {...props} />;
