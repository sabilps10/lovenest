import React from 'react';
import {View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import UpComingEventShimmer from '../../Loader/Home/upComingEventShimmer';
import EventSlider from '../../Event/SliderEvent';

const Events = props => {
  const {isLoadingUpComingEvent, isErrorUpComingEvent, upComingEvent} = props;
  return (
    <>
      {isLoadingUpComingEvent ? (
        <View
          style={{
            marginVertical: 15,
          }}>
          <UpComingEventShimmer />
        </View>
      ) : isErrorUpComingEvent ? null : (
        <View
          style={{
            marginVertical: 5,
            marginTop: 5,
          }}>
          <EventSlider data={upComingEvent} {...props} />
        </View>
      )}
    </>
  );
};

const Wrapper = compose(withApollo)(Events);

export default props => <Wrapper {...props} />;
