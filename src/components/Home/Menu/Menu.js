/* eslint-disable no-unused-vars */
import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  FlatList,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';
import {connect} from 'react-redux';
import ReduxProductFilter from '../../../redux/thunk/ProductFilterThunk';
import ReduxProductFilterStatus from '../../../redux/thunk/ProductFilterStatusThunk';
const {width: widths} = Dimensions.get('screen');
import Colors from '../../../utils/Themes/Colors';
import GetMenuListAPI from '../../../graphql/queries/merchantCategoryMobile';
import Icon from 'react-native-vector-icons/Feather';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import MenuShimmer from '../../Loader/Home/menuShimmer';

const {medium} = FontType;

const Menu = props => {
  console.log('Menu props: ', props);
  const {navigation, client, productFilterStatus, productFilter} = props;

  const [filterList, setFilterList] = useState([]);
  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  const [NewMenuList, setNewMenuList] = useState(null);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetchProductFilterList();
    fetchMenuList();
    const subscriber = navigation.addListener('focus', () => {
      fetchProductFilterList();
    });

    return subscriber;
  }, [navigation]);

  useEffect(() => {
    if (refreshing) {
      reloadMenu();
    }
  }, [refreshing]);

  const reloadMenu = () => {
    try {
      fetchMenuList();
    } catch (error) {
      setRefreshing(false);
    }
  };

  const fetchMenuList = () => {
    try {
      props?.client
        ?.query({
          query: GetMenuListAPI,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('Res Menu List: ', res);

          const {data, errors} = res;
          const {merchantCategoryMobile} = data;
          const {list1, list2, list3, error} = merchantCategoryMobile;

          if (errors || error) {
            throw new Error(errors);
          } else {
            setNewMenuList({list1, list2, list3});
            setIsError(false);
            setIsLoading(false);
            setRefreshing(false);
          }
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
    }
  };

  const updateFilter = listDataFilterFromAPI => {
    return new Promise(async resolve => {
      try {
        const manipulate = await Promise.all(
          listDataFilterFromAPI.map(async d => {
            const listManipulate = await d.list.map(a => {
              if (a.name === 'Suit') {
                return {
                  name: a.name,
                  checked: true,
                };
              } else {
                return {
                  ...a,
                };
              }
            });
            console.log('listManipulate: ', listManipulate);
            if (listManipulate) {
              return {
                name: d.name,
                list: listManipulate,
              };
            }
          }),
        );

        console.log('manipulate kontol >>> ', manipulate);

        if (manipulate.length === listDataFilterFromAPI.length) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log('Error update Filter: ', error);
        resolve(false);
      }
    });
  };

  const fetchProductFilterList = () => {
    try {
      client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            merchantId: null,
            serviceType: 'Bridal',
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('FILTER DATA RESPONSE: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: list, error} = productsFilter;

          if (errors) {
          } else {
            if (error) {
            } else {
              const filterManipulation = await updateFilter(list);
              await setFilterList([...filterManipulation]);
              await setSelectedType(['Suit']);
              await setSelectedCategory([]);
              await setSelectedColor([]);
              await setSelectedRange([]);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
        });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const navigateSpecificMenu = async navigationName => {
    console.log('navigationName: >>> ', navigationName);
    try {
      if (navigationName === 'Jewellery') {
        navigation.navigate(navigationName, {
          serviceType: navigationName,
          id: null,
        });
      } else if (navigationName === 'Venue') {
        navigation.navigate(navigationName, {
          serviceType: navigationName,
          id: null,
        });
      } else if (navigationName === 'Suit') {
        navigation.navigate(navigationName, {
          serviceType: 'Bridal',
          id: null,
        });
      } else if (navigationName === 'Interior Design') {
        navigation.navigate('InteriorDesign', {
          id: null,
          serviceType: 'Interior Design',
        });
      } else if (navigationName === 'Smart Home') {
        navigation.navigate('SmartHome', {
          id: null,
          serviceType: 'Smart Home',
        });
      } else {
        navigation.navigate(navigationName);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const navigateCategoryMenu = data => {
    try {
      props?.navigation?.navigate('CategoryMenu', {data});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: '100%',
          minHeight: widths * 0.3,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 0,
        }}>
        {/* <ActivityIndicator size="small" color={Colors?.mainGreen} /> */}
        <FlatList
          data={new Array(8).fill().map((e, i) => {
            return {id: i};
          })}
          numColumns={4}
          scrollEnabled={false}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  borderWidth: 0,
                  width: widths / 4,
                  height: widths * 0.25,
                }}>
                <MenuShimmer />
              </View>
            );
          }}
        />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          width: '100%',
          height: widths * 0.3,
          borderWidth: 0,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => setRefreshing(true)}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          {refreshing ? (
            <ActivityIndicator
              size="small"
              color={Colors?.mainGreen}
              style={{marginBottom: 15}}
            />
          ) : (
            <Icon
              size={RFPercentage(2)}
              name="refresh-ccw"
              color={Colors?.mainRed}
              style={{marginBottom: 10}}
            />
          )}
          <Text>{refreshing ? 'Refreshing...' : 'Refresh'}</Text>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View>
        <MenuComponent
          NewMenuList={NewMenuList}
          navigationList1={navigationName =>
            navigateCategoryMenu(navigationName)
          }
          navigationList2And3={item => navigateCategoryMenu(item)}
        />
      </View>
    );
  }
};

const scrollElementWidthPercent = 45;

export const MenuComponent = props => {
  const {NewMenuList, navigationList1, navigationList2And3} = props;
  const [contentOffset, setContentOffset] = React.useState({x: 0, y: 0});
  const [contentSize, setContentSize] = React.useState(0);
  const [scrollViewWidth, setScrollViewWidth] = React.useState(0);

  const scrollPerc =
    (contentOffset.x / (contentSize - scrollViewWidth)) *
    (100 - scrollElementWidthPercent);

  return (
    <View>
      <ScrollView
        scrollEventThrottle={16}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        onScroll={e => {
          setContentOffset(e.nativeEvent.contentOffset);
        }}
        onContentSizeChange={(width, height) => {
          setContentSize(width);
        }}
        onLayout={e => {
          setScrollViewWidth(e.nativeEvent.layout.width);
        }}>
        {/* List 1 */}
        <FlatList
          scrollEnabled={false}
          style={{right: 10}}
          data={NewMenuList?.list1}
          extraData={NewMenuList?.list1}
          numColumns={4}
          keyExtractor={(item, index) => `${item?.id}`}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() => navigationList1(item)}
                style={{width: widths / 4, borderWidth: 0, padding: 5}}>
                <View
                  style={{
                    width: '100%',
                    height: widths * 0.25,
                    backgroundColor: '#ffffff',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={{uri: item?.icon}}
                    style={{
                      top: item?.menuName === 'Homebotic' ? -5 : 0,
                      height: widths * 0.18,
                      width: '100%',
                      marginBottom: 5,
                    }}
                    resizeMethod="auto"
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.5),
                      color: Colors?.black,
                      textAlign: 'center',
                      position: 'absolute',
                      right: 0,
                      bottom: 10,
                      left: 0,
                    }}>
                    {item?.menuName}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
        {/* List 2 */}
        <FlatList
          scrollEnabled={false}
          columnWrapperStyle={{paddingLeft: 2, paddingRight: 2}}
          style={{right: 10}}
          data={NewMenuList?.list2}
          extraData={NewMenuList?.list2}
          numColumns={4}
          keyExtractor={(item, index) => `${item?.id}`}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() => navigationList2And3(item)}
                style={{width: widths / 4, borderWidth: 0, padding: 6}}>
                <View
                  style={{
                    width: '100%',
                    height: widths * 0.25,
                    backgroundColor: '#ffffff',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={{uri: item?.icon}}
                    style={{
                      height: widths * 0.18,
                      width: '100%',
                      marginBottom: 5,
                    }}
                    resizeMethod="auto"
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.5),
                      color: Colors?.black,
                      textAlign: 'center',
                      position: 'absolute',
                      bottom:
                        item?.menuName === 'Editor Choice Award' ||
                        item?.menuName === 'Chinese Wedding Traditional'
                          ? 0
                          : 0,
                      left: 0,
                      right: 0,
                      lineHeight: 14,
                    }}>
                    {item?.menuName === 'Chinese Wedding Traditional'
                      ? 'Chinese\nWedding'
                      : item?.menuName === 'Car Rental'
                      ? 'Car\nRental'
                      : item?.menuName === 'Editor Choice Award'
                      ? 'Editor\nChoice'
                      : item?.menuName === 'Beauty Wellness'
                      ? 'Beauty\nWellness'
                      : item?.menuName}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
        {/* List 3 */}
        <FlatList
          scrollEnabled={false}
          style={{right: 10}}
          data={NewMenuList?.list3}
          extraData={NewMenuList?.list3}
          numColumns={4}
          keyExtractor={(item, index) => `${item?.id}`}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                onPress={() => navigationList2And3(item)}
                style={{width: widths / 4, borderWidth: 0, padding: 5}}>
                <View
                  style={{
                    width: '100%',
                    height: widths * 0.25,
                    backgroundColor: '#ffffff',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={{uri: item?.icon}}
                    style={{
                      height: widths * 0.18,
                      width: '100%',
                      marginBottom: 5,
                    }}
                    resizeMethod="auto"
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.5),
                      color: Colors?.black,
                      textAlign: 'center',
                      position: 'absolute',
                      bottom:
                        item?.menuName === 'Hair & Makeup' ||
                        item?.menuName === 'Photo & Videos'
                          ? 10
                          : 10,
                      left: 0,
                      right: 0,
                    }}>
                    {item?.menuName}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </ScrollView>
      <View
        style={{
          width: '100%',
          paddingTop: 15,
          paddingBottom: 10,
          alignItems: 'center',
        }}>
        <View
          style={{
            width: 80,
            height: 6,
            borderRadius: 5,
            backgroundColor: 'lightgray',
          }}>
          <View
            style={{
              position: 'absolute',
              left: `${Number(scrollPerc || 0).toFixed(0)}%`,
              width: `${scrollElementWidthPercent}%`,
              height: 6,
              borderRadius: 5,
              backgroundColor: Colors?.mainGreen,
            }}
          />
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {};
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxProductFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    productFilterStatus: status => dispatch(ReduxProductFilterStatus(status)),
  };
};

const ConnectComponent = connect(mapStateToProps, mapDispatchToProps)(Menu);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
