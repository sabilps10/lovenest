import React from 'react';
import {
  Text,
  Platform,
  Dimensions,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const heightMenuCard =
  Platform.OS === 'ios'
    ? hasNotch()
      ? heightScreen / 7.7
      : heightScreen / 6.7
    : heightScreen / 8.7;

const MenuCard = props => {
  const {name, tag, icon, onNavigate} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <TouchableOpacity
      style={{right: 8}}
      activeOpacity={1}
      onPress={() => onNavigate(tag)}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <Animated.View
        key={tag}
        style={{
          flexDirection: 'column',
          width: widthScreen / 4.1,
          alignItems: 'center',
          height: heightMenuCard,
          padding: 5,
          transform: [{scale: scale}],
        }}>
        <Image
          source={icon}
          style={{
            width: 52,
            height: 52,
            marginBottom: 10,
          }}
        />
        <Text
          style={{
            bottom: 5,
            lineHeight: 20,
            textAlign: 'center',
            fontSize: RFPercentage(1.7),
          }}>
          {name}
        </Text>
      </Animated.View>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(MenuCard);

export default props => <Wrapper {...props} />;
