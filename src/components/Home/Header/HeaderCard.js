import React from 'react';
import {
  View,
  TouchableOpacity,
  Dimensions,
  Platform,
  Image,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {hasNotch} from 'react-native-device-info';
import {QRCode, commonImage} from '../../../utils/Themes/Images';
import {Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';

const {black, mainRed} = Colors;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const gradientColor = ['#FFEDED', '#FACFC7', '#FACFC7'];
const {QRCodePointViewFrame} = QRCode;
const {bellNotificationIcon, homeTextLogin, lnTextHome} = commonImage;

const HeaderCard = props => {
  const {navigation, isLogin, onPress, badge} = props;
  const goToNotificationScreen = () => {
    try {
      navigation.navigate('Notification');
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToQRScanScreen = () => {
    try {
      navigation.navigate('QR');
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View
      style={{
        position: 'relative',
        top: 0,
        zIndex: 1,
        width: widthScreen,
        height: heightScreen / 3.8,
        flexDirection: 'column',
      }}>
      <LinearGradient
        colors={gradientColor}
        style={{
          flex: 1,
          height: '100%',
          flexDirection: 'column',
          paddingLeft: 15,
          paddingRight: 15,
        }}>
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            top:
              Platform.OS === 'android'
                ? hasNotch()
                  ? 50
                  : 50
                : hasNotch()
                ? 60
                : 50,
          }}>
          <View>
            <Image
              source={lnTextHome}
              style={{width: widthScreen / 4, height: 28}}
              resizeMode="contain"
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => goToQRScanScreen()}
              style={{marginHorizontal: 7}}>
              <Image
                source={QRCodePointViewFrame}
                style={{width: 24, height: 24}}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => goToNotificationScreen()}
              style={{marginHorizontal: 7}}>
              <Image
                source={bellNotificationIcon}
                style={{width: 24, height: 24}}
                resizeMode="contain"
              />
            </TouchableOpacity>
            {isLogin ? null : (
              <TouchableOpacity
                onPress={() => onPress()}
                style={{marginLeft: 7}}>
                <Image
                  source={homeTextLogin}
                  style={{width: 43, height: 24}}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            )}
            {!isLogin ? null : (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('FloristCart');
                }}
                style={{marginLeft: 7}}>
                {badge > 0 ? (
                  <View
                    style={{
                      zIndex: 99,
                      borderWidth: 1,
                      borderColor: 'white',
                      width: 10,
                      height: 10,
                      borderRadius: 10 / 2,
                      backgroundColor: mainRed,
                      position: 'absolute',
                      top: 0,
                      right: 0,
                    }}
                  />
                ) : null}
                <Icon
                  type="Feather"
                  name="shopping-bag"
                  style={{top: 2, fontSize: 18, color: black}}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

const Wrapper = compose(withApollo)(HeaderCard);

export default props => <Wrapper {...props} />;
