import React from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';
import {CardItem, Left, Body, Button, Icon, Right} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import DatePicker from '../../components/Picker/dateAndTimePicker';
import moment from 'moment';

const {black, transparent, brownGrey} = Colors;
const {medium} = FontType;
const {small} = FontSize;

const DatePickerCustom = props => {
  const {
    title,
    value,
    done,
    cancel,
    openPicker,
    openStatus,
    placeholder,
    clearDate,
  } = props;

  const showWidth =
    clearDate === undefined && value === ''
      ? '100%'
      : clearDate !== undefined && value === ''
      ? '100%'
      : clearDate === undefined && value !== ''
      ? '100%'
      : clearDate !== undefined && value !== ''
      ? '90%'
      : '100%';

  const showX =
    clearDate === undefined && value === ''
      ? false
      : clearDate !== undefined && value === ''
      ? false
      : clearDate === undefined && value !== ''
      ? false
      : clearDate !== undefined && value !== ''
      ? true
      : false;

  return (
    <CardItem style={styles.CardItem}>
      <Left style={styles.Left}>
        <Text style={styles.Title}>{title}</Text>
      </Left>
      <Body
        style={{
          ...styles.Body,
          flexBasis: '75%',
        }}>
        <View
          style={{
            ...styles.View,
            width: showWidth,
          }}>
          <Button
            onPress={() => openPicker()}
            style={{
              elevation: 0,
              paddingLeft: 5,
              bottom: 10,
              backgroundColor: transparent,
              height: 30,
              width: '100%',
            }}>
            <Text
              style={{
                color: value === '' ? brownGrey : black,
              }}>
              {value === ''
                ? placeholder
                : moment(value).format('DD MMMM YYYY')}
            </Text>
          </Button>
          {openStatus ? (
            Platform.OS === 'ios' ? (
              <DatePicker
                minDate={new Date()}
                value={value}
                mode="date"
                display="spinner"
                done={selectedDate => {
                  console.log('SelectedDate: ', selectedDate);
                  done(selectedDate);
                }}
                cancel={() => cancel()}
              />
            ) : (
              <DatePicker
                openStatus={openStatus}
                minDate={new Date()}
                value={value}
                mode="date"
                display="spinner"
                done={selectedDate => {
                  console.log('SelectedDate: ', selectedDate);
                  done(selectedDate);
                }}
                cancel={() => cancel()}
              />
            )
          ) : null}
        </View>
        {showX ? (
          <Button
            onPress={() => clearDate()}
            style={{
              elevation: 0,
              backgroundColor: transparent,
              height: 30,
              width: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="x-square"
              style={{
                width: 40,
                height: 40,
                fontSize: 25,
                color: black,
                left: 12,
                top: 10,
              }}
            />
          </Button>
        ) : null}
      </Body>
    </CardItem>
  );
};

const styles = StyleSheet.create({
  CardItem: {
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: transparent,
  },
  Left: {flexBasis: '25%'},
  Title: {
    fontFamily: medium,
    fontSize: small,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.2,
  },
  Body: {paddingLeft: 10, flexDirection: 'row'},
  View: {
    flexDirection: 'row',
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: black,
    // width: '90%',
    height: 35,
  },
  TextInput: {
    width: '100%',
    height: 35,
  },
});

export default DatePickerCustom;
