import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {CardItem, Left, Body} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {black, white} = Colors;
const {medium} = FontType;
const {small} = FontSize;

const TextInPutCustom = props => {
  const {value, placeholder, onChangeText, title} = props;
  return (
    <CardItem style={styles.CardItem}>
      <Left style={styles.Left}>
        <Text style={styles.Title}>{title}</Text>
      </Left>
      <Body style={styles.Body}>
        <View style={styles.View}>
          <TextInput
            value={value}
            placeholder={placeholder}
            returnKeyType="done"
            underlineColorAndroid={white}
            style={styles.TextInput}
            onChangeText={e => {
              onChangeText(e);
            }}
          />
        </View>
      </Body>
    </CardItem>
  );
};

const styles = StyleSheet.create({
  CardItem: {
    paddingLeft: 25,
    paddingRight: 25,
  },
  Left: {flexBasis: '25%'},
  Title: {
    fontFamily: medium,
    fontSize: small,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.2,
  },
  Body: {flexBasis: '75%', paddingLeft: 10},
  View: {
    borderBottomWidth: 1,
    borderBottomColor: black,
    width: '100%',
  },
  TextInput: {
    width: '100%',
    height: 35,
  },
});

export default TextInPutCustom;
