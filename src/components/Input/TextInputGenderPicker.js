import React from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';
import {CardItem, Left, Body} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import GenderPicker from '../../components/Picker/pickerSelect';

const {black} = Colors;
const {medium} = FontType;
const {small} = FontSize;

const GenderCustom = props => {
  const {
    disabled,
    items,
    useNativeAndroid,
    placeholder,
    onChange,
    title,
  } = props;
  return (
    <CardItem style={styles.CardItem}>
      <Left style={styles.Left}>
        <Text style={styles.Title}>{title}</Text>
      </Left>
      <Body style={styles.Body}>
        <View style={styles.View}>
          {Platform.OS === 'ios' ? (
            <GenderPicker
              disabled={!disabled ? false : true}
              items={items}
              useNativeAndroidPickerStyle={useNativeAndroid}
              placeholder={placeholder}
              onChange={e => {
                onChange(e);
              }}
            />
          ) : (
            <View style={{bottom: 20}}>
              <GenderPicker
                disabled={!disabled ? false : true}
                items={items}
                useNativeAndroidPickerStyle={useNativeAndroid}
                placeholder={placeholder}
                onChange={e => {
                  onChange(e);
                }}
              />
            </View>
          )}
        </View>
      </Body>
    </CardItem>
  );
};

const styles = StyleSheet.create({
  CardItem: {
    paddingLeft: 25,
    paddingRight: 25,
  },
  Left: {flexBasis: '25%'},
  Title: {
    fontFamily: medium,
    fontSize: small,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.2,
  },
  Body: {flexBasis: '75%', paddingLeft: 10},
  View: {
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: black,
    width: '100%',
    height: 35,
  },
  TextInput: {
    width: '100%',
    height: 35,
  },
});

export default GenderCustom;
