import React from 'react';
import {View, Text, StyleSheet, TextInput, Platform} from 'react-native';
import {CardItem, Left, Body, Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';

const {black, transparent, veryLightPinkTwo} = Colors;
const {medium} = FontType;
const {small} = FontSize;

const PhonePicker = props => {
  const {openModal, onChange, placeholder, countryCode, value, title} = props;

  return (
    <React.Fragment>
      <CardItem style={styles.CardItem}>
        <Left style={styles.Left}>
          <Text style={styles.Title}>{title}</Text>
        </Left>
        <Body style={styles.Body}>
          <View style={styles.viewWrapperCountryCode}>
            <View style={cardItemForm.countryCodeButton}>
              <Button
                style={cardItemForm.countryCode}
                onPress={() => openModal()}>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={
                      Platform.OS === 'ios'
                        ? {...cardItemForm.textCountryCode}
                        : {...cardItemForm.textCountryCodeAndroid}
                    }>
                    {countryCode}
                  </Text>
                  <Icon
                    type="Feather"
                    name="chevron-down"
                    style={styles.iconCountryCode}
                  />
                </View>
              </Button>
            </View>
            <View style={cardItemForm.phoneNumberForm}>
              <TextInput
                value={value}
                placeholder={placeholder}
                returnKeyType="done"
                keyboardType="phone-pad"
                style={cardItemForm.textInput}
                onChangeText={e => onChange(e)}
              />
            </View>
          </View>
        </Body>
      </CardItem>
    </React.Fragment>
  );
};

const cardItemForm = StyleSheet.create({
  countryCodeButton: {
    flexWrap: 'wrap',
    flexBasis: '28%',
  },
  countryCode: {
    minWidth: '100%',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderRadius: 0,
    backgroundColor: transparent,
    height: 35,
    width: '100%',
    elevation: 0,
    shadowOpacity: 0,
    flexDirection: 'row',
  },
  textCountryCode: {top: 2, left: 2},
  textCountryCodeAndroid: {bottom: 1, left: 2},
  textInput: {
    padding: 5,
    paddingLeft: 20,
    paddingBottom: 9,
    height: 35,
    width: '100%',
    borderLeftWidth: 1,
    borderLeftColor: veryLightPinkTwo,
  },
  phoneNumberForm: {
    height: 35,
    flexBasis: '72%',
  },
});

const styles = StyleSheet.create({
  CardItem: {
    paddingLeft: 25,
    paddingRight: 25,
  },
  Left: {flexBasis: '25%'},
  Title: {
    fontFamily: medium,
    fontSize: small,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.2,
  },
  Body: {flexBasis: '75%', paddingLeft: 10},
  View: {
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: black,
    width: '100%',
    height: 35,
  },
  TextInput: {
    width: '100%',
    height: 35,
  },
  viewWrapperCountryCode: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: black,
    width: '100%',
    height: 35,
  },
  iconCountryCode: {top: 1, right: 8, fontSize: 15, color: black},
});

export default PhonePicker;
