import React from 'react';
import {Text, View, ImageBackground, StyleSheet, Image} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {CardItem} from 'native-base';
import FlipCard from 'react-native-flip-card';
import FrontCC from './images/card-front.png';
import BackCC from './images/card-back.png';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, transparent} = Colors;

const cardStyles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: transparent,
  },
  content: {
    padding: 10,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  frontCard: {
    width: 300,
    height: 190,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
  backCard: {
    width: 300,
    height: 190,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
});

const CardView = props => {
  console.log('CardView Props: ', props);
  const {flip, ccNumber, cvc, expiryDate, expiryYear, cardImageType} = props;
  return (
    <CardItem style={cardStyles.container}>
      <FlipCard
        style={cardStyles.content}
        friction={6}
        perspective={1000}
        flipHorizontal={true}
        flipVertical={false}
        flip={flip}
        clickable={true}
        onFlipEnd={isFlipEnd => {
          console.log('isFlipEnd', isFlipEnd);
        }}>
        <ImageBackground source={FrontCC} style={cardStyles.frontCard}>
          <View
            style={{
              width: 49,
              height: 31,
              position: 'absolute',
              left: 20,
              top: 15,
            }}>
            <Image
              source={cardImageType}
              style={{flex: 1, aspectRatio: 49 / 31, width: null, height: 31}}
              resizeMode="contain"
            />
          </View>
          <Text
            style={{
              position: 'absolute',
              left: 20,
              bottom: 65,
              fontSize: RFPercentage(2),
            }}>
            {ccNumber === '' ? '•••• •••• •••• ••••' : ccNumber}
          </Text>
          <Text
            style={{
              position: 'absolute',
              left: 20,
              bottom: 45,
              fontSize: RFPercentage(1.5),
              color: '#888888',
            }}>
            Expiry Date
          </Text>
          <Text
            style={{
              position: 'absolute',
              left: 20,
              bottom: 30,
              fontSize: RFPercentage(1.5),
            }}>
            {expiryDate === '' ||
            expiryDate === null ||
            expiryDate === undefined
              ? '00'
              : expiryDate}{' '}
            /{' '}
            {expiryYear === '' ||
            expiryYear === null ||
            expiryYear === undefined
              ? '00'
              : expiryYear}
          </Text>
        </ImageBackground>
        <ImageBackground source={BackCC} style={cardStyles.backCard}>
          <Text style={{position: 'absolute', right: 20, top: 85}}>
            {cvc === '' ? '•••' : cvc}
          </Text>
        </ImageBackground>
      </FlipCard>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(CardView);

export default props => <Wrapper {...props} />;
