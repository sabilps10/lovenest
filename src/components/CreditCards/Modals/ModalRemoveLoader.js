import React from 'react';
import {Text, View, Modal, ActivityIndicator, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';

const {overlayDim, black} = Colors;
const {regular} = FontSize;
const {book} = FontType;

const ModalRemoveLoader = props => {
  const {showModalRemoveCard, isSuccessRemove, isFailedRemove} = props;
  return (
    <Modal visible={showModalRemoveCard} transparent>
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          zIndex: 999,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            minHeight: 100,
            minWidth: 100,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
            borderRadius: 10,
            flexDirection: 'column',
            padding: 10,
          }}>
          {showModalRemoveCard && !isSuccessRemove && !isFailedRemove ? (
            <ActivityIndicator
              style={{top: 5}}
              animating
              size="large"
              color="black"
            />
          ) : showModalRemoveCard && isSuccessRemove && !isFailedRemove ? (
            <Icon
              type="Feather"
              name="check"
              style={{top: 9, fontSize: 30, color: '#44D363'}}
            />
          ) : (
            <Icon
              type="Feather"
              name="x"
              style={{top: 9, fontSize: 30, color: 'red'}}
            />
          )}
          <Text
            style={{
              top: 10,
              fontFamily: book,
              fontSize: regular,
              color: black,
              marginVertical: 10,
              textAlign: 'center',
            }}>
            {showModalRemoveCard && !isSuccessRemove && !isFailedRemove
              ? 'Removing...'
              : showModalRemoveCard && isSuccessRemove && !isFailedRemove
              ? 'Remove Success'
              : 'Remove Failed'}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ModalRemoveLoader);

export default props => <Wrapper {...props} />;
