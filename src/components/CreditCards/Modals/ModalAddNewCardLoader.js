import React from 'react';
import {Text, View, Dimensions, ActivityIndicator, Modal} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';

const {overlayDim, black} = Colors;
const {regular} = FontSize;
const {book} = FontType;

const ModalAddNewCardLoader = props => {
  const {showModal, isSuccess, isFailed, isSuccessMsg, isFailedMsg} = props;
  return (
    <Modal visible={showModal} transparent>
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          zIndex: 999,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            minHeight: 100,
            minWidth: 100,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
            borderRadius: 10,
            flexDirection: 'column',
            padding: 10,
          }}>
          {showModal && !isSuccess && !isFailed ? (
            <ActivityIndicator
              style={{top: 5}}
              animating
              size="large"
              color="black"
            />
          ) : showModal && isSuccess && !isFailed ? (
            <Icon
              type="Feather"
              name="check"
              style={{top: 9, fontSize: 30, color: '#44D363'}}
            />
          ) : (
            <Icon
              type="Feather"
              name="x"
              style={{top: 9, fontSize: 30, color: 'red'}}
            />
          )}
          <Text
            style={{
              top: 10,
              fontFamily: book,
              fontSize: regular,
              color: black,
              marginVertical: 10,
              textAlign: 'center',
            }}>
            {showModal && !isSuccess && !isFailed
              ? 'Registering your card'
              : showModal && isSuccess && !isFailed
              ? isSuccessMsg
              : isFailedMsg}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ModalAddNewCardLoader);

export default props => <Wrapper {...props} />;
