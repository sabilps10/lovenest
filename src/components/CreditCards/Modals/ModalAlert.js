import React from 'react';
import {Text, View, Modal, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {overlayDim, black, mainRed: lightSalmon, white} = Colors;
const {medium} = FontType;

const ModalAlert = props => {
  const {visible, onRemove, onCancel} = props;
  return (
    <Modal visible={visible} transparent>
      <View
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          zIndex: 999,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: Dimensions.get('window').width / 1.2,
            backgroundColor: 'white',
            flexDirection: 'column',
            paddingTop: 10,
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 15,
          }}>
          <View style={{width: '100%', marginBottom: 20, marginTop: 10}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.5),
                color: black,
                letterSpacing: 0.4,
              }}>
              Are you sure you want to remove this card?
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Button
              onPress={() => onRemove()}
              style={{
                borderWidth: 1,
                borderColor: lightSalmon,
                backgroundColor: white,
                width: Dimensions.get('window').width / 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: lightSalmon,
                  fontSize: RFPercentage(2),
                }}>
                Remove
              </Text>
            </Button>
            <Button
              onPress={() => onCancel()}
              style={{
                backgroundColor: lightSalmon,
                width: Dimensions.get('window').width / 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: white,
                  fontSize: RFPercentage(2),
                }}>
                Cancel
              </Text>
            </Button>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ModalAlert);

export default props => <Wrapper {...props} />;
