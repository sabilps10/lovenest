import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {CardItem} from 'native-base';

const {black, transparent} = Colors;
const {book} = FontType;

const CardTitle = () => {
  return (
    <CardItem
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: transparent,
      }}>
      <View
        style={{
          width: 250,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          This card will only be charged when you place an order.
        </Text>
      </View>
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(CardTitle);

export default props => <Wrapper {...props} />;
