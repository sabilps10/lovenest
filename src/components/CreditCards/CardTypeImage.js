import React from 'react';
import {View, Image} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

const CardTypeImage = props => {
  const {ccTypeImage} = props;
  return (
    <View style={{left: -5, top: 1, width: 43, height: 25}}>
      <Image
        source={ccTypeImage}
        style={{
          flex: 1,
          aspectRatio: 42 / 25,
          width: 42,
          height: 25,
          marginRight: 5,
        }}
        resizeMode="contain"
      />
    </View>
  );
};

const Wrapper = compose(withApollo)(CardTypeImage);

export default props => <Wrapper {...props} />;
