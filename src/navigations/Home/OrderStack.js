import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import Order from '../../screens/Order/Order';
import OrderDetail from '../../screens/Order/OrderDetail';
import PaymentHistory from '../../screens/Payment/PaymentHistory';
import ProductAttachment from '../../screens/Order/Attachment/ProductAttachmentScreen';
import InteriorProgress from '../../screens/Order/Progress/InteriorProgress';
import ProgressGallery from '../../screens/Order/Progress/ProgressGallery';
import MakePayment from '../../screens/Payment/MakePayment';
// Payment
import SelectPaymentMethod from '../../screens/Payment/SelectPaymentMethod';

// Photo & Album Progress
import PhotoAlbumProgress from '../../screens/Order/PhotoAlbumProgress';

const Stack = createStackNavigator();

const OrderStack = props => {
  const {orderTabRef} = props;

  return (
    <Stack.Navigator initialRouteName="Orders">
      <Stack.Screen
        options={{headerShown: false}}
        name="PhotoAlbumProgress"
        children={childProps => {
          return (
            <PhotoAlbumProgress
              {...props}
              {...childProps}
              orderTabRef={orderTabRef}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Orders"
        children={childProps => {
          return <Order {...props} {...childProps} orderTabRef={orderTabRef} />;
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="OrderDetail"
        children={childProps => <OrderDetail {...props} {...childProps} />}
      />
      <Stack.Screen name="PaymentHistory" component={PaymentHistory} />
      <Stack.Screen
        options={{headerShown: false}}
        name="MakePayment"
        children={childProps => <MakePayment {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ProductAttachment"
        children={childProps => (
          <ProductAttachment {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="InteriorProgress"
        children={childProps => <InteriorProgress {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ProgressGallery"
        children={childProps => <ProgressGallery {...props} {...childProps} />}
      />
      <Stack.Screen
        name="SelectPaymentMethod"
        children={childProps => (
          <SelectPaymentMethod {...props} {...childProps} />
        )}
      />
    </Stack.Navigator>
  );
};

export default OrderStack;
