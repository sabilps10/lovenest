import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

import Invitation from '../../screens/WeddingInvitation/index';
import Draft from '../../screens/WeddingInvitation/Screens/Draft';

// Editor Screen
import Editor from '../../screens/WeddingInvitation/Screens/Editor';
import RSVPDetail from '../../screens/WeddingInvitation/Screens/RSVPDetail';
import InvitedList from '../../screens/WeddingInvitation/Screens/InvitedList';
import TableArrangement from '../../screens/WeddingInvitation/Screens/TableArrangement';
import AddTableArrangement from '../../screens/WeddingInvitation/Screens/AddTableArrangement';
import EditTableArrangement from '../../screens/WeddingInvitation/Screens/EditTableArrangement';

const EInvitationStack = props => {
  const {positionYBottomNav, homeTabRef} = props;
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Invitation"
        children={childProps => {
          return (
            <Invitation
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false, gestureEnabled: false}}
        name="EditorWithTemplate"
        children={childProps => {
          return (
            <Editor
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Draft"
        children={childProps => {
          return (
            <Draft
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="RSVPDetail"
        children={childProps => {
          return (
            <RSVPDetail
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="InvitedList"
        children={childProps => {
          return (
            <InvitedList
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="TableArrangement"
        children={childProps => {
          return (
            <TableArrangement
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AddTableArrangement"
        children={childProps => {
          return (
            <AddTableArrangement
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="EditTableArrangement"
        children={childProps => {
          return (
            <EditTableArrangement
              {...props}
              {...childProps}
              homeTabRef={homeTabRef}
              positionYBottomNav={positionYBottomNav}
            />
          );
        }}
      />
    </Stack.Navigator>
  );
};

export default EInvitationStack;
