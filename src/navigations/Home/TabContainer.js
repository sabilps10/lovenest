import React, {useRef, useEffect} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  Platform,
  Animated,
} from 'react-native';
import {connect} from 'react-redux';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Colors from '../../utils/Themes/Colors';
import {bottomNavActive, bottomNavInactive} from '../../utils/Themes/Images';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

const {lightSalmon, brownGrey} = Colors;
const {medium} = FontType;
const {
  bottomNavActiveHome,
  bottomNavActiveOrder,
  bottomNavActivePayment,
  bottomNavActiveMore,
  bottomNavActiveAppointment,
  bottomNavActiveEinvitation,
} = bottomNavActive;
const {
  bottomNavInactiveHome,
  bottomNavInactiveOrder,
  bottomNavInactivePayment,
  bottomNavInactiveMore,
  bottomNavInactiveAppointment,
  bottomNavInactiveEinvitation,
} = bottomNavInactive;
const widthScreen = Dimensions.get('window').width;

// Screens
import Home from './HomeStack';
import Order from './OrderStack';
import Payment from './PaymentStack';
import EInvitation from './EInvitationStack';
import Appointment from './AppointmentStack';
import More from './MoreStack';

const bottomPosition = Platform.OS === 'ios' ? (hasNotch() ? 45 : 20) : 20;

import AsyncStorage from '@react-native-community/async-storage';
import AsyncToken from '../../utils/AsyncstorageDataStructure/index';
const {asyncToken} = AsyncToken;

import FCM from '../../utils/Notifications/FCMService';
const {
  onRegister,
  checkPermission,
  getFCMToken,
  onNotificationOpenedApp,
  getInitialNotification,
  onMessage,
  subTopic,
} = FCM;

const TabBar = props => {
  console.log('PROPS Tab Bar: ', props);
  const {
    state,
    descriptors,
    navigation,
    orderScrollToTop,
    paymentScrollToTop,
    homeScrollToTop,
  } = props;
  return (
    <SafeAreaView style={tabBarStyle.container}>
      <View
        style={{
          backgroundColor: 'transparent',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: widthScreen,
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <View
          style={{
            borderRadius: widthScreen / 10,
            backgroundColor: 'white',
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            paddingLeft: 20,
            paddingRight: 20,
            height: 60,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 3,
          }}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              } else {
                if (route.name === 'Orders') {
                  orderScrollToTop();
                }
                if (route.name === 'Payments') {
                  paymentScrollToTop();
                }
                if (route.name === 'Home') {
                  homeScrollToTop();
                }
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            if (label === 'Home') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '20%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused ? bottomNavActiveHome : bottomNavInactiveHome
                    }
                    style={tabBarStyle.icon}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            } else if (label === 'Orders') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '20%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused ? bottomNavActiveOrder : bottomNavInactiveOrder
                    }
                    style={tabBarStyle.icon}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            } else if (label === 'Payments') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '20%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused
                        ? bottomNavActivePayment
                        : bottomNavInactivePayment
                    }
                    style={tabBarStyle.icon}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            } else if (label === 'E-Invitation') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '20%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused
                        ? bottomNavActiveEinvitation
                        : bottomNavInactiveEinvitation
                    }
                    style={{width: 20, height: 16, bottom: 1}}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                      top: 5,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            } else if (label === 'Appointments') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '25%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused
                        ? bottomNavActiveAppointment
                        : bottomNavInactiveAppointment
                    }
                    style={tabBarStyle.icon}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            } else if (label === 'More') {
              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                    // flexBasis: '20%',
                    ...tabBarStyle.tab,
                  }}>
                  <Image
                    source={
                      isFocused ? bottomNavActiveMore : bottomNavInactiveMore
                    }
                    style={tabBarStyle.icon}
                  />
                  <Text
                    style={{
                      color: isFocused ? lightSalmon : brownGrey,
                      ...tabBarStyle.text,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            }
          })}
        </View>
      </View>
    </SafeAreaView>
  );
};

const Tab = createBottomTabNavigator();

const TabContainer = props => {
  console.log('PROPS TAB CONTAINER HOME: ', props);
  const {client, navigation, positionYBottomNav} = props;

  const orderTabRef = useRef();
  const paymentTabRef = useRef();
  const homeTabRef = useRef();

  useEffect(() => {
    FCMConfiguration();
    return () => FCMConfiguration();
  }, []);

  useEffect(() => {
    onMessage(client, navigation);
    return () => onMessage(client, navigation);
  }, []);

  const loginChecker = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve(false);
      }
    });
  };

  const FCMConfiguration = async () => {
    try {
      const theLoginResult = await loginChecker();

      if (theLoginResult) {
        await checkPermission(client, navigation);
        await onRegister(client, navigation);
        await getFCMToken(client, navigation);
        // await subTopic();
        await onNotificationOpenedApp(client, navigation);
        await getInitialNotification(client, navigation);
        // await onMessage(client, navigation);
      } else {
        await checkPermission(client, navigation);
        await onRegister(client, navigation);
        await getFCMToken(client, navigation);
        // await subTopic();
        await onNotificationOpenedApp(client, navigation);
        await getInitialNotification(client, navigation);
        // await onMessage(client, navigation);
      }
    } catch (error) {}
  };

  const homeScrollToTop = () => {
    if (homeTabRef?.current?.scrollTo) {
      homeTabRef.current?.scrollTo({y: 0, animated: true});
    }
  };

  const orderScrollToTop = () => {
    if (orderTabRef?.current?.scrollToOffset) {
      orderTabRef.current?.scrollToOffset({offset: 0, animated: true});
    }
  };

  const paymentScrollToTop = () => {
    if (paymentTabRef?.current?.scrollToOffset) {
      paymentTabRef.current.scrollToOffset({
        animated: true,
        offset: 0,
      });
    }
  };

  const isVisible = route => {
    if (
      route?.name === 'Home' ||
      route?.name === 'Orders' ||
      route?.name === 'Payments' ||
      route?.name === 'Appointments' ||
      route?.name === 'More'
    ) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <Tab.Navigator
      lazy
      initialRouteName={'Home'}
      screenOptions={({route}) => ({tabBarVisible: isVisible(route)})}
      tabBar={tabCompProps => {
        return (
          <Animated.View
            style={{
              transform: [{translateY: positionYBottomNav}],
              flexDirection: 'row',
              position: 'absolute',
              bottom: bottomPosition,
            }}>
            <TabBar
              {...tabCompProps}
              orderScrollToTop={() => orderScrollToTop()}
              paymentScrollToTop={() => paymentScrollToTop()}
              homeScrollToTop={() => homeScrollToTop()}
            />
          </Animated.View>
        );
      }}
      tabBarOptions={{
        safeAreaInsets: {
          bottom: 'always',
        },
        activeTintColor: lightSalmon,
      }}>
      <Tab.Screen
        name="Homes"
        options={{
          tabBarLabel: 'Home',
        }}
        children={childProps => (
          <Home
            path="HomeTab"
            {...props}
            {...childProps}
            homeTabRef={homeTabRef}
            positionYBottomNav={positionYBottomNav}
          />
        )}
      />
      <Tab.Screen
        name="Orders"
        options={{
          tabBarLabel: 'Orders',
        }}
        children={childProps => (
          <Order {...props} {...childProps} orderTabRef={orderTabRef} />
        )}
      />
      <Tab.Screen
        name="Payments"
        options={{
          tabBarLabel: 'Payments',
        }}
        children={childProps => (
          <Payment {...props} {...childProps} paymentTabRef={paymentTabRef} />
        )}
      />
      <Tab.Screen
        name="EInvitation"
        options={{
          tabBarLabel: 'E-Invitation',
        }}
        children={childProps => <EInvitation {...props} {...childProps} />}
      />
      <Tab.Screen
        name="Appointments"
        options={({route}) => {
          return {
            tabBarLabel: 'Appointments',
          };
        }}
        children={childProps => <Appointment {...props} {...childProps} />}
      />
      <Tab.Screen
        name="More"
        options={{
          tabBarLabel: 'More',
        }}
        children={childProps => <More {...props} {...childProps} />}
      />
    </Tab.Navigator>
  );
};

const tabBarStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  tab: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 3,
    paddingTop: 3,
  },
  icon: {width: 25, height: 25},
  text: {
    fontFamily: medium,
    fontSize: RFPercentage(1.2),
    letterSpacing: 0.2,
  },
});

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const Wrapper = connect(mapToState, mapToDispatch)(TabContainer);

export default props => <Wrapper {...props} />;
