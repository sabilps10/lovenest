import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import More from '../../screens/More/More';
import Profile from '../../screens/More/Profile';
import EditProfile from '../../screens/More/Profile/EditProfile';
import AboutUs from '../../screens/More/About';
import TermAndConditionStack from './TermAndConditionStack';
import Review from '../../screens/More/Review/Review';
import ReviewMerchantSelect from '../../screens/More/Review/MerchantSelect';
import ReviewServiceSelect from '../../screens/More/Review/ServiceSelect';
import ReviewStaffSelect from '../../screens/More/Review/StaffSelect';
import AppointmentGuide from '../../screens/More/AppointmentGuide/index';
import AppointmentGuidePdfPreview from '../../screens/More/AppointmentGuide/PDFPreview';
import LoveAndWin from '../../screens/More/LoveAndWin/index';
import LoveAndWinHistory from '../../screens/More/LoveAndWin/LoveAndWinHistory';
import LoveAndWinForm from '../../screens/More/LoveAndWin/LoveAndWinForm';
import LoveAndWinHistoryDetail from '../../screens/More/LoveAndWin/HistoryDetail';

const Stack = createStackNavigator();

const MoreStack = props => {
  const {navigation} = props;
  return (
    <Stack.Navigator
      initialRouteName="More"
      screenOptions={data => {
        const {route} = data;
        const {name} = route;

        if (name !== 'More') {
          navigation.setOptions({
            tabBarVisible: false,
          });
        } else {
          navigation.setOptions({
            tabBarVisible: true,
          });
        }
      }}>
      <Stack.Screen
        name="More"
        children={childProps => <More {...props} {...childProps} />}
      />
      <Stack.Screen
        name="AppointmentGuide"
        children={childProps => <AppointmentGuide {...props} {...childProps} />}
      />
      <Stack.Screen
        name="AppointmentGuidePdfPreview"
        children={childProps => (
          <AppointmentGuidePdfPreview {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        name="Profile"
        children={childProps => <Profile {...props} {...childProps} />}
      />
      <Stack.Screen
        name="EditProfile"
        children={childProps => <EditProfile {...props} {...childProps} />}
      />
      <Stack.Screen
        name="AboutUs"
        children={childProps => <AboutUs {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="TermAndConditionStack"
        children={childProps => (
          <TermAndConditionStack {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Review"
        children={childProps => <Review {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ReviewMerchantSelect"
        children={childProps => (
          <ReviewMerchantSelect {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ReviewServiceSelect"
        children={childProps => (
          <ReviewServiceSelect {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ReviewStaffSelect"
        children={childProps => (
          <ReviewStaffSelect {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="LoveAndWin"
        children={childProps => <LoveAndWin {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="LoveAndWinHistory"
        children={childProps => (
          <LoveAndWinHistory {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="LoveAndWinForm"
        children={childProps => <LoveAndWinForm {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="LoveAndWinHistoryDetail"
        children={childProps => (
          <LoveAndWinHistoryDetail {...props} {...childProps} />
        )}
      />
    </Stack.Navigator>
  );
};

export default MoreStack;
