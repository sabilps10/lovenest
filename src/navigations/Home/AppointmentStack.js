import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Colors from '../../utils/Themes/Colors';
import {FontType, FontSize} from '../../utils/Themes/Fonts';

const {black} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

// Screens
import AppointmentTab from '../Appointments';
import AppointmentDetail from '../../screens/Appointment/AppointmentDetail';
import Calendar from '../../screens/Appointment/Calendar';
import MonthlyCalendar from '../../screens/Appointment/MonthlyCalendar';
import DailyCalendar from '../../screens/Appointment/DailyCalendar';
import AppointmentGuidePdfPreview from '../../screens/More/AppointmentGuide/PDFPreview'

const Stack = createStackNavigator();

const AppointmentStack = props => {
  console.log('Appointment Stack Screen Props: ', props);
  const {navigation} = props;
  return (
    <Stack.Navigator
      initialRouteName="Appointment"
      screenOptions={data => {
        const {route: stackNavRoute} = data;
        const {name} = stackNavRoute;

        if (name === 'AppointmentDetail') {
          navigation.setOptions({
            tabBarVisible: false,
          });
        } else if (name === 'Calendar') {
          navigation.setOptions({
            tabBarVisible: false,
          });
        } else if (name === 'MonthlyCalendar') {
          navigation.setOptions({
            tabBarVisible: false,
          });
        } else if (name === 'DailyCalendar') {
          navigation.setOptions({
            tabBarVisible: false,
          });
        } else {
          navigation.setOptions({
            tabBarVisible: true,
          });
        }
      }}>
      <Stack.Screen
        name="Appointments"
        // component={AppointmentTab}
        options={{
          headerTitle: 'Appointments',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
          },
          headerStyle: {
            elevation: 0,
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            },
          },
        }}
        children={childProps => {
          return <AppointmentTab {...props} {...childProps} />;
        }}
      />
      <Stack.Screen
        name="AppointmentGuidePdfPreview"
        // component={AppointmentDetail}
        children={childProps => {
          return <AppointmentGuidePdfPreview {...props} {...childProps} />;
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AppointmentDetail"
        // component={AppointmentDetail}
        children={childProps => {
          return <AppointmentDetail {...props} {...childProps} />;
        }}
      />
      <Stack.Screen options={{headerShown: false}} name="Calendar">
        {theProps => <Calendar {...props} {...theProps} />}
      </Stack.Screen>
      <Stack.Screen options={{headerShown: false}} name="MonthlyCalendar">
        {theProps => <MonthlyCalendar {...props} {...theProps} />}
      </Stack.Screen>
      <Stack.Screen options={{headerShown: false}} name="DailyCalendar">
        {theProps => <DailyCalendar {...props} {...theProps} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export default AppointmentStack;
