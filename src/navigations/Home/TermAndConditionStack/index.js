import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StackActions} from '@react-navigation/native';

// Screens
import TermAndCondition from '../../../screens/More/TermAndCondition';
import PrivacyAndPolcy from '../../../screens/More/TermAndCondition/PrivacyAndPolicy';

const Stack = createStackNavigator();

const TermAndConditionStack = props => {
  return (
    <Stack.Navigator initialRouteName="TermAndCondition">
      <Stack.Screen
        name="TermAndCondition"
        children={childProps => <TermAndCondition {...props} {...childProps} />}
      />
      <Stack.Screen
        name="PrivacyAndPolicy"
        children={childProps => <PrivacyAndPolcy {...props} {...childProps} />}
      />
    </Stack.Navigator>
  );
};

export default TermAndConditionStack;
