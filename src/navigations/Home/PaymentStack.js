import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import Payment from '../../screens/Payment/Payment';
import PaymentDetails from '../../screens/Payment/PaymentDetails';
import PaymentMethod from '../../screens/Payment/PaymentMethod';
import PaymentHistory from '../../screens/Payment/PaymentHistory';
import AddNewCard from '../../screens/Payment/AddNewCard';
import CardDetail from '../../screens/Payment/CardDetail';
import EditCard from '../../screens/Payment/EditCard';
import MakePayment from '../../screens/Payment/MakePayment';
import SelectPaymentMethod from '../../screens/Payment/SelectPaymentMethod';
import PaymentHistoryList from '../../screens/Payment/PaymentHistoryList';

const Stack = createStackNavigator();

const PaymentStack = props => {
  const {paymentTabRef} = props;
  return (
    <Stack.Navigator initialRouteName="Payment">
      <Stack.Screen
        options={{headerShown: false}}
        name="Payment"
        children={childProps => {
          return (
            <Payment {...props} {...childProps} paymentTabRef={paymentTabRef} />
          );
        }}
      />
      <Stack.Screen
        name="PaymentDetails"
        children={childProps => <PaymentDetails {...props} {...childProps} />}
      />
      <Stack.Screen
        name="PaymentMethod"
        children={childProps => <PaymentMethod {...props} {...childProps} />}
      />
      <Stack.Screen
        name="PaymentHistory"
        children={childProps => <PaymentHistory {...props} {...childProps} />}
      />
      <Stack.Screen
        name="AddNewCard"
        children={childProps => <AddNewCard {...props} {...childProps} />}
      />
      <Stack.Screen
        name="CardDetail"
        children={childProps => <CardDetail {...props} {...childProps} />}
      />
      <Stack.Screen
        name="EditCard"
        children={childProps => <EditCard {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="MakePayment"
        children={childProps => <MakePayment {...props} {...childProps} />}
      />
      <Stack.Screen
        name="SelectPaymentMethod"
        children={childProps => (
          <SelectPaymentMethod {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        name="PaymentHistoryList"
        children={childProps => (
          <PaymentHistoryList {...props} {...childProps} />
        )}
      />
    </Stack.Navigator>
  );
};

export default PaymentStack;
