import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import Home from '../../screens/Home/Home';
import Appointments from '../../navigations/Home/AppointmentStack';
import QR from '../../screens/QR';
import Notification from '../../screens/Notification';
import EventDetail from '../../screens/Event/EventDetail';
import UpcomingEvent from '../../screens/Event/UpcomingEvent';
import MerchantDetail from '../../screens/Merchant/MerchantDetail';
import Enquiry from '../../screens/Merchant/Enquiry';

// Stach Inside Merchant Detail
// Product
import Products from '../../screens/Merchant/Product/Products';
import ProductDetail from '../../screens/Merchant/Product/ProductDetail';
import ProductFilter from '../../screens/Merchant/Product/ProductFilter';
import ProductGallery from '../../screens/Merchant/Product/ProductGallery';
import SimilarProduct from '../../screens/Merchant/Product/SimilarProductList';

// Suit Menu
import Suit from '../../screens/Menu/Suit';
import SuitFilter from '../../screens/Menu/Suit/SuitFilter';

// Accessories
import Accessories from '../../screens/Merchant/Accessories/Accessories';
import AccessoriesDetail from '../../screens/Merchant/Accessories/AccessoriesDetail';
import AccessoriesFilter from '../../screens/Merchant/Accessories/AccessoriesFilter';

// Package VENUE OR HOTEL
import Venue from '../../screens/Menu/Venue';
import VenueMerchantDetail from '../../screens/Menu/Venue/VenueMerchantDetail';
import Packages from '../../screens/Menu/Venue/PopularPackageList';
import VenueAndHotelAbout from '../../screens/Menu/Venue/VenueAndHotelAbout';
import VenueAndHotelFilter from '../../screens/Menu/Venue/VenueAndHotelFilter';
import VenueAndHotelPromotionDetail from '../../screens/Menu/Venue/VenueOrHotelPromotionDetail';
import VenueAndHotelProductDetail from '../../screens/Menu/Venue/ProductDetail';
import VenueAndHotelSearch from '../../screens/Menu/Venue/Search';
import VenueAndHotelPDFPreview from '../../screens/Menu/Venue/PDFPreview';
import VenueAndHotelListProductAfterFilter from '../../screens/Menu/Venue/ListProductAfterFilter';
import RateCard from '../../screens/Menu/Venue/Rates';

// Highlight
import Highlight from '../../screens/Merchant/Highlight/Highlight';
import HighlightDetail from '../../screens/Merchant/Highlight/HighlightDetail';

// Order
import Orders from '../../screens/Order/Order';
import OrderDetail from '../../screens/Order/OrderDetail';
import ProductAttachment from '../../screens/Order/Attachment/ProductAttachmentScreen';
import PhotoAlbumProgress from '../../screens/Order/PhotoAlbumProgress';

// Payment
import SelectPaymentMethod from '../../screens/Payment/SelectPaymentMethod';
import AddNewCard from '../../screens/Payment/AddNewCard';
import CardDetail from '../../screens/Payment/CardDetail';
import EditCard from '../../screens/Payment/EditCard';
import PaymentMethod from '../../screens/Payment/PaymentMethod';

// Appointment
import AppointmentDetail from '../../screens/Appointment/AppointmentDetail';

// LN Notif Screen
import LNNotifDetail from '../../screens/Notification/LNNotifDetail';

// Menu Navigation
import Bridal from '../../screens/Menu/Bridal';
import BridalDetail from '../../screens/Menu/Bridal/BridalMerchantDetail';
import AboutMerchant from '../../screens/Menu/Bridal/BridalAbout';
import BridalProductDetail from '../../screens/Menu/Bridal/ProductDetail';
import BridalPromotionDetail from '../../screens/Menu/Bridal/PromotionDetail';
import BridalPortfolioDetail from '../../screens/Menu/Bridal/BridalPortfolioDetail';
import BridalSuitFilter from '../../screens/Menu/Bridal/BridalSuitFilter';
import BridalGownFilter from '../../screens/Menu/Bridal/BridalGownFilter';

// Jewellery Menu
import Jewellery from '../../screens/Menu/Jewellery';
import JewelleryMerchantDetail from '../../screens/Menu/Jewellery/MerchantDetail';
import JewelleryProductDetail from '../../screens/Menu/Jewellery/ProductDetail';
import AboutJewellery from '../../screens/Menu/Jewellery/JewelleryAbout';

// Interior Design Menu
import InteriorDesign from '../../screens/Menu/InteriorDesign';
import InteriorFilter from '../../screens/Menu/InteriorDesign/InteriorFilter';
import InteriorMerchantDetail from '../../screens/Menu/InteriorDesign/InteriorMerchantDetail';

// Smart Home
import SmartHome from '../../screens/Menu/SmartHome';
import SmartHomeMerchantDetail from '../../screens/Menu/SmartHome/SmartHomeMerchantDetail';

// Onboarding
import Login from '../../screens/OnBoarding/Login';

// Review each merchant
import ReviewList from '../../screens/More/Review/ReviewRatingList';
import ReviewRatingAndDetail from '../../screens/More/Review/ReviewAndRatingDetail';

// PhotoBooths
import PhotoBooths from '../../screens/PhotoBooths/index';
import DownloadScreen from '../../screens/PhotoBooths/DownloadScreen';

// Photography
import Photography from '../../screens/Menu/Photography';

// Florist
import Florist from '../../screens/Menu/Florist';
import FloristMerchantDetail from '../../screens/Menu/Florist/FloristMerchantDetail';
import FloristFilter from '../../screens/Menu/Florist/FloristFilter';
import FloristDetail from '../../screens/Menu/Florist/FloristDetail';
import FloristCustomize from '../../screens/Menu/Florist/Customize';
import FloristCart from '../../screens/Menu/Florist/Cart';
import FloristDeliveryOptions from '../../screens/Menu/Florist/DeliveryMethod';
import FloristMerchantAbout from '../../screens/Menu/Florist/FloristAbout';
import FloristPromotionDetail from '../../screens/Menu/Florist/FloristPromotionDetail';
import FloristProjectDetail from '../../screens/Menu/Florist/FloristPortfolioDetail';

// Special Offer
import SpecialOffer from '../../screens/SpecialOffer';
import SpecialOfferDetail from '../../screens/SpecialOffer/Detail';
import SpecialOfferForm from '../../screens/SpecialOffer/Offer';

// Benefit
import Benefits from '../../screens/Benefit/AllBenefit';

// New Home Menu
import CategoryMenu from '../../screens/Menu/CategoryList2And3';

const Stack = createStackNavigator();

const HomeStack = props => {
  console.log('HomeStack Props >>>>>>: ', props);
  const {homeTabRef, positionYBottomNav} = props;

  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        options={{
          headerShown: false,
        }}
        children={childProps => (
          <Home
            {...props}
            {...childProps}
            homeTabRef={homeTabRef}
            positionYBottomNav={positionYBottomNav}
          />
        )}
      />

      {/* Auth */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Login"
        component={Login}
      />

      {/* Appointment */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Appointments"
        children={childProps => <Appointments {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AppointmentDetail"
        children={childProps => (
          <AppointmentDetail {...props} {...childProps} />
        )}
      />

      {/* Event */}
      <Stack.Screen
        options={{headerShown: false}}
        name="EventDetail"
        children={childProps => <EventDetail {...props} {...childProps} />}
      />
      <Stack.Screen
        name="UpcomingEvent"
        children={childProps => <UpcomingEvent {...props} {...childProps} />}
      />

      {/* Merchant */}
      <Stack.Screen
        options={{headerShown: false}}
        name="MerchantDetail"
        component={MerchantDetail}
      />

      {/* QR */}
      <Stack.Screen
        options={{headerShown: false}}
        name="QR"
        children={childProps => <QR {...props} {...childProps} />}
      />

      {/* Notification */}
      <Stack.Screen
        name="Notification"
        children={childProps => <Notification {...props} {...childProps} />}
      />

      {/* Product */}
      <Stack.Screen name="Products" component={Products} />

      {/* Suit */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Suit"
        component={Suit}
      />

      <Stack.Screen
        options={{headerShown: false}}
        name="SuitFilter"
        component={SuitFilter}
      />

      {/* Product Detail */}
      <Stack.Screen
        options={{headerShown: false}}
        name="ProductDetail"
        component={ProductDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ProductGallery"
        component={ProductGallery}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="SimilarProduct"
        component={SimilarProduct}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="HighlightDetail"
        component={HighlightDetail}
      />
      <Stack.Screen name="ProductFilter" component={ProductFilter} />
      <Stack.Screen name="Accessories" component={Accessories} />
      <Stack.Screen name="AccessoriesDetail" component={AccessoriesDetail} />
      <Stack.Screen name="AccessoriesFilter" component={AccessoriesFilter} />
      <Stack.Screen name="Highlight" component={Highlight} />
      <Stack.Screen name="Enquiry" component={Enquiry} />

      {/* For Order notif */}
      <Stack.Screen
        options={{headerShown: false}}
        name="PhotoAlbumProgress"
        children={childProps => {
          return <PhotoAlbumProgress {...props} {...childProps} />;
        }}
      />
      <Stack.Screen name="Orders" component={Orders} />
      <Stack.Screen
        options={{headerShown: false}}
        name="OrderDetail"
        component={OrderDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ProductAttachment"
        children={childProps => (
          <ProductAttachment {...props} {...childProps} />
        )}
      />

      {/* LN Notif Detail */}
      <Stack.Screen
        options={{headerShown: false}}
        name="LNNotifDetail"
        component={LNNotifDetail}
      />

      {/* Payment */}
      <Stack.Screen
        name="SelectPaymentMethod"
        component={SelectPaymentMethod}
      />
      <Stack.Screen name="AddNewCard" component={AddNewCard} />
      <Stack.Screen name="CardDetail" component={CardDetail} />
      <Stack.Screen name="EditCard" component={EditCard} />
      <Stack.Screen name="PaymentMethod" component={PaymentMethod} />

      {/* Menu Navigtaion */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Bridal"
        children={childProps => <Bridal {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalDetail"
        component={BridalDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalProductDetail"
        component={BridalProductDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalPromotionDetail"
        component={BridalPromotionDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalPortfolioDetail"
        component={BridalPortfolioDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalSuitFilter"
        component={BridalSuitFilter}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="BridalGownFilter"
        component={BridalGownFilter}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AboutMerchant"
        component={AboutMerchant}
      />

      {/* Jewellery */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Jewellery"
        component={Jewellery}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="JewelleryMerchantDetail"
        component={JewelleryMerchantDetail}
      />

      <Stack.Screen
        options={{headerShown: false}}
        name="JewelleryProductDetail"
        component={JewelleryProductDetail}
      />

      <Stack.Screen
        options={{headerShown: false}}
        name="AboutJewellery"
        component={AboutJewellery}
      />

      {/* Venue */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Venue"
        children={childProps => <Venue {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueMerchantDetail"
        component={VenueMerchantDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelAbout"
        component={VenueAndHotelAbout}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelFilter"
        component={VenueAndHotelFilter}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelPromotionDetail"
        component={VenueAndHotelPromotionDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelProductDetail"
        component={VenueAndHotelProductDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelSearch"
        component={VenueAndHotelSearch}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelPDFPreview"
        component={VenueAndHotelPDFPreview}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="VenueAndHotelListProductAfterFilter"
        component={VenueAndHotelListProductAfterFilter}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="RateCard"
        component={RateCard}
      />
      <Stack.Screen name="Packages" component={Packages} />

      {/* Interior Design */}
      <Stack.Screen name="InteriorDesign" component={InteriorDesign} />
      <Stack.Screen name="InteriorFilter" component={InteriorFilter} />
      <Stack.Screen
        options={{headerShown: false}}
        name="InteriorMerchantDetail"
        component={InteriorMerchantDetail}
      />

      {/* Smart Home */}
      <Stack.Screen
        options={{headerShown: false}}
        name="SmartHome"
        component={SmartHome}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="SmartHomeMerchantDetail"
        component={SmartHomeMerchantDetail}
      />
      {/* Review List */}
      <Stack.Screen
        options={{headerShown: false}}
        name="ReviewList"
        component={ReviewList}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="ReviewRatingAndDetail"
        component={ReviewRatingAndDetail}
      />

      {/* Photo Booths */}
      <Stack.Screen
        options={{headerShown: true}}
        name="PhotoBooths"
        component={PhotoBooths}
      />
      <Stack.Screen
        options={{headerShown: true}}
        name="DownloadScreen"
        component={DownloadScreen}
      />

      {/* Photography */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Photography"
        component={Photography}
      />

      {/* Florist */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Florist"
        component={Florist}
      />
      <Stack.Screen
        options={{headerShown: false, gestureEnabled: false}}
        name="FloristMerchantDetail"
        component={FloristMerchantDetail}
      />
      <Stack.Screen
        options={{headerShown: false, gestureEnabled: false}}
        name="FloristMerchantAbout"
        component={FloristMerchantAbout}
      />
      <Stack.Screen
        options={{headerShown: false, gestureEnabled: false}}
        name="FloristFilter"
        component={FloristFilter}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristDetail"
        component={FloristDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristCustomize"
        component={FloristCustomize}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristCart"
        children={childProps => <FloristCart {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristProjectDetail"
        component={FloristProjectDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristPromotionDetail"
        component={FloristPromotionDetail}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="FloristDeliveryOptions"
        component={FloristDeliveryOptions}
      />

      {/* Special Offer */}
      <Stack.Screen
        options={{headerShown: false}}
        name="SpecialOffer"
        children={childProps => <SpecialOffer {...props} {...childProps} />}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="SpecialOfferDetail"
        children={childProps => (
          <SpecialOfferDetail {...props} {...childProps} />
        )}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="SpecialOfferForm"
        component={SpecialOfferForm}
      />

      {/* Benefits */}
      <Stack.Screen
        options={{headerShown: false}}
        name="Benefits"
        component={Benefits}
      />

      {/* Home menu with category */}
      <Stack.Screen
        options={{headerShown: false}}
        name="CategoryMenu"
        component={CategoryMenu}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
