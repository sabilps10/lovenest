import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screens Of OnBoarding
import Main from '../../screens/Main';
import WelcomeScreen from '../../screens/OnBoarding/WelcomeScreen';
import Login from '../../screens/OnBoarding/Login';
import OTP from '../../screens/OnBoarding/OTP';
import RegisterMainAccount from '../../screens/OnBoarding/RegisterMainAccount';
import RegisterPartnerAccount from '../../screens/OnBoarding/RegisterPartnerAccount';

// Screen Home
import MainHome from '../Home/TabContainer';

const Stack = createStackNavigator();

const OnBoardingStack = properties => {
  const props = properties;
  const {client, CommonActions} = props;
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Main">
      <Stack.Screen
        name="Main"
        children={navProps => {
          return (
            <Main {...navProps} client={client} CommonActions={CommonActions} />
          );
        }}
      />
      <Stack.Screen
        name="WelcomeScreen"
        children={navProps => {
          return (
            <WelcomeScreen
              {...navProps}
              client={client}
              CommonActions={CommonActions}
            />
          );
        }}
      />
      <Stack.Screen
        name="Login"
        children={navProps => {
          return (
            <Login
              {...navProps}
              client={client}
              CommonActions={CommonActions}
            />
          );
        }}
      />
      <Stack.Screen
        name="OTP"
        children={navProps => {
          return (
            <OTP {...navProps} client={client} CommonActions={CommonActions} />
          );
        }}
      />
      <Stack.Screen
        name="RegisterMainAccount"
        children={navProps => {
          return (
            <RegisterMainAccount
              {...navProps}
              client={client}
              CommonActions={CommonActions}
            />
          );
        }}
      />
      <Stack.Screen
        name="RegisterPartnerAccount"
        children={navProps => {
          return (
            <RegisterPartnerAccount
              {...navProps}
              client={client}
              CommonActions={CommonActions}
            />
          );
        }}
      />
      <Stack.Screen
        name="MainHome"
        options={{headerShown: false}}
        children={navProps => {
          return (
            <MainHome
              {...navProps}
              client={client}
              CommonActions={CommonActions}
            />
          );
        }}
      />
    </Stack.Navigator>
  );
};

export default OnBoardingStack;
