import React, {useState, useEffect} from 'react';
import {Text, Linking, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import compose from 'lodash/fp/compose';
import {withApollo} from 'react-apollo';
import {CommonActions} from '@react-navigation/native';
import {navigationRef, isReadyRef} from './NavigationServices/index';

import StackOnBoarding from './OnBoardingStack/index';

// Firebase
import DynamicLinks from '@react-native-firebase/dynamic-links';

const RootContainer = props => {
  console.log('Props Root Container: ', props);
  const {client} = props;

  useEffect(() => {
    const subs = openDeepLink();

    return subs;
  }, []);

  const openDeepLink = () => {
    try {
      // Open and Navigate when app foreground or background
      DynamicLinks().onLink(link => {
        console.log('MASUUUUUUUKKKKK ONNNNNNNNlink >>>>> ', link);
        if (link) {
          const trimed = link?.url?.replace(
            'https://lovenest.page.link/app/?link=',
            '',
          );
          if (trimed) {
            if (trimed) {
              Linking.canOpenURL(trimed).then(supported => {
                if (supported) {
                  Linking.openURL(trimed?.replace('%3D', '='));
                } else {
                  console.log("Don't know how to open URI: " + trimed);
                }
              });
            }
          }
        }
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const linking = {
    prefixes: ['https://lovenest.page.link/', 'lovenest://'],
  };

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
      linking={linking}
      fallback={
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      }>
      <StackOnBoarding
        {...props}
        client={client}
        CommonActions={CommonActions}
      />
    </NavigationContainer>
  );
};

const Wrapper = compose(withApollo)(RootContainer);
export default props => <Wrapper {...props} />;
