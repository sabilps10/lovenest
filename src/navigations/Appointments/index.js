import React, {useEffect} from 'react';
import {View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../utils/Themes/Colors';
import {FontType, FontSize} from '../../utils/Themes/Fonts';

const {black, greyLine, mainRed} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

import Completed from '../../screens/Appointment/Completed';
import InCompleted from '../../screens/Appointment/InCompleted';

const Tab = createMaterialTopTabNavigator();

const AppointmentTabs = props => {
  console.log('AppointmentTabs Props:  >>>>>>> ', props);
  const {navigation} = props;

  useEffect(() => {
    navigationOptions();
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
    });

    return () => {
      subscriber();
    };
  }, []);

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Appointment',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.2,
        borderBottomColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <View style={{display: 'none'}} />;
      },
    });
  };
  return (
    <Tab.Navigator
      lazy
      initialRouteName="Incomplete"
      tabBarPosition="top"
      tabBarOptions={{
        upperCaseLabel: false,
        indicatorStyle: {
          backgroundColor: mainRed,
        },
        labelStyle: {
          fontFamily: medium,
          letterSpacing: 0.25,
          fontSize: RFPercentage(1.6),
          textTransform: 'none',
        },
        activeTintColor: mainRed,
        inactiveTintColor: greyLine,
        allowFontScaling: true,
      }}>
      <Tab.Screen
        name="Incomplete"
        // component={InCompleted}
        options={{tabBarLabel: 'Incomplete'}}
        children={childProps => {
          return <InCompleted {...props} {...childProps} />;
        }}
      />
      <Tab.Screen
        name="Completed"
        // component={Completed}
        options={{tabBarLabel: 'Completed'}}
        children={childProps => {
          return <Completed {...props} {...childProps} />;
        }}
      />
    </Tab.Navigator>
  );
};

export default AppointmentTabs;
