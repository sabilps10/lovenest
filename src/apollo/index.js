import AsyncStorage from '@react-native-community/async-storage';
import ApolloClient from 'apollo-client';
import {createUploadLink} from 'apollo-upload-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {from} from 'apollo-link';
import {setContext} from 'apollo-link-context';
import graphqlServerEndPoint from '../environment/constant';
import AsyncStruct from '../utils/AsyncstorageDataStructure';

const {asyncToken} = AsyncStruct;

const authLink = setContext(async (_, {headers}) => {
  try {
    const token = await AsyncStorage.getItem(asyncToken);
    if (token) {
      return {
        headers: {
          ...headers,
          authorization: token || '',
        },
      };
    } else {
      return {
        headers: {
          ...headers,
          authorization: token || '',
        },
      };
    }
  } catch (error) {
    console.log('error Apollo: ', error);
  }
});

const httpLink = new createUploadLink({
  uri: graphqlServerEndPoint,
});

const createClient = () => {
  return new ApolloClient({
    link: from([authLink, httpLink]),
    cache: new InMemoryCache({
      addTypename: false,
    }),
    connectToDevTools: true,
    queryDeduplication: true,
    defaultOptions: {
      watchQuery: {
        pollInterval: 0,
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
      },
      query: {
        pollInterval: 0,
        ssr: false,
        fetchPolicy: 'no-cache',
        notifyOnNetworkStatusChange: true,
        errorPolicy: 'all',
      },
      mutate: {
        errorPolicy: 'all',
      },
    },
  });
};

const client = createClient();

export default client;
