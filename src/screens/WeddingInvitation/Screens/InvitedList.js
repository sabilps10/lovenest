import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import _ from 'lodash';
import moment from 'moment';
import AsyncImage from '../../../components/Image/AsyncImage';
import {Header, Icon, Card, CardItem} from 'native-base';
import Pie from 'react-native-pie';
import Clipboard from '@react-native-clipboard/clipboard';
import {charImage, tableIcon} from '../../../utils/Themes/Images';

const {boxTable, circleTable, retangleTable} = tableIcon;
const {charSpiteGreen, charSpiteRed, charNoteGreen, charNoteRed} = charImage;
const {
  white,
  black,
  mainRed,
  mainGreen,
  headerBorderBottom,
  greyLine,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions?.get('screen');

// Query
import GetInvitedList from '../../../graphql/queries/getInvitedListV2';

const InvitedList = props => {
  console.log('InvitedList Props: ', props);

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [totalData, setTotalData] = useState(0);
  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);

  const [refreshing, setRefreshing] = useState(false);
  const [loadMore, setLoadMore] = useState(false);

  useEffect(() => {
    fetch();

    if (refreshing) {
      onRefresh();
    }

    if (loadMore) {
      fetchMore();
    }
  }, [refreshing, loadMore]);

  const fetchMore = useCallback(async () => {
    try {
      await setPageNumber(pageNumber + 1);
      await fetch();
    } catch (error) {
      await setLoadMore(false);
    }
  }, [loadMore]);

  const onRefresh = useCallback(async () => {
    try {
      await setPageNumber(1);
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  }, [refreshing]);

  const fetch = async () => {
    try {
      props?.client
        ?.query({
          query: GetInvitedList,
          variables: {
            templateId: String(props?.route?.params?.templateId),
            itemDisplayed,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async res => {
          console.log('Res of fetch invited list: ', res);
          const {data, errors} = res;
          const {getCustomerWeddingInvitationsV2} = data;
          const {
            data: result,
            error,
            totalCount,
          } = getCustomerWeddingInvitationsV2;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setLoadMore(false);
          } else {
            await setTotalData(totalCount);

            let oldData = [...list, ...result];
            const removeDuplcateData = await _.uniqBy(oldData, 'id');

            await setList([...removeDuplcateData]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
            await setLoadMore(false);
          }
        })
        .catch(async error => {
          console.log('fetch invited list error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
          await setLoadMore(false);
        });
    } catch (error) {
      console.log('fetch invited list error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setLoadMore(false);
    }
  };

  const goBack = () => {
    try {
      props?.navigation?.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers goBack={goBack} />
          <View style={{flex: 1, paddingTop: 15}}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{alignSelf: 'center'}}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers goBack={goBack} />
          <View
            style={{
              flex: 1,
              paddingTop: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={async () => await setRefreshing(true)}
              style={{
                bottom: 45,
                borderRadius: 3,
                backgroundColor: mainGreen,
                alignSelf: 'center',
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  letterSpacing: 0.3,
                  color: white,
                }}>
                Refresh
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers goBack={goBack} />
          <View style={{flex: 1, zIndex: -999}}>
            <List
              totalData={totalData}
              loadMore={loadMore}
              setLoadMore={async e => await setLoadMore(e)}
              refreshing={refreshing}
              onRefresh={() => setRefreshing(true)}
              list={list}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
};

export const List = props => {
  const {list, refreshing, onRefresh, loadMore, setLoadMore, totalData} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return <Cards item={item} index={index} />;
    },
    [list],
  );

  const FooterList = () => {
    return (
      <View
        style={{
          width: '100%',
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>{loadMore ? 'Load more...' : ''}</Text>
      </View>
    );
  };

  return (
    <FlatList
      onEndReached={() => {
        if (list?.length < totalData) {
          setLoadMore(true);
        } else {
          setLoadMore(false);
        }
      }}
      onEndReachedThreshold={0.5}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      contentContainerStyle={{padding: 15, paddingLeft: 0, paddingRight: 0}}
      data={list}
      extraData={list}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      ListFooterComponent={FooterList}
    />
  );
};

export const Cards = props => {
  const {item, index} = props;

  const [isExpand, setIsExpand] = useState(false);

  return (
    <TouchableOpacity
      disabled
      style={{
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: headerBorderBottom,
        borderBottomColor: headerBorderBottom,
        padding: 10,
      }}>
      <View style={{width: '100%', flexDirection: 'row'}}>
        <View style={{flex: 0.25, borderWidth: 0}}>
          <View
            style={{
              width: width * 0.1,
              height: width * 0.1,
              borderRadius: (width * 0.1) / 2,
              backgroundColor: greyLine,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="user"
              style={{color: white, fontSize: RFPercentage(3)}}
            />
          </View>
        </View>
        <View style={{flex: 1, borderWidth: 0}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
          <Text
            style={{
              marginTop: 10,
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: greyLine,
              letterSpacing: 0.3,
            }}>
            {item?.phoneNo}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              marginTop: 10,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Image
              resizeMode="contain"
              source={
                item?.tableType === 'Round'
                  ? circleTable
                  : item?.tableType === 'Square'
                  ? boxTable
                  : retangleTable
              }
              style={{
                width: width * 0.05,
                height: width * 0.05,
                marginRight: 10,
              }}
            />
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.4),
                color: greyLine,
                letterSpacing: 0.3,
              }}>
              {item?.totalSeat === 0 || !item?.totalSeat
                ? '0'
                : item?.totalSeat}{' '}
              Seat(s)
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            borderWidth: 0,
            justifyContent: 'flex-end',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          {item?.attendances?.length === 0 ? null : (
            <TouchableOpacity
              onPress={() => setIsExpand(!isExpand)}
              style={{padding: 10, borderWidth: 0}}>
              <Icon
                type="Feather"
                name={isExpand ? 'chevron-up' : 'chevron-down'}
                style={{
                  fontSize: RFPercentage(3),
                  color: greyLine,
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
      {isExpand ? (
        <Childs list={item?.attendances ? item?.attendances : []} />
      ) : null}
    </TouchableOpacity>
  );
};

export const Childs = props => {
  const {list} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <View
          style={{
            width: '100%',
            padding: 5,
            borderRadius: 4,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              backgroundColor: '#F3F1F5',
              padding: 15,
              borderRadius: 3,
            }}>
            <View style={{flex: 0.25, borderWidth: 0}}>
              <View
                style={{
                  width: width * 0.1,
                  height: width * 0.1,
                  borderRadius: (width * 0.1) / 2,
                  backgroundColor: greyLine,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="user"
                  style={{color: white, fontSize: RFPercentage(3)}}
                />
              </View>
            </View>
            <View style={{flex: 1, borderWidth: 0}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {item?.name}
              </Text>
              <Text
                style={{
                  marginTop: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                {item?.phoneNo}
              </Text>
              <View
                style={{
                  marginTop: 15,
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Image
                  source={item?.rsvpStatus ? charNoteGreen : charNoteRed}
                  style={{
                    width: width * 0.05,
                    height: width * 0.05,
                    marginRight: 10,
                  }}
                  resizeMode="contain"
                />
                <Image
                  source={item?.vaccinatedSwab ? charSpiteGreen : charSpiteRed}
                  style={{
                    width: width * 0.05,
                    height: width * 0.05,
                    marginRight: 10,
                  }}
                  resizeMode="contain"
                />
              </View>
            </View>
            <View
              style={{
                flex: 0.5,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: width * 0.05,
                  height: width * 0.05,
                  borderRadius: (width * 0.05) / 2,
                  backgroundColor: item?.rsvpStatus ? '#80ED99' : '#FF0000',
                }}
              />
            </View>
          </View>
        </View>
      );
    },
    [list],
  );

  return (
    <View style={{marginTop: 15}}>
      <FlatList
        scrollEnabled={false}
        contentContainerStyle={{padding: 15}}
        data={list}
        extraData={list}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </View>
  );
};

export const Headers = props => {
  const {goBack} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Invitation List
        </Text>
      </View>
      <View style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(InvitedList);

export default props => <Wrapper {...props} />;
