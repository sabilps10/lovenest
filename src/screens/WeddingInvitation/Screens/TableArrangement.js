import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  View,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
  ScrollView,
  RefreshControl,
  Animated,
  Modal,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Icon, Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';
import {charImage} from '../../../utils/Themes/Images';
import {connect} from 'react-redux';

const {
  white,
  headerBorderBottom,
  overlayDim,
  greyLine,
  black,
  mainGreen,
  mainRed,
} = Colors;
const {book, medium} = FontType;
const {charEmptyTable, charTableIconRed} = charImage;
const {width} = Dimensions?.get('window');
import moment from 'moment';

import RNFetchBlob from 'rn-fetch-blob';

// Query GQL
import GET_TABLES from '../../../graphql/queries/getTables';
import EXPORT_PDF from '../../../graphql/queries/exportPdfTableArrangement';

// GQL Mutation
import DELETE_TABLE from '../../../graphql/mutations/deleteTable';

const TableArrangement = props => {
  console.log('TableArrangement Props: ', props);
  const {positionYBottomNav, client} = props;

  const [listTable, setListTable] = useState([]);
  const itemDisplayed = 100;
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);

  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [success, setSuccess] = useState(false);
  const [failed, setFailed] = useState(false);
  const [message, setMessage] = useState('');

  // Export PDF
  const [showModalExport, setShowModalExport] = useState(false);
  const [successDownload, setSuccessDownload] = useState(false);
  const [percentage, setPercentage] = useState(0);
  const [loadingButton, setLoadingButton] = useState(false);

  useEffect(() => {
    onChangeOpacity(false);
    fetchTableList();

    const subs = props?.navigation?.addListener('focus', () => {
      fetchTableList();
    });

    if (refreshing) {
      onRefresh();
    }

    return () => {
      subs;
    };
  }, [refreshing]);

  const onDownload = async () => {
    try {
      await setShowModalExport(true);
      if (Platform.OS === 'ios') {
        await iOSDownload();
      } else {
        await androidDownload();
      }
    } catch (error) {
      await setShowModalExport(false);
    }
  };

  const iOSDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: EXPORT_PDF,
          variables: {
            templateId: String(props?.route?.params?.templateId),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingTablesPdf} = data;
          const {data: exportData, error} = exportWeddingTablesPdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length) {
                const fileName = 'InvitationSummary.pdf';
                const paths = await RNFetchBlob.fs.dirs.CacheDir;

                await RNFetchBlob.config({
                  fileCache: true,
                  title: 'summary',
                  appendExt: 'pdf',
                  path: paths + fileName,
                })
                  .fetch(
                    'GET',
                    `${exportData[0]?.dynamicUrl}`,
                    // 'https://ars.els-cdn.com/content/image/1-s2.0-S1525001616328027-mmc2.pdf',
                    {
                      'Content-Type': 'octet-stream',
                    },
                    'base64DataString',
                  )
                  .progress({interval: 10}, async (received, total) => {
                    console.log('progress', received / total);
                    await setPercentage((received / total) * 100);
                  })
                  .then(async res => {
                    console.log('RESS BROOO: ', res.path());
                    // open the document directly

                    if (await res.path()) {
                      await setPercentage(0);
                      await setSuccessDownload(true);
                      await setSuccessDownload(false);
                      await setShowModalExport(false);

                      // await openDoc(res);
                      await RNFetchBlob.fs
                        .writeFile(res.path(), res.data, 'base64')
                        .then(async resp => {
                          console.log(resp);
                          await RNFetchBlob.ios.openDocument(res.path());
                          await setLoadingButton(false);
                        })
                        .catch(r => {
                          throw r;
                        });

                      await setTimeout(async () => {
                        await RNFetchBlob.fs
                          .writeFile(res.path(), res.data, 'base64')
                          .then(async resp => {
                            console.log(resp);
                            await RNFetchBlob.ios.openDocument(res.path());
                            await setLoadingButton(false);
                          })
                          .catch(r => {
                            throw r;
                          });
                      }, 0);
                    }
                  })
                  .catch(errorss => {
                    throw errorss;
                  });
              } else {
                await setPercentage(0);
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setPercentage(0);
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setPercentage(0);
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const androidDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: EXPORT_PDF,
          variables: {
            templateId: String(props?.route?.params?.templateId),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res DOWNLOAD ANDROID: ', ress);
          const {data, errors} = ress;
          const {exportWeddingTablesPdf} = data;
          const {data: exportData, error} = exportWeddingTablesPdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length === 1) {
                if (exportData[0]?.dynamicUrl) {
                  const fileName = `/${exportData[0]?.name}-${moment().format(
                    'DDMMMMYYYY',
                  )}-${moment().format('hhmmssa')}.pdf`;
                  RNFetchBlob.config({
                    fileCache: true,
                    addAndroidDownloads: {
                      useDownloadManager: true,
                      notification: true,
                      mediaScannable: true,
                      title: `${fileName}`,
                      path: `${RNFetchBlob?.fs?.dirs?.DownloadDir}${fileName}`,
                    },
                  })
                    .fetch(
                      'GET',
                      `${exportData[0]?.dynamicUrl}`,
                      {
                        'Content-Type': 'octet-stream',
                      },
                      'base64DataString',
                    )
                    .progress({interval: 10}, async (received, total) => {
                      console.log('progress', received / total);
                      await setPercentage((received / total) * 100);
                    })
                    .then(async res => {
                      console.log('RESS BROOO: ', res);
                      // open the document directly
                      await setSuccessDownload(true);
                      await setTimeout(async () => {
                        await setSuccessDownload(false);
                        await setShowModalExport(false);
                        await setPercentage(0);
                        await setTimeout(async () => {
                          await RNFetchBlob.android.actionViewIntent(
                            res.path(),
                            'application/pdf',
                          );
                          await setLoadingButton(false);
                        }, 1000);
                      }, 1500);
                      // or show options
                      // RNFetchBlob.ios.openDocument(res.path())
                    })
                    .catch(errorss => {
                      throw errorss;
                    });
                } else {
                  await setShowModalExport(false);
                  await setLoadingButton(false);
                }
              } else {
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const deleteTableById = async id => {
    try {
      await setIsLoadingSubmit(true);
      await setMessage('Deleting...');
      props?.client
        ?.mutate({
          mutation: DELETE_TABLE,
          variables: {
            id: parseInt(id, 10),
          },
        })
        .then(async res => {
          const {data, errors} = res;
          const {deleteTable} = data;
          const {error} = deleteTable;

          if (errors || error) {
            await setFailed(true);
            await setMessage('failed to delete!');

            setTimeout(async () => {
              await setFailed(false);
              await setSuccess(false);
              await setMessage('');
              await setIsLoadingSubmit(false);
            }, 1500);
          } else {
            await setSuccess(true);
            await setMessage('Successfully delete');

            setTimeout(async () => {
              await setFailed(false);
              await setSuccess(false);
              await setMessage('');
              await setIsLoadingSubmit(false);

              await setRefreshing(true);
            }, 1500);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setFailed(true);
      await setMessage('failed to delete!');

      setTimeout(async () => {
        await setFailed(false);
        await setSuccess(false);
        await setMessage('');
        await setIsLoadingSubmit(false);
      }, 1500);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onRefresh = useCallback(async () => {
    try {
      await setPageNumber(1);
      await fetchTableList();
    } catch (error) {
      await refreshing(false);
    }
  }, [refreshing]);

  const fetchTableList = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_TABLES,
          variables: {
            templateId: String(props?.route?.params?.templateId),
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Get Tables API: ', res);
          const {data, errors} = res;
          const {getTables} = data;
          const {data: list, error, totalCount} = getTables;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            await setTotalData(totalCount);
            let oldList = listTable;
            let newArray = [...oldList, ...list];
            let cleanUpDuplicateData = await _.uniqBy(newArray, 'tableName');

            await setListTable(pageNumber === 1 ? list : cleanUpDuplicateData);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1}}>
          <Headers goBack={() => props?.navigation?.goBack(null)} />
          <View style={{flex: 1, justifyContent: 'center'}}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginTop: 25}}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1}}>
          <Headers goBack={() => props?.navigation?.goBack(null)} />
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                height: width * 1.8,
                borderWidth: 1,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: headerBorderBottom,
                  textAlign: 'center',
                }}>
                Failed to Load Data, please pull to refresh!
              </Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1}}>
          <ModalDownloader
            visible={showModalExport}
            onClose={async () => {
              await setShowModalExport(false);
            }}
            status={successDownload}
            percentage={percentage}
          />
          <ModalLoadingSubmit
            visible={isLoadingSubmit}
            success={success}
            failed={failed}
            message={message}
          />
          <Headers goBack={() => props?.navigation?.goBack(null)} />
          <View style={{flex: 1, zIndex: -999}}>
            {listTable?.length === 0 ? null : (
              <View
                style={{
                  paddingTop: 5,
                  paddingRight: 10,
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}>
                {loadingButton ? (
                  <ActivityIndicator size="large" color={mainGreen} />
                ) : (
                  <TouchableOpacity
                    onPress={() => onDownload()}
                    style={{
                      padding: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="download"
                      style={{fontSize: RFPercentage(2.5), color: black}}
                    />
                  </TouchableOpacity>
                )}
              </View>
            )}
            <List
              deleteTable={id => {
                console.log('ID DELETE: ', id);
                deleteTableById(id);
              }}
              refreshing={refreshing}
              onRefresh={() => setRefreshing(true)}
              navigation={props?.navigation}
              templateId={props?.route?.params?.templateId}
              goToAdd={() =>
                props?.navigation?.navigate('AddTableArrangement', {
                  templateId: props?.route?.params?.templateId,
                })
              }
              list={listTable}
            />
            {listTable?.length === 0 ? null : (
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                  padding: 10,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    try {
                      props?.navigation?.navigate('AddTableArrangement', {
                        templateId: props?.route?.params?.templateId,
                      });
                    } catch (error) {}
                  }}
                  style={{
                    width: '100%',
                    backgroundColor: mainGreen,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingTop: 15,
                    paddingBottom: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: white,
                      letterSpacing: 0.3,
                    }}>
                    ADD TABLE ARRANGEMENT
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </SafeAreaView>
    );
  }
};

export const ModalDownloader = props => {
  const {visible, onClose, status, percentage} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            backgroundColor: overlayDim,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Button
            onPress={() => onClose()}
            style={{
              position: 'absolute',
              top: 10,
              right: 15,
              zIndex: 99,
              backgroundColor: white,
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              borderRadius: 35 / 2,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                color: black,
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
              }}
            />
          </Button>
          {status ? (
            <View
              style={{
                padding: 10,
                width: width / 6,
                height: width / 6,
                borderWidth: 4,
                borderColor: mainRed,
                borderRadius: ((width / 6) * 0.5 + (width / 6) * 0.5) / 2,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="check"
                style={{fontSize: 40, color: mainGreen}}
              />
            </View>
          ) : (
            <View
              style={{
                minWidth: width / 4,
                borderRadius: 4,
                padding: 10,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{marginTop: 10}}
              />
              <Text style={{color: black, marginTop: 10}}>
                {percentage.toFixed(1)}%
              </Text>
              <Text style={{color: black, marginTop: 10}}>Exporting...</Text>
            </View>
          )}
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export const ModalLoadingSubmit = props => {
  const {visible, failed, success, message} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            padding: 25,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
          }}>
          {!success && !failed ? (
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginBottom: 15}}
            />
          ) : success && !failed ? (
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: RFPercentage(2.2), color: 'green'}}
            />
          ) : !success && failed ? (
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.2), color: 'red'}}
            />
          ) : null}
          <Text
            style={{
              textAlign: 'center',
              fontSize: RFPercentage(1.8),
              fontStyle: 'italic',
            }}>
            {message}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const List = props => {
  const {list} = props;

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <TableCard
        deleteTable={id => props?.deleteTable(id)}
        navigation={props?.navigation}
        templateId={props?.templateId}
        item={item}
        index={index}
      />
    );
  };

  const ListEmptyComponent = () => {
    return (
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          height: width * 1.8,
          borderWidth: 0,
        }}>
        <View
          style={{
            width: '100%',
            paddingLeft: 15,
            paddingRight: 15,
            bottom: 35,
          }}>
          <View style={{width: '100%', height: width * 0.57, borderWidth: 0}}>
            <Image
              source={charEmptyTable}
              resizeMode="cover"
              style={{
                flex: 1,
                width: '100%',
                height: width * 0.56,
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
              }}
            />
          </View>
        </View>
        <View
          style={{
            bottom: 15,
            marginBottom: 45,
            width: '100%',
            paddingLeft: 15,
            paddingRight: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.3),
              color: black,
              letterSpacing: 0.3,
            }}>
            Table Arrangement
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          <TouchableOpacity
            onPress={() => props?.goToAdd()}
            style={{
              width: '100%',
              backgroundColor: mainGreen,
              padding: 15,
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              ADD TABLE
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <FlatList
      refreshControl={
        <RefreshControl
          refreshing={props?.refreshing}
          onRefresh={() => props?.onRefresh()}
        />
      }
      contentContainerStyle={{padding: 15}}
      data={list}
      extraData={list}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      ListEmptyComponent={ListEmptyComponent}
    />
  );
};

export const TableCard = props => {
  const {item, index} = props;

  // For minimize card of table
  const [collapse, setCollapse] = useState(false);

  return (
    <TouchableOpacity
      onPress={() => {
        try {
          props?.navigation?.navigate('EditTableArrangement', {
            templateId: props?.templateId,
            id: item?.id,
          });
        } catch (error) {}
      }}
      style={{
        marginBottom: 15,
        padding: 5,
        width: '100%',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: greyLine,
      }}>
      <View style={{width: '100%', borderWidth: 0, flexDirection: 'row'}}>
        <View
          style={{
            flex: 0.15,
            borderWidth: 0,
            paddingTop: 5,
            paddingBottom: 5,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Image
            resizeMode="contain"
            source={charTableIconRed}
            style={{
              left: 5,
              bottom: 2,
              width: width * 0.06,
              height: width * 0.07,
            }}
          />
        </View>
        <View
          style={{
            flex: 1,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'flex-start',
            paddingLeft: 10,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
            }}>
            {item?.tableName}
          </Text>
          <Text
            style={{
              marginTop: 5,
              fontStyle: 'italic',
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}>{`${item?.tableType} ${item?.guestList?.length} seat(s)`}</Text>
        </View>
        <TouchableOpacity
          disabled={true}
          onPress={() => setCollapse(!collapse)}
          style={{
            flex: 0.2,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name={collapse ? 'chevron-up' : 'chevron-down'}
            style={{
              display: 'none',
              fontSize: RFPercentage(3),
              color: '#2a2a2a',
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props?.deleteTable(item?.id);
          }}
          style={{
            flex: 0.2,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{
              fontSize: RFPercentage(3),
              color: '#2a2a2a',
            }}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export const Headers = props => {
  const {goBack} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Table Arrangement
        </Text>
      </View>
      <View style={{flex: 0.1}} />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(TableArrangement);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
