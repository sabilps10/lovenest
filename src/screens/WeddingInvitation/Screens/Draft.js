import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Header, Icon} from 'native-base';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';
import AsyncImage from '../../../components/Image/AsyncImage';

// Query
import GetDraft from '../../../graphql/queries/customerDraftV2';

// Mutation
import DeleteDraftById from '../../../graphql/mutations/deleteDraftByIdV2';

const {width, height} = Dimensions?.get('screen');
const {
  white,
  black,
  greyLine,
  overlayDim,
  mainRed,
  mainGreen,
  headerBorderBottom,
} = Colors;
const {book, medium} = FontType;

const Draft = props => {
  const {positionYBottomNav} = props;

  const [list, setList] = useState([]);
  const [totalData, setTotalData] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [selectedDraftIdToDelete, setSelectedDraftIdToDelete] = useState(null);
  const [openModalConfirmDelete, setOpenModalConfirmDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  useEffect(() => {
    onChangeOpacity(false);
    const subs = fetch();
    const onFocus = props?.navigation?.addListener('focus', () => {
      onRefresh();
    });

    if (isLoadMore) {
      fetchMore();
    }

    if (refreshing) {
      onRefresh();
    }

    if (isLoadingDelete) {
      deleteDraft();
    }

    return () => {
      subs;
      onFocus();
    };
  }, [isLoadMore, refreshing, isLoadingDelete]);

  const deleteDraft = useCallback(async () => {
    try {
      props?.client
        ?.mutate({
          mutation: DeleteDraftById,
          variables: {
            templateId: String(selectedDraftIdToDelete),
          },
        })
        .then(async res => {
          console.log('Res delete Draft: ', res);

          const {data, errors} = res;
          const {customerDeleteDraftV2} = data;
          const {error} = customerDeleteDraftV2;

          if (errors || error) {
            await setSelectedDraftIdToDelete(null);
            await setIsLoadingDelete(false);
            setTimeout(async () => {
              await setOpenModalConfirmDelete(false);
            }, 1000);
          } else {
            // success delete;
            await setSelectedDraftIdToDelete(null);
            await setIsLoadingDelete(false);
            setTimeout(async () => {
              await setOpenModalConfirmDelete(false);
              await setList([]);
              await setRefreshing(true);
            }, 1000);
          }
        })
        .catch(async error => {
          await setSelectedDraftIdToDelete(null);
          await setIsLoadingDelete(false);
          setTimeout(async () => {
            await setOpenModalConfirmDelete(false);
          }, 1000);
        });
    } catch (error) {
      await setSelectedDraftIdToDelete(null);
      await setIsLoadingDelete(false);
      setTimeout(async () => {
        await setOpenModalConfirmDelete(false);
      }, 1000);
    }
  }, [isLoadingDelete]);

  const fetch = async () => {
    try {
      console.log('Masuk Fetch', pageNumber);
      await props?.client
        ?.query({
          query: GetDraft,
          variables: {
            itemDisplayed: itemDisplayed,
            pageNumber: pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async res => {
          console.log('Res Draft: ', res);

          const {data, errors} = res;
          const {customerDrafts} = data;
          const {data: result, error, totalCount} = customerDrafts;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalData(totalCount);
              let oldData = [...result, ...list];
              const cleanData = await _.uniqBy(oldData, 'templateId');
              console.log('cleanData: ', cleanData);

              await setList([...cleanData]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('fetch draft error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const fetchMore = useCallback(async () => {
    try {
      console.log('Masuk fetchMore');
      await setPageNumber(pageNumber + 1);
      await fetch();
    } catch (error) {
      await setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const onRefresh = useCallback(() => {
    try {
      setPageNumber(1);
      setRefreshing(true);
    } catch (error) {
      setRefreshing(false);
    }
  }, [refreshing]);

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <Headers
          goBack={() => {
            props?.navigation?.goBack(null);
          }}
        />
        <View style={{flex: 1, alignItems: 'center', paddingTop: 25}}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <Headers
          goBack={() => {
            props?.navigation?.goBack(null);
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => setRefreshing(true)}
            style={{
              backgroundColor: mainGreen,
              padding: 15,
              borderRadius: 3,
              bottom: 25,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
              }}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <ModalConfirmDelete
          isLoadingDelete={isLoadingDelete}
          confirm={async () => {
            try {
              console.log('Draft ID: ', selectedDraftIdToDelete);
              if (selectedDraftIdToDelete) {
                console.log('Masuk SINI: ', selectedDraftIdToDelete);
                await setIsLoadingDelete(true);
              } else {
                console.log('Masuk Kontol');
                await setSelectedDraftIdToDelete(null);
                await setOpenModalConfirmDelete(false);
              }
            } catch (error) {
              console.log('Error delete draft: ', error);
              await setSelectedDraftIdToDelete(null);
              await setOpenModalConfirmDelete(false);
            }
          }}
          visible={openModalConfirmDelete}
          cancel={async () => {
            await setSelectedDraftIdToDelete(null);
            await setOpenModalConfirmDelete(false);
          }}
        />
        <Headers
          goBack={() => {
            props?.navigation?.goBack(null);
          }}
        />
        <View style={{flex: 1, zIndex: -99}}>
          <ListDraft
            openModalDelete={async draftId => {
              try {
                await setSelectedDraftIdToDelete(draftId);
                await setOpenModalConfirmDelete(true);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            total={totalData}
            setIsLoadMore={async status => await setIsLoadMore(status)}
            isLoadMore={isLoadMore}
            refreshing={refreshing}
            onRefresh={() => setRefreshing(true)}
            data={list}
            {...props}
          />
        </View>
      </View>
    );
  }
};

export const ModalConfirmDelete = props => {
  console.log('ModalConfirmDelete props: ', props);
  const {cancel, confirm, visible, isLoadingDelete} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
          paddingLeft: 45,
          paddingRight: 45,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: white,
            borderRadius: 5,
            paddingTop: 10,
            paddingBottom: 10,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 15,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainGreen,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Are you sure want to delete this draft ?
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={cancel}
              style={{
                padding: 10,
                borderWidth: 0,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={confirm}
              style={{
                padding: 10,
                borderWidth: 0,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainGreen,
                }}>
                {isLoadingDelete ? 'Deleting...' : 'Yes, sure!'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ListDraft = props => {
  console.log('props list draft: ', {...props});
  const {
    data,
    total,
    setIsLoadMore,
    isLoadMore,
    refreshing,
    onRefresh,
    openModalDelete,
  } = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.templateId}`;
    },
    [data],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <Cards
          item={item}
          index={index}
          {...props}
          onDelete={draftId => openModalDelete(draftId)}
        />
      );
    },
    [data],
  );

  return (
    <FlatList
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={() => onRefresh()} />
      }
      onEndReached={() => {
        if (data?.length < total) {
          console.log('Masuk True isLoadMore');
          setIsLoadMore(true);
        } else {
          console.log('Masuk False isLoadMore');
          setIsLoadMore(false);
        }
      }}
      onEndReachedThreshold={0.5}
      contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
      data={data}
      extraData={data}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      numColumns={2}
      initialNumToRender={10}
      ListHeaderComponent={() => {
        if (data?.length === 0) {
          return null;
        } else {
          return <HeaderList total={total} />;
        }
      }}
      ListEmptyComponent={() => {
        return <EmptyDraft />;
      }}
      ListFooterComponent={() => {
        if (isLoadMore) {
          return <FooterList />;
        } else {
          return null;
        }
      }}
    />
  );
};

export const EmptyDraft = props => {
  return (
    <View
      style={{
        borderWidth: 0,
        width: '100%',
        height: width * 1.6,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(2),
          color: greyLine,
          letterSpacing: 0.3,
        }}>
        Empty Draft
      </Text>
    </View>
  );
};

export const FooterList = props => {
  return (
    <View
      style={{
        width: '100%',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text>Load more...</Text>
    </View>
  );
};

export const HeaderList = props => {
  const {total} = props;
  return (
    <View
      style={{
        width: '100%',
        paddingTop: 0,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.6),
          color: greyLine,
        }}>
        Total: {total}
      </Text>
    </View>
  );
};

export const Cards = props => {
  const {item, index, onDelete} = props;
  return (
    <TouchableOpacity
      onPress={() => {
        const imageUrl = item?.coverImageUrl ? item?.coverImageUrl : null;
        const customCover = item?.customCoverImageUrl
          ? item?.customCoverImageUrl
          : null;
        console.log('KONTOL DRAFT LIST ON NAVIGATE: ', {
          item: item,
          templateId: item?.templateId,
          url: imageUrl,
          customCover,
        });
        props?.navigation?.navigate('EditorWithTemplate', {
          templateId: item?.templateId,
          url: imageUrl,
          customCover,
        });
      }}
      style={{flex: 1 / 2, borderWidth: 0, padding: 10}}>
      <View
        style={{
          width: '100%',
          height: width * 0.6,
          backgroundColor: headerBorderBottom,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          borderRadius: 3,
        }}>
        <TouchableOpacity
          onPress={() => onDelete(item?.templateId)}
          style={{
            backgroundColor: white,
            padding: 7,
            position: 'absolute',
            top: 10,
            right: 10,
            zIndex: 99,
            borderRadius: 50,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{fontSize: RFPercentage(2), color: greyLine}}
          />
        </TouchableOpacity>
        <AsyncImage
          source={{
            uri: item?.imageUrl ? `${item?.imageUrl}` : `${item?.canvasImgUrl}`,
          }}
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            borderRadius: 3,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

export const Headers = props => {
  const {goBack} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Draft
        </Text>
      </View>
      <View style={{flex: 0.1}} />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(Draft);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
