import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
  Modal,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import _ from 'lodash';
import moment from 'moment';
import AsyncImage from '../../../components/Image/AsyncImage';
import {Header, Icon, Card, CardItem, Button} from 'native-base';
import Pie from 'react-native-pie';
import Clipboard from '@react-native-clipboard/clipboard';
import {charImage} from '../../../utils/Themes/Images';

import RNFetchBlob from 'rn-fetch-blob';

const {
  white,
  black,
  mainRed,
  mainGreen,
  headerBorderBottom,
  greyLine,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions?.get('screen');

// Query
import GetRSVPDetail from '../../../graphql/queries/getRSVPDetailV2';
import ExportPDF from '../../../graphql/queries/exportWeddingAttendancePdf';

const RSVPDetail = props => {
  console.log('RSVP Detail Props: ', props);
  const {client} = props;

  const [showModalExport, setShowModalExport] = useState(false);
  const [successDownload, setSuccessDownload] = useState(false);
  const [percentage, setPercentage] = useState(0);
  const [loadingButton, setLoadingButton] = useState(false);

  const [detail, setDetail] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetch();

    if (refreshing) {
      onRefresh();
    }
  }, [refreshing]);

  const onDownload = async () => {
    try {
      await setShowModalExport(true);
      if (Platform.OS === 'ios') {
        await iOSDownload();
      } else {
        await androidDownload();
      }
    } catch (error) {
      await setShowModalExport(false);
    }
  };

  const iOSDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          variables: {
            templateId: props?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length) {
                const fileName = 'InvitationSummary.pdf';
                const paths = await RNFetchBlob.fs.dirs.CacheDir;

                await RNFetchBlob.config({
                  fileCache: true,
                  title: 'summary',
                  appendExt: 'pdf',
                  path: paths + fileName,
                })
                  .fetch(
                    'GET',
                    `${exportData[0]?.dynamicUrl}`,
                    // 'https://ars.els-cdn.com/content/image/1-s2.0-S1525001616328027-mmc2.pdf',
                    {
                      'Content-Type': 'octet-stream',
                    },
                    'base64DataString',
                  )
                  .progress({interval: 10}, async (received, total) => {
                    console.log('progress', received / total);
                    await setPercentage((received / total) * 100);
                  })
                  .then(async res => {
                    console.log('RESS BROOO: ', res.path());
                    // open the document directly

                    if (await res.path()) {
                      await setPercentage(0);
                      await setSuccessDownload(true);
                      await setSuccessDownload(false);
                      await setShowModalExport(false);

                      // await openDoc(res);
                      await RNFetchBlob.fs
                        .writeFile(res.path(), res.data, 'base64')
                        .then(async resp => {
                          console.log(resp);
                          await RNFetchBlob.ios.openDocument(res.path());
                          await setLoadingButton(false);
                        })
                        .catch(r => {
                          throw r;
                        });

                      await setTimeout(async () => {
                        await RNFetchBlob.fs
                          .writeFile(res.path(), res.data, 'base64')
                          .then(async resp => {
                            console.log(resp);
                            await RNFetchBlob.ios.openDocument(res.path());
                            await setLoadingButton(false);
                          })
                          .catch(r => {
                            throw r;
                          });
                      }, 0);
                    }
                  })
                  .catch(errorss => {
                    throw errorss;
                  });
              } else {
                await setPercentage(0);
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setPercentage(0);
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setPercentage(0);
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const androidDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          variables: {
            templateId: props?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length === 1) {
                if (exportData[0]?.dynamicUrl) {
                  const fileName = `/${exportData[0]?.name}-${moment().format(
                    'DDMMMMYYYY',
                  )}-${moment().format('hhmmssa')}.pdf`;
                  RNFetchBlob.config({
                    fileCache: true,
                    addAndroidDownloads: {
                      useDownloadManager: true,
                      notification: true,
                      mediaScannable: true,
                      title: `${fileName}`,
                      path: `${RNFetchBlob?.fs?.dirs?.DownloadDir}${fileName}`,
                    },
                  })
                    .fetch(
                      'GET',
                      `${exportData[0]?.dynamicUrl}`,
                      {
                        'Content-Type': 'octet-stream',
                      },
                      'base64DataString',
                    )
                    .progress({interval: 10}, async (received, total) => {
                      console.log('progress', received / total);
                      await setPercentage((received / total) * 100);
                    })
                    .then(async res => {
                      console.log('RESS BROOO: ', res);
                      // open the document directly
                      await setSuccessDownload(true);
                      await setTimeout(async () => {
                        await setSuccessDownload(false);
                        await setShowModalExport(false);
                        await setPercentage(0);
                        await setTimeout(async () => {
                          await RNFetchBlob.android.actionViewIntent(
                            res.path(),
                            'application/pdf',
                          );
                          await setLoadingButton(false);
                        }, 1000);
                      }, 1500);
                      // or show options
                      // RNFetchBlob.ios.openDocument(res.path())
                    })
                    .catch(errorss => {
                      throw errorss;
                    });
                } else {
                  await setShowModalExport(false);
                  await setLoadingButton(false);
                }
              } else {
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const onRefresh = useCallback(async () => {
    try {
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  }, [refreshing]);

  const fetch = async () => {
    try {
      console.log(
        'KONTOL TEMPLATE ID: ',
        String(props?.route?.params?.templateId),
      );
      await props?.client
        ?.query({
          query: GetRSVPDetail,
          variables: {
            templateId: String(props?.route?.params?.templateId),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res of Fetch Summary: ', res);
          const {data, errors} = res;
          const {getInvitationSummaryV2} = data;
          const {rsvp, vaccination, totalInvitation, error} =
            getInvitationSummaryV2;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            const summary = {
              rsvp,
              vaccination,
              totalInvitation,
            };

            await setDetail({...summary});
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(async error => {
          console.log('Error fetch: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
        });
    } catch (error) {
      console.log('Error fetch summary detail: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const goBack = () => {
    try {
      props?.navigation?.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const navigateDetailIstInvitation = async () => {
    try {
      props?.navigation?.navigate('InvitedList', {
        templateId: props?.route?.params?.templateId,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers goBack={goBack} />
          <View style={{flex: 1, paddingTop: 15}}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{alignSelf: 'center'}}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers goBack={goBack} />
          <View
            style={{
              flex: 1,
              paddingTop: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                bottom: 45,
                borderRadius: 3,
                backgroundColor: mainGreen,
                alignSelf: 'center',
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  letterSpacing: 0.3,
                  color: white,
                }}>
                Refresh
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <ModalDownloader
            visible={showModalExport}
            onClose={async () => {
              await setShowModalExport(false);
            }}
            status={successDownload}
            percentage={percentage}
          />

          <Headers goBack={goBack} />
          <View style={{flex: 1, zIndex: -999}}>
            <ScrollView
              contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => setRefreshing(true)}
                />
              }>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                {loadingButton ? (
                  <ActivityIndicator size="large" color={mainGreen} />
                ) : (
                  <TouchableOpacity
                    onPress={onDownload}
                    style={{
                      padding: 8,
                      alignSelf: 'flex-start',
                      borderWidth: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="download"
                      style={{fontSize: RFPercentage(2.2), color: black}}
                    />
                  </TouchableOpacity>
                )}
              </View>
              <Chart summary={detail} />
              <ButtonTableArrangement
                summary={detail}
                onPress={() => {
                  props?.navigation?.navigate('TableArrangement', {
                    templateId: props?.route?.params?.templateId,
                  });
                }}
              />
              <ButtonInvitationList onPress={navigateDetailIstInvitation} />
              <TotalInvitation
                summary={detail}
                totalInvitation={detail?.totalInvitation}
              />
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
};

export const ModalDownloader = props => {
  const {visible, onClose, status, percentage} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            backgroundColor: overlayDim,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Button
            onPress={() => onClose()}
            style={{
              position: 'absolute',
              top: 10,
              right: 15,
              zIndex: 99,
              backgroundColor: white,
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              borderRadius: 35 / 2,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                color: black,
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
              }}
            />
          </Button>
          {status ? (
            <View
              style={{
                padding: 10,
                width: width / 6,
                height: width / 6,
                borderWidth: 4,
                borderColor: mainRed,
                borderRadius: ((width / 6) * 0.5 + (width / 6) * 0.5) / 2,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="check"
                style={{fontSize: 40, color: mainGreen}}
              />
            </View>
          ) : (
            <View
              style={{
                minWidth: width / 4,
                borderRadius: 4,
                padding: 10,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{marginTop: 10}}
              />
              <Text style={{color: black, marginTop: 10}}>
                {percentage.toFixed(1)}%
              </Text>
              <Text style={{color: black, marginTop: 10}}>Exporting...</Text>
            </View>
          )}
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export const ButtonTableArrangement = props => {
  const {onPress} = props;

  return (
    <Card transparent style={{marginTop: 20}}>
      <CardItem
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          disabled={props?.summary?.rsvp?.attending > 0 ? false : true}
          onPress={onPress}
          style={{
            padding: 10,
            borderWidth: 2,
            borderColor:
              props?.summary?.rsvp?.attending > 0 ? mainRed : greyLine,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: props?.summary?.rsvp?.attending > 0 ? mainRed : greyLine,
              letterSpacing: 0.3,
            }}>
            TABLE ARRANGEMENT
          </Text>
        </TouchableOpacity>
      </CardItem>
    </Card>
  );
};

export const ButtonInvitationList = props => {
  const {onPress} = props;
  return (
    <Card transparent style={{marginTop: 0}}>
      <CardItem
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={onPress}
          style={{padding: 10, borderWidth: 2, borderColor: mainRed}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            INVITATION LIST
          </Text>
        </TouchableOpacity>
      </CardItem>
    </Card>
  );
};

export const TotalInvitation = props => {
  const {summary, totalInvitation} = props;
  return (
    <Card transparent>
      <CardItem>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              textAlign: 'center',
              marginBottom: 10,
            }}>
            Total of Invitation Sent
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.7),
              color: mainRed,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            {totalInvitation}
          </Text>
        </View>
      </CardItem>
      <CardItem>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Attending
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.rsvp?.attending ? summary?.rsvp?.attending : 0}
            </Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Vaccinated
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.vaccination?.vaccinated
                ? summary?.vaccination?.vaccinated
                : 0}
            </Text>
          </View>
        </View>
      </CardItem>
      <CardItem>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Not Attending
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.rsvp?.notAttending ? summary?.rsvp?.notAttending : 0}
            </Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Not Vaccinated
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.vaccination?.notVaccinated
                ? summary?.vaccination?.notVaccinated
                : 0}
            </Text>
          </View>
        </View>
      </CardItem>
    </Card>
  );
};

export const Chart = props => {
  const {summary} = props;
  return (
    <Card transparent>
      <CardItem>
        <View style={{flex: 1, borderWidth: 0}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                marginBottom: 25,
              }}>
              RSVP
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <PieChart type={'attendees'} summary={summary} />
          </View>
          <Legend title1={'Attending'} title2={'Not Attending'} />
        </View>
        <View style={{flex: 1, borderWidth: 0}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                marginBottom: 25,
              }}>
              Vaccination
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <PieChart type={'vaccination'} summary={summary} />
          </View>
          <Legend title1={'Vaccinated'} title2={'Not Vaccinated'} />
        </View>
      </CardItem>
    </Card>
  );
};

export const Legend = props => {
  const {title1, title2} = props;
  return (
    <View
      style={{
        marginTop: 15,
        width: '100%',
        borderWidth: 0,
        paddingTop: 5,
        paddingBottom: 5,
      }}>
      <View
        style={{
          paddingLeft: 25,
          width: '100%',
          borderWidth: 0,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginVertical: 5,
        }}>
        <View
          style={{
            width: 20,
            height: 20,
            backgroundColor: mainRed,
            marginRight: 10,
          }}
        />
        <Text
          style={{
            fontSize: RFPercentage(1.5),
            color: black,
            fontFamily: medium,
            letterSpacing: 0.3,
          }}>
          {title1}
        </Text>
      </View>
      <View
        style={{
          paddingLeft: 25,
          width: '100%',
          borderWidth: 0,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginVertical: 5,
        }}>
        <View
          style={{
            width: 20,
            height: 20,
            backgroundColor: greyLine,
            marginRight: 10,
          }}
        />
        <Text
          style={{
            fontSize: RFPercentage(1.5),
            color: black,
            fontFamily: medium,
            letterSpacing: 0.3,
          }}>
          {title2}
        </Text>
      </View>
    </View>
  );
};

export const PieChart = props => {
  console.log('PieChart Props: >>>>>> ', props);
  const {summary, type} = props;

  if (type === 'attendees') {
    return (
      <Pie
        backgroundColor={headerBorderBottom}
        radius={80}
        sections={[
          {
            percentage: summary?.rsvp?.attendingPerc
              ? summary?.rsvp?.attendingPerc
              : 0,
            color: mainRed,
          },
          {
            percentage: summary?.rsvp?.notAttendingPerc
              ? summary?.rsvp?.notAttendingPerc
              : 0,
            color: greyLine,
          },
        ]}
        strokeCap={'butt'}
      />
    );
  } else {
    return (
      <Pie
        backgroundColor={headerBorderBottom}
        radius={80}
        sections={[
          {
            percentage: summary?.vaccination?.vaccinatedPerc
              ? summary?.vaccination?.vaccinatedPerc
              : 0,
            color: mainRed,
          },
          {
            percentage: summary?.vaccination?.notVaccinatedPerc
              ? summary?.vaccination?.notVaccinatedPerc
              : 0,
            color: greyLine,
          },
        ]}
        strokeCap={'butt'}
      />
    );
  }
};

export const Headers = props => {
  const {goBack} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Detail
        </Text>
      </View>
      <View style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(RSVPDetail);

export default props => <Wrapper {...props} />;
