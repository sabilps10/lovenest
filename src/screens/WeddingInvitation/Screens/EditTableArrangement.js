import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  View,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
  ScrollView,
  RefreshControl,
  Animated,
  KeyboardAvoidingView,
  TextInput,
  Modal,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';
import {charImage, tableIcon} from '../../../utils/Themes/Images';
import {connect} from 'react-redux';

const {
  white,
  headerBorderBottom,
  greyLine,
  black,
  mainGreen,
  mainRed,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {charEmptyTable, charTableIconRed} = charImage;
const {boxTable, circleTable, retangleTable} = tableIcon;
const {width} = Dimensions?.get('window');

// GQL Query
import GET_TABLE from '../../../graphql/queries/getTableDetail';
import GET_GROUP from '../../../graphql/queries/getGroupContact';
import GET_GUEST from '../../../graphql/queries/getWeddingAttendance';

// GQL Mutation
import UPDATE_TABLE from '../../../graphql/mutations/updateTable';

const EditTableArrangement = props => {
  console.log('Add Table Arrangement Screen Status: ', props);
  const {positionYBottomNav} = props;

  // For List Table Added
  const [tableDetail, setTableDetail] = useState([
    {
      tableType: null,
      tableName: '',
      tableDescription: '',
      group: null,
      guestList: [],
    },
  ]);

  // For Modal Data Table Type
  const tableType = [
    {
      id: 1,
      tableName: 'Square Table',
      icon: boxTable,
    },
    {
      id: 2,
      tableName: 'Round Table',
      icon: circleTable,
    },
    {
      id: 3,
      tableName: 'Retangle Table',
      icon: retangleTable,
    },
  ];

  const itemDisplayed = 100;
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [success, setSuccess] = useState(false);
  const [failed, setFailed] = useState(false);
  const [message, setMessage] = useState('');

  useEffect(() => {
    onChangeOpacity(false);
    fetchTableList();
  }, []);

  const fetchTableList = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_TABLE,
          variables: {
            id: parseInt(props?.route?.params?.id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Get Tables Detail API: ', res);
          const {data, errors} = res;
          const {getTable} = data;
          const {data: detail, error} = getTable;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setTableDetail([detail]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onSubmit = async () => {
    try {
      await setIsLoadingSubmit(true);
      await setMessage('Submitting...');
      const tableArrangement = await cleanUpDataForSubmit(tableDetail);

      if (tableArrangement) {
        const variables = {
          id: props?.route?.params?.id,
          tableArrangement: tableArrangement[0],
        };

        console.log('VARIABLES GQL: ', variables);

        props?.client
          ?.mutate({
            mutation: UPDATE_TABLE,
            variables,
          })
          .then(async res => {
            const {data, errors} = res;
            const {updateTable} = data;
            const {data: result, error} = updateTable;

            if (errors || error) {
              await setFailed(true);
              await setMessage('failed to submit!');

              setTimeout(async () => {
                await setFailed(false);
                await setSuccess(false);
                await setMessage('');
                await setIsLoadingSubmit(false);
              }, 1500);
            } else {
              await setSuccess(true);
              await setMessage('Successfully submit');

              setTimeout(async () => {
                await setFailed(false);
                await setSuccess(false);
                await setMessage('');
                await setIsLoadingSubmit(false);

                props?.navigation?.goBack(null);
              }, 1500);
            }
          })
          .catch(error => {
            throw error;
          });
      }
    } catch (error) {
      console.log('Errro SUBMIT: ', error);
      await setFailed(true);
      await setMessage('failed to submit!');

      setTimeout(async () => {
        await setFailed(false);
        await setSuccess(false);
        await setMessage('');
        await setIsLoadingSubmit(false);
      }, 1500);
    }
  };

  const cleanUpDataForSubmit = tableDetailData => {
    return new Promise(async (resolve, reject) => {
      try {
        const datas = await Promise.all(
          tableDetailData?.map(async (d, i) => {
            const getArrayGuestSelected = await getArrayGuestSelectedData(
              d?.guestList,
            );
            console.log('getArrayGuestSelected>>>> ', getArrayGuestSelected);

            const groupId = await getGroupCleanData(d?.group);
            console.log('groupId>>>> ', groupId);

            if (
              d?.tableType === '' ||
              d?.tableName === '' ||
              !d?.group ||
              !groupId ||
              getArrayGuestSelected?.length === 0
            ) {
              reject('Invalid Data, check your input!');
            } else {
              return {
                ...d,
                group: groupId,
                guestList: getArrayGuestSelected,
              };
            }
          }),
        );

        if (datas) {
          resolve(datas);
        } else {
          reject('Invalid Data, check your input!');
        }
      } catch (error) {
        console.log('Error: ', error);
        reject(String(error));
      }
    });
  };

  const getGroupCleanData = groupData => {
    return new Promise((resolve, reject) => {
      try {
        if (groupData?.id) {
          resolve(groupData?.id);
        } else {
          reject('Please select group!');
        }
      } catch (error) {
        reject('Please select group!');
      }
    });
  };

  const getArrayGuestSelectedData = listSelectedGuest => {
    return new Promise(async (resolve, reject) => {
      try {
        const arrayTypeList = await Promise.all(
          listSelectedGuest?.map((d, i) => {
            return d?.id;
          }),
        );

        if (arrayTypeList) {
          resolve(arrayTypeList);
        } else {
          reject('Data of guest list not valid');
        }
      } catch (error) {
        reject('Data of guest list not valid');
      }
    });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: white}}>
      <View style={{flex: 1}}>
        <ModalLoadingSubmit
          visible={isLoadingSubmit}
          success={success}
          failed={failed}
          message={message}
        />
        <Headers
          goBack={() => {
            props?.navigation?.goBack(null);
          }}
        />
        <React.Fragment>
          {isLoading && !isError ? (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{marginTop: 15}}
              />
            </View>
          ) : !isLoading && isError ? (
            <View
              style={{
                flex: 1,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                Failed To Load Data
              </Text>
            </View>
          ) : (
            <TableList
              templateId={props?.route?.params?.templateId}
              client={props?.client}
              removeTable={index => {
                let oldList = tableDetail;
                oldList?.splice(index, 1);
                setTableDetail([...oldList]);
              }}
              addMoreTable={() => {
                let oldList = tableDetail;
                setTableDetail([
                  ...oldList,
                  {
                    tableType: '',
                    tableName: '',
                    tableDescription: '',
                    group: null,
                    guestList: [],
                  },
                ]);
              }}
              tableList={tableDetail}
              tableTypeList={tableType}
              onChangeTableType={(value, index) => {
                let oldList = tableDetail;
                oldList[index].tableType = value;
                setTableDetail([...oldList]);
              }}
              onChangeGroup={(value, index) => {
                let oldList = tableDetail;
                oldList[index].group = {
                  id: value?.id,
                  name: value?.name,
                };
                setTableDetail([...oldList]);
              }}
              onChangeTableName={(value, index) => {
                let oldList = tableDetail;
                oldList[index].tableName = value;
                setTableDetail([...oldList]);
              }}
              onChangeTableDescription={(value, index) => {
                let oldList = tableDetail;
                oldList[index].tableDescription = value;
                setTableDetail([...oldList]);
              }}
              onChangeGuest={(value, index) => {
                try {
                  console.log('onChangeGuest>>>> ANU: ', {value, index});
                  let oldList = tableDetail;
                  let merging = [...oldList[index].guestList, {...value}];

                  const removeDuplicated = _.uniqBy(merging, 'id');
                  oldList[index].guestList = removeDuplicated;
                  console.log('OLDIESSS GUEST DATA: ', oldList);

                  setTableDetail([...oldList]);
                } catch (error) {
                  console.log('Error OnChangeGuest: ', error);
                }
              }}
              onDeleteGuest={(idx, parentIndex) => {
                console.log('Big Parent onDeleteGuest: ', {idx, parentIndex});
                let oldData = tableDetail;
                oldData[parentIndex].guestList.splice(idx, 1);
                setTableDetail([...oldData]);
              }}
            />
          )}
        </React.Fragment>
        <View
          style={{position: 'absolute', bottom: 0, width: '100%', padding: 10}}>
          <TouchableOpacity
            onPress={onSubmit}
            style={{
              width: '100%',
              backgroundColor: mainGreen,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 15,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              EDIT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export const ModalLoadingSubmit = props => {
  const {visible, failed, success, message} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            padding: 25,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
          }}>
          {!success && !failed ? (
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginBottom: 15}}
            />
          ) : success && !failed ? (
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: RFPercentage(2.2), color: 'green'}}
            />
          ) : !success && failed ? (
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.2), color: 'red'}}
            />
          ) : null}
          <Text
            style={{
              textAlign: 'center',
              fontSize: RFPercentage(1.8),
              fontStyle: 'italic',
            }}>
            {message}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const TableList = props => {
  console.log('TableList props YUHU: ', props);
  const {tableList, addMoreTable} = props;

  const [guestSelected, setGuestSelected] = useState([]);

  useEffect(() => {
    if (tableList?.length > 0) {
      cleanDataOfGuestList();
    }
  }, [tableList]);

  const cleanDataOfGuestList = () => {
    try {
      updateGuestSelected()
        .then(res => {
          console.log('GUEST LIST ID: ', res);
          const cleanData = res?.filter(Boolean);
          console.log('KONTOL ANU: ', cleanData);
          setGuestSelected(cleanData[0] ? cleanData[0] : []);
        })
        .catch(error => {
          console.log('Error cleanDataOfGuestList: ', error);
        });
    } catch (error) {
      console.log('Error cleanDataOfGuestList 2: ', error);
    }
  };

  const updateGuestSelected = () => {
    return new Promise(async (resolve, reject) => {
      try {
        // get clean array of id for guest list
        const cleanUp = await Promise.all(
          tableList
            ?.map(async (d, i) => {
              if (d?.guestList?.length > 0) {
                const guestData = await getGuestDataClean(d?.guestList);

                return guestData;
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );
        resolve(cleanUp);
      } catch (error) {
        resolve([]);
      }
    });
  };

  const getGuestDataClean = dataList => {
    console.log('dataList >>> ', dataList);
    return new Promise(async (resolve, reject) => {
      try {
        const cleanData = await Promise.all(
          dataList?.map((d, i) => {
            return d?.id;
          }),
        );

        if (cleanData) {
          resolve(cleanData);
        } else {
          resolve(null);
        }
      } catch (error) {
        resolve(null);
      }
    });
  };

  const keyExtractor = (item, index) => {
    return `${index}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <TableCard
        parentIndex={index}
        onDeleteGuest={(idx, parentIndex) => {
          console.log('TableList onDeleteGuest: ', idx);
          props?.onDeleteGuest(idx, parentIndex);
        }}
        guestSelected={guestSelected}
        tableList={tableList}
        templateId={props?.templateId}
        client={props?.client}
        {...props}
        item={item}
        index={index}
      />
    );
  };

  const ListFooterComponent = () => {
    return (
      <View style={{width: '100%', display: 'none'}}>
        <View
          style={{
            width: '100%',
            paddingTop: 10,
            paddingBottom: 10,
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={addMoreTable}
            style={{borderWidth: 0, paddingTop: 5, paddingBottom: 5}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="plus-square"
                style={{fontSize: RFPercentage(2.8), color: mainRed}}
              />
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                Add New Table
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {/* List Added Table */}
      <FlatList
        contentContainerStyle={{padding: 15, paddingBottom: 60}}
        data={tableList}
        extraData={tableList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListFooterComponent={ListFooterComponent}
      />
    </View>
  );
};

export const TableCard = props => {
  console.log('Table card props: ', props);
  const {item, index} = props;

  // For minimize card of table
  const [collapse, setCollapse] = useState(false);

  // for open table modal
  const [onOpenModalTableType, setOnOpenTableType] = useState(false);

  // For Open modal Group
  const [onOpenModalGroup, setOnOpenModalGroup] = useState(false);

  // For open modal Guest List by groups
  const [onOpenModalGuest, setOnOpenModalGuest] = useState(false);

  return (
    <React.Fragment>
      {/* Modal Table type */}
      <ModalTableType
        tableTypeList={props?.tableTypeList}
        visible={onOpenModalTableType}
        onClose={() => setOnOpenTableType(!onOpenModalTableType)}
        onChangeTableType={value => {
          props?.onChangeTableType(value, index);
        }}
      />
      {/* Modal group */}
      <ModalGroup
        tableList={props?.tableList}
        templateId={props?.templateId}
        client={props?.client}
        visible={onOpenModalGroup}
        onClose={() => setOnOpenModalGroup(!onOpenModalGroup)}
        onChangeGroup={value => {
          props?.onChangeGroup(value, index);
        }}
      />
      {/* Modal Guest */}
      <ModalGuest
        templateId={props?.templateId}
        guestSelected={props?.guestSelected}
        client={props?.client}
        group={item?.group}
        visible={onOpenModalGuest}
        onClose={() => setOnOpenModalGuest(!onOpenModalGuest)}
        onChangeGuest={value => {
          console.log('Value Guest: ', value);
          props?.onChangeGuest({...value}, index);
        }}
      />

      {/* Content */}
      <View
        style={{
          marginBottom: 15,
          padding: 5,
          width: '100%',
          borderWidth: 1,
          borderRadius: 3,
          borderColor: greyLine,
        }}>
        <View style={{width: '100%', borderWidth: 0, flexDirection: 'row'}}>
          <View
            style={{
              flex: 0.15,
              borderWidth: 0,
              paddingTop: 5,
              paddingBottom: 5,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Image
              resizeMode="contain"
              source={charTableIconRed}
              style={{
                left: 5,
                bottom: 2,
                width: width * 0.06,
                height: width * 0.07,
              }}
            />
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'flex-start',
              paddingRight: 5,
              flexWrap: 'wrap',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.tableName === '' ? `Table ${index + 1}` : item?.tableName}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => setCollapse(!collapse)}
            style={{
              flex: 0.2,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name={collapse ? 'chevron-up' : 'chevron-down'}
              style={{fontSize: RFPercentage(3), color: '#2a2a2a'}}
            />
          </TouchableOpacity>
          {index > 0 ? (
            <TouchableOpacity
              onPress={() => props?.removeTable(index)}
              style={{
                flex: 0.2,
                borderWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: RFPercentage(3), color: '#2a2a2a'}}
              />
            </TouchableOpacity>
          ) : null}
        </View>
        {collapse ? (
          <React.Fragment>
            <View
              style={{
                borderWidth: 1,
                borderStyle: 'dashed',
                borderRadius: 5,
                borderColor: greyLine,
                height: 0.1,
                width: '100%',
                marginVertical: 10,
              }}
            />
            <View
              style={{
                width: '100%',
                borderWidth: 0,
                paddingLeft: 4,
                paddingRight: 4,
              }}>
              {/* Table Type */}
              <ButtonInput
                onPress={() => {
                  setOnOpenTableType(true);
                }}
                value={item?.tableType}
                placeholder={'Select Table Type'}
                title={'Table Type'}
              />

              {/* Table Name */}
              <Form
                required={true}
                numberOfLines={1}
                title={'Table Name'}
                value={item?.tableName}
                placeholder={'Type table name here...'}
                onChange={e => {
                  props?.onChangeTableName(e, index);
                }}
              />

              {/* Table Description */}
              <Form
                required={false}
                numberOfLines={1}
                title={'Table Description'}
                value={item?.tableDescription}
                placeholder={'Type table description here...'}
                onChange={e => {
                  props?.onChangeTableDescription(e, index);
                }}
              />

              {/* Groups */}
              <ButtonInput
                onPress={() => {
                  setOnOpenModalGroup(true);
                }}
                value={item?.group}
                placeholder={'Select group'}
                title={'Group'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                padding: 10,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => setOnOpenModalGuest(!onOpenModalGuest)}
                disabled={item?.group ? false : true}
                style={{
                  borderColor: item?.group ? mainRed : greyLine,
                  borderRadius: 15,
                  borderWidth: 1,
                  padding: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    color: item?.group ? mainRed : greyLine,
                    letterSpacing: 0.3,
                  }}>
                  Add Guest
                </Text>
              </TouchableOpacity>
            </View>

            {/* List Guest */}
            <ListGuest
              onDeleteGuest={idx => {
                console.log('List GUEST: ', idx);
                props?.onDeleteGuest(idx, props?.parentIndex);
              }}
              data={item?.guestList}
            />
          </React.Fragment>
        ) : null}
      </View>
    </React.Fragment>
  );
};

export const ListGuest = props => {
  const {data} = props;

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          width: '100%',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 5,
          paddingBottom: 10,
          borderBottomColor: headerBorderBottom,
          borderBottomWidth: 1,
          flexDirection: 'row',
        }}>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
          <Text
            style={{
              marginTop: 10,
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: greyLine,
              letterSpacing: 0.3,
            }}>
            {item?.phoneNo}
          </Text>
        </View>
        <View
          style={{
            flex: 0.15,
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={() => props?.onDeleteGuest(index)}
            style={{
              padding: 4,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: 5,
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2), color: black}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View
      style={{
        marginTop: 10,
        flex: 1,
        borderWidth: data?.length === 0 ? 0 : 1,
        borderColor: headerBorderBottom,
        borderRadius: 4,
        paddingTop: 10,
      }}>
      <View
        style={{
          width: '100%',
          paddingLeft: 10,
          paddingRight: 10,
          marginBottom: 15,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'flex-start',
            borderBottomWidth: 1,
            borderColor: mainRed,
            padding: 7,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            Selected Guest
          </Text>
        </View>
      </View>
      <FlatList
        contentContainerStyle={{padding: 15}}
        data={data}
        extraData={data}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </View>
  );
};

export const ModalGuest = props => {
  console.log('ModalGuest Props: ', props);
  const {client, group, visible, onClose, onChangeGuest, guestSelected} = props;

  const [list, setList] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);

  const [totalData, setTotalData] = useState(0);

  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    console.log('ANUUUUUU:  >>> ', guestSelected);
    if (visible && guestSelected) {
      fetch();
    }
  }, [guestSelected, visible]);

  const fetch = async () => {
    try {
      console.log('PARAMS GET GUEST: ', {
        templateId: props?.templateId,
        selectedIds: guestSelected,
        itemDisplayed,
        pageNumber,
      });
      client
        ?.query({
          query: GET_GUEST,
          variables: {
            templateId: props?.templateId,
            selectedIds: guestSelected,
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Response get Guest: ', res);
          const {data, errors} = res;
          const {getWeddingAttendancesByTemplate} = data;
          const {
            data: result,
            error,
            totalCount,
          } = getWeddingAttendancesByTemplate;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
          } else {
            await setTotalData(totalCount);
            let oldData = list;
            let newData = [...oldData, ...result];
            let cleanUpDuplicated = await _.uniqBy(newData, 'id');

            await setList([...cleanUpDuplicated]);
            await setIsError(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
          }
        });
    } catch (error) {
      console.log('Error fetch guest list: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
    }
  };

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        disabled={props?.guestSelected[index] === item?.id ? true : false}
        onPress={() => {
          onChangeGuest({
            id: item?.id,
            name: item?.name,
            phoneNo: item?.phoneNo,
          });
          onClose();
        }}
        style={{
          marginVertical: 10,
          borderBottomWidth: 1,
          borderBottomColor: headerBorderBottom,
          borderRadius: 5,
          width: '100%',
          padding: 10,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'flex-start',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: props?.guestSelected[index] === item?.id ? greyLine : black,
            letterSpacing: 0.3,
          }}>
          {item?.name}
        </Text>
        <Text
          style={{
            fontStyle: 'italic',
            marginTop: 10,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: props?.guestSelected[index] === item?.id ? greyLine : black,
            letterSpacing: 0.3,
          }}>
          {item?.phoneNo}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <Modal transparent visible={visible} animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{width: '100%', height: width * 1.5, backgroundColor: white}}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Select Guest
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
            />
          </View>

          {/* Group List */}
          <React.Fragment>
            {isLoading && !isError ? (
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator
                  size="large"
                  color={mainGreen}
                  style={{marginTop: 15}}
                />
              </View>
            ) : !isLoading && isError ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>Failed to load data guest!</Text>
              </View>
            ) : (
              <View
                style={{
                  flex: 1,
                }}>
                <FlatList
                  contentContainerStyle={{paddingLeft: 15, paddingRight: 15}}
                  data={list}
                  extraData={list}
                  keyExtractor={keyExtractor}
                  renderItem={renderItem}
                />
              </View>
            )}
          </React.Fragment>
        </View>
      </View>
    </Modal>
  );
};

export const ModalGroup = props => {
  console.log('ModalGroup Props: ', props);
  const {templateId, visible, onClose, onChangeGroup, tableList} = props;

  const [group, setGroup] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);

  useEffect(() => {
    if (visible && tableList) {
      fetchGroupAPI();
    }
  }, [visible, tableList]);

  const fetchGroupAPI = async () => {
    try {
      props?.client
        ?.query({
          query: GET_GROUP,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res fetch group: ', res);
          const {data, errors} = res;
          const {rsvpGroups} = data;
          const {data: result, error} = rsvpGroups;

          if (errors || error) {
            await setIsError(false);
            await setIsLoading(false);
          } else {
            await setGroup([...result]);
            await setIsError(false);
            await setIsLoading(false);
          }
        });
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
    }
  };

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          onChangeGroup({id: item?.id, name: item?.name});
          onClose();
        }}
        style={{
          marginVertical: 10,
          borderBottomWidth: 1,
          borderBottomColor: headerBorderBottom,
          borderRadius: 5,
          width: '100%',
          padding: 10,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
          }}>
          {item?.name}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <Modal transparent visible={visible} animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{width: '100%', height: width * 1.5, backgroundColor: white}}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Select Group
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
            />
          </View>

          {/* Group List */}
          <React.Fragment>
            {isLoading && !isError ? (
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator
                  size="large"
                  color={mainGreen}
                  style={{marginTop: 15}}
                />
              </View>
            ) : !isLoading && isError ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>Failed to load data group!</Text>
              </View>
            ) : (
              <View
                style={{
                  flex: 1,
                }}>
                <FlatList
                  contentContainerStyle={{paddingLeft: 15, paddingRight: 15}}
                  data={group}
                  extraData={group}
                  keyExtractor={keyExtractor}
                  renderItem={renderItem}
                />
              </View>
            )}
          </React.Fragment>
        </View>
      </View>
    </Modal>
  );
};

export const ModalTableType = props => {
  const {tableTypeList, visible, onClose, onChangeTableType} = props;

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          onChangeTableType(item?.tableName);
          onClose();
        }}
        style={{
          marginVertical: 10,
          borderWidth: 1,
          borderColor: headerBorderBottom,
          borderRadius: 5,
          width: '100%',
          padding: 10,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <Image
          resizeMode="contain"
          source={item?.icon}
          style={{width: width * 0.05, height: width * 0.05}}
        />
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            marginLeft: 10,
          }}>
          {item?.tableName}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <Modal transparent visible={visible} animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{width: '100%', height: width * 1.5, backgroundColor: white}}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Select Table Type
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
            />
          </View>

          {/* Table Type List */}
          <FlatList
            scrollEnabled={false}
            contentContainerStyle={{padding: 15, paddingTop: 0}}
            data={tableTypeList}
            extraData={tableTypeList}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />
        </View>
      </View>
    </Modal>
  );
};

export const Form = props => {
  const {title, value, onChange, placeholder, numberOfLines, required} = props;

  return (
    <View style={{width: '100%', marginVertical: 10}}>
      <Text>
        <Text
          style={{
            color: required ? mainRed : 'transparent',
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          *
        </Text>
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          {title}
        </Text>
      </Text>
      <View style={{width: '100%'}}>
        <TextInput
          numberOfLines={numberOfLines}
          underlineColorAndroid={'transparent'}
          placeholderTextColor={greyLine}
          style={
            Platform?.OS === 'android'
              ? {
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  borderBottomColor: headerBorderBottom,
                  borderBottomWidth: 1,
                  letterSpacing: 0.3,
                }
              : {
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  borderBottomColor: headerBorderBottom,
                  borderBottomWidth: 1,
                  letterSpacing: 0.3,
                  height: 45,
                  paddingLeft: 4,
                }
          }
          value={value}
          placeholder={placeholder}
          onChangeText={e => {
            onChange(e);
          }}
        />
      </View>
    </View>
  );
};

export const ButtonInput = props => {
  console.log('ButtonInput Props: ', props);
  const {onPress, title, placeholder, value} = props;

  return (
    <View style={{width: '100%', marginVertical: 10}}>
      <Text>
        <Text
          style={{
            color: mainRed,
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          *
        </Text>
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          {title}
        </Text>
      </Text>
      <TouchableOpacity
        onPress={onPress}
        style={{
          flexDirection: 'row',
          width: '100%',
          paddingTop: 15,
          paddingBottom: 15,
          paddingLeft: 5,
          borderBottomWidth: 1,
          borderBottomColor: headerBorderBottom,
        }}>
        <View
          style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
          <Text
            style={{
              color: value ? black : greyLine,
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
            }}>
            {value ? (title === 'Group' ? value?.name : value) : placeholder}
          </Text>
        </View>
        <View
          style={{
            flex: 0.1,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-down"
            style={{fontSize: RFPercentage(2), color: greyLine}}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export const Headers = props => {
  const {goBack} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          Edit Table Arrangement
        </Text>
      </View>
      <View style={{flex: 0.1}} />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(EditTableArrangement);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
