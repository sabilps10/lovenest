import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  useLayoutEffect,
  useImperativeHandle,
} from 'react';
import {
  Keyboard,
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  PanResponder,
  Animated,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Pressable,
  UIManager,
  findNodeHandle,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import Colors from '../../../utils/Themes/Colors';
import {FontType, arrayCanvasFont} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Dash from 'react-native-dash';
import {Icon, CardItem, Card} from 'native-base';
import Slider from '@react-native-community/slider';
import FontColorPicker from '../Libs/Colors';
import _ from 'lodash';
import {selectContactPhone} from 'react-native-select-contact';
import Contacts from 'react-native-contacts';
import moment from 'moment';
import {launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {ReactNativeFile} from 'apollo-upload-client';
import ViewShot from 'react-native-view-shot';
import RNFetchBlob from 'rn-fetch-blob';

const {medium, book} = FontType;
const {
  white,
  greyLine,
  headerBorderBottom,
  black,
  mainGreen,
  mainRed,
  overlayDim,
} = Colors;
const {width, height} = Dimensions?.get('window');

import ModalListContact from '../Components/ListContactFromPhone';
import Header from '../EditorComponents/Header';
import AsyncStorage from '@react-native-community/async-storage';

// Form Schedules
import Form from '../../../components/FormInput/FormInput';
import DateAndTimePickupForm from '../Components/DateTimeForm';

// Mutation
import uploadImageMobile from '../../../graphql/mutations/uploadImageMobile';
import createDraft from '../../../graphql/mutations/customerDraftTemplateV2';
import getDraftDetail from '../../../graphql/queries/customerDraftDetailv2';
import sendEinvitation from '../../../graphql/mutations/sendInvitationV2';

// Query
import getGroupContact from '../../../graphql/queries/getGroupContact';

/**
 * Note For API Upload Image
 * Type params has
 *  for imageUrl params use type <DraftImage>
 *  for canvasImgUrl params use type <DraftCanvasImage>
 *  for customCoverImage params use type <CustomCoverImage>
 */

const Editor = props => {
  console.log('Editor Props: ', props);
  const {positionYBottomNav, route} = props;
  const {params} = route;
  const {templateId, url: imageUrl, customCover} = params;

  // List of text compoment
  const [compText, setCompText] = useState([]);

  // Open Modal to write new text
  const [openModalAddNewText, setOpenModalAddNewText] = useState(false);

  // Open Editor toolkit such text, fontStyle, color, or align editor
  const [onOpenEditorToolKit, setOnOpenEditorToolKit] = useState(false);
  const [selectedIndexComp, setSelectedIndexComp] = useState(null);

  // Open Modal delete confirmation comp
  const [showModalDeleteComp, setShowModalDeleteComp] = useState(false);

  // Open Modal Access contacts (android and ios)
  const [onOpenContact, setOnOpenContact] = useState(false);
  const [isLoadingOpenContact, setIsLoadingOpenContact] = useState(false);
  const [isFirstTimeLoadContact, setIsFirstTimeLoadContact] = useState(0);
  const [contactSelected, setContactSelected] = useState([]);

  // custom image template
  const [customImageTemplate, setCustomImageTemplate] = useState(null);
  const [isLoadingCustomImage, setIsLoadingCustomImage] = useState(false);

  // save to draft purpose
  const [isLoadingSaveToDraft, setIsLoadingSaveToDraft] = useState(false);

  //  send invitation loading
  const [isLoadingSendInvitation, setIsLoadingSendInvitation] = useState(false);

  //  captured card view shot
  const [capturedCard, setCapturedCard] = useState(null);
  const refCapturedCard = useRef();

  // save draft loadingStatus
  const [loadingTextSaveDraft, setLoadingTextSaveDraft] =
    useState('Loading...');

  // send invitation loading status
  const [isLoadingTextSend, setIsLoadingTextSend] = useState('Loading...');

  // Scehdule
  const [schedules, setSchedules] = useState([
    {
      eventName: '',
      startTime: '',
      endTime: '',
    },
  ]);
  const [isLoadingSchedule, setIsLoadingSchedules] = useState(false);
  const [isErrorSchedules, setIsErrorSchedule] = useState(false);
  const [showModalSchedule, setShowModalSchedules] = useState(false);

  // draft load first data
  const [isLoadingDraftCover, setIsLoadingDraftCover] = useState(false);

  // Show modal after select contact
  const [showModalContactCorrection, setShowModalContactCorrection] =
    useState(false);
  const [tempContact, setTempContact] = useState([]);

  useEffect(() => {
    onChangeOpacity(false);
    if (selectedIndexComp !== null) {
      openOrCloseEditorToolKit();
    }

    if (isLoadingOpenContact) {
      openNativeContact();
    }

    if (isLoadingCustomImage) {
      pickImageCustomTemplate();
    }

    if (isLoadingSaveToDraft) {
      saveToDraft();
    }

    if (isLoadingSendInvitation) {
      sendInvitation();
    }
  }, [
    selectedIndexComp,
    isLoadingOpenContact,
    isLoadingCustomImage,
    isLoadingSaveToDraft,
    isLoadingSendInvitation,
  ]);

  useEffect(() => {
    console.log('BAZENGAN');
    const subs = fetchDraftDetail();
    return subs;
  }, []);

  const fetchDraftDetail = () => {
    try {
      console.log('PRops kontol: ', props);
      if (typeof templateId === 'string' && !imageUrl && customCover) {
        console.log('CUSTOM BG CANVAS');
        props?.client
          ?.query({
            query: getDraftDetail,
            variables: {
              templateId: String(templateId),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async resDraft => {
            console.log('Res Draft KONTOL 1: ', resDraft);
            const {data, errors} = resDraft;
            const {customerDraft} = data;
            const {data: result, error} = customerDraft;

            if (error || errors) {
              //
            } else {
              await setCustomImageTemplate(customCover);
              await setCompText([...result?.elements]);
              await setSchedules([...result?.schedules]);
              const normailize = await Promise.all(
                result?.contacts?.map((d, i) => {
                  return {
                    ...d,
                    group: d?.group?.id,
                  };
                }),
              );
              await setContactSelected([...normailize]);
            }
          })
          .catch(errorDraft => {
            console.log('Error Draft Detail: ', errorDraft);
          });
      } else if (typeof templateId === 'string' && imageUrl && !customCover) {
        console.log('DEFAULT BG CANVAS');
        props?.client
          ?.query({
            query: getDraftDetail,
            variables: {
              templateId: String(templateId),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async resDraft => {
            console.log('Res Draft KONTOL 1: ', resDraft);
            const {data, errors} = resDraft;
            const {customerDraft} = data;
            const {data: result, error} = customerDraft;

            if (error || errors) {
              //
            } else {
              await setCustomImageTemplate(null);
              await setCompText([...result?.elements]);
              await setSchedules([...result?.schedules]);
              const normailize = await Promise.all(
                result?.contacts?.map((d, i) => {
                  return {
                    ...d,
                    group: d?.group?.id,
                  };
                }),
              );
              await setContactSelected([...normailize]);
            }
          })
          .catch(errorDraft => {
            console.log('Error Draft Detail: ', errorDraft);
          });
      } else {
        // this is templateId will be number type
      }
    } catch (error) {
      console.log('Error get Draft Detail: ', error);
    }
  };

  const imagePickerAndroidPermission = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title: 'Love Nest App Gallery Access Permission',
            message:
              'Love Nest App needs access to your Gallery for using image as Invitation Background',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
          resolve(true);
        } else {
          console.log('Camera permission denied');
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const pickImageCustomTemplate = async () => {
    try {
      if (Platform?.OS === 'android') {
        //  android side
        const askPermission = await imagePickerAndroidPermission();

        if (askPermission) {
          launchImageLibrary(
            {
              mediaType: 'photo',
              selectionLimit: 1,
            },
            res => {
              if (res?.didCancel) {
                console.log('Upload image canceled');
                setIsLoadingCustomImage(false);
              } else if (res?.error) {
                console.log('image upload error: ', res?.error);
                setIsLoadingCustomImage(false);
              } else {
                const {uri} = res?.assets[0];

                if (uri) {
                  ImagePicker?.openCropper({
                    path: uri,
                    width: width * 0.9,
                    height: width * 1.25,
                  })
                    .then(image => {
                      console.log('Image Picker Cropper: ', image);
                      const {path} = image;
                      if (path) {
                        const uuid =
                          'file_' + Math.random().toString(36).substr(2, 9);
                        const file = new ReactNativeFile({
                          uri: path,
                          name: `${uuid}.png`,
                          type: 'image/png',
                        });
                        console.log('File For Custom: ', file);

                        if (file) {
                          props?.client
                            ?.mutate({
                              mutation: uploadImageMobile,
                              variables: {
                                imagefile: file,
                                type: 'CustomCoverImage',
                              },
                            })
                            .then(resultCustom => {
                              console.log(
                                'Res Upload image for Custom Cover: ',
                                resultCustom,
                              );

                              const {data, errors} = resultCustom;
                              const {uploadImageMobile: apiImageUpload} = data;
                              const {data: result, error} = apiImageUpload;

                              if (errors || error) {
                                setIsLoadingCustomImage(false);
                              } else {
                                setCustomImageTemplate(result?.url);
                                setTimeout(() => {
                                  setIsLoadingCustomImage(false);
                                }, 1500);
                              }
                            })
                            .catch(error => {
                              setIsLoadingCustomImage(false);
                            });
                        } else {
                          setIsLoadingCustomImage(false);
                        }
                      } else {
                        setIsLoadingCustomImage(false);
                      }
                    })
                    .catch(errorCropper => {
                      setIsLoadingCustomImage(false);
                    });
                } else {
                  setIsLoadingCustomImage(false);
                }
              }
            },
          );
        } else {
          // Not Granted permission
          setIsLoadingCustomImage(false);
        }
      } else {
        // iOS side
        launchImageLibrary(
          {
            mediaType: 'photo',
            selectionLimit: 1,
          },
          res => {
            console.log('Res IOS Image Picker: ', res);
            if (res?.didCancel) {
              setIsLoadingCustomImage(false);
            } else if (res?.error) {
              setIsLoadingCustomImage(false);
            } else {
              const {uri, type, filename} = res?.assets[0];
              if (uri) {
                setTimeout(async () => {
                  ImagePicker?.openCropper({
                    path: uri,
                    width: width * 0.9,
                    height: width * 1.25,
                    writeTempFile: true,
                    mediaType: 'photo',
                  })
                    .then(image => {
                      console.log('Image Picker Cropper: ', image);
                      const {path} = image;
                      if (path) {
                        const uuid =
                          'file_' + Math.random().toString(36).substr(2, 9);
                        const file = new ReactNativeFile({
                          uri: `file://${path}`,
                          name: `${uuid}.png`,
                          type,
                        });
                        console.log('File For Custom IOS: ', file);
                        if (file) {
                          props?.client
                            ?.mutate({
                              mutation: uploadImageMobile,
                              variables: {
                                imagefile: file,
                                type: 'CustomCoverImage',
                              },
                            })
                            .then(resultCustom => {
                              console.log(
                                'Res Upload image for Custom Cover IOS: ',
                                resultCustom,
                              );

                              const {data, errors} = resultCustom;
                              const {uploadImageMobile: apiImageUpload} = data;
                              const {data: result, error} = apiImageUpload;

                              if (errors || error) {
                                setIsLoadingCustomImage(false);
                              } else {
                                setCustomImageTemplate(result?.url);
                                setTimeout(() => {
                                  setIsLoadingCustomImage(false);
                                }, 1500);
                              }
                            })
                            .catch(error => {
                              setIsLoadingCustomImage(false);
                            });
                        } else {
                          setIsLoadingCustomImage(false);
                        }
                      } else {
                        setIsLoadingCustomImage(false);
                      }
                    })
                    .catch(errorCropper => {
                      setIsLoadingCustomImage(false);
                    });
                }, 2000);
              } else {
                setIsLoadingCustomImage(false);
              }
            }
          },
        );
      }
    } catch (error) {
      console.log('Error Pick Image Template: ', error);
      setIsLoadingCustomImage(false);
    }
  };

  const openModalEditContactList = () => {
    try {
      setOnOpenContact(!onOpenContact);
    } catch (error) {
      setOnOpenContact(!onOpenContact);
    }
  };

  // Android or iOS check permission
  const contactCheckPermission = () => {
    return new Promise((resolve, reject) => {
      try {
        if (Platform.OS === 'android') {
          //  Android
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
              title: 'Contacts',
              message: 'This app would like to view your contacts.',
              buttonPositive: 'Please accept bare mortal',
            },
          )
            .then(status => {
              console.log('Permission Android: ', status);
              if (status === 'granted') {
                resolve(true);
              } else {
                resolve(false);
              }
            })
            .catch(error => {
              console.log('error Permission Android: ', error);
              resolve(false);
            });
        } else {
          //  for iOS permission
          Contacts?.checkPermission()
            .then(permission => {
              if (permission === 'undefined') {
                Contacts.requestPermission().then(() => {
                  resolve(false);
                });
              }
              if (permission === 'authorized') {
                // yay!
                resolve(true);
              }
              if (permission === 'denied') {
                // x.x
                resolve(false);
              }
            })
            .catch(error => {
              resolve(false);
            });
        }
      } catch (error) {
        console.log('Error Check Permission: ', error);
        resolve(false);
      }
    });
  };

  const openLoadingBeforeAccessContact = async () => {
    try {
      const checkPermission = await contactCheckPermission();

      if (checkPermission) {
        await setIsLoadingOpenContact(true);
      } else {
        await setIsLoadingOpenContact(false);
      }
    } catch (error) {
      console.log('Error Access Contact: ', error);
      await setIsLoadingOpenContact(false);
    }
  };

  const onSaveToSelectedContact = async e => {
    try {
      let oldContactSelected = [...contactSelected, {...e}];
      const removeDuplicated = await _.uniqBy(oldContactSelected, 'id');
      await setContactSelected([...removeDuplicated]);
      await setTempContact([]);
      await setShowModalContactCorrection(false);
    } catch (error) {
      console.log('Error: ', error);
      await setShowModalContactCorrection(false);
    }
  };

  const openNativeContact = useCallback(async () => {
    try {
      if (isLoadingOpenContact) {
        if (isFirstTimeLoadContact === 0) {
          // first time load need to wait read contact and need time for it
          await setTimeout(async () => {
            const getPopUpContact = await accessContact();
            console.log('getPopUpContact first time: ', getPopUpContact);

            if (getPopUpContact?.phoneNo) {
              await setTempContact([{...getPopUpContact}]);
              await setIsFirstTimeLoadContact(isFirstTimeLoadContact + 1);
              await setIsLoadingOpenContact(false);

              await setTimeout(async () => {
                await setShowModalContactCorrection(true);
              }, 1000);
            } else {
              // failed get data
              await setIsLoadingOpenContact(false);
            }
          }, 1500);
        } else {
          //  second time all data cached, so no need to wait in long period
          const getPopUpContact = await accessContact();
          console.log('getPopUpContact second time: ', getPopUpContact);

          if (getPopUpContact?.phoneNo) {
            console.log('MASUK ADA KOMTOL');
            await setTempContact([{...getPopUpContact}]);
            await setIsFirstTimeLoadContact(isFirstTimeLoadContact + 1);
            await setIsLoadingOpenContact(false);

            await setTimeout(async () => {
              await setShowModalContactCorrection(true);
            }, 1000);
          } else {
            // failed get data
            console.log('MASUK TIDAAAKKK ADA KOMTOL');
            await setIsLoadingOpenContact(false);
          }
        }
      } else {
        // Nothing To Do
      }
    } catch (error) {
      console.log('Error Open Native Contact: ', error);
      await setIsLoadingOpenContact(false);
    }
  }, [isLoadingOpenContact]);

  const accessContact = () => {
    return new Promise(resolve => {
      try {
        selectContactPhone()
          .then(selection => {
            console.log('CONTACT SELECTION: ', selection);
            if (!selection) {
              resolve(null);
            } else {
              let {contact, selectedPhone} = selection;
              console.log(
                `Selected ${selectedPhone.type} phone number ${selectedPhone.number} from ${contact.name}`,
              );

              const normalizePhoneNumber = selectedPhone.number
                ?.replace(/\s/g, '')
                ?.replace('-', '');
              const checkPhoneNumberHasPlus =
                normalizePhoneNumber[0] === '+' ? true : false;
              console.log('KONTOL IOS: ', {
                id: contact?.recordId,
                name: contact?.name,
                phoneNo: normalizePhoneNumber.replace('-', ''),
                valid: checkPhoneNumberHasPlus,
                selected: false,
                email: '',
                note: '',
                group: '',
                gender: '',
                paxType: '',
              });
              resolve({
                id: contact?.recordId,
                name: contact?.name,
                phoneNo: normalizePhoneNumber.replace('-', ''),
                valid: checkPhoneNumberHasPlus,
                selected: false,
                email: '',
                note: '',
                group: '',
                gender: '',
                paxType: '',
              });
            }
          })
          .catch(error => {
            console.log('Error: ', error);
            resolve(null);
          });
      } catch (error) {
        resolve(null);
      }
    });
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  // Add new text data
  const addNewText = async objText => {
    try {
      const uuid = '_' + Math.random().toString(36).substr(2, 9);
      await setCompText([
        ...compText,
        {
          id: uuid,
          ...objText,
          x: 0,
          y: 0,
        },
      ]);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  //  Edit text data
  const editText = async objText => {
    try {
      compText[selectedIndexComp].text = objText.text;

      await setCompText([...compText]);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  // open/close Editor Toolkit
  const openOrCloseEditorToolKit = useCallback(() => {
    setOnOpenEditorToolKit(!onOpenEditorToolKit);
  }, [selectedIndexComp]);

  const captureCard = () => {
    return new Promise(async (resolve, reject) => {
      try {
        await refCapturedCard?.current
          ?.capture()
          .then(res => {
            console.log('URL Front Captured: ', res);
            resolve(Platform?.OS === 'ios' ? `file://${res}` : res);
          })
          .catch(error => {
            console.log('Error captured: ', error);
            resolve(false);
          });
      } catch (error) {
        console.log('Error captured: ', error);
        resolve(false);
      }
    });
  };

  const uploadFileCaptured = path => {
    console.log('KONTOL PATH: ', path);
    return new Promise((resolve, reject) => {
      try {
        const uuid = 'file_' + Math.random().toString(36).substr(2, 9);
        const file = new ReactNativeFile({
          uri: path,
          name: `${uuid}.png`,
          type: 'image/png',
        });

        console.log('FILEEEEE: ', file);

        if (file) {
          props?.client
            ?.mutate({
              mutation: uploadImageMobile,
              variables: {
                imagefile: file,
                type: customImageTemplate ? 'DraftCanvasImage' : 'DraftImage',
              },
            })
            .then(res => {
              console.log('RES SUCCESS UPLOAD IMAGE: ', res);
              const {data, errors} = res;
              const {uploadImageMobile: uploadImageMobileAPI} = data;
              const {data: result, error} = uploadImageMobileAPI;

              if (errors || error) {
                resolve(false);
              } else {
                resolve(result?.url);
              }
            })
            .catch(error => {
              console.log('Failed update captired to BE: ', error);
              resolve(false);
            });
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const sendInvitation = () => {
    try {
      console.log('Send Data Collected: ', {
        contacts: contactSelected,
        elements: compText,
        schedules,
        customImageTemplate,
      });

      captureCard()
        ?.then(res => {
          console.log('Res Captured: ', res);
          setIsLoadingTextSend('Capturing...');

          uploadFileCaptured(res)
            .then(url => {
              if (url) {
                setIsLoadingTextSend('Uploading To Server...');
                const uuid =
                  'Custom_' + Math.random().toString(36).substr(2, 9);
                const id = !customImageTemplate ? templateId : uuid;

                const checkContactIsEmpty =
                  contactSelected?.length === 0 ? true : false;
                const checkElementsIsEmpty =
                  compText?.length === 0 ? true : false;

                const variables = {
                  templateId:
                    typeof templateId === 'string'
                      ? String(templateId)
                      : String(id),
                  contacts: checkContactIsEmpty ? [] : contactSelected,
                  elements: checkElementsIsEmpty ? [] : compText,
                  schedules,
                  imageUrl: !customImageTemplate ? url : null,
                  canvasImgUrl: !customImageTemplate ? null : url,
                  type: !customImageTemplate ? 'default' : 'custom',
                  customCoverImageUrl: customImageTemplate
                    ? customImageTemplate
                    : null,
                  coverImageUrl: customImageTemplate ? null : imageUrl,
                };

                console.log('variables save to draft: ', variables);
                props?.client
                  ?.mutate({
                    mutation: sendEinvitation,
                    variables,
                  })
                  .then(resultSaving => {
                    console.log('Result Saving to Draft: ', resultSaving);
                    const {data, errors} = resultSaving;
                    const {sendInvitationV2} = data;
                    const {data: results, error} = sendInvitationV2;

                    if (errors || error) {
                      setIsLoadingTextSend('Failed To Upload');
                      setTimeout(() => {
                        setIsLoadingSendInvitation(false);
                      }, 1500);
                    } else {
                      // Success
                      console.log('Success Save: ', results);
                      setIsLoadingTextSend('Submitted Successfully');
                      setTimeout(() => {
                        setIsLoadingSendInvitation(false);
                        setTimeout(() => {
                          props?.navigation?.goBack();
                        }, 1000);
                      }, 1500);
                    }
                  })
                  .catch(errorSaveToDraft => {
                    console.log('errorSaveToDraft: ', errorSaveToDraft);
                    setIsLoadingTextSend('Failed To Upload');
                    setTimeout(() => {
                      setIsLoadingSendInvitation(false);
                    }, 1500);
                  });
              } else {
                setIsLoadingTextSend('Failed To Upload');
                setTimeout(() => {
                  setIsLoadingSendInvitation(false);
                }, 1500);
              }
            })
            .catch(error => {
              console.log('Error: ', error);
              setIsLoadingTextSend('Failed To Upload');
              setTimeout(() => {
                setIsLoadingSendInvitation(false);
              }, 1500);
            });
        })
        .catch(error => {
          console.log('Error Waiting captured: ', error);
          setIsLoadingTextSend('Failed To Upload');
          setTimeout(() => {
            setIsLoadingSendInvitation(false);
          }, 1500);
        });
    } catch (error) {
      console.log('Error Save To Draft: ', error);
      setIsLoadingTextSend('Failed To Upload');
      setTimeout(() => {
        setIsLoadingSendInvitation(false);
      }, 1500);
    }
  };

  const saveToDraft = () => {
    try {
      console.log('Save Data Collected: ', {
        contacts: contactSelected,
        elements: compText,
        schedules,
        customImageTemplate,
      });

      captureCard()
        ?.then(res => {
          console.log('Res Captured: ', res);
          setLoadingTextSaveDraft('Capturing...');

          uploadFileCaptured(res)
            .then(url => {
              if (url) {
                setLoadingTextSaveDraft('Uploading To Server...');
                const uuid =
                  'Custom_' + Math.random().toString(36).substr(2, 9);
                const id = !customImageTemplate ? templateId : uuid;

                const checkContactIsEmpty =
                  contactSelected?.length === 0 ? true : false;
                const checkElementsIsEmpty =
                  compText?.length === 0 ? true : false;

                const variables = {
                  templateId:
                    typeof templateId === 'string'
                      ? String(templateId)
                      : String(id),
                  contacts: checkContactIsEmpty ? [] : contactSelected,
                  elements: checkElementsIsEmpty ? [] : compText,
                  schedules,
                  imageUrl: !customImageTemplate ? url : null,
                  canvasImgUrl: !customImageTemplate ? null : url,
                  type: !customImageTemplate ? 'default' : 'custom',
                  customCoverImageUrl: customImageTemplate
                    ? customImageTemplate
                    : null,
                  coverImageUrl: customImageTemplate ? null : imageUrl,
                };

                console.log('variables save to draft: ', variables);
                props?.client
                  ?.mutate({
                    mutation: createDraft,
                    variables,
                  })
                  .then(resultSaving => {
                    console.log('Result Saving to Draft: ', resultSaving);
                    const {data, errors} = resultSaving;
                    const {customerDraftTemplateV2} = data;
                    const {data: results, error} = customerDraftTemplateV2;

                    if (errors || error) {
                      setLoadingTextSaveDraft('Failed To Upload');
                      setTimeout(() => {
                        setIsLoadingSaveToDraft(false);
                      }, 1500);
                    } else {
                      // Success
                      console.log('Success Save: ', results);
                      setLoadingTextSaveDraft('Successfully Save');
                      setTimeout(() => {
                        setIsLoadingSaveToDraft(false);
                        setTimeout(() => {
                          props?.navigation?.goBack();
                        }, 1000);
                      }, 1500);
                    }
                  })
                  .catch(errorSaveToDraft => {
                    console.log('errorSaveToDraft: ', errorSaveToDraft);
                    setLoadingTextSaveDraft('Failed To Upload');
                    setTimeout(() => {
                      setIsLoadingSaveToDraft(false);
                    }, 1500);
                  });
              } else {
                setLoadingTextSaveDraft('Failed To Upload');
                setTimeout(() => {
                  setIsLoadingSaveToDraft(false);
                }, 1500);
              }
            })
            .catch(error => {
              console.log('Error: ', error);
              setLoadingTextSaveDraft('Failed To Upload');
              setTimeout(() => {
                setIsLoadingSaveToDraft(false);
              }, 1500);
            });
        })
        .catch(error => {
          console.log('Error Waiting captured: ', error);
          setLoadingTextSaveDraft('Failed To Upload');
          setTimeout(() => {
            setIsLoadingSaveToDraft(false);
          }, 1500);
        });
    } catch (error) {
      console.log('Error Save To Draft: ', error);
      setLoadingTextSaveDraft('Failed To Upload');
      setTimeout(() => {
        setIsLoadingSaveToDraft(false);
      }, 1500);
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: white}}>
      {/* Modal Text Area when text selected on canvas */}
      <ModalTextArea
        selectedText={
          // For init value of text (add or edit text)
          selectedIndexComp === null ? '' : compText[selectedIndexComp]?.text
        }
        visible={openModalAddNewText} // showing modal status
        onCloseModal={() => {
          // close modal
          setOpenModalAddNewText(false);
        }}
        onCancel={() => {
          // close modal
          setOpenModalAddNewText(false);
        }}
        onSave={async objText => {
          // save final value of object of text (usage in add new text and edit text)
          if (selectedIndexComp !== null) {
            // Edit text by index selected
            await editText(objText);
            await setOpenModalAddNewText(!openModalAddNewText); // open modal with text area (add or edit text)
            await setOnOpenEditorToolKit(!onOpenEditorToolKit); // open bottom bar toolkit editor with list of buttons
            await setSelectedIndexComp(null); // reset selected index component text to default (null)
          } else {
            // Add New Text
            await addNewText(objText);
            await setOpenModalAddNewText(!openModalAddNewText);
          }
        }}
      />

      {/* Modal Confirm Delete Comp */}
      <ModalDeleteComp
        visible={showModalDeleteComp}
        cancel={async () => {
          await setSelectedIndexComp(null);
          await setOnOpenEditorToolKit(!onOpenEditorToolKit);
          await setShowModalDeleteComp(!showModalDeleteComp);
        }}
        deleteIt={async () => {
          try {
            if (selectedIndexComp !== null) {
              const oldList = compText;
              const newList = oldList;

              await newList?.splice(selectedIndexComp, 1);

              await setCompText([...newList]);

              await setSelectedIndexComp(null);
              await setOnOpenEditorToolKit(!onOpenEditorToolKit);
              await setShowModalDeleteComp(!showModalDeleteComp);
            } else {
              // Do Nothing
            }
          } catch (error) {
            console.log('Delete It Comp: ', error);
          }
        }}
      />

      {/* Modal Loading Contact */}
      <ModalLoadingContact visible={isLoadingOpenContact} />

      {/* Modal Edit List Contact/Selected Contact */}
      <ModalListContact
        selectedContactList={contactSelected}
        visible={onOpenContact}
        openOrClosed={() => openModalEditContactList()}
        onSave={async updatedContactList => {
          try {
            console.log('Masuk SIni mas Bro: ', updatedContactList);
            await setContactSelected(updatedContactList);
            await openModalEditContactList();
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
      />

      {/* Modal Schedules */}
      <ModalSchedules
        list={schedules}
        isLoading={isLoadingSchedule}
        isError={isErrorSchedules}
        visible={showModalSchedule}
        onClose={() => {
          setShowModalSchedules(prevState => !prevState);
        }}
        deleteSchedules={idx => {
          try {
            let oldSchedule = schedules;
            oldSchedule.splice(idx, 1);
            setSchedules([...oldSchedule]);
          } catch (error) {
            console.log('Error deleteSchedules: ', error);
          }
        }}
        addNewSchedule={() => {
          try {
            setSchedules([
              ...schedules,
              {
                eventName: '',
                startTime: '',
                endTime: '',
              },
            ]);
          } catch (error) {
            console.log('Error Add New Schedules: ', error);
          }
        }}
        onUpdateData={updatedScheduleList => {
          try {
            console.log('updatedScheduleList: ', updatedScheduleList);
            setSchedules([...updatedScheduleList]);
            setShowModalSchedules(false);
          } catch (error) {
            console.log('Error onUpdateData: ', error);
          }
        }}
      />

      {/* Modal Loading Save Draft */}
      <ModalLoadingSaveDraft
        visible={isLoadingSaveToDraft}
        textLoading={loadingTextSaveDraft}
      />

      {/* Modal Loading Send Invitation */}
      <ModalLoadingSendInvitation
        visible={isLoadingSendInvitation}
        textLoading={isLoadingTextSend}
      />

      {/* Modal contact correction */}
      <ModalContactCorrection
        client={props?.client}
        tempContact={tempContact?.[0]}
        visible={showModalContactCorrection}
        onClose={async () => {
          await setTempContact([]);
          await setShowModalContactCorrection(false);
        }}
        onSave={e => {
          console.log('Contact E: ', e);
          console.log('Contacts Params: ', contactSelected);
          onSaveToSelectedContact(e);
        }}
      />

      {/* Gap */}
      <Header
        {...props}
        isLoadingSaveToDraft={isLoadingSaveToDraft}
        isLoadingSendInvitation={isLoadingSendInvitation}
        isLoadingCustomImage={isLoadingCustomImage}
        customImageTemplate={customImageTemplate}
        onSaveToDraft={async () => {
          await setSelectedIndexComp(null);
          await setIsLoadingSaveToDraft(true);
        }}
        onSend={async () => {
          // sent
          await setSelectedIndexComp(null);
          await setIsLoadingSendInvitation(true);
        }}
        addCustomeImageTemplate={() => {
          console.log('Obj Image');
          setIsLoadingCustomImage(!isLoadingCustomImage);
        }}
      />
      <Canvas
        refCapturedCard={refCapturedCard}
        imageUrl={customImageTemplate ? customImageTemplate : imageUrl}
        selectedIndexComp={selectedIndexComp}
        list={compText}
        onDeleteComp={idx => {
          try {
            if (idx === selectedIndexComp) {
              //  proceed if id === selectedIndexComp to open modal
              setShowModalDeleteComp(!showModalDeleteComp);
            } else {
              // dont proceed
            }
          } catch (error) {}
        }}
        selectComponent={async idx => {
          await setSelectedIndexComp(null);
          await setSelectedIndexComp(idx);
          await setOnOpenEditorToolKit(!onOpenEditorToolKit);
        }}
        onRelease={(listData, id, x, y) => {
          try {
            // set state when xy changes while on release pan responder
            console.log('On Release Parent CB Data: ', {listData, id, x, y});
            console.log('Parent onRelease: ', listData);
            const getIndex = compText.findIndex(e => e.id === id);
            console.log('GET INDEX: ', getIndex);

            if (Platform?.OS === 'ios') {
              // IOS calculation
              // compText[getIndex].x = x - width * 0.097;
              // compText[getIndex].y = y - width * 0.412;
              compText[getIndex].x = x;
              compText[getIndex].y = y;

              setCompText([...compText]);
            } else {
              // Android calculation
              compText[getIndex].x = x - width * 0.11;
              compText[getIndex].y = y - width * 0.212;
            }
          } catch (error) {
            console.log('Error Update XY Axis: ', error);
          }
        }}
      />

      {/* All Toolkit goes here */}
      <View style={{flex: 0.4, backgroundColor: 'transparent', paddingTop: 5}}>
        {/* ToolKit Mode */}
        <EditorToolKit
          listData={compText} // list data text
          onEditText={() => setOpenModalAddNewText(!openModalAddNewText)} // showing modal text area
          onChangeFontStyle={e => {
            try {
              console.log('onChangeFontSize E: ', e);
              if (selectedIndexComp !== null) {
                let oldList = compText;
                let newList = oldList;

                newList[selectedIndexComp].fontFamily = e;
                setCompText([...newList]);
              }
            } catch (error) {
              // Error
            }
          }}
          onChangeFontSize={e => {
            try {
              console.log('onChangeFontSize E: ', e);
              if (selectedIndexComp !== null) {
                let oldList = compText;
                let newList = oldList;

                newList[selectedIndexComp].fontSize = e;
                setCompText([...newList]);
              }
            } catch (error) {
              // Error
            }
          }}
          onChangeFontColor={e => {
            try {
              console.log('onChangeFontColor E: ', e);
              if (selectedIndexComp !== null) {
                let oldList = compText;
                let newList = oldList;

                newList[selectedIndexComp].color = e;
                setCompText([...newList]);
              }
            } catch (error) {
              // Error
            }
          }}
          onChangeFontAlign={e => {
            try {
              console.log('onChangeFontAlign E: ', e);
              if (selectedIndexComp !== null) {
                let oldList = compText;
                let newList = oldList;

                newList[selectedIndexComp].textAlign = e;
                setCompText([...newList]);
              }
            } catch (error) {
              // Error
            }
          }}
          index={selectedIndexComp} // selected index of text when it pressed or selected
          visible={onOpenEditorToolKit} // showing bottom bar toolkit with list featured button
          onClose={async () => {
            await setOnOpenEditorToolKit(!onOpenEditorToolKit);
            await setSelectedIndexComp(null);
          }} // closing and reset selected index to default (null)
        />
        {/* Simple Toolkit mode */}
        <ToolKit
          onOpenSchedule={() => {
            setShowModalSchedules(prevState => !prevState);
          }}
          openEditContact={() => openModalEditContactList()}
          contactList={contactSelected}
          isContactOpened={onOpenContact}
          openContact={() => openLoadingBeforeAccessContact()}
          visible={onOpenEditorToolKit ? true : false} // show bottom bar with two button featured (add new text and upload image)
          addNewText={() => setOpenModalAddNewText(true)} // trigger show modal with text area
        />
      </View>
    </SafeAreaView>
  );
};

export const ModalContactCorrection = props => {
  console.log('ModalContactCorrection: ', props);
  const {visible, onClose, tempContact, client} = props;

  const [gender, setGender] = useState([
    {
      name: 'Male',
      selected: false,
    },
    {
      name: 'Female',
      selected: false,
    },
  ]);
  const [paxType, setPaxType] = useState([
    {
      name: 'Adult',
      selected: false,
    },
    {
      name: 'Child',
      selected: false,
    },
    {
      name: 'Baby',
      selected: false,
    },
  ]);

  const [isLoading, setIsLoading] = useState(true);

  const [list, setList] = useState(null);

  const [showModalGroups, setShowModalGroups] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState(null);

  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    if (tempContact?.id) {
      fetch();
    }
  }, [props?.tempContact?.id]);

  const fetch = async () => {
    try {
      await setList({...tempContact});
      await setIsLoading(false);
    } catch (error) {
      console.log('Prop Temp Contact: ', error);
      await setIsLoading(false);
    }
  };

  const onChangeGender = async (l, e, i) => {
    try {
      console.log('onChangeGender List Kontol: ', l);
      console.log('onChangeGender:: e & i: ', {e, i});
      await setList({...list, gender: e});
    } catch (error) {
      console.log('Error Update Gender: ', error);
    }
  };

  const onChangePaxType = async (l, e, i) => {
    try {
      console.log('onChangePaxType List Kontol: ', l);
      console.log('onChangePaxType:: e & i: ', {e, i});

      await setList({...list, paxType: e});
    } catch (error) {
      console.log('Error Update PaxType: ', error);
    }
  };

  const onSave = () => {
    try {
      console.log('Data For Save Contac: ', {
        ...list,
        group: selectedGroup?.id,
      });
      if (list) {
        if (
          list?.name === '' ||
          list?.gender === '' ||
          list?.paxType === '' ||
          list?.phoneNo === '' ||
          list?.group !== ''
        ) {
          console.log('Masuk Error Sini: ', {
            ...list,
            group: selectedGroup?.id,
            valid: true,
          });
          setErrorMsg('Failed to save, please check your input!');

          setTimeout(() => {
            setErrorMsg('');
          }, 1500);
        } else {
          if (list?.phoneNo[0] !== '+' || list?.phoneNo?.length < 11) {
            setErrorMsg('Phone number format invalid, should +6563346659');

            setTimeout(() => {
              setErrorMsg('');
            }, 1500);
          } else {
            // success
            console.log('SUKSES KONTOL: ', {
              ...list,
              group: selectedGroup?.id,
              valid: true,
            });
            props?.onSave({...list, group: selectedGroup?.id, valid: true});
          }
        }
      } else {
        // error
        setErrorMsg('Failed to save');

        setTimeout(() => {
          setErrorMsg('');
        }, 1500);
      }
    } catch (error) {
      console.log('Error on save modal contact correction: ', error);
      setErrorMsg(String(error.message));

      setTimeout(() => {
        setErrorMsg('');
      }, 1500);
    }
  };

  return (
    <>
      <Modal visible={visible} transparent animationType="slide">
        {showModalGroups ? (
          <ModalGroupContact
            client={props?.client}
            visible={showModalGroups}
            onClose={() => {
              setShowModalGroups(false);
            }}
            selectedGroup={selectedGroup}
            onSelect={async (item, index) => {
              try {
                await setSelectedGroup({id: item?.id, name: item?.name});
                await setShowModalGroups(false);
              } catch (error) {
                console.log('Error: ', error);
                await setShowModalGroups(false);
              }
            }}
          />
        ) : null}
        {/* White Area */}
        <SafeAreaView style={{flex: 1}}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform?.OS === 'ios' ? 'padding' : 'height'}>
            <View style={{flex: 1}}>
              <TouchableOpacity
                onPress={onClose}
                style={{flex: 1, backgroundColor: overlayDim}}
              />
              <View
                style={{
                  width: '100%',
                  minHeight: width * 1.5,
                  backgroundColor: white,
                }}>
                <HeadersModal
                  title={'Update Contact'}
                  onClose={onClose}
                  onSave={onSave}
                />

                <View style={{flex: 1}}>
                  {isLoading ? (
                    <ActivityIndicator
                      size="large"
                      color={mainGreen}
                      style={{alignSelf: 'center'}}
                    />
                  ) : (
                    <ScrollView
                      style={{flex: 1}}
                      contentContainerStyle={{
                        padding: 10,
                        paddingBottom: 115,
                      }}>
                      {/* Error Message */}
                      {errorMsg === '' ? null : (
                        <View
                          style={{
                            width: '100%',
                          }}>
                          <Text style={{color: mainRed, fontStyle: 'italic'}}>
                            {errorMsg}
                          </Text>
                        </View>
                      )}

                      {/* Name */}
                      <CustomTextInput
                        title="Name"
                        value={list?.name}
                        placeHolder={'Name'}
                        onChange={e => {
                          try {
                            setList({...list, name: e});
                          } catch (error) {
                            console.log('Error Name Input: ', error);
                          }
                        }}
                        keyboardType="default"
                      />
                      {/* Gender */}
                      <Gender
                        list={list}
                        title={'Gender'}
                        value={list.gender}
                        data={gender}
                        onChange={onChangeGender}
                      />
                      {/* Pax type */}
                      <PaxType
                        list={list}
                        title={'Pax Type'}
                        value={list.paxType}
                        data={paxType}
                        onChange={onChangePaxType}
                      />
                      {/* Groups */}
                      <View
                        style={{
                          width: '100%',
                          paddingTop: 10,
                          paddingBottom: 10,
                        }}>
                        <Text
                          style={{
                            color: mainRed,
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            letterSpacing: 0.3,
                          }}>
                          *
                          <Text
                            style={{
                              color: black,
                              fontFamily: medium,
                              fontSize: RFPercentage(1.8),
                              letterSpacing: 0.3,
                            }}>
                            Group
                          </Text>
                        </Text>
                        <TouchableOpacity
                          onPress={async () => await setShowModalGroups(true)}
                          style={{
                            padding: 10,
                            paddingLeft: 5,
                            paddingRight: 5,
                            borderBottomWidth: 1,
                            borderBottomColor: headerBorderBottom,
                          }}>
                          <Text>
                            {selectedGroup?.name
                              ? selectedGroup?.name
                              : 'Select group'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      {/* Email */}
                      <CustomTextInput
                        title="Email"
                        value={list?.email}
                        placeHolder={'Email'}
                        onChange={e => {
                          try {
                            setList({...list, email: e});
                          } catch (error) {
                            console.log('Error Email Input: ', error);
                          }
                        }}
                        keyboardType="default"
                      />
                      <CustomTextInput
                        title="Phone"
                        value={list?.phoneNo}
                        placeHolder={'Phone'}
                        onChange={e => {
                          try {
                            setList({...list, phoneNo: e});
                          } catch (error) {
                            console.log('Error Phone Input: ', error);
                          }
                        }}
                        keyboardType="phone-pad"
                      />
                      {/* Note */}
                      <CustomTextInput
                        title="Note"
                        value={list?.note}
                        placeHolder={'Note'}
                        onChange={e => {
                          try {
                            setList({...list, note: e});
                          } catch (error) {
                            console.log('Error Note Input: ', error);
                          }
                        }}
                        keyboardType="default"
                      />
                    </ScrollView>
                  )}
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </Modal>
    </>
  );
};

export const ModalGroupContact = props => {
  console.log('ModalGroupContact Props: ', props);
  const {visible, onClose, client, onSelect, selectedGroup} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (visible) {
      fetch();
    }
  }, [visible]);

  const fetch = () => {
    try {
      client
        ?.query({
          query: getGroupContact,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Group Contact: ', res);
          const {data, errors} = res;
          const {rsvpGroups} = data;
          const {data: result, error} = rsvpGroups;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setList([...result]);
            await setTimeout(async () => {
              await setIsError(false);
              await setIsLoading(false);
            }, 2000);
          }
        })
        .catch(error => {
          console.log('Error fetch Group: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Fetch Group: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list, selectedGroup],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity
          onPress={() => onSelect(item, index)}
          style={{
            width: '100%',
            padding: 25,
            paddingLeft: 5,
            paddingRight: 5,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: selectedGroup?.id === item?.id ? mainRed : black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
        </TouchableOpacity>
      );
    },
    [list, selectedGroup],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Group
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {}}
                style={{padding: 5, display: 'none'}}>
                <Icon
                  type="Feather"
                  name="save"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* List Groups */}
          {isLoading ? (
            <View style={{width: '100%'}}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{alignSelf: 'center'}}
              />
            </View>
          ) : (
            <FlatList
              style={{bottom: 10}}
              contentContainerStyle={{padding: 15}}
              data={list}
              extraData={list}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

export const PaxType = props => {
  console.log('PaxType Props: ', props);
  const {value, data, onChange, title} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [value, data, props?.list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      if (item?.name === value) {
        return (
          <TouchableOpacity
            onPress={() => onChange(props?.list, item?.name, index)}
            style={{
              marginRight: 10,
              borderRadius: 25,
              padding: 15,
              paddingTop: 5,
              paddingBottom: 5,
              backgroundColor: mainRed,
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            onPress={() => onChange(props?.list, item?.name, index)}
            style={{
              marginRight: 10,
              borderRadius: 25,
              padding: 15,
              paddingTop: 5,
              paddingBottom: 5,
              backgroundColor: '#FFEDED',
              borderWidth: 1,
              borderColor: mainRed,
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        );
      }
    },
    [value, data, props?.list],
  );

  return (
    <View>
      <Text
        style={{
          color: mainRed,
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          letterSpacing: 0.3,
        }}>
        *
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Text>
      <FlatList
        contentContainerStyle={{
          marginVertical: 10,
          paddingTop: 5,
          paddingBottom: 5,
          paddingLeft: 5,
          paddingRight: 5,
        }}
        horizontal={true}
        scrollEnabled={false}
        data={data}
        extraData={data}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </View>
  );
};

export const Gender = props => {
  console.log('Gender Props: ', props);
  const {value, data, onChange, title} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [data, value, props?.list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      if (item?.name === value) {
        return (
          <TouchableOpacity
            onPress={() => onChange(props?.list, item?.name, index)}
            style={{
              marginRight: 10,
              borderRadius: 25,
              padding: 15,
              paddingTop: 5,
              paddingBottom: 5,
              backgroundColor: mainRed,
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            onPress={() => onChange(props?.list, item?.name, index)}
            style={{
              marginRight: 10,
              borderRadius: 25,
              padding: 15,
              paddingTop: 5,
              paddingBottom: 5,
              backgroundColor: '#FFEDED',
              borderWidth: 1,
              borderColor: mainRed,
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        );
      }
    },
    [data, value, props?.list],
  );

  return (
    <View>
      <Text
        style={{
          color: mainRed,
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          letterSpacing: 0.3,
        }}>
        *
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Text>
      <FlatList
        contentContainerStyle={{
          marginVertical: 10,
          paddingTop: 5,
          paddingBottom: 5,
          paddingLeft: 5,
          paddingRight: 5,
        }}
        horizontal={true}
        scrollEnabled={false}
        data={data}
        extraData={data}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </View>
  );
};

export const CustomTextInput = props => {
  // phone-pad or default
  const {value, onChange, title, keyboardType, placeHolder} = props;

  const regexEmailTest = () => {
    const regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    const valid = regex.test(value);

    if (value !== '' && valid) {
      // valid
      return null;
    } else {
      if (value === '') {
        return null;
      } else {
        return (
          <Text
            style={{
              color: mainRed,
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              letterSpacing: 0.3,
            }}>
            Email format is invalid!
          </Text>
        );
      }
    }
  };

  return (
    <View style={{marginVertical: 10}}>
      <Text
        style={{
          color: mainRed,
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          letterSpacing: 0.3,
        }}>
        {title === 'Email' || title === 'Note' ? '' : '*'}
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
          }}>
          {title} {title === 'Email' || title === 'Note' ? '(Optional)' : ''}
        </Text>
      </Text>
      {title === 'Note' ? (
        <TextInput
          keyboardType={keyboardType}
          style={{
            marginVertical: 5,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
            paddingLeft: 5,
            paddingRight: 5,
            paddingTop: 5,
            paddingBottom: 5,
            minHeight: width * 0.15,
            textAlignVertical: 'top',
          }}
          multiline
          numberOfLines={5}
          value={value}
          placeholder={placeHolder}
          onChangeText={e => onChange(e)}
        />
      ) : (
        <TextInput
          keyboardType={keyboardType}
          style={{
            marginVertical: 5,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
            paddingLeft: 5,
            paddingRight: 5,
            paddingTop: 5,
            paddingBottom: 5,
          }}
          value={value}
          placeholder={placeHolder}
          onChangeText={e => onChange(e)}
        />
      )}
      {title === 'Email' ? regexEmailTest() : null}
      {title === 'Phone' ? (
        value?.length < 11 || value?.length === 0 ? (
          <Text
            style={{
              color: mainRed,
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              letterSpacing: 0.3,
            }}>
            phone number must have 11 characters length minimum
          </Text>
        ) : value[0] !== '+' ? (
          <Text
            style={{
              color: mainRed,
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              letterSpacing: 0.3,
            }}>
            format is invalid, must follow ex: +6563346659
          </Text>
        ) : null
      ) : null}
    </View>
  );
};

export const HeadersModal = props => {
  const {onClose, onSave, title} = props;
  return (
    <View
      style={{
        backgroundColor: white,
        bottom: 13,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        width: '100%',
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={onClose} style={{padding: 5}}>
          <Icon
            type="Feather"
            name="x"
            style={{fontSize: RFPercentage(2.4), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(2),
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={onSave} style={{padding: 5}}>
          <Icon
            type="Feather"
            name="save"
            style={{fontSize: RFPercentage(2.4), color: black}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const ModalLoadingSendInvitation = props => {
  const {visible, textLoading} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 45,
          paddingRight: 45,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {textLoading === 'Submitted Successfully' ? (
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: RFPercentage(2.2), color: mainGreen}}
            />
          ) : textLoading === 'Failed To Upload' ? (
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.2), color: mainRed}}
            />
          ) : (
            <ActivityIndicator size="large" color={mainGreen} />
          )}

          <Text style={{marginTop: 10}}>{textLoading}</Text>
        </View>
      </View>
    </Modal>
  );
};

export const ModalLoadingSaveDraft = props => {
  const {visible, textLoading} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 45,
          paddingRight: 45,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {textLoading === 'Successfully Save' ? (
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: RFPercentage(2.2), color: mainGreen}}
            />
          ) : textLoading === 'Failed To Upload' ? (
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.2), color: mainRed}}
            />
          ) : (
            <ActivityIndicator size="large" color={mainGreen} />
          )}

          <Text style={{marginTop: 10}}>{textLoading}</Text>
        </View>
      </View>
    </Modal>
  );
};

export const CardSchedules = props => {
  const {
    item,
    index,
    onChangeEventName,
    onChangeStartTime,
    onChangeEndTime,
    deleteSchedules,
  } = props;

  return (
    <Card style={{borderRadius: 5}}>
      <CardItem
        style={{
          paddingLeft: 15,
          paddingRight: 15,
          backgroundColor: 'transparent',
        }}>
        <View style={{marginRight: 10}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: mainRed,
            }}>
            Event Schedule #{index + 1}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            borderBottomWidth: 1,
            borderColor: mainRed,
          }}
        />
        {index === 0 ? null : (
          <TouchableOpacity
            onPress={() => {
              deleteSchedules(index);
            }}
            style={{
              width: 25,
              height: 25,
              marginLeft: 10,
              borderRadius: 4,
              borderWidth: 1,
              borderColor: headerBorderBottom,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 2,
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                left: Platform?.OS === 'android' ? 8 : 9,
                fontSize: 15,
                color: greyLine,
              }}
            />
          </TouchableOpacity>
        )}
      </CardItem>
      <CardItem
        style={{
          paddingTop: 0,
          width: '100%',
          paddingLeft: 0,
          paddingRight: 0,
          backgroundColor: 'transparent',
        }}>
        <View>
          <View style={{width: '100%'}}>
            <Form
              isError={false}
              required
              value={item?.eventName}
              onChangeText={text => {
                console.log('Text Event Name: ', text);
                onChangeEventName(index, text);
              }}
              title={'Event Name'}
              placeholder={'Event Name'}
              multiline={false}
              numberOfLines={1}
              isLoading={false}
            />
          </View>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <DateAndTimePickupForm
                title={'Start Time'}
                placeholder={'Select time'}
                disabled={false}
                type={'Time'}
                date={item?.startTime}
                onChange={time => {
                  console.log(
                    'onChangeTime Start Time: ',
                    moment(time).format('hh:mm:ss'),
                  );

                  onChangeStartTime(index, time);
                }}
              />
            </View>
            <View style={{flex: 1}}>
              <DateAndTimePickupForm
                title={'End Time'}
                placeholder={'Select time'}
                disabled={false}
                type={'Time'}
                date={item?.endTime}
                onChange={time => {
                  console.log(
                    'onChangeTime End Time: ',
                    moment(time).format('hh:mm:ss'),
                  );

                  onChangeEndTime(index, time);
                }}
              />
            </View>
          </View>
        </View>
      </CardItem>
    </Card>
  );
};

export const ModalSchedules = props => {
  const {
    visible,
    onClose,
    list,
    isLoading,
    isError,
    addNewSchedule,
    deleteSchedules,
    onUpdateData,
  } = props;

  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, [list?.length]);

  const fetchData = () => {
    setData([...list]);
  };

  const keyExtractor = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [data],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <CardSchedules
          item={item}
          index={index}
          deleteSchedules={idx => deleteSchedules(idx)}
          onChangeEventName={(idx, e) => {
            try {
              console.log('OnchangeEventName: ', {idx, e});
              let oldData = data;
              oldData[idx].eventName = e;
              setData([...oldData]);
            } catch (error) {
              console.log('Error OnChangeEventName: ', error);
            }
          }}
          onChangeStartTime={(idx, e) => {
            try {
              let oldData = data;
              oldData[idx].startTime = e;
              setData([...oldData]);
            } catch (error) {
              console.log('Error onChangeStartTime: ', error);
            }
          }}
          onChangeEndTime={(idx, e) => {
            try {
              let oldData = data;
              oldData[idx].endTime = e;
              setData([...oldData]);
            } catch (error) {
              console.log('Error onChangeEndTime: ', error);
            }
          }}
        />
      );
    },
    [data],
  );

  const ListFooterComponent = () => {
    return (
      <View
        style={{
          paddingTop: 10,
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            addNewSchedule();
          }}
          style={{
            flexDirection: 'row',
            padding: 5,
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <View
            style={{
              borderRadius: 3,
              marginRight: 10,
              width: 20,
              height: 20,
              borderWidth: 1,
              borderColor: mainRed,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="plus"
              style={{color: mainRed, fontSize: 15}}
            />
          </View>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            Add more schedule
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Modal visible={visible} transparent animationType="slide">
      <SafeAreaView style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View style={{backgroundColor: white, minHeight: width * 1.7}}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Schedules
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => onUpdateData(data)}
                style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="save"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform?.OS === 'android' ? 'height' : 'padding'}>
            <FlatList
              contentContainerStyle={{
                paddingBottom: 15,
                paddingLeft: 15,
                paddingRight: 15,
              }}
              data={data}
              extraData={data}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
              ListFooterComponent={() => ListFooterComponent()}
            />
          </KeyboardAvoidingView>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export const ModalLoadingContact = props => {
  const {visible} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          backgroundColor: overlayDim,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 35,
          paddingRight: 35,
        }}>
        <View
          style={{
            borderRadius: 5,
            padding: 15,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginVertical: 10}}
          />
          <Text
            style={{
              lineHeight: 21,
              letterSpacing: 0.3,
              fontFamily: book,
              fontSize: RFPercentage(1.7),
              color: black,
              textAlign: 'center',
            }}>
            Plase wait , we are scanning your contact and it might take several
            minutes
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const Canvas = props => {
  console?.log('Canvas Length List: ', props?.list?.length);
  const {
    imageUrl,
    selectedIndexComp,
    list,
    onDeleteComp,
    selectComponent,
    onRelease,
    refCapturedCard,
  } = props;

  const [data, setData] = useState([]);

  useEffect(() => {
    updateList();
  }, [list?.length]);

  const updateList = useCallback(() => {
    setData([...props?.list]);
  }, [list?.length]);

  return (
    <View
      style={{
        flex: 1.2,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderWidth: 0,
        paddingTop: 5,
      }}>
      <ViewShot ref={refCapturedCard}>
        <View
          style={{
            width: width * 0.8,
            height: width * 1.11,
            borderWidth: Platform?.OS === 'ios' ? 0.5 : 0,
            borderColor:
              Platform?.OS === 'ios' ? headerBorderBottom : 'transparent',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
            elevation: 3,
          }}>
          <ImageBackground
            source={{uri: imageUrl}}
            resizeMode="cover"
            style={{
              flex: 1,
              position: 'absolute',
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              borderRadius: 3,
            }}>
            <ListCompText
              selectedIndexComp={selectedIndexComp}
              list={data}
              onDeleteComp={idx => {
                try {
                  onDeleteComp(idx);
                } catch (error) {}
              }}
              selectComponent={async idx => {
                try {
                  await selectComponent(idx);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              onRelease={async (listData, id, x, y) => {
                try {
                  await onRelease(listData, id, x, y);
                } catch (error) {
                  console.log('Error Update XY Axis: ', error);
                }
              }}
            />
          </ImageBackground>
        </View>
      </ViewShot>
    </View>
  );
};

export const ModalDeleteComp = props => {
  const {visible, cancel, deleteIt} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 45,
          paddingRight: 45,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: white,
            borderWidth: 1,
            borderRadius: 5,
            padding: 15,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              flexWrap: 'wrap',
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Are sure want to delete this items ?
            </Text>
          </View>
          <View
            style={{
              marginTop: 25,
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={cancel}
              style={{
                marginHorizontal: 10,
                backgroundColor: white,
                borderWidth: 1,
                borderColor: mainRed,
                padding: 10,
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.5),
                  color: mainRed,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={deleteIt}
              style={{
                backgroundColor: white,
                borderWidth: 0,
                borderColor: mainRed,
                padding: 10,
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.5),
                  color: mainRed,
                }}>
                Yes, Delete it!
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ModalTextArea = props => {
  console.log('ModalTextArea Props: ', props);
  const {visible, onCloseModal, onCancel, onSave, selectedText} = props;
  const [text, setText] = useState('');

  useEffect(() => {
    fetchText();
    if (!visible) {
      resetText();
    }
  }, [visible]);

  const fetchText = () => {
    try {
      setText(selectedText);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const resetText = useCallback(() => {
    if (!visible) {
      setText('');
    }
  }, [visible]);

  const saveUpdate = () => {
    if (text?.length === 0) {
      onCancel();
    } else {
      onSave({
        text,
        fontSize: 1.5,
        color: '#6B6B6B',
        fontFamily: 'Arial',
        textAlign: 'left',
      });
    }
  };

  return (
    <Modal visible={visible} transparent animationType="slide">
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform?.OS === 'android' ? 'height' : 'padding'}>
        <View style={{flex: 1}}>
          <TouchableOpacity
            onPress={onCloseModal}
            style={{flex: 1, backgroundColor: overlayDim}}
          />
          <View
            style={{
              minHeight: width * 0.7,
              backgroundColor: white,
              width: '100%',
            }}>
            <View
              style={{
                backgroundColor: white,
                bottom: 15,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                borderBottomColor: headerBorderBottom,
                borderBottomWidth: 1,
                width: '100%',
                padding: 15,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                {selectedText === '' ? 'Add New Text' : 'Edit Text'}
              </Text>
            </View>
            <View style={{width: '100%', padding: 15, paddingTop: 0}}>
              <TextInput
                textAlignVertical="top"
                value={text}
                placeholder={'write text here...'}
                style={{
                  padding: 10,
                  width: '100%',
                  minHeight: width * 0.3,
                  borderWidth: 1,
                  borderColor: headerBorderBottom,
                  borderRadius: 3,
                  backgroundColor: '#ebeff5',
                }}
                multiline={true}
                numberOfLines={Platform?.OS === 'ios' ? 100 : 5}
                onChangeText={e => {
                  setText(e);
                }}
              />
            </View>
            <View
              style={{
                padding: 15,
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={onCancel}
                style={{
                  backgroundColor: white,
                  padding: 8,
                  paddingLeft: 15,
                  paddingRight: 15,
                  borderRadius: 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: mainRed,
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={saveUpdate}
                style={{
                  marginLeft: 15,
                  backgroundColor: mainGreen,
                  padding: 9,
                  paddingLeft: 25,
                  paddingRight: 25,
                  borderRadius: 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: white,
                  }}>
                  Save
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export const ListCompText = props => {
  const {list, selectComponent, selectedIndexComp, onDeleteComp, onRelease} =
    props;

  const [isLoading, setIsLoading] = useState(true);
  const [updatedList, setUpdatedList] = useState([]);

  useEffect(() => {
    fetchUpdatedList();
  }, [list?.length]);

  const fetchUpdatedList = () => {
    setUpdatedList([...list]);
    setIsLoading(false);
  };

  if (isLoading) {
    return null;
  } else {
    return (
      <View
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          borderWidth: 1,
          borderColor: 'transparent',
        }}>
        {updatedList?.map((item, index) => {
          return (
            <PanComp
              key={`panComp${item?.id}`}
              onDeleteComp={idx => onDeleteComp(idx)}
              list={updatedList}
              lengthData={updatedList?.length}
              selectedIndexComp={selectedIndexComp}
              selectComponent={idx => selectComponent(idx)}
              item={item}
              index={index}
              onRelease={(listData, idx, x, y) => {
                onRelease(listData, idx, x, y);
              }}
            />
          );
        })}
      </View>
    );
  }
};

export const PanComp = props => {
  console.log('PanComp: ', props);
  const {
    list,
    item,
    index,
    onRelease,
    selectComponent,
    selectedIndexComp,
    lengthData,
    onDeleteComp,
  } = props;

  const boxRef = useRef();
  const pan = useRef(new Animated.ValueXY()).current;

  // To update prop of useRef per component, because if not using this the list props not update because of useref in nested component
  const [onEnd, setOnEnd] = useState('standBy');

  const panResponder = useRef(
    PanResponder?.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: (evt, gesture) => {
        // setOnEnd('standBy');
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value,
        });
        pan.setValue({x: 0, y: 0});
      },
      onPanResponderMove: (evt, gesture) => {
        setOnEnd('onMove');
        return Animated.event([
          null,
          {
            dx: pan.x,
            dy: pan.y,
          },
        ])(evt, gesture);
      },
      onPanResponderRelease: (evt, gesture) => {
        setOnEnd('onEnd');
        pan.flattenOffset();
      },
    }),
  ).current;

  useEffect(() => {
    if (onEnd === 'onEnd') {
      getUpdatePosition();
    }

    if (onEnd === 'standBy') {
      fetchXY();
    }
  }, [onEnd]);

  const fetchXY = () => {
    try {
      pan?.setValue({x: item?.x, y: item?.y});
    } catch (error) {
      console.log('Error Fetch XY: ', error);
    }
  };

  const measureNode = node => {
    // TODO - there are problems on android
    if (Platform?.OS === 'android') {
      return new Promise((resolve, reject) => {
        boxRef?.current?.measureInWindow((x, y, w, h) => {
          resolve({x, y});
        });
      });
    } else {
      return new Promise((resolve, reject) =>
        UIManager.measure(node, (x, y, width, height, pageX, pageY) => {
          console.log(y);
          resolve({x, y});
        }),
      );
    }
  };

  const getUpdatePosition = useCallback(async () => {
    try {
      const getXY = await measureNode(findNodeHandle(boxRef?.current));
      console.log('KONTOL XY: ', getXY);
      if (getXY) {
        const {x, y} = getXY;
        onRelease(list, item.id, x, y);
      }
      // dont use measure, in android x and y always 0 when using measure, better using measureInWindow
      // boxRef?.current?.measureInWindow((x, y, w, h) => {
      //   onRelease(list, item.id, x, y);
      // });
    } catch (error) {
      console.log('Error: getUpdatePosition: ', error);
    }
  }, [onEnd]);

  return (
    <Animated.View
      renderToHardwareTextureAndroid={true}
      collapsable={false}
      ref={boxRef}
      {...panResponder?.panHandlers}
      style={[
        {
          position: 'absolute',
          zIndex: index + 2,
          alignSelf: 'flex-start',
          flexDirection: 'row',
          transform: [
            {
              translateX: pan?.x.interpolate({
                inputRange: [-10, ((width * 0.8) / 100) * 78],
                outputRange: [-10, ((width * 0.8) / 100) * 78],
                extrapolate: 'clamp',
              }),
            },
            {
              translateY: pan?.y.interpolate({
                inputRange: [-10, ((width * 1.11) / 100) * 90],
                outputRange: [-10, ((width * 1.11) / 100) * 90],
                extrapolate: 'clamp',
              }),
            },
          ],
        },
      ]}>
      <TouchableOpacity
        onPress={() => {
          selectComponent(index);
        }}
        style={{
          alignSelf: 'flex-start',
          borderStyle: 'dashed',
          borderWidth:
            selectedIndexComp !== null
              ? index === selectedIndexComp
                ? 1
                : 0
              : lengthData > 0
              ? selectedIndexComp === null
                ? 0
                : selectedIndexComp === lengthData - 1
                ? 1
                : 0
              : 0,
          borderRadius: 1,
          borderColor: item?.color ? item?.color : '#6B6B6B',
          padding: 10,
        }}>
        <Text
          style={{
            fontFamily: item?.fontFamily ? item?.fontFamily : 'Arial',
            textAlign: item?.textAlign ? item?.textAlign : 'left',
            fontSize: RFPercentage(item?.fontSize ? item?.fontSize : 1.5),
            color: item?.color ? item?.color : '#6B6B6B',
          }}>
          {item?.text}
        </Text>
      </TouchableOpacity>
      {index === selectedIndexComp && selectedIndexComp !== null ? (
        <TouchableOpacity
          onPress={() => onDeleteComp(index)}
          style={{
            marginLeft: 10,
            alignSelf: 'flex-start',
            padding: 10,
            borderWidth: 1,
            borderColor: headerBorderBottom,
            backgroundColor: white,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="trash-2"
            style={{fontSize: RFPercentage(2.2), color: 'red'}}
          />
        </TouchableOpacity>
      ) : null}
    </Animated.View>
  );
};

export const EditorToolKitXButton = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        marginLeft: 10,
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: (width * 0.09) / 2,
        width: width * 0.09,
        height: width * 0.09,
        backgroundColor: mainRed,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="x"
        style={{fontSize: RFPercentage(2.5), color: white}}
      />
    </TouchableOpacity>
  );
};

export const EditorToolKitMainButton = props => {
  // type = MaterialIcons
  // name = keyboard
  const {isActive, type, name, text, onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        marginHorizontal: 10,
        padding: 5,
        borderWidth: name === 'x' ? 1 : 0,
        borderColor: name === 'x' ? mainRed : 'transparent',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Icon
        type={type}
        name={name}
        style={{
          fontSize:
            text === 'Align' || text === 'Size' || text === 'Close'
              ? RFPercentage(2.5)
              : RFPercentage(3),
          color: isActive ? mainRed : black,
        }}
      />
      <Text
        style={{
          fontFamily: medium,
          fontSize: text === 'Close' ? RFPercentage(1.5) : RFPercentage(1.7),
          color: isActive ? mainRed : black,
          letterSpacing: 0.3,
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export const EditorToolKit = props => {
  const {
    visible,
    onClose,
    onEditText,
    index,
    listData,
    onChangeFontSize,
    onChangeFontAlign,
    onChangeFontColor,
    onChangeFontStyle,
  } = props;

  const [isActive, setIsActive] = useState(null);

  useEffect(() => {
    if (!visible) {
      resetIsActive();
    }
  }, [visible]);

  const resetIsActive = () => {
    setIsActive(null);
  };

  if (visible) {
    return (
      <View
        style={{
          width: '100%',
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          borderTopColor: headerBorderBottom,
          borderTopWidth: 1,
          backgroundColor: white,
        }}>
        <ScrollView contentContainerStyle={{padding: 15}} horizontal>
          <EditorToolKitMainButton
            type={'MaterialIcons'}
            name={'keyboard'}
            text={'Edit Text'}
            isActive={isActive === 'Edit Text' ? true : false}
            onPress={() => {
              if (isActive === 'Edit Text') {
                setIsActive(null);
              } else {
                setIsActive('Edit Text');
                onEditText();
              }
            }}
          />
          <EditorToolKitMainButton
            type={'MaterialIcons'}
            name={'text-format'}
            text={'Font'}
            isActive={isActive === 'Font' ? true : false}
            onPress={() => {
              if (isActive === 'Font') {
                setIsActive(null);
              } else {
                setIsActive('Font');
              }
            }}
          />
          <EditorToolKitMainButton
            type={'MaterialIcons'}
            name={'open-in-full'}
            text={'Size'}
            isActive={isActive === 'Size' ? true : false}
            onPress={() => {
              if (isActive === 'Size') {
                setIsActive(null);
              } else {
                setIsActive('Size');
              }
            }}
          />
          <EditorToolKitMainButton
            type={'MaterialIcons'}
            name={'color-lens'}
            text={'Color'}
            isActive={isActive === 'Color' ? true : false}
            onPress={() => {
              if (isActive === 'Color') {
                setIsActive(null);
              } else {
                setIsActive('Color');
              }
            }}
          />
          <EditorToolKitMainButton
            type={'MaterialIcons'}
            name={'format-align-center'}
            text={'Align'}
            isActive={isActive === 'Align' ? true : false}
            onPress={() => {
              if (isActive === 'Align') {
                setIsActive(null);
              } else {
                setIsActive('Align');
              }
            }}
          />
          <EditorToolKitMainButton
            type={'Feather'}
            name={'x'}
            text={'Close'}
            isActive={true}
            onPress={() => {
              try {
                onClose();
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
          {/* <EditorToolKitXButton onPress={onClose} /> */}
        </ScrollView>

        {/* Toolkit for Font Size */}
        {isActive === 'Size' ? (
          <ToolKitFontSize
            value={listData[index]?.fontSize}
            onChangeFontSize={e => onChangeFontSize(e)}
          />
        ) : null}

        {/* Toolit for text align */}
        {isActive === 'Align' ? (
          <ToolKitFontAlign
            value={listData[index]?.textAlign}
            onChangeFontAlign={e => onChangeFontAlign(e)}
          />
        ) : null}

        {/* ToolKit Color Picker */}
        {isActive === 'Color' ? (
          <ToolKitFontColor
            value={listData[index]?.color}
            onChangeFontColor={e => onChangeFontColor(e)}
          />
        ) : null}

        {/* Toolkit for Font Style */}
        {isActive === 'Font' ? (
          <ToolKitFontStyle
            value={listData[index]?.fontFamily}
            onChangeFontStyle={e => onChangeFontStyle(e)}
          />
        ) : null}
      </View>
    );
  } else {
    return null;
  }
};

export const CardFontStyle = props => {
  const {item, index, value, onChangeFontStyle} = props;
  return (
    <TouchableOpacity
      onPress={() => onChangeFontStyle(item?.scriptName)}
      style={{
        width: '100%',
        padding: 15,
        paddingTop: 8,
        paddingBottom: 8,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          color: value === item?.scriptName ? mainRed : black,
          fontFamily: item?.scriptName,
          fontSize: RFPercentage(1.6),
        }}>
        {item?.name}
      </Text>
    </TouchableOpacity>
  );
};

export const ToolKitFontStyle = props => {
  const {value, onChangeFontStyle} = props;

  const fontListRef = useRef();
  const [getIndexSelectedFont, setGetIndexSelectedFont] = useState(null);

  useEffect(() => {
    scrollToSelected();
    if (getIndexSelectedFont) {
      scrollTo();
    }
  }, [getIndexSelectedFont]);

  const scrollTo = useCallback(() => {
    try {
      if (getIndexSelectedFont) {
        // go scroll baby
        fontListRef?.current?.scrollToIndex({
          animated: true,
          index: getIndexSelectedFont,
          viewPosition: 0,
        });
      } else {
        //  Do nothing Scroll
      }
    } catch (error) {
      console.log('Error Scroll To Index: ', error);
    }
  }, [getIndexSelectedFont]);

  const scrollToSelected = async () => {
    if (value === 'Arial') {
      // Do Nothing
      setGetIndexSelectedFont(false);
    } else {
      const getIndex = _.findIndex(arrayCanvasFont, {scriptName: value});
      console.log('List Selected Font Index: ', getIndex);
      if (getIndex) {
        setGetIndexSelectedFont(getIndex);
      } else {
        setGetIndexSelectedFont(false);
      }
    }
  };

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [value],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <CardFontStyle
          value={value}
          item={item}
          index={index}
          onChangeFontStyle={e => onChangeFontStyle(e)}
        />
      );
    },
    [value],
  );

  const getItemLayout = useCallback(
    (item, index) => {
      return {
        length: arrayCanvasFont?.length,
        offset: width * 0.062 * index,
        index,
      };
    },
    [value],
  );

  return (
    <View style={{width: '100%', height: width * 0.3, borderWidth: 0}}>
      <FlatList
        onScrollToIndexFailed={info => {
          const wait = new Promise(resolve => setTimeout(resolve, 500));
          wait.then(() => {
            fontListRef.current?.scrollToIndex({
              animated: true,
              index: getIndexSelectedFont,
              viewPosition: 0,
            });
          });
        }}
        getItemLayout={getItemLayout}
        ref={fontListRef}
        contentContainerStyle={{
          paddingTop: 10,
          paddingLeft: 10,
          borderTopWidth: 1,
          borderTopColor: headerBorderBottom,
        }}
        data={arrayCanvasFont}
        extraData={arrayCanvasFont}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </View>
  );
};

export const ToolKitFontColor = props => {
  const {value, onChangeFontColor} = props;
  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [value],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <ColorPickerCard
          value={value}
          item={item}
          index={index}
          onChangeFontColor={color => onChangeFontColor(color)}
        />
      );
    },
    [value],
  );

  return (
    <View style={{width: '100%', height: width * 0.3}}>
      <View
        style={{
          flex: 1,
          borderTopColor: headerBorderBottom,
          borderTopWidth: 1,
        }}>
        <FlatList
          contentContainerStyle={{
            padding: 45,
            paddingTop: 15,
            paddingBottom: 15,
          }}
          scrollEnabled={true}
          numColumns={6}
          data={FontColorPicker}
          extraData={FontColorPicker}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

export const ColorPickerCard = props => {
  const {value, item, index, onChangeFontColor} = props;

  return (
    <View
      style={{
        flex: 1 / 6,
        borderWidth: 0,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={
          value === item?.hex
            ? {
                width: width * 0.05,
                height: width * 0.05,
                borderRadius: (width * 0.05) / 2,
                borderWidth: 1,
                padding: 5,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }
            : {
                width: width * 0.05,
                height: width * 0.05,
                borderRadius: (width * 0.05) / 2,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }
        }>
        <TouchableOpacity
          onPress={() => onChangeFontColor(item?.hex)}
          style={
            value === item?.hex
              ? {
                  borderRadius: (width * 0.05) / 2,
                  position: 'absolute',
                  top: 2,
                  right: 2,
                  bottom: 2,
                  left: 2,
                  backgroundColor: item?.hex,
                }
              : {
                  borderRadius: (width * 0.05) / 2,
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  backgroundColor: item?.hex,
                }
          }
        />
      </View>
    </View>
  );
};

export const ToolKitFontAlign = props => {
  /**
   * left
   * center
   * right
   */
  const {value, onChangeFontAlign} = props;

  const onChange = textAlign => {
    try {
      onChangeFontAlign(textAlign);
    } catch (error) {
      // Error
      console.log('ToolKitFontAlign Error: ', error);
    }
  };

  return (
    <View
      style={{
        borderTopWidth: 1,
        borderTopColor: headerBorderBottom,
        width: '100%',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <TouchableOpacity
        onPress={() => onChange('left')}
        style={{
          marginHorizontal: 5,
          padding: 5,
          borderWidth: 1,
          borderColor: value === 'left' ? mainRed : black,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="align-left"
          style={{
            fontSize: RFPercentage(2.2),
            color: value === 'left' ? mainRed : black,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onChange('center')}
        style={{
          marginHorizontal: 5,
          padding: 5,
          borderWidth: 1,
          borderColor: value === 'center' ? mainRed : black,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="align-center"
          style={{
            fontSize: RFPercentage(2.2),
            color: value === 'center' ? mainRed : black,
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onChange('right')}
        style={{
          marginHorizontal: 5,
          padding: 5,
          borderWidth: 1,
          borderColor: value === 'right' ? mainRed : black,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="align-right"
          style={{
            fontSize: RFPercentage(2.2),
            color: value === 'right' ? mainRed : black,
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export const ToolKitFontSize = props => {
  const {value, onChangeFontSize} = props;

  const onChange = e => {
    try {
      onChangeFontSize(parseFloat(e?.toFixed(2)));
    } catch (error) {
      console.log('Error onValueChange: ', error);
    }
  };

  return (
    <View
      style={{
        padding: 15,
        paddingTop: 10,
        paddingBottom: 10,
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: headerBorderBottom,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          flex: 0.1,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Text
          style={{
            fontSize: RFPercentage(1.5),
            color: greyLine,
            fontFamily: medium,
          }}>
          A
        </Text>
      </View>
      <View style={{flex: 1}}>
        <Slider
          onSlidingComplete={onChange}
          value={value ? value : 1.5}
          style={{width: '100%', height: 40}}
          minimumValue={1.5}
          maximumValue={4}
          minimumTrackTintColor={mainRed}
          maximumTrackTintColor={greyLine}
        />
      </View>
      <View
        style={{
          flex: 0.1,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Text
          style={{
            fontSize: RFPercentage(3),
            color: greyLine,
            fontFamily: medium,
          }}>
          A
        </Text>
      </View>
    </View>
  );
};

export const AddContact = props => {
  const {data, isContactOpened, openContact, openEditContact} = props;
  return (
    <View
      style={{
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 15,
      }}>
      {data?.length === 0 ? (
        <RedBarContact openContact={openContact} />
      ) : (
        <GreenBarContact
          data={data}
          openEditContact={() => openEditContact()}
          AddMoreContact={() => openContact()}
        />
      )}
    </View>
  );
};

export const RedBarContact = props => {
  const {openContact} = props;
  return (
    <TouchableOpacity
      onPress={() => openContact()}
      style={{
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
        backgroundColor: '#FFEDED',
        width: '100%',
        flexDirection: 'row',
        padding: 10,
        borderRadius: 5,
      }}>
      <View style={{flex: 1}}>
        <Text
          style={{
            fontFamily: book,
            letterSpacing: 0.3,
            lineHeight: 20,
            fontSize: RFPercentage(1.7),
            color: mainRed,
          }}>
          It’s seems you haven’t add contact list to send your invitation card !
        </Text>
      </View>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <TouchableOpacity style={{padding: 5}}>
          <Icon
            type="Feather"
            name="chevron-right"
            style={{color: mainRed, fontSize: RFPercentage(2)}}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export const GreenBarContact = props => {
  const {data, openEditContact, AddMoreContact} = props;

  if (data?.length) {
    const invalidContact = data?.filter(element => {
      return element?.valid === false;
    });

    return (
      <View
        style={{
          backgroundColor: '#F4FAF1',
          width: '100%',
          flexDirection: 'row',
          padding: 10,
          borderRadius: 5,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,
          elevation: 2,
        }}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Text
            style={{
              fontFamily: book,
              letterSpacing: 0.3,
              lineHeight: 20,
              fontSize: RFPercentage(1.7),
              color: mainGreen,
            }}>
            Selected Contact(s):{' '}
            <Text style={{fontFamily: medium}}>{data?.length}</Text>
          </Text>
          {invalidContact?.length === 0 ? null : (
            <Text
              style={{
                fontFamily: book,
                letterSpacing: 0.3,
                lineHeight: 20,
                fontSize: RFPercentage(1.7),
                color: mainRed,
              }}>
              Invalid Format Contact(s):{' '}
              <Text style={{fontFamily: medium}}>
                {invalidContact ? invalidContact?.length : ''}
              </Text>
            </Text>
          )}
        </View>
        <View
          style={{
            flex: 0.2,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => openEditContact()}
            style={{padding: 5, marginHorizontal: 10}}>
            <Icon
              type="Feather"
              name="edit"
              style={{color: mainGreen, fontSize: RFPercentage(2)}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => AddMoreContact()}
            style={{padding: 5, marginHorizontal: 10}}>
            <Icon
              type="Feather"
              name="plus"
              style={{color: mainGreen, fontSize: RFPercentage(2)}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    return null;
  }
};

export const ToolKit = props => {
  const {
    addNewText,
    visible,
    isContactOpened,
    openContact,
    contactList,
    openEditContact,
    onOpenSchedule,
  } = props;
  if (visible) {
    return null;
  }
  return (
    <View
      style={{
        width: '100%',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: white,
        paddingTop: 10,
      }}>
      {/* Contact Button Red or Green Bar */}
      <AddContact
        openEditContact={openEditContact}
        data={contactList}
        visible={isContactOpened}
        openContact={openContact}
      />
      <View style={{width: '100%', flexDirection: 'row'}}>
        <View
          style={{
            flex: 1,
            borderRightColor: headerBorderBottom,
            borderRightWidth: 0.5,
            padding: 7,
          }}>
          <TouchableOpacity
            onPress={addNewText}
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 5,
              paddingRight: 5,
            }}>
            <Icon
              type="Feather"
              name="type"
              style={{fontSize: RFPercentage(3), color: mainRed}}
            />
            <Text
              style={{
                marginLeft: 10,
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(2),
              }}>
              Add Text
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            borderLeftColor: headerBorderBottom,
            borderLeftWidth: 0.5,
            padding: 7,
          }}>
          <TouchableOpacity
            onPress={onOpenSchedule}
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 5,
              paddingRight: 5,
            }}>
            <Icon
              type="Feather"
              name="book-open"
              style={{fontSize: RFPercentage(3), color: mainRed}}
            />
            <Text
              style={{
                marginLeft: 10,
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(2),
              }}>
              Schedule
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export const ButtonFlip = props => {
  return (
    <View
      style={{
        marginTop: 15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <TouchableOpacity
        style={{
          borderRadius: 5,
          padding: 10,
          paddingLeft: 25,
          paddingRight: 25,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: mainGreen,
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: white,
            fontSize: RFPercentage(1.8),
          }}>
          FLIP
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(Editor);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
