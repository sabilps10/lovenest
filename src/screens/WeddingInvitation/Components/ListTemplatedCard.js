import React, {useCallback, useState, useEffect} from 'react';
import {
  Text,
  RefreshControl,
  ActivityIndicator,
  FlatList,
  View,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Modal,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Icon} from 'native-base';
import {connect} from 'react-redux';

const {mainGreen, greyLine, white, black, headerBorderBottom} = Colors;
const {medium} = FontType;
const {width, height} = Dimensions.get('window');

// Component
import TemplateCard from './TemplateCard';

import EmptyScreen from './EmptyScreen';

const ListTemplateCard = props => {
  console.log('LIST TEMPLATE CARD PROPS: ', props);
  const {
    list,
    onPress,
    setIsLoadMore,
    isLoadMore,
    onLoadMore,
    refreshing,
    isLoading,
    isError,
    onRefresh,
    isLogin,
    toggleTemplate,
    toggleDraft,
    positionYBottomNav,
  } = props;

  useEffect(() => {
    const subs = onChangeOpacity();

    return subs;
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const keyExt = useCallback(
    item => {
      return `${item.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TemplateCard onPress={i => onPress(i)} item={item} index={index} />
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <View
        style={{
          paddingTop: 15,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={mainGreen} />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: white,
        }}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View
            style={{
              flex: 1,
              height: height * 0.9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <EmptyScreen tabName="create" />
          </View>
        </ScrollView>
      </View>
    );
  } else {
    if (list?.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
          }}>
          <EmptyScreen tabName="create" />
        </View>
      );
    } else {
      return (
        <FlatList
          ListFooterComponent={() => {
            if (list?.length === 0) {
              return null;
            } else {
              return (
                <View
                  style={{
                    opacity: isLoadMore ? 1 : 0,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    paddingBottom: 15,
                  }}>
                  <ActivityIndicator
                    size="small"
                    color={mainGreen}
                    style={{marginHorizontal: 5}}
                  />
                  <Text
                    style={{
                      fontStyle: 'italic',
                      fontSize: 12,
                      color: greyLine,
                      textAlign: 'center',
                    }}>
                    Loading more...
                  </Text>
                </View>
              );
            }
          }}
          onEndReachedThreshold={0.01}
          // onEndReached={() => setIsLoadMore(false)}
          // onMomentumScrollBegin={() => setIsLoadMore(false)}
          onMomentumScrollEnd={async () => {
            try {
              await onLoadMore();
            } catch (error) {
              await setIsLoadMore(false);
            }
          }}
          ListHeaderComponent={() => {
            if (list?.length === 0) {
              return null;
            } else {
              return (
                <View
                  style={{
                    width: '100%',
                    padding: 10,
                    paddingTop: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <View style={{flex: 0.3, borderWidth: 0}} />
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: RFPercentage(1.6),
                        fontFamily: medium,
                        color: greyLine,
                      }}>
                      Choose Your Template
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 0.3,
                      borderWidth: 0,
                      justifyContent: 'center',
                      alignItems: 'flex-end',
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={toggleDraft}
                      style={{padding: 5}}>
                      <Icon
                        type="Feather"
                        name="menu"
                        style={{fontSize: 25, color: greyLine}}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              );
            }
          }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          numColumns={2}
          contentContainerStyle={{
            paddingBottom: 120,
          }}
          columnWrapperStyle={{
            paddingLeft: 10,
            paddingRight: 10,
          }}
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={renderItem}
        />
      );
    }
  }
};


const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(ListTemplateCard);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
