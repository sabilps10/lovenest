import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  ActivityIndicator,
  Modal,
  TouchableOpacity,
  Platform,
  Alert,
  RefreshControl,
  TextInput,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Left, Body, Right, Icon, Button} from 'native-base';
import _ from 'lodash';

const {
  white,
  black,
  greyLine,
  overlayDim,
  mainGreen,
  mainRed,
  headerBorderBottom,
} = Colors;
const {medium, book} = FontType;

import Contacts from 'react-native-contacts';

const ListContactFromPhone = props => {
  console.log('ListContactFromPhone Props: ', props);
  const {visible, openOrClosed, onSave, selectedContactList} = props;

  const flatlistRef = useRef(null);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);
  const [resultList, setResultList] = useState([]);

  const [isLoadingSaving, setIsLoadingSaving] = useState(false);

  const [search, setSearch] = useState('');
  const [isLoadingSearch, setIsLoadingSearch] = useState(false);
  const [searchResult, setSearchResult] = useState([]);

  useEffect(() => {
    if (visible) {
      fetch();
    }
  }, [visible, selectedContactList, onUpdateList]);

  const fetch = async () => {
    try {
      await setList([...selectedContactList]);
      await setIsError(false);
      await setIsLoading(false);
    } catch (error) {
      console.log('Failed get contact list from parent: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const save = async () => {
    try {
      await setIsLoadingSaving(true);
      setTimeout(async () => {
        await setIsLoadingSaving(false);
        await props?.onSave([...list]);
      }, 2000);
    } catch (error) {
      await setIsLoadingSaving(false);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [list],
  );

  const selecting = async index => {
    try {
      let oldList = list;
      oldList[index].selected = !oldList[index].selected;

      await setList([...oldList]);
      await setResultList([]);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onDelete = index => {
    let oldDataList = list;
    oldDataList?.splice(index, 1);
    setList([...oldDataList]);
  };

  const onUpdateList = async (idx, name, phoneNumber) => {
    console.log('onUpdateList: ', {idx, name, phoneNumber});
    const validationPhoneNumber = phoneNumber?.[0] === '+' ? true : false;
    const validateLengthPhoneNumber = phoneNumber?.length >= 11 ? true : false;
    if (
      name === '' ||
      name === ' ' ||
      !validationPhoneNumber ||
      !validateLengthPhoneNumber
    ) {
      // invalid format
      let oldData = list;
      oldData[idx].name = name;
      oldData[idx].phoneNo = phoneNumber;
      oldData[idx].valid = false;
      console.log('OLD Name: ', oldData[idx].name);
      console.log('New Name: ', name);
      await setList([...oldData]);
    } else {
      //  Valid
      let oldData = list;
      oldData[idx].name = name;
      oldData[idx].phoneNo = phoneNumber.replace(/\s/g, '');
      oldData[idx].valid = true;
      oldData[idx].selected = false;
      console.log('OLD Name: ', oldData[idx].name);
      console.log('New Name: ', name);
      await setList([...oldData]);
    }
  };

  const renderItem = useCallback(
    ({item, index}) => {
      if (item) {
        return (
          <CardList
            onUpdateList={onUpdateList}
            selecting={idx => selecting(idx)}
            onDelete={idx => onDelete(idx)}
            item={item}
            index={index}
          />
        );
      } else {
        return null;
      }
    },
    [list],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <KeyboardAvoidingView
        behavior={Platform?.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: overlayDim}}>
          <TouchableOpacity
            onPress={openOrClosed}
            style={{flex: 0.2, backgroundColor: overlayDim}}
          />
          <View style={{flex: 1, backgroundColor: white}}>
            <Headers
              isLoadingSaving={isLoadingSaving}
              close={openOrClosed}
              onSave={save}
            />
            {resultList?.length === 0 ? (
              <FlatList
                ref={flatlistRef}
                disableVirtualization
                legacyImplementation
                initialNumToRender={10}
                refreshControl={
                  <RefreshControl
                    refreshing={isLoading}
                    onRefresh={() => fetch()}
                  />
                }
                style={{bottom: 17}}
                contentContainerStyle={{paddingBottom: 15}}
                ListHeaderComponent={() => {
                  return (
                    <Card
                      transparent
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        marginTop: 15,
                        marginBottom: 15,
                      }}>
                      <CardItem
                        style={{
                          width: '100%',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            left: 10,
                            fontFamily: medium,
                            fontSize: RFPercentage(1.7),
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          Total Selected: {list?.length}
                        </Text>
                      </CardItem>
                    </Card>
                  );
                }}
                data={list}
                extraData={list}
                keyExtractor={keyExt}
                renderItem={renderItem}
              />
            ) : null}
          </View>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export const CardList = props => {
  const {item, index, selecting, onDelete, onUpdateList} = props;

  const [contactName, setContactName] = useState(item?.name);
  const [number, setNumber] = useState(item?.phoneNo);

  if (item) {
    return (
      <Card transparent>
        <CardItem
          button
          onPress={() => selecting(index)}
          style={{width: '100%', paddingTop: 0}}>
          <View style={{flex: 0.1}}>
            <View
              style={{
                width: 35,
                height: 35,
                borderRadius: 35 / 2,
                backgroundColor: greyLine,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="user"
                style={{
                  left: 10,
                  fontSize: RFPercentage(1.8),
                  color: white,
                }}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: item?.valid ? black : mainRed,
                letterSpacing: 0.3,
              }}>
              {item?.name === '' || item?.name === ' ' ? 'No-Name' : item?.name}
            </Text>
            <Text
              style={{
                marginTop: 5,
                fontFamily: medium,
                fontStyle: 'italic',
                fontSize: RFPercentage(1.4),
                color: item?.valid ? black : mainRed,
                letterSpacing: 0.3,
              }}>
              {item?.phoneNo === '' || !item?.phoneNo
                ? 'No-Phone-Number'
                : item?.phoneNo}
            </Text>
            {item?.valid ? null : (
              <View
                style={{
                  top: 7,
                  marginBottom: 10,
                  alignSelf: 'flex-start',
                  padding: 5,
                  backgroundColor: mainRed,
                  borderRadius: 25,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1),
                    color: white,
                  }}>
                  {item?.name === '' || item?.name === ' '
                    ? "Contact name can't be empty !"
                    : item?.phoneNo?.[0] !== '+'
                    ? 'Invalid Format, it must +(country code)(phone number) !'
                    : item?.phoneNo?.length < 11
                    ? 'Phone number length minimum must have 11 digits'
                    : ''}
                </Text>
              </View>
            )}
          </View>
          <View style={{flex: 0.1}}>
            {item?.selected ? (
              <TouchableOpacity
                onPress={() => onDelete(index)}
                style={{padding: 10}}>
                <View
                  style={{
                    right: 15,
                    width: 35,
                    height: 35,
                    borderRadius: 35 / 2,
                    backgroundColor: white,
                    borderWidth: 0,
                    borderColor: 'grey',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="trash-2"
                    style={{
                      left: 9,
                      fontSize: RFPercentage(2),
                      color: mainRed,
                    }}
                  />
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => selecting(index)}
                style={{padding: 10}}>
                <View
                  style={{
                    right: 15,
                    width: 35,
                    height: 35,
                    borderRadius: 35 / 2,
                    backgroundColor: white,
                    borderWidth: 0,
                    borderColor: 'grey',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="chevron-down"
                    style={{
                      left: 9,
                      fontSize: RFPercentage(2),
                      color: greyLine,
                    }}
                  />
                </View>
              </TouchableOpacity>
            )}
          </View>
        </CardItem>
        {item?.selected ? (
          <Card
            transparent
            style={{paddingLeft: 15, paddingRight: 15, marginBottom: 15}}>
            <CardItem
              style={{
                borderWidth: 1,
                borderRadius: 5,
                borderColor: headerBorderBottom,
              }}>
              <View style={{flex: 1}}>
                <View style={{marginVertical: 5}}>
                  <Text
                    style={{
                      textDecorationLine: 'underline',
                      fontFamily: medium,
                      fontSize: RFPercentage(1.5),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    Edit Contact
                  </Text>
                </View>
                <TextInput
                  value={contactName}
                  placeholder={item?.contactName}
                  onChangeText={e => {
                    setContactName(e);
                  }}
                  style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                    borderRadius: 5,
                    backgroundColor: headerBorderBottom,
                    marginVertical: 5,
                  }}
                />
                <TextInput
                  keyboardType={
                    Platform?.OS === 'android' ? 'phone-pad' : 'phone-pad'
                  }
                  value={number}
                  placeholder={item?.phoneNumber}
                  onChangeText={e => {
                    setNumber(e);
                  }}
                  style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                    borderRadius: 5,
                    backgroundColor: headerBorderBottom,
                    marginVertical: 5,
                  }}
                />
              </View>
              <View
                style={{
                  flex: 0.2,
                  borderWidth: 0,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    //
                    onUpdateList(index, contactName, number);
                  }}
                  style={{padding: 10, left: 25}}>
                  <View
                    style={{
                      right: 15,
                      width: 35,
                      height: 35,
                      borderRadius: 35 / 2,
                      backgroundColor: white,
                      borderWidth: 0,
                      borderColor: 'grey',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="edit"
                      style={{
                        left: 9,
                        fontSize: RFPercentage(2),
                        color: mainGreen,
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </CardItem>
          </Card>
        ) : null}
        <CardItem style={{paddingTop: 0}}>
          <View
            style={{
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              height: 1,
            }}
          />
        </CardItem>
      </Card>
    );
  } else {
    return null;
  }
};

export const Headers = props => {
  const {close, onSave, isLoadingSaving} = props;
  return (
    <Card
      style={{
        marginTop: 0,
        bottom: 15,
        marginLeft: 0,
        marginRight: 0,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
      }}>
      <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
        <Left style={{flex: 0.2}}>
          <Button
            onPress={close}
            style={{
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: white,
              alignSelf: 'flex-start',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </Left>
        <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            Phone Contact(s)
          </Text>
        </Body>
        <Right style={{flex: 0.2}}>
          <Button
            disabled={isLoadingSaving ? true : false}
            onPress={onSave}
            style={{
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: white,
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            {isLoadingSaving ? (
              <ActivityIndicator size="small" color={mainGreen} />
            ) : (
              <Icon
                type="Feather"
                name="save"
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 24,
                  color: black,
                }}
              />
            )}
          </Button>
        </Right>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(ListContactFromPhone);

export default props => <Wrapper {...props} />;
