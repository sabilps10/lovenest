import React from 'react';
import {Text, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';
import {Card, CardItem} from 'native-base';

const {width, height} = Dimensions.get('window');
const {white, black, greyLine} = Colors;
const {book, medium} = FontType;
const {charCreateInvitation, charMyInvitation, charMyRSVP} = charImage;

const EmptyScreen = props => {
  const {tabName} = props;
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Card transparent style={{bottom: 50}}>
        <CardItem
          cardBody
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <Image
            source={
              tabName === 'create'
                ? charCreateInvitation
                : tabName === 'myinvitation'
                ? charMyInvitation
                : charMyRSVP
            }
            style={{width: width, height: width * 0.8}}
            resizeMode="contain"
          />
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.2),
              color: black,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            {tabName === 'create'
              ? 'No Template Yet.'
              : tabName === 'myinvitation'
              ? 'You don’t have Invitation'
              : "You don't have RSVP"}
          </Text>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
              textAlign: 'center',
              lineHeight: 20,
            }}>
            {tabName === 'create'
              ? 'we are preparing the template, please stay tune'
              : tabName === 'myinvitation'
              ? 'This section contains your invitation, with a required form to fill out'
              : 'This section contains your RSVP, with analictycs data and lists'}
          </Text>
        </CardItem>
      </Card>
    </View>
  );
};

const Wrapper = compose(withApollo)(EmptyScreen);

export default props => <Wrapper {...props} />;
