import React, {useEffect, useState, useCallback} from 'react';
import {
  Text,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Dimensions,
  Image,
  View,
  RefreshControl,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Icon} from 'native-base';

// Query
import GET_DRAFT from '../../../graphql/queries/getCustomerInvitationDraft';
import DELETE_DRAFT from '../../../graphql/mutations/deleteCustomerDraftTemplate';

// Components
import TemplateCard from './TemplateDraftCard';
import DraftDetailScreen from './DraftDetail';

const {
  white,
  black,
  mainRed,
  mainGreen,
  headerBorderBottom,
  greyLine,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

const ListDraft = props => {
  const {client, navigation, toggleTemplate, toggleDraft} = props;

  const [list, setList] = useState([]);
  const [totalData, setTotalData] = useState(0);
  const [itemDisplayed, setItemDisplayed] = useState(4);
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [indexSelected, setIndexSelected] = useState(null);

  const [showModalDeleteDraft, setShowModalDeleteDraft] = useState(false);

  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalError, setModalError] = useState(false);

  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const [selectedIdForDelete, setSelectedIdForDelete] = useState(null);

  useEffect(() => {
    fetch();
  }, [indexSelected]);

  const onLoadMore = async () => {
    try {
      if (list?.length >= totalData) {
        // Do nothing
        await setTimeout(async () => {
          await setIsLoadMore(false);
        }, 500);
      } else {
        await setIsLoadMore(true);
        await setItemDisplayed(itemDisplayed + 10);
        await client
          .query({
            query: GET_DRAFT,
            variables: {
              itemDisplayed: parseInt(itemDisplayed, 10),
              pageNumber: parseInt(pageNumber, 10),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async res => {
            console.log('Res Draft: ', res);
            const {data, errors} = res;
            const {getCustomerInvitationDraft} = data;
            const {
              data: listDraft,
              error,
              totalCount,
            } = getCustomerInvitationDraft;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              } else {
                await setList([...listDraft]);
                await setTotalData(totalCount);
                await setIsError(false);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              }
            }
          })
          .catch(async error => {
            console.log('Error 1: ', error);
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          });
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await setItemDisplayed(10);
      await setPageNumber(1);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_DRAFT,
          variables: {
            itemDisplayed: parseInt(itemDisplayed, 10),
            pageNumber: parseInt(pageNumber, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Draft: ', res);
          const {data, errors} = res;
          const {getCustomerInvitationDraft} = data;
          const {
            data: listDrafts,
            error,
            totalCount,
          } = getCustomerInvitationDraft;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              await setList([...listDrafts]);
              await setTotalData(totalCount);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error 1: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
        });
    } catch (error) {
      console.log('Error 2: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const onPress = async i => {
    try {
      console.log('I On Press: ', i);
      await setIndexSelected(i);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      console.log('ITEM MA BRU: ', item);
      return (
        <TemplateCard
          openModalDelete={async id => {
            try {
              await setSelectedIdForDelete(id);
              await setShowModalDeleteDraft(!showModalDeleteDraft);
            } catch (error) {
              await selectedIdForDelete(null);
              await setShowModalDeleteDraft(false);
            }
          }}
          onPress={i => onPress(i)}
          item={item}
          index={index}
        />
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 15,
        }}>
        <ActivityIndicator size="large" color={mainGreen} />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          flex: 1,
        }}>
        {/* Header */}
        <View
          style={{
            // borderWidth: 1,
            width: '100%',
            padding: 10,
            paddingTop: 15,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <View style={{flex: 0.2, borderWidth: 0}}>
            <TouchableOpacity
              onPress={toggleTemplate}
              style={{
                width: 35,
                height: 35,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="arrow-left"
                style={{fontSize: RFPercentage(2.5), color: black}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              List Draft
            </Text>
          </View>
          <View style={{flex: 0.2, borderWidth: 0}} />
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={onRefresh}
            style={{
              bottom: 40,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 10,
              paddingLeft: 15,
              paddingRight: 15,
              borderRadius: 4,
              backgroundColor: mainGreen,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    if (indexSelected || indexSelected !== null) {
      return (
        <DraftDetailScreen
          navigation={props?.navigation}
          client={props?.client}
          templateData={list[indexSelected]}
          goBack={async () => {
            try {
              await setIndexSelected(null);
            } catch (error) {}
          }}
        />
      );
    } else {
      return (
        <View style={{flex: 1}}>
          <ModalWarningDelete
            isLoadingDelete={isLoadingDelete}
            visible={showModalDeleteDraft}
            success={modalSuccess}
            error={modalError}
            submit={async () => {
              try {
                console.log('MASUK ON SUBMIT DELETE DRAFT', {
                  selectedIdForDelete,
                });
                if (selectedIdForDelete) {
                  await setIsLoadingDelete(true);
                  await client
                    .mutate({
                      mutation: DELETE_DRAFT,
                      variables: {
                        draftId: parseInt(selectedIdForDelete, 10),
                      },
                    })
                    .then(async res => {
                      const {data, errors} = res;
                      const {deleteCustomerDraftTemplate} = data;
                      const {error} = deleteCustomerDraftTemplate;

                      if (errors) {
                        throw new Error('Failed to delete');
                      } else {
                        if (error) {
                          throw new Error('Failed to delete');
                        } else {
                          await setModalSuccess(true);
                          await setTimeout(async () => {
                            await setSelectedIdForDelete(null);
                            await setModalError(false);
                            await setModalSuccess(false);
                            await setIsLoadingDelete(false);
                            await setShowModalDeleteDraft(false);
                            await onRefresh();
                          }, 1500);
                        }
                      }
                    })
                    .catch(error => {
                      throw error;
                    });
                } else {
                  await setModalError(true);
                  await setTimeout(async () => {
                    await setSelectedIdForDelete(null);
                    await setModalError(false);
                    await setModalSuccess(false);
                    await setIsLoadingDelete(false);
                    await setShowModalDeleteDraft(false);
                  }, 1500);
                }
              } catch (error) {
                console.log('Error Delete Draft: ', error);
                await setModalError(true);
                await setTimeout(async () => {
                  await setSelectedIdForDelete(null);
                  await setModalError(false);
                  await setModalSuccess(false);
                  await setIsLoadingDelete(false);
                  await setShowModalDeleteDraft(false);
                }, 1500);
              }
            }}
            onClose={async () => {
              try {
                await setSelectedIdForDelete(null);
                await setShowModalDeleteDraft(false);
              } catch (error) {
                await setSelectedIdForDelete(null);
                await setShowModalDeleteDraft(false);
              }
            }}
          />
          <FlatList
            ListEmptyComponent={() => {
              return (
                <View
                  style={{
                    height: width * 0.99,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      top: 60,
                      fontFamily: medium,
                      fontSize: RFPercentage(2),
                      color: greyLine,
                      letterSpacing: 0.3,
                      textAlign: 'center',
                    }}>
                    You don't have draft yet
                  </Text>
                </View>
              );
            }}
            ListFooterComponent={() => {
              if (isLoadMore) {
                return (
                  <View
                    style={{
                      padding: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <ActivityIndicator size="small" color={mainGreen} />
                  </View>
                );
              } else {
                return null;
              }
            }}
            onEndReachedThreshold={0.01}
            onEndReached={async () => {
              try {
                await onLoadMore();
              } catch (error) {
                await setIsLoadMore(false);
              }
            }}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            ListHeaderComponent={() => {
              return (
                <View
                  style={{
                    // borderWidth: 1,
                    width: '100%',
                    padding: 10,
                    paddingTop: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <View style={{flex: 0.2, borderWidth: 0}}>
                    <TouchableOpacity
                      onPress={toggleTemplate}
                      style={{
                        width: 35,
                        height: 35,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon
                        type="Feather"
                        name="arrow-left"
                        style={{fontSize: RFPercentage(2.5), color: black}}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: black,
                        fontSize: RFPercentage(1.8),
                        letterSpacing: 0.3,
                        textAlign: 'center',
                      }}>
                      List Draft
                    </Text>
                  </View>
                  <View style={{flex: 0.2, borderWidth: 0}} />
                </View>
              );
            }}
            data={list}
            extraData={list}
            keyExtractor={keyExt}
            renderItem={renderItem}
            numColumns={2}
            contentContainerStyle={{
              paddingBottom: 120,
            }}
            columnWrapperStyle={{
              paddingLeft: 10,
              paddingRight: 10,
            }}
          />
        </View>
      );
    }
  }
};

export const ModalWarningDelete = props => {
  const {visible, modalError, modalSuccess, submit, onClose, isLoadingDelete} =
    props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        <View
          style={{
            padding: 15,
            backgroundColor: white,
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 15,
              paddingLeft: 25,
              paddingRight: 25,
            }}>
            {modalSuccess ? (
              <Text
                style={{
                  fontFamily: medium,
                  color: mainGreen,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Successfully delete!
              </Text>
            ) : modalError ? (
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Failed to delete !
              </Text>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Are you sure want to delete this draft ?
              </Text>
            )}
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              marginTop: 25,
              marginBottom: 10,
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity onPress={onClose}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity
                disabled={isLoadingDelete ? true : false}
                onPress={submit}>
                {isLoadingDelete ? (
                  <ActivityIndicator size="small" color={mainRed} />
                ) : (
                  <Text
                    style={{
                      fontFamily: medium,
                      color: mainRed,
                      fontSize: RFPercentage(1.8),
                    }}>
                    Yes
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ListDraft);

export default props => <Wrapper {...props} />;
