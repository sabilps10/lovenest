import React, {useState, useRef, useEffect, useCallback} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
  TouchableOpacity,
  Animated,
  Modal,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Left,
  Body,
  Right,
  Button,
} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../utils/AsyncstorageDataStructure/index';
import _ from 'lodash';

// Query
import ListSummaryQuery from '../../../graphql/queries/getInvitationSummaryByTemplate';

// Mutation
import DeleteHistory from '../../../graphql/mutations/deleteCustomerHistoryInvitation';

const {asyncToken} = AsyncData;
const {
  white,
  black,
  mainRed,
  mainGreen,
  greyLine,
  headerBorderBottom,
  overlayDim,
} = Colors;
const {book, medium, BaskervilleBold, Baskerville} = FontType;
const {width, height} = Dimensions.get('window');

const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedCard = new Animated.createAnimatedComponent(Card);

import EmptyScreen from './EmptyScreen';
import RNFetchBlob from 'rn-fetch-blob';

// Query
import ExportPDF from '../../../graphql/queries/exportWeddingAttendancePdf';

const ListSentInvitation = props => {
  const {client, onPress} = props;

  const [list, setList] = useState([]);
  const [grandTotalInvitation, setGrandTotalInvitation] = useState(0);
  const [grandtotalAttendees, setGrandTotalAttendees] = useState(0);
  const [totalData, setTotalData] = useState(0);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [itemDisplayed, setItemDispLayed] = useState(4);
  const [pageNumber, setPageNumber] = useState(1);

  const [showModalExport, setShowModalExport] = useState(false);
  const [successDownload, setSuccessDownload] = useState(false);
  const [percentage, setPercentage] = useState(0);
  const [loadingButton, setLoadingButton] = useState(false);

  const [isLogin, setIsLogin] = useState(false);

  const [showModalDeleteDraft, setShowModalDeleteDraft] = useState(false);

  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalError, setModalError] = useState(false);

  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  const [indexSelected, setIndexSelected] = useState(null);

  const [selectedIdForDelete, setSelectedIdForDelete] = useState(null);

  useEffect(() => {
    checkIsLogin();
    fetch();

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = props?.navigation?.addListener('tabPress', () => {
      setTheRefresh();
      fetch();
    });

    return subs;
  }, [indexSelected, isLoadMore, refreshing]);

  const setTheRefresh = async () => {
    await setRefreshing(true);
  };

  const checkToken = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);

        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const isLogins = await checkToken();
      if (isLogins) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLogin(false);
    }
  };

  const onLoadMore = useCallback(async () => {
    await setIsLoadMore(true);
    await setPageNumber(pageNumber + 1);
    await fetch();
  }, [isLoadMore]);

  const onRefresh = useCallback(async () => {
    try {
      await setList([]);
      await setRefreshing(true);
      await setPageNumber(1);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  }, [setList]);

  const fetch = async () => {
    try {
      await client
        .query({
          query: ListSummaryQuery,
          variables: {
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res fethc List Sent Invitation: ', res);
          const {data, errors} = res;
          const {getInvitationSummaryByTemplate} = data;
          const {
            data: listSent,
            totalCount,
            error,
            totalAttendance: totalAttend,
            totalInvitation: totalInvi,
          } = getInvitationSummaryByTemplate;

          if (errors) {
            await setIsError(true);
            await setIsLoadMore(false);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoadMore(false);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              const oldData = list.concat(listSent);
              // const newData = [...new Set(oldData)];
              const filtering = await _.uniqBy(oldData, 'id');
              console.log('filtering >>>>>>>>>>>>> ', filtering);
              await setTotalData(totalCount);
              await setGrandTotalAttendees(totalAttend);
              await setGrandTotalInvitation(totalInvi);
              await setList([...filtering]);
              await setIsError(false);
              await setIsLoadMore(false);
              await setIsLoading(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoadMore(false);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <SentCard
          openModalDelete={async id => {
            try {
              await setSelectedIdForDelete(id);
              await setShowModalDeleteDraft(!showModalDeleteDraft);
            } catch (error) {
              await selectedIdForDelete(null);
              await setShowModalDeleteDraft(false);
            }
          }}
          onPress={id => onPress('SentDetail', id)}
          item={item}
          index={index}
        />
      );
    },
    [list],
  );

  const onDownload = async () => {
    try {
      await setShowModalExport(true);
      if (Platform.OS === 'ios') {
        await iOSDownload();
      } else {
        await androidDownload();
      }
    } catch (error) {
      await setShowModalExport(false);
    }
  };

  const iOSDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length) {
                const fileName = 'InvitationSummary.pdf';
                const paths = await RNFetchBlob.fs.dirs.CacheDir;

                await RNFetchBlob.config({
                  fileCache: true,
                  title: 'summary',
                  appendExt: 'pdf',
                  path: paths + fileName,
                })
                  .fetch(
                    'GET',
                    `${exportData[0]?.dynamicUrl}`,
                    // 'https://ars.els-cdn.com/content/image/1-s2.0-S1525001616328027-mmc2.pdf',
                    {
                      'Content-Type': 'octet-stream',
                    },
                    'base64DataString',
                  )
                  .progress({interval: 10}, async (received, total) => {
                    console.log('progress', received / total);
                    await setPercentage((received / total) * 100);
                  })
                  .then(async res => {
                    console.log('RESS BROOO: ', res.path());
                    // open the document directly

                    if (await res.path()) {
                      await setPercentage(0);
                      await setSuccessDownload(true);
                      await setSuccessDownload(false);
                      await setShowModalExport(false);

                      // await openDoc(res);
                      await RNFetchBlob.fs
                        .writeFile(res.path(), res.data, 'base64')
                        .then(async resp => {
                          console.log(resp);
                          await RNFetchBlob.ios.openDocument(res.path());
                          await setLoadingButton(false);
                        })
                        .catch(r => {
                          throw r;
                        });

                      await setTimeout(async () => {
                        await RNFetchBlob.fs
                          .writeFile(res.path(), res.data, 'base64')
                          .then(async resp => {
                            console.log(resp);
                            await RNFetchBlob.ios.openDocument(res.path());
                            await setLoadingButton(false);
                          })
                          .catch(r => {
                            throw r;
                          });
                      }, 0);
                    }
                  })
                  .catch(errorss => {
                    throw errorss;
                  });
              } else {
                await setPercentage(0);
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setPercentage(0);
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setPercentage(0);
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const androidDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length === 1) {
                if (exportData[0]?.dynamicUrl) {
                  const fileName = `/${exportData[0]?.name}-${moment().format(
                    'DDMMMMYYYY',
                  )}-${moment().format('hhmmssa')}.pdf`;
                  RNFetchBlob.config({
                    fileCache: true,
                    addAndroidDownloads: {
                      useDownloadManager: true,
                      notification: true,
                      mediaScannable: true,
                      title: `${fileName}`,
                      path: `${RNFetchBlob?.fs?.dirs?.DownloadDir}${fileName}`,
                    },
                  })
                    .fetch(
                      'GET',
                      `${exportData[0]?.dynamicUrl}`,
                      {
                        'Content-Type': 'octet-stream',
                      },
                      'base64DataString',
                    )
                    .progress({interval: 10}, async (received, total) => {
                      console.log('progress', received / total);
                      await setPercentage((received / total) * 100);
                    })
                    .then(async res => {
                      console.log('RESS BROOO: ', res);
                      // open the document directly
                      await setSuccessDownload(true);
                      await setTimeout(async () => {
                        await setSuccessDownload(false);
                        await setShowModalExport(false);
                        await setPercentage(0);
                        await setTimeout(async () => {
                          await RNFetchBlob.android.actionViewIntent(
                            res.path(),
                            'application/pdf',
                          );
                          await setLoadingButton(false);
                        }, 1000);
                      }, 1500);
                      // or show options
                      // RNFetchBlob.ios.openDocument(res.path())
                    })
                    .catch(errorss => {
                      throw errorss;
                    });
                } else {
                  await setShowModalExport(false);
                  await setLoadingButton(false);
                }
              } else {
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const listHeader = () => {
    return (
      <Card transparent>
        <CardItem
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={{flex: 0.2}} />
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Summary
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            {isLogin ? (
              loadingButton ? (
                <ActivityIndicator size="small" color={mainGreen} />
              ) : (
                <TouchableOpacity
                  // disabled={showModalExport ? true : false} this not use yet
                  disabled={true}
                  onPress={onDownload}
                  style={{
                    display: 'none',
                    padding: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="download"
                    style={{
                      color: black,
                      fontSize: RFPercentage(2.5),
                      left: 20,
                    }}
                  />
                </TouchableOpacity>
              )
            ) : null}
          </View>
        </CardItem>
        <CardItem>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                color: black,
                fontFamily: book,
                letterSpacing: 0.3,
                textAlign: 'left',
              }}>
              TOTAL INVITATION SENT
            </Text>
          </View>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                color: black,
                fontFamily: medium,
                letterSpacing: 0.3,
                textAlign: 'right',
              }}>
              {grandTotalInvitation === 0 ? 0 : grandTotalInvitation}
            </Text>
          </View>
        </CardItem>
        <CardItem>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                color: black,
                fontFamily: book,
                letterSpacing: 0.3,
                textAlign: 'left',
              }}>
              TOTAL ATTENDANCE
            </Text>
          </View>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                color: black,
                fontFamily: medium,
                letterSpacing: 0.3,
                textAlign: 'right',
              }}>
              {grandtotalAttendees === 0 ? 0 : grandtotalAttendees}
            </Text>
          </View>
        </CardItem>
      </Card>
    );
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Content>
          <View
            style={{
              width: '100%',
              paddingTop: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Content
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View style={{flex: 1, height: height * 0.87}}>
            <EmptyScreen tabName="myrsvp" />
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1}}>
        <ModalDownloader
          visible={showModalExport}
          onClose={async () => {
            await setShowModalExport(false);
          }}
          status={successDownload}
          percentage={percentage}
        />
        <ModalWarningDelete
          isLoadingDelete={isLoadingDelete}
          visible={showModalDeleteDraft}
          success={modalSuccess}
          error={modalError}
          submit={async () => {
            try {
              console.log('MASUK ON SUBMIT DELETE DRAFT', {
                selectedIdForDelete,
              });
              if (selectedIdForDelete) {
                await setIsLoadingDelete(true);
                const variables = {
                  templateId: parseInt(selectedIdForDelete, 10),
                };

                console.log('VARIABLEESSSS: ', variables);
                await client
                  .mutate({
                    mutation: DeleteHistory,
                    variables,
                  })
                  .then(async res => {
                    const {data, errors} = res;
                    const {deleteCustomerHistoryInvitation} = data;
                    const {error} = deleteCustomerHistoryInvitation;

                    if (errors) {
                      throw new Error('Failed to delete');
                    } else {
                      if (error) {
                        throw new Error('Failed to delete');
                      } else {
                        await setModalSuccess(true);
                        await setTimeout(async () => {
                          await setSelectedIdForDelete(null);
                          await setModalError(false);
                          await setModalSuccess(false);
                          await setIsLoadingDelete(false);
                          await setShowModalDeleteDraft(false);
                          await onRefresh();
                        }, 1500);
                      }
                    }
                  })
                  .catch(error => {
                    throw error;
                  });
              } else {
                await setModalError(true);
                await setTimeout(async () => {
                  await setSelectedIdForDelete(null);
                  await setModalError(false);
                  await setModalSuccess(false);
                  await setIsLoadingDelete(false);
                  await setShowModalDeleteDraft(false);
                }, 1500);
              }
            } catch (error) {
              console.log('Error Delete Draft: ', error);
              await setModalError(true);
              await setTimeout(async () => {
                await setSelectedIdForDelete(null);
                await setModalError(false);
                await setModalSuccess(false);
                await setIsLoadingDelete(false);
                await setShowModalDeleteDraft(false);
              }, 1500);
            }
          }}
          onClose={async () => {
            try {
              await setSelectedIdForDelete(null);
              await setShowModalDeleteDraft(false);
            } catch (error) {
              await setSelectedIdForDelete(null);
              await setShowModalDeleteDraft(false);
            }
          }}
        />
        <FlatList
          onEndReachedThreshold={0.3}
          onEndReached={async () => {
            if (list?.length < totalData) {
              console.log('MASUKKK BRO');
              await setIsLoadMore(true);
            } else {
              console.log('MASUKKK WUCAU');
              await setIsLoadMore(false);
            }
          }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                }}>
                {isLoadMore ? (
                  <ActivityIndicator size="small" color={mainGreen} />
                ) : null}
              </View>
            );
          }}
          ListHeaderComponent={listHeader}
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={renderItem}
          numColumns={2}
          contentContainerStyle={{
            paddingBottom: 120,
          }}
          columnWrapperStyle={{
            paddingLeft: 10,
            paddingRight: 10,
          }}
        />
      </View>
    );
  }
};

export const ModalWarningDelete = props => {
  const {visible, modalError, modalSuccess, submit, onClose, isLoadingDelete} =
    props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        <View
          style={{
            padding: 15,
            backgroundColor: white,
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 15,
              paddingLeft: 25,
              paddingRight: 25,
            }}>
            {modalSuccess ? (
              <Text
                style={{
                  fontFamily: medium,
                  color: mainGreen,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Successfully delete!
              </Text>
            ) : modalError ? (
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Failed to delete !
              </Text>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                  lineHeight: 21,
                }}>
                Are you sure want to delete this history ?
              </Text>
            )}
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              marginTop: 25,
              marginBottom: 10,
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity onPress={onClose}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity
                disabled={isLoadingDelete ? true : false}
                onPress={submit}>
                {isLoadingDelete ? (
                  <ActivityIndicator size="small" color={mainRed} />
                ) : (
                  <Text
                    style={{
                      fontFamily: medium,
                      color: mainRed,
                      fontSize: RFPercentage(1.8),
                    }}>
                    Yes
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ModalDownloader = props => {
  const {visible, onClose, status, percentage} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Button
          onPress={() => onClose()}
          style={{
            position: 'absolute',
            top: 50,
            left: 25,
            zIndex: 99,
            backgroundColor: white,
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            borderRadius: 35 / 2,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{
              color: black,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
            }}
          />
        </Button>
        {status ? (
          <View
            style={{
              padding: 10,
              width: width / 6,
              height: width / 6,
              borderWidth: 4,
              borderColor: mainRed,
              borderRadius: ((width / 6) * 0.5 + (width / 6) * 0.5) / 2,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: 40, color: mainGreen}}
            />
          </View>
        ) : (
          <View
            style={{
              minWidth: width / 4,
              borderRadius: 4,
              padding: 10,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginTop: 10}}
            />
            <Text style={{color: black, marginTop: 10}}>
              {percentage.toFixed(1)}%
            </Text>
            <Text style={{color: black, marginTop: 10}}>Exporting...</Text>
          </View>
        )}
      </View>
    </Modal>
  );
};

export const SentCard = props => {
  console.log('SentCard >>> ', props);
  const {item, onPress, openModalDelete} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    return (
      <View
        style={{
          flex: 1 / 2,
          padding: 5,
          paddingLeft: 2.5,
          paddingRight: 2.5,
        }}>
        <AnimatedTouch
          onPress={() => {
            try {
              // go to detail
              onPress(item?.id);
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            transparent
            style={{
              transform: [{scale}],
            }}>
            <TouchableOpacity
              onPress={() => openModalDelete(item?.id)}
              style={{
                width: 25,
                height: 25,
                position: 'absolute',
                zIndex: 5,
                top: 10,
                right: 10,
                backgroundColor: 'white',
                borderRadius: 25 / 2,
                borderWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: 15, color: black}}
              />
            </TouchableOpacity>
            <CardItem cardBody>
              <View
                style={{
                  flex: 1,
                  width: '100%',
                  height: width * 0.65,
                }}>
                <View
                  style={{
                    position: 'absolute',
                    flex: 1,
                    zIndex: 3,
                    width: '100%',
                    height: width * 0.65,
                    padding: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      maxWidth: width / 2.5,
                      minWidth: width / 2.5,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 0,
                      marginBottom: 3,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1),
                        textAlign: 'center',
                        // lineHeight: 18,
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      Together In Love
                    </Text>
                  </View>

                  {/* Groom Name */}
                  <View
                    style={{
                      width: width / 3.5,
                      padding: 2,
                      // minWidth: width / 2,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.7),
                        fontWeight: '400',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.groomName === '' || !item?.groomName
                        ? 'N/A'
                        : item?.groomName}
                    </Text>
                  </View>

                  {/* & */}
                  <Text
                    style={{
                      fontFamily: Baskerville,
                      fontSize: RFPercentage(2),
                      fontWeight: '300',
                      color: item?.textCollor ? item?.textCollor : black,
                    }}>
                    &
                  </Text>

                  {/* Bride Name */}
                  <View
                    style={{
                      width: width / 3.5,
                      padding: 2,
                      // minWidth: width / 2,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.7),
                        fontWeight: '400',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.brideName === '' || !item?.brideName
                        ? 'N/A'
                        : item?.brideName}
                    </Text>
                  </View>

                  {/* Date */}
                  <View
                    style={{
                      maxWidth: width / 2.5,
                      minWidth: width / 2.5,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth:
                        item?.weddingDate === '' || !item?.weddingDate ? 1 : 0,
                      marginBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'DD'
                        : moment(item?.weddingDate).format('DD')}
                    </Text>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        // lineHeight: 18,
                        color: item?.textCollor ? item?.textCollor : black,
                        marginHorizontal: 5,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'MMMM'
                        : moment(item?.weddingDate).format('MMMM')}
                    </Text>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'YYYY'
                        : moment(item?.weddingDate).format('YYYY')}
                    </Text>
                  </View>

                  {/* Location */}
                  <View
                    style={{
                      width: width / 3.5,
                      padding: 2,
                      // minWidth: width / 2,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: item?.location === '' ? 1 : 0,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.2),
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.location === '' || !item?.location
                        ? 'N/A'
                        : item?.location}
                    </Text>
                  </View>
                </View>
                <AsyncImage
                  source={
                    item?.dynamicUrl
                      ? {uri: `${item?.dynamicUrl}`}
                      : {uri: `${item.url}`}
                  }
                  placeholderColor={'white'}
                  resizeMode="cover"
                  style={{
                    position: 'absolute',
                    flex: 1,
                    zIndex: 1,
                    width: '100%',
                    height: width * 0.65,
                  }}
                  loaderStyle={{
                    width: width / 7,
                    height: height / 7,
                  }}
                />
              </View>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                paddingBottom: 0,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: greyLine,
                }}>
                {item?.name ? item?.name : 'N/A'}
              </Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                paddingBottom: 0,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: book,
                  fontSize: RFPercentage(1.5),
                  color: greyLine,
                }}>
                Total Invitation:{' '}
                {item?.totalInvitation === 0 ? 0 : item?.totalInvitation}
              </Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                paddingBottom: 0,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: book,
                  fontSize: RFPercentage(1.5),
                  color: greyLine,
                }}>
                Total Attendance:{' '}
                {item?.totalAttendance === 0 ? 0 : item?.totalAttendance}
              </Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: book,
                  fontSize: RFPercentage(1.2),
                  fontStyle: 'italic',
                  color: greyLine,
                }}>
                Latest sent on{' '}
                {item?.createdOn
                  ? moment(item?.createdOn).format('DD MMM YYYY')
                  : ''}
              </Text>
            </CardItem>
          </AnimatedCard>
        </AnimatedTouch>
      </View>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(ListSentInvitation);

export default props => <Wrapper {...props} />;
