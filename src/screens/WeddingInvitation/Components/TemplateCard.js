import React, {useRef} from 'react';
import {Text, Animated, Dimensions, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';

const {width, height} = Dimensions.get('window');
const {greyLine} = Colors;
const {medium} = FontType;

const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedCard = new Animated.createAnimatedComponent(Card);

const TemplateCard = props => {
  console.log('Template Card Props:', props);
  const {item, index, onPress} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    return (
      <View
        style={{
          flex: 1 / 2,
          padding: 5,
          paddingLeft: 2.5,
          paddingRight: 2.5,
        }}>
        <AnimatedTouch
          onPress={() => {
            try {
              onPress(index);
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            transparent
            style={{
              transform: [{scale}],
            }}>
            <CardItem cardBody>
              <AsyncImage
                source={
                  item?.dynamicUrl
                    ? {uri: `${item?.dynamicUrl}`}
                    : {uri: `${item.url}`}
                }
                placeholderColor={'white'}
                resizeMode="cover"
                style={{
                  flex: 1,
                  width: '100%',
                  height: width * 0.65,
                }}
                loaderStyle={{
                  width: width / 7,
                  height: height / 7,
                }}
              />
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: greyLine,
                }}>
                {item?.name ? item?.name : 'N/A'}
              </Text>
            </CardItem>
          </AnimatedCard>
        </AnimatedTouch>
      </View>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(TemplateCard);

export default props => <Wrapper {...props} />;
