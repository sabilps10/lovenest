import React, {useRef} from 'react';
import {Text, Animated, Dimensions, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Icon} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';
import moment from 'moment';

const {width, height} = Dimensions.get('window');
const {greyLine, white, black} = Colors;
const {medium, book, Baskerville, BaskervilleBold} = FontType;

const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedCard = new Animated.createAnimatedComponent(Card);

const TemplateDraftCard = props => {
  console.log('Template DRAFT Card Props:', props);
  const {item, index, onPress, openModalDelete} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    return (
      <View
        style={{
          flex: 1 / 2,
          padding: 5,
          paddingLeft: 2.5,
          paddingRight: 2.5,
        }}>
        <AnimatedTouch
          onPress={() => {
            try {
              onPress(index);
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <AnimatedCard
            transparent
            style={{
              transform: [{scale}],
            }}>
            <CardItem cardBody>
              <View
                style={{
                  position: 'relative',
                  width: '100%',
                  height: width * 0.65,
                }}>
                <TouchableOpacity
                  onPress={() => openModalDelete(item?.id)}
                  style={{
                    width: 25,
                    height: 25,
                    position: 'absolute',
                    zIndex: 5,
                    top: 10,
                    right: 10,
                    backgroundColor: 'white',
                    borderRadius: 25 / 2,
                    borderWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{left: 10, fontSize: 15, color: black}}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    position: 'absolute',
                    width: '100%',
                    height: width * 0.65,
                    zIndex: 3,
                    backgroundColor: 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      maxWidth: width / 2.5,
                      minWidth: width / 2.5,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 0,
                      marginBottom: 3,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1),
                        textAlign: 'center',
                        // lineHeight: 18,
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      Together In Love
                    </Text>
                  </View>

                  {/* Groom Name */}
                  <View
                    style={{
                      width: width / 5.5,
                      padding: 2,
                      borderWidth: 0,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.7),
                        fontWeight: '400',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.groomName === '' || !item?.groomName
                        ? 'N/A'
                        : item?.groomName}
                    </Text>
                  </View>

                  {/* & */}
                  <Text
                    style={{
                      fontFamily: Baskerville,
                      fontSize: RFPercentage(2),
                      fontWeight: '300',
                      color: item?.textCollor ? item?.textCollor : black,
                    }}>
                    &
                  </Text>

                  {/* Bride Name */}
                  <View
                    style={{
                      width: width / 5.5,
                      padding: 2,
                      borderWidth: 0,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.7),
                        fontWeight: '400',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.brideName === '' || !item?.brideName
                        ? 'N/A'
                        : item?.brideName}
                    </Text>
                  </View>

                  {/* Date */}
                  <View
                    style={{
                      maxWidth: width / 2.5,
                      minWidth: width / 2.5,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth:
                        item?.weddingDate === '' || !item?.weddingDate ? 1 : 0,
                      marginBottom: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'DD'
                        : moment(item?.weddingDate).format('DD')}
                    </Text>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        // lineHeight: 18,
                        color: item?.textCollor ? item?.textCollor : black,
                        marginHorizontal: 5,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'MMMM'
                        : moment(item?.weddingDate).format('MMMM')}
                    </Text>
                    <Text
                      style={{
                        fontFamily: BaskervilleBold,
                        fontSize: RFPercentage(1),
                        fontWeight: '600',
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.weddingDate === '' || !item?.weddingDate
                        ? 'YYYY'
                        : moment(item?.weddingDate).format('YYYY')}
                    </Text>
                  </View>

                  {/* Location */}
                  <View
                    style={{
                      width: width / 3.5,
                      padding: 2,
                      // minWidth: width / 2,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: item?.location === '' ? 1 : 0,
                    }}>
                    <Text
                      style={{
                        fontFamily: Baskerville,
                        fontSize: RFPercentage(1.2),
                        textAlign: 'center',
                        color: item?.textCollor ? item?.textCollor : black,
                      }}>
                      {item?.location === '' || !item?.location
                        ? 'N/A'
                        : item?.location}
                    </Text>
                  </View>
                </View>
                <AsyncImage
                  source={
                    item?.template?.dynamicUrl
                      ? {uri: `${item?.template?.dynamicUrl}`}
                      : {uri: `${item?.template?.url}`}
                  }
                  placeholderColor={'white'}
                  resizeMode="cover"
                  style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    zIndex: 1,
                  }}
                  loaderStyle={{
                    width: width / 7,
                    height: height / 7,
                  }}
                />
              </View>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: greyLine,
                }}>
                {item?.templateName ? item?.templateName : 'N/A'}
              </Text>
            </CardItem>
          </AnimatedCard>
        </AnimatedTouch>
      </View>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(TemplateDraftCard);

export default props => <Wrapper {...props} />;
