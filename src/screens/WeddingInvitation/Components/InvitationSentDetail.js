import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  RefreshControl,
  FlatList,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Pie from 'react-native-pie';
import {Container, Content, Card, CardItem, Icon, Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';
import Clipboard from '@react-native-clipboard/clipboard';
import moment from 'moment';

const {charSpiteGreen, charSpiteRed, charNoteGreen, charNoteRed} = charImage;
const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  overlayDim,
  mainGreen,
  mainRed,
} = Colors;
const {book, medium} = FontType;
const {width} = Dimensions.get('window');

// Query
import GET_INVITATION_SUMMARY from '../../../graphql/queries/getInvitationSummary';
import GET_INVITATION_LIST from '../../../graphql/queries/getCustomerWeddingInvitations';

import EmptyScreen from './EmptyScreen';

import RNFetchBlob from 'rn-fetch-blob';

// Query
import ExportPDF from '../../../graphql/queries/exportWeddingAttendancePdf';

const InvitationSentDetail = props => {
  console.log('InvitationSentDetail Props: ', props);
  const {navigation, client, goBack} = props;

  // Downloader
  const [showModalExport, setShowModalExport] = useState(false);
  const [successDownload, setSuccessDownload] = useState(false);
  const [percentage, setPercentage] = useState(0);
  const [loadingButton, setLoadingButton] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [summary, setSummary] = useState(null);
  const [totalInvitation, setTotalInvitation] = useState(0);
  const [refreshing, setRefreshing] = useState(false);

  const [modalInvitationListVisible, setModalInvitationVisible] =
    useState(false);

  useEffect(() => {
    fetch();

    const subs = navigation.addListener('focus', () => {
      fetch();
    });

    return subs;
  }, [props?.id]);

  const onDownload = async () => {
    try {
      await setShowModalExport(true);
      if (Platform.OS === 'ios') {
        await iOSDownload();
      } else {
        await androidDownload();
      }
    } catch (error) {
      await setShowModalExport(false);
    }
  };

  const iOSDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          variables: {
            templateId: props?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length) {
                const fileName = 'InvitationSummary.pdf';
                const paths = await RNFetchBlob.fs.dirs.CacheDir;

                await RNFetchBlob.config({
                  fileCache: true,
                  title: 'summary',
                  appendExt: 'pdf',
                  path: paths + fileName,
                })
                  .fetch(
                    'GET',
                    `${exportData[0]?.dynamicUrl}`,
                    // 'https://ars.els-cdn.com/content/image/1-s2.0-S1525001616328027-mmc2.pdf',
                    {
                      'Content-Type': 'octet-stream',
                    },
                    'base64DataString',
                  )
                  .progress({interval: 10}, async (received, total) => {
                    console.log('progress', received / total);
                    await setPercentage((received / total) * 100);
                  })
                  .then(async res => {
                    console.log('RESS BROOO: ', res.path());
                    // open the document directly

                    if (await res.path()) {
                      await setPercentage(0);
                      await setSuccessDownload(true);
                      await setSuccessDownload(false);
                      await setShowModalExport(false);

                      // await openDoc(res);
                      await RNFetchBlob.fs
                        .writeFile(res.path(), res.data, 'base64')
                        .then(async resp => {
                          console.log(resp);
                          await RNFetchBlob.ios.openDocument(res.path());
                          await setLoadingButton(false);
                        })
                        .catch(r => {
                          throw r;
                        });

                      await setTimeout(async () => {
                        await RNFetchBlob.fs
                          .writeFile(res.path(), res.data, 'base64')
                          .then(async resp => {
                            console.log(resp);
                            await RNFetchBlob.ios.openDocument(res.path());
                            await setLoadingButton(false);
                          })
                          .catch(r => {
                            throw r;
                          });
                      }, 0);
                    }
                  })
                  .catch(errorss => {
                    throw errorss;
                  });
              } else {
                await setPercentage(0);
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setPercentage(0);
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setPercentage(0);
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const androidDownload = async () => {
    try {
      await setLoadingButton(true);
      await client
        .query({
          query: ExportPDF,
          variables: {
            templateId: props?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async ress => {
          console.log('res: ', ress);
          const {data, errors} = ress;
          const {exportWeddingAttendancePdf} = data;
          const {data: exportData, error} = exportWeddingAttendancePdf;

          if (errors) {
            await setShowModalExport(false);
          } else {
            if (error) {
              await setShowModalExport(false);
            } else {
              if (exportData?.length === 1) {
                if (exportData[0]?.dynamicUrl) {
                  const fileName = `/${exportData[0]?.name}-${moment().format(
                    'DDMMMMYYYY',
                  )}-${moment().format('hhmmssa')}.pdf`;
                  RNFetchBlob.config({
                    fileCache: true,
                    addAndroidDownloads: {
                      useDownloadManager: true,
                      notification: true,
                      mediaScannable: true,
                      title: `${fileName}`,
                      path: `${RNFetchBlob?.fs?.dirs?.DownloadDir}${fileName}`,
                    },
                  })
                    .fetch(
                      'GET',
                      `${exportData[0]?.dynamicUrl}`,
                      {
                        'Content-Type': 'octet-stream',
                      },
                      'base64DataString',
                    )
                    .progress({interval: 10}, async (received, total) => {
                      console.log('progress', received / total);
                      await setPercentage((received / total) * 100);
                    })
                    .then(async res => {
                      console.log('RESS BROOO: ', res);
                      // open the document directly
                      await setSuccessDownload(true);
                      await setTimeout(async () => {
                        await setSuccessDownload(false);
                        await setShowModalExport(false);
                        await setPercentage(0);
                        await setTimeout(async () => {
                          await RNFetchBlob.android.actionViewIntent(
                            res.path(),
                            'application/pdf',
                          );
                          await setLoadingButton(false);
                        }, 1000);
                      }, 1500);
                      // or show options
                      // RNFetchBlob.ios.openDocument(res.path())
                    })
                    .catch(errorss => {
                      throw errorss;
                    });
                } else {
                  await setShowModalExport(false);
                  await setLoadingButton(false);
                }
              } else {
                await setShowModalExport(false);
                await setLoadingButton(false);
              }
            }
          }
        })
        .catch(async error => {
          await setShowModalExport(false);
          await setLoadingButton(false);
        });
    } catch (error) {
      await setShowModalExport(false);
      await setLoadingButton(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      if (props?.id) {
        await client
          .query({
            query: GET_INVITATION_SUMMARY,
            variables: {
              templateId: props?.id,
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async res => {
            console.log('res summary invitation: ', res);
            const {data, errors} = res;
            const {getInvitationSummary} = data;
            const {
              rsvp,
              vaccination,
              error,
              totalInvitation: total,
            } = getInvitationSummary;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
                await setRefreshing(false);
              } else {
                await setSummary({
                  rsvp,
                  vaccination,
                });
                await setTotalInvitation(total ? total : 0);
                await setIsError(false);
                await setIsLoading(false);
                await setRefreshing(false);
              }
            }
          })
          .catch(error => {
            throw error;
          });
      } else {
        await setIsError(true);
        await setIsLoading(false);
        await setRefreshing(false);
      }
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const onOpenModal = async () => {
    try {
      await setModalInvitationVisible(!modalInvitationListVisible);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Content contentContainerStyle={{paddingTop: 15}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          backgroundColor: white,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <EmptyScreen tabName="myrsvp" />
      </View>
    );
  } else {
    return (
      <Container>
        <ListInvitationModal
          id={props?.id ? props?.id : null}
          {...props}
          onClose={onOpenModal}
          visible={modalInvitationListVisible}
        />
        <ModalDownloader
          visible={showModalExport}
          onClose={async () => {
            await setShowModalExport(false);
          }}
          status={successDownload}
          percentage={percentage}
        />
        <Content
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          contentContainerStyle={{paddingTop: 15, paddingBottom: 80}}>
          <Headers
            goBack={goBack}
            loadingButton={loadingButton}
            showModalExport={showModalExport}
            onDownload={onDownload}
          />
          <Chart summary={summary} />
          <ButtonInvitationList onPress={onOpenModal} />
          <TotalInvitation
            summary={summary}
            totalInvitation={totalInvitation}
          />
        </Content>
      </Container>
    );
  }
};

export const ModalDownloader = props => {
  const {visible, onClose, status, percentage} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Button
          onPress={() => onClose()}
          style={{
            position: 'absolute',
            top: 50,
            left: 25,
            zIndex: 99,
            backgroundColor: white,
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            borderRadius: 35 / 2,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{
              color: black,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
            }}
          />
        </Button>
        {status ? (
          <View
            style={{
              padding: 10,
              width: width / 6,
              height: width / 6,
              borderWidth: 4,
              borderColor: mainRed,
              borderRadius: ((width / 6) * 0.5 + (width / 6) * 0.5) / 2,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: 40, color: mainGreen}}
            />
          </View>
        ) : (
          <View
            style={{
              minWidth: width / 4,
              borderRadius: 4,
              padding: 10,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginTop: 10}}
            />
            <Text style={{color: black, marginTop: 10}}>
              {percentage.toFixed(1)}%
            </Text>
            <Text style={{color: black, marginTop: 10}}>Exporting...</Text>
          </View>
        )}
      </View>
    </Modal>
  );
};

export const Headers = props => {
  const {
    goBack,
    onSubmit,
    onDownload,
    isLoadingSubmit,
    loadingButton,
    showModalExport,
  } = props;
  return (
    <Card transparent>
      <CardItem>
        <View style={{flex: 0.3, borderWidth: 0}}>
          <TouchableOpacity onPress={goBack}>
            <Icon
              type="Feather"
              name="arrow-left"
              style={{color: 'black', fontSize: 24}}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
              textAlign: 'center',
            }}>
            {props?.templateData?.name
              ? props?.templateData?.name
              : 'Detail RSVP'}
          </Text>
        </View>
        <View
          style={{
            flex: 0.3,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {loadingButton ? (
            <ActivityIndicator size="small" color={mainGreen} />
          ) : (
            <TouchableOpacity
              disabled={showModalExport ? true : false}
              onPress={onDownload}
              style={{
                padding: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="download"
                style={{
                  color: black,
                  fontSize: RFPercentage(2.5),
                  left: 20,
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </CardItem>
    </Card>
  );
};

export const ListInvitationModal = props => {
  const {navigation, client, visible, onClose} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoadMore, setIsLoadMore] = useState(false);

  const [totalData, setTotalData] = useState(0);

  useEffect(() => {
    if (visible) {
      fetch();
    }
  }, [visible, props?.id]);

  const onLoadMore = async () => {
    try {
      if (list?.length >= totalData) {
        await setIsLoadMore(false);
      } else {
        await setIsLoadMore(true);
        await setTimeout(async () => {
          await setItemDisplayed(itemDisplayed + 10);
          await fetch();
        }, 1000);
      }
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const fetch = async () => {
    try {
      if (props?.id) {
        await client
          .query({
            query: GET_INVITATION_LIST,
            variables: {
              templateId: props?.id,
              itemDisplayed,
              pageNumber,
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async res => {
            console.log('Res Modal List Invitation: ', res);
            const {data, errors} = res;
            const {getCustomerWeddingInvitations} = data;
            const {
              data: arrayList,
              error,
              totalCount,
            } = getCustomerWeddingInvitations;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
                await setIsLoadMore(false);
              } else {
                await setTotalData(totalCount);
                await setList([...arrayList]);
                await setIsError(false);
                await setIsLoading(false);
                await setIsLoadMore(false);
              }
            }
          })
          .catch(error => {
            throw error;
          });
      } else {
        await setIsError(true);
        await setIsLoading(false);
        await setIsLoadMore(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return <ParentCard item={item} index={index} />;
    },
    [list],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 0.3, backgroundColor: overlayDim}}
        />
        <View style={{flex: 1, backgroundColor: white}}>
          <Card
            style={{
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              bottom: 20,
              marginLeft: 0,
              marginRight: 0,
            }}>
            <CardItem style={{backgroundColor: 'transparent'}}>
              <View style={{flex: 0.2}} />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    textAlign: 'center',
                    letterSpacing: 0.3,
                  }}>
                  Invitation List
                </Text>
              </View>
              <View style={{flex: 0.2}}>
                <TouchableOpacity
                  onPress={onClose}
                  style={{
                    padding: 5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{
                      left: 10,
                      color: black,
                      fontSize: RFPercentage(2.2),
                    }}
                  />
                </TouchableOpacity>
              </View>
            </CardItem>
          </Card>
          <View style={{flex: 1}}>
            {isLoading && !isError ? (
              <Card transparent style={{bottom: 10}}>
                <CardItem
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator size="large" color={mainGreen} />
                </CardItem>
              </Card>
            ) : !isLoading && isError ? (
              <Card transparent style={{bottom: 10}}>
                <CardItem
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text>Failed To Load!</Text>
                </CardItem>
              </Card>
            ) : (
              <FlatList
                ListFooterComponent={() => {
                  return (
                    <View
                      style={{
                        opacity: isLoadMore ? 1 : 0,
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        paddingBottom: 15,
                      }}>
                      <ActivityIndicator
                        size="small"
                        color={mainGreen}
                        style={{marginHorizontal: 5}}
                      />
                      <Text
                        style={{
                          fontStyle: 'italic',
                          fontSize: 12,
                          color: greyLine,
                          textAlign: 'center',
                        }}>
                        Loading more...
                      </Text>
                    </View>
                  );
                }}
                onEndReachedThreshold={0.01}
                onMomentumScrollEnd={async () => {
                  try {
                    await onLoadMore();
                  } catch (error) {
                    await setIsLoadMore(false);
                  }
                }}
                contentContainerStyle={{paddingBottom: 30}}
                data={list}
                extraData={list}
                keyExtractor={keyExt}
                renderItem={renderItem}
              />
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ParentCard = props => {
  const {item, index} = props;

  const [isExpand, setIsExpand] = useState(false);

  if (item) {
    return (
      <Card transparent>
        <CardItem
          disabled={item?.attendances?.length === 0 ? true : false}
          button
          onPress={async () => {
            try {
              if (item?.attendances?.length === 0) {
                // Do nothing
              } else {
                await setIsExpand(!isExpand);
              }
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          style={{width: '100%', paddingTop: 0}}>
          <View style={{flex: 0.1}}>
            <View
              style={{
                width: 35,
                height: 35,
                borderRadius: 35 / 2,
                backgroundColor: greyLine,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="user"
                style={{left: 10, fontSize: RFPercentage(1.8), color: white}}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              paddingLeft: 25,
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.name === '' ? 'No-Name' : item?.name}
            </Text>
            <Text
              style={{
                marginTop: 5,
                fontFamily: medium,
                fontStyle: 'italic',
                fontSize: RFPercentage(1.4),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.phoneNo ? item?.phoneNo : 'No Phone Number'}
            </Text>
          </View>
          <View style={{flex: 0.1}}>
            {isExpand ? (
              <View
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 35 / 2,
                  backgroundColor: 'tranparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <View>
                  <Text
                    style={{
                      color: '#7FB13F',
                      fontFamily: medium,
                      fontSize: RFPercentage(2),
                      letterSpacing: 0.3,
                    }}>
                    (
                    {item?.attendances?.length ? item?.attendances?.length : ''}
                    )
                  </Text>
                </View>
                <Icon
                  type="Feather"
                  name="chevron-up"
                  style={{
                    marginLeft: 5,
                    fontSize: RFPercentage(2.5),
                    color: 'grey',
                  }}
                />
              </View>
            ) : (
              <View
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 35 / 2,
                  backgroundColor: 'tranparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <View>
                  <Text
                    style={{
                      color:
                        item?.attendances?.length === 0 ? mainRed : '#7FB13F',
                      fontFamily: medium,
                      fontSize: RFPercentage(2),
                      letterSpacing: 0.3,
                    }}>
                    (
                    {item?.attendances?.length
                      ? item?.attendances?.length
                      : '0'}
                    )
                  </Text>
                </View>
                {item?.attendances?.length === 0 ? null : (
                  <Icon
                    type="Feather"
                    name="chevron-down"
                    style={{
                      marginLeft: 5,
                      fontSize: RFPercentage(2.5),
                      color: 'grey',
                    }}
                  />
                )}
              </View>
            )}
          </View>
        </CardItem>
        {isExpand ? (
          <CardItem>
            <ChildListAttendees
              url={item?.invitationUrl}
              attendees={item?.attendances}
              parentIndex={index}
            />
          </CardItem>
        ) : (
          <CardItem style={{paddingTop: 0}}>
            <View
              style={{
                borderBottomColor: headerBorderBottom,
                borderBottomWidth: 1,
                width: '100%',
                height: 1,
              }}
            />
          </CardItem>
        )}
      </Card>
    );
  } else {
    return null;
  }
};

export const ChildListAttendees = props => {
  const {attendees, parentIndex, url} = props;

  const [copied, setCopied] = useState(false);

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [attendees],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <Card transparent>
          <CardItem
            style={{
              width: '100%',
              paddingTop: 0,
              borderBottomWidth: 1,
              borderBottomColor: headerBorderBottom,
            }}>
            <View style={{flex: 0.1}}>
              <View
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 35 / 2,
                  backgroundColor: greyLine,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="user"
                  style={{left: 10, fontSize: RFPercentage(1.8), color: white}}
                />
              </View>
            </View>
            <View
              style={{
                flex: 1,
                paddingLeft: 25,
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {item?.name === '' ? 'No-Name' : item?.name}
              </Text>
              <Text
                style={{
                  marginTop: 5,
                  fontFamily: medium,
                  fontStyle: 'italic',
                  fontSize: RFPercentage(1.4),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {item?.phoneNo ? item?.phoneNo : 'No Phone Number'}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 35 / 2,
                  backgroundColor: 'tranparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={item?.vaccinatedSwab ? charSpiteGreen : charSpiteRed}
                  style={{width: 20, height: 20}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 35 / 2,
                  backgroundColor: 'tranparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={item?.rsvpStatus ? charNoteGreen : charNoteRed}
                  style={{width: 20, height: 20}}
                  resizeMode="contain"
                />
              </View>
            </View>
          </CardItem>
          <CardItem style={{paddingTop: 0}}>
            <View
              style={{
                borderBottomColor: 'transparent',
                borderBottomWidth: 1,
                width: '100%',
                height: 1,
              }}
            />
          </CardItem>
        </Card>
      );
    },
    [attendees],
  );

  if (attendees) {
    return (
      <FlatList
        disableVirtualization
        legacyImplementation
        scrollEnabled={false}
        ListHeaderComponent={() => {
          return (
            <Card
              transparent
              style={{bottom: 10, marginLeft: 0, marginRight: 0}}>
              <CardItem
                button
                onPress={async () => {
                  try {
                    await Clipboard.setString(props?.url);
                    await setCopied(true);
                    await setTimeout(async () => {
                      await setCopied(false);
                    }, 1000);
                  } catch (error) {
                    console.log('Error: ', error);
                  }
                }}
                style={{backgroundColor: headerBorderBottom, width: '100%'}}>
                <View style={{flex: 1, flexDirection: 'row', padding: 2}}>
                  <Text
                    selectable
                    style={{
                      color: black,
                      fontFamily: medium,
                      letterSpacing: 0.3,
                      fontSize: RFPercentage(1.5),
                    }}>
                    {props?.url}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0.1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="copy"
                    style={{fontSize: RFPercentage(2), color: black}}
                  />
                </View>
              </CardItem>
              {copied ? (
                <CardItem style={{paddingTop: 5, paddingBottom: 0}}>
                  <Text
                    style={{
                      fontSize: RFPercentage(1.3),
                      color: greyLine,
                      fontStyle: 'italic',
                    }}>
                    link copied
                  </Text>
                </CardItem>
              ) : null}
            </Card>
          );
        }}
        data={attendees}
        extraData={attendees}
        keyExtractor={keyExt}
        renderItem={renderItem}
      />
    );
  } else {
    return null;
  }
};

export const TotalInvitation = props => {
  const {summary, totalInvitation} = props;
  return (
    <Card transparent>
      <CardItem>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              textAlign: 'center',
              marginBottom: 10,
            }}>
            Total of Invitation Sent
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.7),
              color: mainRed,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            {totalInvitation}
          </Text>
        </View>
      </CardItem>
      <CardItem>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Attending
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.rsvp?.attending ? summary?.rsvp?.attending : 0}
            </Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Vaccinated
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.vaccination?.vaccinated
                ? summary?.vaccination?.vaccinated
                : 0}
            </Text>
          </View>
        </View>
      </CardItem>
      <CardItem>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Not Attending
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.rsvp?.notAttending ? summary?.rsvp?.notAttending : 0}
            </Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                marginBottom: 10,
              }}>
              Not Vaccinated
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.7),
                color: mainRed,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {summary?.vaccination?.notVaccinated
                ? summary?.vaccination?.notVaccinated
                : 0}
            </Text>
          </View>
        </View>
      </CardItem>
    </Card>
  );
};

export const ButtonInvitationList = props => {
  const {onPress} = props;
  return (
    <Card transparent style={{marginTop: 20}}>
      <CardItem
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={onPress}
          style={{padding: 10, borderWidth: 2, borderColor: mainRed}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            INVITATION LIST
          </Text>
        </TouchableOpacity>
      </CardItem>
    </Card>
  );
};

export const Chart = props => {
  const {summary} = props;
  return (
    <Card transparent>
      <CardItem>
        <View style={{flex: 1, borderWidth: 0}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                marginBottom: 25,
              }}>
              RSVP
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <PieChart type={'attendees'} summary={summary} />
          </View>
          <Legend title1={'Attending'} title2={'Not Attending'} />
        </View>
        <View style={{flex: 1, borderWidth: 0}}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                marginBottom: 25,
              }}>
              Vaccination
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <PieChart type={'vaccination'} summary={summary} />
          </View>
          <Legend title1={'Vaccinated'} title2={'Not Vaccinated'} />
        </View>
      </CardItem>
    </Card>
  );
};

export const Legend = props => {
  const {title1, title2} = props;
  return (
    <View
      style={{
        marginTop: 15,
        width: '100%',
        borderWidth: 0,
        paddingTop: 5,
        paddingBottom: 5,
      }}>
      <View
        style={{
          paddingLeft: 25,
          width: '100%',
          borderWidth: 0,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginVertical: 5,
        }}>
        <View
          style={{
            width: 20,
            height: 20,
            backgroundColor: mainRed,
            marginRight: 10,
          }}
        />
        <Text
          style={{
            fontSize: RFPercentage(1.5),
            color: black,
            fontFamily: medium,
            letterSpacing: 0.3,
          }}>
          {title1}
        </Text>
      </View>
      <View
        style={{
          paddingLeft: 25,
          width: '100%',
          borderWidth: 0,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginVertical: 5,
        }}>
        <View
          style={{
            width: 20,
            height: 20,
            backgroundColor: greyLine,
            marginRight: 10,
          }}
        />
        <Text
          style={{
            fontSize: RFPercentage(1.5),
            color: black,
            fontFamily: medium,
            letterSpacing: 0.3,
          }}>
          {title2}
        </Text>
      </View>
    </View>
  );
};

export const PieChart = props => {
  console.log('PieChart Props: >>>>>> ', props);
  const {summary, type} = props;

  if (type === 'attendees') {
    return (
      <Pie
        backgroundColor={headerBorderBottom}
        radius={80}
        sections={[
          {
            percentage: summary?.rsvp?.attendingPerc
              ? summary?.rsvp?.attendingPerc
              : 0,
            color: mainRed,
          },
          {
            percentage: summary?.rsvp?.notAttendingPerc
              ? summary?.rsvp?.notAttendingPerc
              : 0,
            color: greyLine,
          },
        ]}
        strokeCap={'butt'}
      />
    );
  } else {
    return (
      <Pie
        backgroundColor={headerBorderBottom}
        radius={80}
        sections={[
          {
            percentage: summary?.vaccination?.vaccinatedPerc
              ? summary?.vaccination?.vaccinatedPerc
              : 0,
            color: mainRed,
          },
          {
            percentage: summary?.vaccination?.notVaccinatedPerc
              ? summary?.vaccination?.notVaccinatedPerc
              : 0,
            color: greyLine,
          },
        ]}
        strokeCap={'butt'}
      />
    );
  }
};

const Wrapper = compose(withApollo)(InvitationSentDetail);

export default props => <Wrapper {...props} />;
