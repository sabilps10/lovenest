import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../utils/AsyncstorageDataStructure';
import Colors from '../../../utils/Themes/Colors';
import _ from 'lodash';
import {FontType} from '../../../utils/Themes/Fonts';
import AsyncImage from '../../../components/Image/AsyncImage';
import {useNavigation} from '@react-navigation/native';

const {medium} = FontType;
const {asyncToken} = AsyncData;
const {mainGreen, greyLine, white, headerBorderBottom} = Colors;
const {width, height} = Dimensions?.get('screen');

// Query
import GET_LIST_INVITATION_TEMPLATE from '../../../graphql/queries/getListInvitationTemplate';
import EmptyScreen from './EmptyScreen';
import {RFPercentage} from 'react-native-responsive-fontsize';

import Headers from '../Components/Header'

const ListTemplate = props => {
  const {client, navigation} = props;

  const [list, setList] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);
  const [total, setTotal] = useState(0);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    loginCheck();

    if (isLoadMore) {
      fetchMore();
    }

    if (refreshing) {
      onRefresh();
    }
  }, [isLoadMore, refreshing]);

  const onRefresh = useCallback(async () => {
    try {
      await setPageNumber(1);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  }, [refreshing]);

  const fetchMore = useCallback(async () => {
    try {
      console.log('Masuk fetchMore');
      await setPageNumber(pageNumber + 1);
      await fetch();
    } catch (error) {
      await setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_LIST_INVITATION_TEMPLATE,
          variables: {
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Response Fetch List Template E-Invitation: ', res);
          const {data, errors} = res;
          const {templatesInvitationMobile} = data;
          const {data: listData, error, totalCount} = templatesInvitationMobile;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await setTotal(totalCount);
              let oldData = list?.concat(listData);
              const removeDuplicatedData = await _.uniqBy(oldData, 'id');
              console.log('removeDuplicatedData: ', removeDuplicatedData);
              // await setList([]);
              await setList([...removeDuplicatedData]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          }
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const tokenAvailCheck = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage?.getItem(asyncToken);
        console.log('token: ', token);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const loginCheck = async () => {
    try {
      const isTrue = await tokenAvailCheck();
      console.log('isTrue: ', isTrue);
      if (isTrue) {
        await setIsLogin(true);
        await fetch();
      } else {
        await setIsLogin(false);
        await setIsError(true);
        await setIsLoading(false);
        await setIsLoadMore(false);
      }
    } catch (error) {
      await setIsLogin(false);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator
          size="large"
          color={mainGreen}
          style={{bottom: 90}}
        />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => setRefreshing(true)}
            />
          }>
          <View style={{width, height: width * 1.7}}>
            <EmptyScreen tabName="create" />
          </View>
        </ScrollView>
      </View>
    );
  } else {
    if (list?.length === 0) {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }>
            <View style={{width, height: width * 1.7}}>
              <EmptyScreen tabName="create" />
            </View>
          </ScrollView>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          {list?.length === 0 ? null : <Headers {...props} />}
          <List
            refreshing={refreshing}
            onRefresh={() => setRefreshing(true)}
            total={total}
            isLoadMore={isLoadMore}
            list={list}
            setIsLoadMore={status => setIsLoadMore(status)}
          />
        </View>
      );
    }
  }
};

export const List = props => {
  const {list, total, setIsLoadMore, isLoadMore, refreshing, onRefresh} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return <Cards item={item} index={index} />;
    },
    [list],
  );

  return (
    <FlatList
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      onEndReached={() => {
        if (list?.length < total) {
          console.log('Masuk True isLoadMore');
          setIsLoadMore(true);
        } else {
          console.log('Masuk False isLoadMore');
          setIsLoadMore(false);
        }
      }}
      onEndReachedThreshold={0.5}
      contentContainerStyle={{
        paddingBottom: 130,
        paddingLeft: 5,
        paddingRight: 5,
      }}
      initialNumToRender={10}
      data={list}
      extraData={list}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      numColumns={2}
      ListEmptyComponent={() => {
        return (
          <View style={{flex: 1, backgroundColor: white}}>
            <View style={{width, height: width * 1.4}}>
              <EmptyScreen tabName="create" />
            </View>
          </View>
        );
      }}
      ListHeaderComponent={() => {
        if (list?.length === 0) {
          return null;
        } else {
          return <HeaderList total={total} />;
        }
      }}
      ListFooterComponent={() => {
        if (isLoadMore) {
          return <FooterList />;
        } else {
          return null;
        }
      }}
    />
  );
};

export const HeaderList = props => {
  const {total} = props;
  return (
    <View
      style={{
        width: '100%',
        paddingTop: 0,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.6),
          color: greyLine,
        }}>
        Total: {total}
      </Text>
    </View>
  );
};

export const FooterList = props => {
  return (
    <View
      style={{
        width: '100%',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text>Load more...</Text>
    </View>
  );
};

export const Cards = props => {
  const {item, index} = props;
  const navigation = useNavigation();

  const navigate = (templateId, url) => {
    try {
      console?.log('KONTOL KARTU: ', url);
      navigation?.navigate('EditorWithTemplate', {
        templateId,
        url,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <TouchableOpacity
      onPress={() => {
        console?.log('KONTOL KARTU: ', item);
        const uri = item?.dynamicUrl
          ? `${item?.dynamicUrl}=nu`
          : `${item?.url}`;

        navigate(item?.id, uri);
      }}
      style={{
        flex: 1 / 2,
        padding: 10,
      }}>
      <View
        style={{
          width: '100%',
          height: width * 0.6,
          backgroundColor: headerBorderBottom,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          borderRadius: 3,
        }}>
        <AsyncImage
          source={{
            uri: item?.dynamicUrl ? `${item?.dynamicUrl}=nu` : `${item?.url}`,
          }}
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            borderRadius: 3,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(ListTemplate);

export default props => <Wrapper {...props} />;
