import React, {useState, useEffect} from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  View,
  Platform,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import {Card, CardItem} from 'native-base';

const {white, black, greyLine, mainRed, overlayDim} = Colors;
const {medium, book} = FontType;

import DateTimePicker from '@react-native-community/datetimepicker';

const DateAndTimePickupForm = props => {
  const {title, disabled, type, date, onChange} = props;

  const [showIOSDatePicker, setShowIOSDatePicker] = useState(false);
  const [showAndroidDatePicker, setShowAndroidDatePicker] = useState(false);

  const onOpenPicker = async () => {
    try {
      if (Platform?.OS === 'android') {
        await setShowAndroidDatePicker(!showAndroidDatePicker);
      } else {
        await setShowIOSDatePicker(!showIOSDatePicker);
      }
    } catch (error) {
      await setShowIOSDatePicker(false);
      await setShowAndroidDatePicker(false);
    }
  };

  return (
    <>
      <CardItem>
        <View style={{width: '100%'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: mainRed}}>*</Text>
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
            {disabled ? (
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: medium,
                  color: greyLine,
                  fontSize: RFPercentage(1.5),
                  letterSpacing: 0.3,
                  fontStyle: 'italic',
                }}>
                (please select date first!)
              </Text>
            ) : null}
          </View>
          <View style={{marginTop: 5, paddingLeft: 7, paddingRight: 7}}>
            <TouchableOpacity
              disabled={disabled}
              onPress={onOpenPicker}
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <Text
                style={{
                  color: date === '' ? greyLine : black,
                  fontSize: RFPercentage(1.85),
                  letterSpacing: 0.3,
                }}>
                {date === ''
                  ? `Select ${type === 'Date' ? 'Date' : 'Time'}`
                  : moment(date).format(
                      type === 'Date' ? 'DD MMM YYYY' : 'hh:mm A',
                    )}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 10,
              borderBottomColor: greyLine,
              borderBottomWidth: 1,
              width: '100%',
              height: 0.5,
            }}
          />
        </View>
      </CardItem>
      {/* IOS */}
      {Platform?.OS === 'ios' ? (
        showIOSDatePicker ? (
          <Modal visible={showIOSDatePicker} animationType="fade" transparent>
            <IOSDatePicker
              theDate={date}
              pickerMode={type === 'Date' ? 'date' : 'time'}
              onChange={async e => {
                console.log('eeeee: ', e);
                await onChange(e);
                await onOpenPicker();
              }}
              cancel={() => onOpenPicker()}
            />
          </Modal>
        ) : null
      ) : null}
      {/* Android */}
      {Platform?.OS === 'android' ? (
        showAndroidDatePicker ? (
          <AndroidDatePicker
            cancel={() => onOpenPicker()}
            pickerMode={type === 'Date' ? 'date' : 'time'}
            show={showAndroidDatePicker}
            theDate={date === '' ? '' : date}
            onChange={async e => {
              console.log('EEE android : ', e);
              try {
                console.log('eeeee: ', e);
                await onOpenPicker();
                await onChange(e);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
        ) : null
      ) : null}
    </>
  );
};

export const AndroidDatePicker = props => {
  const {show, theDate, pickerMode, onChange: onChangeDate, cancel} = props;
  console.log('theDate Date Picker Android: ', theDate);

  const [date, setDate] = useState(theDate);
  const [mode, setMode] = useState(pickerMode);
  const [showup, setShowUp] = useState(false);

  useEffect(() => {
    settingShow(show);
  }, []);

  const settingShow = value => {
    setShowUp(value);
  };

  const onChange = (event, selectedDate) => {
    console.log('Event Date Picker Android: ', event);
    console.log('SelectedDate Date Picker Android: ', selectedDate);
    if (event.type === 'set') {
      onChangeDate(selectedDate);
      settingShow(false);
    } else {
      cancel();
      settingShow(false);
    }
  };

  return (
    <View>
      {showup ? (
        <DateTimePicker
          testID="dateTimePicker"
          minimumDate={new Date()}
          value={theDate === '' ? new Date() : new Date(date)}
          mode={pickerMode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </View>
  );
};

export const IOSDatePicker = props => {
  const {theDate, pickerMode, onChange, cancel} = props;

  const [date, setDate] = useState(theDate === '' ? new Date() : theDate);

  const onChangeDate = (event, selectedDate) => {
    setDate(selectedDate);
  };

  const ok = () => {
    onChange(date === '' ? new Date() : date);
  };

  return (
    <View
      style={{
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: overlayDim,
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          cancel();
        }}
        style={{flex: 2, backgroundColor: 'transparent'}}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: greyLine,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: '#f8f8f8',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => cancel()}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => {
                ok();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: '100%', backgroundColor: white}}>
          {pickerMode === 'time' ? (
            <DateTimePicker
              testID="dateTimePicker"
              value={date === '' ? new Date() : new Date(date)}
              mode={pickerMode}
              is24Hour={true}
              display="spinner"
              onChange={onChangeDate}
            />
          ) : (
            <DateTimePicker
              minimumDate={new Date()}
              testID="dateTimePicker"
              value={date === '' ? new Date() : new Date(date)}
              mode={pickerMode}
              is24Hour={true}
              display="spinner"
              onChange={onChangeDate}
            />
          )}
        </View>
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(DateAndTimePickupForm);

export default props => <Wrapper {...props} />;
