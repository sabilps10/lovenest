import React, {useCallback, useState, useEffect, useRef} from 'react';
import {
  Text,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  ImageBackground,
  Modal,
  FlatList,
  ActivityIndicator,
  Animated,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Form from '../../../components/FormInput/FormInput';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Icon,
} from 'native-base';
import _ from 'lodash';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import FlipCard from 'react-native-flip-card';
import {selectContactPhone} from 'react-native-select-contact';
import {connect} from 'react-redux';
import ViewShot from 'react-native-view-shot';
import {ReactNativeFile} from 'apollo-upload-client';
import Contacts from 'react-native-contacts';

const {
  black,
  white,
  overlayDim,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
} = Colors;
const {medium, Baskerville, BaskervilleBold} = FontType;
const {width} = Dimensions.get('window');

// Componenst
import DateAndTimePickupForm from './DateTimeForm';
import ModalContactPhone from './ListContactFromPhone';

//  Mutation
import createWeddingInvitation from '../../../graphql/mutations/customerWeddingInvitation';
import saveToDraf from '../../../graphql/mutations/customerDraftTemplate';

import {commonImage} from '../../../utils/Themes/Images';
const {failedGear, successGear} = commonImage;

const SingleForm = props => {
  console.log('Single Form Props: ', props);
  const {templateData, client, positionYBottomNav} = props;

  const frontRef = useRef(null);
  const backRef = useRef(null);

  const [showModalColorPicker, setShowModalColorPicker] = useState(false);
  const [selectedColor, setSelectedColor] = useState(black);
  const [input, setInput] = useState({
    groom: '',
    bride: '',
    date: '',
    location: '',
  });
  const [eventTime, setEventTime] = useState([
    {
      eventName: '',
      startTime: '',
      endTime: '',
    },
  ]);

  const [selectedContactList, setSelectedContactList] = useState([]);

  const [showOpenContactListFromPhone, setShowOpenContactListFromPhone] =
    useState(false);

  const [onSubmitError, setOnSubmitError] = useState(false);
  const [onSubmitErrorMsg, setOnSubmitErrorMsg] = useState('');

  const [onSubmitSuccess, setOnSubmitSuccess] = useState(false);
  const [onSubmitSuccessMsg, setOnSubmitSuccessMsg] = useState('');

  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);

  const [onSubmitErrorDraft, setOnSubmitErrorDraft] = useState(false);
  const [onSubmitErrorMsgDraft, setOnSubmitErrorMsgDraft] = useState('');

  const [onSubmitSuccessDraft, setOnSubmitSuccessDraft] = useState(false);
  const [onSubmitSuccessMsgDraft, setOnSubmitSuccessMsgDraft] = useState('');
  const [isLoadingSubmitDraft, setIsLoadingSubmitDraft] = useState(false);

  const [isLoadingContactOpen, setIsLoadingContactOpen] = useState(false);

  const [flip, setFlip] = useState(false);

  const [isFirstOpen, setIsFirstOpen] = useState(0);

  useEffect(() => {
    if (isLoadingContactOpen) {
      openPopUpNativeContact();
    }

    // const subs = onChangeOpacity();
    // return subs;
  }, [isLoadingContactOpen]);

  const onFlip = () => {
    setFlip(!flip);
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const checkPermissionContact = () => {
    return new Promise(resolve => {
      try {
        if (Platform?.OS === 'ios') {
          Contacts?.checkPermission()
            .then(permission => {
              // Contacts.PERMISSION_AUTHORIZED || Contacts.PERMISSION_UNDEFINED || Contacts.PERMISSION_DENIED
              if (permission === 'undefined') {
                Contacts.requestPermission().then(() => {
                  resolve(false);
                });
              }
              if (permission === 'authorized') {
                // yay!
                resolve(true);
              }
              if (permission === 'denied') {
                // x.x
                resolve(false);
              }
            })
            .catch(error => {
              console.log('IOS Error Permission: ', error);
              resolve(false);
            });
        } else {
          //  Android
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
              title: 'Contacts',
              message: 'This app would like to view your contacts.',
              buttonPositive: 'Please accept bare mortal',
            },
          )
            .then(status => {
              console.log('Permission Android: ', status);
              if (status === 'granted') {
                resolve(true);
              } else {
                resolve(false);
              }
            })
            .catch(error => {
              console.log('error Permission Android: ', error);
            });
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const openModalListContact = async () => {
    try {
      const checkingPermission = await checkPermissionContact();
      console.log('checkingPermission >>> ', checkingPermission);
      if (checkingPermission) {
        await setIsLoadingContactOpen(true);
      } else {
        // not granted permission
        await setIsLoadingContactOpen(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingContactOpen(false);
      await setShowOpenContactListFromPhone(false);
    }
  };

  const openPopUpNativeContact = async () => {
    try {
      if (isFirstOpen === 0) {
        await setTimeout(async () => {
          let oldSelectedContactList = selectedContactList;
          const resOfSelectedContact = await openNativeContact();
          console.log('resOfSelectedContact >>> ', resOfSelectedContact);

          if (resOfSelectedContact?.phoneNo) {
            oldSelectedContactList.push(resOfSelectedContact);
            console.log('OLD Selected Contact List: ', oldSelectedContactList);
            const removeDuplicated = await _.uniqBy(
              oldSelectedContactList,
              'id',
            );
            console.log('removeDuplicated: ', removeDuplicated);
            await setIsFirstOpen(1);
            await setSelectedContactList([...removeDuplicated]);
          } else {
            // failed add to contact
            setIsLoadingContactOpen(false);
            console.log('Failed add contact list');
          }
        }, 1500);
      } else {
        //  not first time load to open
        let oldSelectedContactList = selectedContactList;
        const resOfSelectedContact = await openNativeContact();
        console.log('resOfSelectedContact >>> ', resOfSelectedContact);

        if (resOfSelectedContact?.phoneNo) {
          oldSelectedContactList.push(resOfSelectedContact);
          console.log('OLD Selected Contact List: ', oldSelectedContactList);
          const removeDuplicated = await _.uniqBy(oldSelectedContactList, 'id');
          console.log('removeDuplicated: ', removeDuplicated);
          await setSelectedContactList([...removeDuplicated]);
        } else {
          // failed add to contact
          setIsLoadingContactOpen(false);
          console.log('Failed add contact list');
        }
      }
    } catch (error) {
      await setIsLoadingContactOpen(false);
      await setShowOpenContactListFromPhone(false);
    }
  };

  const openNativeContact = () => {
    return new Promise(resolve => {
      try {
        selectContactPhone()
          .then(selection => {
            console.log('CONTACT SELECTION: ', selection);
            if (!selection) {
              setIsLoadingContactOpen(false);
              resolve(null);
            }

            let {contact, selectedPhone} = selection;
            console.log(
              `Selected ${selectedPhone.type} phone number ${selectedPhone.number} from ${contact.name}`,
            );

            const normalizePhoneNumber = selectedPhone.number
              ?.replace(/\s/g, '')
              ?.replace('-', '');
            const checkPhoneNumberHasPlus =
              normalizePhoneNumber[0] === '+' ? true : false;
            setIsLoadingContactOpen(false);
            resolve({
              id: contact?.recordId,
              name: contact?.name,
              phoneNo: normalizePhoneNumber,
              valid: checkPhoneNumberHasPlus,
              selected: false,
            });
          })
          .catch(error => {
            setIsLoadingContactOpen(false);
            console.log('Error: ', error);
            resolve(null);
          });
      } catch (error) {
        resolve(null);
      }
    });
  };

  const handleOnChange = (propsName, value) => {
    try {
      if (propsName === 'groom') {
        setInput({
          ...input,
          [propsName]: value,
        });
      } else if (propsName === 'bride') {
        setInput({
          ...input,
          [propsName]: value,
        });
      } else if (propsName === 'date') {
        setInput({
          ...input,
          [propsName]: value,
        });
      } else {
        setInput({
          ...input,
          [propsName]: value,
        });
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onSelectedColor = async color => {
    try {
      console.log('Selected Colors: ', color);
      await setSelectedColor(color);
      await modalVisibility();
    } catch (error) {}
  };

  const modalVisibility = async () => {
    try {
      await setShowModalColorPicker(!showModalColorPicker);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const validateEventTimeEmptiness = () => {
    return new Promise(async resolve => {
      try {
        const getTotalValid = await eventTime
          ?.map(d => {
            if (
              d?.eventName === '' ||
              d?.startTime === '' ||
              d?.endTime === ''
            ) {
              return false;
            } else {
              return true;
            }
          })
          .filter(Boolean);
        console.log('getTotalValid >>> ', getTotalValid);
        if (getTotalValid) {
          resolve(getTotalValid);
        } else {
          resolve([]);
        }
      } catch (error) {
        console.log('Error validateEventTimeEmptiness: ', error);
        resolve([]);
      }
    });
  };

  const saveToDraftTemplate = async () => {
    try {
      console.log('Input: ', input);
      await setIsLoadingSubmitDraft(true);
      if (
        input?.groom === '' ||
        input?.bride === '' ||
        input?.date === '' ||
        input?.location === ''
      ) {
        await setIsLoadingSubmitDraft(false);
        await setOnSubmitErrorDraft(true);
        await setOnSubmitErrorMsgDraft(
          'Please fill in the form and the number of recipients',
        );
        await setTimeout(async () => {
          await setOnSubmitErrorDraft(false);
          await setOnSubmitErrorMsgDraft('');
        }, 2000);
      } else {
        // all filled

        const invalidContactList = await selectedContactList.filter(element => {
          return element?.valid === false;
        });
        console.log('invalidContactList: >> ', invalidContactList);

        if (invalidContactList?.length === 0) {
          const frontTierCaptuire = await capturingFrontCard();

          const backTierCapture = await capturingBackCard();

          console.log('SATTT: ', {
            frontTierCaptuire,
            backTierCapture,
          });

          const frontImageParams = new ReactNativeFile({
            uri: frontTierCaptuire,
            name: `${input?.groom}with${input?.bride}isfronttype.png`,
            type: 'image/png',
          });

          const backImageParams = new ReactNativeFile({
            uri: backTierCapture,
            name: `${input?.groom}with${input?.bride}isbacktype.png`,
            type: 'image/png',
          });

          const variables = {
            templateId: parseInt(templateData?.id, 10),
            templateName: templateData?.name,
            brideName: input?.bride,
            groomName: input?.groom,
            weddingDate: moment(input?.date).format('DD MMMM YYYY'),
            eventTime: eventTime,
            location: input?.location,
            contactSelected: selectedContactList,
            textCollor: selectedColor,
            frontImage: frontImageParams,
            backImage: backImageParams,
          };

          console.log('variables ANUUU >>> ', variables);

          await client
            .mutate({
              mutation: saveToDraf,
              variables,
            })
            .then(async res => {
              console.log('Res Submit Invitation DRAFT: ', res);
              const {data, errors} = res;
              const {customerDraftTemplate: cwi} = data;
              const {error} = cwi;

              if (errors) {
                await setIsLoadingSubmitDraft(false);
                await setOnSubmitErrorDraft(true);
                await setOnSubmitErrorMsgDraft('Failed to Save Invitation!');
                await setTimeout(async () => {
                  await setOnSubmitErrorDraft(false);
                  await setOnSubmitErrorMsgDraft('');
                }, 2000);
              } else {
                if (error) {
                  await setIsLoadingSubmitDraft(false);
                  await setOnSubmitErrorDraft(true);
                  await setOnSubmitErrorMsgDraft('Failed to Save Invitation!');
                  await setTimeout(async () => {
                    await setOnSubmitErrorDraft(false);
                    await setOnSubmitErrorMsgDraft('');
                  }, 2000);
                } else {
                  await setIsLoadingSubmitDraft(false);
                  await setOnSubmitErrorDraft(false);
                  await setOnSubmitErrorMsgDraft('');
                  await setOnSubmitSuccessDraft(true);
                  await setOnSubmitSuccessMsgDraft('Invitation Save to Draft!');
                  await setTimeout(async () => {
                    await setOnSubmitErrorDraft(false);
                    await setOnSubmitErrorMsgDraft('');
                    await setOnSubmitSuccessDraft(false);
                    await setOnSubmitSuccessMsgDraft('');
                    await props?.backToList();
                  }, 2000);
                }
              }
            })
            .catch(error => {
              throw error;
            });
        } else {
          await setIsLoadingSubmit(false);
          await setOnSubmitError(true);
          await setOnSubmitErrorMsg('There is invalid format of contact !');
          await setTimeout(async () => {
            await setOnSubmitError(false);
            await setOnSubmitErrorMsg('');
          }, 2000);
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingSubmitDraft(false);
      await setOnSubmitErrorDraft(true);
      await setOnSubmitErrorMsgDraft('Failed to Save invitation!');
      await setTimeout(async () => {
        await setOnSubmitErrorDraft(false);
        await setOnSubmitErrorMsgDraft('');
      }, 2000);
    }
  };

  const capturingFrontCard = async () => {
    return new Promise(async resolve => {
      try {
        await setFlip(false);
        await frontRef?.current
          ?.capture()
          .then(async url => {
            console.log('URL Front Captured: ', url);
            resolve(`file://${url}`);
          })
          .catch(error => {
            console.log('Error: ', error);
            resolve(false);
          });
      } catch (error) {
        resolve(false);
      }
    });
  };

  const capturingBackCard = async () => {
    return new Promise(async resolve => {
      try {
        await setFlip(true);
        await setTimeout(async () => {
          await backRef?.current
            ?.capture()
            .then(async urls => {
              console.log('URL Back Captured: ', urls);
              await setFlip(true);
              resolve(`file://${urls}`);
            })
            .catch(error => {
              console.log('Error: ', error);
              resolve(false);
            });
        }, 2000);
      } catch (error) {
        resolve(false);
      }
    });
  };

  const onSubmit = async () => {
    try {
      console.log('Input: ', input);
      await setIsLoadingSubmit(true);
      const eventTimeValidation = await validateEventTimeEmptiness();
      console.log('eventTimeValidation >>>> ', eventTimeValidation);
      if (
        input?.groom === '' ||
        input?.bride === '' ||
        input?.date === '' ||
        input?.location === '' ||
        eventTimeValidation?.length === 0 ||
        eventTimeValidation?.length !== eventTime?.length ||
        selectedContactList?.length === 0
      ) {
        await setIsLoadingSubmit(false);
        await setOnSubmitError(true);
        await setOnSubmitErrorMsg(
          'Please fill in the form and the number of recipients',
        );
        await setTimeout(async () => {
          await setOnSubmitError(false);
          await setOnSubmitErrorMsg('');
        }, 2000);
      } else {
        // all filled
        const frontTierCaptuire = await capturingFrontCard();

        const backTierCapture = await capturingBackCard();

        console.log('SATTT: ', {
          frontTierCaptuire,
          backTierCapture,
        });

        const frontImageParams = new ReactNativeFile({
          uri: frontTierCaptuire,
          name: `${input?.groom}with${input?.bride}isfronttype.png`,
          type: 'image/png',
        });

        const backImageParams = new ReactNativeFile({
          uri: backTierCapture,
          name: `${input?.groom}with${input?.bride}isbacktype.png`,
          type: 'image/png',
        });

        const variables = {
          templateId: parseInt(templateData?.id, 10),
          brideName: input?.bride,
          groomName: input?.groom,
          weddingDate: moment(input?.date).format('DD MMMM YYYY'),
          eventTime: eventTime,
          location: input?.location,
          invitations: selectedContactList,
          textCollor: selectedColor,
          frontImage: frontImageParams,
          backImage: backImageParams,
        };
        console.log('variables ANUUU >>> ', variables);

        await client
          .mutate({
            mutation: createWeddingInvitation,
            variables,
          })
          .then(async res => {
            console.log('Res Submit Invitation: ', res);
            const {data, errors} = res;
            const {customerWeddingInvitation: cwi} = data;
            const {error} = cwi;

            if (errors) {
              await setIsLoadingSubmit(false);
              await setOnSubmitError(true);
              await setOnSubmitErrorMsg('Failed to send invitation!');
              await setTimeout(async () => {
                await setOnSubmitError(false);
                await setOnSubmitErrorMsg('');
              }, 2000);
            } else {
              if (error) {
                await setIsLoadingSubmit(false);
                await setOnSubmitError(true);
                await setOnSubmitErrorMsg('Failed to send invitation!');
                await setTimeout(async () => {
                  await setOnSubmitError(false);
                  await setOnSubmitErrorMsg('');
                }, 2000);
              } else {
                await setIsLoadingSubmit(false);
                await setOnSubmitError(false);
                await setOnSubmitErrorMsg('');
                await setOnSubmitSuccess(true);
                await setOnSubmitSuccessMsg('Invitation Sent!');
                await setTimeout(async () => {
                  await setOnSubmitError(false);
                  await setOnSubmitErrorMsg('');
                  await setOnSubmitSuccess(false);
                  await setOnSubmitSuccessMsg('');
                  await props?.backToList();
                }, 2000);
              }
            }
          })
          .catch(error => {
            throw error;
          });
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingSubmit(false);
      await setOnSubmitError(true);
      await setOnSubmitErrorMsg('Failed to send invitation!');
      await setTimeout(async () => {
        await setOnSubmitError(false);
        await setOnSubmitErrorMsg('');
      }, 2000);
    }
  };

  const keyExt = useCallback(
    (item, indexes) => {
      return `${indexes}`;
    },
    [eventTime],
  );

  const renderItem = useCallback(
    ({item, index: indexes}) => {
      return (
        <>
          <CardItem style={{paddingLeft: 15, paddingRight: 15}}>
            <View style={{marginRight: 10}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: mainRed,
                }}>
                Event Schedule #{indexes + 1}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                borderBottomWidth: 1,
                borderColor: mainRed,
              }}
            />
            {indexes === 0 ? null : (
              <TouchableOpacity
                onPress={() => removeEventTime(indexes)}
                style={{
                  width: 25,
                  height: 25,
                  marginLeft: 10,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: headerBorderBottom,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 2,
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{
                    left: Platform?.OS === 'android' ? 8 : 9,
                    fontSize: 15,
                    color: greyLine,
                  }}
                />
              </TouchableOpacity>
            )}
          </CardItem>
          <CardItem
            style={{
              paddingTop: 0,
              width: '100%',
              paddingLeft: 0,
              paddingRight: 0,
            }}>
            <View>
              <View style={{width: '100%'}}>
                <Form
                  isError={false}
                  required
                  value={item?.eventName}
                  onChangeText={async text => {
                    console.log('Text Event Name: ', text);
                    await onChangeEventTime(indexes, 'eventName', text);
                  }}
                  title={'Event Name'}
                  placeholder={'Event Name'}
                  multiline={false}
                  numberOfLines={1}
                  isLoading={false}
                />
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <DateAndTimePickupForm
                    title={'Start Time'}
                    placeholder={'Select time'}
                    disabled={false}
                    type={'Time'}
                    date={item?.startTime}
                    onChange={async time => {
                      console.log(
                        'onChangeTime Start Time: ',
                        moment(time).format('hh:mm:ss'),
                      );
                      await onChangeEventTime(indexes, 'startTime', time);
                    }}
                  />
                </View>
                <View style={{flex: 1}}>
                  <DateAndTimePickupForm
                    title={'End Time'}
                    placeholder={'Select time'}
                    disabled={false}
                    type={'Time'}
                    date={item?.endTime}
                    onChange={async time => {
                      console.log(
                        'onChangeTime End Time: ',
                        moment(time).format('hh:mm:ss'),
                      );
                      await onChangeEventTime(indexes, 'endTime', time);
                    }}
                  />
                </View>
              </View>
            </View>
          </CardItem>
        </>
      );
    },
    [eventTime],
  );

  const onChangeEventTime = async (i, alias, value) => {
    try {
      let oldEventTime = eventTime;
      let newArrayEventTime = oldEventTime[i];

      if (alias === 'eventName') {
        newArrayEventTime.eventName = value;
        await setEventTime([...oldEventTime]);
      } else if (alias === 'startTime') {
        newArrayEventTime.startTime = value;
        await setEventTime([...oldEventTime]);
      } else if (alias === 'endTime') {
        newArrayEventTime.endTime = value;
        await setEventTime([...oldEventTime]);
      } else {
        // Do Nothing
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const removeEventTime = async i => {
    try {
      if (i === 0) {
        // Do Nothing
      } else {
        let oldForm = eventTime;
        await oldForm.splice(i, 1);
        await setEventTime([...oldForm]);
      }
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const addNewEventTime = async () => {
    try {
      if (eventTime?.length >= 1 && eventTime?.length <= 5) {
        const newBlankForm = {
          eventName: '',
          startTime: '',
          endTime: '',
        };
        await setEventTime([...eventTime, newBlankForm]);
      } else {
        // Full
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const openContactSelectedModal = () => {
    try {
      setShowOpenContactListFromPhone(!showOpenContactListFromPhone);
    } catch (error) {
      console.log('Error Open Modal Selected Contact: ', error);
    }
  };

  return (
    <Container>
      <ModalColorPicker
        modalVisibility={modalVisibility}
        visible={showModalColorPicker}
        onSelectedColor={color => onSelectedColor(color)}
      />
      <ModalContactPhone
        selectedContactList={selectedContactList}
        visible={showOpenContactListFromPhone}
        openOrClosed={openContactSelectedModal}
        onSave={async contactSelected => {
          try {
            //
            await setSelectedContactList(contactSelected);
            await openContactSelectedModal();
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
      />

      {/* Modal First Load Contact */}
      <ModalLoadContact isLoadingContactOpen={isLoadingContactOpen} />

      {/* IF SUCCESS SUBMIT DRAFT */}
      {onSubmitSuccessDraft ? (
        <View
          style={{
            position: 'absolute',
            top: 5,
            left: 0,
            right: 0,
            zIndex: 99999,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#F4FAF1',
                padding: 10,
                maxWidth: width / 1.8,
                flexDirection: 'row',
                flexWrap: 'wrap',
                borderRadius: 25,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}>
              <View
                style={{
                  // flex: 0.1,
                  borderWidth: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={successGear}
                  style={{width: width / 25, height: width / 25}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  // flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 5,
                  paddingRight: 5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: mainGreen,
                  }}>
                  {onSubmitSuccessMsgDraft}
                </Text>
              </View>
            </View>
          </View>
        </View>
      ) : null}

      {/* IF ERROR AFTER SUBMIT DRAFT */}
      {onSubmitErrorDraft ? (
        <View
          style={{
            position: 'absolute',
            top: 5,
            left: 0,
            right: 0,
            zIndex: 99999,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#FFEDED',
                padding: 10,
                // maxWidth: width / 1.8,
                flexDirection: 'row',
                flexWrap: 'wrap',
                borderRadius: 25,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}>
              <View
                style={{
                  // flex: 0.1,
                  borderWidth: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={failedGear}
                  style={{width: width / 25, height: width / 25}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  // flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 5,
                  paddingRight: 5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: mainRed,
                  }}>
                  {onSubmitErrorMsgDraft}
                </Text>
              </View>
            </View>
          </View>
        </View>
      ) : null}

      {/* IF SUCCESS SUBMIT */}
      {onSubmitSuccess ? (
        <View
          style={{
            position: 'absolute',
            top: 5,
            left: 0,
            right: 0,
            zIndex: 99999,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#F4FAF1',
                padding: 10,
                maxWidth: width / 1.8,
                flexDirection: 'row',
                flexWrap: 'wrap',
                borderRadius: 25,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}>
              <View
                style={{
                  // flex: 0.1,
                  borderWidth: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={successGear}
                  style={{width: width / 25, height: width / 25}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  // flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 5,
                  paddingRight: 5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: mainGreen,
                  }}>
                  {onSubmitSuccessMsg}
                </Text>
              </View>
            </View>
          </View>
        </View>
      ) : null}

      {/* IF ERROR AFTER SUBMIT */}
      {onSubmitError ? (
        <View
          style={{
            position: 'absolute',
            top: 5,
            left: 0,
            right: 0,
            zIndex: 99999,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#FFEDED',
                padding: 10,
                // maxWidth: width / 1.8,
                flexDirection: 'row',
                flexWrap: 'wrap',
                borderRadius: 25,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
              }}>
              <View
                style={{
                  // flex: 0.1,
                  borderWidth: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={failedGear}
                  style={{width: width / 25, height: width / 25}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  // flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 5,
                  paddingRight: 5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: mainRed,
                  }}>
                  {onSubmitErrorMsg}
                </Text>
              </View>
            </View>
          </View>
        </View>
      ) : null}
      <Content contentContainerStyle={{paddingTop: 15, paddingBottom: 100}}>
        <Headers
          {...props}
          onSubmit={onSubmit}
          isLoadingSubmit={isLoadingSubmit}
        />

        <AlertContactList
          openModalListContact={openContactSelectedModal}
          contactList={selectedContactList}
          openPhoneContact={openModalListContact}
          isLoadingContactOpen={isLoadingContactOpen}
        />

        <InvitationcardFlip
          setIsLoadingSubmitDraft={setIsLoadingSubmitDraft}
          isLoadingSubmit={isLoadingSubmit}
          onFlip={onFlip}
          flip={flip}
          frontRef={frontRef}
          backRef={backRef}
          {...props}
          selectedColor={selectedColor}
          input={input}
          eventTime={eventTime}
          modalVisibility={modalVisibility}
        />

        <Card transparent style={{marginTop: 0, marginBottom: 0}}>
          <CardItem
            style={{
              width: '100%',
              justifyContent: 'flex-end',
              alignItems: 'center',
              paddingBottom: 0,
              paddingTop: 0,
            }}>
            <TouchableOpacity
              onPress={modalVisibility}
              style={{
                bottom: 15,
                right: 10,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
                zIndex: 99,
                backgroundColor: white,
                width: 45,
                height: 45,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="edit-2"
                style={{left: 10, color: mainRed, fontSize: 15}}
              />
              <Text
                style={{
                  top: 2,
                  color: mainRed,
                  fontSize: RFPercentage(1),
                  fontFamily: medium,
                }}>
                Colors
              </Text>
            </TouchableOpacity>
          </CardItem>
        </Card>

        <Form
          isError={false}
          required
          value={input?.groom}
          onChangeText={text => {
            console.log('Text: ', text);
            handleOnChange('groom', text);
          }}
          title={'Name'}
          placeholder={'Groom'}
          multiline={false}
          numberOfLines={1}
          isLoading={false}
        />

        <Form
          isError={false}
          required
          value={input?.bride}
          onChangeText={text => {
            console.log('Text: ', text);
            handleOnChange('bride', text);
          }}
          title={'Name'}
          placeholder={'Bride'}
          multiline={false}
          numberOfLines={1}
          isLoading={false}
        />

        <DateAndTimePickupForm
          title={'Date'}
          placeholder={'Select date'}
          disabled={false}
          type={'Date'}
          date={input?.date}
          onChange={async date => {
            console.log('onChangeDate: ', date);
            await handleOnChange('date', date);
          }}
        />

        <FlatList
          data={eventTime}
          extraData={eventTime}
          keyExtractor={keyExt}
          renderItem={renderItem}
          ListFooterComponent={() => {
            if (eventTime?.length === 5) {
              return null;
            } else {
              return (
                <CardItem
                  style={{paddingLeft: 10, paddingRight: 10, paddingTop: 0}}>
                  <TouchableOpacity
                    onPress={addNewEventTime}
                    style={{
                      flexDirection: 'row',
                      padding: 5,
                      paddingLeft: 10,
                      paddingRight: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        borderRadius: 3,
                        marginRight: 10,
                        width: 20,
                        height: 20,
                        borderWidth: 1,
                        borderColor: mainRed,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon
                        type="Feather"
                        name="plus"
                        style={{left: 8.5, color: mainRed, fontSize: 15}}
                      />
                    </View>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      Add more schedule
                    </Text>
                  </TouchableOpacity>
                </CardItem>
              );
            }
          }}
        />

        <Form
          isError={false}
          required
          value={input?.location}
          onChangeText={text => {
            console.log('Text: ', text);
            handleOnChange('location', text);
          }}
          title={'Location'}
          placeholder={'Location'}
          multiline={true}
          numberOfLines={3}
          isLoading={false}
        />

        {/* Button */}
        <CardItem>
          <TouchableOpacity
            disabled={isLoadingSubmitDraft ? true : false}
            onPress={saveToDraftTemplate}
            style={{
              width: '100%',
              borderWidth: 1,
              borderColor: mainGreen,
              borderRadius: 4,
              padding: 12,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {isLoadingSubmitDraft ? (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: mainGreen,
                  letterSpacing: 0.3,
                }}>
                SAVING...
              </Text>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: mainGreen,
                  letterSpacing: 0.3,
                }}>
                SAVE
              </Text>
            )}
          </TouchableOpacity>
        </CardItem>
        <CardItem>
          <TouchableOpacity
            disabled={isLoadingSubmit ? true : false}
            onPress={onSubmit}
            style={{
              backgroundColor: mainGreen,
              width: '100%',
              borderWidth: 1,
              borderColor: mainGreen,
              borderRadius: 4,
              padding: 12,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {isLoadingSubmit ? (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                SENDING...
              </Text>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                SEND
              </Text>
            )}
          </TouchableOpacity>
        </CardItem>
      </Content>
    </Container>
  );
};

export const ModalLoadContact = props => {
  const {isLoadingContactOpen} = props;

  return (
    <Modal visible={isLoadingContactOpen} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 35,
          paddingRight: 35,
        }}>
        <View
          style={{
            backgroundColor: white,
            borderRadius: 5,
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginTop: 10}}
          />
          <View
            style={{
              paddingLeft: 25,
              paddingRight: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                marginVertical: 10,
                textAlign: 'center',
                lineHeight: 20,
              }}>
              Plase wait , we are scanning your contact and it might take
              several minutes
            </Text>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const InvitationcardFlip = props => {
  console.log('FLIP VALUE: ', props?.flip);
  const {
    selectedColor,
    input,
    modalVisibility,
    eventTime,
    frontRef,
    backRef,
    flip,
    onFlip,
    isLoadingSubmit,
    setIsLoadingSubmitDraft,
  } = props;

  return (
    <FlipCard
      style={{
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      useNativeDriver={true}
      friction={6000}
      perspective={1000}
      flipHorizontal={true}
      flipVertical={false}
      flip={flip}
      clickable={true}>
      <ViewShot ref={frontRef}>
        <TouchableOpacity onPress={onFlip}>
          <TemplateCanvas
            {...props}
            setIsLoadingSubmitDraft={setIsLoadingSubmitDraft}
            isLoadingSubmit={isLoadingSubmit}
            eventTime={eventTime}
            selectedColor={selectedColor}
            input={input}
            modalVisibility={modalVisibility}
          />
        </TouchableOpacity>
      </ViewShot>
      <ViewShot ref={backRef}>
        <TouchableOpacity onPress={onFlip}>
          <TemplateCanvasBackSide
            {...props}
            eventTime={eventTime}
            input={input}
            selectedColor={selectedColor}
          />
        </TouchableOpacity>
      </ViewShot>
    </FlipCard>
  );
};

export const AlertContactList = props => {
  const {
    contactList,
    openPhoneContact,
    isLoadingContactOpen,
    openModalListContact,
  } = props;

  if (contactList?.length === 0) {
    return (
      <Card transparent style={{paddingLeft: 20, paddingRight: 20}}>
        <CardItem
          button
          onPress={openPhoneContact}
          style={{
            backgroundColor: '#FFEDED',
            borderWidth: 1,
            borderRadius: 4,
            borderColor: mainRed,
          }}>
          <View style={{flex: 1}}>
            <Text
              style={{
                textAlign: 'left',
                fontStyle: 'italic',
                fontWeight: '500',
                fontSize: RFPercentage(1.7),
                color: mainRed,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {
                "It's seems you haven't add contact list to send your invitation card !"
              }
            </Text>
          </View>
          <TouchableOpacity
            onPress={openPhoneContact}
            style={{
              flex: 0.2,
              borderWidth: 0,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            {isLoadingContactOpen ? (
              <ActivityIndicator size="small" color={mainRed} />
            ) : (
              <Icon
                type="Feather"
                name="arrow-right"
                style={{left: 10, fontSize: RFPercentage(2.1), color: mainRed}}
              />
            )}
          </TouchableOpacity>
        </CardItem>
      </Card>
    );
  } else {
    const invalidContact = contactList?.filter(element => {
      return element?.valid === false;
    });
    return (
      <Card transparent style={{paddingLeft: 20, paddingRight: 20}}>
        <CardItem
          style={{
            backgroundColor: '#F4FAF1',
            borderWidth: 1,
            borderRadius: 4,
            borderColor: mainGreen,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                textAlign: 'left',
                fontStyle: 'italic',
                fontWeight: '400',
                fontSize: RFPercentage(1.7),
                color: mainGreen,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {'Selected Contact(s): '}
              <Text
                style={{fontWeight: '700'}}>{`${contactList?.length}`}</Text>
            </Text>
            {invalidContact?.length === 0 ? null : (
              <View
                style={{
                  marginTop: 10,
                  alignSelf: 'flex-start',
                  backgroundColor: mainRed,
                  borderRadius: 5,
                  padding: 5,
                }}>
                <Text
                  style={{
                    textAlign: 'left',
                    fontStyle: 'italic',
                    fontWeight: '400',
                    fontSize: RFPercentage(1.7),
                    color: white,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  Invalid Contact(s): {invalidContact?.length}
                </Text>
              </View>
            )}
          </View>
          <View
            style={{
              flex: 0.4,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                openModalListContact();
              }}
              style={{
                padding: 5,
                flex: 0.2,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="edit"
                style={{
                  left: 8,
                  fontSize: RFPercentage(2.1),
                  color: mainGreen,
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                // open contact native
                openPhoneContact();
              }}
              style={{
                padding: 5,
                left: 10,
                flex: 0.2,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {isLoadingContactOpen ? (
                <ActivityIndicator size="small" color={mainRed} />
              ) : (
                <Icon
                  type="Feather"
                  name="plus"
                  style={{
                    left: 8,
                    fontSize: RFPercentage(2.1),
                    color: mainGreen,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
        </CardItem>
      </Card>
    );
  }
};

export const ModalColorPicker = props => {
  const {visible, onSelectedColor, modalVisibility} = props;

  const listColor = [
    {
      colorName: 'White',
      colorCode: white,
    },
    {
      colorName: 'Black',
      colorCode: black,
    },
    {
      colorName: 'Red',
      colorCode: mainRed,
    },
    {
      colorName: 'Green',
      colorCode: mainGreen,
    },
  ];

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1, backgroundColor: overlayDim}}>
        <TouchableOpacity onPress={modalVisibility} style={{flex: 1}} />
        <View style={{backgroundColor: white}}>
          <Card transparent>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    textDecorationLine: 'underline',
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                  }}>
                  List Colors
                </Text>
                <TouchableOpacity
                  onPress={modalVisibility}
                  style={{padding: 5}}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{left: 10, fontSize: RFPercentage(3), color: black}}
                  />
                </TouchableOpacity>
              </View>
            </CardItem>
            <FlatList
              contentContainerStyle={{paddingBottom: 55, paddingTop: 15}}
              data={listColor}
              keyExtractor={item => `${item.colorName}`}
              renderItem={({item}) => {
                return (
                  <CardItem
                    button
                    onPress={() => {
                      onSelectedColor(item?.colorCode);
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text>{item?.colorName}</Text>
                      <View
                        style={{
                          borderWidth: 1,
                          borderColor: greyLine,
                          width: 30,
                          height: 30,
                          borderRadius: 30 / 2,
                          backgroundColor: item?.colorCode,
                        }}
                      />
                    </View>
                  </CardItem>
                );
              }}
            />
          </Card>
        </View>
      </View>
    </Modal>
  );
};

export const TemplateCanvasBackSide = props => {
  const {templateData, selectedColor, eventTime} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [eventTime],
  );

  const renderItem = useCallback(
    ({item}) => {
      return (
        <Card transparent style={{marginTop: 0, marginBottom: 0}}>
          <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  marginBottom: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: BaskervilleBold,
                    fontSize: RFPercentage(1.8),
                    color: selectedColor,
                    letterSpacing: 0.3,
                  }}>
                  {!item?.eventName || item?.eventName === ''
                    ? 'N/A'
                    : item?.eventName}
                </Text>
              </View>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text
                  style={{
                    marginBottom: 5,
                    fontFamily: Baskerville,
                    fontSize: RFPercentage(1.5),
                    color: selectedColor,
                    letterSpacing: 0.3,
                  }}>
                  {item?.startTime
                    ? moment(item?.startTime).format('hh:mm A')
                    : 'N/A'}{' '}
                  -{' '}
                  {item?.endTime
                    ? moment(item?.endTime).format('hh:mm A')
                    : 'N/A'}
                </Text>
              </View>
            </View>
          </CardItem>
        </Card>
      );
    },
    [eventTime],
  );

  return (
    <View style={{width: width, height: width * 1.3, padding: 25}}>
      <ImageBackground
        resizeMode="cover"
        source={
          templateData?.dynamicUrl
            ? {uri: `${templateData?.dynamicUrl}=nu`}
            : {uri: `${templateData?.url}`}
        }
        style={{
          flex: 1,
        }}>
        <FlatList
          disableVirtualization
          legacyImplementation
          ListHeaderComponent={() => {
            return (
              <Card transparent>
                <CardItem
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                  }}>
                  <Text
                    style={{
                      fontFamily: BaskervilleBold,
                      fontSize: RFPercentage(2),
                      color: selectedColor,
                      letterSpacing: 0.3,
                    }}>
                    Event Schedules
                  </Text>
                </CardItem>
              </Card>
            );
          }}
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          keyExtractor={keyExt}
          renderItem={renderItem}
          data={eventTime?.length === 0 ? [] : eventTime}
          extraData={eventTime?.length === 0 ? [] : eventTime}
        />
      </ImageBackground>
    </View>
  );
};

export const TemplateCanvas = props => {
  const {
    templateData,
    input,
    modalVisibility,
    selectedColor,
    isLoadingSubmit,
    setIsLoadingSubmitDraft,
  } = props;

  return (
    <View style={{width: width, height: width * 1.3, padding: 25}}>
      <ImageBackground
        resizeMode="cover"
        source={
          templateData?.dynamicUrl
            ? {uri: `${templateData?.dynamicUrl}=nu`}
            : {uri: `${templateData?.url}`}
        }
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {/* <TouchableOpacity
          onPress={modalVisibility}
          style={{
            display:
              isLoadingSubmit || setIsLoadingSubmitDraft ? 'none' : 'flex',
            zIndex: 99,
            backgroundColor: white,
            position: 'absolute',
            top: 10,
            right: 10,
            width: 45,
            height: 45,
            borderRadius: 45 / 2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="edit-2"
            style={{color: mainRed, fontSize: 15}}
          />
          <Text
            style={{
              top: 2,
              color: mainRed,
              fontSize: RFPercentage(1),
              fontFamily: medium,
            }}>
            Colors
          </Text>
        </TouchableOpacity> */}

        {/* Greetings */}
        <View
          style={{
            maxWidth: width / 2.5,
            minWidth: width / 2.5,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 0,
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontFamily: Baskerville,
              fontSize: RFPercentage(1.8),
              textAlign: 'center',
              // lineHeight: 18,
              color: selectedColor,
            }}>
            Together In Love
          </Text>
        </View>

        {/* Groom Name */}
        <View
          style={{
            maxWidth: width / 2.5,
            minWidth: width / 2.5,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: input?.groom === '' ? 1 : 0,
          }}>
          <Text
            style={{
              fontFamily: Baskerville,
              fontSize: RFPercentage(2.3),
              fontWeight: '400',
              textAlign: 'center',
              color: selectedColor,
            }}>
            {input?.groom === '' || !input?.groom ? 'N/A' : input?.groom}
          </Text>
        </View>

        {/* & */}
        <Text
          style={{
            fontFamily: Baskerville,
            fontSize: RFPercentage(3),
            fontWeight: '300',
            color: selectedColor,
          }}>
          &
        </Text>

        {/* Bride Name */}
        <View
          style={{
            maxWidth: width / 2.5,
            minWidth: width / 2.5,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: input?.bride === '' ? 1 : 0,
            marginBottom: 15,
          }}>
          <Text
            style={{
              fontFamily: Baskerville,
              fontSize: RFPercentage(2.3),
              fontWeight: '400',
              textAlign: 'center',
              color: selectedColor,
            }}>
            {input?.bride === '' || !input?.bride ? 'N/A' : input?.bride}
          </Text>
        </View>

        {/* Date */}
        <View
          style={{
            maxWidth: width / 2.5,
            minWidth: width / 2.5,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: input?.date === '' ? 1 : 0,
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontFamily: BaskervilleBold,
              fontSize: RFPercentage(1.7),
              fontWeight: '600',
              textAlign: 'center',
              color: selectedColor,
            }}>
            {input?.date === '' ? 'DD' : moment(input?.date).format('DD')}
          </Text>
          <Text
            style={{
              fontFamily: BaskervilleBold,
              fontSize: RFPercentage(1.7),
              fontWeight: '600',
              textAlign: 'center',
              // lineHeight: 18,
              color: selectedColor,
              marginHorizontal: 10,
            }}>
            {input?.date === '' ? 'MMMM' : moment(input?.date).format('MMMM')}
          </Text>
          <Text
            style={{
              fontFamily: BaskervilleBold,
              fontSize: RFPercentage(1.7),
              fontWeight: '600',
              textAlign: 'center',
              color: selectedColor,
            }}>
            {input?.date === '' ? 'YYYY' : moment(input?.date).format('YYYY')}
          </Text>
        </View>

        {/* Location */}
        <View
          style={{
            maxWidth: width / 2,
            minWidth: width / 2,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: input?.location === '' ? 1 : 0,
          }}>
          <Text
            style={{
              fontFamily: Baskerville,
              fontSize: RFPercentage(1.7),
              textAlign: 'center',
              color: selectedColor,
            }}>
            {input?.location === '' || !input?.location
              ? 'N/A'
              : input?.location}
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export const Headers = props => {
  console.log('Headers Single Form: ', props);
  const {backToList, onSubmit, isLoadingSubmit} = props;
  return (
    <Card transparent>
      <CardItem>
        <Left style={{flex: 0.3, borderWidth: 0}}>
          <TouchableOpacity onPress={backToList}>
            <Icon
              type="Feather"
              name="arrow-left"
              style={{color: 'black', fontSize: 24}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
              textAlign: 'center',
            }}>
            {props?.templateData?.name
              ? props?.templateData?.name
              : 'Invitation Card'}
          </Text>
        </Body>
        <Right
          style={{
            flex: 0.3,
            borderWidth: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            disabled={true}
            onPress={onSubmit}
            style={{
              display: 'none',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {isLoadingSubmit ? (
              <ActivityIndicator
                size="small"
                color={mainGreen}
                style={{marginBottom: 5}}
              />
            ) : (
              <Icon
                type="Feather"
                name="send"
                style={{color: 'black', fontSize: 24, marginBottom: 5}}
              />
            )}
            <Text
              style={{
                color: black,
                fontSize: RFPercentage(1.3),
                fontFamily: medium,
                letterSpacing: 0.3,
              }}>
              {isLoadingSubmit ? 'Sending...' : 'Send'}
            </Text>
          </TouchableOpacity>
        </Right>
      </CardItem>
    </Card>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(SingleForm);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
