import React from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {greyLine, white} = Colors;
const {medium} = FontType;

const Headers = props => {
  const goToDraft = () => {
    // go to draft
    try {
      props?.navigation?.navigate('Draft');
    } catch (error) {
      console.log('Error to draft: ', error);
    }
  };

  const goToCustomCanvas = () => {
    try {
      props?.navigation?.navigate('EditorWithTemplate', {
        templateId: null,
        url: null,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        backgroundColor: white,
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0,
      }}>
      <View
        style={{
          flex: 0.3,
          borderWidth: 0,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 0,
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: greyLine,
            fontSize: RFPercentage(1.6),
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          Select Your Template
        </Text>
      </View>
      <View
        style={{
          flex: 0.3,
          borderWidth: 0,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Button onPress={goToDraft} iconName={'bookmark'} />
        <Button onPress={goToCustomCanvas} iconName={'plus'} />
      </View>
    </Header>
  );
};

export const Button = props => {
  const {onPress, iconName} = props;
  return (
    <TouchableOpacity onPress={onPress} style={{padding: 10, borderWidth: 0}}>
      <Icon
        type="Feather"
        name={iconName}
        style={{color: greyLine, fontSize: RFPercentage(2.5)}}
      />
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(Headers);

export default props => <Wrapper {...props} />;
