import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import _ from 'lodash';
import moment from 'moment';
import AsyncImage from '../../../components/Image/AsyncImage';
import {Icon} from 'native-base';

const {
  white,
  black,
  mainRed,
  mainGreen,
  headerBorderBottom,
  greyLine,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions?.get('screen');

import EmptyScreen from '../Components/EmptyScreen';

// Query
import GetListRSVP from '../../../graphql/queries/getListRSVPV2';
import getGroupContact from '../../../graphql/queries/getGroupContact';

// GQL Mutation
import DELETE_INVITATION from '../../../graphql/mutations/deleteInvitationByTemplateId';

const ListRSVP = props => {
  const {positionYBottomNav} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);
  const [totalInvited, setTotalInvited] = useState(0);
  const [totalAttend, setTotalAttend] = useState(0);

  const [loadMore, setLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [openModalFilter, setOpenModalFilter] = useState(false);
  const [selectedFilter, setSelectedFilter] = useState(null);

  const [selectedDraftIdToDelete, setSelectedDraftIdToDelete] = useState(null);
  const [openModalConfirmDelete, setOpenModalConfirmDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  useEffect(() => {
    onChangeOpacity(false);
    fetch();

    if (refreshing) {
      onRefresh();
    }

    if (loadMore) {
      fetchMore();
    }

    if (selectedFilter) {
      console.log('ANU BAZENG UEfee: ', selectedFilter);
      refetchAfterFilter();
    }

    if (isLoadingDelete) {
      deleteInvitation();
    }

    const tabPress = props?.navigation?.addListener('tabPress', () => {
      onChangeOpacity(false);
    });
    const onFocus = props?.navigation?.addListener('focus', () => {
      onChangeOpacity(false);
    });

    return () => {
      tabPress;
      onFocus;
      refetchAfterFilter;
    };
  }, [refreshing, loadMore, selectedFilter, isLoadingDelete]);

  const deleteInvitation = useCallback(() => {
    try {
      props?.client
        ?.mutate({
          mutation: DELETE_INVITATION,
          variables: {
            templateId: String(selectedDraftIdToDelete),
          },
        })
        .then(async res => {
          const {data, errors} = res;
          const {deleteInvitationByTemplate} = data;
          const {error} = deleteInvitationByTemplate;

          if (errors || error) {
            await setSelectedDraftIdToDelete(null);
            await setIsLoadingDelete(false);
            setTimeout(async () => {
              await setOpenModalConfirmDelete(false);
            }, 1000);
          } else {
            // success delete;
            await setSelectedDraftIdToDelete(null);
            await setIsLoadingDelete(false);
            setTimeout(async () => {
              await setOpenModalConfirmDelete(false);
              await setList([]);
              await setRefreshing(true);
            }, 1000);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {}
  }, [isLoadingDelete]);

  const refetchAfterFilter = useCallback(async () => {
    try {
      console.log('REFETCH AFTER FI:TER: ', selectedFilter);
      await props?.client
        ?.query({
          query: GetListRSVP,
          variables: {
            group: selectedFilter?.id ? selectedFilter?.id : null,
            itemDisplayed,
            pageNumber: 1,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async res => {
          console.log('Res fetch RSVP List: ', res);
          const {data, errors} = res;
          const {getInvitationSummaryByTemplateV2} = data;
          const {
            data: result,
            error,
            totalCount,
            totalInvitation,
            totalAttendance,
          } = getInvitationSummaryByTemplateV2;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setLoadMore(false);
            await setRefreshing(false);
            await setOpenModalFilter(false);
          } else {
            await setTotalData(totalCount);
            await setTotalInvited(totalInvitation);
            await setTotalAttend(totalAttendance);
            let oldData = [...result];
            const removeDuplicatedData = await _.unionBy(oldData, 'id');

            await setList(
              pageNumber === 1 ? [...result] : [...removeDuplicatedData],
            );
            await setIsError(false);
            await setIsLoading(false);
            await setLoadMore(false);
            await setRefreshing(false);
            await setOpenModalFilter(false);
          }
        })
        .catch(async error => {
          console.log('Error fetch rsvp list: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setLoadMore(false);
          await setRefreshing(false);
          await setOpenModalFilter(false);
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setLoadMore(false);
      await setRefreshing(false);
      await setOpenModalFilter(false);
      await setSelectedFilter(null);
      await setOpenModalFilter(false);
    }
  }, [selectedFilter]);

  const fetchMore = useCallback(async () => {
    try {
      await setPageNumber(pageNumber + 1);
      await fetch();
    } catch (error) {
      await setLoadMore(false);
    }
  }, [loadMore]);

  const onRefresh = useCallback(() => {
    setPageNumber(1);
    fetch();
  }, [refreshing]);

  const fetch = async () => {
    try {
      console.log('ANU KONTOL: ', selectedFilter?.id);
      props?.client
        ?.query({
          query: GetListRSVP,
          variables: {
            group: selectedFilter?.id ? selectedFilter?.id : null,
            itemDisplayed,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async res => {
          console.log('Res fetch RSVP List: ', res);
          const {data, errors} = res;
          const {getInvitationSummaryByTemplateV2} = data;
          const {
            data: result,
            error,
            totalCount,
            totalInvitation,
            totalAttendance,
          } = getInvitationSummaryByTemplateV2;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setLoadMore(false);
            await setRefreshing(false);
            await setOpenModalFilter(false);
          } else {
            await setTotalData(totalCount);
            await setTotalInvited(totalInvitation);
            await setTotalAttend(totalAttendance);
            let oldData = [...list, ...result];
            const removeDuplicatedData = await _.unionBy(oldData, 'id');

            await setList([...removeDuplicatedData]);
            await setIsError(false);
            await setIsLoading(false);
            await setLoadMore(false);
            await setRefreshing(false);
            await setOpenModalFilter(false);
          }
        })
        .catch(async error => {
          console.log('Error fetch rsvp list: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setLoadMore(false);
          await setRefreshing(false);
          await setOpenModalFilter(false);
        });
    } catch (error) {
      console.log('Error fetch rsvp list: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setLoadMore(false);
      await setRefreshing(false);
      await setOpenModalFilter(false);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1, alignItems: 'center', paddingTop: 15}}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }>
            <View style={{width, height: width * 1.7}}>
              <EmptyScreen tabName="myrsvp" />
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: white}}>
          <ModalConfirmDelete
            isLoadingDelete={isLoadingDelete}
            confirm={async () => {
              try {
                console.log('Draft ID: ', selectedDraftIdToDelete);
                if (selectedDraftIdToDelete) {
                  console.log('Masuk SINI: ', selectedDraftIdToDelete);
                  await setIsLoadingDelete(true);
                } else {
                  console.log('Masuk Kontol');
                  await setSelectedDraftIdToDelete(null);
                  await setOpenModalConfirmDelete(false);
                }
              } catch (error) {
                console.log('Error delete draft: ', error);
                await setSelectedDraftIdToDelete(null);
                await setOpenModalConfirmDelete(false);
              }
            }}
            visible={openModalConfirmDelete}
            cancel={async () => {
              await setSelectedDraftIdToDelete(null);
              await setOpenModalConfirmDelete(false);
            }}
          />
          <List
            onDelete={async id => {
              console.log('ID Delete Invitation: ', id);
              try {
                try {
                  await setSelectedDraftIdToDelete(String(id));
                  await setOpenModalConfirmDelete(true);
                } catch (error) {
                  console.log('Error: ', error);
                }
              } catch (error) {
                console.log('Error Delete On Delete: ', error)
              }
            }}
            {...props}
            loadMore={loadMore}
            setLoadMore={e => setLoadMore(e)}
            refreshing={refreshing}
            onRefresh={() => setRefreshing(true)}
            totalData={totalData}
            totalInvitation={totalInvited}
            totalAttendance={totalAttend}
            list={list}
            visible={openModalFilter}
            onOpenModalFilter={() => setOpenModalFilter(true)}
            onCloseModalFilter={() => setOpenModalFilter(false)}
            onSelectFilter={async item => {
              console.log('BAZENGAN FILTER: ', item);
              await setSelectedFilter({...item});
            }}
            selectedFilter={selectedFilter}
            onDeleteFilter={() => setSelectedFilter(null)}
          />
        </View>
      </SafeAreaView>
    );
  }
};

export const ModalConfirmDelete = props => {
  console.log('ModalConfirmDelete props: ', props);
  const {cancel, confirm, visible, isLoadingDelete} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
          paddingLeft: 45,
          paddingRight: 45,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: white,
            borderRadius: 5,
            paddingTop: 10,
            paddingBottom: 10,
          }}>
          <View
            style={{
              width: '100%',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 15,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainGreen,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Are you sure want to delete this Invitation ?
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={cancel}
              style={{
                padding: 10,
                borderWidth: 0,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={confirm}
              style={{
                padding: 10,
                borderWidth: 0,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainGreen,
                }}>
                {isLoadingDelete ? 'Deleting...' : 'Yes, sure!'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ModalGroupContact = props => {
  console.log('ModalGroupContact Props: ', props);
  const {visible, onClose, client, onSelect, selectedGroup} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (visible) {
      fetch();
    }
  }, [visible]);

  const fetch = () => {
    try {
      client
        ?.query({
          query: getGroupContact,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Group Contact: ', res);
          const {data, errors} = res;
          const {rsvpGroups} = data;
          const {data: result, error} = rsvpGroups;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setList([...result]);
            await setTimeout(async () => {
              await setIsError(false);
              await setIsLoading(false);
            }, 1000);
          }
        })
        .catch(error => {
          console.log('Error fetch Group: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Fetch Group: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list, selectedGroup],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity
          onPress={() => onSelect(item, index)}
          style={{
            width: '100%',
            padding: 25,
            paddingLeft: 5,
            paddingRight: 5,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: selectedGroup?.id === item?.id ? mainRed : black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
        </TouchableOpacity>
      );
    },
    [list, selectedGroup],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Group
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {}}
                style={{padding: 5, display: 'none'}}>
                <Icon
                  type="Feather"
                  name="save"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* List Groups */}
          {isLoading ? (
            <View style={{width: '100%'}}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{alignSelf: 'center'}}
              />
            </View>
          ) : (
            <FlatList
              style={{bottom: 10}}
              contentContainerStyle={{padding: 15}}
              data={list}
              extraData={list}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

export const List = props => {
  console.log('LIST KONTOL FILTERNAME: ', props?.selectedFilter);
  const {
    list,
    loadMore,
    setLoadMore,
    refreshing,
    onRefresh,
    totalAttendance,
    totalInvitation,
    totalData,
    visible,
    onOpenModalFilter,
    onCloseModalFilter,
    onSelectFilter,
    selectedFilter,
    onDeleteFilter,
  } = props;

  const navigateToDetail = (item, index) => {
    try {
      props?.navigation?.navigate('RSVPDetail', {
        id: item?.id,
        templateId: item?.templateId,
      });
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list, selectedFilter],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <View style={{flex: 1 / 2, borderWidth: 0, padding: 10}}>
          <TouchableOpacity
            onPress={() => navigateToDetail(item, index)}
            style={{
              width: '100%',
              height: width * 0.6,
              borderWidth: 0,
              backgroundColor: headerBorderBottom,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              borderRadius: 3,
            }}>
            <TouchableOpacity
              onPress={() => {
                props?.onDelete(item?.templateId);
              }}
              style={{
                backgroundColor: white,
                padding: 7,
                position: 'absolute',
                top: 10,
                right: 10,
                zIndex: 99,
                borderRadius: 50,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
              }}>
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: RFPercentage(2), color: greyLine}}
              />
            </TouchableOpacity>
            <AsyncImage
              style={{
                borderRadius: 3,
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
              }}
              source={{
                uri: item?.imageUrl ? item?.imageUrl : item?.canvasImgUrl,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    },
    [list, selectedFilter],
  );

  const ListHeaderComponent = () => {
    return (
      <View
        style={{width: '100%', padding: 10, paddingLeft: 15, paddingRight: 15}}>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: greyLine,
              letterSpacing: 0.3,
              marginBottom: 10,
            }}>
            Sent Invitation
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => onOpenModalFilter()}
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              padding: 7,
              paddingLeft: 15,
              paddingRight: 15,
              borderWidth: 1,
              borderColor: mainRed,
              borderRadius: 25,
              alignItems: 'center',
              marginRight: 10,
            }}>
            <Icon
              type="Feather"
              name="filter"
              style={{fontSize: RFPercentage(2.2), color: mainRed}}
            />
            <Text
              style={{
                marginLeft: 10,
                fontSize: RFPercentage(1.7),
                color: mainRed,
                fontFamily: medium,
              }}>
              Filter
            </Text>
          </TouchableOpacity>
          {selectedFilter?.name ? (
            <TouchableOpacity
              onPress={onDeleteFilter}
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                padding: 7,
                paddingLeft: 15,
                paddingRight: 15,
                borderWidth: 1,
                borderColor: mainRed,
                borderRadius: 25,
                alignItems: 'center',
                backgroundColor: mainRed,
              }}>
              <Text
                style={{
                  marginRight: 10,
                  fontSize: RFPercentage(1.7),
                  color: white,
                  fontFamily: medium,
                }}>
                {selectedFilter?.name ? selectedFilter?.name : ''}
              </Text>
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: RFPercentage(2.2), color: white}}
              />
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  };

  const FooterList = () => {
    return (
      <View
        style={{
          width: '100%',
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>{loadMore ? 'Load more...' : ''}</Text>
      </View>
    );
  };

  return (
    <>
      {visible ? (
        <ModalGroupContact
          client={props?.client}
          visible={visible}
          onClose={() => {
            onCloseModalFilter();
          }}
          onSelect={async (itm, idx) => {
            try {
              onSelectFilter({...itm});
            } catch (error) {
              console.log('Error: ', error);
              onCloseModalFilter();
            }
          }}
        />
      ) : null}
      <FlatList
        onEndReached={() => {
          if (list?.length < totalData) {
            setLoadMore(true);
          } else {
            setLoadMore(false);
          }
        }}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
        ListHeaderComponent={ListHeaderComponent}
        initialNumToRender={10}
        numColumns={2}
        data={list}
        extraData={list}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListFooterComponent={FooterList}
        ListEmptyComponent={() => {
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={{flex: 1, backgroundColor: white}}>
                <View style={{width, height: width * 1.4}}>
                  <EmptyScreen tabName="myrsvp" />
                </View>
              </View>
            </SafeAreaView>
          );
        }}
      />
    </>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(ListRSVP);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
