import React, {useEffect, useState, useCallback} from 'react';
import {
  Text,
  Alert,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Linking,
  Modal,
  Animated,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import FlipCard from 'react-native-flip-card';
import {Container, Content, Card, CardItem, Icon} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';
import {connect} from 'react-redux';

const {
  white,
  black,
  greyLine,
  mainRed,
  mainGreen,
  overlayDim,
  headerBorderBottom,
} = Colors;
const {book, medium, italiano, Baskerville, BaskervilleBold} = FontType;
const {width, height} = Dimensions.get('window');

// Query
import GET_INVITATION_BY_ID from '../../../graphql/queries/invitationByUniqId';
import getGroupContact from '../../../graphql/queries/getGroupContact';

// Mutation
import submitRSVP from '../../../graphql/mutations/invitationRSVPV2';

// Components
import Form from '../../../components/FormInput/FormInput';
import EmptyScreen from '../Components/EmptyScreen';

import {tableIcon} from '../../../utils/Themes/Images';

const {boxTable, circleTable, retangleTable} = tableIcon;

const MyInvitation = props => {
  console.log('MyInvitation Screen Props: ', props);
  const {navigation, client, route, positionYBottomNav} = props;

  const [invitationDetail, setInvitationDetail] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [isRSVP, setIsRSVP] = useState(false);
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [isErrorSubmit, setIsErrorSubmit] = useState(false);
  const [isSuccessSubmit, setIsSuccessSubmit] = useState(false);

  // table list
  const [selectedTable, setSelectedTable] = useState(null);

  // show Modal Group List
  const [showModalGroup, setShowModalGroup] = useState(false);
  const [selectedID, setSelectedID] = useState(null);

  // Show Modal Schedules
  const [showModalSchedule, setShowModalSchedule] = useState(false);

  const [inputs, setInputs] = useState([
    {
      name: '',
      phoneNo: '',
      email: '',
      rsvpStatus: true,
      vaccinatedSwab: true,
      gender: '',
      paxType: '',
      group: null,
      note: '',
    },
  ]);

  useEffect(() => {
    onChangeOpacity(false);
    validationToken();
    const tabPress = props?.navigation?.addListener('tabPress', () => {
      onChangeOpacity(false);
    });
    const onFocus = props?.navigation?.addListener('focus', () => {
      onChangeOpacity(false);
    });

    return () => {
      tabPress;
      onFocus;
    };
  }, [route?.params?.invitationToken]);

  const manipulateIDGroup = itm => {
    console.log('MANIPULATE ID GROUP: ', {itm, selectedID});
    let oldArr = inputs;
    oldArr[selectedID].group = itm;
    setInputs([...oldArr]);
    setSelectedID(null);
    setShowModalGroup(false);
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const validationToken = async () => {
    try {
      if (route?.params?.invitationToken) {
        await setIsLoading(true);
        await setIsError(false);
        await fetch();
      } else {
        await setIsError(true);
        await setIsLoading(false);
      }
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const addNewForm = async () => {
    try {
      const newBlankForm = {
        name: '',
        phoneNo: '',
        email: '',
        rsvpStatus: true,
        vaccinatedSwab: true,
        gender: '',
        paxType: '',
        group: null,
        note: '',
      };
      await setInputs([...inputs, newBlankForm]);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const removeForm = async index => {
    try {
      if (index === 0) {
        // Do Nothing (Prevent Initatiate State Removed)
      } else {
        console.log('index Remove Form: ', index);
        let oldForm = inputs;
        await oldForm.splice(index, 1);
        console.log('OLD FORM: ', oldForm);
        await setInputs([...oldForm]);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetch = async () => {
    try {
      if (route?.params?.invitationToken) {
        // there is token
        client
          .query({
            query: GET_INVITATION_BY_ID,
            variables: {
              uniqueId: String(route?.params?.invitationToken),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('Response: ', response);
            const {data, errors} = response;
            const {invitationByUniqueId} = data;
            const {data: detail, error} = invitationByUniqueId;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
              } else {
                if (detail) {
                  await setInvitationDetail(detail);
                  await setIsError(false);
                  await setIsLoading(false);
                } else {
                  await setIsError(true);
                  await setIsLoading(false);
                }
              }
            }
          })
          .catch(async error => {
            console.log('error Query mamen: ', error);
            await setIsError(true);
            await setIsLoading(false);
          });
      } else {
        //
        await setIsError(true);
        await setIsLoading(false);
      }
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const RSVP = async () => {
    try {
      await setIsRSVP(!isRSVP);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const validationDataInput = listInput => {
    console.log('KONTOL VALIDASI: ', listInput);
    return new Promise(async (resolve, reject) => {
      try {
        const getZero = await Promise.all(
          listInput
            ?.map((d, i) => {
              console.log('D KONTOL: ', d);
              if (
                d?.name === '' ||
                d?.phoneNo === '' ||
                d?.gender === '' ||
                d?.paxType === '' ||
                d?.group === undefined ||
                d?.group === null
              ) {
                return 'failed';
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        if (getZero) {
          console.log('getZero: ', getZero);
          if (getZero?.length === 0) {
            resolve([]);
          } else {
            reject(['failed']);
          }
        } else {
          reject(['failed']);
        }
      } catch (error) {
        reject(['failed']);
      }
    });
  };

  const onSubmit = async () => {
    try {
      console.log('MASUK SUBMIT: ', {
        input: inputs,
        invitationId: invitationDetail?.uniqueId,
      });
      await setIsLoadingSubmit(true);
      const validate = await validationDataInput(inputs); // if ['failed'] means not pass, if [] is pass
      console.log('Validate: ', validate);

      if (validate?.length === 0) {
        // pass validation
        const cleanUpAttendees = await Promise.all(
          inputs?.map((d, i) => {
            return {
              ...d,
              group: d?.group?.id,
            };
          }),
        );
        console.log('Clening CK: ', cleanUpAttendees);
        if (cleanUpAttendees?.length === inputs?.length) {
          const variables = {
            // invitationId: String(route?.params?.invitationToken),
            // tableType: selectedTable,
            // totalSeat: inputs?.length,
            // attendances: cleanUpAttendees,
            invitationId: String(route?.params?.invitationToken),
            tableType: selectedTable,
            totalSeat: inputs?.length,
            attendances: [...cleanUpAttendees],
          };

          console.log('variables >>> ', variables);
          await client
            .mutate({
              mutation: submitRSVP,
              variables,
            })
            .then(async res => {
              console.log('res submit rsvp: ', res);
              const {data, errors} = res;
              const {customerWeddingAttendanceV2} = data;
              const {error} = customerWeddingAttendanceV2;

              if (errors) {
                await setIsErrorSubmit(true);

                await setTimeout(async () => {
                  await setIsErrorSubmit(false);
                  await setIsLoadingSubmit(false);
                }, 1000);
              } else {
                if (error) {
                  await setIsErrorSubmit(true);

                  await setTimeout(async () => {
                    await setIsErrorSubmit(false);
                    await setIsLoadingSubmit(false);
                  }, 1000);
                } else {
                  await setIsSuccessSubmit(true);
                }
              }
            });
        } else {
          await setIsErrorSubmit(true);

          await setTimeout(async () => {
            await setIsErrorSubmit(false);
            await setIsLoadingSubmit(false);
          }, 1000);
        }
      } else {
        await setIsErrorSubmit(true);

        await setTimeout(async () => {
          await setIsErrorSubmit(false);
          await setIsLoadingSubmit(false);
        }, 1000);
      }
    } catch (error) {
      console.log('Error: ', error);

      await setIsErrorSubmit(true);

      await setTimeout(async () => {
        await setIsErrorSubmit(false);
        await setIsLoadingSubmit(false);
      }, 1000);
    }
  };

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator
          size="large"
          color={mainGreen}
          style={{bottom: 30}}
        />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <EmptyScreen tabName="myinvitation" />
      </View>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <ModalSchedule
          visible={showModalSchedule}
          onClose={() => setShowModalSchedule(false)}
          list={invitationDetail?.schedules ? invitationDetail?.schedules : []}
        />
        {showModalGroup && selectedID !== null ? (
          <ModalGroupContact
            selectedID={selectedID}
            client={props?.client}
            visible={showModalGroup}
            onClose={() => {
              setShowModalGroup(false);
            }}
            onSelect={itm => {
              try {
                manipulateIDGroup(itm);
              } catch (error) {
                console.log('Error: ', error);
                setShowModalGroup(false);
              }
            }}
          />
        ) : null}
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform?.OS === 'ios' ? 'padding' : 'height'}>
          <ScrollView
            contentContainerStyle={{paddingTop: 15, paddingBottom: 80}}>
            <View style={{flex: 1}}>
              {/* Reminder Note Bar */}
              <Card transparent style={{paddingLeft: 15, paddingRight: 15}}>
                <CardItem
                  style={{
                    backgroundColor: '#FAF8F4',
                    width: '100%',
                    borderRadius: 4,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: mainRed,
                      letterSpacing: 0.3,
                      lineHeight: 21,
                    }}>
                    Bring your SWAB Test or Vaccine results with you when you
                    enter the venue
                  </Text>
                </CardItem>
              </Card>
              {/* Invitation Card */}
              <InvitationCard detail={invitationDetail} />
              {/* Modal Loader when submit RSVP */}
              <ModalLoader
                onSuccessClose={async () => {
                  try {
                    await setInputs([
                      {
                        name: '',
                        phoneNo: '',
                        email: '',
                        rsvpStatus: true,
                        vaccinatedSwab: true,
                      },
                    ]);
                    await setIsSuccessSubmit(false);
                    await setIsLoadingSubmit(false);
                    await RSVP();
                  } catch (error) {
                    await setIsSuccessSubmit(false);
                    await setIsLoadingSubmit(false);
                  }
                }}
                isLoadingSubmit={isLoadingSubmit}
                isErrorSubmit={isErrorSubmit}
                isSuccessSubmit={isSuccessSubmit}
              />
              {/* Button RSVP and Schedule */}
              <View
                style={{
                  marginVertical: 15,
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingLeft: 20,
                  paddingRight: 20,
                }}>
                <ButtonCustom title={'RSVP'} onPress={RSVP} />
                <ButtonCustom
                  title={'SCHEDULE'}
                  onPress={() => {
                    setShowModalSchedule(true);
                  }}
                />
              </View>
              {/* List Table --- disble currently */}
              {/* {isRSVP ? (
                <ListTable
                  totalSeat={inputs?.length ? inputs?.length : 0}
                  onSelectedTable={async e => {
                    try {
                      await setSelectedTable(e);
                    } catch (error) {
                      console.log('Error selected table: ', error);
                    }
                  }}
                  selectedTable={selectedTable}
                />
              ) : null} */}
              {/* List Form */}
              {isRSVP ? (
                <DynamicForm
                  client={props?.client}
                  onOpenModalGroup={i => {
                    console.log('Index modal Group: ', i);
                    setSelectedID(i);
                    setShowModalGroup(true);
                  }}
                  onSubmit={onSubmit}
                  inputs={inputs}
                  addNewForm={addNewForm}
                  removeForm={index => removeForm(index)}
                  onChangeGender={e => {
                    try {
                      console.log('OnChangeGender: ', e);
                      const {item, index, parentIndex} = e;
                      let oldData = inputs;
                      oldData[parentIndex].gender = item?.name;
                      setInputs([...oldData]);
                    } catch (error) {
                      console.log('Error OnChangeGender: ', error);
                    }
                  }}
                  onChangePaxType={e => {
                    try {
                      console.log('OnChangeGender: ', e);
                      const {item, index, parentIndex} = e;
                      let oldData = inputs;
                      oldData[parentIndex].paxType = item?.name;
                      setInputs([...oldData]);
                    } catch (error) {
                      console.log('Error OnChangePaxType: ', error);
                    }
                  }}
                  openModalGroup={() => {
                    try {
                      setShowModalGroup(true);
                    } catch (error) {
                      console.log('Open Modal Group Error: ', error);
                    }
                  }}
                  onChangeText={async (index, alias, value) => {
                    try {
                      console.log('Index OnChangeText : ', index);
                      console.log('alias OnChangeText : ', alias);
                      console.log('value OnChangeText :', value);

                      let arrayInput = inputs;
                      let newArrayInput = arrayInput[index];

                      if (alias === 'name') {
                        newArrayInput.name = value;
                        await setInputs([...arrayInput]);
                      } else if (alias === 'phoneNo') {
                        newArrayInput.phoneNo = value;
                        await setInputs([...arrayInput]);
                      } else if (alias === 'note') {
                        newArrayInput.note = value;
                        await setInputs([...arrayInput]);
                      } else {
                        // email
                        newArrayInput.email = value;
                        await setInputs([...arrayInput]);
                      }
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                  onChangeCheckBox={async (index, alias) => {
                    try {
                      console.log('ALIASS: ', alias);
                      if (alias === 'rsvpStatus') {
                        let arrayInput = inputs;
                        let newArrayInput = arrayInput[index];
                        newArrayInput.rsvpStatus = !newArrayInput.rsvpStatus;

                        await setInputs([...arrayInput]);
                      } else {
                        let arrayInput = inputs;
                        let newArrayInput = arrayInput[index];
                        newArrayInput.vaccinatedSwab =
                          !newArrayInput.vaccinatedSwab;

                        await setInputs([...arrayInput]);
                      }
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                />
              ) : null}
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
};

export const ModalSchedule = props => {
  const {visible, onClose, list} = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <View
          style={{
            width: '100%',
            paddingTop: 10,
            paddingBottom: 10,
            borderBottomWidth: 1,
            borderColor: headerBorderBottom,
          }}>
          {/*  */}
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              marginBottom: 10,
            }}>
            {item?.eventName}
          </Text>
          <View
            style={{
              alignSelf: 'flex-start',
              padding: 10,
              backgroundColor: headerBorderBottom,
              borderRadius: 3,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.5),
                color: black,
                letterSpacing: 0.3,
              }}>
              {moment(item?.startTime).format('hh:mm A')} -{' '}
              {moment(item?.endTime).format('hh:mm A')}
            </Text>
          </View>
        </View>
      );
    },
    [list],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{width: '100%', height: width * 1.6, backgroundColor: white}}>
          {/* Header */}
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Schedules
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {}}
                style={{display: 'none', padding: 5}}>
                <Icon
                  type="Feather"
                  name="save"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* Content */}
          <View style={{flex: 1}}>
            <FlatList
              contentContainerStyle={{
                paddingBottom: 25,
                paddingLeft: 15,
                paddingRight: 15,
              }}
              data={list}
              extraData={list}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ModalGroupContact = props => {
  console.log('ModalGroupContact Props: ', props);
  const {visible, onClose, client, onSelect, selectedGroup} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = () => {
    try {
      client
        ?.query({
          query: getGroupContact,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Group Contact: ', res);
          const {data, errors} = res;
          const {rsvpGroups} = data;
          const {data: result, error} = rsvpGroups;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setList([...result]);
            await setTimeout(async () => {
              await setIsError(false);
              await setIsLoading(false);
            }, 500);
          }
        })
        .catch(error => {
          console.log('Error fetch Group: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Fetch Group: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const keyExtractor = (item, index) => {
    return `${item?.id}`;
  };

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity
          onPress={() => onSelect(item)}
          style={{
            width: '100%',
            padding: 25,
            paddingLeft: 5,
            paddingRight: 5,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: selectedGroup?.id === item?.id ? mainRed : black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
        </TouchableOpacity>
      );
    },
    [props?.parentIndex],
  );

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <View
            style={{
              backgroundColor: white,
              bottom: 13,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
              width: '100%',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={onClose} style={{padding: 5}}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: black,
                  fontSize: RFPercentage(2),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Group
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {}}
                style={{padding: 5, display: 'none'}}>
                <Icon
                  type="Feather"
                  name="save"
                  style={{fontSize: RFPercentage(2.4), color: black}}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* List Groups */}
          {isLoading ? (
            <View style={{width: '100%'}}>
              <ActivityIndicator
                size="large"
                color={mainGreen}
                style={{alignSelf: 'center'}}
              />
            </View>
          ) : (
            <FlatList
              style={{bottom: 10}}
              contentContainerStyle={{padding: 15}}
              data={list}
              extraData={list}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

export const ListTable = props => {
  console.log('ListTable Props: ', props);
  const {onSelectedTable, selectedTable, totalSeat} = props;

  const [list, setList] = useState([
    {
      id: 1,
      tableName: 'Square Table',
      icon: boxTable,
    },
    {
      id: 2,
      tableName: 'Round Table',
      icon: circleTable,
    },
    {
      id: 3,
      tableName: 'Retangle Table',
      icon: retangleTable,
    },
  ]);

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [selectedTable],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <View
          style={{
            width: '100%',
            padding: 15,
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: headerBorderBottom,
            borderRadius: 3,
            marginVertical: 10,
          }}>
          <View
            style={{
              flex: 0.3,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{width: width * 0.1, height: width * 0.1, borderWidth: 0}}>
              <AsyncImage
                source={item?.icon}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  width: width * 0.1,
                  height: width * 0.1,
                }}
                resizeMode={'contain'}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.tableName}
            </Text>
            <Text
              style={{
                marginTop: 10,
                fontFamily: book,
                fontSize: RFPercentage(1.4),
                color: black,
                letterSpacing: 0.3,
                display: selectedTable === item?.tableName ? 'flex' : 'none',
              }}>
              Total Seat: {totalSeat}
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => onSelectedTable(item?.tableName)}
              style={{padding: 10}}>
              <View
                style={{
                  width: width * 0.04,
                  height: width * 0.04,
                  borderWidth: 2,
                  borderRadius: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="check"
                  style={{
                    color:
                      selectedTable === item?.tableName
                        ? mainGreen
                        : 'transparent',
                    fontSize: RFPercentage(1.3),
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    },
    [selectedTable],
  );

  const ListHeaderComponent = () => {
    return (
      <View
        style={{
          width: '100%',
          paddingTop: 10,
          paddingBottom: 10,
          borderTopColor: headerBorderBottom,
          borderTopWidth: 1,
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
          }}>
          Table Selection:{' '}
        </Text>
      </View>
    );
  };

  return (
    <FlatList
      ListHeaderComponent={ListHeaderComponent}
      disableVirtualization={true}
      scrollEnabled={false}
      contentContainerStyle={{paddingLeft: 20, paddingRight: 20}}
      data={list}
      extraData={list}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
    />
  );
};

export const ModalLoader = props => {
  const {isLoadingSubmit, isErrorSubmit, isSuccessSubmit, onSuccessClose} =
    props;

  return (
    <Modal visible={isLoadingSubmit} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
        }}>
        {isErrorSubmit ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexWrap: 'wrap',
              borderRadius: 4,
              backgroundColor: white,
              padding: 15,
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.1), color: mainRed}}
            />
            <Text
              style={{
                fontStyle: 'italic',
                fontSize: RFPercentage(1.8),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              Failed to submit
            </Text>
          </View>
        ) : isSuccessSubmit ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexWrap: 'wrap',
              borderRadius: 4,
              backgroundColor: 'transparent',
              padding: 15,
              paddingLeft: 35,
              paddingRight: 35,
            }}>
            <Card style={{borderRadius: 15, paddingTop: 25, paddingBottom: 25}}>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                  paddingBottom: 35,
                }}>
                <View
                  style={{
                    width: 40,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: mainGreen,
                    borderRadius: 40 / 2,
                  }}>
                  <Icon
                    type="Feather"
                    name="check"
                    style={{left: 4, fontSize: 25, color: white}}
                  />
                </View>
              </CardItem>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                  paddingBottom: 0,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(2),
                    color: mainRed,
                  }}>
                  Thank You For RSVP
                </Text>
              </CardItem>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingLeft: 35,
                  paddingRight: 35,
                  backgroundColor: 'transparent',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    fontStyle: 'italic',
                    color: mainGreen,
                    textAlign: 'center',
                    lineHeight: 20,
                  }}>
                  Our Pleasure If You To Attend To Our Special Day
                </Text>
              </CardItem>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                  paddingTop: 40,
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={onSuccessClose}
                    style={{
                      width: '100%',
                      borderRadius: 4,
                      padding: 10,
                      backgroundColor: mainGreen,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: white,
                        textAlign: 'center',
                        letterSpacing: 0.3,
                      }}>
                      Close
                    </Text>
                  </TouchableOpacity>
                </View>
              </CardItem>
            </Card>
          </View>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexWrap: 'wrap',
              borderRadius: 4,
              backgroundColor: white,
              padding: 15,
            }}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginBottom: 10}}
            />
            <Text
              style={{
                fontStyle: 'italic',
                fontSize: RFPercentage(1.8),
                color: greyLine,
                letterSpacing: 0.3,
              }}>
              Submitting...
            </Text>
          </View>
        )}
      </View>
    </Modal>
  );
};

export const DynamicForm = props => {
  console.log('DynamicForm Props: ', props);
  const {
    inputs,
    addNewForm,
    removeForm,
    onChangeCheckBox,
    onChangeText,
    onChangeGender,
    onChangePaxType,
    onSubmit,
    onOpenModalGroup,
  } = props;

  const openWeb = () => {
    try {
      const url = 'https://www.vaccine.gov.sg/';
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log("Don't know how to open URI: " + url);
        }
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const openPdf = () => {
    try {
      const url =
        'https://storage.googleapis.com/love-nest-233803.appspot.com/U_kvrUvzW-healthcerts-clinics_300421.pdf';
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log("Don't know how to open URI: " + url);
        }
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = (item, index) => {
    return `${index} s`;
  };

  const renderItem = ({item, index}) => {
    return (
      <React.Fragment>
        <Card>
          <Card transparent>
            <CardItem style={{width: '100%'}}>
              <View style={{marginRight: 10}}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    color: greyLine,
                    letterSpacing: 0.3,
                  }}>
                  Attendees {index + 1}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 0.5,
                  borderBottomColor: greyLine,
                  borderBottomWidth: 1,
                }}
              />
              {index === 0 ? null : (
                <View style={{flex: 0.1, marginLeft: 5}}>
                  <TouchableOpacity
                    onPress={() => removeForm(index)}
                    style={{
                      borderColor: greyLine,
                      borderRadius: 4,
                      borderWidth: 1,
                      width: 25,
                      height: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name={'x'}
                      style={{left: 10, fontSize: 15, color: greyLine}}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </CardItem>
          </Card>
          <Form
            isError={false}
            required
            value={item?.name}
            onChangeText={text => {
              console.log('Text: ', text);
              onChangeText(index, 'name', text);
            }}
            title={'Name'}
            placeholder={'Name'}
            multiline={false}
            numberOfLines={1}
            isLoading={false}
          />
          {/* Gender */}
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: mainRed,
                }}>
                *
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                  }}>
                  Gender
                </Text>
              </Text>
            </CardItem>
            <CardItem>
              <Gender
                onChange={e => {
                  onChangeGender({...e, parentIndex: index});
                }}
                value={item?.gender}
              />
            </CardItem>
          </Card>
          {/* Pax Type */}
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: mainRed,
                }}>
                *
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                  }}>
                  Pax Type
                </Text>
              </Text>
            </CardItem>
            <CardItem>
              <PaxType
                onChange={e => {
                  onChangePaxType({...e, parentIndex: index});
                }}
                value={item?.paxType}
              />
            </CardItem>
          </Card>
          {/* Group */}
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: mainRed,
                }}>
                *
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                  }}>
                  Group
                </Text>
              </Text>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                console.log('INDEX BAPAK KAU: ', index);
                onOpenModalGroup(index);
              }}>
              <View style={{width: '100%'}}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: item?.group ? black : headerBorderBottom,
                  }}>
                  {item?.group?.name ? item?.group?.name : 'Select group'}
                </Text>
                <View
                  style={{
                    width: '100%',
                    height: 1,
                    borderBottomWidth: 1,
                    borderBottomColor: headerBorderBottom,
                    marginTop: 10,
                  }}
                />
              </View>
            </CardItem>
          </Card>
          {/* Phone Number */}
          <Form
            isError={false}
            required
            value={item?.contactNumber}
            onChangeText={text => {
              console.log('Text: ', text);
              onChangeText(index, 'phoneNo', text);
            }}
            title={'Contact Number'}
            placeholder={'+65837xxx'}
            multiline={false}
            numberOfLines={1}
            isLoading={false}
          />
          <Form
            isError={false}
            required
            value={item?.email}
            onChangeText={text => {
              console.log('Text: ', text);
              onChangeText(index, 'email', text);
            }}
            title={'Email'}
            placeholder={'email'}
            multiline={false}
            numberOfLines={1}
            isLoading={false}
          />
          {index > 0 ? null : (
            <Form
              isError={false}
              required
              value={item?.note}
              onChangeText={text => {
                console.log('Text: ', text);
                onChangeText(index, 'note', text);
              }}
              title={'Note'}
              placeholder={'Note'}
              multiline={true}
              numberOfLines={5}
              isLoading={false}
            />
          )}
          <CardItem style={{width: '100%'}}>
            <View style={{width: '100%'}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: mainRed}}>*</Text>
                <Text
                  style={{
                    left: 3,
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                  }}>
                  Will You Attend The Invitation?
                </Text>
              </View>
              <CheckBox
                type="rsvpStatus"
                inputs={inputs}
                index={index}
                onChangeCheckBox={(i, alias) => onChangeCheckBox(i, alias)}
              />
            </View>
          </CardItem>
          <CardItem style={{width: '100%'}}>
            <View style={{width: '100%'}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: mainRed}}>*</Text>
                <Text
                  style={{
                    left: 3,
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                  }}>
                  Are you Vaccinated/Swab Test?
                </Text>
              </View>
              <CheckBox
                type="vaccinatedSwab"
                inputs={inputs}
                index={index}
                onChangeCheckBox={(i, alias) => onChangeCheckBox(i, alias)}
              />
            </View>
          </CardItem>
        </Card>
      </React.Fragment>
    );
  };

  return (
    <React.Fragment>
      <FlatList
        scrollEnabled={false}
        data={inputs}
        extraData={inputs}
        keyExtractor={keyExt}
        renderItem={renderItem}
        contentContainerStyle={{
          paddingBottom: 15,
          paddingLeft: 20,
          paddingRight: 20,
        }}
        ListFooterComponent={() => {
          return (
            <Card transparent>
              <CardItem>
                <TouchableOpacity
                  onPress={addNewForm}
                  style={{
                    flexDirection: 'row',
                    padding: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderWidth: 2,
                      borderColor: mainRed,
                      borderRadius: 4,
                      marginRight: 10,
                    }}>
                    <Icon
                      type="Feather"
                      name="plus"
                      style={{color: mainRed, fontSize: 15}}
                    />
                  </View>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: mainRed,
                      letterSpacing: 0.3,
                    }}>
                    Add New Attendees
                  </Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem style={{paddingTop: 50}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{color: mainRed}}>*</Text>
                  <Text
                    style={{
                      left: 3,
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(1.8),
                      letterSpacing: 0.3,
                    }}>
                    Not vacinnated or SWAB Test yet?
                  </Text>
                </View>
              </CardItem>
              <CardItem style={{paddingRight: 40}}>
                <Text
                  style={{
                    left: 10,
                    fontFamily: book,
                    color: 'grey',
                    fontStyle: 'italic',
                    fontSize: RFPercentage(1.6),
                    lineHeight: 21,
                  }}>
                  Please click{' '}
                  <Text onPress={openWeb} style={{color: mainRed}}>
                    here
                  </Text>{' '}
                  for SWAB/Vaccine Registration
                </Text>
              </CardItem>
              <CardItem style={{paddingRight: 40}}>
                <Text
                  style={{
                    left: 10,
                    fontFamily: book,
                    color: 'grey',
                    fontStyle: 'italic',
                    fontSize: RFPercentage(1.6),
                    lineHeight: 21,
                  }}>
                  Or click{' '}
                  <Text onPress={openPdf} style={{color: mainRed}}>
                    here
                  </Text>{' '}
                  to see location of Vaccine/SWAB Test
                </Text>
              </CardItem>
              <Card transparent>
                <CardItem
                  style={{
                    paddingTop: 30,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={onSubmit}
                    style={{backgroundColor: mainGreen, padding: 10}}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.8),
                        color: white,
                        letterSpacing: 0.3,
                      }}>
                      SUBMIT
                    </Text>
                  </TouchableOpacity>
                </CardItem>
              </Card>
            </Card>
          );
        }}
      />
    </React.Fragment>
  );
};

export const PaxType = props => {
  const {onChange, value} = props;

  const [listPaxType, setListPaxType] = useState([
    {
      name: 'Adult',
    },
    {
      name: 'Child',
    },
    {
      name: 'Baby',
    },
  ]);

  const update = (item, index) => {
    onChange({item, index});
  };

  const keyExtractor = useCallback((item, index) => {
    return `${index}`;
  });

  const renderItem = useCallback(({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => update(item, index)}
        style={{
          marginRight: 15,
          borderRadius: 25,
          padding: 10,
          paddingLeft: 15,
          paddingRight: 15,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'flex-start',
          borderColor: mainRed,
          borderWidth: 1,
          backgroundColor: item?.name === value ? mainRed : '#FFEDED',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: item?.name === value ? white : mainRed,
            letterSpacing: 0.3,
          }}>
          {item?.name}
        </Text>
      </TouchableOpacity>
    );
  });

  return (
    <FlatList
      scrollEnabled={false}
      horizontal
      data={listPaxType}
      extraData={listPaxType}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
    />
  );
};

export const Gender = props => {
  const {onChange, value} = props;

  const [listGender, setListGender] = useState([
    {
      name: 'Male',
    },
    {
      name: 'Female',
    },
  ]);

  const update = (item, index) => {
    onChange({item, index});
  };

  const keyExtractor = useCallback((item, index) => {
    return `${index}`;
  });

  const renderItem = useCallback(({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => update(item, index)}
        style={{
          marginRight: 15,
          borderRadius: 25,
          padding: 10,
          paddingLeft: 15,
          paddingRight: 15,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'flex-start',
          borderColor: mainRed,
          borderWidth: 1,
          backgroundColor: item?.name === value ? mainRed : '#FFEDED',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: item?.name === value ? white : mainRed,
            letterSpacing: 0.3,
          }}>
          {item?.name}
        </Text>
      </TouchableOpacity>
    );
  });

  return (
    <FlatList
      scrollEnabled={false}
      horizontal
      data={listGender}
      extraData={listGender}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
    />
  );
};

export const CheckBox = props => {
  const {inputs, index, onChangeCheckBox, type} = props;

  const status =
    type === 'rsvpStatus'
      ? inputs[index]?.rsvpStatus
      : inputs[index]?.vaccinatedSwab;

  if (status) {
    return (
      <View style={{padding: 10, paddingTop: 15, flexDirection: 'row'}}>
        <View style={{flexDirection: 'row', marginRight: 15}}>
          <TouchableOpacity
            onPress={() => onChangeCheckBox(index, type)}
            style={{
              width: 20,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: 4,
              backgroundColor: black,
              flexDirection: 'row',
              marginRight: 10,
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{
                left: 11,
                fontSize: RFPercentage(1.5),
                color: white,
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              top: 3,
              fontFamily: book,
              color: black,
              fontSize: RFPercentage(1.6),
              letterSpacing: 0.3,
            }}>
            Yes
          </Text>
        </View>
        <View style={{flexDirection: 'row', marginRight: 15}}>
          <TouchableOpacity
            onPress={() => onChangeCheckBox(index, type)}
            style={{
              width: 20,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 2,
              borderRadius: 4,
              backgroundColor: white,
              flexDirection: 'row',
              marginRight: 10,
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{
                left: 11,
                fontSize: RFPercentage(1.5),
                color: white,
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              top: 3,
              fontFamily: book,
              color: black,
              fontSize: RFPercentage(1.6),
              letterSpacing: 0.3,
            }}>
            No
          </Text>
        </View>
      </View>
    );
  } else {
    return (
      <View style={{padding: 10, paddingTop: 15, flexDirection: 'row'}}>
        <View style={{flexDirection: 'row', marginRight: 15}}>
          <TouchableOpacity
            onPress={() => onChangeCheckBox(index, type)}
            style={{
              width: 20,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 2,
              borderRadius: 4,
              backgroundColor: white,
              flexDirection: 'row',
              marginRight: 10,
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{
                left: 11,
                fontSize: RFPercentage(1.5),
                color: white,
              }}
            />
          </TouchableOpacity>

          <Text
            style={{
              top: 3,
              fontFamily: book,
              color: black,
              fontSize: RFPercentage(1.6),
              letterSpacing: 0.3,
            }}>
            Yes
          </Text>
        </View>
        <View style={{flexDirection: 'row', marginRight: 15}}>
          <TouchableOpacity
            onPress={() => onChangeCheckBox(index, type)}
            style={{
              width: 20,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: 4,
              backgroundColor: black,
              flexDirection: 'row',
              marginRight: 10,
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{
                left: 11,
                fontSize: RFPercentage(1.5),
                color: white,
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              top: 3,
              fontFamily: book,
              color: black,
              fontSize: RFPercentage(1.6),
              letterSpacing: 0.3,
            }}>
            No
          </Text>
        </View>
      </View>
    );
  }
};

export const ButtonCustom = props => {
  const {title, onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        alignSelf: 'flex-start',
        borderWidth: 1,
        borderColor: title === 'RSVP' ? white : mainGreen,
        backgroundColor: title === 'RSVP' ? mainGreen : white,
        padding: 10,
        paddingLeft: 25,
        paddingRight: 25,
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: title === 'RSVP' ? white : mainGreen,
          letterSpacing: 0.3,
        }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export const InvitationCard = props => {
  const {detail} = props;

  return (
    <View
      style={{width: '100%', padding: 15, paddingLeft: 20, paddingRight: 20}}>
      <View
        style={{
          width: '100%',
          height: width * 1.35,
          backgroundColor: headerBorderBottom,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
          borderRadius: 5,
        }}>
        <AsyncImage
          source={{
            uri:
              detail?.type === 'custom'
                ? detail?.canvasImgUrl
                : detail?.imageUrl,
          }}
          style={{
            borderRadius: 5,
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
          }}
          resizeMode={'cover'}
        />
      </View>
    </View>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(MyInvitation);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
