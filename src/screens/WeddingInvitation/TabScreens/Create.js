import React, {useEffect} from 'react';
import {View, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';

// Components
import Header from '../Components/Header';
import ListTemplate from '../Components/ListTemplate';

const CreateTab = props => {
  const {positionYBottomNav} = props;
  useEffect(() => {
    onChangeOpacity(true);
    const tabPress = props?.navigation?.addListener('tabPress', () => {
      onChangeOpacity(true);
    });
    const onFocus = props?.navigation?.addListener('focus', () => {
      onChangeOpacity(true);
    });

    return () => {
      tabPress;
      onFocus;
    };
  }, []);

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  return (
    <View style={{flex: 1}}>
      {/* <Header {...props} /> */}
      <ListTemplate {...props} />
    </View>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(CreateTab);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
