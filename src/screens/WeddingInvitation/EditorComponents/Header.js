import React from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, greyLine, mainGreen, mainRed, white, headerBorderBottom} = Colors;
const {medium} = FontType;

const Headers = props => {
  const goBack = () => {
    props?.navigation?.goBack(null);
  };

  const save = () => {
    props?.onSaveToDraft();
  };

  const send = () => {
    props?.onSend();
  };

  const addCustomImageTemplate = () => {
    try {
      props?.addCustomeImageTemplate();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        backgroundColor: white,
      }}>
      <View
        style={{
          flex: 0.5,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <Button onPress={goBack} type={'navigation'} />
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          Canvas
        </Text>
      </View>
      <View
        style={{
          flex: 0.5,
          flexDirection: 'row',
          borderWidth: 0,
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        {props?.isLoadingCustomImage ? (
          <ActivityIndicator size="small" color={mainGreen} />
        ) : (
          <Button onPress={addCustomImageTemplate} type={'image'} />
        )}
        {props?.isLoadingSaveToDraft ? (
          <ActivityIndicator size="small" color={mainGreen} />
        ) : (
          <Button onPress={save} type={'save'} />
        )}
        {props?.isLoadingSendInvitation ? (
          <ActivityIndicator size="small" color={mainGreen} />
        ) : (
          <Button onPress={send} type={'send'} />
        )}
      </View>
    </Header>
  );
};

export const Button = props => {
  const {onPress, type} = props;
  return (
    <TouchableOpacity onPress={onPress} style={{padding: 5, borderWidth: 0}}>
      <Icon
        type="Feather"
        name={
          type === 'navigation'
            ? 'chevron-left'
            : type === 'save'
            ? 'save'
            : type === 'image'
            ? 'image'
            : 'send'
        }
        style={{color: black, fontSize: RFPercentage(2.5)}}
      />
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(Headers);

export default props => <Wrapper {...props} />;
