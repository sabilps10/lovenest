const colors = [
  {
    id: 1,
    hex: '#F7D4E0',
  },
  {
    id: 2,
    hex: '#E0858A',
  },
  {
    id: 3,
    hex: '#FCDEED',
  },
  {
    id: 4,
    hex: '#E085D7',
  },
  {
    id: 5,
    hex: '#4B82EC',
  },
  {
    id: 6,
    hex: '#A570BF',
  },
  {
    id: 7,
    hex: '#FBF1E4',
  },
  {
    id: 8,
    hex: '#F4AB82',
  },
  {
    id: 9,
    hex: '#3EB9CA',
  },
  {
    id: 10,
    hex: '#7FB13F',
  },
  {
    id: 11,
    hex: '#CFEFF3',
  },
  {
    id: 12,
    hex: '#FAC4C7',
  },
  {
    id: 13,
    hex: '#F8BEC2',
  },
  {
    id: 14,
    hex: '#FFC95A',
  },
  {
    id: 15,
    hex: '#D4D4D4',
  },
  {
    id: 16,
    hex: '#CAB77C',
  },
  {
    id: 17,
    hex: '#F5E8E7',
  },
  {
    id: 18,
    hex: '#999999',
  },
  {
    id: 19,
    hex: '#6B6B6B',
  },
  {
    id: 20,
    hex: '#EEF4E5',
  },
  {
    id: 21,
    hex: '#E5ECFB',
  },
  {
    id: 22,
    hex: '#CF615D',
  },
  {
    id: 23,
    hex: '#F1A340',
  },
  {
    id: 24,
    hex: '#B67B7F',
  },
];

export default colors;
