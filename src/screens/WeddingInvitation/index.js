import React, {useEffect, useState} from 'react';
import {
  Text,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';

import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Tab Config
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

// Screen Tabs
import CreateTabScreen from './TabScreens/Create';
import MyInvitationTabScreen from './TabScreens/MyInvitation';
import MyRSVPTabScreen from './TabScreens/MyRSVP';

const {white, mainRed, mainGreen, black, headerBorderBottom, greyLine} = Colors;
const {medium, book} = FontType;

const EInvitation = props => {
  console.log('EInvitation Tab Container Props: ', props);

  const {navigation, positionYBottomNav} = props;

  useEffect(() => {
    StatusBar.setBarStyle('dark-content');
    StatusBar.setBackgroundColor('white');
    navigationOptions();
    // onChangeOpacity(true);
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      // onChangeOpacity(true);
    });

    return () => {
      subscriber();
    };
  }, []);

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Invitation',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: RFPercentage(1.8),
      },
      headerStyle: {
        borderBottomWidth: 0.2,
        borderBottomColor: white,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return null;
      },
      headerRight: () => {
        return null;
      },
    });
  };

  return (
    <Tab.Navigator
      lazy
      initialRouteName="CreateTabScreen"
      tabBarPosition="top"
      tabBarOptions={{
        upperCaseLabel: false,
        indicatorStyle: {
          backgroundColor: mainRed,
        },
        labelStyle: {
          fontFamily: medium,
          letterSpacing: 0.25,
          fontSize: RFPercentage(1.6),
          textTransform: 'none',
        },
        activeTintColor: mainRed,
        inactiveTintColor: greyLine,
        allowFontScaling: true,
      }}>
      <Tab.Screen
        name="CreateTabScreen"
        options={{tabBarLabel: 'Create'}}
        children={childProps => {
          return <CreateTabScreen {...props} {...childProps} />;
        }}
      />
      <Tab.Screen
        name="MyInvitationTabScreen"
        options={{tabBarLabel: 'My Invitation'}}
        children={childProps => {
          return <MyInvitationTabScreen {...props} {...childProps} />;
        }}
      />
      <Tab.Screen
        name="MyRSVPTabScreen"
        options={{tabBarLabel: 'My RSVP'}}
        children={childProps => {
          return <MyRSVPTabScreen {...props} {...childProps} />;
        }}
      />
    </Tab.Navigator>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(EInvitation);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
