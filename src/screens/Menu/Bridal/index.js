import React, {useEffect} from 'react';
import {Text, StatusBar, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Header, Left, Body, Right, Icon, Button} from 'native-base';
import BridalList from '../../Home/TabScreens/Bridal';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';

const {white, black, greyLine} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const Bridal = props => {
  console.log('Bridal Props: ', props);
  const {navigation, positionYBottomNav} = props;

  useEffect(() => {
    StatusBar.setTranslucent(false);

    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(false);
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subscriber;
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };
  return (
    <Container style={{backgroundColor: white}}>
      <Headers goBack={goBack} />
      <BridalList {...props} />
    </Container>
  );
};

export const Headers = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Bridal
        </Text>
      </Body>
      <Right style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="home"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(Bridal);

export default props => <Wrapper {...props} />;
