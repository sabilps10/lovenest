import React, {useRef, useEffect, useState} from 'react';
import {Text, Dimensions, Image, View, ScrollView} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Header, Left, Body, Right, Button, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
// import AsyncImage from '../../../components/Image/AsyncImage';
import {charImage} from '../../../utils/Themes/Images';

const {charNoOrder} = charImage;
const {width, height} = Dimensions.get('window');
const {white, black, mainGreen, greyLine} = Colors;
const {medium, book} = FontType;

// Components
import Tabs from './Tabs';
import GlobalHeader from './Components/GlobalHeader';
import ScrollToTopButton from './Components/ScrollToTopButton';

// Queries
import GET_MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';
import ButtonEnquiry from './Components/ButtonEnquiry';

const BridalMerchantDetail = props => {
  console.log('BridalMerchantDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const scrollViewRef = useRef();

  const [activeTab, setActiveTab] = useState('Home');

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetchMerchantDetail();
  }, []);

  const fetchMerchantDetail = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('response fetch merchant detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const onActiveTab = async tabName => {
    try {
      await setActiveTab(tabName);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const scrollToTop = () => {
    console.log('Ref Scroll view: ', scrollViewRef?.current);
    try {
      scrollViewRef?.current?.scrollTo({
        y: 0,
        animated: true,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          onPress={fetchMerchantDetail}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: 15,
            paddingTop: 5,
            paddingBottom: 5,
            backgroundColor: mainGreen,
          }}>
          <Text>Refresh</Text>
        </Button>
      </View>
    );
  } else {
    if (!isLoading && detail?.length === 0) {
      // When merchant not found or deleted from back office
      return (
        <Container style={{backgroundColor: white}}>
          <Headers goBack={() => {}} />
          <View
            style={{
              padding: 15,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={charNoOrder}
              style={{bottom: 90, width, height: height / 3}}
              resizeMode="contain"
            />
            <Text
              style={{
                bottom: 75,
                fontFamily: medium,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2.5),
              }}>
              Merchant Not Found
            </Text>
            <Text
              style={{
                lineHeight: 20,
                bottom: 60,
                fontFamily: book,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2),
              }}>
              We are sorry, current merchant is out from our services
            </Text>
          </View>
        </Container>
      );
    } else {
      // when merchant is available on system
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          {activeTab === 'Home' ? (
            <>
              <ScrollView
                bounces={false}
                ref={scrollViewRef}
                decelerationRate="fast">
                <Tabs
                  detail={detail}
                  id={id}
                  serviceType={serviceType}
                  activeTab={activeTab}
                  onActiveTab={tabName => onActiveTab(tabName)}
                  {...props}
                />
              </ScrollView>
              <ScrollToTopButton onPress={scrollToTop} />
              <ButtonEnquiry
                title={'ENQUIRY NOW'}
                {...props}
                onPress={() => {
                  navigation.navigate('Enquiry', {
                    id: detail[0]?.id,
                    serviceType: detail[0]?.serviceType,
                  });
                }}
              />
            </>
          ) : (
            <>
              {activeTab !== 'Home' ? (
                <GlobalHeader
                  activeTab={activeTab}
                  {...props}
                  detail={detail}
                />
              ) : null}
              <Tabs
                id={id}
                detail={detail}
                serviceType={serviceType}
                activeTab={activeTab}
                onActiveTab={tabName => onActiveTab(tabName)}
                {...props}
              />
            </>
          )}
        </View>
      );
    }
  }
};

export const Headers = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Bridal
        </Text>
      </Body>
      <Right style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="home"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(BridalMerchantDetail);

export default props => <Wrapper {...props} />;
