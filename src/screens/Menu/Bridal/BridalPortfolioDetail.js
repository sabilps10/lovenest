import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  Animated,
  FlatList,
  View,
  ScrollView,
  RefreshControl,
  Modal,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Header,
  Left,
  Body,
  Right,
  Icon,
} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';
import Headers from '../../../components/Header/Common/index';

import ModalImageViewer from './Components/ImageViewer';

// Query
import GET_PROJECT_DETAIL from '../../../graphql/queries/getProject';

const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;
const {
  white,
  black,
  mainRed,
  mainGreen,
  headerBorderBottom,
  greyLine,
  superGrey,
  transparent,
} = Colors;
const heightSlider = height / 1.58;

const FloristProjectDetail = props => {
  console.log('FloristProjectDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [openImageViewer, setOpenImageViewer] = useState(false);

  useEffect(() => {
    fetchProjectDetail();

    if (refreshing) {
      fetchProjectDetail();
    }

    const subs = navigation.addListener('focus', () => {
      fetchProjectDetail();

      if (refreshing) {
        fetchProjectDetail();
      }
    });

    return () => {
      subs;
    };
  }, [navigation, id, refreshing]);

  const openImageModal = async () => {
    try {
      await setOpenImageViewer(!openImageViewer);
    } catch (error) {
      console.log('Error: ', error);
      await setOpenImageViewer(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const fetchProjectDetail = async () => {
    try {
      await client
        .query({
          query: GET_PROJECT_DETAIL,
          variables: {
            id,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchProjectDetail response: ', response);
          const {data, errors} = response;
          const {getProject} = data;
          const {data: detailProject, error} = getProject;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              await setDetail([detailProject]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Headers
          title={'Portfolio'}
          buttonLeft={() => navigation.goBack(null)}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <View style={{zIndex: 99}}>
          <Headers
            title={'Portfolio'}
            buttonLeft={() => navigation.goBack(null)}
            buttonRight={() => {}}
            showRightButton={false}
          />
        </View>
        <Content
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              height: height / 1.1,
            }}>
            <Text>Failed to Load Data, pull to reload!</Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <ModalImageViewer
          visible={openImageViewer}
          onPress={openImageModal}
          images={detail[0]?.galleries}
        />
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          contentContainerStyle={{}}>
          <View style={{flex: 1}}>
            <View
              style={{
                position: 'absolute',
                top: 0,
                width: '100%',
                backgroundColor: 'transparent',
                zIndex: 99,
              }}>
              <TransprentHeader goBack={() => navigation.goBack(null)} />
            </View>
            <ProductImageSlider
              openImageModal={openImageModal}
              gallery={detail[0]?.galleries}
              {...props}
            />
            <Card transparent style={{marginTop: 0, marginBottom: 0}}>
              <CardItem style={{paddingTop: 0}}>
                <Text
                  style={{
                    fontSize: RFPercentage(2),
                    color: black,
                    fontFamily: medium,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  {detail[0]?.name}
                </Text>
              </CardItem>
              <CardItem>
                <Text
                  style={{
                    fontSize: RFPercentage(1.8),
                    color: black,
                    fontFamily: book,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  {detail[0].description}
                </Text>
              </CardItem>
              <CardItem>
                <View style={{height: '100%'}}>
                  <Icon
                    type="Feather"
                    name="tag"
                    style={{
                      top: 2,
                      color: superGrey,
                      fontSize: RFPercentage(1.7),
                    }}
                  />
                </View>
                <View>
                  <Text
                    style={{
                      right: 10,
                      fontFamily: book,
                      color: superGrey,
                      fontSize: RFPercentage(1.7),
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    {detail[0]?.bridalTheme}
                  </Text>
                </View>
              </CardItem>
            </Card>
          </View>
        </ScrollView>
      </View>
    );
  }
};

export const TransprentHeader = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: 'transparent',
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0,
      }}>
      <Left
        style={{flex: 0.5, backgroundColor: 'transparent', paddingLeft: 10}}>
        <Button
          onPress={goBack}
          style={{
            borderRadius: 35 / 2,
            backgroundColor: white,
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={
              Platform?.OS === 'android'
                ? {
                    right: 4,
                    color: black,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                  }
                : {
                    color: black,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                  }
            }
          />
        </Button>
      </Left>
      <Body style={{flex: 1, backgroundColor: 'transparent'}} />
      <Right style={{flex: 0.5, backgroundColor: 'transparent'}} />
    </Header>
  );
};

export const ProductImageSlider = props => {
  const {gallery, navigation, openImageModal} = props;
  const scrollX = new Animated.Value(0);
  const keyExt = (item, index) => `${String(item.id)} Images`;
  const RenderSlider = () =>
    useMemo(() => {
      return (
        <>
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            initialNumToRender={3}
            legacyImplementation
            disableVirtualization
            decelerationRate="fast"
            data={gallery}
            extraData={gallery}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            pagingEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            initialScrollIndex={0}
            renderItem={({item, index}) => {
              return <CardImage item={item} />;
            }}
          />
          <View
            style={{
              position: 'relative',
              zIndex: 10,
              bottom: 50,
              right: 0,
              left: 0,
              width: '100%',
              flexDirection: 'row',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {gallery.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {gallery.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: 7,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 7 / 2,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 9,
                          width: 9,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 9 / 2,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
            <View
              style={{
                flex: 0.3,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  // navigation.navigate('ProductGallery', {
                  //   images: gallery,
                  // });
                  openImageModal();
                }}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: white,
                  paddingLeft: 10,
                  paddingRight: 10,
                  height: height / 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  View All
                </Text>
              </Button>
            </View>
          </View>
        </>
      );
    }, [gallery]);
  if (gallery) {
    return RenderSlider();
  } else {
    return null;
  }
};

export const CardImage = props => {
  const {item} = props;
  const {url, dynamicUrl} = item;
  const logoSource = dynamicUrl ? {uri: `${dynamicUrl}=h800`} : {uri: url};
  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card
          style={{
            elevation: 0,
            shadowOpacity: 0,
            marginLeft: 0,
            marginTop: 0,
            marginBottom: 0,
            marginRight: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            width,
          }}>
          <CardItem cardBody>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                shadowOpacity: 0,
                elevation: 0,
                width,
                height: heightSlider,
                aspectRatio: width / heightSlider,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={greyLine}
            />
          </CardItem>
        </Card>
      );
    }, [item]);

  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(FloristProjectDetail);

export default props => <Wrapper {...props} />;
