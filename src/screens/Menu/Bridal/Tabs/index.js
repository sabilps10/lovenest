import React from 'react';
import {Text, Animated, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

// Tabs
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

import TabBar from '../TabBars';

// Tab Screens
import Home from '../TabScreens/Home';
import Gown from '../TabScreens/Gown';
import Suit from '../TabScreens/Suit';
import Portfolio from '../TabScreens/Portfolio';
import Promotion from '../TabScreens/Promotion';

//  Components
import CoverImage from '../Components/CoverImage';
import GlobalHeader from '../Components/GlobalHeader';

const Tabs = props => {
  console.log('Tab props: ', props);
  const {id, serviceType, activeTab, onActiveTab, detail} = props;

  return (
    <Tab.Navigator
      lazy={true}
      swipeEnabled={true}
      animationEnabled={true}
      initialRouteName={activeTab}
      tabBar={tabBarProps => {
        return (
          <Animated.View>
            {activeTab === 'Home' ? (
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  zIndex: 2,
                }}>
                <GlobalHeader activeTab={activeTab} {...props} />
              </View>
            ) : null}
            {activeTab === 'Home' ? (
              <View style={{zIndex: 1, borderWidth: 0, top: 0}}>
                <CoverImage
                  serviceType={
                    detail[0]?.serviceType ? detail[0]?.serviceType : ''
                  }
                  name={detail[0]?.name ? detail[0]?.name : 'n/a'}
                  logoSource={
                    detail[0]?.logoImageDynamicUrl
                      ? {uri: `${detail[0]?.logoImageDynamicUrl}=h500`}
                      : {uri: detail[0]?.logoImageUrl}
                  }
                  imageSource={
                    detail[0]?.coverImageDynamicUrl
                      ? {uri: `${detail[0]?.coverImageDynamicUrl}=h500`}
                      : {uri: `${detail[0]?.coverImageUrl}`}
                  }
                  tagline={detail[0]?.tagline}
                />
              </View>
            ) : null}
            <TabBar {...tabBarProps} />
          </Animated.View>
        );
      }}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Home',
        }}
        name="Home"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          console.log('NAMEEEEEEEEEEEE: ', routeNamePress);
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Home
              id={id}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Gown',
        }}
        name="Gown"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Gown
              id={id}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Suit',
        }}
        name="Suit"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Suit
              id={id}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Portfolio',
        }}
        name="Portfolio"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Portfolio
              id={id}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Promotion',
        }}
        name="Promotion"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Promotion
              id={id}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
    </Tab.Navigator>
  );
};

const Wrapper = compose(withApollo)(Tabs);

export default props => <Wrapper {...props} />;
