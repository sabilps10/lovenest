import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  FlatList,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  ScrollView,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Button,
  Content,
  Card,
  CardItem,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';

const {
  white,
  overlayDim,
  black,
  greyLine,
  headerBorderBottom,
  mainGreen,
  mainRed,
} = Colors;
const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;

// Components
import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFiltered from '../../../components/Button/ButtonFilters/Unfilter';
import ButtonSubmit from './Components/ButtonEnquiry';

// Query
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';

// Redux Bridal Gown Filter
import ReduxGownFilter from '../../../redux/thunk/Bridal/Gown/index';
import ReduxGownReset from '../../../redux/thunk/Bridal/Gown/reset';

const BridalGownFilter = props => {
  console.log('BridalGownFilter Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {serviceType} = params;

  const [showModal, setShowModal] = useState(false);

  const [filterList, setFilterList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  useEffect(() => {
    fetchWithRedux();

    const subs = navigation.addListener('focus', () => {
      fetchWithRedux();
    });

    return () => {
      subs();
    };
  }, [navigation]);

  const fetchWithRedux = async () => {
    try {
      if (props?.list?.length === 0) {
        await fetchFilter();
      } else {
        await setFilterList([...props?.list]);
        await setIsError(false);
        await setIsLoading(false);
      }
    } catch (error) {
      console.log('Error fetchWithRedux: ', error);
      await isError(true);
      await setIsLoading(false);
    }
  };

  const fetchFilter = async () => {
    try {
      await client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            serviceType,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response Fetch Filter Gown: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: filters, error} = productsFilter;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setFilterList([...filters]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const reset = async () => {
    try {
      await props?.reduxGownReset();
      navigation.goBack(null);
      console.log('Gown Filter Reset: ', props);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const checkList = async (parentIndex, index) => {
    let oldData = filterList;
    oldData[parentIndex].list[index].checked =
      !filterList[parentIndex].list[index].checked;
    await setFilterList([...oldData]);

    if (filterList[parentIndex].name === 'Type') {
      let tempSelectedArray = selectedType;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedType(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Category') {
      let tempSelectedArray = selectedCategory;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedCategory(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Color') {
      let tempSelectedArray = selectedColor;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedColor(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Range') {
      let tempSelectedArray = selectedRange;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedRange(tempSelectedArray);
    }
  };

  const onPress = async () => {
    try {
      console.log('Apply Filter Gown: ', {
        filterList,
      });
      await setShowModal(true);
      await props?.reduxGownFilter([...filterList]);

      await setTimeout(async () => {
        await setShowModal(false);
        await goBack();
      }, 2000);
    } catch (error) {
      console.log('Error: ', error);
      await setShowModal(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalLoader visible={showModal} />
      <Headers title={'Gown Filter'} goBack={goBack} reset={reset} />
      <ScrollView>
        <ListFilter
          checkList={(p, i) => checkList(p, i)}
          isLoading={isLoading}
          isError={isError}
          list={filterList}
        />
      </ScrollView>
      <ButtonSubmit title={'APPLY FILTER'} onPress={() => onPress()} />
    </View>
  );
};

export const ModalLoader = props => {
  const {visible} = props;
  return (
    <Modal visible={visible} animationType="fade" transparent>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginBottom: 10}}
          />
          <Text style={{color: greyLine, fontStyle: 'italic'}}>
            Updating...
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const ListFilterContent = props => {
  const {list, parentIndex, checkList} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${item.index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      const capitalParse = item?.name.toUpperCase();
      const suitIsIncluded = capitalParse.includes('SUIT');
      if (item?.checked) {
        if (item?.name === 'Suit' || suitIsIncluded) {
          return null;
        } else {
          return (
            <ButtonFiltered
              item={item}
              name={item?.name}
              index={index}
              checked={item?.checked}
              parentIndex={parentIndex}
              checkList={(p, i) => {
                console.log('Check List Function: ', {p, i});
                checkList(p, i);
              }}
              {...props}
            />
          );
        }
      } else {
        if (item?.name === 'Suit' || suitIsIncluded) {
          return null;
        } else {
          return (
            <ButtonUnFiltered
              item={item}
              name={item?.name}
              index={index}
              checked={item?.checked}
              parentIndex={parentIndex}
              checkList={(p, i) => {
                console.log('Check List Function: ', {p, i});
                checkList(p, i);
              }}
              {...props}
            />
          );
        }
      }
    },
    [list],
  );

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <FlatList
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderItem}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          width: '100%',
        }}
      />
    );
  }
};

export const ListFilter = props => {
  const {list, isLoading, isError, checkList} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${item.name}`;
    },
    [list],
  );

  const renderList = useCallback(
    ({item, index}) => {
      return (
        <Card transparent>
          <CardItem style={{width}}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingLeft: 1.5,
                paddingRight: 1.5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {item?.name}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingLeft: 1.5,
                paddingRight: 0.5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                You may select more than one
              </Text>
            </View>
          </CardItem>
          <CardItem>
            <ListFilterContent
              list={item?.list}
              parentIndex={index}
              checkList={(p, i) => checkList(p, i)}
            />
          </CardItem>
        </Card>
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <View
        style={{
          paddingTop: 15,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color={mainGreen} size="large" />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <FlatList
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderList}
      />
    );
  }
};

export const Headers = props => {
  const {goBack, reset, title} = props;

  return (
    <Header
      androidStatusBarColor={white}
      iosBarStyle={'dark-content'}
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
      }}>
      <Left style={{flex: 0.3}}>
        <View>
          <Button
            onPress={() => goBack()}
            style={{
              backgroundColor: white,
              elevation: 0,
              shadowOpacity: 0,
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </View>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.3}}>
        <View>
          <Button
            onPress={() => reset()}
            style={{
              backgroundColor: white,
              elevation: 0,
              shadowOpacity: 0,
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: mainRed,
                letterSpacing: 0.3,
                fontSize: RFPercentage(1.6),
              }}>
              Reset
            </Text>
          </Button>
        </View>
      </Right>
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: >> ', state);
  const {bridalGownFilterList} = state;
  return {
    list: bridalGownFilterList?.list,
  };
};

const mapToDispatch = dispatch => {
  console.log('mapToDispatch: >> ', dispatch);
  return {
    reduxGownFilter: list => dispatch(ReduxGownFilter(list)),
    reduxGownReset: () => dispatch(ReduxGownReset()),
  };
};

const Connector = connect(mapToState, mapToDispatch)(BridalGownFilter);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
