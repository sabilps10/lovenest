import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import CardProduct from '../../../../../components/Cards/Merchants/ProductCard';
import Loader from '../../../../../components/Loader/circleLoader';
import QueryPublicProduct from '../../../../../graphql/queries/productsPublic';
import Colors from '../../../../../utils/Themes/Colors';
import {FontType} from '../../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');
const {black, mainRed} = Colors;
const {medium} = FontType;

const ProductAccessories = props => {
  console.log('ProductAccessories Props: ', props);
  const {
    client,
    navigation,
    merchantId,
    serviceType,
    isLoading: loadingParent,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [product, setProducts] = useState([]);

  useEffect(() => {
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return subscriber;
  }, [loadingParent]);

  const fetch = () => {
    try {
      const variables = {
        merchantId,
        serviceType,
        accsCategory: [],
        accsDetails: [],
        accsRange: [],
        pageSize: 2,
        pageNumber: 1,
      };
      client
        .query({
          query: QueryPublicProduct,
          variables,
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response Product Accessories: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProducts([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          height: height / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <Card transparent style={{marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0}}>
        {product?.length > 0 ? (
          <CardItem
            style={{
              paddingTop: 0,
              paddingBottom: 0,
              paddingLeft: 17,
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              Accessories
            </Text>
            <Button
              onPress={() => {
                navigation.navigate('Accessories', {
                  id: merchantId,
                  serviceType,
                  filtered: true,
                });
              }}
              style={{
                backgroundColor: 'transparent',
                elevation: 0,
                shadowOpacity: 0,
                height: height / 20,
                paddingLeft: 10,
                paddingRight: 0,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  right: -15,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                See All
              </Text>
              <Icon
                type="Feather"
                name="arrow-right"
                style={{right: -10, fontSize: 18, color: mainRed}}
              />
            </Button>
          </CardItem>
        ) : null}
        <FlatList
          legacyImplementation
          disableVirtualization
          numColumns={2}
          contentContainerStyle={{
            paddingTop: 10,
            paddingBottom: 15,
            // paddingLeft: 10,
            // paddingRight: 10,
            alignSelf: 'flex-start',
            // justifyContent: 'flex-start'
          }}
          columnWrapperStyle={{
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: 7,
            paddingRight: 10,
          }}
          data={product}
          extraData={product}
          keyExtractor={item => String(item.id)}
          renderItem={({item}) => {
            const {
              featuredImageDynamicURL,
              featuredImageURL,
              name,
              id: productId,
              merchantDetails,
            } = item;
            const source =
              featuredImageDynamicURL === null
                ? {uri: featuredImageURL}
                : {uri: `${featuredImageDynamicURL}=h500`};
            const {id} = merchantDetails;
            return (
              <CardProduct
                label={'Product'}
                name={name}
                source={source}
                merchantId={id}
                productId={productId}
                navigation={navigation}
              />
            );
          }}
        />
      </Card>
    );
  }
};

const Wrapper = compose(withApollo)(ProductAccessories);

export default props => <Wrapper {...props} />;
