import React, {useState, useEffect} from 'react';
import {View, Dimensions, Image, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {charImage} from '../../../../../utils/Themes/Images';
import ButtonSeeAllProduct from '../../../../../components/Home/PublicProduct/ButtonSeeAllProduct';
import GET_MERCHANT_DETAIL from '../../../../../graphql/queries/getMerchantVer2';
import SpecialOfferCard from '../../../../../components/Cards/Merchants/SpecialOfferCard';
import Loader from '../../../../../components/Loader/circleLoader';

const {charGoldtag} = charImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const SpecialOffer = props => {
  console.log('ANUUU JANCUUKKSSS: ', props);
  const {navigation, client, id, isLoading} = props;

  const [isLoadingSpecialOffer, setIsLoadingSpecialOffer] = useState(true);
  const [isErrorSpecialOffer, setIsErrorSpecialOffer] = useState(false);

  const [specialOfferList, setSpecialOfferList] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, isLoading, id]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          networkPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('SPEICAL OFFER >> Merchant Detail Response: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors === undefined) {
            const {promotions} = getMerchantVer2[0];
            if (promotions) {
              await setSpecialOfferList([...promotions]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            } else {
              await setSpecialOfferList([]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            }
          } else {
            await setIsErrorSpecialOffer(true);
            await setIsLoadingSpecialOffer(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorSpecialOffer(true);
          await setIsLoadingSpecialOffer(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorSpecialOffer(true);
      setIsLoadingSpecialOffer(false);
    }
  };

  const extKey = item => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: widthScreen / 1.1,
      offset: (widthScreen / 1.1) * index,
      index,
    };
  };

  if (isLoadingSpecialOffer && !isErrorSpecialOffer) {
    return (
      <View
        style={{
          width: widthScreen,
          height: heightScreen / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoadingSpecialOffer && isErrorSpecialOffer) {
    return null;
  } else {
    return (
      <>
        {specialOfferList.length === 0 ? null : (
          <View
            style={{
              // width: widthScreen,
              // minHeight: heightScreen / 2.3,
              paddingBottom: 25,
              marginBottom: 5,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.25}}
              end={{x: 1.9, y: 5}}
              locations={[0.0, 0.2, 0.6]}
              colors={['#FBF1E4', 'white', 'white']}
              style={{
                position: 'absolute',
                top: 0,
                zIndex: 0,
                width: '100%',
                height: '100%',
              }}
            />
            <Image
              source={charGoldtag}
              style={{
                position: 'absolute',
                top: 10,
                left: 0,
                zIndex: 2,
                width: 43,
                height: 54,
              }}
              resizeMode="contain"
            />
            <ButtonSeeAllProduct
              title={'Special Offers'}
              disableButton={true}
              {...props}
            />
            <View style={{top: 30, zIndex: 4}}>
              <FlatList
                scrollEnabled={false}
                disableVirtualization
                legacyImplementation
                data={specialOfferList}
                keyExtractor={extKey}
                listKey={extKey}
                getItemLayout={getItemLayout}
                contentContainerStyle={{
                  padding: 15,
                  alignItems: 'center',
                }}
                renderItem={({item}) => {
                  const {
                    promoImageURL,
                    promoImageDynamicURL,
                    promoLinkURL,
                  } = item;
                  const source = promoImageDynamicURL
                    ? {uri: `${promoImageDynamicURL}=h500`}
                    : {uri: promoImageURL};
                  const disabled =
                    promoLinkURL && promoLinkURL !== '' ? false : true;
                  return (
                    <SpecialOfferCard
                      item={item}
                      source={source}
                      disabled={disabled}
                      promoLinkURL={promoLinkURL}
                      navigation={navigation}
                    />
                  );
                }}
              />
            </View>
          </View>
        )}
      </>
    );
  }
};

const Wrapper = compose(withApollo)(SpecialOffer);

export default props => <Wrapper {...props} />;
