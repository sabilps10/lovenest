import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import CardProduct from '../../../../../components/Cards/Merchants/ProductCard';
import Loader from '../../../../../components/Loader/circleLoader';
import QueryPublicProduct from '../../../../../graphql/queries/productsPublic';
import Colors from '../../../../../utils/Themes/Colors';
import {FontType} from '../../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import GET_FILTER_LIST from '../../../../../graphql/queries/productFilter';
import {connect} from 'react-redux';
import ReduxProductFilter from '../../../../../redux/thunk/ProductFilterThunk';
import ReduxProductFilterStatus from '../../../../../redux/thunk/ProductFilterStatusThunk';
const {width, height} = Dimensions.get('window');
const {black, mainRed} = Colors;
const {medium} = FontType;

const ProductSuite = props => {
  console.log('ProductSuite Props: ', props);
  const {
    client,
    navigation,
    merchantId,
    serviceType,
    bridalType,
    isLoading: loadingParent,
    productFilterStatus,
    productFilter,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [product, setProducts] = useState([]);

  const [filterList, setFilterList] = useState([]);
  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  const [isLoadingFilter, setIsLoadingFilter] = useState(true);

  useEffect(() => {
    fetch();
    fetchProductFilterList();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
      fetchProductFilterList();
    });

    return subscriber;
  }, [loadingParent]);

  const updateFilter = listDataFilterFromAPI => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulate = await Promise.all(
          listDataFilterFromAPI.map(async (d, i) => {
            const listManipulate = await d.list.map((a, x) => {
              if (a.name === 'Suit') {
                return {
                  name: a.name,
                  checked: true,
                };
              } else {
                return {
                  ...a,
                };
              }
            });
            console.log('listManipulate kontol >>>> ', listManipulate);
            if (listManipulate) {
              return {
                name: d.name,
                list: listManipulate,
              };
            }
          }),
        );

        console.log('manipulate kontol >>> ', manipulate);

        if (manipulate.length === listDataFilterFromAPI.length) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log('Error update Filter: ', error);
        resolve(false);
      }
    });
  };

  const fetchProductFilterList = () => {
    try {
      console.log('Masuk sini bray >>>>>>>>>>');
      client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            merchantId,
            serviceType,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('FILTER DATA RESPONSE: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: list, error} = productsFilter;

          if (errors) {
            await setIsLoadingFilter(false);
          } else {
            if (error) {
              await setIsLoadingFilter(false);
            } else {
              const filterManipulation = await updateFilter(list);
              await setFilterList([...filterManipulation]);
              await setSelectedType(['Suit']);
              await setSelectedCategory([]);
              await setSelectedColor([]);
              await setSelectedRange([]);
              await setIsLoadingFilter(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsLoadingFilter(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingFilter(false);
    }
  };

  const fetch = () => {
    try {
      const variables = {
        merchantId,
        serviceType,
        bridalType,
        pageSize: 2,
        pageNumber: 1,
      };
      client
        .query({
          query: QueryPublicProduct,
          variables,
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response Product Suite: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProducts([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          height: height / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <>
        {product?.length === 0 ? null : (
          <Card transparent style={{marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0}}>
            {product?.length > 0 ? (
              <CardItem
                style={{
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingLeft: 17,
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Graze your suit
                </Text>
                {!isLoadingFilter ? (
                  <Button
                    onPress={async () => {
                      await productFilterStatus(true);
                      await productFilter(
                        filterList,
                        selectedType,
                        selectedCategory,
                        selectedColor,
                        selectedRange,
                      );
                      navigation.navigate('Products', {
                        id: merchantId,
                        serviceType,
                        filtered: true,
                      });
                    }}
                    style={{
                      backgroundColor: 'transparent',
                      elevation: 0,
                      shadowOpacity: 0,
                      height: height / 20,
                      paddingLeft: 10,
                      paddingRight: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={{
                        right: -15,
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      See All
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{right: -10, fontSize: 18, color: mainRed}}
                    />
                  </Button>
                ) : null}
              </CardItem>
            ) : null}
            <FlatList
              legacyImplementation
              disableVirtualization
              numColumns={2}
              contentContainerStyle={{
                paddingTop: 10,
                paddingBottom: 15,
                // paddingLeft: 10,
                // paddingRight: 10,
                alignSelf: 'flex-start',
                // justifyContent: 'flex-start'
              }}
              columnWrapperStyle={{
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingLeft: 7,
                paddingRight: 10,
              }}
              data={product}
              extraData={product}
              keyExtractor={item => String(item.id)}
              renderItem={({item}) => {
                const {
                  featuredImageDynamicURL,
                  featuredImageURL,
                  name,
                  id: productId,
                  merchantDetails,
                } = item;
                const source =
                  featuredImageDynamicURL === null
                    ? {uri: featuredImageURL}
                    : {uri: `${featuredImageDynamicURL}=h500`};
                const {id} = merchantDetails;
                return (
                  <CardProduct
                    label={'Product'}
                    name={name}
                    source={source}
                    merchantId={id}
                    productId={productId}
                    navigation={navigation}
                  />
                );
              }}
            />
          </Card>
        )}
      </>
    );
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {};
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxProductFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    productFilterStatus: status => dispatch(ReduxProductFilterStatus(status)),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductSuite);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
