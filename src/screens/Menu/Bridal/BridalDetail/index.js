import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  Dimensions,
  StatusBar,
  Animated,
  Image,
  ScrollView,
  FlatList,
  Platform,
  RefreshControl,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType, FontSize} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import LinearGradient from 'react-native-linear-gradient';
import {CommonActions} from '@react-navigation/native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
} from 'native-base';
import {charImage, BridalBadge, socialMediaIcon} from '../../../../utils/Themes/Images';
import HighlightList from '../../../Merchant/Highlight/ShortHighlight';
import ProBadge from '../../../../components/Cards/Merchants/ProBadge';
import Map from '../../../../components/Map/MapWithNavigationButton';
import GeocodeByAddress from '../../../../utils/Geocoding';
import ButtonSocialMedia from '../../../../components/Button/buttonSocialMedia';

// Common Utils
const {facebook, youtube, instagram, pinterest} = socialMediaIcon;
const {blueBadge, pinkBadge} = BridalBadge;
const {charMerchantAbout, charBridalBadge} = charImage;
const {book, medium, bold} = FontType;
const {small, regular} = FontSize;
const {
  white,
  transparent,
  black,
  greyLine,
  superGrey,
  mainGreen,
  mainRed,
  overlayDim,
} = Colors;

// Query
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';

// Common Screen
import ErrorScreen from '../../../../components/ErrorScreens/NotLoginYet';
import Loader from '../../../../components/Loader/circleLoader';
import ProductSuite from './Components/ProductSuit';
import ProductGown from './Components/ProductGown';
import ProductAccessories from './Components/ProductAccessories';
import SpecialOffer from './Components/SpecialOffer';

// Animated Utils
const {width, height} = Dimensions.get('window');
const HEADER_HEIGHT = hasNotch() ? 85 : 75;
const HEADER_CARD_IMAGE_HEIGHT = height / 2.94;
const SCROLL_HEIGHT = HEADER_CARD_IMAGE_HEIGHT - HEADER_HEIGHT;
const defaultHeight = height / 1.25;

const BridalDetail = props => {
  console.log('BridalDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {serviceType, id} = params;

  const [merchantDetailData, setMerchantDetailData] = useState([]);
  const [landscape, setLandscape] = useState([]);
  const [portrait, setPortrait] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [scrollY] = useState(new Animated.Value(0));

  const [isDescExpand, setIsDescExpand] = useState(false);

  useEffect(() => {
    StatusBar.setTranslucent(true);
    fetch();

    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(true);
      fetch();
    });

    return subscriber;
  }, [navigation, serviceType, id]);

  const toggleRedMore = () => {
    setIsDescExpand(!isDescExpand);
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const setLanscapeImage = extraPhoto => {
    console.log('Extra Photo Pak: ', extraPhoto);
    return new Promise(async (resolve, reject) => {
      try {
        const manipulate = await Promise.all(
          extraPhoto
            .map((d, i) => {
              if (d.orientation === 'Landscape') {
                return {...d};
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );
        console.log('manipulate >>> ', manipulate);
        if (manipulate) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const setPortraitImage = extraPhoto => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulate = await Promise.all(
          extraPhoto
            .map((d, i) => {
              if (d.orientation === 'Potrait') {
                return {...d};
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );
        console.log('manipulate >>> ', manipulate);
        if (manipulate) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error Go Back: ', error);
    }
  };

  const fetch = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response fetch bridal detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            console.log(
              'getMerchantVer2[0].extraPhoto >>>> ',
              getMerchantVer2[0].extraPhoto,
            );
            const extraPhoto =
              getMerchantVer2[0]?.extraPhoto.length > 0
                ? getMerchantVer2[0].extraPhoto
                : [];
            const getListImageLandscape = await setLanscapeImage(extraPhoto);
            const getListImagePortrait = await setPortraitImage(extraPhoto);
            await setLandscape([...getListImageLandscape]);
            await setPortrait([...getListImagePortrait]);
            await setMerchantDetailData([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          console.log('Failed fetch bridal detail: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefreshing(false);
          StatusBar.setTranslucent(false);
        });
    } catch (error) {
      console.log('Error Fetch Merchant Detail: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
    }
  };

  const resetToHome = () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Main'}],
        }),
      );
    } catch (error) {
      console.log('Error reset to home from Products: ', error);
    }
  };

  const openMap = item => {
    try {
      const label = `${item.locationName}, ${item.address}, ${item.country}`;

      const url = Platform.select({
        ios: `https://www.google.com/maps/search/?api=1&query=${label}&center=${
          item.latitude
        },${item.longitude}`,
        android: `geo:${item.latitude},${item.longitude}?q=${label}`,
      });
      Linking.canOpenURL(url)
        .then(supported => {
          if (!supported) {
            const browser_url = `https://www.google.de/maps/@${item.latitude},${
              item.longitude
            }?q=${label}`;
            return Linking.openURL(browser_url);
          } else {
            return Linking.openURL(url);
          }
        })
        .catch(error => console.log('Error: ', error));
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const openCall = phoneNumber => {
    try {
      Linking.openURL(`tel:${phoneNumber}`);
    } catch (error) {
      console.log('Error on Call: ', error);
    }
  };

  const openMapByAddress = async address => {
    try {
      const getLocationDetail = await GeocodeByAddress.detailLocation(address);
      console.log('getLocationDetail: ', getLocationDetail);

      if (getLocationDetail) {
        const lat = getLocationDetail.location.lat;
        const lng = getLocationDetail.location.lng;

        const latitude = lat;
        const longitude = lng;
        const label = `${address}`;

        const splitingAddress = label.replace(',', ' ');
        const replaceAddressWithReservedSymbol = splitingAddress.replace(
          '#',
          '',
        );
        console.log('splitingAddress: ', replaceAddressWithReservedSymbol);

        const url = Platform.select({
          ios: `https://www.google.com/maps/search/?api=1&query=${replaceAddressWithReservedSymbol}&center=${latitude},${longitude}`,
          android: `geo:${latitude},${longitude}?q=${replaceAddressWithReservedSymbol}`,
        });
        Linking.canOpenURL(url)
          .then(supported => {
            if (!supported) {
              const browser_url = `https://www.google.de/maps/@${latitude},${longitude}?q=${label}`;
              return Linking.openURL(browser_url);
            }
            return Linking.openURL(url);
          })
          .catch(err => console.log('error', err));
      }
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const sendEmail = email => {
    try {
      Linking.openURL(`mailto:${email}`);
    } catch (error) {
      console.log('Send Email Error: ', error);
    }
  };

  const sosmedOpenLink = url => {
    if (url !== '') {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log(`Don't know how to open URI: ${url}`);
        }
      });
    }
  };

  if (isLoading && !isError) {
    // return loading
    return (
      <Container>
        <HeaderDefault
          serviceType={serviceType}
          onPressLeft={() => goBack()}
          onPressRight={() => resetToHome()}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loader />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    // return error screen
    return (
      <Container>
        <HeaderDefault
          serviceType={serviceType}
          onPressLeft={() => goBack()}
          onPressRight={() => resetToHome()}
        />
        <ErrorScreen message="Refresh" onPress={() => {}} />
      </Container>
    );
  } else {
    // return data
    const {
      id: merchantId,
      coverImageUrl,
      coverImageDynamicUrl,
      logoImageUrl,
      logoImageDynamicUrl,
      name,
      description,
      tagline,
      products,
      projects,
      promotions,
    } = merchantDetailData[0];

    const imageSource =
      coverImageDynamicUrl === null
        ? {uri: coverImageUrl}
        : {uri: `${coverImageDynamicUrl}=h500`};
    const logoSource =
      logoImageDynamicUrl === null
        ? {uri: logoImageUrl}
        : {uri: `${logoImageDynamicUrl}=h200`};

    const HEADER_OPACITY = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [0.0, 0.5, 1.0],
    });

    const goToAboutMerchant = () => {
      try {
        navigation.push('AboutMerchant', {id});
      } catch (error) {
        console.log('Error: ', error);
      }
    };

    return (
      <Container>
        <StatusBar translucent backgroundColor="transparent" animated />
        <MiddleTextHeader
          {...props}
          merchantId={merchantId}
          name={name}
          titleOpacity={HEADER_OPACITY}
          leftSide={goBack}
          rightSide={goToAboutMerchant}
        />
        <WhiteBarHeader
          merchantId={merchantId}
          {...props}
          headerOpacity={HEADER_OPACITY}
        />
        <Animated.ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          bounces={true}
          decelerationRate="fast"
          scrollEventThrottle={Platform.OS === 'android' ? 16 : 1}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}
          style={{zIndex: 0}}>
          <HeaderImage
            logoSource={logoSource}
            imageSource={imageSource}
            name={name}
            tagline={tagline}
          />
          {products?.length === 0 &&
          projects?.length === 0 &&
          promotions?.length === 0 ? (
            <View>
              <GalleryImages
                merchantDetailData={merchantDetailData}
                landscape={landscape}
                portrait={portrait}
              />
              <Description
                isDescExpand={isDescExpand}
                name={name}
                description={description}
                toggleRedMore={toggleRedMore}
              />
              {merchantDetailData[0]?.serviceType === 'Bridal' ? (
                <BadgeMerchant />
              ) : null}
              <ListMapAddress
                merchantDetail={merchantDetailData}
                openCall={openCall}
                openMap={openMap}
              />
              <AddressCardNonList
                merchantDetail={merchantDetailData}
                openCall={openCall}
                openMapByAddress={openMapByAddress}
                sosmedOpenLink={sosmedOpenLink}
                sendEmail={sendEmail}
              />
            </View>
          ) : (
            <View>
              <GalleryImages
                merchantDetailData={merchantDetailData}
                landscape={landscape}
                portrait={portrait}
              />
              <ProductGown
                {...props}
                isLoading={refreshing}
                serviceType={serviceType}
                merchantId={id}
                bridalType={['Gown']}
              />
              <ProductSuite
                {...props}
                isLoading={refreshing}
                serviceType={serviceType}
                merchantId={id}
                bridalType={['Suit']}
              />
              <ProductAccessories
                {...props}
                isLoading={refreshing}
                serviceType={'Accessories'}
                merchantId={id}
              />
              <HighlightList showShort={true} {...props} />
              <SpecialOffer isLoading={refreshing} {...props} id={id} />
            </View>
          )}
        </Animated.ScrollView>
        <Footer>
          <FooterTab style={{backgroundColor: mainGreen}}>
            <View style={{backgroundColor: mainGreen, width: '100%'}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Enquiry', {serviceType, id});
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  SUBMIT ENQUIRY
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
};

export const AddressCardNonList = props => {
  const {
    merchantDetail,
    openCall,
    openMapByAddress,
    sosmedOpenLink,
    sendEmail,
  } = props;

  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card transparent style={{marginTop: 0}}>
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openMapByAddress(merchantDetail[0].address);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].address}`}</Text>
              </View>
            </CardItem>
          ) : null}
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openCall(
                  `${merchantDetail[0].countryCode}${merchantDetail[0].phone}`,
                );
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].countryCode}${
                  merchantDetail[0].phone
                }`}</Text>
              </View>
            </CardItem>
          ) : null}
          <CardItem
            button
            onPress={() => {
              sendEmail(merchantDetail[0].email);
            }}
            style={{width: '100%', top: 5}}>
            <View
              style={{
                flex: 0.1,
                height: '100%',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 3,
              }}>
              <Icon
                type="Feather"
                name="mail"
                style={{fontSize: RFPercentage(2), color: black}}
              />
            </View>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {merchantDetail[0].email}
              </Text>
            </View>
          </CardItem>
          <CardItem style={{width: '100%'}}>
            <View
              style={{
                height: 1,
                width: '100%',
                borderBottomColor: greyLine,
                borderBottomWidth: 0.5,
              }}
            />
          </CardItem>
          <CardItem>
            {merchantDetail[0].facebookLink ? (
              <ButtonSocialMedia
                iconType={facebook}
                onPress={() => sosmedOpenLink(merchantDetail[0].facebookLink)}
              />
            ) : null}
            {merchantDetail[0].instagramLink ? (
              <ButtonSocialMedia
                iconType={instagram}
                onPress={() => sosmedOpenLink(merchantDetail[0].instagramLink)}
              />
            ) : null}
            {merchantDetail[0].pinterestLink ? (
              <ButtonSocialMedia
                iconType={pinterest}
                onPress={() => sosmedOpenLink(merchantDetail[0].pinterestLink)}
              />
            ) : null}
            {merchantDetail[0].youtubeLink ? (
              <ButtonSocialMedia
                iconType={youtube}
                onPress={() => sosmedOpenLink(merchantDetail[0].youtubeLink)}
              />
            ) : null}
          </CardItem>
        </Card>
      );
    }, [merchantDetail]);

  if (merchantDetail) {
    return RenderCard();
  } else {
    return null;
  }
};

export const ListMapAddress = props => {
  const {merchantDetail, openCall, openMap} = props;
  const keyExt = item => `${String(item.id)}`;
  const RenderList = () =>
    useMemo(() => {
      return (
        <View>
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Locate Us
              </Text>
            </CardItem>
          </Card>
          <FlatList
            scrollEnabled={false}
            legacyImplementation
            disableVirtualization
            data={merchantDetail[0].addresses}
            extraData={merchantDetail[0].addresses}
            keyExtractor={keyExt}
            listKey={keyExt}
            renderItem={({item}) => {
              return (
                <MapAddressCard
                  item={item}
                  openCall={openCall}
                  openMap={openMap}
                />
              );
            }}
          />
        </View>
      );
    }, [merchantDetail]);
  if (merchantDetail?.length > 0) {
    return RenderList();
  } else {
    return null;
  }
};

export const MapAddressCard = props => {
  const {item, openCall, openMap} = props;
  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card
          style={{
            marginLeft: 15,
            marginRight: 15,
            paddingTop: 0,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Map
            buttonOnTheRight={true}
            disableContainerStyle={true}
            withDirectionText={false}
            forMapAddresses={`${item.locationName}, ${item.address}, ${
              item.country
            }`}
            onNavigate={() => openMap(item)}
          />
          <Card transparent style={{marginTop: 0}}>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>{`${item.locationName}`}</Text>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                openMap(item);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.locationName}, ${item.address}, ${
                  item.country
                }`}</Text>
              </View>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                openCall(`${item.countryCode}${item.phone}`);
              }}
              style={{width: '100%', backgroundColor: transparent}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.countryCode}${item.phone}`}</Text>
              </View>
            </CardItem>
          </Card>
        </Card>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const BadgeMerchant = () => {
  const listBadge = [
    {
      id: 1,
      image: blueBadge,
    },
    {
      id: 2,
      image: pinkBadge,
    },
  ];

  return (
    <FlatList
      data={listBadge}
      extraData={item => `${item.id}`}
      legacyImplementation
      disableVirtualization
      numColumns={2}
      columnWrapperStyle={{paddingLeft: 15, marginVertical: 15}}
      scrollEnabled={false}
      renderItem={({item}) => {
        return (
          <View
            style={{
              width: width / 5,
              height: height / 9,
              marginRight: 10,
            }}>
            <Image
              source={item.image}
              style={{flex: 1, width: '100%', height: height / 5}}
              resizeMode="contain"
            />
          </View>
        );
      }}
    />
  );
};

export const Description = props => {
  const {name, description, toggleRedMore, isDescExpand} = props;

  return (
    <Card transparent>
      <CardItem style={{width: '100%'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
          }}>
          Description
        </Text>
      </CardItem>
      <CardItem style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
        {description?.length > 200 ? (
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {isDescExpand
              ? description
              : `${description.substring(0, 200)}... `}
            <Text
              onPress={toggleRedMore}
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              {isDescExpand ? ' Read less' : 'Read more'}
            </Text>
          </Text>
        ) : (
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {description}
          </Text>
        )}
      </CardItem>
    </Card>
  );
};

export const GalleryImages = props => {
  console.log('ANU PROP image Gallery Bridal: ', props);
  const {merchantDetailData, landscape, portrait} = props;
  if (merchantDetailData[0]?.extraPhoto?.length > 0) {
    return (
      <View style={{marginTop: 15, marginBottom: 15}}>
        <LandscapeImage
          imageSource={{
            uri: landscape[0]?.dynamicUrl
              ? `${landscape[0].dynamicUrl}=h500`
              : landscape[0].url,
          }}
        />
        <PortraitImages imageList={portrait} />
        <LandscapeImage
          imageSource={{
            uri: landscape[1]?.dynamicUrl
              ? `${landscape[1].dynamicUrl}=h500`
              : landscape[1].url,
          }}
        />
      </View>
    );
  } else {
    return null;
  }
};

export const PortraitImages = props => {
  const {imageList} = props;
  return (
    <FlatList
      scrollEnabled={false}
      data={imageList}
      extraData={imageList}
      keyExtractor={(item, index) => `${item.id} Portrait`}
      legacyImplementation
      disableVirtualization
      numColumns={2}
      columnWrapperStyle={{
        justifyContent: 'space-between',
        marginVertical: 4,
      }}
      renderItem={({item, index}) => {
        const imgSrc = item?.dynamicUrl
          ? {uri: `${item.dynamicUrl}=h500`}
          : {uri: item.url};
        return (
          <View
            style={{
              width: width / 2.02,
              height: height / 2.5,
              backgroundColor: greyLine,
            }}>
            <Image
              source={imgSrc}
              style={{flex: 1, width: '100%', height: height / 2.5}}
              resizeMode="cover"
            />
          </View>
        );
      }}
    />
  );
};

export const LandscapeImage = props => {
  const {imageSource} = props;

  return (
    <View
      style={{
        width,
        height: height / 3,
        backgroundColor: greyLine,
      }}>
      <Image
        source={imageSource}
        style={{flex: 1, width, height: height / 3}}
        resizeMode="cover"
      />
    </View>
  );
};

export const HeaderImage = props => {
  const {logoSource, imageSource, name, tagline} = props;

  return (
    <View
      style={{
        width,
        height: height / 3,
        backgroundColor: greyLine,
        flexDirection: 'column',
        marginBottom: 15,
      }}>
      <View
        style={{
          flexDirection: 'column',
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1,
        }}>
        <Image
          source={imageSource}
          style={{
            position: 'absolute',
            zIndex: 0,
            width,
            height: height / 3,
          }}
          resizeMode="cover"
        />
        <LinearGradient
          locations={Platform.OS === 'ios' ? [0.15, 0.78] : [0.5, 0.4, 0.8]}
          colors={
            Platform.OS === 'ios'
              ? [transparent, '#FFFFFF', '#FFFFFF']
              : [transparent, transparent, '#FFFFFF']
          }
          style={{
            position: 'absolute',
            zIndex: 2,
            width: '100%',
            height: '100%',
            bottom: 0,
          }}
        />
      </View>
      <View
        style={{
          paddingTop: 70,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2,
          // backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            minWidth: width / 5,
            maxWidth: width / 5,
            minHeight: height / 11,
            maxHeight: height / 11,
            backgroundColor: white,
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={logoSource}
            style={{
              // flex: 1,
              width: '100%',
              height: height / 15,
            }}
            resizeMode="contain"
          />
        </View>
        <View style={{marginVertical: 15, marginTop: 20}}>
          <ProBadge title={'Pro Bridal Merchant'} />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: medium,
              fontSize: RFPercentage(2.2),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 25,
            }}>
            {name}
          </Text>
          <Text
            style={{
              top: 5,
              textAlign: 'center',
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 23,
            }}>
            {tagline}
          </Text>
        </View>
      </View>
    </View>
  );
};

export const WhiteBarHeader = props => {
  const {headerOpacity} = props;

  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        zIndex: 1,
        height: HEADER_HEIGHT,
        width: width,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        flexDirection: 'row',
        backgroundColor: white,
        opacity: headerOpacity,
      }}>
      <Left style={{flex: 0.1}} />
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      />
      <Right style={{flex: 0.1}} />
    </Animated.View>
  );
};

export const MiddleTextHeader = props => {
  const {name, titleOpacity, leftSide, rightSide, merchantId} = props;

  return (
    <View
      style={{
        zIndex: 2,
        position: 'absolute',
        top: hasNotch() ? 50 : 30,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <View style={{flex: 0.1}}>
        <LeftButton onPress={leftSide} />
      </View>
      <View style={{flex: 0.15}} />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <Animated.Text
          numberOfLines={1}
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
            opacity: titleOpacity,
          }}>
          {name}
        </Animated.Text>
      </View>
      <View style={{flex: 0.15, flexDirection: 'row'}}>
        <ReviewButton merchantId={merchantId} {...props} />
      </View>
      <View style={{flex: 0.1, flexDirection: 'row'}}>
        <RightButton onPress={rightSide} />
      </View>
    </View>
  );
};

export const ReviewButton = props => {
  const {navigation, merchantId} = props;

  return (
    <Button
      onPress={() => navigation.navigate('ReviewList', {id: merchantId})}
      style={{
        // right: 40,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="Feather"
        name="star"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const RightButton = props => {
  const {onPress} = props;
  return (
    <Button
      onPress={() => onPress()}
      style={{
        // right: 30,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="MaterialCommunityIcons"
        name="store"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const LeftButton = props => {
  const {onPress} = props;
  return (
    <Button
      onPress={() => onPress()}
      style={{
        // zIndex: 2,
        // position: 'absolute',
        // top: hasNotch() ? 45 : 10,
        // left: 15,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const HeaderDefault = props => {
  const {serviceType, onPressLeft, onPressRight} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          style={{
            elevation: 0,
            shadowOpacity: 0,
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: transparent,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          {serviceType}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(BridalDetail);

export default props => <Wrapper {...props} />;
