import React from 'react';
import {Text, Image, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';
import {FontType} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';
import {connect} from 'react-redux';

const {white, black, headerBorderBottom} = Colors;
const {medium} = FontType;

import {charImage} from '../../../../utils/Themes/Images';

const {charFilterIcon, charIconAbout, charIconReview} = charImage;

import {CommonActions} from '@react-navigation/native';

// redux
import ReduxResetSuit from '../../../../redux/thunk/Bridal/Suit/reset';
import ReduxResetGown from '../../../../redux/thunk/Bridal/Gown/reset';

const MerchantDetailHeader = props => {
  const {activeTab, detail, navigation} = props;

  const goBack = async () => {
    try {
      await props?.reduxResetGown();
      await props?.reduxResetSuit();
      await navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToAbout = () => {
    try {
      navigation.navigate('AboutMerchant', {
        id: detail[0]?.id,
        serviceType: detail[0]?.serviceType,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToHome = async () => {
    try {
      await navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToFilter = () => {
    try {
      console.log('Active Tab Filter checker: ', activeTab);
      if (activeTab === 'Gown') {
        // Send to BridalGownFilter Screen
        navigation.navigate('BridalGownFilter', {
          id: detail[0]?.id,
          serviceType: detail[0]?.serviceType,
        });
      } else if (activeTab === 'Suit') {
        // Send to BridaSuitFilterScreen
        navigation.navigate('BridalSuitFilter', {
          id: detail[0]?.id,
          serviceType: detail[0]?.serviceType,
        });
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToReview = () => {
    try {
      navigation.navigate('ReviewList', {id: detail[0]?.id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor={activeTab === 'Home' ? 'transparent' : 'white'}
      translucent={activeTab === 'Home' ? true : false}
      style={
        activeTab === 'Home'
          ? {
              paddingTop:
                Platform.OS === 'android'
                  ? hasNotch()
                    ? 55
                    : 45
                  : hasNotch()
                  ? 0
                  : 0,
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
            }
          : {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
              marginBottom: 10,
            }
      }>
      <Left style={{flex: 0.4, paddingLeft: 10}}>
        <Button
          onPress={goBack}
          style={{
            borderRadius: 35 / 2,
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={
              Platform.OS === 'ios'
                ? {marginLeft: 0, marginRight: 0, fontSize: 24, color: black}
                : {
                    right: 4,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: black,
                  }
            }
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {activeTab !== 'Home' ? (
          <>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              {activeTab}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                marginVertical: 5,
                fontSize: RFPercentage(1.3),
                color: 'grey',
                letterSpacing: 0.3,
              }}>
              {detail[0]?.name}
            </Text>
          </>
        ) : null}
      </Body>
      <Right
        style={{
          flex: 0.4,
          justifyContent:
            activeTab === 'Home' || activeTab === 'Products'
              ? 'space-between'
              : 'flex-end',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        {activeTab === 'Home' ? (
          <Button
            onPress={goToReview}
            style={{
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity:
                activeTab === 'Home'
                  ? Platform.OS === 'android'
                    ? 1
                    : 0.2
                  : 0,
              shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowRadius: 1.41,
            }}>
            <Image
              source={charIconReview}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : activeTab === 'Gown' || activeTab === 'Suit' ? (
          <Button
            onPress={goToFilter}
            style={{
              opacity: 1,
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity: activeTab === 'Home' ? 1 : 0,
            }}>
            <Image
              source={charFilterIcon}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : null}
        <Button
          onPress={() => {
            if (activeTab === 'Home') {
              goToAbout();
            } else {
              goToHome();
            }
          }}
          style={{
            borderRadius: 35 / 2,
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          {activeTab === 'Home' ? (
            <Image
              source={charIconAbout}
              style={{bottom: 2, width: 25, height: 25}}
              resizeMode="contain"
            />
          ) : (
            <Icon
              type="Feather"
              name="home"
              style={{
                bottom: 0.5,
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          )}
        </Button>
      </Right>
    </Header>
  );
};

const mapToState = () => {
  return {};
};

const mapToDispatch = dispatch => {
  return {
    reduxResetSuit: () => dispatch(ReduxResetSuit()),
    reduxResetGown: () => dispatch(ReduxResetGown()),
  };
};

const connector = connect(mapToState, mapToDispatch)(MerchantDetailHeader);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
