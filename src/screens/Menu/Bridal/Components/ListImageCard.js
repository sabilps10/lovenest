import React from 'react';
import {FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ImageCards from './ImageCards';

const ListImageCard = props => {
  const {listImageCard, isLoading, isError} = props;

  const keyExt = item => `${item.id}`;

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (listImageCard?.length === 0) {
      return null;
    } else {
      return (
        <FlatList
          legacyImplementation
          disableVirtualization
          scrollEnabled={false}
          initialNumToRender={5}
          data={listImageCard}
          extraData={listImageCard}
          keyExtractor={keyExt}
          renderItem={({item, index}) => {
            const {dynamicUrl, url} = item;
            const source = dynamicUrl
              ? {uri: `${dynamicUrl}=nu-pp`}
              : {uri: url};

            if (index === 0 || index === 1) {
              return null;
            } else {
              return <ImageCards source={source} />;
            }
          }}
        />
      );
    }
  }
};

const Wrapper = compose(withApollo)(ListImageCard);

export default props => <Wrapper {...props} />;
