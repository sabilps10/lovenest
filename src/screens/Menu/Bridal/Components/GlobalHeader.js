import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import HeaderHome from './MerchantDetailHeader';

const GlobalHeader = props => {
  const {activeTab} = props;

  return <HeaderHome activeTab={activeTab} {...props} />;
};
const Wrapper = compose(withApollo)(GlobalHeader);

export default props => <Wrapper {...props} />;
