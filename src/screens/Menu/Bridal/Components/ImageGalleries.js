import React, {useState, useEffect} from 'react';
import {Dimensions, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';

const {width, height} = Dimensions.get('window');

const Galleries = props => {
    console.log('MASUK Galleries: ', props);
  const {images} = props;

  const [imagesPortrait, setImagePortrait] = useState([]);
  const [imagesLandscape, setImageLandscape] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    manipulated();
  }, [images]);

  const manipulated = async () => {
    try {
      if (!images || images?.length === 0) {
        throw new Error('Empty Data');
      } else {
        const groupingPotrait = await grouping(images, 'Potrait');
        console.log('groupingPotrait <Potrait>: ', groupingPotrait);

        const groupingLandscape = await grouping(images, 'Landscape');
        console.log('groupingLandscape <Landscape>: ', groupingLandscape);

        if (groupingPotrait && groupingLandscape) {
          await setImagePortrait([...groupingPotrait]);
          await setImageLandscape([...groupingLandscape]);

          await setIsError(false);
          await setIsLoading(false);
        } else {
          throw new Error('Empty Data');
        }
      }
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const grouping = (listImages, styles) => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulating = await Promise.all(
          listImages
            .map((d, i) => {
              if (d.orientation === styles) {
                return {
                  ...d,
                };
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        if (manipulating) {
          resolve([...manipulating]);
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  if (!images || images?.length === 0) {
    return null;
  } else {
    if (isLoading && !isError) {
      return null;
    } else if (!isLoading && isError) {
      return null;
    } else {
      if (imagesPortrait?.length === 0 && imagesLandscape?.length === 0) {
        return null;
      } else {
        return (
          <View style={{width}}>
            {/* TOP IMAGE */}
            {imagesLandscape?.length === 0
              ? null
              : imagesLandscape?.length === 1
              ? null
              : imagesLandscape.map((d, i) => {
                  const source = d?.dynamicUrl
                    ? {uri: `${d?.dynamicUrl}=nu-pp`}
                    : {uri: `${d?.url}`};

                  if (i === 0) {
                    return (
                      <View
                        style={{
                          width,
                          height: width * 0.6,
                          borderWidth: 0,
                          marginTop: 5,
                        }}>
                        <AsyncImage
                          source={source}
                          style={{
                            flex: 1,
                            width: '100%',
                            height: width * 0.6,
                          }}
                          resizeMode="cover"
                          loaderStyle={{
                            width: width / 7,
                            height: width / 7,
                          }}
                          placeholderColor={'white'}
                        />
                      </View>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* SIDE BY SIDE IMAGES */}
            {imagesPortrait?.length === 0 ? null : (
              <View style={{width, flexDirection: 'row', marginVertical: 5}}>
                {imagesPortrait.map((d, i) => {
                  const isEven = i % 2 === 0 ? true : false;
                  const source = d?.dynamicUrl
                    ? {uri: `${d?.dynamicUrl}=nu-pp`}
                    : {uri: `${d?.url}`};
                  return (
                    <View
                      style={{
                        flex: 1,
                        height: width * 0.8,
                        borderWidth: 0,
                        paddingLeft: !isEven ? 2.5 : 0,
                        paddingRight: isEven ? 2.5 : 0,
                      }}>
                      <AsyncImage
                        source={source}
                        style={{
                          flex: 1,
                          width: '100%',
                          height: width * 0.8,
                        }}
                        resizeMode="cover"
                        loaderStyle={{
                          width: width / 7,
                          height: width / 7,
                        }}
                        placeholderColor={'white'}
                      />
                    </View>
                  );
                })}
              </View>
            )}

            {/* Bottom Image */}
            {imagesLandscape?.length === 0
              ? null
              : imagesLandscape?.length === 1
              ? imagesLandscape.map((d, i) => {
                  const source = d?.dynamicUrl
                    ? {uri: `${d?.dynamicUrl}=nu-pp`}
                    : {uri: `${d?.url}`};

                  if (i === 0) {
                    return (
                      <View
                        style={{width, height: width * 0.6, borderWidth: 0}}>
                        <AsyncImage
                          source={source}
                          style={{
                            flex: 1,
                            width: '100%',
                            height: width * 0.6,
                          }}
                          resizeMode="cover"
                          loaderStyle={{
                            width: width / 7,
                            height: width / 7,
                          }}
                          placeholderColor={'white'}
                        />
                      </View>
                    );
                  } else {
                    return null;
                  }
                })
              : imagesLandscape.map((d, i) => {
                  const source = d?.dynamicUrl
                    ? {uri: `${d?.dynamicUrl}=nu-pp`}
                    : {uri: `${d?.url}`};

                  if (i === 1) {
                    return (
                      <View
                        style={{width, height: width * 0.6, borderWidth: 0}}>
                        <AsyncImage
                          source={source}
                          style={{
                            flex: 1,
                            width: '100%',
                            height: width * 0.6,
                          }}
                          resizeMode="cover"
                          loaderStyle={{
                            width: width / 7,
                            height: width / 7,
                          }}
                          placeholderColor={'white'}
                        />
                      </View>
                    );
                  } else {
                    return null;
                  }
                })}
          </View>
        );
      }
    }
  }
};

const Wrapper = compose(withApollo)(Galleries);

export default props => <Wrapper {...props} />;
