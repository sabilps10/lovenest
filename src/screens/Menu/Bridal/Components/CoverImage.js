import React from 'react';
import {Text, StatusBar, Dimensions, Image, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import LinearGradient from 'react-native-linear-gradient';

// Components
import ProBadge from '../../../../components/Cards/Merchants/ProBadge';

const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;
const {
  white,
  black,
  mainGreen,
  mainRed,
  transparent,
  overlayDim,
  greyLine,
  superGrey,
  headerBorderBottom,
} = Colors;

const CoverImage = props => {
  const {logoSource, imageSource, name, tagline, serviceType} = props;

  return (
    <View
      style={{
        width,
        height: width * 0.8,
        backgroundColor: greyLine,
        flexDirection: 'column',
      }}>
      <View
        style={{
          flexDirection: 'column',
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1,
        }}>
        <Image
          source={imageSource}
          style={{
            position: 'absolute',
            zIndex: 0,
            width,
            height: width * 0.65,
          }}
          resizeMode="cover"
        />
        <LinearGradient
          locations={Platform.OS === 'ios' ? [0.15, 0.78] : [0.5, 0.4, 0.78]}
          colors={
            Platform.OS === 'ios'
              ? [transparent, '#FFFFFF', '#FFFFFF']
              : [transparent, transparent, '#FFFFFF']
          }
          style={{
            position: 'absolute',
            zIndex: 2,
            width: '100%',
            height: '100%',
            bottom: 0,
          }}
        />
      </View>
      <View
        style={{
          paddingTop: 140,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            minWidth: width / 5,
            maxWidth: width / 5,
            minHeight: width / 5,
            maxHeight: width / 5,
            backgroundColor: white,
            borderRadius: 2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={logoSource}
            style={{
              flex: 1,
              width: '100%',
              height: height / 15,
            }}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            top: 10,
            width: '100%',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                fontSize: RFPercentage(2),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 25,
              }}>
              {name}
            </Text>
          </View>
          <View style={{paddingLeft: 25, paddingRight: 25}}>
            <Text
              style={{
                fontStyle: 'italic',
                textAlign: 'center',
                fontFamily: book,
                fontSize: RFPercentage(1.6),
                color: superGrey,
                letterSpacing: 0.3,
                lineHeight: 21,
              }}>
              {tagline}
            </Text>
          </View>
        </View>
        <View style={{marginVertical: 15, marginTop: 20}}>
          <ProBadge title={`Pro ${serviceType} Merchant`} />
        </View>
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(CoverImage);

export default props => <Wrapper {...props} />;
