import React, {useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {charImage} from '../../../../utils/Themes/Images';
import ButtonSeeAllProduct from '../../../../components/Home/PublicProduct/ButtonSeeAllProduct';
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';
import SpecialOfferCard from '../../../../components/Cards/Merchants/SpecialOfferCard';
import Loader from '../../../../components/Loader/circleLoader';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';

const {white} = Colors;
const {charLeftShuriken, charRightShuriken} = charImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const {medium, book} = FontType;

const SpecialOffer = props => {
  console.log('ANUUU JANCUUKKSSS: ', props);
  const {navigation, client, id, isLoading} = props;

  const [isLoadingSpecialOffer, setIsLoadingSpecialOffer] = useState(true);
  const [isErrorSpecialOffer, setIsErrorSpecialOffer] = useState(false);

  const [specialOfferList, setSpecialOfferList] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, isLoading, id]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          networkPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log(
            'Related Professional >> Merchant Detail Response: ',
            response,
          );
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors === undefined) {
            const {relatedMerchant} = getMerchantVer2[0];
            if (relatedMerchant) {
              await setSpecialOfferList([...relatedMerchant]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            } else {
              await setSpecialOfferList([]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            }
          } else {
            await setIsErrorSpecialOffer(true);
            await setIsLoadingSpecialOffer(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorSpecialOffer(true);
          await setIsLoadingSpecialOffer(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorSpecialOffer(true);
      setIsLoadingSpecialOffer(false);
    }
  };

  const extKey = item => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: widthScreen / 1.1,
      offset: (widthScreen / 1.1) * index,
      index,
    };
  };

  if (isLoadingSpecialOffer && !isErrorSpecialOffer) {
    return (
      <View
        style={{
          width: widthScreen,
          height: heightScreen / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoadingSpecialOffer && isErrorSpecialOffer) {
    return null;
  } else {
    return (
      <>
        {specialOfferList.length === 0 ? null : (
          <LinearGradient
            start={{x: 0.0, y: 0.15}}
            end={{x: 0.0, y: 9}}
            locations={[0.0, 0.2]}
            colors={['#CAB77C', 'white']}
            style={{
              zIndex: 0,
              width: '100%',
              flexDirection: 'row',
              marginBottom: 25,
            }}>
            <Image
              source={charLeftShuriken}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 2,
                width: 55,
                height: 90,
              }}
              resizeMode="contain"
            />
            <Image
              source={charRightShuriken}
              style={{
                position: 'absolute',
                bottom: 10,
                right: 0,
                zIndex: 2,
                width: 55,
                height: 90,
              }}
              resizeMode="contain"
            />
            <View
              style={{
                width: widthScreen,
                paddingBottom: 25,
              }}>
              <ButtonSeeAllProduct
                title={'Related Professionals'}
                related={true}
                disableButton={true}
                {...props}
              />
              <View style={{top: 30}}>
                <FlatList
                  scrollEnabled={false}
                  disableVirtualization
                  legacyImplementation
                  data={specialOfferList}
                  keyExtractor={extKey}
                  listKey={extKey}
                  getItemLayout={getItemLayout}
                  contentContainerStyle={{
                    padding: 15,
                  }}
                  renderItem={({item}) => {
                    const {name, tagline, serviceType, id: merchantId} = item;
                    const sourceImage = item?.logoImageDynamicUrl
                      ? item.logoImageDynamicUrl
                      : item.logoImageUrl;
                    return (
                      <>
                        <RelatedMerchantCard
                          merchantId={merchantId}
                          serviceType={serviceType}
                          name={name}
                          tagline={tagline}
                          sourceImage={sourceImage}
                          {...props}
                        />
                      </>
                    );
                  }}
                />
              </View>
            </View>
          </LinearGradient>
        )}
      </>
    );
  }
};

export const RelatedMerchantCard = props => {
  const {
    navigation,
    name,
    tagline,
    sourceImage,
    merchantId,
    serviceType,
  } = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  return (
    <TouchableOpacity
      style={{marginVertical: 5}}
      activeOpacity={1}
      onPress={() => {
        try {
          console.log('Service type ma brooo: ', serviceType);
          if (serviceType === 'Bridal') {
            navigation.push('BridalDetail', {id: merchantId, serviceType});
          } else if (serviceType === 'Jewellery') {
            navigation.push('JewelleryMerchantDetail', {
              id: merchantId,
              serviceType,
            });
          } else if (serviceType === 'Interior Design') {
            navigation.push('InteriorMerchantDetail', {
              id: merchantId,
              serviceType,
            });
          } else if (serviceType === 'Venue') {
            navigation.push('VenueMerchantDetail', {
              id: merchantId,
              serviceType,
            });
          } else if (serviceType === 'Smart Home') {
            navigation.push('SmartHomeMerchantDetail', {
              id: merchantId,
              serviceType,
            });
          }
        } catch (error) {
          console.log('error: ', error);
        }
      }}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <Animated.View
        style={{
          width: '100%',
          minHeight: 10,
          flexDirection: 'row',
          transform: [{scale}],
        }}>
        <View style={{flex: 0.2}}>
          <View
            style={{
              width: '100%',
              height: widthScreen * 0.15,
              backgroundColor: 'white',
            }}>
            <Image
              source={{uri: sourceImage}}
              style={{
                flex: 1,
                height: widthScreen * 0.15,
                width: '100%',
              }}
              resizeMode="cover"
            />
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
          }}>
          <View
            style={{
              top: -2,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginBottom: 5,
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: white,
                letterSpacing: 0.3,
              }}>
              {name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginBottom: 5,
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.6),
                color: white,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {tagline}
            </Text>
          </View>
        </View>
      </Animated.View>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(SpecialOffer);

export default props => <Wrapper {...props} />;
