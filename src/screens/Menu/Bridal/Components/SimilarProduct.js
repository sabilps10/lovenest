import React, {useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {Icon, Card, CardItem} from 'native-base';
import SIMILAR_PRODUCT from '../../../../graphql/queries/productSimilar';
import Loader from '../../../../components/Loader/circleLoader';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import CardProduct from './CardProduct';
import {commonImage} from '../../../../utils/Themes/Images';

const {mainRed, black} = Colors;
const {loveCrown} = commonImage;
const {width, height} = Dimensions.get('window');
const {medium} = FontType;

const SimilarProduct = props => {
  console.log('SimilarProduct Component', props);
  const {
    navigation,
    client,
    id,
    isLoading: loadingParent,
    isLogin,
    runWishList,
    onPress,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize] = useState(2);
  const [pageNumber] = useState(1);

  const [product, setProduct] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, loadingParent, id]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: SIMILAR_PRODUCT,
          variables: {
            id: parseInt(id, 10),
            pageSize,
            pageNumber,
          },
          networkPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Similar Product: ', response);
          const {data, errors} = response;
          const {productsSimilar} = data;
          const {data: list, error} = productsSimilar;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProduct([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const keyExt = item => `${String(item.id)} Parent`;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: width,
          height: height / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (product?.length === 0) {
      return null;
    } else {
      return (
        <LinearGradient
          start={{x: 0.0, y: 0.15}}
          end={{x: 0.0, y: 5}}
          locations={[0.0, 0.15, 0.9]}
          colors={['#FFEDED', 'white', 'white']}
          style={{
            zIndex: 0,
            flex: 1,
            marginTop: 15,
          }}>
          <Image
            source={loveCrown}
            style={{
              width: width / 8,
              height: height / 8,
              position: 'absolute',
              top: -10,
              left: 0,
              zIndex: 1,
            }}
            resizeMode="contain"
          />
          <View style={{zIndex: 10, right: 1}}>
            <FlatList
              ListHeaderComponent={() => {
                return (
                  <Card
                    transparent
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      marginBottom: 0,
                      marginTop: 15,
                    }}>
                    <CardItem
                      style={{
                        width: '100%',
                        paddingBottom: 0,
                        backgroundColor: 'transparent',
                        marginBottom: 10,
                      }}>
                      <View
                        style={{
                          width: '100%',
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <View>
                          <Text
                            style={{
                              fontSize: RFPercentage(2),
                              color: black,
                              fontFamily: medium,
                              letterSpacing: 0.3,
                            }}>
                            Similar Product
                          </Text>
                        </View>
                        <View
                          style={{
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                          }}>
                          <TouchableOpacity
                            disabled
                            onPress={() => {}}
                            style={{
                              display: 'none',
                              left: 20,
                              alignSelf: 'flex-end',
                              flexDirection: 'row',
                              padding: 5,
                              paddingLeft: 10,
                              paddingRight: 10,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: RFPercentage(1.8),
                                color: mainRed,
                                fontFamily: medium,
                                letterSpacing: 0.3,
                              }}>
                              See All
                            </Text>
                            <View style={{marginLeft: 10}}>
                              <Icon
                                type="Feather"
                                name="arrow-right"
                                style={{
                                  color: mainRed,
                                  fontSize: RFPercentage(2.2),
                                }}
                              />
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </CardItem>
                  </Card>
                );
              }}
              ListFooterComponent={() => {
                return (
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 10,
                      bottom: 5,
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        try {
                          navigation.goBack(null);
                        } catch (error) {
                          console.log('Error: ', error);
                        }
                      }}
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.7),
                          color: mainRed,
                          letterSpacing: 0.3,
                          marginRight: 5,
                        }}>
                        Explore The Collection
                      </Text>
                      <View
                        style={{
                          width: 20,
                          height: 20,
                          borderRadius: 20 / 2,
                          borderWidth: 1,
                          borderColor: mainRed,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Icon
                          type="Feather"
                          name="arrow-right"
                          style={{fontSize: 15, color: mainRed}}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }}
              data={product}
              extraData={product}
              keyExtractor={keyExt}
              numColumns={2}
              columnWrapperStyle={{
                paddingLeft: 10,
                paddingRight: 10,
                marginBottom: 15,
              }}
              renderItem={({item, index}) => {
                return (
                  <CardProduct
                    isLogin={isLogin}
                    runWishList={(theid, isWishlist) => {
                      runWishList(theid, isWishlist);
                    }}
                    onPress={() => onPress(id)}
                    index={index}
                    {...props}
                    item={item}
                  />
                );
              }}
            />
          </View>
        </LinearGradient>
      );
    }
  }
};

const Wrapper = compose(withApollo)(SimilarProduct);

export default props => <Wrapper {...props} />;
