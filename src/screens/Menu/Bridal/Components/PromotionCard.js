import React from 'react';
import {Dimensions, View, TouchableOpacity, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import Colors from '../../../../utils/Themes/Colors';
import {hasNotch} from 'react-native-device-info';

const {width, height} = Dimensions.get('window');
const {headerBorderBottom, greyLine} = Colors;

const PromotionCard = props => {
  const {item, onPress} = props;
  const {promoImageDynamicURL, promoImageURL} = item;
  const source = promoImageDynamicURL
    ? {uri: `${promoImageDynamicURL}=h500`}
    : {uri: promoImageURL};
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          borderWidth: 0.5,
          borderColor: greyLine,
          width: '100%',
          height: width * 0.5,
          backgroundColor: headerBorderBottom,
          // shadowColor: '#000',
          // shadowOffset: {
          //   width: 0,
          //   height: 1,
          // },
          // shadowOpacity: 0.2,
          // shadowRadius: 1.41,
          // elevation: 2,
        }}>
        <AsyncImage
          source={source}
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 0,
          }}
          resizeMode="cover"
          placeholderColor={headerBorderBottom}
          loaderStyle={{
            width: width / 7,
            height: height / 7,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(PromotionCard);

export default props => <Wrapper {...props} />;
