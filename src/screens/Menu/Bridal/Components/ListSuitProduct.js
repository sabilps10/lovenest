import React, {useCallback, useMemo} from 'react';
import {
  Text,
  StatusBar,
  FlatList,
  ActivityIndicator,
  Dimensions,
  Image,
  View,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../../utils/Themes/Images';

const {white, black, mainGreen, mainRed, greyLine, headerBorderBottom} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');
const {charCategoryIcon, charEmptyFlorist, charComingSoon} = charImage;

// Components
import CardProduct from '../Components/CardProduct';

const ListGownProduct = props => {
  console.log('ListGownProduct Props: ', props);
  const {
    listRef,
    list,
    isLogin,
    isLoading,
    isError,
    runWishList,
    onRefresh,
    refreshing,
    isLoadMore,
    setIsLoadMore,
    totalCount,
  } = props;

  const keyExt = useCallback((item, index) => {
    return `${item?.id}`;
  }, []);

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <CardProduct
          item={item}
          index={index}
          isLogin={isLogin}
          runWishList={(id, isWishlist) => runWishList(id, isWishlist, list)}
          onPress={id => onPress(id)}
        />
      );
    },
    [isLogin, list],
  );

  const onPress = id => {
    try {
      console.log('PRODUCT ID: ', id);
      props?.navigation?.push('BridalProductDetail', {id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color={mainGreen} />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list) {
      if (list?.length === 0) {
        return (
          <View
            style={{
              padding: 15,
              paddingTop: 10,
              flex: 1,
              maxHeight: height / 1.41,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={charComingSoon}
              style={{width, height: height / 3}}
              resizeMode="contain"
            />
            <Text
              style={{
                marginVertical: 15,
                fontFamily: medium,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2.5),
              }}>
              Out of Stock
            </Text>
            <Text
              style={{
                lineHeight: 20,
                fontFamily: book,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2),
              }}>
              Keep updated with us, preparing our products very soon !
            </Text>
          </View>
        );
      } else {
        return (
          <FlatList
            ref={listRef}
            decelerationRate={'fast'}
            ListFooterComponent={() => {
              return <Footer isLoadMore={isLoadMore} />;
            }}
            onEndReached={() => {
              console.log('Masuk End Reached');
              if (list?.length < totalCount) {
                setIsLoadMore(true);
              } else {
                setIsLoadMore(false);
              }
            }}
            onEndReachedThreshold={0.6}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => onRefresh()}
              />
            }
            disableVirtualization
            legacyImplementation
            initialNumToRender={4}
            data={list}
            extraData={list}
            keyExtractor={keyExt}
            numColumns={2}
            renderItem={renderItem}
            contentContainerStyle={{
              paddingBottom: 25,
            }}
            columnWrapperStyle={{
              paddingLeft: 10,
              paddingRight: 10,
            }}
          />
        );
      }
    } else {
      return null;
    }
  }
};

export const Footer = props => {
  const {isLoadMore} = props;
  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // borderWidth: 1,
      }}>
      {isLoadMore ? (
        <ActivityIndicator
          size="small"
          color={mainGreen}
          style={{marginHorizontal: 5}}
        />
      ) : null}
      <Text
        style={{
          fontStyle: 'italic',
          fontSize: 12,
          color: greyLine,
          textAlign: 'center',
        }}>
        {isLoadMore ? 'Loading more...' : ''}
      </Text>
    </View>
  );
};

const Memoize = React.memo(ListGownProduct);

const Wrapper = compose(withApollo)(Memoize);

export default props => <Wrapper {...props} />;
