import React from 'react';
import {View, Dimensions, ActivityIndicator} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import ImageCards from './ImageCards';

const {width} = Dimensions.get('window');
const {headerBorderBottom, mainRed} = Colors;

const ShowImageCardByIndex = props => {
  const {listImageCard, isLoading, isError, showByIndex} = props;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          height: width * 1.2,
          backgroundColor: headerBorderBottom,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color={mainRed} size="large" />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (listImageCard?.length === 0) {
      return null;
    } else {
      const showedData = listImageCard[showByIndex]
        ? listImageCard[showByIndex]
        : [];
      console.log('YUHU: ', showedData);
      if (showedData?.length === 0) {
        return null;
      } else {
        const {dynamicUrl, url} = showedData;
        const source = dynamicUrl ? {uri: `${dynamicUrl}=s0`} : {uri: url};

        return <ImageCards source={source} />;
      }
    }
  }
};

const Wrapper = compose(withApollo)(ShowImageCardByIndex);

export default props => <Wrapper {...props} />;
