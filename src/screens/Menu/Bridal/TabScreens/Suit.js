import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  FlatList,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Image,
  View,
  ScrollView,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType, Fonttype} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
import {Icon} from 'native-base';

// Redux
import ReduxSuitFilter from '../../../../redux/thunk/Bridal/Suit/index';

// Async Storage
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure/index';

// Query
import GET_PRODUCTS from '../../../../graphql/queries/productsPublic';

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

const {
  white,
  black,
  headerBorderBottom,
  overlayDim,
  greyLine,
  mainRed,
  mainGreen,
} = Colors;
const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;

// Components
import ModalLoader from '../Components/ModalLoader';
import ListProduct from '../Components/ListSuitProduct';
import ButtonFiltered from '../../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFiltered from '../../../../components/Button/ButtonFilters/Unfilter';

const {asyncToken} = AsyncData;
import _ from 'lodash';

const Suit = props => {
  console.log('Suit Tab Props: ', props);
  const {navigation, client, id, serviceType} = props;

  const listRef = useRef(null);

  let [isLogin, setIsLogin] = useState(false);
  let [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [showModalRemoveFilter, setShowModalRemoveFilter] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [list, setList] = useState([]);
  const [totalCount, seTotalCount] = useState(0);

  const pageSize = 16;
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    checkIsLogin();
    fetchProduct();

    if (isLoadMore) {
      shouldFetch();
    }

    if (refreshing) {
      onRefresh();
    }

    const subs = navigation.addListener('focus', () => {
      console.log('PROPS LITSSSS: ', props?.lists);
      checkIsLogin();
      if (props?.lists?.length === 0) {
        console.log('MASSOOOOKKKKKKK SINIII');
        fetchProduct();
      } else {
        console.log('WUCAAAAAAAUUUUUUU');
        refreshAfterFilter();
      }
    });

    const subsBlur = navigation.addListener('blur', () => {
      resetAll();
    });

    return () => {
      subs();
      subsBlur();
    };
  }, [navigation, props?.lists, isLoadMore, showModalRemoveFilter, refreshing]);

  const resetAll = async () => {
    await setList([]);
    await setIsLoading(true);
  };

  const shouldFetch = useCallback(async () => {
    await setPageNumber(pageNumber + 1);
    await fetchProduct();
  }, [isLoadMore]);

  const onRefresh = useCallback(async () => {
    await setRefreshing();
    await setIsLoading(true);
    await setList([]);
    await setPageNumber(1);
    await setTimeout(async () => {
      await fetchProduct();
    }, 2000);
  }, [refreshing]);

  const refreshAfterFilter = useCallback(async () => {
    await setIsLoading(true);
    await setList([]);
    await setPageNumber(1);
    await setTimeout(async () => {
      await fetchProduct();
    }, 2000);
  }, [showModalRemoveFilter, list, pageNumber, props?.lists]);

  const runWishList = async (productId, statusWhislist, oldlist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId, oldlist);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId, oldlist);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkToken = () => {
    return new Promise(async resolve => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);

        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const userIsLogin = await checkToken();
      if (userIsLogin) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const manipulateSelectedFilterRedux = filterType => {
    return new Promise(async (resolve, reject) => {
      try {
        const getListSelected = await Promise.all(
          props?.lists
            ?.map((d, i) => {
              if (d?.name === filterType) {
                return d;
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );

        if (getListSelected) {
          // console.log('getListSelected >>> ', getListSelected);
          const getChecked = await Promise.all(
            getListSelected[0]?.list
              ?.map((d, i) => {
                if (d?.checked) {
                  return d?.name;
                } else {
                  return false;
                }
              })
              .filter(Boolean),
          );

          if (getChecked) {
            resolve(getChecked);
          } else {
            resolve([]);
          }
        } else {
          resolve([]);
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  const fetchProduct = async () => {
    try {
      console.log('Prop FETCH PRODUCT: ', props?.lists);
      const getCategory = await manipulateSelectedFilterRedux('Category');

      const getColor = await manipulateSelectedFilterRedux('Color');
      console.log('getColor >>> ', getColor);
      const getRange = await manipulateSelectedFilterRedux('Range');

      if (
        getCategory?.length === 0 &&
        getColor?.length === 0 &&
        getRange?.length === 0
      ) {
        const variables = {
          serviceType,
          merchantId: [parseInt(id, 10)],
          bridalType: ['Suit'],
          bridalCategory: [],
          bridalColor: [],
          bridalRange: [],
          pageSize,
          pageNumber,
        };

        console.log('Suit Variables: ', variables);

        await client
          .query({
            query: GET_PRODUCTS,
            variables,
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('Suit Response List: ', response);
            const {data, errors} = response;
            const {productsPublic} = data;
            const {data: listProduct, error, totalData} = productsPublic;

            if (errors || error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await seTotalCount(totalData);
              const oldData = list.concat(listProduct);
              // const newData = [...new Set(oldData)];
              const filtering = await _.uniqBy(oldData, 'id');
              console.log('filtering >>>>>>>>>>>>> ', filtering);
              await setList([...filtering]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          })
          .catch(error => {
            throw error;
          });
      } else {
        const variables = {
          serviceType,
          merchantId: [parseInt(id, 10)],
          bridalType: ['Suit'],
          bridalCategory: getCategory?.length === 0 ? [] : getCategory,
          bridalColor: getColor?.length === 0 ? [] : getColor,
          bridalRange: getRange?.length === 0 ? [] : getRange,
          pageSize,
          pageNumber,
        };

        console.log('Suit Variables: ', variables);

        await client
          .query({
            query: GET_PRODUCTS,
            variables,
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('Suit Response List: ', response);
            const {data, errors} = response;
            const {productsPublic} = data;
            const {data: listProduct, error, totalData} = productsPublic;

            if (errors || error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await seTotalCount(totalData);
              const oldData = list.concat(listProduct);
              // const newData = [...new Set(oldData)];
              const filtering = await _.uniqBy(oldData, 'id');
              console.log('filtering >>>>>>>>>>>>> ', filtering);
              await setList([...filtering]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          })
          .catch(error => {
            throw error;
          });
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const removeFilter = async (parentId, index) => {
    try {
      console.log('Parent ID and Index: ', {parentId, index});
      await setShowModalRemoveFilter(true);
      let oldList = props?.lists;
      oldList[parentId].list[index].checked =
        !oldList[parentId].list[index].checked;
      console.log('OLD LISTS: ', oldList);
      await props?.reduxSuitFilter(oldList);
      await setTimeout(async () => {
        await refreshAfterFilter();
        await setShowModalRemoveFilter(false);
      }, 1000);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const scrollToTop = () => {
    try {
      listRef?.current?.scrollToOffset({animated: true, offset: 0});
    } catch (error) {}
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalRemoveFilterLoader visible={showModalRemoveFilter} />
      <ModalLoader isLoading={isLoadingWishList} />
      <ListFilteredData removeFilter={removeFilter} {...props} />
      <ListProduct
        listRef={listRef}
        {...props}
        isError={isError}
        isLoading={isLoading}
        isLogin={isLogin}
        runWishList={(productId, statusWhislist, oldList) => {
          console.log('KONTOL: ', list);
          runWishList(productId, statusWhislist, oldList);
        }}
        list={list}
        refreshing={refreshing}
        onRefresh={onRefresh}
        pageSize={pageSize}
        pageNumber={pageNumber}
        isLoadMore={isLoadMore}
        setIsLoadMore={async status => await setIsLoadMore(status)}
        totalCount={totalCount}
      />
      <ButtonScrollToTop onPress={scrollToTop} />
    </View>
  );
};

export const ButtonScrollToTop = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 35,
        height: 35,
        position: 'absolute',
        bottom: 25,
        right: 25,
        borderRadius: 35 / 2,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{fontSize: RFPercentage(2.2), color: mainRed}}
      />
    </TouchableOpacity>
  );
};

export const ModalRemoveFilterLoader = props => {
  const {visible} = props;
  return (
    <Modal visible={visible} animationType="fade" transparent>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginBottom: 10}}
          />
          <Text style={{color: greyLine, fontStyle: 'italic'}}>
            Removing...
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const ListFilterButton = props => {
  const {list, parentIndex, removeFilter} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      if (item?.checked) {
        return (
          <TouchableOpacity
            onPress={() => {
              try {
                removeFilter(parentIndex, index);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            style={{
              padding: 5,
              paddingLeft: 15,
              paddingRight: 15,
              borderRadius: 15,
              backgroundColor: headerBorderBottom,
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 15,
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: 'grey',
              }}>
              {item?.name}
            </Text>
            <Icon
              type="Feather"
              name="x"
              style={{
                top: 1,
                left: 5,
                color: 'grey',
                fontSize: RFPercentage(1.8),
              }}
            />
          </TouchableOpacity>
        );
      } else {
        return null;
      }
    },
    [list],
  );

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <FlatList
        scrollEnabled={false}
        horizontal
        contentContainerStyle={{
          padding: 2.5,
          paddingTop: 10,
          paddingBottom: 10,
        }}
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderItem}
      />
    );
  }
};

export const ListFilteredData = props => {
  console.log('ListFilteredData >>> ', props);
  const {lists: list, removeFilter} = props;
  // this list from redux filter data

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <View style={{width}}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal
          style={{
            flexDirection: 'row',
            width: '100%',
          }}
          contentContainerStyle={{
            padding: 2,
            paddingLeft: 7.5,
            paddingRight: 7.5,
          }}>
          {list?.map((item, index) => {
            return (
              <View key={`${item.name}`}>
                <ListFilterButton
                  removeFilter={removeFilter}
                  list={item?.list}
                  parentIndex={index}
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
};

const mapToState = state => {
  const {bridalSuitFilterList} = state;
  return {
    lists: bridalSuitFilterList?.list,
  };
};

const mapToDispatch = dispatch => {
  console.log('mapToDispatch: >> ', dispatch);
  return {
    reduxSuitFilter: list => dispatch(ReduxSuitFilter(list)),
  };
};

const Connector = connect(mapToState, mapToDispatch)(Suit);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
