import React, {useRef, useState, useEffect, useMemo, useCallback} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {charImage} from '../../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;
const {white, black, mainRed, superGrey, greyLine, mainGreen} = Colors;
const {charNoPortfolio, charComingSoon} = charImage;

import GET_PORTO from '../../../../graphql/queries/getAllProjects';

const Portfolio = props => {
  console.log('Portfolio Tab: ', props);
  const {navigation, client, id, serviceType} = props;

  const listRef = useRef(null);

  const [listPorto, setListPorto] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalCount, setTotalCount] = useState(0);

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchPorto();

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      fetchPorto();
    });

    return () => {
      subs();
    };
  }, [navigation, isLoadMore]);

  const onLoadMore = useCallback(() => {
    try {
      setPageNumber(pageNumber + 1);
      fetchPorto();
    } catch (error) {
      setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await setPageNumber(1);
      await fetchPorto();
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetchPorto = async () => {
    try {
      await client
        .query({
          query: GET_PORTO,
          variables: {
            serviceType,
            merchantId: parseInt(id, 10),
            itemDisplayed: pageSize,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPorto Response: ', response);
          const {data, errors} = response;
          const {getAllProjects} = data;
          const {data: list, error, totalData} = getAllProjects;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setListPorto([...listPorto, ...list]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  const renderItem = useCallback(
    ({item, index}) => {
      return <PortoCard {...props} item={item} index={index} />;
    },
    [listPorto],
  );

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [listPorto],
  );

  const scrollToTop = () => {
    try {
      listRef?.current?.scrollToOffset({animated: true, offset: 0});
    } catch (error) {}
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{color: greyLine, fontStyle: 'italic'}}>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Content>
          <View
            style={{
              flex: 1,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: greyLine, fontStyle: 'italic'}}>
              Failed To Fetch Data
            </Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <FlatList
          ref={listRef}
          decelerationRate="fast"
          legacyImplementation
          disableVirtualization
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                {isLoadMore ? (
                  <ActivityIndicator
                    size="small"
                    color={mainGreen}
                    style={{marginHorizontal: 5}}
                  />
                ) : null}
                {isLoadMore ? (
                  <Text
                    style={{
                      fontStyle: 'italic',
                      fontSize: 12,
                      color: greyLine,
                      textAlign: 'center',
                    }}>
                    Loading more...
                  </Text>
                ) : null}
              </View>
            );
          }}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  padding: 15,
                  paddingTop: 0,
                  paddingBottom: 0,
                  flex: 1,
                  height: height / 1.41,
                  justifyContent: 'center',
                  alignItems: 'center',
                  // bottom: 35,
                  // borderWidth: 1,
                }}>
                <Image
                  source={charComingSoon}
                  style={{width, height: height / 3}}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginVertical: 15,
                    fontFamily: medium,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                  }}>
                  No Portfolio Yet
                </Text>
                <Text
                  style={{
                    lineHeight: 20,
                    fontFamily: book,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2),
                  }}>
                  This section will contain our portfolio, base on occassion.
                </Text>
              </View>
            );
          }}
          onEndReachedThreshold={0.3}
          onEndReached={() => {
            if (listPorto?.length <= totalCount) {
              setIsLoadMore(true);
            } else {
              setIsLoadMore(false);
            }
          }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          contentContainerStyle={{
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 25,
          }}
          data={listPorto}
          extraData={listPorto}
          keyExtractor={keyExt}
          renderItem={renderItem}
        />
        <ButtonScrollToTop onPress={scrollToTop} />
      </View>
    );
  }
};

export const ButtonScrollToTop = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 35,
        height: 35,
        position: 'absolute',
        bottom: 25,
        right: 25,
        borderRadius: 35 / 2,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{fontSize: RFPercentage(2.2), color: mainRed}}
      />
    </TouchableOpacity>
  );
};

export const PortoCard = props => {
  const {item, index, navigation} = props;

  if (item) {
    const {id, name, featuredImageDynamicURL, featuredImageURL, galleries} =
      item;

    const RenderCard = () => {
      return useMemo(() => {
        return (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => navigation.navigate('BridalPortfolioDetail', {id})}>
            <Card style={{elevation: 0, shadowOpacity: 0}}>
              <CardItem cardBody style={{backgroundColor: 'transparent'}}>
                <View style={{width: '100%', height: width * 0.45}}>
                  {galleries?.length === 0 || galleries === 1 ? (
                    <AsyncImage
                      source={
                        featuredImageDynamicURL
                          ? {uri: `${featuredImageDynamicURL}=h500`}
                          : {uri: `${featuredImageURL}`}
                      }
                      style={{flex: 1, width: '100%', height: '100%'}}
                      placeholderColor={white}
                      resizeMode="cover"
                      loaderStyle={{width: width / 7, height: height / 7}}
                    />
                  ) : (
                    <DynamicImage images={galleries} />
                  )}
                </View>
              </CardItem>
              <CardItem>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  {name}
                </Text>
              </CardItem>
              <CardItem
                style={{
                  paddingTop: 0,
                  width: '100%',
                  backgroundColor: 'transparent',
                }}>
                <View>
                  <Icon
                    type="Feather"
                    name="tag"
                    style={{fontSize: RFPercentage(2), color: superGrey}}
                  />
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    paddingRight: 15,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={{
                      right: 10,
                      color: superGrey,
                      fontSize: RFPercentage(1.4),
                      fontFamily: book,
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    {item?.bridalTheme}
                  </Text>
                </View>
              </CardItem>
            </Card>
          </TouchableOpacity>
        );
      }, [item, index]);
    };

    return RenderCard();
  } else {
    return null;
  }
};

export const DynamicImage = props => {
  const {images} = props;

  if (images && images?.length !== 0) {
    if (images?.length >= 3) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View style={{flex: 1, paddingRight: 2}}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{width: '100%', height: '100%', flex: 1}}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 1, paddingLeft: 2}}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
            <View style={{flex: 1, paddingLeft: 2, paddingTop: 4}}>
              <AsyncImage
                source={
                  images[2]?.dynamicUrl
                    ? {uri: `${images[2]?.dynamicUrl}=h500`}
                    : {uri: `${images[2]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length >= 2) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View style={{flex: 1, paddingRight: 2}}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{width: '100%', height: '100%', flex: 1}}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 1, paddingLeft: 2}}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length === 1) {
      return (
        <View style={{flex: 1}}>
          <AsyncImage
            source={
              images[0]?.dynamicUrl
                ? {uri: `${images[0]?.dynamicUrl}=h500`}
                : {uri: `${images[0]?.url}`}
            }
            resizeMode="cover"
            style={{width: '100%', height: '100%', flex: 1}}
            placeholderColor={white}
            loaderStyle={{width: width / 15, height: height / 15}}
          />
        </View>
      );
    }
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(Portfolio);

export default props => <Wrapper {...props} />;
