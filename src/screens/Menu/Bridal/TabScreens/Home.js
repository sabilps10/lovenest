import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Modal,
  SafeAreaView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Galleries from '../Components/ImageGalleries';
import {hasNotch} from 'react-native-device-info';

// Queries
import GET_IMAGE_CARD from '../../../../graphql/queries/getImageCardFlorist';
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';
import GET_POPULAR_PRODUCT from '../../../../graphql/queries/floristPopularProduct';

const {width, height} = Dimensions.get('window');
const {white, black, overlayDim, mainRed, mainGreen} = Colors;
const {book, medium} = FontType;

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure';
const {asyncToken} = AsyncData;

import ListPromotion from '../Components/ListPromotion';
import ModalLoader from '../Components/ModalLoader';
import PopularProduct from '../Components/PopularProduct';
import TopBanner from '../Components/TopBanner';
import ListImageCard from '../Components/ListImageCard';
import ShowImageCardByIndex from '../Components/ShowImageCardByIndex';
import ButtonEnquiry from '../Components/ButtonEnquiry';

import VideoPlayer from '../Components/VideoPlayer';

const Home = props => {
  console.log('Bridal Home Tab Props: ', props);

  const {id, serviceType, navigation, client, route} = props;

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoadingPromotion, setIsLoadingPromotion] = useState(true);
  const [isErrorPromotion, setIsErrorPromotion] = useState(false);
  const [itemDisplayedPromotion, setItemDisplayedPromotion] = useState(1);

  const [popularGown, setPopularGown] = useState([]);
  const [isLoadingPopularGown, setIsLoadingPopularGown] = useState(true);
  const [isErrorPopularGown, setIsErrorPopularGown] = useState(false);

  const [popularSuit, setPopularSuit] = useState([]);
  const [isLoadingPopularSuit, setIsLoadingPopularSuit] = useState(true);
  const [isErrorPopularSuit, setIsErrorPopularSuit] = useState(false);

  const [imageCardList, setImageCardList] = useState([]);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [isLoadingImageCard, setIsLoadingImageCard] = useState(true);
  const [isErrorImageCard, setIsErrorImageCard] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    runCheckingLogin();
    fetchMerchantDetail();
    fetchImagecard();
    getPopularGown();
    getPopularSuit();
    fetchPromotion();
    const subs = navigation.addListener('focus', () => {
      runCheckingLogin();
      fetchMerchantDetail();
      fetchImagecard();
      getPopularGown();
      getPopularSuit();
      fetchPromotion();
    });

    return () => {
      subs();
    };
  }, [navigation, id, serviceType]);

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await getPopularSuit();
              await getPopularGown();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await getPopularGown();
              await getPopularSuit();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const getPopularProduct = bridalType => {
    return new Promise(async (resolve, reject) => {
      try {
        await client
          .query({
            query: GET_POPULAR_PRODUCT,
            variables: {
              serviceType,
              merchantId: parseInt(id, 10),
              bridalType,
              pageSize: 2,
              pageNumber: 1,
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(response => {
            console.log('Response Get Popular Product: ', response);
            const {data, errors} = response;
            const {productsPopular} = data;
            const {data: list, error} = productsPopular;

            if (errors) {
              resolve([]);
            } else {
              if (error) {
                resolve([]);
              } else {
                resolve([...list]);
              }
            }
          })
          .catch(error => {
            throw error;
          });
      } catch (error) {
        console.log('Error: ', error);
        resolve([]);
      }
    });
  };

  const getPopularGown = async () => {
    try {
      const listPopularGown = await getPopularProduct(['Gown']);
      console.log('listPopularGown >>> : ', listPopularGown);
      if (listPopularGown) {
        await setPopularGown([...listPopularGown]);
        await setIsErrorPopularGown(false);
        await setIsLoadingPopularGown(false);
      } else {
        await setPopularGown([]);
        await setIsErrorPopularGown(false);
        await setIsLoadingPopularGown(false);
      }
    } catch (error) {
      await setPopularGown([]);
      await setIsErrorPopularGown(true);
      await setIsLoadingPopularGown(false);
    }
  };

  const getPopularSuit = async () => {
    try {
      const listPopularSuit = await getPopularProduct(['Suit']);
      console.log('listPopularSuit >>>> : ', listPopularSuit);
      if (listPopularSuit) {
        await setPopularSuit([...listPopularSuit]);
        await setIsErrorPopularSuit(false);
        await setIsLoadingPopularSuit(false);
      } else {
        await setPopularSuit([]);
        await setIsErrorPopularSuit(false);
        await setIsLoadingPopularSuit(false);
      }
    } catch (error) {
      await setPopularSuit([]);
      await setIsErrorPopularSuit(true);
      await setIsLoadingPopularSuit(false);
    }
  };

  const fetchPromotion = async () => {
    try {
      await client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(id, 10),
            itemDisplayed: parseInt(itemDisplayedPromotion, 10),
            pageNumber: 1,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error} = getAllPromotions;

          if (errors) {
            await setIsErrorPromotion(true);
            await setIsLoadingPromotion(false);
          } else {
            if (error) {
              await setIsErrorPromotion(true);
              await setIsLoadingPromotion(false);
            } else {
              await setListPromotion([...list]);
              await setIsErrorPromotion(false);
              await setIsLoadingPromotion(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPromotion(true);
      await setIsLoadingPromotion(false);
    }
  };

  const fetchMerchantDetail = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('response fetch merchant detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const runCheckingLogin = async () => {
    try {
      const loginStatus = await checkIsLogin();
      if (loginStatus) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      await setIsLogin(false);
    }
  };

  const checkIsLogin = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const fetchImagecard = async () => {
    try {
      await client
        .query({
          query: GET_IMAGE_CARD,
          variables: {
            merchantId: parseInt(id, 10),
            itemDisplayed,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchImageCard: ', response);
          const {data, errors} = response;
          const {getImageCards} = data;
          const {data: list, error} = getImageCards;

          if (errors) {
            await setIsErrorImageCard(true);
            await setIsLoadingImageCard(false);
          } else {
            if (error) {
              await setIsErrorImageCard(true);
              await setIsLoadingImageCard(false);
            } else {
              await setImageCardList([...list]);
              await setIsErrorImageCard(false);
              await setIsLoadingImageCard(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchImagecard', error);
      await setIsErrorImageCard(true);
      await setIsLoadingImageCard(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalLoader isLoading={isLoadingWishList} />
      <ScrollView>
        <VideoPlayer
          url={
            detail[0]?.video[0]?.url ? {uri: detail[0]?.video[0]?.url} : null
          }
          isLoading={isLoading}
          isError={isError}
        />
        {/* Top Banner */}
        <TopBanner
          {...props}
          isLoading={isLoading}
          isError={isError}
          detail={detail}
        />
        <ShowImageCardByIndex
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          showByIndex={0}
        />
        {/* Popular Gown */}
        <PopularProduct
          type={'Gown'}
          list={popularGown}
          isLoading={isLoadingPopularGown}
          isError={isErrorPopularGown}
          detail={detail}
          runWishList={(productId, statusWhislist) => {
            runWishList(productId, statusWhislist);
          }}
          isLogin={isLogin}
          {...props}
        />

        <ShowImageCardByIndex
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          showByIndex={1}
        />

        {/* Popular Suit */}
        <PopularProduct
          type={'Suit'}
          list={popularSuit}
          isLoading={isLoadingPopularSuit}
          isError={isErrorPopularSuit}
          detail={detail}
          runWishList={(productId, statusWhislist) => {
            runWishList(productId, statusWhislist);
          }}
          isLogin={isLogin}
          {...props}
        />

        {/* Image Card List */}
        <ListImageCard
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          isLoadMore={isLoadMore}
        />

        {/* List Promo */}
        <ListPromotion
          {...props}
          detail={detail}
          list={listPromotion}
          isLoading={isLoadingPromotion}
          isError={isErrorPromotion}
        />

        {/* Button Enquiry */}
        {/* <ButtonEnquiry
          title={'ENQUIRY NOW'}
          {...props}
          onPress={() => {
            navigation.navigate('Enquiry', {id, serviceType});
          }}
        /> */}
      </ScrollView>
    </View>
  );
};

const Wrapper = compose(withApollo)(Home);

export default props => <Wrapper {...props} />;
