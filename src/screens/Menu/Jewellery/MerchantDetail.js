import React, {useState, useEffect, useCallback} from 'react';
import {
  TouchableOpacity,
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {charImage} from '../../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

// Tab
import Tab from './Tab';

// Components
import Globalheader from './Components/Globalheader';
import Headers from './Components/Header';

// Query
import GET_MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  headerBorderBottom,
  greyLine,
  transparent,
  mainGreen,
  mainRed,
} = Colors;
const {medium, book} = FontType;
const {charComingSoon} = charImage;

const MerchantDetail = props => {
  console.log('Merchant Detail Jewellery Screen: ', props);

  const [activeTab, setActiveTab] = useState('Home');

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetch();
  }, [props?.route?.params?.id]);

  const fetch = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(props?.route?.params?.id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Merchant Detail Res: ', res);
          const {data, errors} = res;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (getMerchantVer2?.length === 0) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setDetail([...getMerchantVer2]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Headers
          title="Jewellery"
          goBack={() => props?.navigation?.goBack(null)}
          goHome={() => props?.navigation?.goBack(null)}
        />
        <Content>
          <View
            style={{
              flex: 1,
              paddingTop: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return <MerchantNotFound {...props} />;
  } else {
    if (detail?.length === 0) {
      return <MerchantNotFound {...props} />;
    } else {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          {activeTab === 'Home' ? (
            <>
              <ScrollView bounces={false}>
                {activeTab !== 'Home' ? (
                  <Globalheader
                    activeTab={activeTab}
                    {...props}
                    detail={detail}
                  />
                ) : null}
                <Tab
                  id={props?.route?.params?.id}
                  detail={detail}
                  activeTab={activeTab}
                  setActiveTab={tabName => setActiveTab(tabName)}
                  {...props}
                />
              </ScrollView>
            </>
          ) : (
            <>
              {activeTab !== 'Home' ? (
                <Globalheader
                  activeTab={activeTab}
                  {...props}
                  detail={detail}
                />
              ) : null}
              <Tab
                id={props?.route?.params?.id}
                detail={detail}
                activeTab={activeTab}
                setActiveTab={tabName => setActiveTab(tabName)}
                {...props}
              />
            </>
          )}

          {activeTab === 'Home' ? (
            <TouchableOpacity
              onPress={() => {
                try {
                  props?.navigation.navigate('Enquiry', {
                    id: detail[0]?.id,
                    serviceType: detail[0]?.serviceType,
                  });
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              style={{
                marginTop: 15,
                justifyContent: 'center',
                alignItems: 'center',
                width,
                height: hasNotch() ? 60 : 55,
                backgroundColor: mainGreen,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: white,
                }}>
                ENQUIRY NOW
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      );
    }
  }
};

export const MerchantNotFound = props => {
  return (
    <Container style={{backgroundColor: white}}>
      <Headers
        title="Jewellery"
        goBack={() => props?.navigation?.goBack(null)}
        goHome={() => props?.navigation?.goBack(null)}
      />
      <Content>
        <View
          style={{
            flex: 1,
            height: width * 1.5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={charComingSoon}
            style={{width: '100%', height: width * 0.5}}
            resizeMethod="auto"
            resizeMode="contain"
          />
          <Text
            style={{
              marginVertical: 15,
              marginTop: 25,
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              textAlign: 'center',
              letterSpacing: 0.3,
            }}>
            Merchant Not found
          </Text>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              textAlign: 'center',
              letterSpacing: 0.3,
            }}>
            We are sorry, current merchant is out from our services
          </Text>
        </View>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(MerchantDetail);

export default props => <Wrapper {...props} />;
