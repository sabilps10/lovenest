/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Platform,
  Animated,
  RefreshControl,
  ScrollView,
  Image,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Header,
  Footer,
  FooterTab,
} from 'native-base';
import {hasNotch} from 'react-native-device-info';
import JustLoader from '../../../components/Loader/JustLoader';
import Loader from '../../../components/Loader/LoaderWithContainer';
import AsyncImage from '../../../components/Image/AsyncImage';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncModel from '../../../utils/AsyncstorageDataStructure';
import ModalImageViewer from './Components/ImageViewer';
import ModalImageAttatchmentViewer from './Components/ImageViewer';
import ModalLoader from './Components/ModalLoader';
import ButtonEnquiry from './Components/ButtonEnquiry';
import SimilarCollection from './Components/SimilarProduct';
import {charImage} from '../../../utils/Themes/Images';
import LinearGradient from 'react-native-linear-gradient';

// Query
import GET_PRODUCT_DETAIL from '../../../graphql/queries/fetchProduct';

// Mutation
import MutationAddWishList from '../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../graphql/mutations/removeWishlist';

const {charRightShuriken} = charImage;
const {asyncToken} = AsyncModel;
const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  brownCream,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const heightSlider = height / 1.58;

const ProductDetail = props => {
  console.log('ProductDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refresh, setRefresh] = useState(false);

  const [dataProduct, setDataProduct] = useState(null);

  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);
  const [isLogin, setIsLogin] = useState(true);

  const [openImageViewer, setOpenImageViewer] = useState(false);
  const [openImageAttatchmentViewer, setOpenImageAttachmentViewer] =
    useState(false);

  useEffect(() => {
    checkIsLogin();
    fetchProduct();
    const subscriber = navigation.addListener('focus', () => {
      checkIsLogin();
      fetchProduct();
    });

    return subscriber;
  }, [navigation]);

  const openImageModal = async () => {
    try {
      await setOpenImageViewer(!openImageViewer);
    } catch (error) {
      console.log('Error: ', error);
      await setOpenImageViewer(false);
    }
  };

  const runCheckingLogin = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);
        console.log('token >>> ', token);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const login = await runCheckingLogin();
      console.log('Login >>>> : ', login);
      await setIsLogin(login);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefresh(true);
      await fetchProduct();
    } catch (error) {
      console.log('Error: ', error);
      await setRefresh(false);
    }
  };

  const fetchProduct = () => {
    try {
      client
        .query({
          query: GET_PRODUCT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response get product detail: ', response);
          const {data, errors} = response;
          const {fetchProduct: resFetchProduct} = data;
          const {data: product, error} = resFetchProduct;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefresh(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefresh(false);
            } else {
              await setDataProduct(product);
              await setIsError(false);
              await setIsLoading(false);
              await setRefresh(false);
            }
          }
        })
        .catch(error => {
          console.log('Error get product detail: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefresh(false);
        });
    } catch (error) {
      console.log('Error get product detail: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefresh(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un wishlist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {data: resp, error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {data: resp, error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  if (isLoading && !isError) {
    return <Loader />;
  } else if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Headers goBack={goBack} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={async () => {
              try {
                await setIsLoading(true);
                await fetchProduct();
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            style={{
              borderRadius: 4,
              borderWidth: 1,
              height: height / 17,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text style={{fontFamily: medium, fontSize: RFPercentage(1.8)}}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={'transparent'}
          barStyle={'dark-content'}
        />
        <ModalLoader isLoading={isLoadingWishList} />
        <ModalImageViewer
          visible={openImageViewer}
          onPress={openImageModal}
          images={dataProduct?.galleries}
        />
        <ModalImageAttatchmentViewer
          visible={openImageAttatchmentViewer}
          onPress={async () =>
            await setOpenImageAttachmentViewer(!openImageAttatchmentViewer)
          }
          images={
            dataProduct?.attachment
              ? [
                  {
                    id: dataProduct?.attachment?.id,
                    url: dataProduct?.attachment?.dynamicUrl
                      ? dataProduct?.attachment?.dynamicUrl
                      : dataProduct?.attachment?.url,
                  },
                ]
              : []
          }
        />
        <ScrollView
          bounces={false}
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
          }>
          <ProductImageSlider
            {...props}
            openImageModal={openImageModal}
            isLogin={isLogin}
            productId={dataProduct.id}
            isWishlist={dataProduct.isWishlist}
            runWishList={(productId, isWishlist) =>
              runWishList(productId, isWishlist)
            }
            gallery={dataProduct?.galleries ? dataProduct.galleries : []}
          />
          <HeaderTitleAndDescription data={dataProduct} />
          {dataProduct.type === 'Bridal' ? (
            <ContentBridal data={dataProduct} />
          ) : null}
          {dataProduct.type === 'Accessories' ? (
            <ContentAccessories data={dataProduct} />
          ) : null}
          {dataProduct.type === 'Jewellery' ? (
            <ContentJewellery data={dataProduct} />
          ) : null}
          {dataProduct.type === 'Venue' ? (
            <ContentPackageSpecification data={dataProduct} />
          ) : null}
          {dataProduct.type === 'Venue' ? (
            <PackageDetails data={dataProduct} />
          ) : null}
          <SimilarCollection
            onPress={ids => {
              navigation.push('BridalProductDetail', {id: ids});
            }}
            isLogin={isLogin}
            runWishList={(ids, isWishlist) => runWishList(ids, isWishlist)}
            id={id}
            {...props}
          />
          <MerchantBaseOnProduct {...props} data={dataProduct} />
          {isLogin ? (
            <ButtonEnquiry
              title={'ENQUIRY NOW'}
              {...props}
              onPress={() => {
                try {
                  if (
                    dataProduct?.merchantDetails?.id &&
                    dataProduct?.merchantDetails?.serviceType
                  ) {
                    navigation.navigate('Enquiry', {
                      id: dataProduct?.merchantDetails?.id,
                      serviceType: dataProduct?.merchantDetails?.serviceType,
                    });
                  }
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
            />
          ) : null}
        </ScrollView>
      </View>
    );
  }
};

export const MerchantBaseOnProduct = props => {
  console.log('MerchantBaseOnProduct: ', props);
  const {data, navigation} = props;
  const {merchantDetails} = data;
  const source = merchantDetails?.logoImageDynamicUrl
    ? {uri: `${merchantDetails.logoImageDynamicUrl}=h500`}
    : {uri: merchantDetails.logoImageUrl};
  const name = merchantDetails?.name ? merchantDetails.name : 'N/A';
  const tagline = merchantDetails?.tagline ? merchantDetails.tagline : 'N/A';
  return (
    <View>
      <LinearGradient
        start={{x: 0.0, y: 0.3}}
        end={{x: 0.0, y: 8}}
        locations={[0.0, 0.2]}
        colors={['#CAB77C', 'white']}
        style={{
          marginVertical: 5,
          // minHeight: height / 5.5,
          width: '100%',
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingTop: 15,
          paddingBottom: 15,
        }}>
        <Image
          source={charRightShuriken}
          style={{
            width: width / 10,
            height: height / 17,
            position: 'absolute',
            right: 10,
            top: 20,
            zIndex: 1,
          }}
        />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            paddingLeft: 15,
            paddingRight: 15,
            paddingTop: 5,
            paddingBottom: 5,
          }}>
          <View
            style={{
              flex: 0.39,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <View
              style={{
                width: width / 4.5,
                height: height / 9.8,
                backgroundColor: 'white',
                borderRadius: 4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={source}
                style={{width: '100%', height: height / 13}}
                resizeMode="contain"
              />
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flexWrap: 'wrap',
                marginBottom: 5,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: 'white',
                  letterSpacing: 0.3,
                }}>
                {name}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flexWrap: 'wrap',
                // marginBottom: 5,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: 'white',
                  letterSpacing: 0.3,
                }}>
                {tagline}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                position: 'absolute',
                bottom: 0,
              }}>
              <TouchableOpacity
                onPress={() => {
                  try {
                    if (props?.route?.params?.menuName === 'SuitMenu') {
                      navigation.push('BridalDetail', {
                        id: props?.data?.merchantDetails?.id,
                        serviceType: props?.data?.merchantDetails?.serviceType,
                      });
                      // console.log('Props ANU CUK: ', props);
                    } else {
                      navigation.goBack(null);
                    }
                  } catch (error) {
                    console.log('Error: ', error);
                  }
                }}
                style={{
                  borderWidth: 1,
                  borderColor: 'white',
                  flexDirection: 'row',
                  padding: 5,
                  paddingLeft: 10,
                  paddingRight: 10,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: 'white',
                    letterSpacing: 0.3,
                    marginRight: 3,
                  }}>
                  Explore
                </Text>
                <View
                  style={{
                    marginLeft: 5,
                    borderWidth: 1,
                    borderColor: 'white',
                    borderRadius: 20 / 2,
                    height: 20,
                    width: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="arrow-right"
                    style={{fontSize: 13, color: 'white'}}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export const ListItemProduct = props => {
  const {label, content} = props;

  return (
    <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
      <View
        style={{
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          flex: 0.6,
          height: '100%',
          minHeight: 30,
          paddingBottom: 10,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {label}
        </Text>
      </View>
      <View
        style={{
          minHeight: 30,
          height: '100%',
          flex: 1,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          justifyContent: 'flex-end',
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingBottom: 10,
        }}>
        <Text
          style={{
            textAlign: 'right',
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {content}
        </Text>
      </View>
    </CardItem>
  );
};

export const PackageDetails = props => {
  const {data} = props;
  const {hotelPackageDetails} = data;

  return (
    <Card
      transparent
      style={{
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Packages Details
        </Text>
      </CardItem>
      <CardItem>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {hotelPackageDetails}
        </Text>
      </CardItem>
    </Card>
  );
};

export const ContentPackageSpecification = props => {
  const {data} = props;
  const {hotelPrice, hotelCapacity} = data;
  return (
    <Card
      transparent
      style={{
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Specification
        </Text>
      </CardItem>
      <CardItem
        style={{
          width: '100%',
          backgroundColor: 'transparent',
          justifyContent: 'flex-start',
          alignItems: 'center',
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}>
        <View style={{width: 25}}>
          <Icon
            type="Feather"
            name="users"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: RFPercentage(1.8),
              color: black,
            }}
          />
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {hotelCapacity} pax
          </Text>
        </View>
      </CardItem>
    </Card>
  );
};

export const PendantInformation = props => {
  const {jewelType, jewelMetal, jewelSettingType, jewelWeight} = props;

  return (
    <Card
      transparent
      style={{
        marginLeft: 0,
        marginRight: 0,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Pendant Information
        </Text>
      </CardItem>
      <ListItemProduct label={'Type'} content={jewelType} />
      <ListItemProduct label={'Metal'} content={jewelMetal} />
      <ListItemProduct label={'Setting Type'} content={jewelSettingType} />
      <ListItemProduct label={'Appr. Weight'} content={jewelWeight} />
    </Card>
  );
};

export const DiamondInformation = props => {
  const {jewelDiamondInformation} = props;
  const keyExt = (item, index) => `${item.id}`;
  if (jewelDiamondInformation) {
    if (jewelDiamondInformation.length === 0) {
      return null;
    } else {
      return (
        <FlatList
          legacyImplementation
          disableVirtualization
          scrollEnabled={false}
          data={jewelDiamondInformation}
          extraData={jewelDiamondInformation}
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item, index}) => {
            const {total, weight} = item;
            return (
              <Card
                transparent
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  elevation: 0,
                  shadowOpacity: 0,
                }}>
                <CardItem>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                      lineHeight: 20,
                    }}>
                    Diamond Information
                  </Text>
                </CardItem>
                <ListItemProduct
                  label={'No. of round diamond'}
                  content={total}
                />
                <ListItemProduct
                  label={'Appr. round diamond weight'}
                  content={weight}
                />
              </Card>
            );
          }}
        />
      );
    }
  } else {
    return null;
  }
};

export const GemsInformation = props => {
  const {jewelGemsInformation} = props;
  const keyExt = (item, index) => `${item.id}`;
  if (jewelGemsInformation) {
    if (jewelGemsInformation.length === 0) {
      return null;
    } else {
      return (
        <FlatList
          legacyImplementation
          disableVirtualization
          scrollEnabled={false}
          data={jewelGemsInformation}
          extraData={jewelGemsInformation}
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item, index}) => {
            const {total, weight} = item;
            return (
              <Card
                transparent
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  elevation: 0,
                  shadowOpacity: 0,
                }}>
                <CardItem>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: black,
                      letterSpacing: 0.3,
                      lineHeight: 20,
                    }}>
                    Gems Information
                  </Text>
                </CardItem>
                <ListItemProduct label={'No. of ruby'} content={total} />
                <ListItemProduct label={'Appr. ruby weight'} content={weight} />
              </Card>
            );
          }}
        />
      );
    }
  } else {
    return null;
  }
};

export const ContentJewellery = props => {
  const {data} = props;
  const {
    jewelType,
    jewelMetal,
    jewelSettingType,
    jewelWeight,
    jewelDiamondInformation,
    jewelGemsInformation,
  } = data;

  return (
    <>
      <PendantInformation
        jewelType={jewelType}
        jewelMetal={jewelMetal}
        jewelSettingType={jewelSettingType}
        jewelWeight={jewelWeight}
      />
      <DiamondInformation jewelDiamondInformation={jewelDiamondInformation} />
      <GemsInformation jewelGemsInformation={jewelGemsInformation} />
    </>
  );
};

export const ContentAccessories = props => {
  const {data} = props;
  const {accsType, accsCategory, accsDetails, accsRange} = data;

  return (
    <Card
      transparent
      style={{
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Specification
        </Text>
      </CardItem>
      <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
        <View style={{width: 25}}>
          <Icon
            type="Feather"
            name="tag"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: RFPercentage(2),
              color: black,
            }}
          />
        </View>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {accsType}
          </Text>
        </View>
      </CardItem>
      <ListItemProduct label={'Details'} content={accsDetails} />
      <ListItemProduct label={'Range'} content={accsRange} />
      <ListItemProduct label={'Category'} content={accsCategory} />
    </Card>
  );
};

export const ContentBridal = props => {
  const {data} = props;
  const {bridalType, bridalSpecColour, bridalSpecFabric, bridalSpecDetails} =
    data;
  return (
    <Card
      transparent
      style={{
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: greyLine,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Specification
        </Text>
      </CardItem>
      <ListItemProduct label={'Color'} content={bridalSpecColour} />
      <ListItemProduct label={'Fabric'} content={bridalSpecFabric} />
      <ListItemProduct label={'Details'} content={bridalSpecDetails} />
    </Card>
  );
};

export const HeaderTitleAndDescription = props => {
  console.log('HeaderTitleAndDescription: >>> ', props);
  const {data} = props;
  const {name, description, type, hotelPrice} = data;

  if (type === 'Venue') {
    return (
      <Card
        transparent
        style={{
          marginTop: 0,
          marginLeft: 0,
          marginRight: 0,
          shadowOpacity: 0,
          elevation: 0,
          borderColor: greyLine,
          borderWidth: 0.5,
          borderRadius: 0,
        }}>
        <CardItem style={{width: '100%'}}>
          <View
            style={{
              flex: 1,
              height: '100%',
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2),
                color: black,
                letterSpacing: 0.39,
                lineHeight: 25,
                textAlign: 'left',
              }}>
              {name}
            </Text>
          </View>
          <View
            style={{
              flex: 0.5,
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.3),
                color: black,
                letterSpacing: 0.39,
                lineHeight: 25,
                textAlign: 'right',
              }}>
              ${hotelPrice ? hotelPrice : 'N/A'}
            </Text>
          </View>
        </CardItem>
        <CardItem>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {description}
          </Text>
        </CardItem>
      </Card>
    );
  } else {
    // selain venue
    return (
      <Card
        transparent
        style={{
          marginTop: 0,
          marginLeft: 0,
          marginRight: 0,
          shadowOpacity: 0,
          elevation: 0,
          borderColor: greyLine,
          borderWidth: 0.5,
          borderRadius: 0,
          bottom: 10,
        }}>
        {data?.bridalType ? (
          <CardItem>
            <View
              style={{
                borderRadius: 15,
                backgroundColor: '#D4D4D4',
                padding: 1.5,
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: black,
                  letterSpacing: 0.39,
                  lineHeight: 25,
                }}>
                {data?.bridalType ? data?.bridalType : ''}
              </Text>
            </View>
          </CardItem>
        ) : null}
        <CardItem>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              letterSpacing: 0.39,
              lineHeight: 25,
            }}>
            {name}
          </Text>
        </CardItem>
        <CardItem>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {description}
          </Text>
        </CardItem>
      </Card>
    );
  }
};

export const ButtonAddToCart = props => {
  const {onPress, title} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const CardImage = props => {
  const {item} = props;
  const {url, dynamicUrl} = item;
  const logoSource = dynamicUrl ? {uri: `${dynamicUrl}=h800`} : {uri: url};
  const RenderCard = () =>
    useMemo(() => {
      return (
        <View
          style={{
            elevation: 0,
            shadowOpacity: 0,
            marginLeft: 0,
            marginTop: 0,
            marginBottom: 0,
            marginRight: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            width,
          }}>
          <View style={{width, height: heightSlider}}>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                shadowOpacity: 0,
                elevation: 0,
                width,
                height: heightSlider,
                aspectRatio: width / heightSlider,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={'#f8f8f8'}
            />
          </View>
        </View>
      );
    }, [item]);

  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const BackButton = props => {
  const {navigation} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 35,
        left: 15,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          navigation.goBack(null);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 35,
          width: 35,
          justifyContent: 'center',
          borderRadius: 35 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        <Icon
          type="Feather"
          name="chevron-left"
          style={{
            right: 0.5,
            marginLeft: 0,
            marginRight: 0,
            fontSize: 25,
            color: black,
          }}
        />
      </TouchableOpacity>
    </Animated.View>
  );
};

export const HeartButton = props => {
  const {isWishlist, id, runWishList} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 35,
        right: 15,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          runWishList(id, isWishlist);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 35,
          width: 35,
          justifyContent: 'center',
          borderRadius: 35 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        {isWishlist ? (
          <Icon
            type="MaterialCommunityIcons"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        ) : (
          <Icon
            type="Feather"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        )}
      </TouchableOpacity>
    </Animated.View>
  );
};

export const ProductImageSlider = props => {
  console.log;
  const {
    gallery,
    navigation,
    productId,
    isWishlist,
    runWishList,
    isLogin,
    openImageModal,
  } = props;
  const scrollX = new Animated.Value(0);
  const keyExt = (item, index) => `${String(item.id)} Images`;
  const RenderSlider = () =>
    useMemo(() => {
      return (
        <>
          <BackButton navigation={navigation} />
          {isLogin ? (
            <HeartButton
              id={productId}
              runWishList={(id, statusWhislist) =>
                runWishList(id, statusWhislist)
              }
              isWishlist={isWishlist}
            />
          ) : null}
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            initialNumToRender={3}
            legacyImplementation
            disableVirtualization
            decelerationRate="fast"
            data={gallery}
            extraData={gallery}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            pagingEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            initialScrollIndex={0}
            renderItem={({item, index}) => {
              return <CardImage item={item} />;
            }}
          />
          <View
            style={{
              position: 'relative',
              zIndex: 10,
              bottom: 50,
              right: 0,
              left: 0,
              width: '100%',
              flexDirection: 'row',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {gallery.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {gallery.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: 7,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 7 / 2,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 9,
                          width: 9,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 9 / 2,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
            <View
              style={{
                flex: 0.3,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  openImageModal();
                }}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: white,
                  paddingLeft: 10,
                  paddingRight: 10,
                  height: height / 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  View All
                </Text>
              </Button>
            </View>
          </View>
        </>
      );
    }, [gallery]);
  if (gallery) {
    return RenderSlider();
  } else {
    return null;
  }
};

export const Headers = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        zIndex: 99,
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Florist
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(ProductDetail);

export default props => <Wrapper {...props} />;
