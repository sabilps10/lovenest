import React, {useEffect, useState, useCallback} from 'react';
import {
  Text,
  Animated,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
import {charImage} from '../../../utils/Themes/Images';
import {Container, Content} from 'native-base';

// Components
import Headers from './Components/Header';
import MerchantList from './Components/MerchantList';
import MerchantDetail from './MerchantDetail';

// Query
import GET_MERCHANT_LIST from '../../../graphql/queries/getMerchantByServiceType';

const {white, black, greyLine, headerBorderBottom, mainGreen, mainRed} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const {charComingSoon} = charImage;

const ListMerchantJewellery = props => {
  const {navigation, client, positionYBottomNav} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefresing] = useState(false);

  useEffect(() => {
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }

    fetch();
  }, []);

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_LIST,
          variables: {
            serviceType: 'Jewellery',
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Main Jewellery', res);
          const {data, errors} = res;
          const {getMerchantsByServiceType} = data;
          const {data: merchantList, error} = getMerchantsByServiceType;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefresing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefresing(false);
            } else {
              await setList([...merchantList]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefresing(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setRefresing(false);
    }
  };

  const onRefresh = useCallback(() => {
    fetch();
  }, [refreshing]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      // error
    }
  };

  const goHome = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      // error
    }
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Headers title="Jewellery" goBack={goBack} goHome={goHome} />
        <Content>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10,
            }}>
            <ActivityIndicator color={mainGreen} size="large" />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers title="Jewellery" goBack={goBack} goHome={goHome} />
        <Content>
          <View
            style={{
              flex: 1,
              width,
              height: width * 1.5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                setRefresing(true);
                onRefresh();
              }}
              style={{
                padding: 10,
                borderRadius: 5,
                backgroundColor: mainGreen,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: white,
                  textAlign: 'center',
                  letterSpacing: 0.3,
                }}>
                Refresh
              </Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  } else {
    if (list?.length === 0) {
      return (
        <Container style={{backgroundColor: white}}>
          <Headers title="Jewellery" goBack={goBack} goHome={goHome} />
          <Content>
            <View
              style={{
                flex: 1,
                height: width * 1.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={charComingSoon}
                style={{width: '100%', height: width * 0.5}}
                resizeMethod="auto"
                resizeMode="contain"
              />
              <Text
                style={{
                  marginVertical: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: black,
                  textAlign: 'center',
                  letterSpacing: 0.3,
                }}>
                Comming Soon
              </Text>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  textAlign: 'center',
                  letterSpacing: 0.3,
                }}>
                Keep updated with us, we preparing our feature very soon
              </Text>
            </View>
          </Content>
        </Container>
      );
    } else if (list?.length === 1) {
      const theProps = {
        ...props,
        route: {
          ...props?.route,
          params: {
            ...props?.route?.params,
            serviceType: list[0]?.serviceType,
            id: list[0].id,
          },
        },
      };
      return <MerchantDetail {...theProps} />;
    } else {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          <Headers title="Jewellery" goBack={goBack} goHome={goHome} />
          <MerchantList {...props} />
        </View>
      );
    }
  }
};

const mapToState = state => {
  return {
    positionYBottomNav: state?.positionYBottomNav,
  };
};

const mapToDispatch = dispatch => {
  return {};
};

const Connector = connect(mapToState, mapToDispatch)(ListMerchantJewellery);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
