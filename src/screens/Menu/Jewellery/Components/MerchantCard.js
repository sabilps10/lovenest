import React from 'react';
import {View, Dimensions, TouchableOpacity, Animated, Text} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button} from 'native-base';
import ColorGenerator from '../../../../utils/Themes/GradientMerchantCardGenerator';
import {FontSize, FontType} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncImage from '../../../../components/Image/AsyncImage';

const {book} = FontSize;
const {medium} = FontType;
const {black, greyLine, transparent, newContainerColor, mainGreen, white} =
  Colors;
const {width, height} = Dimensions.get('window');

const cardWidth = Dimensions.get('window').width / 1.1;
const cardHeight = 245;
const imageWidth = Dimensions.get('window').width / 7.5;
const imageHeight = Dimensions.get('window').height / 16.5;

const AnimatedCard = Animated.createAnimatedComponent(Card);

const MerchantCardWithGradient = props => {
  console.log('ColorGenerator >>>> ', ColorGenerator);
  const {id, serviceType, navigation, title, tag, source, iconSource} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  return (
    <TouchableOpacity
      style={{marginBottom: 10}}
      activeOpacity={1}
      onPress={() => {
        try {
          navigation?.push('JewelleryMerchantDetail', {id, serviceType});
        } catch (error) {
          //   error
        }
      }}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        style={{
          minHeight: cardHeight,
          maxHeight: cardHeight,
          width: cardWidth,
          borderRadius: 4,
          borderWidth: 0.5,
          borderColor: greyLine,
          marginBottom: 15,
          transform: [{scale: scale}],
        }}>
        <CardItem cardBody style={{backgroundColor: transparent}}>
          <AsyncImage
            style={{
              flex: 1,
              width: '100%',
              height: 169.44,
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
            }}
            loaderStyle={{width: 60 / 1.5, height: 60 / 1.5}}
            source={source}
            placeholderColor={newContainerColor}
          />
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexDirection: 'row',
            flexWrap: 'wrap',
            backgroundColor: transparent,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              height: '100%',
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                paddingRight: 15,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                {title}
              </Text>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                {tag}
              </Text>
            </View>
          </View>
          <View style={{flex: 0.5}}>
            <Button
              onPress={() => {
                navigation.navigate('Enquiry', {serviceType, id});
              }}
              style={{
                width: width / 4,
                height: height / 25,
                backgroundColor: mainGreen,
                borderRadius: 4,
                elevation: 0,
                shadowOpacity: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                Book Now
              </Text>
            </Button>
          </View>
        </CardItem>
      </AnimatedCard>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(MerchantCardWithGradient);

export default props => <Wrapper {...props} />;
