import React from 'react';
import {Dimensions, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {hasNotch} from 'react-native-device-info';
import Colors from '../../../../utils/Themes/Colors';

const {headerBorderBottom} = Colors;
const {width, height} = Dimensions.get('window');

const ImageCards = props => {
  const {source} = props;

  return (
    <View
      style={{
        width,
        height: width * 1.2,
        marginTop: 15,
      }}>
      <AsyncImage
        source={source}
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          width,
          zIndex: 0,
        }}
        resizeMode="cover"
        placeholderColor={headerBorderBottom}
        loaderStyle={{
          width: width / 7,
          height: height / 7,
        }}
      />
    </View>
  );
};

const Wrapper = compose(withApollo)(ImageCards);

export default props => <Wrapper {...props} />;
