import React, {useMemo} from 'react';
import {
  Text,
  Dimensions,
  Image,
  View,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {charImage} from '../../../../utils/Themes/Images';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {Card, CardItem, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AnimatedFloristCard from './JewelleryProductCard';

const {charCupStar} = charImage;
const {black, mainRed, mainGreen} = Colors;
const {medium} = FontType;
const {width, height} = Dimensions.get('window');

// Tab Action
import {TabActions} from '@react-navigation/native';

const ListPopularProducts = props => {
  const {
    addToCartCustomize,
    isLoading,
    isError,
    list,
    navigation,
    isLogin,
    runWishList,
    buttonDisable,
    detail,
  } = props;

  const keyExt = item => `${item.id}`;

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0) {
      return null;
    } else {
      const onPress = () => {
        try {
          const jumpToAction = TabActions.jumpTo('Product', {
            id: detail[0]?.id,
            serviceType: detail[0]?.serviceType,
          });
          navigation.dispatch(jumpToAction);
        } catch (error) {
          console.log('Error: ', error);
        }
      };

      const Rendering = () =>
        useMemo(() => {
          return (
            <LinearGradient
              // start={{x: 0.0, y: 0.25}}
              // end={{x: 0.5, y: 1.0}}
              // locations={[0, 0.1, 0.5, 0.6]}
              colors={
                buttonDisable
                  ? ['#FBF1E4', 'white', 'white', '#FBF1E4']
                  : ['#FBF1E4', 'white', 'white']
              }
              style={{
                // paddingBottom: 15,
                zIndex: 0,
                flex: 1,
                marginVertical: 15,
                marginBottom: 5,
                // borderWidth: 1,
              }}>
              <Image
                source={charCupStar}
                style={{
                  width: width / 8,
                  height: height / 8,
                  position: 'absolute',
                  top: -10,
                  left: 0,
                  zIndex: 1,
                }}
                resizeMode="contain"
              />
              <Card transparent style={{marginBottom: 0, zIndex: 2}}>
                <CardItem
                  style={{
                    backgroundColor: 'transparent',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(2),
                      letterSpacing: 0.3,
                    }}>
                    {buttonDisable ? 'Popular Flower' : 'Popular Florist'}
                  </Text>
                  <Button
                    disabled={buttonDisable ? true : false}
                    onPress={() => onPress()}
                    style={{
                      opacity: buttonDisable ? 0 : 1,
                      left: 10,
                      padding: 10,
                      paddingLeft: 0,
                      paddingRight: 0,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'transparent',
                      elevation: 0,
                      shadowOpacity: 0,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: mainRed,
                        fontSize: RFPercentage(1.8),
                        letterSpacing: 0.3,
                      }}>
                      See All
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{
                        marginLeft: 10,
                        fontSize: RFPercentage(2.5),
                        color: mainRed,
                      }}
                    />
                  </Button>
                </CardItem>
              </Card>
              <FlatList
                disableVirtualization
                legacyImplementation
                scrollEnabled={false}
                data={list}
                extraData={list}
                keyExtractor={keyExt}
                numColumns={2}
                columnWrapperStyle={{
                  paddingLeft: 10,
                  paddingRight: 10,
                  marginBottom: 5,
                  //   borderWidth: 1,
                }}
                renderItem={({item, index}) => {
                  return (
                    <AnimatedFloristCard
                      isLogin={isLogin}
                      addToCartCustomize={dataProduct =>
                        addToCartCustomize(dataProduct)
                      }
                      runWishList={(id, isWishlist) =>
                        runWishList(id, isWishlist)
                      }
                      {...props}
                      item={item}
                      index={index}
                    />
                  );
                }}
              />
            </LinearGradient>
          );
        }, [
          isLoading,
          isError,
          list,
          navigation,
          isLogin,
          runWishList,
          buttonDisable,
          detail,
        ]);

      return Rendering();
    }
  }
};

const Wrapper = compose(withApollo)(ListPopularProducts);

export default props => <Wrapper {...props} />;
