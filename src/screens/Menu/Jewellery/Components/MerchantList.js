import React, {useEffect, useState, useCallback} from 'react';
import {
  ActivityIndicator,
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {FontType} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';
import {Container, Content} from 'native-base';
import {charImage} from '../../../../utils/Themes/Images';

// Query
import GET_MERCHANT_LIST from '../../../../graphql/queries/getMerchantByServiceType';

// Components
import MerchantCard from './MerchantCard';

const {charComingSoon} = charImage;
const {white, black, headerBorderBottom, mainGreen, mainRed} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

const MerchantList = props => {
  const {navigation, client} = props;

  const [totalCount, setTotalCount] = useState(0);
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const itemDisplayed = 10;
  const [pageNumber, setPageNumber] = useState(1);

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetch();

    if (refreshing) {
      fetch();
    }

    if (isLoadMore) {
      fetchMore();
    }
  }, [isLoadMore, refreshing]);

  const fetchMore = useCallback(async () => {
    await setPageNumber(pageNumber + 1);
    await fetch();
  }, [isLoadMore]);

  const onRefresh = async () => {
    try {
      await setPageNumber(1);
      await setRefreshing(true);
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_LIST,
          variables: {
            serviceType: 'Jewellery',
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Jewellery Merchant List: ', res);
          const {data, errors} = res;
          const {getMerchantsByServiceType} = data;
          const {
            data: listMerchant,
            error,
            totalData,
          } = getMerchantsByServiceType;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              const oldData = list.concat(listMerchant);
              const filtering = await _.unionBy(oldData, 'id');
              await setList([...filtering]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <MerchantCard
          id={item?.id}
          serviceType={item?.serviceType}
          navigation={props?.navigation}
          title={item?.name}
          tag={item?.tagline}
          source={
            item?.coverImageDynamicUrl
              ? {uri: `${item?.coverImageDynamicUrl}=h500`}
              : {uri: item?.coverImageUrl}
          }
        />
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <Container>
        <Content>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 15,
            }}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return <MerchantNotFound {...props} />;
  } else {
    return (
      <View style={{flex: 1}}>
        <FlatList
          initialNumToRender={10}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  width,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                {isLoadMore ? (
                  <ActivityIndicator size="small" color={mainGreen} />
                ) : null}
              </View>
            );
          }}
          onEndReached={() => {
            if (list?.length < totalCount) {
              setIsLoadMore(true);
            } else {
              setIsLoadMore(false);
            }
          }}
          onEndReachedThreshold={0.01}
          contentContainerStyle={{
            width,
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 15,
          }}
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={renderItem}
        />
      </View>
    );
  }
};

export const MerchantNotFound = props => {
  return (
    <Container style={{backgroundColor: white}}>
      <Content>
        <View
          style={{
            flex: 1,
            height: width * 1.5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={charComingSoon}
            style={{width: '100%', height: width * 0.5}}
            resizeMethod="auto"
            resizeMode="contain"
          />
          <Text
            style={{
              marginVertical: 15,
              marginTop: 25,
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              textAlign: 'center',
              letterSpacing: 0.3,
            }}>
            Merchant Not found
          </Text>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              textAlign: 'center',
              letterSpacing: 0.3,
            }}>
            We are sorry, current merchant is out from our services
          </Text>
        </View>
      </Content>
    </Container>
  );
};

const mapToState = state => {
  return {};
};

const mapToDispatch = dispatch => {
  return {};
};

const Connector = connect(mapToState, mapToDispatch)(MerchantList);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
