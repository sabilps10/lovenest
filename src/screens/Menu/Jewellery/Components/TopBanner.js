import React from 'react';
import {
  Text,
  Dimensions,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {Icon} from 'native-base';
import {charImage} from '../../../../utils/Themes/Images';

// Tab Action
import {TabActions} from '@react-navigation/native';

const {charJewelleryBanner} = charImage;
const {width, height} = Dimensions.get('window');
const {black, mainRed, superGrey, greyLine, headerBorderBottom} = Colors;
const {medium, book} = FontType;

const TopBanner = props => {
  console.log('TopBanner: ', props);
  const {isError, isLoading, detail, navigation} = props;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          height: width * 0.6,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: headerBorderBottom,
          marginBottom: 15,
        }}>
        <ActivityIndicator />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (detail?.length === 0 || !detail) {
      return null;
    } else {
      const {id, serviceType, coverImageUrl, coverImageDynamicUrl, tagline} =
        detail[0];
      const onPress = () => {
        try {
          if (id && serviceType) {
            const jumpToAction = TabActions.jumpTo('Portfolio', {
              id,
              serviceType,
            });
            navigation.dispatch(jumpToAction);
          } else {
            //
          }
        } catch (error) {
          console.log('Error: ', error);
        }
      };
      const source = coverImageDynamicUrl
        ? {uri: `${coverImageDynamicUrl}`}
        : {uri: coverImageUrl};
      return (
        <View style={{width, height: width * 0.6, marginBottom: 15}}>
          <AsyncImage
            source={charJewelleryBanner}
            style={{
              flex: 1,
              width: '100%',
              height: width * 0.6,
            }}
            resizeMode="cover"
            placeholderColor={headerBorderBottom}
            loaderStyle={{
              width: width / 7,
              height: height / 7,
            }}
          />
          <View
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              zIndex: 1,
              padding: 15,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <View style={{width: '75%', paddingLeft: 15}}>
              <Text
                style={{
                  fontSize: RFPercentage(1.7),
                  fontFamily: book,
                  color: superGrey,
                  letterSpacing: 0.3,
                }}>
                Always Fit
              </Text>
              <Text
                style={{
                  marginTop: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(2.7),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 28,
                }}>
                {'When words fail, jewellery speaks.'}
              </Text>
            </View>

            <View style={{marginTop: 15}}>
              <TouchableOpacity
                disable={true}
                onPress={() => {
                  onPress();
                }}>
                <View
                  style={{
                    display: 'none',
                    padding: 5,
                    paddingLeft: 10,
                    paddingRight: 10,
                    borderWidth: 1.5,
                    borderColor: mainRed,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: mainRed,
                      letterSpacing: 0.3,
                      marginRight: 10,
                    }}>
                    Explore
                  </Text>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20 / 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1.5,
                      borderColor: mainRed,
                    }}>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{fontSize: 18, color: mainRed}}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
  }
};

const Wrapper = compose(withApollo)(TopBanner);

export default props => <Wrapper {...props} />;
