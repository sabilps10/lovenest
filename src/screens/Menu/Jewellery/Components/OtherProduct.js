import React from 'react';
import {Text, Dimensions, View, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Icon} from 'native-base';
import AsyncImage from '../../../../components/Image/AsyncImage';

//  Tab Actions
import {TabActions} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
const {white, black, mainRed} = Colors;
const {medium} = FontType;

const OtherProduct = props => {
  console.log('OtherProduct: ', props);
  const {listOtherProduct, isLoading, isError, navigation, detail} = props;

  const keyExt = item => `${item.id}`;

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    const onPress = () => {
      try {
        const jumpToAction = TabActions.jumpTo('Product', {
          id: detail[0]?.id,
          serviceType: detail[0]?.serviceType,
        });
        navigation.dispatch(jumpToAction);
      } catch (error) {
        console.log('Error: ', error);
      }
    };
    return (
      <FlatList
        ListFooterComponent={() => {
          if (listOtherProduct?.length === 0) {
            return null;
          } else {
            return (
              <Card transparent>
                <CardItem
                  button
                  onPress={() => onPress()}
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: mainRed,
                      fontSize: RFPercentage(1.8),
                      letterSpacing: 0.3,
                      marginRight: 5,
                    }}>
                    Explore More Product
                  </Text>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20 / 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1.5,
                      borderColor: mainRed,
                    }}>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{left: 8, fontSize: 18, color: mainRed}}
                    />
                  </View>
                </CardItem>
              </Card>
            );
          }
        }}
        ListHeaderComponent={() => {
          if (listOtherProduct?.length === 0) {
            return null;
          } else {
            return (
              <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                <CardItem style={{paddingLeft: 5, paddingRight: 5}}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(2),
                      letterSpacing: 0.3,
                    }}>
                    Other Product
                  </Text>
                </CardItem>
              </Card>
            );
          }
        }}
        contentContainerStyle={{
          marginVertical: 15,
        }}
        legacyImplementation
        disableVirtualization
        scrollEnabled={false}
        numColumns={3}
        data={listOtherProduct}
        extraData={listOtherProduct}
        keyExtractor={keyExt}
        renderItem={({item}) => {
          const {featuredImageDynamicURL, featuredImageURL} = item;
          const source = featuredImageDynamicURL
            ? {uri: `${featuredImageDynamicURL}=h500`}
            : {uri: featuredImageURL};
          return (
            <View
              style={{
                width: width / 3.2,
                height: height / 5.5,
                backgroundColor: white,
                // borderWidth: 1,
              }}>
              <View
                style={{
                  position: 'absolute',
                  top: 5,
                  left: 5,
                  right: 5,
                  bottom: 5,
                }}>
                <AsyncImage
                  source={source}
                  style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    zIndex: 0,
                  }}
                  resizeMode="cover"
                  placeholderColor={'white'}
                  loaderStyle={{
                    width: width / 7,
                    height: height / 7,
                  }}
                />
              </View>
            </View>
          );
        }}
      />
    );
  }
};

const Wrapper = compose(withApollo)(OtherProduct);

export default props => <Wrapper {...props} />;
