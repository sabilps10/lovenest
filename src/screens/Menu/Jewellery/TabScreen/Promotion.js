import React, {useState, useEffect, useMemo, useCallback, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  RefreshControl,
  Image,
  FlatList,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';

import {charImage} from '../../../../utils/Themes/Images';

import _ from 'lodash';

const {
  charEmptyFloristPromotion,
  charOfferNotAvailable,
  charDelivery,
  charSelfPickup,
  charComingSoon,
} = charImage;

// Query
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';

// Components
import PromotionCard from '../Components/PromotionCard';

const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  mainRed,
  mainGreen,
  greyLine,
  superGrey,
  headerBorderBottom,
  transparent,
} = Colors;
const {book, medium} = FontType;

const Promotions = props => {
  console.log('Tab Promotion Screen: ', props);
  const {navigation, client, id, serviceType} = props;

  const listRef = useRef(null);

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchPromotion();

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      fetchPromotion();
    });

    return subs;
  }, [navigation, id, serviceType, isLoadMore]);

  const onLoadMore = useCallback(() => {
    try {
      setPageNumber(pageNumber + 1);
      fetchPromotion();
    } catch (error) {
      setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await setPageNumber(1);
      await fetchPromotion();
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetchPromotion = async () => {
    try {
      await client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(id, 10),
            itemDisplayed,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion Response: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error, totalData} = getAllPromotions;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await setTotalCount(totalData);
              const oldData = list.concat(listPromotion);
              // const newData = [...new Set(oldData)];
              const filtering = await _.uniqBy(oldData, 'id');
              console.log('Filtering Jewellery List >>>>>>>>>>>>> ', filtering);
              await setListPromotion([...filtering]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <View style={{marginVertical: 10}}>
          <PromotionCard
            item={item}
            index={index}
            onPress={() => {
              navigation.navigate('BridalPromotionDetail', {
                id: item?.id,
              });
            }}
          />
        </View>
      );
    },
    [listPromotion],
  );

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [listPromotion],
  );

  const scrollToTop = () => {
    try {
      listRef?.current?.scrollToOffset({animated: true, offset: 0});
    } catch (error) {}
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <FlatList
        ref={listRef}
        ListFooterComponent={() => {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {isLoadMore ? (
                <ActivityIndicator
                  size="small"
                  color={mainGreen}
                  style={{marginHorizontal: 5}}
                />
              ) : null}
              {isLoadMore ? (
                <Text
                  style={{
                    fontStyle: 'italic',
                    fontSize: 12,
                    color: greyLine,
                    textAlign: 'center',
                  }}>
                  Loading more...
                </Text>
              ) : null}
            </View>
          );
        }}
        ListEmptyComponent={() => {
          if (!isLoading) {
            return (
              <View
                style={{
                  padding: 15,
                  flex: 1,
                  height: height / 1.41,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={charComingSoon}
                  style={{width, height: height / 3}}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginVertical: 15,
                    fontFamily: medium,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                  }}>
                  Promotion Soon
                </Text>
                <Text
                  style={{
                    lineHeight: 20,
                    fontFamily: book,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2),
                  }}>
                  Keep updated with us, preparing our promotion very soon !
                </Text>
              </View>
            );
          } else {
            return null;
          }
        }}
        contentContainerStyle={{
          paddingLeft: 15,
          paddingBottom: 15,
          paddingRight: 15,
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        data={listPromotion}
        extraData={listPromotion}
        keyExtractor={keyExt}
        onEndReachedThreshold={0.03}
        onEndReached={() => {
          if (listPromotion?.length < totalCount) {
            setIsLoadMore(true);
          } else {
            setIsLoadMore(false);
          }
        }}
        legacyImplementation
        decelerationRate="fast"
        renderItem={renderItem}
      />
      <ButtonScrollToTop onPress={scrollToTop} />
    </View>
  );
};

export const ButtonScrollToTop = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 35,
        height: 35,
        position: 'absolute',
        bottom: 25,
        right: 25,
        borderRadius: 35 / 2,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{fontSize: RFPercentage(2.2), color: mainRed}}
      />
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(Promotions);

export default props => <Wrapper {...props} />;
