import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Query
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';
import GET_IMAGE_CARD from '../../../../graphql/queries/getImageCardFlorist';
import GET_POPULAR_PRODUCT from '../../../../graphql/queries/floristPopularProduct';
import GET_PRODUCTS from '../../../../graphql/queries/floristCommonProducts';
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';

// Components
import Galleries from '../Components/ImageGalleries';
import VideoPlayer from '../Components/VideoPlayer';
import TopBanner from '../Components/TopBanner';
import ShowImageCardByIndex from '../Components/ShowImageCardByIndex';
import PopularProductList from '../Components/PopularProduct';
import OtherProduct from '../Components/OtherProduct';
import ListImageCard from '../Components/ListImageCard';
import ListPromotion from '../Components/ListPromotion';
import ModalLoader from '../Components/ModalLoader';

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure';
const {asyncToken} = AsyncData;

const {white} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions?.get('window');

const TabHome = props => {
  console.log('Tab Home Props: ', props);

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [imageCardList, setImageCardList] = useState([]);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [isLoadingImageCard, setIsLoadingImageCard] = useState(true);
  const [isErrorImageCard, setIsErrorImageCard] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [popularProduct, setPopularProduct] = useState([]);
  const [isLoadingPopularProduct, setIsLoadingPopularProduct] = useState(true);
  const [isErrorPopularProduct, setIsErrorPopularProduct] = useState(false);

  const [listOtherProduct, setListOtherProduct] = useState([]);
  const [isLoadingOtherProduct, setIsLoadingOtherProduct] = useState(true);
  const [isErrorOtherProduct, setIsErrorOtherProduct] = useState(false);

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoadingPromotion, setIsLoadingPromotion] = useState(true);
  const [isErrorPromotion, setIsErrorPromotion] = useState(false);
  const [itemDisplayedPromotion, setItemDisplayedPromotion] = useState(1);

  useEffect(() => {
    runCheckingLogin();
    fetchMerchantDetail();
    fetchImagecard();
    fetchPopularProduct();
    fetchOtherProduct();
    fetchPromotion();
  }, [props?.id]);

  const fetchPromotion = async () => {
    try {
      await props?.client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(props?.id, 10),
            itemDisplayed: parseInt(itemDisplayedPromotion, 10),
            pageNumber: 1,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error} = getAllPromotions;

          if (errors) {
            await setIsErrorPromotion(true);
            await setIsLoadingPromotion(false);
          } else {
            if (error) {
              await setIsErrorPromotion(true);
              await setIsLoadingPromotion(false);
            } else {
              await setListPromotion([...list]);
              await setIsErrorPromotion(false);
              await setIsLoadingPromotion(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPromotion(true);
      await setIsLoadingPromotion(false);
    }
  };

  const fetchOtherProduct = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_PRODUCTS,
          variables: {
            serviceType: 'Jewellery',
            merchantId: parseInt(props?.id, 10),
            pageSize: 6,
            pageNumber: 1,
            // isRandom: true,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchOtherProduct: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsErrorOtherProduct(true);
            await setIsLoadingOtherProduct(false);
          } else {
            if (error) {
              await setIsErrorOtherProduct(true);
              await setIsLoadingOtherProduct(false);
            } else {
              await setListOtherProduct([...list]);
              await setIsErrorOtherProduct(false);
              await setIsLoadingOtherProduct(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorOtherProduct(true);
      await setIsLoadingOtherProduct(false);
    }
  };

  const runCheckingLogin = async () => {
    try {
      const loginStatus = await checkIsLogin();
      if (loginStatus) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      await setIsLogin(false);
    }
  };

  const checkIsLogin = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      props?.client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      props?.client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const fetchPopularProduct = async () => {
    try {
      await props?.client
        .query({
          query: GET_POPULAR_PRODUCT,
          variables: {
            merchantId: parseInt(props?.id, 10),
            serviceType: 'Jewellery',
            pageSize: 4,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchPopularProduct: ', response);
          const {data, errors} = response;
          const {productsPopular} = data;
          const {data: listPopularProducts, error} = productsPopular;

          if (errors) {
            await setIsErrorPopularProduct(true);
            await setIsLoadingPopularProduct(false);
          } else {
            if (error) {
              await setIsErrorPopularProduct(true);
              await setIsLoadingPopularProduct(false);
            } else {
              await setPopularProduct([...listPopularProducts]);
              await setIsErrorPopularProduct(false);
              await setIsLoadingPopularProduct(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchPopularProduct', error);
      await setIsErrorPopularProduct(true);
      await setIsLoadingPopularProduct(false);
    }
  };

  const fetchImagecard = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_IMAGE_CARD,
          variables: {
            merchantId: parseInt(props?.id, 10),
            itemDisplayed,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchImageCard: ', response);
          const {data, errors} = response;
          const {getImageCards} = data;
          const {data: list, error} = getImageCards;

          if (errors) {
            await setIsErrorImageCard(true);
            await setIsLoadingImageCard(false);
          } else {
            if (error) {
              await setIsErrorImageCard(true);
              await setIsLoadingImageCard(false);
            } else {
              await setImageCardList([...list]);
              await setIsErrorImageCard(false);
              await setIsLoadingImageCard(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchImagecard', error);
      await setIsErrorImageCard(true);
      await setIsLoadingImageCard(false);
    }
  };

  const fetchMerchantDetail = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(props?.id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Home Tab response fetch merchant detail: ', res);
          const {data, errors} = res;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const addToCartCustomize = dataProduct => {
    try {
      if (isLogin) {
        props?.navigation.navigate('FloristCustomize', {data: dataProduct});
      } else {
        props?.navigation.navigate('Login', {showXIcon: true});
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalLoader isLoading={isLoadingWishList} />
      <ScrollView>
        <VideoPlayer
          url={
            detail[0]?.video[0]?.url ? {uri: detail[0]?.video[0]?.url} : null
          }
          isLoading={isLoading}
          isError={isError}
        />
        <TopBanner
          {...props}
          isLoading={isLoading}
          isError={isError}
          detail={detail}
        />
        <ShowImageCardByIndex
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          showByIndex={0}
        />

        {/* Galleries using extra photo from merchantDetail API */}
        {/* <Galleries images={detail[0]?.extraPhoto} /> */}

        {/* Popular Suit */}
        <PopularProductList
          {...props}
          addToCartCustomize={dataProduct => addToCartCustomize(dataProduct)}
          detail={detail}
          buttonDisable={false}
          isLogin={isLogin}
          isLoading={isLoadingPopularProduct}
          isError={isErrorPopularProduct}
          list={popularProduct}
          runWishList={(productId, statusWhislist) =>
            runWishList(productId, statusWhislist)
          }
        />
        <ShowImageCardByIndex
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          showByIndex={1}
        />
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <OtherProduct
            {...props}
            detail={detail}
            listOtherProduct={listOtherProduct}
            isLoading={isLoadingOtherProduct}
            isError={isErrorOtherProduct}
          />
        </View>
        <ListImageCard
          listImageCard={imageCardList}
          isLoading={isLoadingImageCard}
          isError={isErrorImageCard}
          isLoadMore={isLoadMore}
        />
        <ListPromotion
          {...props}
          detail={detail}
          list={listPromotion}
          isLoading={isLoadingPromotion}
          isError={isErrorPromotion}
        />
      </ScrollView>
    </View>
  );
};

const Wrapper = compose(withApollo)(TabHome);

export default props => <Wrapper {...props} />;
