import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  ActivityIndicator,
  Dimensions,
  Image,
  View,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {CardItem, Container, Content, Icon, Card} from 'native-base';
import {charImage} from '../../../../utils/Themes/Images';
import JewelleryCard from '../Components/JewelleryProductCard';

// Async Storage
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure/index';

// Query
import GET_PRODUCTS from '../../../../graphql/queries/productsPublic';
import GET_PRODUCT_CATEGORIES from '../../../../graphql/queries/productFilter';

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

import _ from 'lodash';
import ModalLoader from '../Components/ModalLoader';

const {asyncToken} = AsyncData;
const {white, black, headerBorderBottom, overlayDim, mainRed, mainGreen} =
  Colors;
const {width} = Dimensions.get('window');
const {book, medium} = FontType;
const {charComingSoon} = charImage;

const TabProduct = props => {
  console.log('TabProduct Props: ', props);
  const {client} = props;

  const listRef = useRef(null);

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [list, setList] = useState([]);
  const [totalCount, seTotalCount] = useState(0);

  const pageSize = 16;
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [isLoadingCategories, setIsLoadingCategories] = useState(false);
  const [isErrorCategories, setIsErrorCategories] = useState(false);
  const [listCategories, setListCategoires] = useState([]);
  const [showModalCategories, setShowModalCategories] = useState(false);

  const [selectedType, setSelectedType] = useState([]);

  useEffect(() => {
    checkIsLogin();
    fetch();
    fetchCatgories();

    if (isLoadMore) {
      fetchMore();
    }
  }, [isLoadMore, selectedType]);

  const scrollToTop = () => {
    try {
      listRef?.current?.scrollToOffset({animated: true, offset: 0});
    } catch (error) {}
  };

  const runWishList = async (productId, statusWhislist, oldlist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId, oldlist);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId, oldlist);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkToken = () => {
    return new Promise(async resolve => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);

        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const userIsLogin = await checkToken();
      if (userIsLogin) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetchCatgories = async () => {
    try {
      await client
        .query({
          query: GET_PRODUCT_CATEGORIES,
          variables: {
            merchantId: parseInt(props?.id, 10),
            serviceType: 'Jewellery',
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Fetch Categories List: ', res);
          const {data, errors} = res;
          const {productsFilter} = data;
          const {data: listType, error} = productsFilter;

          if (errors) {
            await setIsErrorCategories(true);
            await setIsLoadingCategories(false);
          } else {
            if (error) {
              await setIsErrorCategories(true);
              await setIsLoadingCategories(false);
            } else {
              console.log('listType[0]?.list >>>> ', listType[0]?.list);
              await setListCategoires(
                listType?.length === 0 || !listType
                  ? []
                  : [...listType[0]?.list],
              );
              await setIsErrorCategories(false);
              await setIsLoadingCategories(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsErrorCategories(true);
      await setIsLoadingCategories(false);
    }
  };

  const onLoadMore = async status => {
    await setIsLoadMore(status);
    await setPageNumber(pageNumber + 1);
  };

  const fetchMore = useCallback(async () => {
    try {
      const variables = {
        serviceType: 'Jewellery',
        merchantId: parseInt(props?.id, 10),
        jewelleryType: selectedType,
        pageNumber,
        pageSize,
      };
      console.log('fetchMore: BGST: ', variables);
      await client
        .query({
          query: GET_PRODUCTS,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('List Jewellery Product Res: ', res);

          const {data, errors} = res;
          const {productsPublic} = data;
          const {data: listProduct, error, totalData} = productsPublic;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            await seTotalCount(totalData);
            const oldData = list.concat(listProduct);
            // const newData = [...new Set(oldData)];
            const filtering = await _.uniqBy(oldData, 'id');
            console.log('Filtering Jewellery List >>>>>>>>>>>>> ', filtering);
            await setList([...filtering]);
            await setIsError(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  }, [isLoadMore]);

  const onRefresh = async () => {
    try {
      console.log('SELECTED TYPE ON ON REFRESH', selectedType);
      await setRefreshing(true);
      await setList([]);
      await setTimeout(async () => {
        await shouldRefresh();
      }, 1500);
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const shouldRefresh = useCallback(async () => {
    try {
      console.log('YUUUUUUUHHHUUU: ', selectedType);
      const variables = {
        serviceType: 'Jewellery',
        merchantId: parseInt(props?.id, 10),
        jewelleryType: selectedType,
        pageNumber,
        pageSize,
      };
      console.log('shouldRefresh: BGST: ', variables);
      await client
        .query({
          query: GET_PRODUCTS,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('List Jewellery Product Res: ', res);

          const {data, errors} = res;
          const {productsPublic} = data;
          const {data: listProduct, error, totalData} = productsPublic;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            await seTotalCount(totalData);
            await setList([...listProduct]);
            await setIsError(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  }, []);

  const fetch = async () => {
    try {
      const variables = {
        serviceType: 'Jewellery',
        merchantId: parseInt(props?.id, 10),
        jewelleryType: selectedType,
        pageNumber,
        pageSize,
      };
      console.log('fetch: BGST: ', variables);
      await client
        .query({
          query: GET_PRODUCTS,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('List Jewellery Product Res: ', res);

          const {data, errors} = res;
          const {productsPublic} = data;
          const {data: listProduct, error, totalData} = productsPublic;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            await seTotalCount(totalData);
            const oldData = list.concat(listProduct);
            // const newData = [...new Set(oldData)];
            const filtering = await _.uniqBy(oldData, 'id');
            console.log('Filtering Jewellery List >>>>>>>>>>>>> ', filtering);
            await setList([...filtering]);
            await setIsError(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const keyExt = useCallback(
    item => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <JewelleryCard
          isLogin={isLogin}
          addToCartCustomize={() => {
            //
          }}
          runWishList={(productId, statusWhislist) =>
            runWishList(productId, statusWhislist, list)
          }
          {...props}
          item={item}
          index={index}
        />
      );
    },
    [list],
  );

  const onSumbit = async selectedListType => {
    try {
      console.log('SELECTED LIST TYPE: ', selectedListType);
      await setPageNumber(1);
      await setRefreshing(true);
      await setList([]);
      await setSelectedType([...selectedListType]);
    } catch (error) {
      // error
      await setRefreshing(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Content>
          <View
            style={{
              flex: 1,
              width,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 15,
            }}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Content>
          <View
            style={{
              flex: 1,
              width,
              height: width * 1.5,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Image
              source={charComingSoon}
              style={{width, height: width * 0.5}}
              resizeMethod="auto"
              resizeMode="contain"
            />

            <Text
              style={{
                marginVertical: 15,
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Out of Stock
            </Text>
            <Text
              style={{
                lineHeight: 25,
                fontFamily: book,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              We are sorry, currently our product is out of stock
            </Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <ModalLoader isLoading={isLoadingWishList} />
        <ModalCategories
          onReset={onSumbit}
          onSubmit={onSumbit}
          isLoading={isLoadingCategories}
          isError={isErrorCategories}
          listType={listCategories}
          visible={showModalCategories}
          onClose={() => setShowModalCategories(false)}
        />
        <Card transparent style={{marginTop: 0, marginBottom: 0}}>
          <CardItem
            style={{
              paddingTop: 5,
              paddingBottom: 5,
              backgroundColor: 'white',
            }}>
            <CategoriesButton
              onPress={() => setShowModalCategories(true)}
              selectedType={selectedType}
            />
          </CardItem>
        </Card>
        <FlatList
          ref={listRef}
          ListEmptyComponent={() => {
            if (isLoading || refreshing) {
              return null;
            } else {
              return (
                <View
                  style={{
                    flex: 1,
                    paddingLeft: 15,
                    paddingRight: 15,
                    width,
                    height: width * 1.55,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={charComingSoon}
                    style={{width, height: width * 0.5, bottom: 55}}
                    resizeMethod="auto"
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      bottom: 25,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                      textAlign: 'center',
                    }}>
                    Out of Stock
                  </Text>
                  <Text
                    style={{
                      bottom: 15,
                      lineHeight: 25,
                      fontFamily: book,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                      textAlign: 'center',
                    }}>
                    We are sorry, currently our product is out of stock
                  </Text>
                </View>
              );
            }
          }}
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  width: '100%',
                  paddingTop: 5,
                  paddingBottom: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator
                  animating={isLoadMore}
                  size={'small'}
                  color={mainGreen}
                />
              </View>
            );
          }}
          onEndReachedThreshold={0.6}
          onEndReached={() => {
            try {
              if (list?.length < totalCount) {
                onLoadMore(true);
              } else {
                onLoadMore(false);
              }
            } catch (error) {
              onLoadMore(false);
            }
          }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          numColumns={2}
          contentContainerStyle={{
            paddingBottom: 25,
          }}
          columnWrapperStyle={{
            paddingLeft: 10,
            paddingRight: 10,
          }}
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={renderItem}
        />
        <ButtonScrollToTop onPress={scrollToTop} />
      </View>
    );
  }
};

export const ModalCategories = props => {
  console.log('ModalCategories: >>>>>> ', props);
  const {onClose, visible, onReset, listType, onSubmit, isLoading, isError} =
    props;

  const [isLoadingBtn, setIsLoadingBtn] = useState(false);
  const [list, setList] = useState(listType);

  const keyExt = useCallback(
    item => {
      return `${item.id}`;
    },
    [listType],
  );

  const manipulatingSelectedList = data => {
    return new Promise(async resolve => {
      try {
        const manipulated = await Promise.all(
          data
            ?.map(d => {
              if (d?.checked) {
                return d?.name;
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );

        if (manipulated) {
          resolve(manipulated);
        } else {
          resolve([]);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve([]);
      }
    });
  };

  const submit = async () => {
    try {
      await setIsLoadingBtn(true);
      const finalSelectedData = await manipulatingSelectedList(list);
      console.log('finalSelectedData >>> ', finalSelectedData);
      await onSubmit(finalSelectedData);
      await setIsLoadingBtn(false);
      await onClose();
    } catch (error) {
      console.log('Error: ', error);
      await onClose();
    }
  };

  const onResetting = async () => {
    try {
      await setList([...listType]);
      await onReset([]);
      await onClose();
    } catch (error) {
      await onClose();
    }
  };

  const setSelectedType = async index => {
    try {
      console.log('index selected: ', listType);
      let oldList = list;
      const manipulated = await Promise.all(
        oldList?.map((d, i) => {
          if (index === i) {
            return {
              ...d,
              checked: true,
            };
          } else {
            return {
              ...d,
              checked: false,
            };
          }
        }),
      );

      console.log('manipulated >>>> ', manipulated);
      if (manipulated?.length === listType?.length) {
        await setList([...manipulated]);
      }
    } catch (error) {
      console.log('Error Filter Type Jewellery: ', error);
    }
  };

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <Card
          transparent
          style={{
            elevation: 0,
            shadowOpacity: 0,
            // marginLeft: 0,
            // marginRight: 0,
          }}>
          <CardItem
            button
            onPress={() => {
              setSelectedType(index);
            }}
            style={{
              borderBottomWidth: 1,
              borderBottomColor: headerBorderBottom,
            }}>
            <Text
              style={{
                fontSize: RFPercentage(1.6),
                fontFamily: medium,
                color: item?.checked ? mainRed : black,
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </CardItem>
        </Card>
      );
    },
    [listType],
  );

  return (
    <Modal visible={visible} transparent animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View>
          <View
            style={{width, backgroundColor: white, minHeight: width * 0.95}}>
            <HeaderModal
              onClose={onClose}
              title="Type"
              reset={() => {
                // reset
                onResetting();
              }}
            />
            <View style={{flex: 1}}>
              {isLoading && !isError ? (
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator color={mainGreen} size="large" />
                </View>
              ) : !isLoading && isError ? (
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text>Failed to get list Jewellery Type</Text>
                </View>
              ) : (
                <FlatList
                  contentContainerStyle={{paddingTop: 5, paddingBottom: 15}}
                  style={{bottom: 25}}
                  data={list}
                  extraData={list}
                  keyExtractor={keyExt}
                  renderItem={renderItem}
                />
              )}
            </View>
            <ButtonFilter onPress={submit} isLoading={isLoadingBtn} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export const ButtonScrollToTop = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 35,
        height: 35,
        position: 'absolute',
        bottom: 25,
        right: 25,
        borderRadius: 35 / 2,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{fontSize: RFPercentage(2.2), color: mainRed}}
      />
    </TouchableOpacity>
  );
};

export const ButtonFilter = props => {
  const {onPress, isLoading} = props;
  return (
    <TouchableOpacity
      disabled={isLoading ? true : false}
      onPress={onPress}
      style={{
        backgroundColor: mainGreen,
        height: width * 0.14,
        position: 'absolute',
        bottom: 0,
        width,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: white,
          letterSpacing: 0.3,
        }}>
        {isLoading ? 'Loading....' : 'SUBMIT'}
      </Text>
    </TouchableOpacity>
  );
};

export const HeaderModal = props => {
  const {onClose, title, reset} = props;

  return (
    <Card
      style={{
        bottom: 25,
        marginTop: 0,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        marginLeft: 0,
        marginRight: 0,
      }}>
      <CardItem style={{backgroundColor: 'transparent'}}>
        <View style={{flex: 0.3}}>
          <TouchableOpacity onPress={onClose}>
            <Icon
              type="Feather"
              name="x"
              style={{fontSize: RFPercentage(2.5), color: black}}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
        </View>
        <View style={{flex: 0.3}}>
          <TouchableOpacity
            style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}
            onPress={reset}>
            <Text
              style={{
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              Reset
            </Text>
          </TouchableOpacity>
        </View>
      </CardItem>
    </Card>
  );
};

export const CategoriesButton = props => {
  const {onPress, selectedType} = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderWidth: 1,
        borderRadius: 25,
        borderColor: mainRed,
        flexDirection: 'row',
        padding: 5,
        paddingTop: 2.5,
        paddingBottom: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: white,
      }}>
      <Text
        style={{
          left: 10,
          bottom: 2,
          fontFamily: medium,
          color: mainRed,
          fontSize: RFPercentage(1.5),
          letterSpacing: 0.3,
          marginRight: 5,
        }}>
        {selectedType?.length === 0 ? 'Filter By Type' : selectedType[0]}
      </Text>
      <View style={{bottom: 2, borderWidth: 0}}>
        <Icon
          type="MaterialIcons"
          name="arrow-drop-up"
          style={{fontSize: RFPercentage(2), top: 5, color: mainRed, left: 10}}
        />
        <Icon
          type="MaterialIcons"
          name="arrow-drop-down"
          style={{
            fontSize: RFPercentage(2),
            bottom: 5,
            color: mainRed,
            left: 10,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(TabProduct);

export default props => <Wrapper {...props} />;
