import React from 'react';
import {Animated, Text, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

// Tabs
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

import TabBar from '../TabBar';

// Components
import CoverImage from '../Components/CoverImage';
import GlobalHeader from '../Components/Globalheader';

// Tab Screens
import Home from '../TabScreen/Home';
import Product from '../TabScreen/Product';
import Promotion from '../TabScreen/Promotion';

const Tabs = props => {
  const {activeTab, detail} = props;
  return (
    <Tab.Navigator
      lazy={true}
      swipeEnabled={true}
      animationEnabled={true}
      initialRouteName={'Home'}
      tabBar={tabBarProps => {
        return (
          <Animated.View>
            {activeTab === 'Home' ? (
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  zIndex: 2,
                }}>
                <GlobalHeader activeTab={activeTab} {...props} />
              </View>
            ) : null}
            {activeTab === 'Home' ? (
              <View style={{zIndex: 1, borderWidth: 0, top: 0}}>
                <CoverImage
                  serviceType={
                    detail[0]?.serviceType ? detail[0]?.serviceType : ''
                  }
                  name={detail[0]?.name ? detail[0]?.name : 'n/a'}
                  logoSource={
                    detail[0]?.logoImageDynamicUrl
                      ? {uri: `${detail[0]?.logoImageDynamicUrl}=h500`}
                      : {uri: detail[0]?.logoImageUrl}
                  }
                  imageSource={
                    detail[0]?.coverImageDynamicUrl
                      ? {uri: `${detail[0]?.coverImageDynamicUrl}=h500`}
                      : {uri: `${detail[0]?.coverImageUrl}`}
                  }
                  tagline={detail[0]?.tagline}
                />
              </View>
            ) : null}
            <TabBar {...tabBarProps} />
          </Animated.View>
        );
      }}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Home',
        }}
        name="Home"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          props?.setActiveTab(routeNamePress);
        }}
        children={childProps => {
          const theProps = {...props, ...childProps};
          return <Home {...theProps} />;
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Product',
        }}
        name="Product"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          props?.setActiveTab(routeNamePress);
        }}
        children={childProps => {
          const theProps = {...props, ...childProps};
          return <Product {...theProps} />;
        }}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Promotion',
        }}
        name="Promotion"
        listeners={event => {
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          props?.setActiveTab(routeNamePress);
        }}
        children={childProps => {
          const theProps = {...props, ...childProps};
          return <Promotion {...theProps} />;
        }}
      />
    </Tab.Navigator>
  );
};

const Wrapper = compose(withApollo)(Tabs);

export default props => <Wrapper {...props} />;
