import React, {useState, useEffect, useRef} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {Button} from 'native-base';
import {connect} from 'react-redux';

const {white, mainGreen} = Colors;

// Query
import FloristCartBadge from '../../../graphql/queries/floristCartBadge';
import GET_MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

import Tabs from './TabBars/index';
import GlobalHeader from './Components/GlobalHeader';
import ScrollToTopButton from './Components/ScrollToTopButton';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncModel from '../../../utils/AsyncstorageDataStructure';

const {asyncToken} = AsyncModel;

const MerchantDetail = props => {
  console.log('MerchantDetail: ', props);

  const scrollViewRef = useRef();

  const {client, route, navigation} = props;
  const {params} = route;
  const {id} = params;

  const [activeTab, setActiveTab] = useState('Home');

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [badge, setBadge] = useState(0);
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    checkIsLogin();
    if (isLogin) {
      fetchBadge();
    }
    fetchMerchantDetail();

    const subs = navigation.addListener('focus', () => {
      checkIsLogin();
      if (isLogin) {
        fetchBadge();
      }
      fetchMerchantDetail();
    });

    return subs;
  }, [activeTab, navigation, isLogin]);

  const checkingToken = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const auth = await checkingToken();

      if (auth) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLogin(false);
    }
  };

  const fetchBadge = () => {
    try {
      client
        .query({
          query: FloristCartBadge,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Fetch Badge: ', response);
          const {data, errors} = response;
          const {cartBadge} = data;
          const {totalCount, error} = cartBadge;

          if (errors) {
            await setBadge(0);
          } else {
            if (error) {
              await setBadge(0);
            } else {
              if (totalCount) {
                await setBadge(totalCount);
              } else {
                await setBadge(0);
              }
            }
          }
        })
        .catch(async error => {
          console.log('error: ', error);
          await setBadge(0);
        });
    } catch (error) {
      console.log('Error: ', error);
      setBadge(0);
    }
  };

  const fetchMerchantDetail = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('response fetch merchant detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const scrollToTop = () => {
    console.log('Ref Scroll view: ', scrollViewRef?.current);
    try {
      scrollViewRef?.current?.scrollTo({
        y: 0,
        animated: true,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          onPress={fetchMerchantDetail}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: 15,
            paddingTop: 5,
            paddingBottom: 5,
            backgroundColor: mainGreen,
          }}>
          <Text>Refresh</Text>
        </Button>
      </View>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        {activeTab === 'Home' ? (
          <>
            <ScrollView
              bounces={true}
              ref={scrollViewRef}
              decelerationRate="fast">
              <Tabs
                badge={badge}
                id={id}
                serviceType={detail[0]?.serviceType}
                detail={detail}
                onActiveTab={tabName => setActiveTab(tabName)}
                activeTab={activeTab}
                {...props}
              />
            </ScrollView>
            <ScrollToTopButton onPress={scrollToTop} />
          </>
        ) : (
          <>
            {activeTab !== 'Home' ? (
              <GlobalHeader
                badge={badge}
                activeTab={activeTab}
                {...props}
                detail={detail}
              />
            ) : null}
            <Tabs
              badge={badge}
              id={id}
              serviceType={detail[0]?.serviceType}
              detail={detail}
              onActiveTab={tabName => setActiveTab(tabName)}
              activeTab={activeTab}
              {...props}
            />
          </>
        )}
      </View>
    );
  }
};

const mapToState = state => {
  return {};
};

const mapToDispatch = dispatch => {
  return {};
};

const connector = connect(
  mapToState,
  mapToDispatch,
)(MerchantDetail);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
