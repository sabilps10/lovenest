import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  Linking,
  Platform,
  Image,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {socialMediaIcon} from '../../../utils/Themes/Images';
import GeocodeByAddress from '../../../utils/Geocoding';
import {BridalBadge} from '../../../utils/Themes/Images';

import Map from '../../../components/Map/MapWithNavigationButton';
import ButtonSocialMedia from '../../../components/Button/buttonSocialMedia';

import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';
import Loader from '../../../components/Loader/circleLoader';
import MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

import RelatedMerchant from '../Bridal/BridalDetail/Components/RelatedMerchant';

const {blueBadge, pinkBadge} = BridalBadge;
const {white, transparent, mainRed, greyLine, black} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;
const {facebook, youtube, instagram, pinterest} = socialMediaIcon;
const {width, height} = Dimensions.get('window');

const AboutMerchant = props => {
  console.log('AboutMerchant Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [merchantDetailData, setMerchantDetailData] = useState([]);

  const [isDescExpand, setIsDescExpand] = useState(false);
  const [refreshing] = useState(false);

  useEffect(() => {
    fetch();

    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return () => {
      subscriber();
    };
  }, [navigation]);

  const fetch = () => {
    try {
      client
        .query({
          query: MERCHANT_DETAIL,
          variables: {
            id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setMerchantDetailData([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const openMapByAddress = async address => {
    try {
      const getLocationDetail = await GeocodeByAddress.detailLocation(address);
      console.log('getLocationDetail: ', getLocationDetail);

      if (getLocationDetail) {
        const lat = getLocationDetail.location.lat;
        const lng = getLocationDetail.location.lng;

        const latitude = lat;
        const longitude = lng;
        const label = `${address}`;

        const splitingAddress = label.replace(',', ' ');
        const replaceAddressWithReservedSymbol = splitingAddress.replace(
          '#',
          '',
        );
        console.log('splitingAddress: ', replaceAddressWithReservedSymbol);

        const url = Platform.select({
          ios: `https://www.google.com/maps/search/?api=1&query=${replaceAddressWithReservedSymbol}&center=${latitude},${longitude}`,
          android: `geo:${latitude},${longitude}?q=${replaceAddressWithReservedSymbol}`,
        });
        Linking.canOpenURL(url)
          .then(supported => {
            if (!supported) {
              const browser_url = `https://www.google.de/maps/@${latitude},${longitude}?q=${label}`;
              return Linking.openURL(browser_url);
            }
            return Linking.openURL(url);
          })
          .catch(err => console.log('error', err));
      }
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const openMap = item => {
    try {
      const label = `${item.locationName}, ${item.address}, ${item.country}`;

      const url = Platform.select({
        ios: `https://www.google.com/maps/search/?api=1&query=${label}&center=${
          item.latitude
        },${item.longitude}`,
        android: `geo:${item.latitude},${item.longitude}?q=${label}`,
      });
      Linking.canOpenURL(url)
        .then(supported => {
          if (!supported) {
            const browser_url = `https://www.google.de/maps/@${item.latitude},${
              item.longitude
            }?q=${label}`;
            return Linking.openURL(browser_url);
          } else {
            return Linking.openURL(url);
          }
        })
        .catch(error => console.log('Error: ', error));
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const sendEmail = email => {
    try {
      Linking.openURL(`mailto:${email}`);
    } catch (error) {
      console.log('Send Email Error: ', error);
    }
  };

  const openCall = phoneNumber => {
    try {
      Linking.openURL(`tel:${phoneNumber}`);
    } catch (error) {
      console.log('Error on Call: ', error);
    }
  };

  const sosmedOpenLink = url => {
    if (url !== '') {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log(`Don't know how to open URI: ${url}`);
        }
      });
    }
  };

  const toggleRedMore = () => {
    setIsDescExpand(!isDescExpand);
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <HeaderDefault isDefault={true} leftSide={() => goBack()} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loader />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <HeaderDefault isDefault={true} leftSide={() => goBack()} />
        <ErrorScreen message="Refresh" onPress={() => {}} />
      </Container>
    );
  } else {
    const {name, description} = merchantDetailData[0];

    return (
      <Container>
        <HeaderDefault leftSide={() => goBack()} />
        <Content>
          <Description
            isDescExpand={isDescExpand}
            name={name}
            description={description}
            toggleRedMore={toggleRedMore}
          />
          {merchantDetailData?.serviceType === 'Bridal' ? (
            <BadgeMerchant />
          ) : null}
          <ListMapAddress
            merchantDetail={merchantDetailData}
            openCall={openCall}
            openMap={openMap}
          />
          <AddressCardNonList
            merchantDetail={merchantDetailData}
            openCall={openCall}
            openMapByAddress={openMapByAddress}
            sosmedOpenLink={sosmedOpenLink}
            sendEmail={sendEmail}
          />
          <RelatedMerchant isLoading={refreshing} {...props} id={id} />
        </Content>
      </Container>
    );
  }
};

export const BadgeMerchant = () => {
  const listBadge = [
    {
      id: 1,
      image: blueBadge,
    },
    {
      id: 2,
      image: pinkBadge,
    },
  ];

  return (
    <FlatList
      data={listBadge}
      extraData={item => `${item.id}`}
      legacyImplementation
      disableVirtualization
      numColumns={2}
      columnWrapperStyle={{paddingLeft: 15, marginVertical: 15}}
      scrollEnabled={false}
      renderItem={({item}) => {
        return (
          <View
            style={{
              width: width / 5,
              height: height / 9,
              marginRight: 10,
            }}>
            <Image
              source={item.image}
              style={{flex: 1, width: '100%', height: height / 5}}
              resizeMode="contain"
            />
          </View>
        );
      }}
    />
  );
};

export const Description = props => {
  const {name, description, toggleRedMore, isDescExpand} = props;

  return (
    <Card transparent>
      <CardItem style={{width: '100%'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {name}
        </Text>
      </CardItem>
      <CardItem style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
        {description?.length > 200 ? (
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {isDescExpand
              ? description
              : `${description.substring(0, 200)}... `}
            <Text
              onPress={toggleRedMore}
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              {isDescExpand ? ' Read less' : 'Read more'}
            </Text>
          </Text>
        ) : (
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {description}
          </Text>
        )}
      </CardItem>
    </Card>
  );
};

export const AddressCardNonList = props => {
  const {
    merchantDetail,
    openCall,
    openMapByAddress,
    sosmedOpenLink,
    sendEmail,
  } = props;

  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card transparent style={{marginTop: 0}}>
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openMapByAddress(merchantDetail[0].address);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].address}`}</Text>
              </View>
            </CardItem>
          ) : null}
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openCall(
                  `${merchantDetail[0].countryCode}${merchantDetail[0].phone}`,
                );
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].countryCode}${
                  merchantDetail[0].phone
                }`}</Text>
              </View>
            </CardItem>
          ) : null}
          <CardItem
            button
            onPress={() => {
              sendEmail(merchantDetail[0].email);
            }}
            style={{width: '100%', top: 5}}>
            <View
              style={{
                flex: 0.1,
                height: '100%',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 3,
              }}>
              <Icon
                type="Feather"
                name="mail"
                style={{fontSize: RFPercentage(2), color: black}}
              />
            </View>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {merchantDetail[0].email}
              </Text>
            </View>
          </CardItem>
          <CardItem style={{width: '100%'}}>
            <View
              style={{
                height: 1,
                width: '100%',
                borderBottomColor: greyLine,
                borderBottomWidth: 0.5,
              }}
            />
          </CardItem>
          <CardItem>
            {merchantDetail[0].facebookLink ? (
              <ButtonSocialMedia
                iconType={facebook}
                onPress={() => sosmedOpenLink(merchantDetail[0].facebookLink)}
              />
            ) : null}
            {merchantDetail[0].instagramLink ? (
              <ButtonSocialMedia
                iconType={instagram}
                onPress={() => sosmedOpenLink(merchantDetail[0].instagramLink)}
              />
            ) : null}
            {merchantDetail[0].pinterestLink ? (
              <ButtonSocialMedia
                iconType={pinterest}
                onPress={() => sosmedOpenLink(merchantDetail[0].pinterestLink)}
              />
            ) : null}
            {merchantDetail[0].youtubeLink ? (
              <ButtonSocialMedia
                iconType={youtube}
                onPress={() => sosmedOpenLink(merchantDetail[0].youtubeLink)}
              />
            ) : null}
          </CardItem>
        </Card>
      );
    }, [merchantDetail]);

  if (merchantDetail) {
    return RenderCard();
  } else {
    return null;
  }
};

export const ListMapAddress = props => {
  const {merchantDetail, openCall, openMap} = props;
  const keyExt = item => `${String(item.id)}`;
  const RenderList = () =>
    useMemo(() => {
      return (
        <View>
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Locate Us
              </Text>
            </CardItem>
          </Card>
          <FlatList
            scrollEnabled={false}
            legacyImplementation
            disableVirtualization
            data={merchantDetail[0].addresses}
            extraData={merchantDetail[0].addresses}
            keyExtractor={keyExt}
            listKey={keyExt}
            renderItem={({item}) => {
              return (
                <MapAddressCard
                  item={item}
                  openCall={openCall}
                  openMap={openMap}
                />
              );
            }}
          />
        </View>
      );
    }, [merchantDetail]);
  if (merchantDetail?.length > 0) {
    return RenderList();
  } else {
    return null;
  }
};

export const MapAddressCard = props => {
  const {item, openCall, openMap} = props;
  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card
          style={{
            marginLeft: 15,
            marginRight: 15,
            paddingTop: 0,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Map
            buttonOnTheRight={true}
            disableContainerStyle={true}
            withDirectionText={false}
            forMapAddresses={`${item.locationName}, ${item.address}, ${
              item.country
            }`}
            onNavigate={() => openMap(item)}
          />
          <Card transparent style={{marginTop: 0}}>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>{`${item.locationName}`}</Text>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                openMap(item);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.6),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.locationName}, ${item.address}, ${
                  item.country
                }`}</Text>
              </View>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                openCall(`${item.countryCode}${item.phone}`);
              }}
              style={{width: '100%', backgroundColor: transparent}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.6),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.countryCode}${item.phone}`}</Text>
              </View>
            </CardItem>
          </Card>
        </Card>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const HeaderDefault = props => {
  const {isDefault, leftSide, rightSide} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => leftSide()}
          style={{
            backgroundColor: transparent,
            elevation: 0,
            shadowOpacity: 0,
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          About
        </Text>
      </Body>
      <Right style={{flex: 0.1}}>
        {/* {isDefault ? null : (
          <Button
            onPress={() => rightSide()}
            style={{
              backgroundColor: transparent,
              elevation: 0,
              shadowOpacity: 0,
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="share"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        )} */}
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(AboutMerchant);

export default props => <Wrapper {...props} />;
