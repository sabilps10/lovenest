import React, {useState, useEffect, useRef, useMemo} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  Modal,
  FlatList,
  Animated,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure/index';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {charImage} from '../../../../utils/Themes/Images';

// Redux
import {connect} from 'react-redux';
import FilterCategoriesRedux from '../../../../redux/thunk/FloristCategoriesFilterThunk';
import FilterThemeRedux from '../../../../redux/thunk/FloristThemeFilterThunk';
import FilterColorRedux from '../../../../redux/thunk/FloristColorFilterThunk';
import FilterTypeRedux from '../../../../redux/thunk/FloristTypeFilterThunk';

// Components
import PopularProduct from '../Components/PopularFlorist';
import AnimatedFloristCard from '../Components/FloristCard';

const {
  white,
  black,
  greyLine,
  mainGreen,
  mainRed,
  overlayDim,
  headerBorderBottom,
  superGrey,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');
const {asyncToken} = AsyncData;
const {charCategoryIcon, charEmptyFlorist} = charImage;

// Query
import GET_POPULAR_PRODUCT from '../../../../graphql/queries/floristPopularProduct';
import GET_PRODUCTS from '../../../../graphql/queries/floristCommonProducts';
import GET_FLORIST_CATEGORY from '../../../../graphql/queries/getFlowerCategories';

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

const Products = props => {
  console.log('Product Tab Screen Props: ', props);
  const {
    navigation,
    client,
    id,
    serviceType,
    detail,
    categories,
    selectedCategories: reduxSelectedCategory,
    price: priceState,
    size: sizeState,
    type: typeState,
    color: colorState,
    themes: themeState,
    status,
    filterPrice,
    filterSize,
    filterType,
    filterColor,
    filterStatus,
    filterCategories,
  } = props;

  const pageNumber = 1;

  const [modalCategoryVisible, setModalCategoryVisible] = useState(false);

  let [listCategory, setlistCategory] = useState([]);
  let [isLoadingCategory, setIsLoadingCategory] = useState(true);
  let [isErrorCategory, setIsErrorCategory] = useState(false);

  let [isLogin, setIsLogin] = useState(false);
  let [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  let [listPopularProduct, setListPopularProduct] = useState([]);
  let [pageSize, setPageSize] = useState(4);
  let [isLoadingPopularProduct, setIsLoadingPopularProduct] = useState(true);
  let [isErrorPopularProduct, setIsErrorPopularProduct] = useState(false);

  let [otherProduct, setOtherProduct] = useState([]);
  let [isLoadingOtherProduct, setIsLoadingOtherProduct] = useState(true);
  let [isErrorOtherProduct, setIsErrorOtherProduct] = useState(false);
  let [pageSizeOtherProduct, setPageSizeOtherProduct] = useState(10);

  let [refreshing, setRefreshing] = useState(false);
  let [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    checkIsLogin();

    if (modalCategoryVisible) {
      fetchFloristCategories(categories);
    }

    fetchPopularProduct();
    fetchOtherProduct();

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      checkIsLogin();
      if (modalCategoryVisible) {
        fetchFloristCategories(categories);
      }
      fetchPopularProduct();
      fetchOtherProduct();

      if (isLoadMore) {
        onLoadMore();
      }
    });

    return subs;
  }, [
    id,
    serviceType,
    refreshing,
    categories,
    reduxSelectedCategory,
    navigation,
    priceState,
    sizeState,
    typeState,
    colorState,
    status,
    modalCategoryVisible,
    isLoadMore,
  ]);

  const addToCartCustomize = dataProduct => {
    try {
      if (isLogin) {
        navigation.navigate('FloristCustomize', {data: dataProduct});
      } else {
        navigation.navigate('Login', {showXIcon: true});
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const toggleCategoryModal = async () => {
    try {
      await setModalCategoryVisible(!modalCategoryVisible);
    } catch (error) {
      console.log('Error: ', error);
      await setModalCategoryVisible(false);
    }
  };

  const getSelected = arrayDataSelected => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulated = await Promise.all(
          arrayDataSelected
            .map((d, i) => {
              if (d.selected) {
                return d.name;
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        if (manipulated) {
          resolve(manipulated);
        } else {
          resolve([]);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve([]);
      }
    });
  };

  const fetchFloristCategories = async reduxCategories => {
    console.log('KONTOL: >>>>>>> ', id);
    try {
      if (reduxCategories?.length !== 0) {
        console.log('MASUK SINI KONTOL');
        await setlistCategory([...reduxCategories]);
        await setIsErrorCategory(false);
        await setIsLoadingCategory(false);
      } else {
        console.log('Masuk SANA AKAKAKAKAKAKAKAK');
        await client
          .query({
            query: GET_FLORIST_CATEGORY,
            variables: {
              merchantId: parseInt(id, 10),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('fetchFloristCategory: ', response);
            const {data, errors} = response;
            const {floristCategories} = data;
            const {data: list, error} = floristCategories;

            if (errors) {
              await setIsErrorCategory(true);
              await setIsLoadingCategory(false);
            } else {
              if (error) {
                await setIsErrorCategory(true);
                await setIsLoadingCategory(false);
              } else {
                const manipulate = await Promise.all(
                  list.map((d, i) => {
                    return {
                      ...d,
                      selected: false,
                    };
                  }),
                );
                if (manipulate?.length === list?.length) {
                  await setlistCategory([...manipulate]);
                  await setIsErrorCategory(false);
                  await setIsLoadingCategory(false);
                } else {
                  throw new Error('Failed manipulated data categories Filter');
                }
              }
            }
          })
          .catch(error => {
            console.log('Error: ', error);
            throw error;
          });
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorCategory(true);
      await setIsLoadingCategory(false);
    }
  };

  const onLoadMore = async () => {
    try {
      await setPageSizeOtherProduct(pageSizeOtherProduct + 10);
      await fetchOtherProduct();
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const fetchOtherProduct = async () => {
    try {
      const getPriceFilter =
        parseFloat(priceState[0]) === 0 && parseFloat(priceState[1]) === 0
          ? null
          : {
              min: priceState[0] ? parseFloat(priceState[0]) : 0.0,
              max: priceState[1] ? parseFloat(priceState[1]) : 0.0,
            };
      console.log('getPriceFilter: ', getPriceFilter);

      const getSizeFilter =
        parseFloat(sizeState[0]) === 0 && parseFloat(sizeState[1]) === 0
          ? null
          : {
              min: sizeState[0] ? parseFloat(sizeState[0]) : 0.0,
              max: sizeState[1] ? parseFloat(sizeState[1]) : 0.0,
            };
      console.log('getSizeFilter: ', getSizeFilter);

      const selectedCategories =
        reduxSelectedCategory?.length === 0 ? [] : [reduxSelectedCategory[0]];
      console.log('selectedCategories: ', selectedCategories);

      const selectedType = await getSelected(typeState);
      console.log('selectedType: ', selectedType);

      const selectedColor = await getSelected(colorState);
      console.log('selectedColor: ', selectedColor);

      const selectedTheme = await getSelected(themeState);
      console.log('selectedTheme: ', selectedTheme);

      const variables = {
        merchantId: [parseInt(id, 10)],
        serviceType,
        priceRange: getPriceFilter,
        sizeRange: getSizeFilter,
        floristColor: selectedColor,
        floristType: selectedType,
        floristCategory: selectedCategories,
        floristTheme: selectedTheme,
        pageSize: parseInt(pageSizeOtherProduct, 10),
        pageNumber,
      };

      console.log('variables >>>> ', variables);

      await client
        .query({
          query: GET_PRODUCTS,
          variables,
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchOtherProduct: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsErrorOtherProduct(true);
            await setIsLoadingOtherProduct(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsErrorOtherProduct(true);
              await setIsLoadingOtherProduct(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setOtherProduct([...list]);
              await setIsErrorOtherProduct(false);
              await setIsLoadingOtherProduct(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorOtherProduct(true);
      await setIsLoadingOtherProduct(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  const fetchPopularProduct = async () => {
    try {
      await client
        .query({
          query: GET_POPULAR_PRODUCT,
          variables: {
            merchantId: [parseInt(id, 10)],
            serviceType,
            pageSize,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPopularProduct: ', response);
          const {data, errors} = response;
          const {productsPopular} = data;
          const {data: list, error} = productsPopular;

          if (errors) {
            await setIsErrorPopularProduct(true);
            await setIsLoadingPopularProduct(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsErrorPopularProduct(true);
              await setIsLoadingPopularProduct(false);
              await setRefreshing(false);
            } else {
              await setListPopularProduct([...list]);
              await setIsErrorPopularProduct(false);
              await setIsLoadingPopularProduct(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPopularProduct(true);
      await setIsLoadingPopularProduct(false);
      await setRefreshing(false);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await fetchOtherProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await fetchOtherProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkToken = () => {
    return new Promise(async resolve => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);

        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const userIsLogin = await checkToken();
      if (userIsLogin) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onUpdateSelected = async index => {
    try {
      await setRefreshing(true);
      let oldData = listCategory;
      const loop = await Promise.all(
        oldData.map((d, i) => {
          if (index === i) {
            return {
              ...d,
              selected: !d?.selected,
            };
          } else {
            return {
              ...d,
              selected: false,
            };
          }
        }),
      );

      if (loop) {
        const checkingLoop = await Promise.all(
          loop
            .map((d, i) => {
              if (d?.selected) {
                return {
                  ...d,
                };
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        console.log('checking loop: ', checkingLoop);
        if (checkingLoop?.length > 0) {
          await setlistCategory([...loop]);
          await filterCategories(
            [...loop],
            loop[index]?.selected ? [loop[index]?.name] : [],
          );
          await setModalCategoryVisible(false);
        } else {
          await setlistCategory([...loop]);
          await filterCategories([], []);
          await setModalCategoryVisible(false);
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const removeCategorySelected = async name => {
    try {
      await setRefreshing(true);
      const loop = await Promise.all(
        listCategory.map((d, i) => {
          if (d?.name === name) {
            return {
              ...d,
              selected: false,
            };
          } else {
            return {
              ...d,
              selected: false,
            };
          }
        }),
      );

      if (loop) {
        await filterCategories([], []);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  return (
    <Container style={{backgroundColor: white}}>
      <ModalLoader isLoading={isLoadingWishList} />
      <ModalListCategory
        onSelectItem={async index => onUpdateSelected(index)}
        isLoading={isLoadingCategory}
        isError={isErrorCategory}
        list={listCategory}
        isVisible={modalCategoryVisible}
        toogle={() => toggleCategoryModal()}
      />
      <View style={{flex: 1}}>
        {/* List Category */}
        <View style={{width: '100%', flexDirection: 'row'}}>
          <ScrollView
            horizontal
            contentContainerStyle={{
              flexDirection: 'row',
              width: '100%',
              paddingTop: 10,
              paddingBottom: 10,
            }}>
            <SortCategory toggleCategoryModal={toggleCategoryModal} />
            {reduxSelectedCategory?.length === 0 ? null : (
              <TouchableOpacity
                onPress={removeCategorySelected}
                style={{
                  marginLeft: 15,
                  borderWidth: 1,
                  borderRadius: 25,
                  borderColor: mainRed,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 5,
                  paddingLeft: 10,
                  paddingRight: 10,
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: mainRed,
                    letterSpacing: 0.3,
                  }}>
                  {reduxSelectedCategory[0]}
                </Text>
                <Icon
                  type="Feather"
                  name="x"
                  style={{marginLeft: 5, top: 1, fontSize: 15, color: mainRed}}
                />
              </TouchableOpacity>
            )}
          </ScrollView>
        </View>

        {/* List Filtered Data */}
        <View style={{width: '100%'}}>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal
            style={{
              flexDirection: 'row',
              width: '100%',
            }}
            contentContainerStyle={{
              padding: 2,
              paddingLeft: 7.5,
              paddingRight: 7.5,
              marginVertical:
                colorState?.length === 0 &&
                themeState?.length === 0 &&
                typeState?.length === 0
                  ? 0
                  : 5,
            }}>
            {/* type */}
            {typeState?.length === 0
              ? null
              : typeState.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            typeState.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === typeState?.length) {
                            console.log('typeState >>> ', updateData);
                            await props?.filterType(updateData);
                            await setRefreshing(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Color */}
            {colorState?.length === 0
              ? null
              : colorState.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            colorState.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === colorState?.length) {
                            console.log('colorState >>> ', updateData);
                            await props?.filterColor(updateData);
                            await setRefreshing(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Theme */}
            {themeState?.length === 0
              ? null
              : themeState.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            themeState.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === themeState?.length) {
                            console.log('themeState >>> ', updateData);
                            await props?.filterTheme(updateData);
                            await setRefreshing(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}
          </ScrollView>
        </View>

        <ListOtherProduct
          {...props}
          setIsLoadMore={async () => await setIsLoadMore(true)}
          isLoadMore={isLoadMore}
          onLoadMore={onLoadMore}
          refreshing={refreshing}
          onRefresh={onRefresh}
          list={otherProduct}
          isLoading={isLoadingOtherProduct}
          isError={isErrorOtherProduct}
          isLogin={isLogin}
          addToCartCustomize={dataProduct => addToCartCustomize(dataProduct)}
          runWishList={(productId, statusWhislist) =>
            runWishList(productId, statusWhislist)
          }>
          {reduxSelectedCategory?.length === 0 ? (
            <PopularProduct
              {...props}
              addToCartCustomize={dataProduct =>
                addToCartCustomize(dataProduct)
              }
              detail={detail}
              buttonDisable={true}
              isLogin={isLogin}
              isLoading={isLoadingPopularProduct}
              isError={isErrorPopularProduct}
              list={status ? [] : listPopularProduct}
              runWishList={(productId, statusWhislist) =>
                runWishList(productId, statusWhislist)
              }
            />
          ) : null}
        </ListOtherProduct>
      </View>
    </Container>
  );
};

export const SortCategory = props => {
  const {toggleCategoryModal} = props;
  return (
    <TouchableOpacity
      onPress={toggleCategoryModal}
      style={{
        marginLeft: 15,
        borderWidth: 1,
        borderRadius: 25,
        borderColor: mainRed,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
      }}>
      <View style={{flexDirection: 'row'}}>
        <Image
          source={charCategoryIcon}
          style={{width: 18, height: 18}}
          resizeMode="contain"
        />
        <Text
          style={{
            top: 1,
            marginLeft: 6,
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: mainRed,
            letterSpacing: 0.3,
          }}>
          Sort by Category
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export const ModalListCategory = props => {
  console.log('ModalListCategory: ', props);
  const {list, isLoading, isError, onSelectItem, isVisible, toogle} = props;

  const keyExt = item => `${item.name}`;

  return (
    <Modal visible={isVisible} transparent animationType="fade">
      <View style={{flex: 1, backgroundColor: overlayDim}}>
        <TouchableOpacity
          onPress={toogle}
          style={{flex: 1, backgroundColor: 'transparent'}}
        />
        <View
          style={{
            width: '100%',
            minHeight: '20%',
            maxHeight: '50%',
            backgroundColor: 'white',
            paddingBottom: 20,
          }}>
          <Card
            style={{
              position: 'relative',
              top: -5,
              zIndex: 2,
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
              elevation: 0,
              shadowOpacity: 0,
              border: 0,
              borderBottom: 1,
              borderBottomColor: 'grey',
            }}>
            <CardItem
              style={{
                backgroundColor: 'transparent',
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Button
                onPress={toogle}
                style={{
                  opacity: 1,
                  borderRadius: 26,
                  alignSelf: 'flex-end',
                  paddingTop: 0,
                  paddingBottom: 0,
                  height: 35,
                  width: 35,
                  justifyContent: 'center',
                  backgroundColor: 'white',
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: black,
                  }}
                />
              </Button>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: mainGreen,
                  letterSpacing: 0.3,
                }}>
                Sort By Category
              </Text>
              <Button
                disabled
                style={{
                  opacity: 0,
                  borderRadius: 26,
                  alignSelf: 'flex-end',
                  paddingTop: 0,
                  paddingBottom: 0,
                  height: 35,
                  width: 35,
                  justifyContent: 'center',
                  backgroundColor: white,
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: black,
                  }}
                />
              </Button>
            </CardItem>
          </Card>
          {isLoading && !isError ? (
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginVertical: 15}}
            />
          ) : !isLoading && isError ? null : (
            <FlatList
              ListEmptyComponent={() => {
                return (
                  <View
                    style={{
                      flex: 1,
                      height: height / 3,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontStyle: 'italic',
                        fontSize: RFPercentage(2.3),
                        color: greyLine,
                        textAlign: 'center',
                      }}>
                      No Category Found
                    </Text>
                  </View>
                );
              }}
              data={list}
              extraData={list}
              keyExtractor={keyExt}
              renderItem={({item, index}) => {
                return (
                  <Card
                    transparent
                    style={{
                      marginTop: 5,
                      marginBottom: 5,
                      marginRight: 10,
                      marginLeft: 10,
                    }}>
                    <CardItem
                      button
                      onPress={() => onSelectItem(index)}
                      style={{
                        borderBottomColor: greyLine,
                        borderBottomWidth: 1,
                        width: '100%',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={{
                            fontSize: RFPercentage(1.7),
                            fontFamily: book,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {item.name}
                        </Text>
                        <Text
                          style={{
                            marginLeft: 5,
                            fontSize: RFPercentage(1.7),
                            fontFamily: medium,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          ({item.total})
                        </Text>
                      </View>
                      <Icon
                        type="Feather"
                        name="check"
                        style={{
                          opacity: item?.selected ? 1 : 0,
                          fontSize: RFPercentage(2),
                          color: mainRed,
                        }}
                      />
                    </CardItem>
                  </Card>
                );
              }}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

export const ListOtherProduct = props => {
  console.log('LIST OTHER PRODUCT PROPS:', props);
  const {
    addToCartCustomize,
    list,
    isLoading,
    isError,
    runWishList,
    isLogin,
    refreshing,
    onRefresh,
    isLoadMore,
    onLoadMore,
    setIsLoadMore,
  } = props;

  const keyExt = item => `${item.id}`;

  if (isLoading && !isError) {
    return (
      <View
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color={mainGreen} />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0) {
      return (
        <View
          style={{
            padding: 15,
            paddingTop: 10,
            flex: 1,
            maxHeight: height / 1.41,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={charEmptyFlorist}
            style={{width, height: height / 3}}
            resizeMode="contain"
          />
          <Text
            style={{
              marginVertical: 15,
              fontFamily: medium,
              color: black,
              textAlign: 'center',
              fontSize: RFPercentage(2.5),
            }}>
            Out of Stock
          </Text>
          <Text
            style={{
              lineHeight: 20,
              fontFamily: book,
              color: black,
              textAlign: 'center',
              fontSize: RFPercentage(2),
            }}>
            Keep updated with us, preparing our products very soon !
          </Text>
        </View>
      );
    } else {
      const Rendering = () =>
        useMemo(() => {
          return (
            <FlatList
              ListEmptyComponent={() => {
                if (!isLoading) {
                  return (
                    <View
                      style={{
                        padding: 15,
                        paddingTop: 10,
                        flex: 1,
                        maxHeight: height / 1.41,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={charEmptyFlorist}
                        style={{width, height: height / 3}}
                        resizeMode="contain"
                      />
                      <Text
                        style={{
                          marginVertical: 15,
                          fontFamily: medium,
                          color: black,
                          textAlign: 'center',
                          fontSize: RFPercentage(2.5),
                        }}>
                        Out of Stock
                      </Text>
                      <Text
                        style={{
                          lineHeight: 20,
                          fontFamily: book,
                          color: black,
                          textAlign: 'center',
                          fontSize: RFPercentage(2),
                        }}>
                        Keep updated with us, preparing our products very soon !
                      </Text>
                    </View>
                  );
                } else {
                  return null;
                }
              }}
              ListFooterComponent={() => {
                if (isLoadMore) {
                  return (
                    <Card transparent>
                      <CardItem>
                        <View
                          style={{
                            opacity: isLoadMore ? 1 : 0,
                            width: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <ActivityIndicator
                            size="small"
                            color={mainGreen}
                            style={{marginVertical: 5}}
                          />
                          <Text
                            style={{
                              fontStyle: 'italic',
                              fontSize: 12,
                              color: greyLine,
                              textAlign: 'center',
                            }}>
                            Loading more...
                          </Text>
                        </View>
                      </CardItem>
                    </Card>
                  );
                } else {
                  return null;
                }
              }}
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
              ListHeaderComponent={() => {
                return <React.Fragment>{props.children}</React.Fragment>;
              }}
              onEndReached={({distanceFromEnd}) => {
                if (!isLoadMore) {
                  setIsLoadMore();
                }
              }}
              onEndReachedThreshold={0.01}
              legacyImplementation
              decelerationRate="fast"
              disableVirtualization
              data={list}
              extraData={list}
              keyExtractor={keyExt}
              numColumns={2}
              contentContainerStyle={{
                paddingBottom: 25,
              }}
              columnWrapperStyle={{
                paddingLeft: 10,
                paddingRight: 10,
                marginBottom: 15,
              }}
              renderItem={({item, index}) => {
                return (
                  <AnimatedFloristCard
                    isLogin={isLogin}
                    addToCartCustomize={dataProduct =>
                      addToCartCustomize(dataProduct)
                    }
                    runWishList={(id, isWishlist) =>
                      runWishList(id, isWishlist)
                    }
                    {...props}
                    item={item}
                    index={index}
                  />
                );
              }}
            />
          );
        }, [
          list,
          isLoading,
          isError,
          runWishList,
          isLogin,
          refreshing,
          onRefresh,
          isLoadMore,
          onLoadMore,
        ]);
      return Rendering();
    }
  }
};

export const ModalLoader = props => {
  const {isLoading} = props;

  if (isLoading) {
    return (
      <Modal visible={isLoading} transparent animationType="fade">
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: overlayDim,
          }}>
          <View
            style={{
              backgroundColor: white,
              borderRadius: 4,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 15,
            }}>
            <ActivityIndicator color={mainGreen} />
          </View>
        </View>
      </Modal>
    );
  } else {
    return null;
  }
};

const mapToState = state => {
  const {florist} = state;
  return {
    ...florist,
  };
};

const mapToDispatch = dispatch => {
  return {
    filterCategories: (categories, selectedCategory) =>
      dispatch(FilterCategoriesRedux(categories, selectedCategory)),
    filterColor: colors => dispatch(FilterColorRedux(colors)),
    filterType: types => dispatch(FilterTypeRedux(types)),
    filterTheme: themes => dispatch(FilterThemeRedux(themes)),
  };
};

const connector = connect(
  mapToState,
  mapToDispatch,
)(Products);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
