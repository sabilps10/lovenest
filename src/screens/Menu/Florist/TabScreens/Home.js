/* eslint-disable no-unused-vars */
import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, Text, Dimensions, SafeAreaView} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';
import {Footer, Container, Content, FooterTab} from 'native-base';

// Query
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';
import GET_IMAGE_CARD from '../../../../graphql/queries/getImageCardFlorist';
import GET_POPULAR_PRODUCT from '../../../../graphql/queries/floristPopularProduct';
import GET_PRODUCTS from '../../../../graphql/queries/floristCommonProducts';
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

// Components
import TopBanner from '../Components/TopBanner';
import ShowImageCardByIndex from '../Components/ShowImageCardByIndex';
import ListImageCard from '../Components/ListImageCard';
import ListPromotion from '../Components/ListPromotion';
import PopularProduct from '../Components/PopularFlorist';
import OtherProduct from '../Components/OtherProduct';

const {white, mainGreen} = Colors;
const {medium} = FontType;

import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure';
const {asyncToken} = AsyncData;

const {width, height} = Dimensions.get('window');

const Home = props => {
  console.log('Home Screen Props: ', props);
  const {client, id, serviceType, navigation} = props;
  const pageNumber = 1;

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [detail, setDetail] = useState([]);
  const [isLoadingDetail, setIsLoadingDetail] = useState(true);
  const [isErrorDetail, setIsErrorDetail] = useState(false);

  const [imageCardList, setImageCardList] = useState([]);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [isLoadingImageCard, setIsLoadingImageCard] = useState(true);
  const [isErrorImageCard, setIsErrorImageCard] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [listPopularProduct, setListPopularProduct] = useState([]);
  const [pageSize, setPageSize] = useState(4);
  const [isLoadingPopularProduct, setIsLoadingPopularProduct] = useState(true);
  const [isErrorPopularProduct, setIsErrorPopularProduct] = useState(false);

  const [listOtherProduct, setListOtherProduct] = useState([]);
  const [isLoadingOtherProduct, setIsLoadingOtherProduct] = useState(true);
  const [isErrorOtherProduct, setIsErrorOtherProduct] = useState(false);

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoadingPromotion, setIsLoadingPromotion] = useState(true);
  const [isErrorPromotion, setIsErrorPromotion] = useState(false);
  const [itemDisplayedPromotion, setItemDisplayedPromotion] = useState(1);

  useEffect(() => {
    runCheckingLogin();
    fetchMerchantDetail();
    fetchImagecard();
    fetchPopularProduct();
    fetchOtherProduct();
    fetchPromotion();

    const subs1 = navigation.addListener('focus', () => {
      runCheckingLogin();
      fetchMerchantDetail();
      fetchImagecard();
      fetchPopularProduct();
      fetchOtherProduct();
      fetchPromotion();
    });

    // const subs2 = navigation.addListener('tabPress', () => {
    //   runCheckingLogin();
    //   fetchMerchantDetail();
    //   fetchImagecard();
    //   fetchPopularProduct();
    //   fetchOtherProduct();
    //   fetchPromotion();
    // });

    return () => {
      subs1();
      // subs2();
    };
  }, [navigation, id, serviceType]);

  const fetchPromotion = async () => {
    try {
      await client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(id, 10),
            itemDisplayed: parseInt(itemDisplayedPromotion, 10),
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error} = getAllPromotions;

          if (errors) {
            await setIsErrorPromotion(true);
            await setIsLoadingPromotion(false);
          } else {
            if (error) {
              await setIsErrorPromotion(true);
              await setIsLoadingPromotion(false);
            } else {
              await setListPromotion([...list]);
              await setIsErrorPromotion(false);
              await setIsLoadingPromotion(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPromotion(true);
      await setIsLoadingPromotion(false);
    }
  };

  const fetchOtherProduct = async () => {
    try {
      await client
        .query({
          query: GET_PRODUCTS,
          variables: {
            serviceType,
            merchantId: parseInt(id, 10),
            pageSize: 6,
            pageNumber,
            // isRandom: true,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchOtherProduct: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsErrorOtherProduct(true);
            await setIsLoadingOtherProduct(false);
          } else {
            if (error) {
              await setIsErrorOtherProduct(true);
              await setIsLoadingOtherProduct(false);
            } else {
              await setListOtherProduct([...list]);
              await setIsErrorOtherProduct(false);
              await setIsLoadingOtherProduct(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorOtherProduct(true);
      await setIsLoadingOtherProduct(false);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkIsLogin = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const runCheckingLogin = async () => {
    try {
      const loginStatus = await checkIsLogin();
      if (loginStatus) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      await setIsLogin(false);
    }
  };

  const fetchPopularProduct = async () => {
    try {
      await client
        .query({
          query: GET_POPULAR_PRODUCT,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType,
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchPopularProduct: ', response);
          const {data, errors} = response;
          const {productsPopular} = data;
          const {data: listPopularProducts, error} = productsPopular;

          if (errors) {
            await setIsErrorPopularProduct(true);
            await setIsLoadingPopularProduct(false);
          } else {
            if (error) {
              await setIsErrorPopularProduct(true);
              await setIsLoadingPopularProduct(false);
            } else {
              await setListPopularProduct([...listPopularProducts]);
              await setIsErrorPopularProduct(false);
              await setIsLoadingPopularProduct(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchPopularProduct', error);
      await setIsErrorPopularProduct(true);
      await setIsLoadingPopularProduct(false);
    }
  };

  const fetchImagecard = async () => {
    try {
      await client
        .query({
          query: GET_IMAGE_CARD,
          variables: {
            merchantId: parseInt(id, 10),
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetchImageCard: ', response);
          const {data, errors} = response;
          const {getImageCards} = data;
          const {data: list, error} = getImageCards;

          if (errors) {
            await setIsErrorImageCard(true);
            await setIsLoadingImageCard(false);
          } else {
            if (error) {
              await setIsErrorImageCard(true);
              await setIsLoadingImageCard(false);
            } else {
              await setImageCardList([...list]);
              await setIsErrorImageCard(false);
              await setIsLoadingImageCard(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchImagecard', error);
      await setIsErrorImageCard(true);
      await setIsLoadingImageCard(false);
    }
  };

  const fetchMerchantDetail = async () => {
    try {
      console.log('ANUUUUUU: ', props);
      await client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchMerchantDetail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsErrorDetail(true);
            await setIsLoadingDetail(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsErrorDetail(false);
            await setIsLoadingDetail(false);
          }
        })
        .catch(error => {
          console.log('Error: BROOO', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: fetchMerchantDetail', error);
      await setIsErrorDetail(true);
      await setIsLoadingDetail(false);
    }
  };

  const addToCartCustomize = dataProduct => {
    try {
      if (isLogin) {
        navigation.navigate('FloristCustomize', {data: dataProduct});
      } else {
        navigation.navigate('Login', {showXIcon: true});
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 15,
      }}>
      <TopBanner
        {...props}
        isLoading={isLoadingDetail}
        isError={isErrorDetail}
        detail={detail}
      />
      <ShowImageCardByIndex
        listImageCard={imageCardList}
        isLoading={isLoadingImageCard}
        isError={isErrorImageCard}
        showByIndex={0}
      />
      <PopularProduct
        {...props}
        addToCartCustomize={dataProduct => addToCartCustomize(dataProduct)}
        detail={detail}
        buttonDisable={false}
        isLogin={isLogin}
        isLoading={isLoadingPopularProduct}
        isError={isErrorPopularProduct}
        list={listPopularProduct}
        runWishList={(productId, statusWhislist) =>
          runWishList(productId, statusWhislist)
        }
      />
      <ShowImageCardByIndex
        listImageCard={imageCardList}
        isLoading={isLoadingImageCard}
        isError={isErrorImageCard}
        showByIndex={1}
      />
      <View
        style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <OtherProduct
          {...props}
          detail={detail}
          listOtherProduct={listOtherProduct}
          isLoading={isLoadingOtherProduct}
          isError={isErrorOtherProduct}
        />
      </View>
      <ListImageCard
        listImageCard={imageCardList}
        isLoading={isLoadingImageCard}
        isError={isErrorImageCard}
        isLoadMore={isLoadMore}
      />
      <ListPromotion
        {...props}
        detail={detail}
        list={listPromotion}
        isLoading={isLoadingPromotion}
        isError={isErrorPromotion}
      />
      <TouchableOpacity
        onPress={() => {
          try {
            navigation.navigate('Enquiry', {id, serviceType});
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
        style={{
          marginTop: 15,
          justifyContent: 'center',
          alignItems: 'center',
          width,
          height: hasNotch() ? 60 : 55,
          backgroundColor: mainGreen,
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: white,
          }}>
          ENQUIRY NOW
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Wrapper = compose(withApollo)(Home);

export default props => <Wrapper {...props} />;
