import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  RefreshControl,
  Image,
  FlatList,
  View,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';

import {charImage} from '../../../../utils/Themes/Images';

const {charEmptyFloristPromotion, charDelivery, charSelfPickup} = charImage;

// Query
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';

// Components
import PromotionCard from '../Components/PromotionCard';

const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  mainRed,
  mainGreen,
  greyLine,
  superGrey,
  headerBorderBottom,
  transparent,
} = Colors;
const {book, medium} = FontType;

const Promotions = props => {
  console.log('Tab Promotion Screen: ', props);
  const {navigation, client, id, serviceType} = props;

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const pageNumber = 1;

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchPromotion();

    if (refreshing) {
      fetchPromotion();
    }

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      fetchPromotion();
      if (refreshing) {
        fetchPromotion();
      }
      if (isLoadMore) {
        onLoadMore();
      }
    });

    return subs;
  }, [navigation, id, serviceType, refreshing, isLoadMore]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        await setItemDisplayed(itemDisplayed + 10);
        await fetchPromotion();
      }
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetchPromotion = async () => {
    try {
      await client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(id, 10),
            itemDisplayed,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion Response: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error} = getAllPromotions;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await setListPromotion([...list]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <FlatList
        ListFooterComponent={() => {
          if (isLoadMore) {
            return (
              <Card transparent>
                <CardItem>
                  <View
                    style={{
                      opacity: isLoadMore ? 1 : 0,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator
                      size="small"
                      color={mainGreen}
                      style={{marginVertical: 5}}
                    />
                    <Text
                      style={{
                        fontStyle: 'italic',
                        fontSize: 12,
                        color: greyLine,
                        textAlign: 'center',
                      }}>
                      Loading more...
                    </Text>
                  </View>
                </CardItem>
              </Card>
            );
          } else {
            return null;
          }
        }}
        ListEmptyComponent={() => {
          if (!isLoading) {
            return (
              <View
                style={{
                  padding: 15,
                  paddingTop: 0,
                  paddingBottom: 0,
                  flex: 1,
                  height: height / 1.28,
                  justifyContent: 'center',
                  alignItems: 'center',
                  // borderWidth: 1,
                }}>
                <Image
                  source={charEmptyFloristPromotion}
                  style={{width, height: height / 3}}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginVertical: 15,
                    fontFamily: medium,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                  }}>
                  Promotion Soon
                </Text>
                <Text
                  style={{
                    lineHeight: 20,
                    fontFamily: book,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2),
                  }}>
                  Keep updated with us, preparing our promotion very soon !
                </Text>
              </View>
            );
          } else {
            return null;
          }
        }}
        contentContainerStyle={{
          paddingLeft: 15,
          paddingBottom: 15,
          paddingRight: 15,
        }}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
        data={listPromotion}
        extraData={listPromotion}
        keyExtractor={item => `${item.id}`}
        onEndReachedThreshold={0.1}
        legacyImplementation
        decelerationRate="fast"
        onEndReached={async () => {
          if (!isLoadMore) {
            await setIsLoadMore(true);
          }
        }}
        renderItem={({item, index}) => {
          return (
            <View style={{marginVertical: 10}}>
              <PromotionCard
                item={item}
                index={index}
                onPress={() => {
                  navigation.navigate('FloristPromotionDetail', {id: item?.id});
                }}
              />
            </View>
          );
        }}
      />
    </View>
  );
};

const Wrapper = compose(withApollo)(Promotions);

export default props => <Wrapper {...props} />;
