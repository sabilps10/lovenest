import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {charImage} from '../../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;
const {
  white,
  black,
  overlayDim,
  headerBorderBottom,
  superGrey,
  greyLine,
  mainGreen,
  mainRed,
} = Colors;
const {charNoPortfolio} = charImage;

import GET_PORTO from '../../../../graphql/queries/projectPublicFlorist';

const Portfolio = props => {
  console.log('Portfolio Tab: ', props);
  const {navigation, client, id, serviceType} = props;

  const [listPorto, setListPorto] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize, setPageSize] = useState(5);
  const pageNumber = 1;

  const [refreshing, setRefreshing] = useState(false);
  let [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchPorto();

    if (refreshing) {
      fetchPorto();
    }

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      fetchPorto();
      if (refreshing) {
        fetchPorto();
      }

      if (isLoadMore) {
        onLoadMore();
      }
    });

    return () => {
      subs();
    };
  }, [navigation, refreshing, isLoadMore]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        await setPageSize(pageSize + 10);
        await fetchPorto();
      }
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetchPorto = async () => {
    try {
      await client
        .query({
          query: GET_PORTO,
          variables: {
            serviceType,
            merchantId: [parseInt(id, 10)],
            pageSize,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPorto Response: ', response);
          const {data, errors} = response;
          const {projectsPublic} = data;
          const {data: list, error} = projectsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setListPorto([...list]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{color: greyLine, fontStyle: 'italic'}}>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Content>
          <View
            style={{
              flex: 1,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: greyLine, fontStyle: 'italic'}}>
              Loading...
            </Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <FlatList
          decelerationRate="fast"
          legacyImplementation
          disableVirtualization
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <Card transparent>
                  <CardItem>
                    <View
                      style={{
                        opacity: isLoadMore ? 1 : 0,
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <ActivityIndicator
                        size="small"
                        color={mainGreen}
                        style={{marginVertical: 5}}
                      />
                      <Text
                        style={{
                          fontStyle: 'italic',
                          fontSize: 12,
                          color: greyLine,
                          textAlign: 'center',
                        }}>
                        Loading more...
                      </Text>
                    </View>
                  </CardItem>
                </Card>
              );
            } else {
              return null;
            }
          }}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  padding: 15,
                  paddingTop: 0,
                  paddingBottom: 0,
                  flex: 1,
                  height: height / 1.28,
                  justifyContent: 'center',
                  alignItems: 'center',
                  // borderWidth: 1,
                }}>
                <Image
                  source={charNoPortfolio}
                  style={{width, height: height / 3}}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginVertical: 15,
                    fontFamily: medium,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                  }}>
                  No Portfolio Yet
                </Text>
                <Text
                  style={{
                    lineHeight: 20,
                    fontFamily: book,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2),
                  }}>
                  This section will contain our portfolio, base on occassion.
                </Text>
              </View>
            );
          }}
          onEndReached={async () => {
            if (!isLoadMore) {
              await setIsLoadMore(true);
            }
          }}
          onEndReachedThreshold={0.1}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          contentContainerStyle={{
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 15,
          }}
          data={listPorto}
          extraData={listPorto}
          // data={[]}
          // extraData={[]}
          keyExtractor={item => `${item.id}`}
          renderItem={({item, index}) => {
            return <PortoCard {...props} item={item} index={index} />;
          }}
        />
      </View>
    );
  }
};

export const PortoCard = props => {
  const {item, index, navigation} = props;

  if (item) {
    const {
      id,
      name,
      description,
      featuredImageDynamicURL,
      featuredImageURL,
      galleries,
      floristTheme,
    } = item;

    const RenderCard = () => {
      return useMemo(() => {
        return (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => navigation.navigate('FloristProjectDetail', {id})}>
            <Card style={{elevation: 0, shadowOpacity: 0}}>
              <CardItem cardBody style={{backgroundColor: 'transparent'}}>
                <View style={{width: '100%', height: height / 4}}>
                  {galleries?.length === 0 || galleries === 1 ? (
                    <AsyncImage
                      source={
                        featuredImageDynamicURL
                          ? {uri: `${featuredImageDynamicURL}=h500`}
                          : {uri: `${featuredImageURL}`}
                      }
                      style={{flex: 1, width: '100%', height: '100%'}}
                      placeholderColor={white}
                      resizeMode="cover"
                      loaderStyle={{width: width / 7, height: height / 7}}
                    />
                  ) : (
                    <DynamicImage images={galleries} />
                  )}
                </View>
              </CardItem>
              <CardItem>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  {name}
                </Text>
              </CardItem>
              <CardItem
                style={{
                  paddingTop: 0,
                  width: '100%',
                  backgroundColor: 'transparent',
                }}>
                <View>
                  <Icon
                    type="Feather"
                    name="tag"
                    style={{fontSize: RFPercentage(2), color: superGrey}}
                  />
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    paddingRight: 15,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={{
                      right: 10,
                      color: superGrey,
                      fontSize: RFPercentage(1.4),
                      fontFamily: book,
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    {floristTheme?.join(', ')}
                  </Text>
                </View>
              </CardItem>
            </Card>
          </TouchableOpacity>
        );
      }, [item, index]);
    };

    return RenderCard();
  } else {
    return null;
  }
};

export const DynamicImage = props => {
  const {images} = props;

  if (images && images?.length !== 0) {
    if (images?.length >= 3) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View style={{flex: 1, paddingRight: 2}}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{width: '100%', height: '100%', flex: 1}}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 1, paddingLeft: 2}}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
            <View style={{flex: 1, paddingLeft: 2, paddingTop: 4}}>
              <AsyncImage
                source={
                  images[2]?.dynamicUrl
                    ? {uri: `${images[2]?.dynamicUrl}=h500`}
                    : {uri: `${images[2]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length >= 2) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View style={{flex: 1, paddingRight: 2}}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{width: '100%', height: '100%', flex: 1}}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 1, paddingLeft: 2}}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{width: '100%', height: '100%', flex: 1}}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length === 1) {
      return (
        <View style={{flex: 1}}>
          <AsyncImage
            source={
              images[0]?.dynamicUrl
                ? {uri: `${images[0]?.dynamicUrl}=h500`}
                : {uri: `${images[0]?.url}`}
            }
            resizeMode="cover"
            style={{width: '100%', height: '100%', flex: 1}}
            placeholderColor={white}
            loaderStyle={{width: width / 15, height: height / 15}}
          />
        </View>
      );
    }
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(Portfolio);

export default props => <Wrapper {...props} />;
