import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Platform,
  Animated,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Header,
  Footer,
  FooterTab,
} from 'native-base';
import {hasNotch} from 'react-native-device-info';
import JustLoader from '../../../components/Loader/JustLoader';
import Loader from '../../../components/Loader/LoaderWithContainer';
import AsyncImage from '../../../components/Image/AsyncImage';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncModel from '../../../utils/AsyncstorageDataStructure';
import ModalImageViewer from './Components/ImageViewer';

// Query
import GET_FLORIST_DETAIL from '../../../graphql/queries/fetchProduct';
import FloristCartBadge from '../../../graphql/queries/floristCartBadge';

// Mutation
import MutationAddWishList from '../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../graphql/mutations/removeWishlist';

const {asyncToken} = AsyncModel;
const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  brownCream,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const heightSlider = height / 1.58;

const FloristDetail = props => {
  console.log('FloristDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [badge, setBadge] = useState(0);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refresh, setRefresh] = useState(false);

  const [dataProduct, setDataProduct] = useState(null);

  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);
  const [isLogin, setIsLogin] = useState(true);

  const [openImageViewer, setOpenImageViewer] = useState(false);

  useEffect(() => {
    checkIsLogin();
    fetchProduct();
    fetchBadge();
    const subscriber = navigation.addListener('focus', () => {
      checkIsLogin();
      fetchProduct();
      fetchBadge();
    });

    return subscriber;
  }, [navigation, badge]);

  const openImageModal = async () => {
    try {
      await setOpenImageViewer(!openImageViewer);
    } catch (error) {
      console.log('Error: ', error);
      await setOpenImageViewer(false);
    }
  };

  const fetchBadge = async () => {
    try {
      client
        .query({
          query: FloristCartBadge,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('response badge: ', response);
          const {data, errors} = response;
          const {cartBadge} = data;
          const {totalCount, error} = cartBadge;

          if (errors) {
            await setBadge(0);
          } else {
            if (error) {
              await setBadge(0);
            } else {
              if (totalCount) {
                await setBadge(totalCount);
              } else {
                await setBadge(0);
              }
            }
          }
        })
        .catch(async error => {
          console.log('error: ', error);
          await setBadge(0);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setBadge(0);
    }
  };

  const runCheckingLogin = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);
        console.log('token >>> ', token);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const login = await runCheckingLogin();
      console.log('Login >>>> : ', login);
      await setIsLogin(login);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefresh(true);
      await fetchProduct();
      await fetchBadge();
    } catch (error) {
      console.log('Error: ', error);
      await setRefresh(false);
    }
  };

  const fetchProduct = () => {
    try {
      client
        .query({
          query: GET_FLORIST_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response get product detail: ', response);
          const {data, errors} = response;
          const {fetchProduct: resFetchProduct} = data;
          const {data: product, error} = resFetchProduct;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefresh(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefresh(false);
            } else {
              await setDataProduct(product);
              await setIsError(false);
              await setIsLoading(false);
              await setRefresh(false);
            }
          }
        })
        .catch(error => {
          console.log('Error get product detail: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefresh(false);
        });
    } catch (error) {
      console.log('Error get product detail: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefresh(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un wishlist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {data: resp, error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {data: resp, error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  if (isLoading && !isError) {
    return <Loader />;
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers goBack={goBack} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={async () => {
              try {
                await setIsLoading(true);
                await fetchProduct();
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            style={{
              borderRadius: 4,
              borderWidth: 1,
              height: height / 17,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text style={{fontFamily: medium, fontSize: RFPercentage(1.8)}}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <StatusBar translucent={false} backgroundColor={white} />
        <CricleButtonBack navigation={navigation} />
        {isLoadingWishList ? (
          <View
            style={{
              flex: 1,
              zIndex: 99,
              position: 'absolute',
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: width / 4,
                height: height / 10,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: white,
                borderRadius: 15,
              }}>
              <JustLoader />
            </View>
          </View>
        ) : null}
        <ModalImageViewer
          visible={openImageViewer}
          onPress={openImageModal}
          images={dataProduct?.galleries}
        />
        <Content
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
          }>
          <ProductImageSlider
            {...props}
            openImageModal={openImageModal}
            isLogin={isLogin}
            productId={dataProduct.id}
            isWishlist={dataProduct.isWishlist}
            runWishList={(productId, isWishlist) =>
              runWishList(productId, isWishlist)
            }
            gallery={dataProduct?.galleries ? dataProduct.galleries : []}
          />
          <HeaderTitleAndDescription data={dataProduct} />
        </Content>
        {isLogin && badge > 0 ? (
          <TouchableOpacity
            onPress={() =>
              navigation.push('FloristCart', {from: 'FloristDetail'})
            }
            style={{
              zIndex: 99,
              width: 60,
              height: 60,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              bottom: Platform.OS === 'android' ? 85 : hasNotch() ? 100 : 85,
              right: 15,
              borderRadius: 4,
              backgroundColor: '#FACFC7',
            }}>
            <View
              style={{
                width: 18,
                height: 18,
                borderRadius: 18 / 2,
                backgroundColor: mainRed,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 10,
                right: 10,
                zIndex: 99,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.2),
                  color: white,
                }}>
                {badge > 99 ? '99++' : badge}
              </Text>
            </View>
            <Icon
              type="Feather"
              name="shopping-bag"
              style={{fontSize: 35, color: black}}
            />
          </TouchableOpacity>
        ) : null}
        {isLogin ? (
          <ButtonAddToCart
            title={'ADD TO CART'}
            onPress={() => {
              navigation.navigate('FloristCustomize', {data: dataProduct});
            }}
          />
        ) : null}
      </Container>
    );
  }
};

export const ButtonAddToCart = props => {
  const {onPress, title} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const HeaderTitleAndDescription = props => {
  console.log('HeaderTitleAndDescription: >>> ', props);
  const {data} = props;
  const {
    name,
    description,
    type,
    isFreeDelivery,
    floristPrice,
    floristSize,
    floristCategory,
    floristType,
    floristColor,
    floristTheme,
    floristExtraSize,
    discount,
    floristPriceAfterDiscount,
  } = data;

  return (
    <Card
      transparent
      style={{
        bottom: 15,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem style={{width: '100%'}}>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <View>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.5),
                color: black,
                letterSpacing: 0.39,
                lineHeight: 25,
                textAlign: 'left',
              }}>
              {name}
            </Text>
          </View>
          {discount === 0 ? null : (
            <View
              style={{
                backgroundColor: mainRed,
                padding: 2,
                paddingLeft: 10,
                paddingRight: 10,
                borderRadius: 25,
                marginTop: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.5),
                  color: white,
                  letterSpacing: 0.39,
                  lineHeight: 25,
                  textAlign: 'left',
                }}>
                Discount {discount}%
              </Text>
            </View>
          )}
          {isFreeDelivery ? (
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                padding: 10,
                paddingTop: 2,
                paddingBottom: 2,
                backgroundColor: mainRed,
                borderRadius: 25,
              }}>
              <Icon
                type="Feather"
                name="truck"
                style={{fontSize: RFPercentage(2.5), color: white}}
              />
              <Text
                style={{
                  right: 5,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                FREE DELIVERY
              </Text>
            </View>
          ) : null}
        </View>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            // flexWrap: 'wrap',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(3),
              color: black,
              letterSpacing: 0.39,
              lineHeight: 25,
              textAlign: 'right',
            }}>
            $
            {floristPriceAfterDiscount
              ? floristPriceAfterDiscount.toFixed(2)
              : 'N/A'}
          </Text>
          {floristPriceAfterDiscount &&
          floristPriceAfterDiscount !== floristPrice ? (
            <Text
              style={
                floristPriceAfterDiscount &&
                floristPriceAfterDiscount !== floristPrice
                  ? {
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: greyLine,
                      letterSpacing: 0.39,
                      lineHeight: 25,
                      textAlign: 'right',
                      textDecorationLine: 'line-through',
                    }
                  : {
                      fontFamily: medium,
                      fontSize: RFPercentage(2.5),
                      color: black,
                      letterSpacing: 0.39,
                      lineHeight: 25,
                      textAlign: 'right',
                    }
              }>
              ${floristPrice ? floristPrice.toFixed(2) : 'N/A'}
            </Text>
          ) : null}
        </View>
      </CardItem>
      <CardItem style={{paddingBottom: 0}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: greyLine,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Size
        </Text>
      </CardItem>
      <CardItem style={{paddingTop: 5}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {floristSize} Inches
        </Text>
      </CardItem>

      {/* Florist category */}
      <CardItem style={{paddingBottom: 0}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: greyLine,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Florist Category
        </Text>
      </CardItem>
      <CardItem style={{paddingTop: 5}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {floristCategory}
        </Text>
      </CardItem>

      {/* Florist Type */}
      {!floristType ? null : floristType?.length ? (
        floristType.length !== 0 ? (
          <>
            <CardItem style={{paddingBottom: 0}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Florist Type(s)
              </Text>
            </CardItem>
            <CardItem style={{paddingTop: 5}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                {floristType.join(', ')}
              </Text>
            </CardItem>
          </>
        ) : null
      ) : null}

      {/* Florist Color */}
      {!floristColor ? null : floristColor?.length ? (
        floristColor.length !== 0 ? (
          <>
            <CardItem style={{paddingBottom: 0}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Florist Color(s)
              </Text>
            </CardItem>
            <CardItem style={{paddingTop: 5}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                {floristColor.join(', ')}
              </Text>
            </CardItem>
          </>
        ) : null
      ) : null}

      {/* Florist Theme */}
      {!floristTheme ? null : floristTheme?.length ? (
        floristTheme.length !== 0 ? (
          <>
            <CardItem style={{paddingBottom: 0}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Florist Theme(s)
              </Text>
            </CardItem>
            <CardItem style={{paddingTop: 5}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                {floristTheme.join(', ')}
              </Text>
            </CardItem>
          </>
        ) : null
      ) : null}

      {/* Extra Size Section */}
      {/* {!floristExtraSize ? null : floristExtraSize?.length ? (
        floristExtraSize.length !== 0 ? (
          <>
            <CardItem style={{paddingBottom: 0}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                }}>
                Extra Size Option(s)
              </Text>
            </CardItem>
            {floristExtraSize.map((d, i) => {
              return (
                <CardItem key={i} style={{paddingTop: 5, width: '100%'}}>
                  <View style={{width: '100%', flexDirection: 'row'}}>
                    <View
                      style={{
                        flex: 0.5,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                      }}>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: RFPercentage(1.8),
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        {d.size} Inches
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#FBF1E4',
                          paddingTop: 5,
                          paddingBottom: 5,
                          justifyContent: 'center',
                          alignSelf: 'flex-end',
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                          paddingLeft: 5,
                          paddingRight: 5,
                          borderWidth: 0,
                          borderRadius: 25,
                        }}>
                        <View
                          style={{
                            height: 15,
                            width: 15,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            borderRadius: 15 / 2,
                          }}>
                          <Icon
                            type="Feather"
                            name="arrow-up"
                            style={{
                              left: Platform.OS === 'android' ? 11 : 12.4,
                              fontSize: 10,
                              color: black,
                            }}
                          />
                        </View>
                        <View
                          style={{
                            marginHorizontal: 5,
                            justifyContent: 'center',
                          }}>
                          <Text
                            style={{
                              fontFamily: medium,
                              fontSize: RFPercentage(1.7),
                              color: black,
                              letterSpacing: 0.3,
                            }}>
                            {' '}
                            UPSIZE OPTION: + $
                            {d?.price ? d.price.toFixed(2) : 'N/A'}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </CardItem>
              );
            })}
          </>
        ) : null
      ) : null} */}
      <CardItem style={{paddingBottom: 0}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: greyLine,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Desciption
        </Text>
      </CardItem>
      <CardItem style={{paddingTop: 5}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {description}
        </Text>
      </CardItem>
    </Card>
  );
};

export const CardImage = props => {
  const {item} = props;
  const {url, dynamicUrl} = item;
  const logoSource = dynamicUrl ? {uri: `${dynamicUrl}=h800`} : {uri: url};
  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card
          style={{
            elevation: 0,
            shadowOpacity: 0,
            marginLeft: 0,
            marginTop: 0,
            marginBottom: 0,
            marginRight: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            width,
          }}>
          <CardItem cardBody>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                shadowOpacity: 0,
                elevation: 0,
                width,
                height: heightSlider,
                aspectRatio: width / heightSlider,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={'#f8f8f8'}
            />
          </CardItem>
        </Card>
      );
    }, [item]);

  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const HeartButton = props => {
  const {isWishlist, id, runWishList} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 20,
        right: 15,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          runWishList(id, isWishlist);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 35,
          width: 35,
          justifyContent: 'center',
          borderRadius: 35 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        {isWishlist ? (
          <Icon
            type="MaterialCommunityIcons"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        ) : (
          <Icon
            type="Feather"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        )}
      </TouchableOpacity>
    </Animated.View>
  );
};

export const ProductImageSlider = props => {
  console.log;
  const {
    gallery,
    navigation,
    productId,
    isWishlist,
    runWishList,
    isLogin,
    openImageModal,
  } = props;
  const scrollX = new Animated.Value(0);
  const keyExt = (item, index) => `${String(item.id)} Images`;
  const RenderSlider = () =>
    useMemo(() => {
      return (
        <>
          {isLogin ? (
            <HeartButton
              id={productId}
              runWishList={(id, statusWhislist) =>
                runWishList(id, statusWhislist)
              }
              isWishlist={isWishlist}
            />
          ) : null}
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            initialNumToRender={3}
            legacyImplementation
            disableVirtualization
            decelerationRate="fast"
            data={gallery}
            extraData={gallery}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            pagingEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            initialScrollIndex={0}
            renderItem={({item, index}) => {
              return <CardImage item={item} />;
            }}
          />
          <View
            style={{
              position: 'relative',
              zIndex: 10,
              bottom: 50,
              right: 0,
              left: 0,
              width: '100%',
              flexDirection: 'row',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {gallery.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {gallery.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: 7,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 7 / 2,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 9,
                          width: 9,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 9 / 2,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
            <View
              style={{
                flex: 0.3,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  openImageModal();
                }}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: white,
                  paddingLeft: 10,
                  paddingRight: 10,
                  height: height / 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  View All
                </Text>
              </Button>
            </View>
          </View>
        </>
      );
    }, [gallery]);
  if (gallery) {
    return RenderSlider();
  } else {
    return null;
  }
};

export const CricleButtonBack = props => {
  const {navigation} = props;

  return (
    <Button
      onPress={() => {
        navigation.goBack(null);
      }}
      style={{
        alignSelf: 'flex-end',
        paddingTop: 0,
        paddingBottom: 0,
        height: 35,
        width: 35,
        justifyContent: 'center',
        borderRadius: 35 / 2,
        backgroundColor: white,
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 15,
        left: 15,
        zIndex: 999,
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{marginLeft: 0, marginRight: 0, fontSize: 25, color: black}}
      />
    </Button>
  );
};

export const Headers = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        zIndex: 99,
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Florist
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(FloristDetail);

export default props => <Wrapper {...props} />;
