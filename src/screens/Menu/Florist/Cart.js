import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  FlatList,
  ActivityIndicator,
  Image,
  StatusBar,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Footer,
  FooterTab,
} from 'native-base';
import moment from 'moment';
import {charImage} from '../../../utils/Themes/Images';
import AsyncImage from '../../../components/Image/AsyncImage';

const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  overlayDim,
} = Colors;
const {regular, small} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const {charEmptyFlorist, charDelivery, charSelfPickup} = charImage;
import MapView from '../../../components/Map/index';

import Loader from '../../../components/Loader/JustLoader';

// Query
import QueryGetCart from '../../../graphql/queries/getCartFlorist';

// Mutation
import MutationRemoveItemCart from '../../../graphql/mutations/removeCartFlorist';

export const Headers = props => {
  const {goBack, title} = props;

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          transparent
          style={{
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Cart = props => {
  console.log('Cart Props: ', props);
  const {navigation, client, positionYBottomNav} = props;

  const [orderId, setOrderId] = useState(null);
  const [listItems, setListItems] = useState([]);
  const [subTotal, setSubtotal] = useState(0);
  const [taxes, setTaxes] = useState(0);
  const [totalPrices, setTotalPrices] = useState(0);
  const [deliveryFee, setDeliveryFee] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [addressOption, setAddressOptions] = useState(null);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [showModalRemoveItem, setShowModalRemoveItem] = useState(false);
  const [isSuccessRemove, setIsSuccessRemove] = useState(false);
  const [isErrorRemove, setIsErrorRemove] = useState(false);
  const [isLoadingRemove, setIsLoadingRemove] = useState(false);

  const [selectedRemoveItemIndex, setSelectedRemoveItemIndex] = useState(0);

  const [emptyAddressSelected, setEmptyAddressSelected] = useState(false);

  const [showModalToUpdateDate, setShowModalToUpdateDate] = useState(false);

  useEffect(() => {
    StatusBar.setTranslucent(false);
    fetch();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(false);
      fetch();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subscriber;
  }, [navigation]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const fetch = () => {
    try {
      client
        .query({
          query: QueryGetCart,
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response Get Cart Florist: ', response);
          const {data, errors} = response;
          const {getCartDetails} = data;
          const {data: list, error, totalCount} = getCartDetails;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            // Modal Remove handler
            await setIsLoadingRemove(false);
            await setIsErrorRemove(true);
            await setTimeout(async () => {
              await setShowModalRemoveItem(false);
              await setIsLoadingRemove(false);
              await setIsSuccessRemove(false);
              await setIsErrorRemove(false);
            }, 500);
          } else {
            if (error) {
              await setIsError(false);
              await setIsLoading(false);
              // Modal Remove handler
              await setIsLoadingRemove(false);
              await setIsErrorRemove(true);
              await setTimeout(async () => {
                await setShowModalRemoveItem(false);
                await setIsLoadingRemove(false);
                await setIsSuccessRemove(false);
                await setIsErrorRemove(false);
              }, 500);
            } else {
              const {
                items,
                subtotal: apiSubTotal,
                tax,
                total,
                deliveryFee,
                deliveryOption,
                orderId: id,
              } = list;
              console.log('JANUCCCCC: ', deliveryOption);

              await setAddressOptions({...deliveryOption});
              await setOrderId(id);
              await setListItems([...items]);
              await setSubtotal(apiSubTotal);
              await setTaxes(tax);
              await setDeliveryFee(deliveryFee);
              await setTotalPrices(total);
              await setTotalData(totalCount);
              await setIsError(false);
              await setIsLoading(false);

              // Modal Remove handler
              await setIsLoadingRemove(false);
              await setIsSuccessRemove(true);
              await setTimeout(async () => {
                await setShowModalRemoveItem(false);
                await setIsLoadingRemove(false);
                await setIsSuccessRemove(false);
                await setIsErrorRemove(false);
              }, 500);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          setIsError(false);
          setIsLoading(false);

          // Modal Remove handler
          await setIsLoadingRemove(false);
          await setIsErrorRemove(true);
          await setTimeout(async () => {
            await setShowModalRemoveItem(false);
            await setIsLoadingRemove(false);
            await setIsSuccessRemove(false);
            await setIsErrorRemove(false);
          }, 500);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(false);
      setIsLoading(false);
      // Modal Remove handler
      setIsLoadingRemove(false);
      setIsErrorRemove(true);
      setTimeout(async () => {
        await setShowModalRemoveItem(false);
        await setIsLoadingRemove(false);
        await setIsSuccessRemove(false);
        await setIsErrorRemove(false);
      }, 500);
    }
  };

  const removeItemCart = async index => {
    try {
      await setShowModalRemoveItem(true);
      await setSelectedRemoveItemIndex(index);
    } catch (error) {
      await setShowModalRemoveItem(false);
      await setIsLoadingRemove(false);
      await setIsSuccessRemove(false);
      await setIsErrorRemove(false);
    }
  };

  const removeItem = async () => {
    try {
      await setIsLoadingRemove(true);
      client
        .mutate({
          mutation: MutationRemoveItemCart,
          variables: {
            cartId: parseInt(listItems[selectedRemoveItemIndex].id, 10),
          },
        })
        .then(async response => {
          console.log('Response Remove Item Cart: ', response);
          const {data, errors} = response;
          const {removeFromCart} = data;
          const {error} = removeFromCart;

          if (errors) {
            await setIsLoadingRemove(false);
            await setIsError(true);
            await setTimeout(async () => {
              await setShowModalRemoveItem(false);
              await setIsLoadingRemove(false);
              await setIsSuccessRemove(false);
              await setIsErrorRemove(false);
            }, 500);
          } else {
            if (error) {
              await setIsLoadingRemove(false);
              await setIsError(true);
              await setTimeout(async () => {
                await setShowModalRemoveItem(false);
                await setIsLoadingRemove(false);
                await setIsSuccessRemove(false);
                await setIsErrorRemove(false);
              }, 500);
            } else {
              await setIsLoadingRemove(false);
              await setIsSuccessRemove(true);
              await fetch();
            }
          }
        })
        .catch(async error => {
          console.log('Error Remove Items Cart: ', error);
          await setIsLoadingRemove(false);
          await setIsError(true);
          await setTimeout(async () => {
            await setShowModalRemoveItem(false);
            await setIsLoadingRemove(false);
            await setIsSuccessRemove(false);
            await setIsErrorRemove(false);
          }, 500);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingRemove(false);
      await setIsError(true);
      await setTimeout(async () => {
        await setShowModalRemoveItem(false);
        await setIsLoadingRemove(false);
        await setIsSuccessRemove(false);
        await setIsErrorRemove(false);
      }, 500);
    }
  };

  const cancelRemove = async () => {
    try {
      await setShowModalRemoveItem(false);
      await setIsLoadingRemove(false);
      await setIsSuccessRemove(false);
      await setIsErrorRemove(false);
    } catch (error) {
      await setShowModalRemoveItem(false);
      await setIsLoadingRemove(false);
      await setIsSuccessRemove(false);
      await setIsErrorRemove(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToDeliveryOptions = () => {
    try {
      navigation.navigate('FloristDeliveryOptions');
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Headers title="Cart" goBack={() => goBack()} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              bottom: 15,
              width: width / 4,
              height: height / 9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Loader />
          </View>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers title="Cart" goBack={() => goBack()} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            style={{
              backgroundColor: mainGreen,
              height: height / 16,
              borderRadius: 4,
              elevation: 0,
              shadowOpacity: 0,
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              Refresh
            </Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <Headers title="Cart" goBack={() => goBack()} />
        {showModalToUpdateDate ? (
          <View
            style={{
              position: 'absolute',
              zIndex: 99,
              left: 0,
              top: 0,
              right: 0,
              bottom: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 15,
            }}>
            <View
              style={{
                padding: 10,
                minHeight: height / 8,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 15,
                }}>
                <Text
                  style={{
                    // marginVertical: 10,
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    textAlign: 'center',
                    letterSpacing: 0.3,
                    lineHeight: 18,
                    color: mainRed,
                  }}>
                  Please update the date of(Delivery/Pickup)!
                </Text>
              </View>
            </View>
          </View>
        ) : null}
        {emptyAddressSelected ? (
          <View
            style={{
              position: 'absolute',
              zIndex: 99,
              left: 0,
              top: 0,
              right: 0,
              bottom: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 10,
                height: height / 8,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
              }}>
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: RFPercentage(3), color: mainRed}}
              />
              <Text
                style={{
                  marginVertical: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  textAlign: 'center',
                  letterSpacing: 0.3,
                  lineHeight: 18,
                  color: mainRed,
                }}>
                Please select delivery option!
              </Text>
            </View>
          </View>
        ) : null}
        {showModalRemoveItem ? (
          <View
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              zIndex: 99,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: overlayDim,
            }}>
            {showModalRemoveItem &&
            !isLoadingRemove &&
            !isSuccessRemove &&
            !isErrorRemove ? (
              <View
                style={{
                  width: width / 1.5,
                  height: height / 4.5,
                  backgroundColor: white,
                  borderRadius: 4,
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 15,
                    top: 20,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                      lineHeight: 18,
                      textAlign: 'center',
                    }}>
                    Are you sure want to remove this item from your cart ?
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    position: 'absolute',
                    bottom: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: height / 16,
                      padding: 10,
                    }}>
                    <TouchableOpacity
                      onPress={() => cancelRemove()}
                      style={{
                        backgroundColor: mainRed,
                        borderRadius: 4,
                        width: '100%',
                        height: height / 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.8),
                          color: white,
                          letterSpacing: 0.3,
                        }}>
                        Cancel
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: height / 16,
                      padding: 10,
                    }}>
                    <TouchableOpacity
                      onPress={() => removeItem()}
                      style={{
                        backgroundColor: white,
                        borderRadius: 4,
                        borderWidth: 1,
                        borderColor: mainGreen,
                        width: '100%',
                        height: height / 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.8),
                          color: mainGreen,
                          letterSpacing: 0.3,
                        }}>
                        Remove
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : isLoadingRemove ? (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator size="large" style={{color: mainGreen}} />
              </View>
            ) : isSuccessRemove ? (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="check"
                  style={{color: 'green', fontSize: 35}}
                />
              </View>
            ) : (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{color: mainRed, fontSize: 35}}
                />
              </View>
            )}
          </View>
        ) : null}
        {!isLoading && !isError && listItems.length === 0 ? (
          <View
            style={{
              padding: 15,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={charEmptyFlorist}
              style={{bottom: 90, width, height: height / 3}}
              resizeMode="contain"
            />
            <Text
              style={{
                bottom: 75,
                fontFamily: medium,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2.5),
              }}>
              Your cart is empty
            </Text>
            <Text
              style={{
                lineHeight: 20,
                bottom: 60,
                fontFamily: book,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2),
              }}>
              Please put some of our florist products into your cart
            </Text>
          </View>
        ) : (
          <>
            {addressOption?.deliveryAddress?.address ? (
              <>
                <Card
                  style={{
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    elevation: 0,
                    shadowOpacity: 0,
                    borderBottomColor: headerBorderBottom,
                    borderBottomWidth: 1,
                  }}>
                  <CardItem
                    style={{
                      paddingBottom: 0,
                      width: '100%',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(1.7),
                        letterSpacing: 0.3,
                      }}>
                      {addressOption?.deliveryType &&
                        addressOption.deliveryType.toUpperCase()}{' '}
                      AT
                    </Text>
                    <View>
                      <Button
                        onPress={() => goToDeliveryOptions()}
                        transparent
                        style={{
                          alignSelf: 'flex-end',
                          paddingTop: 0,
                          paddingBottom: 0,
                          height: 35,
                          width: 35,
                          justifyContent: 'center',
                        }}>
                        <Icon
                          type="Feather"
                          name="edit"
                          style={{
                            marginLeft: 0,
                            marginRight: 0,
                            fontSize: RFPercentage(2.5),
                            color: mainGreen,
                          }}
                        />
                      </Button>
                    </View>
                  </CardItem>
                  <CardItem style={{width: '100%'}}>
                    <MapView
                      width={70}
                      height={70}
                      forMapAddress={addressOption?.deliveryAddress?.address}
                    />
                    <View
                      style={{
                        flexBasis: '75%',
                        height: '100%',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                          bottom: 8,
                        }}>
                        <Image
                          source={
                            addressOption?.deliveryType === 'Delivery'
                              ? charDelivery
                              : charSelfPickup
                          }
                          style={{
                            right: 5,
                            width: width / 13,
                            height: height / 28,
                          }}
                          resizeMode="contain"
                        />
                        <Text
                          style={{
                            marginLeft: 5,
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: mainRed,
                            letterSpacing: 0.3,
                          }}>
                          {addressOption?.deliveryType}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: '100%',
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.7),
                            lineHeight: 18,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {addressOption?.deliveryAddress?.address}
                          {addressOption?.deliveryAddressDetail
                            ? addressOption?.deliveryAddressDetail
                                ?.streetAndBlock ||
                              addressOption?.deliveryAddressDetail
                                ?.streetAndBlock !== ''
                              ? `, Block ${addressOption?.deliveryAddressDetail?.streetAndBlock}`
                              : null
                            : null}
                          {addressOption?.deliveryAddressDetail
                            ? addressOption?.deliveryAddressDetail
                                ?.floorAndAppartment ||
                              addressOption?.deliveryAddressDetail
                                ?.floorAndAppartment !== ''
                              ? `, Unit ${addressOption?.deliveryAddressDetail?.floorAndAppartment}`
                              : null
                            : null}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderTopColor: headerBorderBottom,
                      borderTopWidth: 1,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(1.7),
                        letterSpacing: 0.3,
                      }}>
                      {addressOption?.deliveryType &&
                        addressOption.deliveryType.toUpperCase()}{' '}
                      DATE & TIME
                    </Text>
                  </CardItem>
                  <CardItem style={{paddingTop: 0}}>
                    <View style={{flex: 0.1}}>
                      <Icon
                        type="Feather"
                        name="calendar"
                        style={{fontSize: RFPercentage(2.5), color: black}}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          color: black,
                          fontSize: RFPercentage(1.7),
                          letterSpacing: 0.3,
                        }}>{`${
                        addressOption?.deliveryDate
                          ? moment(addressOption.deliveryDate)
                              .utc()
                              .local()
                              .format('dd, MMM DD YYYY')
                          : 'N/A'
                      }, ${
                        addressOption?.timeslotDetail?.timeslot
                          ? addressOption.timeslotDetail.timeslot
                          : 'N/A'
                      }`}</Text>
                    </View>
                  </CardItem>
                </Card>
              </>
            ) : (
              <CardSelectAddress onPress={() => goToDeliveryOptions()} />
            )}
            <Content>
              <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                <CardItem style={{width: '100%', paddingBottom: 0}}>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(1.7),
                        letterSpacing: 0.3,
                      }}>
                      Your Order
                    </Text>
                  </View>
                  <View style={{flex: 0.4}}>
                    <TouchableOpacity
                      onPress={() => {
                        navigation.push('Florist');
                      }}
                      style={{
                        elevation: 0,
                        shadowOpacity: 0,
                        height: height / 25,
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          color: mainRed,
                          fontSize: RFPercentage(1.6),
                          letterSpacing: 0.3,
                        }}>
                        Add Items
                      </Text>
                    </TouchableOpacity>
                  </View>
                </CardItem>
                <FlatList
                  disableVirtualization
                  scrollEnabled={false}
                  data={listItems}
                  extraData={listItems}
                  keyExtractor={(item, index) => `${item.id}`}
                  renderItem={({item, index}) => {
                    const {product, upsize, itemPrice, qty, deliveryFee} = item;
                    const {
                      featuredImageDynamicURL,
                      featuredImageURL,
                      discount,
                      floristPriceAfterDiscount,
                    } = product;

                    const source = featuredImageDynamicURL
                      ? {uri: `${featuredImageDynamicURL}=h500`}
                      : {uri: `${featuredImageURL}`};
                    return (
                      <CardItem style={{width: '100%'}}>
                        <View style={{width: '100%'}}>
                          <View style={{width: '100%', flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 0.2,
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                                paddingTop: 3,
                              }}>
                              <View
                                style={{
                                  width: 25,
                                  height: 25,
                                  backgroundColor: '#E8E8E8',
                                  borderRadius: 25 / 2,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    fontSize: RFPercentage(1.4),
                                    color: black,
                                    letterSpacing: 0.3,
                                  }}>
                                  {index + 1}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: 25,
                                  height: 25,
                                  backgroundColor: 'transparent',
                                  borderRadius: 25 / 2,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}
                              />
                            </View>
                            <View style={{flex: 0.5}}>
                              <View style={{width: '100%', height: height / 8}}>
                                <AsyncImage
                                  source={source}
                                  style={{
                                    flex: 1,
                                    height: '100%',
                                    width: '100%',
                                  }}
                                  resizeMode={'cover'}
                                  placeholderColor="white"
                                  loaderStyle={{
                                    width: width / 7,
                                    height: height / 7,
                                  }}
                                />
                              </View>
                            </View>
                            <View
                              style={{
                                flex: 0.5,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                                paddingTop: 3,
                                paddingLeft: 10,
                              }}>
                              <View style={upsize ? {marginBottom: 10} : {}}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {product?.name ? product.name : 'N/A'}
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: book,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {upsize
                                    ? `Upsize: +${upsize.size} Inches`
                                    : null}
                                </Text>
                              </View>
                              {discount === 0 ||
                              discount === null ||
                              discount === undefined ? null : (
                                <View>
                                  <Text
                                    style={{
                                      fontStyle: 'italic',
                                      fontFamily: book,
                                      color: greyLine,
                                      fontSize: RFPercentage(1.6),
                                      letterSpacing: 0.3,
                                      lineHeight: 18,
                                    }}>
                                    Discount {discount}%
                                  </Text>
                                </View>
                              )}
                            </View>
                            <View
                              style={{
                                flex: 0.2,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingTop: 3,
                              }}>
                              <View style={{marginBottom: 10}}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {qty}
                                </Text>
                              </View>
                              <View />
                            </View>
                            <View
                              style={{
                                flex: 0.4,
                                justifyContent: 'space-between',
                                alignItems: 'flex-end',
                              }}>
                              <View>
                                <Button
                                  onPress={() => removeItemCart(index)}
                                  transparent
                                  style={{
                                    bottom: 0.9,
                                    marginTop: 0,
                                    alignSelf: 'flex-end',
                                    paddingTop: 0,
                                    paddingBottom: 0,
                                    height: 25,
                                    width: 25,
                                    borderRadius: 25 / 2,
                                    justifyContent: 'center',
                                  }}>
                                  <Icon
                                    type="Feather"
                                    name="x"
                                    style={{
                                      color: '#999999',
                                      marginLeft: 0,
                                      marginRight: 0,
                                      fontSize: 15,
                                    }}
                                  />
                                </Button>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {floristPriceAfterDiscount !== 0 ||
                                  floristPriceAfterDiscount !== null ||
                                  floristPriceAfterDiscount !== undefined
                                    ? `$${floristPriceAfterDiscount.toFixed(2)}`
                                    : 'N/A'}
                                </Text>
                                {discount === 0 ||
                                discount === null ||
                                discount === undefined ? null : (
                                  <Text
                                    style={{
                                      textDecorationLine: 'line-through',
                                      fontFamily: medium,
                                      color: greyLine,
                                      fontSize: RFPercentage(1.6),
                                      letterSpacing: 0.3,
                                      lineHeight: 18,
                                    }}>
                                    {itemPrice
                                      ? `$${itemPrice.toFixed(2)}`
                                      : 'N/A'}
                                  </Text>
                                )}
                              </View>
                            </View>
                          </View>
                          <View
                            style={{
                              borderBottomWidth: 1,
                              borderBottomColor: headerBorderBottom,
                              width: '100%',
                              height: 0.5,
                              marginVertical: 5,
                              marginTop: 15,
                            }}
                          />
                        </View>
                      </CardItem>
                    );
                  }}
                />
              </Card>
              <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                <SpacedCardContent
                  title={'Subtotal'}
                  value={`$${subTotal.toFixed(2)}`}
                />
                <SpacedCardContent
                  title={'Taxes'}
                  value={`$${taxes.toFixed(2)}`}
                />
                {addressOption?.deliveryType === 'Delivery' ? (
                  <SpacedCardContent
                    title={'Delivery Fee'}
                    value={`$${deliveryFee.toFixed(2)}`}
                  />
                ) : null}
              </Card>
            </Content>
            <Footer>
              <FooterTab>
                <Card
                  style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    marginTop: 0,
                    marginBottom: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    elevation: 0,
                    shadowOpacity: 0,
                    borderTopColor: headerBorderBottom,
                    borderTopWidth: 1,
                  }}>
                  <SpacedCardContent
                    forBottom={true}
                    title={'Total'}
                    value={`$${totalPrices.toFixed(2)}`}
                  />
                </Card>
              </FooterTab>
            </Footer>
            <ButtonAddToCart
              onPress={async () => {
                try {
                  const currentDate = moment().format('YYYY-MM-DD');
                  console.log('Address >>> ', addressOption);
                  const selectedDate = moment(
                    addressOption?.deliveryDate,
                  ).format('YYYY-MM-DD');
                  console.log('Selected Date: ', selectedDate);
                  const isBefore = moment(selectedDate).isBefore(currentDate);
                  console.log('IS BEFORE', isBefore);

                  if (isBefore) {
                    await setShowModalToUpdateDate(true);

                    await setTimeout(async () => {
                      await setShowModalToUpdateDate(false);
                    }, 2000);
                  } else {
                    if (addressOption?.deliveryAddress) {
                      console.log('ADDRESS OPTION JANCOEG: ', addressOption);
                      navigation.navigate('SelectPaymentMethod', {
                        paymentType: 'Florist',
                        amount: parseFloat(totalPrices),
                        description: addressOption?.deliveryAddress?.notes
                          ? addressOption.deliveryAddress.notes
                          : null,
                        orderId: orderId,
                        from: props?.route?.params?.from,
                      });
                    } else {
                      // not select delivery option
                      await setEmptyAddressSelected(true);

                      await setTimeout(async () => {
                        await setEmptyAddressSelected(false);
                      }, 1000);
                    }
                  }
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              title="SELECT PAYMENT METHOD"
              disabled={addressOption?.deliveryAddress?.address ? false : true}
            />
          </>
        )}
      </Container>
    );
  }
};

export const SpacedCardContent = props => {
  const {title, value, forBottom} = props;

  return (
    <CardItem style={{width: '100%', paddingTop: forBottom ? 20 : 0}}>
      <View
        style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.7),
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.7),
            letterSpacing: 0.3,
          }}>
          {value}
        </Text>
      </View>
    </CardItem>
  );
};

export const ButtonAddToCart = props => {
  const {onPress, title, disabled} = props;
  return (
    <Footer>
      <FooterTab
        style={{backgroundColor: disabled ? headerBorderBottom : mainGreen}}>
        <View
          style={{
            backgroundColor: disabled ? headerBorderBottom : mainGreen,
            width: '100%',
          }}>
          <TouchableOpacity
            disabled={disabled}
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: disabled ? greyLine : white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const CardSelectAddress = props => {
  const {onPress} = props;

  return (
    <Card
      style={{
        marginLeft: 0,
        marginRight: 0,
        paddingLeft: 0,
        paddingRight: 0,
        marginTop: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        borderTopWidth: 0,
      }}>
      <CardItem
        style={{
          width: '100%',
          marginLeft: 0,
          marginRight: 0,
          paddingLeft: 0,
          paddingRight: 0,
          paddingTop: 0,
          paddingBottom: 0,
        }}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            backgroundColor: '#FFEDED',
          }}>
          <View
            style={{
              flex: 1,
              paddingTop: 15,
              paddingBottom: 15,
              paddingLeft: 15,
              paddingRight: 15,
              justifyContent: 'space-between',
              alignItems: 'flex-start',
            }}>
            <View>
              <Text
                style={{
                  fontFamily: medium,
                  color: '#999999',
                  fontSize: RFPercentage(1.8),
                  letterSpacing: 0.3,
                  lineHeight: 18,
                }}>
                DELIVERY OPTIONS
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                  letterSpacing: 0.3,
                  lineHeight: 18,
                }}>
                Select Method
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => onPress()}
              style={{
                left: 40,
                width: 80,
                height: 80,
                borderRadius: 80 / 2,
                backgroundColor: '#FAC4C7',
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingLeft: 8,
              }}>
              <Icon
                type="Feather"
                name="chevron-right"
                style={{fontSize: 25, color: '#999999'}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(Cart);

export default props => <Wrapper {...props} />;
