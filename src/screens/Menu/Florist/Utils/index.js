import {Platform, Dimensions} from 'react-native';
import {hasNotch} from 'react-native-device-info';

const {height} = Dimensions.get('window');

export const HEADER_IMAGE_HEIGHT = height / 3;

export const HEADER_DEFAULT_HEIGHT =
  Platform.OS === 'ios'
    ? hasNotch()
      ? height / 12.5
      : height / 10
    : hasNotch()
    ? height / 7.8
    : height / 8;

export const MINIMUM_TABY_HEIGHT =
  Platform.OS === 'ios'
    ? hasNotch()
      ? height / 5
      : height / 4.8
    : hasNotch()
    ? height / 4.8
    : height / 4.7;
