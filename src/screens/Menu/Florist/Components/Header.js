import React from 'react';
import {Text, Image, Platform, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../../utils/Themes/Images';

// Redux
import {connect} from 'react-redux';
import FilterPriceRedux from '../../../../redux/thunk/FloristPriceFilterThunk';
import FilterSizeRedux from '../../../../redux/thunk/FloristSizeFilterThunk';
import FilterCategoriesRedux from '../../../../redux/thunk/FloristCategoriesFilterThunk';
import FilterTypeRedux from '../../../../redux/thunk/FloristTypeFilterThunk';
import FilterColorRedux from '../../../../redux/thunk/FloristColorFilterThunk';
import FilterStatusRedux from '../../../../redux/thunk/FloristFilterStatusThunk';
import FilterThemeRedux from '../../../../redux/thunk/FloristThemeFilterThunk';

const {
  charFilterIcon,
  charFilterIconRed,
  charIconReview,
  charIconAbout,
} = charImage;
const {white, black, headerBorderBottom, mainRed} = Colors;
const {medium} = FontType;

import {CommonActions} from '@react-navigation/native';

const Headers = props => {
  console.log('HEADER ACTIVE TAB: ', props);
  const {
    status,
    activeTab,
    detail,
    navigation,
    filterPrice,
    filterSize,
    filterType,
    filterColor,
    filterStatus,
    filterCategories,
    filterTheme,
    badge,
  } = props;

  const onPressRightSide = () => {
    try {
      if (activeTab === 'Home') {
        goToAbout();
      } else if (activeTab === 'Products') {
        goToCart();
      } else {
        goToHome();
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onPressLeftSide = () => {
    try {
      if (activeTab === 'Home') {
        goToReview();
      } else if (activeTab === 'Products') {
        goToFilter();
      } else {
        // do nothing
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToCart = () => {
    try {
      navigation.navigate('FloristCart', {id: detail[0]?.id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToHome = async () => {
    try {
      await filterStatus(false);
      await filterPrice([0, 0]);
      await filterSize([0.0, 0.0]);
      await filterType([]);
      await filterColor([]);
      await filterCategories([], []);
      await filterTheme([]);
      await navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToReview = () => {
    try {
      navigation.navigate('ReviewList', {id: detail[0]?.id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToFilter = () => {
    try {
      navigation.navigate('FloristFilter', {
        id: detail[0]?.id,
        serviceType: detail[0]?.serviceType,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToAbout = () => {
    try {
      navigation.navigate('FloristMerchantAbout', {id: detail[0]?.id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goBack = async () => {
    try {
      await filterStatus(false);
      await filterPrice([0, 0]);
      await filterSize([0.0, 0.0]);
      await filterType([]);
      await filterColor([]);
      await filterCategories([], []);
      await filterTheme([]);
      await navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={
        activeTab === 'Home'
          ? {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
            }
          : {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
              marginBottom: 10,
            }
      }>
      <Left style={{flex: 0.4, paddingLeft: 5}}>
        <Button
          onPress={goBack}
          style={{
            borderRadius: 35 / 2,
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={
              Platform.OS === 'ios'
                ? {marginLeft: 0, marginRight: 0, fontSize: 24, color: black}
                : {
                    right: 4,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: black,
                  }
            }
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {activeTab !== 'Home' ? (
          <>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              {activeTab}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                marginVertical: 5,
                fontSize: RFPercentage(1.5),
                color: 'grey',
                letterSpacing: 0.3,
              }}>
              {detail[0]?.name}
            </Text>
          </>
        ) : null}
      </Body>
      <Right
        style={{
          flex: 0.4,
          justifyContent:
            activeTab === 'Home' || activeTab === 'Products'
              ? 'space-between'
              : 'flex-end',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        {activeTab === 'Home' ? (
          <Button
            onPress={onPressLeftSide}
            style={{
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity:
                activeTab === 'Home'
                  ? Platform.OS === 'android'
                    ? 1
                    : 0.2
                  : 0,
              shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowRadius: 1.41,
            }}>
            <Image
              source={charIconReview}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : activeTab === 'Products' ? (
          <Button
            onPress={onPressLeftSide}
            style={{
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity: activeTab === 'Home' ? 1 : 0,
            }}>
            <Image
              source={status ? charFilterIconRed : charFilterIcon}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : null}
        <Button
          onPress={onPressRightSide}
          style={{
            borderRadius: 35 / 2,
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          {activeTab === 'Home' ? (
            <Image
              source={charIconAbout}
              style={{bottom: 2, width: 25, height: 25}}
              resizeMode="contain"
            />
          ) : activeTab === 'Products' ? (
            <View>
              {badge === 0 ? null : (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: white,
                    borderRadius: 10 / 2,
                    zIndex: 2,
                    width: 10,
                    height: 10,
                    backgroundColor: mainRed,
                    position: 'absolute',
                    top: 0,
                    right: 0,
                  }}
                />
              )}
              <Icon
                type="Feather"
                name="shopping-bag"
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  fontSize: 24,
                  color: black,
                }}
              />
            </View>
          ) : (
            <Icon
              type="Feather"
              name="home"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          )}
        </Button>
      </Right>
    </Header>
  );
};

const mapToState = state => {
  const {florist} = state;
  return {
    ...florist,
  };
};

const mapToDispatch = dispatch => {
  return {
    filterPrice: price => dispatch(FilterPriceRedux(price)),
    filterSize: size => dispatch(FilterSizeRedux(size)),
    filterCategories: (categories, selectedCategories) =>
      dispatch(FilterCategoriesRedux(categories, selectedCategories)),
    filterType: type => dispatch(FilterTypeRedux(type)),
    filterColor: color => dispatch(FilterColorRedux(color)),
    filterStatus: status => dispatch(FilterStatusRedux(status)),
    filterTheme: themes => dispatch(FilterThemeRedux(themes)),
  };
};

const connector = connect(
  mapToState,
  mapToDispatch,
)(Headers);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
