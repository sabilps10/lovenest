import React from 'react';
import {Dimensions, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {hasNotch} from 'react-native-device-info';

const {width, height} = Dimensions.get('window');

const ImageCards = props => {
  const {source} = props;

  return (
    <View
      style={{
        width,
        height: width + 180,
        // Platform.OS === 'android'
        //   ? hasNotch()
        //     ? height / 1.55
        //     : height / 1.5
        //   : hasNotch()
        //   ? height / 1.55
        //   : height / 1.3,
        // marginVertical: 5,
      }}>
      <AsyncImage
        source={source}
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          width,
          zIndex: 0,
        }}
        resizeMode="cover"
        placeholderColor={'white'}
        loaderStyle={{
          width: width / 7,
          height: height / 7,
        }}
      />
    </View>
  );
};

const Wrapper = compose(withApollo)(ImageCards);

export default props => <Wrapper {...props} />;
