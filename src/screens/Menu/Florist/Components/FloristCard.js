import React, {useRef} from 'react';
import {
  TouchableOpacity,
  Text,
  Dimensions,
  View,
  Animated,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Icon} from 'native-base';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {hasNotch} from 'react-native-device-info';

const AnimatedCard = new Animated.createAnimatedComponent(Card);
const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);
const {white, black, mainRed, headerBorderBottom, greyLine} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

export const AnimatedFloristCard = props => {
  const {item, index, runWishList, isLogin} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    return (
      <Animated.View
        style={{
          flex: 1 / 2,
          padding: 5,
          paddingBottom: 15,
        }}>
        <AnimatedTouch
          onPress={() => {
            try {
              props?.navigation.push('FloristDetail', {id: item?.id});
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          activeOpacity={1}
          onPressIn={pressIn}
          onPressOut={pressOut}>
          <FloristCard
            isLogin={isLogin}
            scale={scale}
            runWishList={(id, isWishlist) => runWishList(id, isWishlist)}
            item={item}
            index={index}
            {...props}
          />
        </AnimatedTouch>
      </Animated.View>
    );
  } else {
    return null;
  }
};

export const FloristCard = props => {
  const {item, scale, isLogin, runWishList} = props;

  if (item) {
    const {
      id,
      featuredImageURL,
      featuredImageDynamicURL,
      name,
      floristPriceAfterDiscount,
      floristPrice: price,
      soldQty,
      discount,
      isFreeDelivery,
      isWishlist,
    } = item;
    const source = !featuredImageDynamicURL
      ? {uri: featuredImageURL}
      : {uri: `${featuredImageDynamicURL}=h500`};

    return (
      <AnimatedCard
        style={{
          shadowOpacity: 0,
          elevation: 0,
          borderColor: greyLine,
          borderWidth: 1,
          borderRadius: 2,
          transform: [{scale}],
        }}>
        {isLogin ? (
          <HeartButton
            id={id}
            runWishList={(productId, statusWhislist) =>
              runWishList(productId, statusWhislist)
            }
            isWishlist={isWishlist}
          />
        ) : null}
        {discount && discount !== 0.0 ? (
          <View
            style={{
              borderTopLeftRadius: 0,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              zIndex: 99,
              position: 'absolute',
              left: 0,
              top: 0,
              backgroundColor: '#CF615D',
              padding: 10,
              paddingTop: 5,
              paddingBottom: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: white,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              % {discount}
            </Text>
          </View>
        ) : null}
        <CardItem cardBody style={{backgroundColor: 'transparent'}}>
          <AsyncImage
            source={source}
            placeholderColor={'white'}
            resizeMode="cover"
            style={{
              flex: 1,
              width: '100%',
              height: height / 4.33,
            }}
            loaderStyle={{
              width: width / 7,
              height: height / 7,
            }}
          />
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            paddingLeft: 8,
            paddingRight: 8,
            paddingBottom: 5,
            backgroundColor: 'transparent',
          }}>
          <View style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  textAlign: 'left',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 18,
                }}
                numberOfLines={1}>
                {name}
              </Text>
            </View>
          </View>
        </CardItem>
        <CardItem
          style={{
            paddingTop: 0,
            paddingLeft: 10,
            paddingRight: 10,
            backgroundColor: 'transparent',
          }}>
          <Text
            numberOfLines={1}
            style={
              floristPriceAfterDiscount && floristPriceAfterDiscount !== price
                ? {
                    textAlign: 'right',
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    color: greyLine,
                    letterSpacing: 0.3,
                    textDecorationLine: 'line-through',
                  }
                : {
                    textAlign: 'right',
                    fontFamily: medium,
                    fontSize: RFPercentage(2.2),
                    color: mainRed,
                    letterSpacing: 0.3,
                  }
            }>
            {price ? `$${price.toFixed(2)}` : 'N/A'}
          </Text>
          {floristPriceAfterDiscount && floristPriceAfterDiscount !== price ? (
            <Text
              numberOfLines={1}
              style={{
                marginLeft: 5,
                textAlign: 'right',
                fontFamily: medium,
                fontSize: RFPercentage(2.2),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              {floristPriceAfterDiscount
                ? `$${floristPriceAfterDiscount.toFixed(2)}`
                : 'N/A'}
            </Text>
          ) : null}
        </CardItem>
        <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
          <Icon
            type="Feather"
            name="truck"
            style={{
              right: 5,
              fontSize: RFPercentage(1.8),
              color: isFreeDelivery ? mainRed : 'white',
            }}
          />
          <Text
            style={{
              right: 18,
              fontFamily: book,
              fontSize: RFPercentage(1.2),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            {isFreeDelivery ? 'FREE DELIVERY' : ''}
          </Text>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            paddingTop: 5,
            paddingLeft: 10,
            paddingRight: 9,
            paddingBottom: 5,
            backgroundColor: 'transparent',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: greyLine,
              letterSpacing: 0.3,
              bottom: 5,
            }}>
            {soldQty > 0 ? `Sold ${soldQty}` : ''}
          </Text>
          <MiniAddToCart {...props} />
        </CardItem>
      </AnimatedCard>
    );
  } else {
    return null;
  }
};

export const MiniAddToCart = props => {
  const {item, addToCartCustomize} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <AnimatedTouch
      activeOpacity={1}
      onPress={() => {
        addToCartCustomize(item);
      }}
      onPressIn={pressIn}
      onPressOut={pressOut}
      transparent
      style={{position: 'absolute', top: 0, right: 10, zIndex: 99}}>
      <Animated.View
        style={{
          zIndex: 99,
          bottom: 0,
          right: -11,
          width: width / 10,
          height: hasNotch() ? height / 21 : height / 19,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FACFC7',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          borderRadius: 4,
          transform: [{scale: scale}],
        }}>
        <Icon
          type="Feather"
          name="shopping-bag"
          style={{
            left:
              Platform.OS === 'ios'
                ? hasNotch()
                  ? 7
                  : 8.8
                : hasNotch()
                ? 7
                : 7,
            top: hasNotch() ? 2 : 1.5,
            fontSize: RFPercentage(2.5),
            color: black,
          }}
        />
      </Animated.View>
    </AnimatedTouch>
  );
};

export const HeartButton = props => {
  const {isWishlist, id, runWishList} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 10,
        right: 10,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          runWishList(id, isWishlist);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 30,
          width: 30,
          justifyContent: 'center',
          borderRadius: 30 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        {isWishlist ? (
          <Icon
            type="MaterialCommunityIcons"
            name="heart"
            style={{
              top: 1,
              left: 0.2,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        ) : (
          <Icon
            type="Feather"
            name="heart"
            style={{
              top: 1,
              left: 0.2,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        )}
      </TouchableOpacity>
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(AnimatedFloristCard);

export default props => <Wrapper {...props} />;
