import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

import ImageCards from './ImageCards';

const ShowImageCardByIndex = props => {
  const {listImageCard, isLoading, isError, showByIndex} = props;

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (listImageCard?.length === 0) {
      return null;
    } else {
      const showedData = listImageCard[showByIndex]
        ? listImageCard[showByIndex]
        : [];
      console.log('YUHU: ', showedData);
      if (showedData?.length === 0) {
        return null;
      } else {
        const {dynamicUrl, url} = showedData;
        const source = dynamicUrl ? {uri: `${dynamicUrl}=s0`} : {uri: url};

        return <ImageCards source={source} />;
      }
    }
  }
};

const Wrapper = compose(withApollo)(ShowImageCardByIndex);

export default props => <Wrapper {...props} />;
