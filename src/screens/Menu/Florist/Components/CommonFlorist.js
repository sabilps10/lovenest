import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import {FontType} from '../../../../utils/Themes/Fonts';
import AnimatedFloristCard from './FloristCard';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {medium} = FontType;

const CommonProducts = props => {
  const {
    isLoading,
    isError,
    list,
    isLogin,
    runWishList,
    navigation,
    isLoadMore,
  } = props;

  const keyExt = item => `${item.id}`;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          height: 200,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>Loading....</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0) {
      return null; // ---> need add image no product
    } else {
      return (
        <FlatList
          legacyImplementation
          disableVirtualization
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          listKey={keyExt}
          numColumns={2}
          contentContainerStyle={{paddingBottom: 25}}
          columnWrapperStyle={{
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: 7,
            paddingRight: 10,
            zIndex: 2,
            paddingBottom: 35,
          }}
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <Card transparent style={{marginTop: 0, marginBottom: 15}}>
                  <CardItem>
                    <View
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          bottom: 15,
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          fontStyle: 'italic',
                          color: 'grey',
                        }}>
                        Loading more...
                      </Text>
                    </View>
                  </CardItem>
                </Card>
              );
            } else {
              return null;
            }
          }}
          renderItem={({item}) => {
            const {
              featuredImageURL,
              featuredImageDynamicURL,
              id,
              name,
              discount,
              floristPriceAfterDiscount,
              isWishlist,
              floristPrice,
            } = item;
            const source = !featuredImageDynamicURL
              ? {uri: featuredImageURL}
              : {uri: `${featuredImageDynamicURL}=h500`};
            return (
              <AnimatedFloristCard
                runWishList={(productId, statusWhislist) =>
                  runWishList(productId, statusWhislist)
                }
                discount={discount}
                floristPriceAfterDiscount={floristPriceAfterDiscount}
                price={floristPrice}
                isLogin={isLogin}
                isWishlist={isWishlist}
                name={name}
                source={source}
                id={id}
                navigation={navigation}
              />
            );
          }}
        />
      );
    }
  }
};

const Wrapper = compose(withApollo)(CommonProducts);

export default props => <Wrapper {...props} />;
