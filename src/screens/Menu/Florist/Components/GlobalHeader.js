import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import HeaderHome from './Header';

const GlobalHeader = props => {
  const {activeTab, badge} = props;

  return <HeaderHome badge={badge} activeTab={activeTab} {...props} />;
};
const Wrapper = compose(withApollo)(GlobalHeader);

export default props => <Wrapper {...props} />;
