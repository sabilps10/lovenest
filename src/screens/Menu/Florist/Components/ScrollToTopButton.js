import React from 'react';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button, Icon} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {hasNotch} from 'react-native-device-info';

const {white, mainRed} = Colors;

const ScrollToTopButton = props => {
  const {onPress} = props;
  return (
    <Button
      onPress={onPress}
      style={{
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        position: 'absolute',
        bottom: hasNotch() ? 65 : 35,
        right: 15,
        zIndex: 99,
        paddingTop: 0,
        paddingBottom: 0,
        height: 45,
        width: 45,
        justifyContent: 'center',
        backgroundColor: white,
        borderRadius: 45 / 2,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{marginLeft: 0, marginRight: 0, fontSize: 25, color: mainRed}}
      />
    </Button>
  );
};

const Wrapper = compose(withApollo)(ScrollToTopButton);

export default props => <Wrapper {...props} />;
