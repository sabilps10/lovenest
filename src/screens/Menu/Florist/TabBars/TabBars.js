import React, {useMemo, useRef} from 'react';
import {Text, FlatList, TouchableOpacity, Dimensions, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {white, mainRed, greyLine} = Colors;
const {medium} = FontType;
const {width} = Dimensions.get('window');

const TabBars = props => {
  const {state, descriptors, navigation} = props;

  const {routes, index: activeIndex} = state;
  const tabBarRef = useRef();

  const getItemLayout = (data, index) => {
    // Max 4 items visibles at once
    return {
      length: Dimensions.get('window').width / 4,
      offset: (Dimensions.get('window').width / 4) * index,
      index,
    };
  };

  const keyExt = (_, index) => `${String(index)}`;

  const RenderTabBar = () => {
    return useMemo(() => {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 55,
            width,
            backgroundColor: 'white',
            borderBottomColor: greyLine,
            borderBottomWidth: 0,
            borderTopWidth: 0,
            borderTopColor: greyLine,
            paddingTop: 10,
          }}>
          <FlatList
            bounces={false}
            ref={tabBarRef}
            data={routes}
            extraData={routes}
            keyExtractor={keyExt}
            listKey={keyExt}
            getItemLayout={getItemLayout}
            snapToAlignment={'center'}
            snapToInterval={width / 4}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            renderItem={({item: route, index}) => {
              const {options} = descriptors[route.key];
              const label =
                options.tabBarLabel !== undefined
                  ? options.tabBarLabel
                  : options.title !== undefined
                  ? options.title
                  : route.name;

              const isFocused = state.index === index;

              const onPress = () => {
                const event = navigation.emit({
                  type: 'tabPress',
                  target: route.key,
                  canPreventDefault: true,
                });

                if (!isFocused && !event.defaultPrevented) {
                  tabBarRef.current.scrollToIndex({
                    animated: true,
                    index,
                    viewOffset: Dimensions.get('window').width / 2.15,
                  });
                  navigation.navigate(route.name);
                }
              };

              const onLongPress = () => {
                navigation.emit({
                  type: 'tabLongPress',
                  target: route.key,
                });
              };

              const color =
                label === routes[activeIndex].name ? mainRed : greyLine;

              const colorDot =
                label === routes[activeIndex].name ? mainRed : white;

              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole={'button'}
                  accessibilityStates={['selected']}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={() => onPress()}
                  onLongPress={() => onLongPress()}
                  style={{
                    width: width / 4,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.6),
                      color: color,
                      letterSpacing: 0.3,
                    }}>
                    {label}
                  </Text>
                  <View
                    style={{
                      marginTop: 7,
                      marginBottom: 5,
                      width: 5,
                      height: 5,
                      borderRadius: 5 / 2,
                      backgroundColor: colorDot,
                    }}
                  />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      );
    }, [activeIndex]);
  };

  if (routes) {
    return RenderTabBar();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(TabBars);

export default props => <Wrapper {...props} />;
