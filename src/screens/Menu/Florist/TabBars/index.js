import React from 'react';
import {Animated, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';

// Tabs
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

// Tabs Screens
import Home from '../TabScreens/Home';
import Products from '../TabScreens/Products';
import Promotion from '../TabScreens/Promotions';
import Portfolio from '../TabScreens/Portfolios';

// Components
import GlobalHeader from '../Components/GlobalHeader';
import TabBar from './TabBars';
import CoverImage from '../../../../components/Header/Florist/HeaderImage';

const Tabs = props => {
  console.log('Tabs >>> ', props);
  const {activeTab, onActiveTab, detail, id, serviceType, badge} = props;
  return (
    <Tab.Navigator
      lazy={true}
      swipeEnabled={true}
      animationEnabled={true}
      initialRouteName={activeTab}
      tabBar={tabBarProps => {
        return (
          <Animated.View>
            {activeTab === 'Home' ? (
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  zIndex: 2,
                }}>
                <GlobalHeader badge={badge} activeTab={activeTab} {...props} />
              </View>
            ) : null}
            {activeTab === 'Home' ? (
              <View style={{paddingTop: 25}}>
                <CoverImage
                  logoSource={
                    detail[0]?.logoImageDynamicUrl
                      ? {uri: `${detail[0]?.logoImageDynamicUrl}=h500`}
                      : {uri: detail[0]?.logoImageUrl}
                  }
                  tagline={detail[0]?.tagline}
                />
              </View>
            ) : null}
            <TabBar {...tabBarProps} />
          </Animated.View>
        );
      }}>
      <Tab.Screen
        name="Home"
        listeners={event => {
          console.log('Tab Event: ', event);
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Home
              id={id}
              detail={detail}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        name="Products"
        listeners={event => {
          console.log('Tab Event: ', event);
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Products
              id={id}
              detail={detail}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        name="Promotion"
        listeners={event => {
          console.log('Tab Event: ', event);
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Promotion
              id={id}
              detail={detail}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
      <Tab.Screen
        name="Portfolio"
        listeners={event => {
          console.log('Tab Event: ', event);
          const {route: tabPressRoute} = event;
          const {name: routeNamePress} = tabPressRoute;
          onActiveTab(routeNamePress);
        }}
        children={childProps => {
          return (
            <Portfolio
              id={id}
              detail={detail}
              serviceType={serviceType}
              {...props}
              {...childProps}
            />
          );
        }}
      />
    </Tab.Navigator>
  );
};

const Wrapper = compose(withApollo)(Tabs);

export default props => <Wrapper {...props} />;
