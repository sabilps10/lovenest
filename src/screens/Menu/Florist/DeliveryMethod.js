import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  FlatList,
  ActivityIndicator,
  Image,
  TextInput,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Footer,
  FooterTab,
} from 'native-base';
import {charImage} from '../../../utils/Themes/Images';
import Maps from '../../../components/Map/MapDragAblePoint';
import {hasNotch} from 'react-native-device-info';
const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  overlayDim,
} = Colors;
const {regular, small} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const {charDelivery, charSelfPickup} = charImage;

import GEOLOCATION from '../../../utils/CompleteGeoLocation';
import _ from 'lodash';

// Query
import QueryProductOrderAddress from '../../../graphql/queries/productOrderAddressFlorist';

// Mutation
import MutationProductDeliveryDetail from '../../../graphql/mutations/productDeliveryDetail';

import moment, {utc} from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

import Loader from '../../../components/Loader/JustLoader';
import {isNull} from 'lodash/fp';

//  Query get TimeSlot base on date
import TimeSlots from '../../../graphql/queries/getOrderFloristTimeSlot';

const globalDate = moment(new Date()).utc().local().format('DD MMMM YYYY');
const timeSlotData = [
  {
    id: 1,
    time: moment(new Date(`${globalDate} 12:00`))
      .utc()
      .local()
      .format(),
  },
  {
    id: 2,
    time: moment(new Date(`${globalDate} 15:00`))
      .utc()
      .local()
      .format(),
  },
  {
    id: 3,
    time: moment(new Date(`${globalDate} 18:00`))
      .utc()
      .local()
      .format(),
  },
  {
    id: 4,
    time: moment(new Date(`${globalDate} 20:30`))
      .utc()
      .local()
      .format(),
  },
];

export const Headers = props => {
  const {goBack, title} = props;

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          transparent
          style={{
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const DeliveryMethod = props => {
  console.log('DeliveryMethod Props: ', props);
  const {navigation, client} = props;

  const [showTimeSlot, setShowTimeSlot] = useState(false);

  const [listTimeSlot, setListTimeSLot] = useState([]);
  const [loadingTimeSlot, setLoadingTimeSlot] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  // for all
  const [selfPickupAddress, setSelfPickupAddress] = useState('');

  // For Selft Pickup Only
  const [addressList, setAddressList] = useState([]);

  // For Delivery Only
  const [streetsAndBlockNumber, setStreetAndBlockNumber] = useState('');
  const [nameFloorAndApartmentNumber, setNameFloorAndApartmentNumber] =
    useState('');
  const [buildingNameAndNameTown, setBuildingNameAndNameTown] = useState('');
  const [postCode, setPostCode] = useState('');

  const [deliverySelected, setDeliverySelected] = useState(false);
  const [pickupSelected, setPickupSelected] = useState(false);

  const [pickupDate, setPickupDate] = useState('');
  const [pickupTime, setPickupTime] = useState('');
  const [pickupTimeId, setPickupTimeId] = useState(null);

  const [showPickupDateAndroid, setShowPickupDateAndroid] = useState(false);
  const [showPickupTimeAndroid, setShowPickupTimeAndroid] = useState(false);

  const [showPickupDateIOS, setShowPickupDateIOS] = useState(false);
  const [showPickupTimeIOS, setShowPickupTimeIOS] = useState(false);

  const [isLoadingApply, setIsLoadingApply] = useState(false);
  const [isCheckEmptyForm, setCheckIsEmptyForm] = useState(false);

  const [postalCode, setPostalCode] = useState(null);
  const [country, setCountry] = useState('');
  const [city, setCity] = useState('');
  const [lat, setLat] = useState(0.0);
  const [lng, setLng] = useState(0.0);
  const [notes, setNotes] = useState('');

  const [isLoadingSearchAddress, setIsLoadingSearchAddress] = useState(false);

  const [isSuccess, setIsSuccess] = useState(false);
  const [isErrorSubmit, setIsErrorSubmit] = useState(false);
  const [messageError, setMessageError] = useState('');

  useEffect(() => {
    fetchAddressList();
    if (pickupDate !== '' && showTimeSlot) {
      fetcTimeSlot(pickupDate);
    }
  }, [pickupDate, showTimeSlot]);

  const fetcTimeSlot = async date => {
    try {
      console.log(
        'data JANCOEG',
        date !== ''
          ? moment(date).utc().local().format('YYYY-MM-DDTHH:mm:ss[Z]')
          : 'no date',
      );
      await setLoadingTimeSlot(true);
      client
        .query({
          query: TimeSlots,
          variables: {
            deliveryDate: moment(date)
              .utc()
              .local()
              .format('YYYY-MM-DDTHH:mm:ss[Z]'),
            type: deliverySelected ? 'Delivery' : 'Pickup',
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Time Slot: ', res);
          const {data, errors} = res;
          const {getOrderFloristTimeslot} = data;
          const {data: listTime, error} = getOrderFloristTimeslot;

          if (errors) {
            await setLoadingTimeSlot(false);
          } else {
            if (error) {
              await setLoadingTimeSlot(false);
            } else {
              await setListTimeSLot([...listTime]);
              await setLoadingTimeSlot(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setLoadingTimeSlot(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setLoadingTimeSlot(false);
    }
  };

  const manipulateAddresslist = list => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulating = await Promise.all(
          list.map((d, i) => {
            return {
              ...d,
              selected: false,
            };
          }),
        );

        if (manipulating.length === list.length) {
          resolve([...manipulating]);
        } else {
          resolve([]);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve([]);
      }
    });
  };

  const fetchAddressList = () => {
    try {
      client
        .query({
          query: QueryProductOrderAddress,
          variables: {
            type: 'Florist',
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          const {productOrderAddress} = data;
          const {data: list, error} = productOrderAddress;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              const getUpdatedDataAddress = await manipulateAddresslist(list);
              await setAddressList([...getUpdatedDataAddress]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const deliveryOnPress = async () => {
    try {
      if (pickupSelected) {
        await setDeliverySelected(!deliverySelected);
        await setPickupTime('');
        await setPickupTimeId(null);
        await setPickupSelected(false);
      } else {
        await setDeliverySelected(!deliverySelected);
      }
    } catch (error) {}
  };

  const pickupOnPress = async () => {
    try {
      if (deliverySelected) {
        await setPickupSelected(!pickupSelected);
        await setPickupTime('');
        await setPickupTimeId(null);
        await setDeliverySelected(false);
      } else {
        await setPickupSelected(!pickupSelected);
      }
    } catch (error) {}
  };

  const onOpenDatePickup = () => {
    if (Platform.OS === 'android') {
      // Android
      setShowPickupDateAndroid(true);
    } else {
      // iOS
      setShowPickupDateIOS(true);
    }
  };

  const onOpenTimePickup = async () => {
    // if (Platform.OS === 'android') {
    //   // Android
    //   setShowPickupTimeAndroid(true);
    // } else {
    //   // iOS
    //   setShowPickupTimeIOS(true);
    // }
    await setShowTimeSlot(true);
  };

  const onChangeTimePickupIOS = async e => {
    try {
      await setPickupTime(
        moment(e).utc().local().format('DD MMM YYYY hh:mm A'),
      );
      await setShowPickupTimeIOS(false);
    } catch (error) {
      console.log('Error: ', error);
      await setShowPickupTimeIOS(false);
    }
  };

  const onChangeDatePickupIOS = async e => {
    try {
      await setPickupDate(moment(e).utc().local().format('ddd, DD MMMM YYYY'));
      await setShowPickupDateIOS(false);
    } catch (error) {
      console.log('Error: ', error);
      await setShowPickupDateIOS(false);
    }
  };

  const onChangeDatePickupAndroid = async e => {
    try {
      if (e) {
        await setShowPickupDateAndroid(false);
        await setPickupDate(e);
      } else {
        await setShowPickupDateAndroid(false);
      }
    } catch (error) {
      await setShowPickupDateAndroid(false);
    }
  };

  const onChangeTimePickupAndroid = async e => {
    try {
      console.log('EEEE: ', e);
      if (e) {
        await setShowPickupTimeAndroid(false);
        await setPickupTime(e);
      } else {
        await setShowPickupTimeAndroid(false);
      }
    } catch (error) {
      await setShowPickupTimeAndroid(false);
    }
  };

  const onSelectAddress = async index => {
    try {
      const manipulated = await Promise.all(
        addressList.map((d, i) => {
          if (index === i) {
            return {
              ...d,
              selected: true,
            };
          } else {
            return {
              ...d,
              selected: false,
            };
          }
        }),
      );

      if (manipulated.length === addressList.length) {
        await setAddressList([...manipulated]);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const getAddressSelectedForPickup = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const getSelected = await Promise.all(
          addressList
            .map((d, i) => {
              if (d.selected) {
                return {
                  ...d,
                };
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        if (getSelected) {
          console.log('getSelected >>>> ', getSelected);
          resolve([...getSelected]);
        } else {
          resolve([]);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve([]);
      }
    });
  };

  const onApplySelfPickup = async variables => {
    try {
      client
        .mutate({
          mutation: MutationProductDeliveryDetail,
          variables,
        })
        .then(async response => {
          console.log('Response Self Pickup Mutation: ', response);
          const {data, errors} = response;
          const {productDeliveryDetails} = data;
          const {error} = productDeliveryDetails;

          if (errors) {
            await setIsErrorSubmit(true);
            await setTimeout(async () => {
              await setIsLoadingApply(false);
              await setIsSuccess(false);
              await setIsErrorSubmit(false);
            }, 1000);
          } else {
            if (error) {
              if (error === 'Selected timeslot is not available!') {
                await setIsErrorSubmit(true);
                await setMessageError('Selected timeslot is not available!');
                await setTimeout(async () => {
                  await setIsLoadingApply(false);
                  await setIsSuccess(false);
                  await setIsErrorSubmit(false);
                  await setMessageError('');
                }, 1000);
              } else {
                await setIsErrorSubmit(true);
                await setTimeout(async () => {
                  await setIsLoadingApply(false);
                  await setIsSuccess(false);
                  await setIsErrorSubmit(false);
                  await setMessageError('');
                }, 1000);
              }
            } else {
              await setIsSuccess(true);

              await setTimeout(async () => {
                await setIsLoadingApply(false);
                await setIsSuccess(false);
                await setIsErrorSubmit(false);
                navigation.goBack(null);
              }, 1000);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorSubmit(true);
          await setTimeout(async () => {
            await setIsLoadingApply(false);
            await setIsSuccess(false);
            await setIsErrorSubmit(false);
          }, 1000);
        });
    } catch (error) {
      await setIsErrorSubmit(true);
      await setTimeout(async () => {
        await setIsLoadingApply(false);
        await setIsSuccess(false);
        await setIsErrorSubmit(false);
      }, 1000);
    }
  };

  const onApplyDelivery = async variables => {
    try {
      client
        .mutate({
          mutation: MutationProductDeliveryDetail,
          variables,
        })
        .then(async response => {
          console.log('Response Delivery Mutation: ', response);
          const {data, errors} = response;
          const {productDeliveryDetails} = data;
          const {error} = productDeliveryDetails;

          if (errors) {
            await setIsErrorSubmit(true);
            await setTimeout(async () => {
              await setIsLoadingApply(false);
              await setIsSuccess(false);
              await setIsErrorSubmit(false);
            }, 1000);
          } else {
            if (error) {
              if (error === 'Selected timeslot is not available!') {
                await setIsErrorSubmit(true);
                await setMessageError('Selected timeslot is not available!');
                await setTimeout(async () => {
                  await setIsLoadingApply(false);
                  await setIsSuccess(false);
                  await setIsErrorSubmit(false);
                  await setMessageError('');
                }, 1000);
              } else {
                await setIsErrorSubmit(true);
                await setTimeout(async () => {
                  await setIsLoadingApply(false);
                  await setIsSuccess(false);
                  await setIsErrorSubmit(false);
                  await setMessageError('');
                }, 1000);
              }
            } else {
              await setIsSuccess(true);

              await setTimeout(async () => {
                await setIsLoadingApply(false);
                await setIsSuccess(false);
                await setIsErrorSubmit(false);
                navigation.goBack(null);
              }, 1000);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorSubmit(true);
          await setTimeout(async () => {
            await setIsLoadingApply(false);
            await setIsSuccess(false);
            await setIsErrorSubmit(false);
          }, 1000);
        });
    } catch (error) {
      await setIsErrorSubmit(true);
      await setTimeout(async () => {
        await setIsLoadingApply(false);
        await setIsSuccess(false);
        await setIsErrorSubmit(false);
      }, 1000);
    }
  };

  const onApply = async () => {
    try {
      console.log('***********************************');
      console.log('Delivery Selected: ', deliverySelected);
      console.log('Pickup Selected: ', pickupSelected);
      console.log('Address List: ', addressList);
      console.log('Date: ', pickupDate);
      console.log('Time: ', pickupTime);
      console.log('***********************************');

      console.log('================================');
      console.log('Address Complete: ', selfPickupAddress);
      console.log('postalCode: ', postalCode);
      console.log('country: ', country);
      console.log('city: ', city);
      console.log('lat: ', lat);
      console.log('lng: ', lng);
      console.log('================================');

      await setMessageError('');
      await setIsLoadingApply(true);
      await setCheckIsEmptyForm(false);

      if (!deliverySelected && !pickupSelected) {
        console.log('==============x================');
        // if user havent selected address option
        await setIsLoadingApply(false);
        await setCheckIsEmptyForm(true);
        await setTimeout(async () => {
          await setCheckIsEmptyForm(false);
        }, 1000);
      } else if (deliverySelected) {
        // user select delivery option
        if (
          selfPickupAddress === '' ||
          pickupDate === '' ||
          pickupTime === '' ||
          streetsAndBlockNumber === '' ||
          nameFloorAndApartmentNumber === ''
        ) {
          console.log('==============xx================');
          // user not fill all the requeirement
          await setMessageError('Please fill the input!');
          await setIsLoadingApply(false);
          await setCheckIsEmptyForm(true);
          await setTimeout(async () => {
            await setCheckIsEmptyForm(false);
            await setMessageError('');
          }, 1000);
        } else {
          console.log('=============xxx=================');
          // delivery option
          // insuficent data to store to API
          const location = await GEOLOCATION.detailLocation(
            `${selfPickupAddress}, ${streetsAndBlockNumber}, ${nameFloorAndApartmentNumber}, ${buildingNameAndNameTown}`,
          );
          console.log('Location: ', location);
          if (location) {
            const {
              postalCode: postalCodeNumber,
              country: countryName,
              city: cityName,
              lat: lats,
              lng: lngs,
              // address: completeAddress,
            } = location;
            if (!postalCodeNumber) {
              console.log('=============xxxx==================');
              throw new Error('Postal code not found');
            }
            console.log('============xxxxx==================');
            const variables = {
              deliveryType: 'Delivery',
              deliveryDate: moment.utc(new Date(pickupDate)).format(),
              // deliveryTime: moment.utc(new Date(pickupTime)).format(),
              address: {
                locationName: selfPickupAddress,
                address: selfPickupAddress,
                city: cityName,
                country: countryName,
                postCode: postalCodeNumber,
                latitude: parseFloat(lats),
                longitude: parseFloat(lngs),
                notes,
              },
              addressDetail: {
                detailAddreess: selfPickupAddress,
                streetAndBlock: streetsAndBlockNumber,
                floorAndAppartment: nameFloorAndApartmentNumber,
                buildingAndTown: buildingNameAndNameTown,
              },
              deliveryTimeslot: parseInt(pickupTimeId, 10),
            };
            console.log('DELIVERY VARIABLES: ', variables);
            await onApplyDelivery(variables);
          } else {
            console.log('=============xxxxxx==============');
            console.log('MASUK SINI MAS');
            await setIsLoadingApply(false);
            await setCheckIsEmptyForm(true);
            await setTimeout(async () => {
              await setCheckIsEmptyForm(false);
            }, 1000);
          }
        }
      } else {
        // user select self pickup
        const getAddressId = await getAddressSelectedForPickup();
        console.log('getAddressId: ', getAddressId);

        if (
          !getAddressId[0] &&
          getAddressId.length === 0 &&
          pickupDate === '' &&
          pickupTime === ''
        ) {
          console.log('MASUK SINI PICKUP ERROR');
          await setIsLoadingApply(false);
          await setCheckIsEmptyForm(true);
          await setTimeout(async () => {
            await setCheckIsEmptyForm(false);
          }, 1000);
        } else {
          const variables = {
            addressId: getAddressId[0].id,
            // address: {...getAddressId[0], notes},
            deliveryType: 'Pickup',
            deliveryDate: moment.utc(new Date(pickupDate)).format(),
            // deliveryTime: moment.utc(new Date(pickupTime)).format(),
            deliveryTimeslot: parseInt(pickupTimeId, 10),
          };
          console.log('VARIABLES SELF PICKUP: ', variables);
          await onApplySelfPickup(variables);
        }
      }
    } catch (error) {
      console.log('Error: kontol', error.message);
      await setMessageError(
        error.message === 'address not found' ? 'Address not found' : '',
      );
      await setIsLoadingApply(false);
      await setCheckIsEmptyForm(true);
      await setTimeout(async () => {
        await setCheckIsEmptyForm(false);
        await setMessageError('');
      }, 1000);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Headers title="Delivery Options" goBack={() => goBack()} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              bottom: 15,
              width: width / 4,
              height: height / 9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Loader />
          </View>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers title="Delivery Options" goBack={() => goBack()} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            onPress={fetchAddressList}
            style={{
              backgroundColor: mainGreen,
              height: height / 16,
              borderRadius: 4,
              elevation: 0,
              shadowOpacity: 0,
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              Refresh
            </Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <Headers title="Delivery Options" goBack={() => goBack()} />
        <Modal
          transparent
          visible={showTimeSlot}
          animationType="fade"
          onDismiss={() => setShowTimeSlot(false)}>
          <View
            style={{
              flex: 1,
              backgroundColor: overlayDim,
            }}>
            <TouchableOpacity
              onPress={() => setShowTimeSlot(false)}
              style={{flex: 1, backgroundColor: 'transparent'}}
            />
            <View style={{backgroundColor: white}}>
              <View
                style={{
                  position: 'absolute',
                  top: -10,
                  backgroundColor: white,
                  borderTopRightRadius: 5,
                  borderTopLeftRadius: 5,
                  width: '100%',
                  height: 20,
                }}
              />
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,

                  elevation: 5,
                }}>
                <View style={{flex: 0.2, padding: 10}} />
                <View
                  style={{
                    flex: 1,
                    padding: 15,
                    paddingTop: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      padding: 5,
                      paddingLeft: 10,
                      paddingRight: 10,
                      backgroundColor: mainGreen,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexWrap: 'wrap',
                      flexDirection: 'row',
                      borderRadius: 25,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.9),
                        color: white,
                        letterSpacing: 0.2,
                        lineHeight: 18,
                      }}>
                      TIME SLOTS
                    </Text>
                  </View>
                </View>
                <View style={{flex: 0.2, padding: 10}}>
                  <Button
                    onPress={async () => setShowTimeSlot(false)}
                    transparent
                    style={{
                      bottom: 5,
                      alignSelf: 'flex-end',
                      paddingTop: 0,
                      paddingBottom: 0,
                      height: 35,
                      width: 35,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="x"
                      style={{
                        marginLeft: 0,
                        marginRight: 0,
                        fontSize: 30,
                        color: mainGreen,
                      }}
                    />
                  </Button>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  borderWidth: 1,
                  borderRadius: 4,
                  borderColor: greyLine,
                  backgroundColor: '#f8f8f8',
                }}>
                {loadingTimeSlot ? (
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size="large" color={mainRed} />
                  </View>
                ) : (
                  <FlatList
                    contentContainerStyle={{
                      width: '90%',
                      padding: 15,
                      paddingTop: 0,
                      alignSelf: 'center',
                    }}
                    data={listTimeSlot}
                    extraData={listTimeSlot}
                    keyExtractor={(item, index) => `${item.id}`}
                    renderItem={({item, index}) => {
                      return (
                        <Card transparent>
                          <CardItem
                            disabled={
                              item.availabilty && item.status === 'Enabled'
                                ? false
                                : true
                            }
                            onPress={async () => {
                              await setPickupTime(item.timeslot);
                              await setPickupTimeId(item.id);
                              await setShowTimeSlot(false);
                            }}
                            button
                            style={{
                              backgroundColor: 'transparent',
                              borderBottomColor: greyLine,
                              borderBottomWidth: 1,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                textAlign: 'center',
                                fontFamily: medium,
                                fontSize: RFPercentage(1.7),
                                color:
                                  item.availabilty && item.status === 'Enabled'
                                    ? mainGreen
                                    : 'grey',
                                letterSpacing: 0.2,
                                lineHeight: 18,
                              }}>
                              {item.timeslot}
                            </Text>
                          </CardItem>
                        </Card>
                      );
                    }}
                  />
                )}
              </View>
            </View>
          </View>
        </Modal>
        {isLoadingApply ? (
          <View
            style={{
              zIndex: 99,
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {!isSuccess && !isErrorSubmit ? (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator size="large" style={{color: mainGreen}} />
              </View>
            ) : isSuccess ? (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="check"
                  style={{color: 'green', fontSize: 35}}
                />
              </View>
            ) : (
              <View
                style={{
                  minWidth: width / 5,
                  minHeight: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{color: mainRed, fontSize: 35}}
                />
                <Text style={{textAlign: 'center'}}>
                  {messageError !== '' ? messageError : ''}
                </Text>
              </View>
            )}
          </View>
        ) : null}

        {isCheckEmptyForm ? (
          <View
            style={{
              zIndex: 99,
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 10,
                borderRadius: 15,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                type="Feather"
                name="x"
                style={{color: mainRed, fontSize: 35, marginVertical: 5}}
              />
              <Text>
                {messageError !== ''
                  ? messageError
                  : 'Please check your input!'}
              </Text>
            </View>
          </View>
        ) : null}

        <Content contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}>
          <Card style={{marginLeft: 20, marginRight: 20, borderRadius: 4}}>
            <HeaderWithToggle
              onPress={deliveryOnPress}
              title={'Delivery'}
              image={charDelivery}
              value={deliverySelected}
            />
            {deliverySelected ? (
              <>
                <CardItem style={{paddingBottom: 0}}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.5),
                      fontStyle: 'italic',
                      color: greyLine,
                    }}>
                    Note: Long Press on the marker to move it
                  </Text>
                </CardItem>
                <MapView
                  onUpdateLocation={async e => {
                    try {
                      console.log('EEEEKKK: ', e);
                      const {
                        postalCode: postalCodeNumber,
                        country: countryName,
                        city: cityName,
                        lat: lats,
                        lng: lngs,
                        address: completeAddress,
                      } = e;
                      await setPostalCode(String(postalCodeNumber));
                      await setCountry(String(countryName));
                      await setCity(String(cityName));
                      await setLat(parseFloat(lats));
                      await setLng(parseFloat(lngs));
                      await setSelfPickupAddress(completeAddress);
                      await setIsLoadingSearchAddress(false);
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                  address={selfPickupAddress}
                />
                <Form
                  isError={false}
                  required={true}
                  isLoading={isLoadingSearchAddress}
                  numberOfLines={2}
                  multiline={true}
                  value={selfPickupAddress}
                  onChangeText={async e => {
                    await setSelfPickupAddress(e);
                    // if (selfPickupAddress !== '') {
                    //   await updateLocationAddress(e);
                    // }
                  }}
                  title={'Street / Post Code'}
                  placeholder={
                    'exp: City Gate, 371 Beach Road, Singapore 199597'
                  }
                />
                <Form
                  isError={false}
                  required={true}
                  isLoading={null}
                  numberOfLines={2}
                  multiline={true}
                  value={streetsAndBlockNumber}
                  onChangeText={async e => {
                    await setStreetAndBlockNumber(e);
                    // if (selfPickupAddress !== '') {
                    //   await updateLocationAddress(e);
                    // }
                  }}
                  title={'Block'}
                  placeholder={'exp: #19'}
                />
                <Form
                  isError={false}
                  required={true}
                  isLoading={null}
                  numberOfLines={2}
                  multiline={true}
                  value={nameFloorAndApartmentNumber}
                  onChangeText={async e => {
                    await setNameFloorAndApartmentNumber(e);
                    // if (selfPickupAddress !== '') {
                    //   await updateLocationAddress(e);
                    // }
                  }}
                  title={'Unit'}
                  placeholder={'exp: (apartment) - Floor 03'}
                />
                {/* <Form
                  isError={false}
                  required={true}
                  isLoading={null}
                  numberOfLines={2}
                  multiline={true}
                  value={buildingNameAndNameTown}
                  onChangeText={async e => {
                    await setBuildingNameAndNameTown(e);
                    // if (selfPickupAddress !== '') {
                    //   await updateLocationAddress(e);
                    // }
                  }}
                  title={'Unit'}
                  placeholder={'Type your address unit number here ...'}
                /> */}
                <DateAndTimePickupForm
                  disabled={false}
                  isError={false}
                  title={'Date Delivery'}
                  onOpenPicker={onOpenDatePickup}
                  pickupDate={pickupDate}
                  placeholder={'Select your pickup date'}
                  dateFormat={'ddd, DD MMM YYYY'}
                />
                <DateAndTimePickupForm
                  disabled={pickupDate === '' ? true : false}
                  isError={false}
                  title={'Time Delivery'}
                  onOpenPicker={onOpenTimePickup}
                  pickupDate={pickupTime}
                  placeholder={'Select your pickup time'}
                  dateFormat={'hh:mm A'}
                />
                <Form
                  isLoading={null}
                  numberOfLines={3}
                  multiline={true}
                  value={notes}
                  onChangeText={async e => {
                    await setNotes(e);
                  }}
                  title={'Notes'}
                  placeholder={'Write here...'}
                />
              </>
            ) : null}
          </Card>

          {/* Pickup */}
          {addressList.length === 0 ? null : (
            <Card style={{marginLeft: 20, marginRight: 20, borderRadius: 4}}>
              <HeaderWithToggle
                onPress={pickupOnPress}
                title={'Pickup in store'}
                image={charSelfPickup}
                value={pickupSelected}
              />
              {pickupSelected ? (
                <>
                  <FlatList
                    data={addressList}
                    extraData={addressList}
                    keyExtractor={(item, index) => `${item.id}`}
                    renderItem={({item, index}) => {
                      return (
                        <CardItem
                          button
                          onPress={() => onSelectAddress(index)}
                          style={{width: '100%'}}>
                          <View style={{width: '100%', flexDirection: 'row'}}>
                            <View style={{flex: 0.2}}>
                              <IconSelected selected={item.selected} />
                            </View>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                              }}>
                              <View style={{marginBottom: 10}}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    fontSize: RFPercentage(1.8),
                                    color: black,
                                    letterSpacing: 0.3,
                                  }}>
                                  {item.locationName}
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: book,
                                    fontSize: RFPercentage(1.8),
                                    color: black,
                                    letterSpacing: 0.3,
                                    lineHeight: 20,
                                  }}>
                                  {item.address}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </CardItem>
                      );
                    }}
                  />
                  <DateAndTimePickupForm
                    disabled={false}
                    isError={false}
                    title={'Date Pickup'}
                    onOpenPicker={onOpenDatePickup}
                    pickupDate={pickupDate}
                    placeholder={'Select your pickup date'}
                    dateFormat={'ddd, DD MMM YYYY'}
                  />
                  <DateAndTimePickupForm
                    disabled={pickupDate === '' ? true : false}
                    isError={false}
                    title={'Time Pickup'}
                    onOpenPicker={onOpenTimePickup}
                    pickupDate={pickupTime}
                    placeholder={'Select your pickup time'}
                    dateFormat={'hh:mm A'}
                  />
                  {/* <Form
                    isLoading={null}
                    numberOfLines={3}
                    multiline={true}
                    value={notes}
                    onChangeText={async e => {
                      await setNotes(e);
                    }}
                    title={'Notes'}
                    placeholder={'Write here...'}
                  /> */}
                </>
              ) : null}
            </Card>
          )}
        </Content>

        {/* IOS Time Picker */}
        {Platform.OS === 'ios' ? (
          showPickupTimeIOS ? (
            <TimePickerIOS
              cancel={() => setShowPickupTimeIOS(false)}
              pickerMode={'time'}
              theDate={pickupTime === '' ? '' : pickupTime}
              onChange={e => onChangeTimePickupIOS(e)}
            />
          ) : null
        ) : null}

        {/* IOS Date Picker */}
        {Platform.OS === 'ios' ? (
          showPickupDateIOS ? (
            <Modal visible={showPickupDateIOS} transparent animationType="fade">
              <DatePickerIOS
                cancel={() => setShowPickupDateIOS(false)}
                pickerMode={'date'}
                theDate={pickupDate === '' ? '' : pickupDate}
                onChange={e => onChangeDatePickupIOS(e)}
              />
            </Modal>
          ) : null
        ) : null}

        {/* Android Date Picker */}
        {Platform.OS === 'android' ? (
          showPickupDateAndroid ? (
            <DatePickerAndroid
              cancel={async () => await setShowPickupDateAndroid(false)}
              pickerMode={'date'}
              show={showPickupDateAndroid}
              theDate={pickupDate === '' ? '' : pickupDate}
              onChange={e => onChangeDatePickupAndroid(e)}
            />
          ) : null
        ) : null}

        {/* Android Time Picker */}
        {Platform.OS === 'android' ? (
          showPickupTimeAndroid ? (
            <DatePickerAndroid
              cancel={async () => await setShowPickupTimeAndroid(false)}
              pickerMode={'time'}
              show={showPickupTimeAndroid}
              theDate={pickupTime === '' ? '' : pickupTime}
              onChange={e => onChangeTimePickupAndroid(e)}
            />
          ) : null
        ) : null}

        {/* Button Apply */}
        {!deliverySelected && !pickupSelected ? null : Platform.OS === 'ios' ? (
          showPickupDateIOS || showPickupTimeIOS ? null : (
            <ButtonAddToCart
              isLoading={isLoadingApply}
              title={'APPLY'}
              onPress={() => {
                onApply();
              }}
              disabled={false}
            />
          )
        ) : (
          <ButtonAddToCart
            isLoading={isLoadingApply}
            title={'APPLY'}
            onPress={() => {
              onApply();
            }}
            disabled={false}
          />
        )}
      </Container>
    );
  }
};

export const IconSelected = props => {
  const {selected} = props;
  return (
    <View
      style={{
        width: 20,
        height: 20,
        borderWidth: 2,
        borderRadius: 20 / 2,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {selected ? (
        <View
          style={{
            width: 10,
            height: 10,
            borderWidth: 1,
            borderRadius: 10 / 2,
            backgroundColor: black,
          }}
        />
      ) : null}
    </View>
  );
};

export const DateAndTimePickupForm = props => {
  const {
    title,
    onOpenPicker,
    pickupDate,
    placeholder,
    dateFormat,
    isError,
    disabled,
  } = props;
  return (
    <CardItem>
      <View style={{width: '100%'}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{color: mainRed}}>*</Text>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
          {disabled ? (
            <Text
              style={{
                marginLeft: 5,
                fontFamily: medium,
                color: greyLine,
                fontSize: RFPercentage(1.5),
                letterSpacing: 0.3,
                fontStyle: 'italic',
              }}>
              (please select date first!)
            </Text>
          ) : null}
        </View>
        <View style={{marginTop: 5, paddingLeft: 7, paddingRight: 7}}>
          <TouchableOpacity
            disabled={disabled}
            onPress={onOpenPicker}
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'flex-start',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <Text
              style={{
                color: pickupDate === '' ? greyLine : black,
                fontSize: RFPercentage(1.85),
                letterSpacing: 0.3,
              }}>
              {pickupDate === ''
                ? placeholder
                : dateFormat === 'hh:mm A'
                ? pickupDate
                : moment(pickupDate).utc().local().format(dateFormat)}
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: 10,
            borderBottomColor: isError ? mainRed : greyLine,
            borderBottomWidth: 1,
            width: '100%',
            height: 0.5,
          }}
        />
      </View>
    </CardItem>
  );
};

export const Form = props => {
  const {
    isError,
    required,
    value,
    onChangeText,
    title,
    placeholder,
    multiline,
    numberOfLines,
    isLoading,
  } = props;
  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View style={{flexDirection: 'row', width: '100%'}}>
          {required ? <Text style={{color: mainRed}}>*</Text> : null}
          {!required ? (
            <Text
              style={{
                left: 3,
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {`${title}`}
            </Text>
          ) : (
            <Text
              style={{
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          )}
          {isLoading ? (
            <View style={{marginLeft: 10, flexDirection: 'row'}}>
              <Text
                style={{
                  fontSize: RFPercentage(1.5),
                  color: greyLine,
                }}>
                {'updating address...'}
              </Text>
              <ActivityIndicator
                size="small"
                color={mainGreen}
                style={{marginHorizontal: 5, bottom: 5}}
              />
            </View>
          ) : null}
        </View>
        <View style={{marginTop: 5}}>
          <TextInput
            // returnKeyType="done"
            numberOfLines={numberOfLines}
            multiline={multiline}
            style={
              Platform.OS === 'android'
                ? {
                    textAlignVertical: 'top',
                    borderBottomWidth: 1,
                    borderBottomColor: isError ? mainRed : greyLine,
                    backgroundColor: '#ffffff',
                    fontFamily: book,
                    fontSize: RFPercentage(1.8),
                    lineHeight: 18,
                  }
                : {
                    textAlignVertical: 'top',
                    borderBottomWidth: 1,
                    borderBottomColor: isError ? mainRed : greyLine,
                    backgroundColor: '#ffffff',
                    fontFamily: book,
                    paddingLeft: 5,
                    paddingBottom: 10,
                    fontSize: RFPercentage(1.8),
                    lineHeight: 18,
                  }
            }
            value={value}
            placeholder={placeholder}
            onChangeText={e => onChangeText(e)}
          />
        </View>
      </View>
    </CardItem>
  );
};

export const MapView = props => {
  const {address, onUpdateLocation} = props;
  return (
    <CardItem>
      <Maps
        onUpdateLocation={e => onUpdateLocation(e)}
        forMapAddress={address === '' ? 'Singapore' : address}
      />
    </CardItem>
  );
};

export const HeaderWithToggle = props => {
  const {onPress, title, image, value} = props;

  return (
    <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
      <View style={{width: '100%', flexDirection: 'row'}}>
        <View
          style={{
            flex: 0.2,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Image
            source={image}
            style={{width: width / 7, height: height / 20, right: 13}}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
        </View>
        <View style={{flex: 0.2, justifyContent: 'center'}}>
          <Button
            onPress={onPress}
            transparent
            style={{
              alignSelf: 'flex-end',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name={value ? 'chevron-up' : 'chevron-down'}
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </View>
      </View>
    </CardItem>
  );
};

export const TimePickerIOS = props => {
  console.log('KONTOL: ', props);
  const {theDate, pickerMode, onChange, cancel} = props;

  const [date, setDate] = useState(theDate === '' ? new Date() : theDate);

  const onChangeDate = (event, selectedDate) => {
    console.log('IOS Date Picker Event: ', event);
    console.log('IOS Date Picker SelectedDate: ', selectedDate);
    setDate(selectedDate);
  };

  const ok = () => {
    onChange(date === '' ? new Date() : date);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: overlayDim,
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          cancel();
        }}
        style={{flex: 2, backgroundColor: 'transparent'}}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: greyLine,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: '#f8f8f8',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => cancel()}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => {
                ok();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: '100%', backgroundColor: white}}>
          <DateTimePicker
            style={{position: 'relative', left: hasNotch() ? 50 : 20}}
            testID="dateTimePicker"
            value={date === '' ? new Date() : new Date(date)}
            mode={pickerMode}
            is24Hour={true}
            display="default"
            onChange={onChangeDate}
          />
        </View>
      </View>
    </View>
  );
};

export const DatePickerIOS = props => {
  const {theDate, pickerMode, onChange, cancel} = props;

  const [date, setDate] = useState(theDate === '' ? new Date() : theDate);

  const onChangeDate = (event, selectedDate) => {
    console.log('IOS Date Picker Event: ', event);
    console.log('IOS Date Picker SelectedDate: ', selectedDate);
    setDate(selectedDate);
  };

  const ok = () => {
    onChange(date === '' ? new Date() : date);
  };

  return (
    <View
      style={{
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: overlayDim,
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          cancel();
        }}
        style={{flex: 2, backgroundColor: 'transparent'}}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: greyLine,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: '#f8f8f8',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => cancel()}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end',
              padding: 7,
            }}>
            <TouchableOpacity
              onPress={() => {
                ok();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 7,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: mainRed,
                  fontSize: RFPercentage(1.8),
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{width: '100%', backgroundColor: white}}>
          <DateTimePicker
            minimumDate={new Date()}
            // style={{position: 'relative', left: hasNotch() ? 50 : 20}}
            testID="dateTimePicker"
            value={date === '' ? new Date() : new Date(date)}
            mode={pickerMode}
            is24Hour={true}
            display="spinner"
            onChange={onChangeDate}
          />
        </View>
      </View>
    </View>
  );
};

export const DatePickerAndroid = props => {
  const {show, theDate, pickerMode, onChange: onChangeDate, cancel} = props;
  console.log('theDate Date Picker Android: ', theDate);

  const [date, setDate] = useState(theDate);
  const [mode, setMode] = useState(pickerMode);
  const [showup, setShowUp] = useState(false);

  useEffect(() => {
    settingShow(show);
  }, []);

  const settingShow = value => {
    setShowUp(value);
  };

  const onChange = (event, selectedDate) => {
    console.log('Event Date Picker Android: ', event);
    console.log('SelectedDate Date Picker Android: ', selectedDate);
    if (event.type === 'set') {
      onChangeDate(selectedDate);
      settingShow(false);
    } else {
      cancel();
      settingShow(false);
    }
  };

  return (
    <View>
      {showup ? (
        <DateTimePicker
          testID="dateTimePicker"
          minimumDate={new Date()}
          value={theDate === '' ? new Date() : new Date(date)}
          mode={pickerMode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </View>
  );
};

export const TimePickerAndroid = props => {
  const {show, theTime, pickerMode, onChange: onChangeTime, cancel} = props;
  console.log('theDate Date Picker Android: ', theTime);

  const [time, setTime] = useState(theTime);
  const [mode, setMode] = useState(pickerMode);
  const [showup, setShowUp] = useState(false);

  useEffect(() => {
    settingShow(show);
  }, []);

  const settingShow = value => {
    setShowUp(value);
  };

  const onChange = (event, selectedTime) => {
    console.log('Event Date Picker Android: ', event);
    console.log('SelectedDate Date Picker Android: ', selectedTime);
    if (event.type === 'set') {
      onChangeTime(selectedTime);
      settingShow(false);
    } else {
      cancel();
      settingShow(false);
    }
  };

  return (
    <View>
      {showup ? (
        <DateTimePicker
          testID="dateTimePicker"
          value={theTime === '' ? new Date() : new Date(time)}
          mode={pickerMode}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      ) : null}
    </View>
  );
};

export const ButtonAddToCart = props => {
  const {onPress, title, disabled, isLoading} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            disabled={disabled}
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                color={white}
                style={{marginLeft: 10}}
              />
            ) : (
              isNull
            )}
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(DeliveryMethod);

export default props => <Wrapper {...props} />;
