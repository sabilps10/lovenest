/* eslint-disable no-unused-vars */
import React, {useState, useEffect, useRef} from 'react';
import {
  Dimensions,
  View,
  Text,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Image,
  StatusBar,
  Animated
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Button} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import DefaultHeader from '../../../components/Header/Common/';
import {ScrollView} from 'react-native-gesture-handler';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';
import {FontType} from '../../../utils/Themes/Fonts';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncModel from '../../../utils/AsyncstorageDataStructure';

// redux
import {connect} from 'react-redux';

import Tabs from './TabBars/index';
import GlobalHeader from './Components/GlobalHeader';
import ScrollToTopButton from './Components/ScrollToTopButton';

const {charCommingSoonNew} = charImage;
const {white, black, mainGreen} = Colors;
const {height, width} = Dimensions.get('window');
const {book, medium} = FontType;
const {asyncToken} = AsyncModel;

// Card
import CardFlorist from '../../../components/Cards/Merchants/FloristCard';

// Query
import GET_ALL_MERCHANT from '../../../graphql/queries/getMerchantByServiceType';
import FloristCartBadge from '../../../graphql/queries/floristCartBadge';

const Florist = props => {
  console.log('FLORIST PROPS: ', props);
  const {navigation, positionYBottomNav, height: reduxTabScreenheight, client} = props;

  const scrollViewRef = useRef();

  const [activeTab, setActiveTab] = useState('Home');

  const [tabScreenHeight, setTabScreenHeight] = useState(500);

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  let [itemDisplayed, setItemDisplayed] = useState(10);
  const pageNumber = 1;

  const [refresh, setRefresh] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [badge, setBadge] = useState(0);
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    checkIsLogin();
    if (isLogin) {
      fetchBadge();
    }
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    updateTabScreenHeight();
    fetch();
    onLoadMore();

    const subs = navigation.addListener('focus', () => {
      checkIsLogin();
      if (isLogin) {
        fetchBadge();
      }
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
      updateTabScreenHeight();
      fetch();
      onLoadMore();
    });

    return subs;
  }, [isLoadMore, onLoadMore, activeTab, reduxTabScreenheight]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const checkingToken = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const auth = await checkingToken();

      if (auth) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLogin(false);
    }
  };

  const fetchBadge = () => {
    try {
      client
        .query({
          query: FloristCartBadge,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Fetch Badge: ', response);
          const {data, errors} = response;
          const {cartBadge} = data;
          const {totalCount, error} = cartBadge;

          if (errors) {
            await setBadge(0);
          } else {
            if (error) {
              await setBadge(0);
            } else {
              if (totalCount) {
                await setBadge(totalCount);
              } else {
                await setBadge(0);
              }
            }
          }
        })
        .catch(async error => {
          console.log('error: ', error);
          await setBadge(0);
        });
    } catch (error) {
      console.log('Error: ', error);
      setBadge(0);
    }
  };

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        let oldItemDisplayed = itemDisplayed;
        let newItemDisplayed = oldItemDisplayed + 10;
        await setItemDisplayed(newItemDisplayed);
        await fetch();
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefresh(true);
      await setItemDisplayed(3);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefresh(false);
    }
  };

  const fetch = async () => {
    try {
      console.log('ITEM DISPLAYED: ', itemDisplayed);
      client
        .query({
          query: GET_ALL_MERCHANT,
          variables: {
            serviceType: 'Florist',
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response: GET_ALL_MERCHANT', response);
          const {data, errors} = response;
          const {getMerchantsByServiceType} = data;
          const {data: merchantList, error} = getMerchantsByServiceType;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefresh(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setList([...merchantList]);
              // await setList([]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefresh(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefresh(false);
      await setIsLoadMore(false);
    }
  };

  const scrollToTop = () => {
    console.log('Ref Scroll view: ', scrollViewRef?.current);
    try {
      scrollViewRef?.current?.scrollTo({
        y: 0,
        animated: true,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const updateTabScreenHeight = async () => {
    await setTabScreenHeight(reduxTabScreenheight);
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error go Back: ', error);
    }
  };

  if (isLoading && !isError) {
    // Show Loading
    return (
      <Container>
        <DefaultHeader
          title={'Florist'}
          buttonLeft={goBack}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    // Show Error Screen
    return (
      <Container>
        <DefaultHeader
          title={'Florist'}
          buttonLeft={goBack}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            onPress={onRefresh}
            style={{
              borderRadius: 4,
              padding: 5,
              paddingRight: 15,
              paddingLeft: 15,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: mainGreen,
            }}>
            <Text style={{color: white}}>Refresh</Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    if (list?.length === 0) {
      return (
        <Container>
          <DefaultHeader
            title={'Florist'}
            buttonLeft={goBack}
            buttonRight={() => {}}
            showRightButton={false}
          />
          <View
            style={{
              padding: 15,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={charCommingSoonNew}
              style={{bottom: 90, width, height: height / 3}}
              resizeMode="contain"
            />
            <Text
              style={{
                bottom: 75,
                fontFamily: medium,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2.5),
              }}>
              Coming Soon
            </Text>
            <Text
              style={{
                lineHeight: 20,
                bottom: 60,
                fontFamily: book,
                color: black,
                textAlign: 'center',
                fontSize: RFPercentage(2),
              }}>
              Keep updated with us, we preparing our feature very soon !
            </Text>
          </View>
        </Container>
      );
    } else if (list?.length > 1) {
      return (
        <Container style={{backgroundColor: white}}>
          <>
            <DefaultHeader
              title={'Florist'}
              buttonLeft={goBack}
              buttonRight={() => goBack()}
              showRightButton={true}
            />
            <View style={{flex: 1, zIndex: -99, height}}>
              <FlatList
                ListFooterComponent={() => {
                  if (isLoadMore) {
                    return (
                      <ActivityIndicator
                        size="small"
                        color={mainGreen}
                        style={{bottom: 12, marginBottom: 10}}
                      />
                    );
                  } else {
                    return null;
                  }
                }}
                refreshControl={
                  <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
                }
                contentContainerStyle={{padding: 15}}
                data={list}
                extraData={list}
                keyExtractor={item => `${item.id}`}
                legacyImplementation
                disableVirtualization
                onEndReached={async ({distanceFromEnd}) => {
                  if (distanceFromEnd < 0) {
                    onLoadMore();
                  }
                }}
                onMomentumScrollBegin={async () => {
                  await setIsLoadMore(true);
                }}
                onEndReachedThreshold={0.1}
                renderItem={({item}) => {
                  const {
                    id,
                    name,
                    tagline,
                    coverImageUrl,
                    coverImageDynamicUrl,
                    logoImageUrl,
                    logoImageDynamicUrl,
                  } = item;
                  const source = coverImageDynamicUrl
                    ? {uri: `${coverImageDynamicUrl}=h300`}
                    : {uri: coverImageUrl};
                  const iconSource = logoImageDynamicUrl
                    ? {uri: `${logoImageDynamicUrl}=h300`}
                    : {uri: logoImageUrl};
                  return (
                    <CardFlorist
                      title={name}
                      tag={tagline}
                      source={source}
                      iconSource={iconSource}
                      onPress={() => {
                        navigation.navigate('FloristMerchantDetail', {id});
                      }}
                    />
                  );
                }}
              />
            </View>
          </>
        </Container>
      );
    } else {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          <StatusBar translucent={false} />
          {activeTab === 'Home' ? (
            <>
              <ScrollView ref={scrollViewRef} decelerationRate="fast">
                <Tabs
                  badge={badge}
                  id={list[0]?.id}
                  serviceType={list[0]?.serviceType}
                  detail={list}
                  onActiveTab={tabName => setActiveTab(tabName)}
                  activeTab={activeTab}
                  {...props}
                />
              </ScrollView>
              <ScrollToTopButton onPress={scrollToTop} />
            </>
          ) : (
            <>
              {activeTab !== 'Home' ? (
                <GlobalHeader
                  badge={badge}
                  activeTab={activeTab}
                  {...props}
                  detail={list}
                />
              ) : null}
              <Tabs
                badge={badge}
                id={list[0]?.id}
                serviceType={list[0]?.serviceType}
                detail={list}
                onActiveTab={tabName => setActiveTab(tabName)}
                activeTab={activeTab}
                {...props}
              />
            </>
          )}
        </View>
      );
    }
  }
};

const mapToState = state => {
  console.log('Redux State: ', state);
  const {floristTabScreenHeight} = state;
  return {
    floristTabScreenHeight,
    positionYBottomNav: state?.positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(Florist);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
