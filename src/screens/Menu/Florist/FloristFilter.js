import React, {useState, useMemo, useEffect, useRef} from 'react';
import {
  Text,
  View,
  Dimensions,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Slider from '@react-native-community/slider';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Loader from '../../../components/Loader/JustLoader';
import {connect} from 'react-redux';

// Redux
import FilterPriceRedux from '../../../redux/thunk/FloristPriceFilterThunk';
import FilterSizeRedux from '../../../redux/thunk/FloristSizeFilterThunk';
import FilterCategoriesRedux from '../../../redux/thunk/FloristCategoriesFilterThunk';
import FilterTypeRedux from '../../../redux/thunk/FloristTypeFilterThunk';
import FilterColorRedux from '../../../redux/thunk/FloristColorFilterThunk';
import FilterStatusRedux from '../../../redux/thunk/FloristFilterStatusThunk';
import FilterThemeRedux from '../../../redux/thunk/FloristThemeFilterThunk';

// Query
import FloristType from '../../../graphql/queries/getFlowerType';
// import FloristCategories from '../../../graphql/queries/getFlowerCategories';
import FloristColors from '../../../graphql/queries/getFlowerColors';
import FloristThemes from '../../../graphql/queries/getFlowerTheme';
import FloristPriceRange from '../../../graphql/queries/productPriceRange';
import FloristSizeRange from '../../../graphql/queries/productSizeRange';

import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFilter from '../../../components/Button/ButtonFilters/Unfilter';

const {white, greyLine, headerBorderBottom, black, mainRed, mainGreen} = Colors;
const {small, regular} = FontSize;
const {medium} = FontType;
const {width, height} = Dimensions.get('window');

export const Headers = props => {
  const {goBack, reset, title, isLoading} = props;

  return (
    <Header
      androidStatusBarColor={'white'}
      iosBarStyle="dark-content"
      style={{
        elevation: 0,
        shadowOpacity: 0,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
      }}>
      <Left style={{flex: 0.2}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            shadowOpacity: 0,
            borderWidth: 0,
            backgroundColor: 'transparent',
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            right: 5,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(2),
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.2}}>
        {isLoading ? (
          <View style={{width: width / 10, height: height / 15}}>
            <Loader />
          </View>
        ) : (
          <Button
            onPress={() => reset()}
            style={{
              alignSelf: 'flex-end',
              elevation: 0,
              shadowOpacity: 0,
              borderWidth: 0,
              backgroundColor: 'transparent',
              paddingTop: 0,
              paddingBottom: 0,
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              Reset
            </Text>
          </Button>
        )}
      </Right>
    </Header>
  );
};

const FloristFilter = props => {
  console.log('FloristFilter Props: ', props);
  const {
    navigation,
    client,
    price: priceState,
    size: sizeState,
    type: typeState,
    color: colorState,
    themes: themeState,
    status,
    filterPrice,
    filterSize,
    filterType,
    filterColor,
    filterStatus,
    filterTheme,
    route,
    selectedCategories: reduxSelectedCategory,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [minPrice, setMinPrice] = useState(0.0);
  const [maxPrice, setMaxPrice] = useState(0.0);

  const [minSize, setMinSize] = useState(0.0);
  const [maxSize, setMaxSize] = useState(0.0);

  const [isLoadingReset, setIsLoadingReset] = useState(false);
  const [isLoadingSubmitFilter, setIsLoadingSubmitFilter] = useState(false);

  const [valuePrice, setValuePrice] = useState([0, 10]);
  const [isLoadingPrice, setIsLoadingPrice] = useState(true);
  const [isErrorPrice, setIsErrorPrice] = useState(false);

  const [valueSize, setValueSize] = useState([0.0, 1.0]);
  const [isLoadingSize, setIsLoadingSize] = useState(true);
  const [isErrorSize, setIsErrorSize] = useState(false);

  const [type, setType] = useState([]);
  const [isLoadingType, setIsLoadingType] = useState(true);
  const [isErrorType, setIsErrorType] = useState(false);

  const [color, setColor] = useState([]);
  const [isLoadingColor, setIsLoadingColor] = useState(true);
  const [isErrorColor, setIsErrorColor] = useState(false);

  const [themes, setThemes] = useState([]);
  const [isLoadingThemes, setIsLoadingThemes] = useState(true);
  const [isErrorThemes, setIsErrorThemes] = useState(false);

  useEffect(() => {
    globalFetch();
    const subscriber = navigation.addListener('focus', () => {
      globalFetch();
    });

    return subscriber;
  }, []);

  const globalFetch = async () => {
    try {
      if (status) {
        await fetchProductPriceRange(reduxSelectedCategory);
        await fetchProductSizeRange(reduxSelectedCategory);
        await settingPrice(priceState);
        await settingSize(sizeState);
        await fetchFlowerType([...typeState], reduxSelectedCategory);
        await fetchFlowerColor([...colorState], reduxSelectedCategory);
        await fetchFlowerThemes([...themeState], reduxSelectedCategory); // change later 11 March 2021
      } else {
        await fetchProductPriceRange(reduxSelectedCategory);
        await fetchProductSizeRange(reduxSelectedCategory);
        await settingPrice([0, 0]);
        await settingSize([0.0, 0.0]);
        await fetchFlowerType(null, reduxSelectedCategory);
        await fetchFlowerColor(null, reduxSelectedCategory);
        await fetchFlowerThemes(null, reduxSelectedCategory);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const settingPrice = async reduxPrice => {
    try {
      if (reduxPrice) {
        await setValuePrice(reduxPrice);
        await setIsErrorPrice(false);
        await setIsLoadingPrice(false);
      } else {
        await setValuePrice([0, 0]);
        await setIsErrorPrice(false);
        await setIsLoadingPrice(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPrice(true);
      await setIsLoadingPrice(false);
    }
  };

  const settingSize = async reduxSize => {
    try {
      if (reduxSize) {
        await setValueSize(reduxSize);
        await setIsErrorSize(false);
        await setIsLoadingSize(false);
      } else {
        await setValueSize([0.0, 0.0]);
        await setIsErrorSize(false);
        await setIsLoadingSize(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorSize(true);
      await setIsLoadingSize(false);
    }
  };

  const fetchProductSizeRange = async floristCategory => {
    try {
      await client
        .query({
          query: FloristSizeRange,
          variables: {
            type: serviceType,
            merchantdId: parseInt(id, 10),
            floristCategory:
              floristCategory?.length === 0 ? null : floristCategory[0],
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchProductSizeRange: ', response);
          const {data, errors} = response;
          const {productSizeRange} = data;
          const {data: objPrice, error} = productSizeRange;

          if (errors) {
            // error
          } else {
            if (error) {
              // error
            } else {
              const {max, min} = objPrice;
              await setMinSize(parseFloat(min).toFixed(1));
              await setMaxSize(parseFloat(max).toFixed(1));
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetchProductPriceRange = async floristCategory => {
    try {
      await client
        .query({
          query: FloristPriceRange,
          variables: {
            type: serviceType,
            merchantdId: parseInt(id, 10),
            floristCategory:
              floristCategory?.length === 0 ? null : floristCategory[0],
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchProductPriceRange: ', response);
          const {data, errors} = response;
          const {productPriceRange} = data;
          const {data: objPrice, error} = productPriceRange;

          if (errors) {
            // error
          } else {
            if (error) {
              // error
            } else {
              const {max, min} = objPrice;
              await setMinPrice(parseFloat(min).toFixed(1));
              await setMaxPrice(parseFloat(max).toFixed(1));
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetchFlowerThemes = async (reduxData, floristCategory) => {
    try {
      if (reduxData) {
        // redux themes will be here
        await setThemes([...reduxData]);
        await setIsErrorThemes(false);
        await setIsLoadingThemes(false);
      } else {
        await client
          .query({
            query: FloristThemes,
            variables: {
              merchantdId: parseInt(id, 10),
              floristCategory:
                floristCategory?.length !== 0 ? floristCategory[0] : null,
            },
            ssr: false,
            fetchPolicy: 'no-cache',
          })
          .then(async response => {
            console.log('Florist Themes: ', response);
            const {data, errors} = response;
            const {floristThemes} = data;
            const {data: listTheme, error} = floristThemes;

            if (errors) {
              await setIsErrorThemes(true);
              await setIsLoadingThemes(false);
            } else {
              if (error) {
                await setIsErrorThemes(true);
                await setIsLoadingThemes(false);
              } else {
                const manipulate = await Promise.all(
                  listTheme.map((d, i) => {
                    return {
                      ...d,
                      selected: false,
                    };
                  }),
                );

                if (manipulate.length === listTheme.length) {
                  await setThemes([...manipulate]);
                  await setIsErrorThemes(false);
                  await setIsLoadingThemes(false);
                } else {
                  throw new Error('Failed manipulated data theme Filter');
                }
              }
            }
          })
          .catch(error => {
            console.log('Error: ', error);
            throw error;
          });
      }
    } catch (error) {
      await setIsErrorThemes(true);
      await setIsLoadingThemes(false);
    }
  };

  const fetchFlowerType = async (reduxData, floristCategory) => {
    console.log('FLOWER TYPE FETCH: ', {reduxData, floristCategory});
    try {
      if (reduxData) {
        await setType([...reduxData]);
        await setIsErrorType(false);
        await setIsLoadingType(false);
      } else {
        client
          .query({
            query: FloristType,
            variables: {
              merchantdId: parseInt(id, 10),
              floristCategory:
                floristCategory?.length !== 0 ? floristCategory[0] : null,
            },
            ssr: false,
            fetchPolicy: 'no-cache',
          })
          .then(async response => {
            console.log('Response Type: ', response);
            const {data, errors} = response;
            const {floristTypes} = data;
            const {data: listType, error} = floristTypes;

            if (errors) {
              await setIsErrorType(true);
              await setIsLoadingType(false);
            } else {
              if (error) {
                await setIsErrorType(true);
                await setIsLoadingType(false);
              } else {
                const manipulate = await Promise.all(
                  listType.map((d, i) => {
                    return {
                      ...d,
                      selected: false,
                    };
                  }),
                );

                if (manipulate.length === listType.length) {
                  await setType([...manipulate]);
                  await setIsErrorType(false);
                  await setIsLoadingType(false);
                } else {
                  throw new Error('Failed manipulated data Type Filter');
                }
              }
            }
          })
          .catch(error => {
            console.log('Error Type: ', error);
            setIsErrorType(true);
            setIsLoadingType(false);
          });
      }
    } catch (error) {
      setIsErrorType(true);
      setIsLoadingType(false);
    }
  };

  const fetchFlowerColor = async (reduxData, floristCategory) => {
    try {
      if (reduxData) {
        await setColor([...reduxData]);
        await setIsErrorColor(false);
        await setIsLoadingColor(false);
      } else {
        client
          .query({
            query: FloristColors,
            variables: {
              merchantdId: parseInt(id, 10),
              floristCategory:
                floristCategory?.length !== 0 ? floristCategory[0] : null,
            },
            ssr: false,
            fetchPolicy: 'no-cache',
          })
          .then(async response => {
            console.log('Response Color: ', response);
            const {data, errors} = response;
            const {floristSpecColor} = data;
            const {data: listColor, error} = floristSpecColor;

            if (errors) {
              await setIsErrorColor(true);
              await setIsLoadingColor(false);
            } else {
              if (error) {
                await setIsErrorColor(true);
                await setIsLoadingColor(false);
              } else {
                const manipulate = await Promise.all(
                  listColor.map((d, i) => {
                    return {
                      ...d,
                      selected: false,
                    };
                  }),
                );

                if (manipulate.length === listColor.length) {
                  await setColor([...manipulate]);
                  await setIsErrorColor(false);
                  await setIsLoadingColor(false);
                } else {
                  throw new Error('Failed manipulated data categories Filter');
                }
              }
            }
          })
          .catch(error => {
            console.log('Error Color: ', error);
            setIsErrorColor(true);
            setIsLoadingColor(false);
          });
      }
    } catch (error) {
      console.log('Error Color: ', error);
      setIsErrorColor(true);
      setIsLoadingColor(false);
    }
  };

  const goBack = async () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const reset = async () => {
    try {
      await setIsLoadingReset(true);
      await filterStatus(false);
      await filterPrice([0.0, 0.0]);
      await filterSize([0.0, 0.0]);
      await filterType([]);
      await filterColor([]);
      setTimeout(async () => {
        await setIsLoadingReset(false);
        navigation.goBack(null);
      }, 1500);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const submitFilter = async () => {
    try {
      await setIsLoadingSubmitFilter(true);
      await filterStatus(true);
      await filterPrice(valuePrice);
      await filterSize(valueSize);
      await filterType(type);
      await filterColor(color);
      await filterTheme(themes);
      await setTimeout(async () => {
        await setIsLoadingSubmitFilter(false);
        navigation.goBack(null);
      }, 1500);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Container>
      <Headers
        title="Filter"
        goBack={goBack}
        reset={reset}
        isLoading={isLoadingReset}
      />
      <Content>
        <Card transparent>
          <CustomSlider
            isLoading={isLoadingPrice}
            isError={isErrorPrice}
            title={'Price (SGD)'}
            min={0.0}
            max={maxPrice === 0.0 ? 500.0 : maxPrice}
            step={0.1}
            enabledOne={false}
            selectedStyle={{
              backgroundColor: mainRed,
            }}
            unselectedStyle={{
              backgroundColor: headerBorderBottom,
            }}
            customMarker={() => {
              return (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: white,
                    width: 25,
                    height: 25,
                    backgroundColor: mainRed,
                    borderRadius: 25 / 2,
                  }}
                />
              );
            }}
            trackStyle={{backgroundColor: headerBorderBottom}}
            minMarkerOverlapDistance={5}
            values={valuePrice}
            allowOverlap={true}
            sliderLength={width / 1.16}
            onValuesChange={e => setValuePrice([...e])}
            decimal={true}
          />
          {/* Inches Size */}
          <CustomSlider
            isLoading={isLoadingSize}
            isError={isErrorSize}
            title={'Size (Inches)'}
            min={0.0}
            max={maxSize === 0.0 ? 100.0 : maxSize}
            step={0.1}
            enabledOne={false}
            selectedStyle={{
              backgroundColor: mainRed,
            }}
            unselectedStyle={{
              backgroundColor: headerBorderBottom,
            }}
            customMarker={() => {
              return (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: white,
                    width: 25,
                    height: 25,
                    backgroundColor: mainRed,
                    borderRadius: 25 / 2,
                  }}
                />
              );
            }}
            trackStyle={{backgroundColor: headerBorderBottom}}
            minMarkerOverlapDistance={5}
            values={valueSize}
            allowOverlap={true}
            sliderLength={width / 1.16}
            onValuesChange={e => setValueSize([...e])}
            decimal={true}
          />
        </Card>
        <ListFlowerType
          type={type}
          isLoadingType={isLoadingType}
          isErrorType={isErrorType}
          onSelectItem={index => {
            let oldData = type;
            oldData[index].selected = !type[index].selected;
            setType([...oldData]);
          }}
        />
        <ListFlowerColor
          color={color}
          isLoadingColor={isLoadingColor}
          isErrorColor={isErrorColor}
          onSelectItem={index => {
            let oldData = color;
            oldData[index].selected = !color[index].selected;
            setColor([...oldData]);
          }}
        />
        <ListFlowerThemes
          themes={themes}
          isLoadingThemes={isLoadingThemes}
          isErrorThemes={isErrorThemes}
          onSelectItem={index => {
            let oldData = themes;
            oldData[index].selected = !themes[index].selected;
            setThemes([...oldData]);
          }}
        />
      </Content>
      <FooterButton
        onPress={() => {
          submitFilter();
        }}
        isLoading={isLoadingSubmitFilter}
      />
    </Container>
  );
};

export const FooterButton = props => {
  const {onPress, isLoading} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen, width: '100%'}}>
        <View style={{width: '100%', backgroundColor: mainGreen}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: regular,
                letterSpacing: 0.3,
              }}>
              APPLY FILTER
            </Text>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                color={white}
                style={{marginLeft: 10}}
              />
            ) : null}
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const ListFlowerThemes = props => {
  const {themes, isLoadingThemes, isErrorThemes, onSelectItem} = props;

  const keyExt = item => `${item.name}`;

  if (isLoadingThemes && !isErrorThemes) {
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Theme
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View style={{width: width / 10, height: height / 15}}>
                <Loader />
              </View>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  } else if (!isLoadingThemes && isErrorThemes) {
    return null;
  } else {
    if (themes?.length === 0) {
      return null;
    }
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Theme
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                You may select more than one
              </Text>
            </View>
          </View>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexWrap: 'wrap',
            flexDirection: 'row',
          }}>
          <FlatList
            data={themes}
            extraData={themes}
            keyExtractor={keyExt}
            legacyImplementation
            scrollEnabled={false}
            // horizontal
            numColumns={20}
            showsHorizontalScrollIndicator={false}
            columnWrapperStyle={{flexWrap: 'wrap'}}
            contentContainerStyle={{
              // borderWidth: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              width: '100%',
            }}
            renderItem={({item, index}) => {
              if (item?.selected) {
                return (
                  <ButtonFiltered
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              } else {
                return (
                  <ButtonUnFilter
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              }
            }}
          />
        </CardItem>
      </Card>
    );
  }
};

export const ListFlowerType = props => {
  const {type, isLoadingType, isErrorType, onSelectItem} = props;

  const keyExt = (item, index) => `${index} Type`;

  if (isLoadingType && !isErrorType) {
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Type
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View style={{width: width / 10, height: height / 15}}>
                <Loader />
              </View>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  } else if (!isLoadingType && isErrorType) {
    return null;
  } else {
    if (type?.length === 0) {
      return null;
    }
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Type
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                You may select more than one
              </Text>
            </View>
          </View>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexWrap: 'wrap',
            flexDirection: 'row',
          }}>
          <FlatList
            data={type}
            extraData={type}
            keyExtractor={keyExt}
            legacyImplementation
            scrollEnabled={false}
            // horizontal
            numColumns={20}
            showsHorizontalScrollIndicator={false}
            columnWrapperStyle={{flexWrap: 'wrap'}}
            contentContainerStyle={{
              // borderWidth: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              width: '100%',
            }}
            renderItem={({item, index}) => {
              if (item?.selected) {
                return (
                  <ButtonFiltered
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              } else {
                return (
                  <ButtonUnFilter
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              }
            }}
          />
        </CardItem>
      </Card>
    );
  }
};

export const ListFlowerColor = props => {
  const {color, isLoadingColor, isErrorColor, onSelectItem} = props;

  const keyExt = (item, index) => `${index} Type`;

  if (isLoadingColor && !isErrorColor) {
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Colors
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View style={{width: width / 10, height: height / 15}}>
                <Loader />
              </View>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  } else if (!isLoadingColor && isErrorColor) {
    return null;
  } else {
    if (color?.length === 0) {
      return null;
    }
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Flower Colors
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                You may select more than one
              </Text>
            </View>
          </View>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexWrap: 'wrap',
            flexDirection: 'row',
          }}>
          <FlatList
            data={color}
            extraData={color}
            keyExtractor={keyExt}
            legacyImplementation
            scrollEnabled={false}
            // horizontal
            numColumns={20}
            showsHorizontalScrollIndicator={false}
            columnWrapperStyle={{flexWrap: 'wrap'}}
            contentContainerStyle={{
              // borderWidth: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              width: '100%',
            }}
            renderItem={({item, index}) => {
              if (item?.selected) {
                return (
                  <ButtonFiltered
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              } else {
                return (
                  <ButtonUnFilter
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              }
            }}
          />
        </CardItem>
      </Card>
    );
  }
};

export const CustomSlider = props => {
  console.log('CUSTOM SLIDER PROPS: ', props);
  const {
    isLoading,
    isError,
    title,
    min,
    max,
    step,
    enabledOne,
    selectedStyle,
    unselectedStyle,
    customMarker,
    trackStyle,
    minMarkerOverlapDistance,
    values,
    allowOverlap,
    sliderLength,
    onValuesChange,
    decimal,
  } = props;

  if (isError) {
    return null;
  } else {
    return (
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                }}>
                {title}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              {isLoading ? (
                <View style={{width: width / 10, height: height / 15}}>
                  <Loader />
                </View>
              ) : null}
            </View>
          </View>
          {isLoading ? null : (
            <View style={{width: '100%', paddingLeft: 10, marginTop: 10}}>
              <View>
                <MultiSlider
                  min={Number(min)}
                  max={Number(max)}
                  step={step}
                  enabledOne={enabledOne}
                  selectedStyle={{
                    ...selectedStyle,
                  }}
                  unselectedStyle={{
                    ...unselectedStyle,
                  }}
                  customMarker={() => customMarker()}
                  trackStyle={{...trackStyle}}
                  minMarkerOverlapDistance={minMarkerOverlapDistance}
                  values={values}
                  snapped
                  allowOverlap={allowOverlap}
                  sliderLength={sliderLength}
                  onValuesChange={e => {
                    onValuesChange(e);
                  }}
                />
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      right: 8,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                    }}>
                    {decimal ? values[0].toFixed(1) : values[0]}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                    }}>
                    {decimal ? values[1].toFixed(1) : values[1]}
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>
      </CardItem>
    );
  }
};

const mapToState = state => {
  const {florist} = state;
  return {
    ...florist,
  };
};

const mapToDispatch = dispatch => {
  return {
    filterPrice: price => dispatch(FilterPriceRedux(price)),
    filterSize: size => dispatch(FilterSizeRedux(size)),
    filterCategories: (categories, selectedCategories) =>
      dispatch(FilterCategoriesRedux(categories, selectedCategories)),
    filterType: type => dispatch(FilterTypeRedux(type)),
    filterColor: color => dispatch(FilterColorRedux(color)),
    filterStatus: status => dispatch(FilterStatusRedux(status)),
    filterTheme: themes => dispatch(FilterThemeRedux(themes)),
  };
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(FloristFilter);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
