import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  View,
  Dimensions,
  Platform,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Footer,
  FooterTab,
  Button,
} from 'native-base';
import Loader from '../../../components/Loader/LoaderWithContainer';
import MutationAddToCard from '../../../graphql/mutations/addtoCart';
import CHECK_IS_DIFFERENT_MERCHANT from '../../../graphql/queries/FloristProductCartValidation';

const {regular, small} = FontSize;
const {book, medium} = FontType;
const {
  white,
  transparent,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  black,
  overlayDim,
} = Colors;
const {width, height} = Dimensions.get('window');

export const Headers = props => {
  const {goBack, title} = props;

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          transparent
          style={{
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Customize = props => {
  console.log('Customize Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {data} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [floristId, setFloristId] = useState(null);
  const [floristName, setFloristName] = useState('');
  const [floristPrice, setFloristPrice] = useState(0);
  const [floristExtraSize, setFloristExtraSize] = useState([]);
  const [floristQty, setFloristQty] = useState(1);
  const [totalPrice, setTotalPrice] = useState(0);
  const [selectedExtraSizeIndex, setSelectedExtraSizeIndex] = useState(0);
  const [isLoadingSubmitToCart, setIsLoadingSubmitToCart] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [message, setmessage] = useState('');

  const [showModalDiffMerchant, setShowModalDiffMerchant] = useState(false); // check if in cart already merchant and currently user want to add product that has different merchant from last in cart
  const [loadingDiffMerchant, setLoadingDiffMerchant] = useState(false);
  const [errorDiffMerchant, setErrorDiffMerchant] = useState(false);
  const [checkingResult, setCheckingResult] = useState(false);

  useEffect(() => {
    fetch();
  }, []);

  const checkingDiffMerchant = async () => {
    try {
      await setShowModalDiffMerchant(true);
      await setLoadingDiffMerchant(true);
      await client
        .query({
          query: CHECK_IS_DIFFERENT_MERCHANT,
          variables: {
            productId: data?.id,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetch cart Validation: ', response);
          const {data: datas, errors} = response;
          const {productCartValidation} = datas;
          const {validationStatus, error} = productCartValidation;

          if (errors) {
            throw new Error('Error Bro');
          } else {
            if (error) {
              throw new Error('Error Bro');
            } else {
              if (validationStatus) {
                // the same merchant -> user can continue
                await setCheckingResult(validationStatus);
                await setErrorDiffMerchant(false);
                await setLoadingDiffMerchant(false);
                await setShowModalDiffMerchant(false);
                await submitToCartAPI();
              } else {
                // different merchant, show modal alert if the cart will be replaced if continue by user
                await setCheckingResult(validationStatus);
                await setErrorDiffMerchant(false);
                await setLoadingDiffMerchant(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      await setErrorDiffMerchant(true);
      await setLoadingDiffMerchant(false);

      await setTimeout(async () => {
        await setErrorDiffMerchant(false);
        await setLoadingDiffMerchant(false);
      }, 1000);
    }
  };

  const manipulateExtraSize = extraSize => {
    return new Promise(async (resolve, reject) => {
      try {
        const manipulated = await Promise.all(
          extraSize.map((d, i) => {
            return {
              id: (i += 1),
              apiId: d?.id ? d.id : null,
              price: d.price,
              size: d.size,
              selected: false,
            };
          }),
        );

        if (manipulated.length === extraSize.length) {
          resolve([...manipulated]);
        } else {
          resolve([]);
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  const fetch = async () => {
    try {
      if (data) {
        const {
          id,
          name,
          floristSize,
          floristPrice: price,
          floristExtraSize: extraSize,
        } = data;
        const actualExtraSize = await manipulateExtraSize(extraSize);
        await setFloristId(id);
        await setFloristName(name);
        await setFloristExtraSize([
          {
            id: 1,
            price,
            size: floristSize,
            selected: true,
          },
          ...actualExtraSize,
        ]);
        await setFloristPrice(price);
        await setTotalPrice(price);
        await setTimeout(async () => {
          await setIsError(false);
          await setIsLoading(false);
        }, 1500);
      } else {
        await setIsError(true);
        await setIsLoading(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onChangeSelected = async index => {
    try {
      const manipulated = await Promise.all(
        floristExtraSize.map((d, i) => {
          if (index === i) {
            return {
              ...d,
              selected: true,
            };
          } else {
            return {
              ...d,
              selected: false,
            };
          }
        }),
      );

      if (manipulated.length === floristExtraSize.length) {
        await setFloristExtraSize([...manipulated]);
        await setSelectedExtraSizeIndex(index);
        if (index === 0) {
          const total = floristPrice * floristQty;
          await setTotalPrice(total);
        } else {
          const total =
            (floristPrice + floristExtraSize[index].price) * floristQty;
          await setTotalPrice(total);
        }
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const minusQTY = async () => {
    try {
      let oldQTY = floristQty;
      let newQTY = oldQTY - 1;
      if (newQTY === 0) {
      } else {
        let total =
          selectedExtraSizeIndex === 0
            ? floristPrice * newQTY
            : (floristPrice + floristExtraSize[selectedExtraSizeIndex].price) *
              newQTY;
        await setFloristQty(newQTY);
        await setTotalPrice(total);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const plusQTY = async () => {
    try {
      let oldQTY = floristQty;
      let newQTY = oldQTY + 1;
      let total =
        selectedExtraSizeIndex === 0
          ? floristPrice * newQTY
          : (floristPrice + floristExtraSize[selectedExtraSizeIndex].price) *
            newQTY;
      await setFloristQty(newQTY);
      await setTotalPrice(total);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onChange = async index => {
    try {
      await onChangeSelected(index);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const submitToCartAPI = async () => {
    try {
      console.log('ProductId: ', floristId);
      console.log('floristQty: ', floristQty);
      console.log(
        'Florist Extra Size: ',
        floristExtraSize[selectedExtraSizeIndex],
      );
      console.log('Index Selected: ', selectedExtraSizeIndex);
      await setIsLoadingSubmitToCart(true);
      await setmessage('');
      await setIsSuccess(false);
      await setIsFailed(false);
      const variables =
        selectedExtraSizeIndex === 0
          ? {
              productId: parseInt(floristId, 10),
              qty: parseInt(floristQty, 10),
            }
          : {
              productId: parseInt(floristId, 10),
              qty: parseInt(floristQty, 10),
              options: {
                selectedExtraSize: parseInt(
                  floristExtraSize[selectedExtraSizeIndex].apiId,
                  10,
                ),
              },
            };
      console.log('variables: ', variables);
      client
        .mutate({
          mutation: MutationAddToCard,
          variables,
        })
        .then(async response => {
          console.log('Response Add to Cart: ', response);
          const {data: addCartAPIData, errors} = response;
          const {addToCart} = addCartAPIData;
          const {data: resp, error} = addToCart;

          if (errors) {
            await setIsFailed(true);
            await setmessage('Failed add into your cart');
            await setTimeout(async () => {
              await setIsFailed(false);
              await setIsLoadingSubmitToCart(false);
              await setmessage('');
            }, 1000);
          } else {
            if (error) {
              await setIsFailed(true);
              await setmessage(
                error === 'Insufficient product quantity available!'
                  ? 'Insufficient product quantity available!'
                  : 'Failed add into your cart',
              );
              await setTimeout(async () => {
                await setIsFailed(false);
                await setIsLoadingSubmitToCart(false);
              }, 1000);
            } else {
              await setIsSuccess(true);
              await setTimeout(async () => {
                await setIsSuccess(false);
                await setIsLoadingSubmitToCart(false);
                await setmessage('');
                navigation.goBack(null);
              }, 1000);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsFailed(true);
          await setmessage('Failed add into your cart');
          await setTimeout(async () => {
            await setIsFailed(false);
            await setIsLoadingSubmitToCart(false);
            await setmessage('');
          }, 1000);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsFailed(true);
      await setmessage('Failed add into your cart');
      await setTimeout(async () => {
        await setIsFailed(false);
        await setIsLoadingSubmitToCart(false);
        await setmessage('');
      }, 1000);
    }
  };

  if (isLoading && !isError) {
    return <Loader />;
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers title="Customize" goBack={goBack} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            onPress={() => goBack()}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: mainGreen,
              elevation: 0,
              shadowOpacity: 0,
              paddingLeft: 10,
              paddingRight: 10,
              borderRadius: 4,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2),
                color: white,
                letterSpacing: 0.3,
              }}>
              Failed to Load - Go to Previous Screen
            </Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    return (
      <Container style={{backgroundColor: white}}>
        <ModalDiffMerchant
          checkingResult={checkingResult}
          isLoading={loadingDiffMerchant}
          isError={errorDiffMerchant}
          visible={showModalDiffMerchant}
          next={async () => {
            await setLoadingDiffMerchant(false);
            await setErrorDiffMerchant(false);
            await setShowModalDiffMerchant(false);
            await submitToCartAPI();
          }}
          cancel={async () => {
            try {
              await setErrorDiffMerchant(false);
              await setLoadingDiffMerchant(false);
              await setShowModalDiffMerchant(false);
            } catch (error) {
              await setErrorDiffMerchant(false);
              await setLoadingDiffMerchant(false);
              await setShowModalDiffMerchant(false);
            }
          }}
        />
        <Headers title="Customize" goBack={goBack} />
        {isLoadingSubmitToCart ? (
          <View
            style={{
              zIndex: 99,
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {!isSuccess && !isFailed ? (
              <View
                style={{
                  width: width / 5,
                  height: height / 10,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator size="large" style={{color: mainGreen}} />
              </View>
            ) : isSuccess ? (
              <View
                style={{
                  // width: width / 5,
                  height: height / 8,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Icon
                  type="Feather"
                  name="check"
                  style={{color: 'green', fontSize: 35}}
                />
                <Text
                  style={{
                    marginVertical: 10,
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: mainGreen,
                    letterSpacing: 0.3,
                    textAlign: 'center',
                  }}>
                  Succesfully add into your cart
                </Text>
              </View>
            ) : (
              <View
                style={{
                  // width: width / 5,
                  height: height / 8,
                  borderRadius: 15,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{color: mainRed, fontSize: 35}}
                />
                <Text
                  style={{
                    marginVertical: 10,
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: mainGreen,
                    letterSpacing: 0.3,
                    textAlign: 'center',
                  }}>
                  {message}
                </Text>
              </View>
            )}
          </View>
        ) : null}
        <Content contentContainerStyle={{flex: 1}}>
          <TopContent floristName={floristName} floristPrice={floristPrice} />
          <Card transparent>
            <CardItem style={{width: '100%'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    textAlign: 'left',
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  Size
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    textAlign: 'right',
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: greyLine,
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  Require* Select 1
                </Text>
              </View>
            </CardItem>
            <FlatList
              scrollEnabled={false}
              data={floristExtraSize}
              extraData={floristExtraSize}
              keyExtractor={(item, index) => `${index}`}
              renderItem={({item, index}) => {
                return (
                  <Card
                    transparent
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      marginTop: 0,
                      marginBottom: 0,
                    }}>
                    <CardItem
                      button
                      onPress={() => onChange(index)}
                      style={{width: '100%', paddingBottom: 0}}>
                      <View style={{width: '100%', flexDirection: 'column'}}>
                        <View style={{width: '100%', flexDirection: 'row'}}>
                          <View style={{flex: 0.15}}>
                            <IconSelected selected={item.selected} />
                          </View>
                          <View style={{flex: 0.5}}>
                            <Text
                              style={{
                                fontFamily: book,
                                fontSize: RFPercentage(1.8),
                                color: black,
                                letterSpacing: 0.3,
                                lineHeight: 18,
                              }}>
                              {item?.size ? `${item.size} Inches` : 'N/A'}
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              alignItems: 'flex-end',
                            }}>
                            <Text
                              style={{
                                fontFamily: medium,
                                fontSize: RFPercentage(1.8),
                                color: black,
                                letterSpacing: 0.3,
                                lineHeight: 18,
                              }}>
                              {index === 0
                                ? 'FREE'
                                : item?.price
                                ? `+ $${item.price.toFixed(2)}`
                                : 'N/A'}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            borderColor: headerBorderBottom,
                            marginTop: 10,
                            width: '100%',
                            borderBottomWidth: 1,
                            height: 0.5,
                          }}
                        />
                      </View>
                    </CardItem>
                  </Card>
                );
              }}
            />
          </Card>
          <Card
            style={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
              marginLeft: 0,
              marginRight: 0,
              borderBottomWidth: 0,
              elevation: 0,
              shadowOpacity: 0,
              borderTopColor: headerBorderBottom,
              borderTopWidth: 1,
            }}>
            <CardItem style={{width: '100%'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                  }}>
                  Item Quanity
                </Text>
              </View>
              <View
                style={{
                  flex: 0.7,
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <Button
                  onPress={() => minusQTY()}
                  disabled={floristQty === 1 ? true : false}
                  transparent
                  style={{
                    alignSelf: 'center',
                    paddingTop: 0,
                    paddingBottom: 0,
                    height: 35,
                    width: 35,
                    justifyContent: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="minus"
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: black,
                    }}
                  />
                </Button>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                    }}>
                    {floristQty}
                  </Text>
                </View>
                <Button
                  onPress={() => plusQTY()}
                  transparent
                  style={{
                    alignSelf: 'center',
                    paddingTop: 0,
                    paddingBottom: 0,
                    height: 35,
                    width: 35,
                    justifyContent: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="plus"
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: black,
                    }}
                  />
                </Button>
              </View>
            </CardItem>
          </Card>
        </Content>
        <ButtonAddToCart
          disabled={isLoadingSubmitToCart ? true : false}
          isLoading={isLoadingSubmitToCart}
          totalPrice={totalPrice}
          title={'ADD TO CART'}
          onPress={() => {
            // submitToCartAPI()
            checkingDiffMerchant();
          }}
        />
      </Container>
    );
  }
};

export const ModalDiffMerchant = props => {
  const {visible, next, cancel, isLoading, isError, checkingResult} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 30,
        }}>
        <>
          {isLoading ? (
            <View
              style={{
                width: width / 5,
                height: height / 10,
                borderRadius: 15,
                backgroundColor: white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator size="large" style={{color: mainGreen}} />
            </View>
          ) : !isLoading && !checkingResult ? (
            <View
              style={{width: '100%', backgroundColor: white, minHeight: 100}}>
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 15,
                  paddingTop: 20,
                  paddingBottom: 15,
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    lineHeight: 20,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Are you sure want to add this product into your cart ?
                  currently in your cart has product from another merchant, that
                  might be replaced by this product if you press YES.
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: 15,
                }}>
                <TouchableOpacity
                  onPress={cancel}
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: mainRed,
                    borderRadius: 4,
                    padding: 15,
                    paddingLeft: 10,
                    paddingRight: 10,
                    marginHorizontal: 5,
                  }}>
                  <Text style={{color: white}}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={next}
                  style={{
                    flex: 1,
                    marginHorizontal: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: white,
                    borderColor: mainRed,
                    borderWidth: 1.5,
                    borderRadius: 4,
                    padding: 15,
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text style={{color: mainRed}}>Yes, replace it!</Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </>
      </View>
    </Modal>
  );
};

export const ButtonAddToCart = props => {
  const {onPress, title, totalPrice, disabled, isLoading} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            disabled={disabled}
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
            <Text
              style={{
                marginHorizontal: 5,
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              -
            </Text>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              ${totalPrice}
            </Text>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                style={{color: white, marginLeft: 10}}
              />
            ) : null}
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const IconSelected = props => {
  const {selected} = props;
  return (
    <View
      style={{
        width: 20,
        height: 20,
        borderWidth: 2,
        borderRadius: 20 / 2,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {selected ? (
        <View
          style={{
            width: 10,
            height: 10,
            borderWidth: 1,
            borderRadius: 10 / 2,
            backgroundColor: black,
          }}
        />
      ) : null}
    </View>
  );
};

export const TopContent = props => {
  const {floristName, floristPrice} = props;

  return (
    <Card
      style={{
        marginTop: 0,
        borderTopWidth: 0,
        marginLeft: 0,
        marginRight: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <CardItem style={{width: '100%'}}>
        <View style={{flex: 1, paddingTop: 15, paddingBottom: 15}}>
          <Text
            style={{
              textAlign: 'left',
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            {floristName}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              textAlign: 'right',
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            ${floristPrice.toFixed(2)}
          </Text>
        </View>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(Customize);

export default props => <Wrapper {...props} />;
