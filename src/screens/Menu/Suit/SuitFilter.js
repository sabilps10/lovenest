import React, {useCallback, useEffect, useState} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Button,
  Content,
  Card,
  CardItem,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import {connect} from 'react-redux';

import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Components
import Headers from './Components/HeaderFilter';
import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFiltered from '../../../components/Button/ButtonFilters/Unfilter';
import ButtonSubmit from './Components/ButtonEnquiry';

// Redux Bridal Suit Filter
import ReduxSuitFilter from '../../../redux/thunk/Suit/index';
import ReduxSuitReset from '../../../redux/thunk/Suit/reset';

// Query
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';

const {
  white,
  overlayDim,
  black,
  greyLine,
  headerBorderBottom,
  mainGreen,
  mainRed,
} = Colors;
const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;

const SuitFilter = props => {
  const {navigation, client, route} = props;
  const serviceType = 'Bridal';

  const [showModal, setShowModal] = useState(false);

  const [filterList, setFilterList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  useEffect(() => {
    fetchWithRedux();

    const subs = navigation.addListener('focus', () => {
      fetchWithRedux();
    });

    return () => {
      subs();
    };
  }, [navigation]);

  const fetchWithRedux = async () => {
    try {
      if (props?.list?.length === 0) {
        await fetchFilter();
      } else {
        await setFilterList([...props?.list]);
        await setIsError(false);
        await setIsLoading(false);
      }
    } catch (error) {
      console.log('Error fetchWithRedux: ', error);
      await isError(true);
      await setIsLoading(false);
    }
  };

  const fetchFilter = async () => {
    try {
      await client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            serviceType,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response Fetch Filter Suit: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: filters, error} = productsFilter;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setFilterList([...filters]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  const checkList = async (parentIndex, index) => {
    let oldData = filterList;
    oldData[parentIndex].list[index].checked =
      !filterList[parentIndex].list[index].checked;
    await setFilterList([...oldData]);

    if (filterList[parentIndex].name === 'Type') {
      let tempSelectedArray = selectedType;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedType(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Category') {
      let tempSelectedArray = selectedCategory;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedCategory(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Color') {
      let tempSelectedArray = selectedColor;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedColor(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Range') {
      let tempSelectedArray = selectedRange;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedRange(tempSelectedArray);
    }
  };

  const onPress = async () => {
    try {
      console.log('Apply Filter Suit: ', {
        filterList,
      });
      await setShowModal(true);
      await props?.reduxSuitFilter([...filterList]);

      await setTimeout(async () => {
        await setShowModal(false);
        await props?.navigation?.goBack(null);
      }, 2000);
    } catch (error) {
      console.log('Error: ', error);
      await setShowModal(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalLoader visible={showModal} />
      <Headers
        goBack={() => {
          try {
            props?.navigation?.goBack(null);
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
        goToReset={async () => {
          try {
            await props?.reduxSuitReset();
            await navigation.goBack(null);
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
      />
      <ScrollView>
        <ListFilter
          checkList={(p, i) => checkList(p, i)}
          isLoading={isLoading}
          isError={isError}
          list={filterList}
        />
      </ScrollView>
      <ButtonSubmit title={'APPLY FILTER'} onPress={() => onPress()} />
    </View>
  );
};

export const ListFilterContent = props => {
  const {list, parentIndex, checkList} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${item.index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      const capitalParse = item?.name.toUpperCase();
      const gownIsIncluded = capitalParse.includes('GOWN');
      if (item?.checked) {
        if (item?.name === 'Gown' || gownIsIncluded) {
          return null;
        } else {
          return (
            <ButtonFiltered
              item={item}
              name={item?.name}
              index={index}
              checked={item?.checked}
              parentIndex={parentIndex}
              checkList={(p, i) => {
                console.log('Check List Function: ', {p, i});
                checkList(p, i);
              }}
              {...props}
            />
          );
        }
      } else {
        if (item?.name === 'Gown' || gownIsIncluded) {
          return null;
        } else {
          return (
            <ButtonUnFiltered
              item={item}
              name={item?.name}
              index={index}
              checked={item?.checked}
              parentIndex={parentIndex}
              checkList={(p, i) => {
                console.log('Check List Function: ', {p, i});
                checkList(p, i);
              }}
              {...props}
            />
          );
        }
      }
    },
    [list],
  );

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <FlatList
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderItem}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          width: '100%',
        }}
      />
    );
  }
};

export const ListFilter = props => {
  const {list, isLoading, isError, checkList} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${item.name}`;
    },
    [list],
  );

  const renderList = useCallback(
    ({item, index}) => {
      if (item?.name === 'Type') {
        return null;
      } else {
        return (
          <Card transparent>
            <CardItem style={{width}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 1.5,
                  paddingRight: 1.5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  {item?.name}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingLeft: 1.5,
                  paddingRight: 0.5,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.4),
                    color: greyLine,
                    letterSpacing: 0.3,
                  }}>
                  You may select more than one
                </Text>
              </View>
            </CardItem>
            <CardItem>
              <ListFilterContent
                list={item?.list}
                parentIndex={index}
                checkList={(p, i) => checkList(p, i)}
              />
            </CardItem>
          </Card>
        );
      }
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <View
        style={{
          paddingTop: 15,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color={mainGreen} size="large" />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <FlatList
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderList}
      />
    );
  }
};

export const ModalLoader = props => {
  const {visible} = props;
  return (
    <Modal visible={visible} animationType="fade" transparent>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginBottom: 10}}
          />
          <Text style={{color: greyLine, fontStyle: 'italic'}}>
            Updating...
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const mapToState = state => {
  console.log('mapToState: >> ', state);
  const {suitFilterList} = state;
  return {
    list: suitFilterList?.list,
  };
};

const mapToDispatch = dispatch => {
  console.log('mapToDispatch: >> ', dispatch);
  return {
    reduxSuitFilter: list => dispatch(ReduxSuitFilter(list)),
    reduxSuitReset: () => dispatch(ReduxSuitReset()),
  };
};

const Connector = connect(mapToState, mapToDispatch)(SuitFilter);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
