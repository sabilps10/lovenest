import React from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  View,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions;
const {white, mainGreen} = Colors;
const {book, medium} = FontType;

const Components = props => {
  const {onPress, title} = props;
  return (
    <TouchableOpacity
      onPress={() => {
        try {
          onPress();
        } catch (error) {
          console.log('Error: ', error);
        }
      }}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width,
        height: hasNotch() ? 60 : 55,
        backgroundColor: mainGreen,
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: white,
        }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(Components);

export default props => <Wrapper {...props} />;
