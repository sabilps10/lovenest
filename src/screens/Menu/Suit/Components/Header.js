import React from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {charImage} from '../../../../utils/Themes/Images';

const {charFilterIcon} = charImage;
const {black, headerBorderBottom, white} = Colors;
const {medium} = FontType;

const Headers = props => {
  const {goBack, goToFilter, goToHome} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        zIndex: 999,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
      }}>
      <Left style={{flex: 0.3}}>
        <TouchableOpacity onPress={goBack} style={{padding: 5}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(3.5), color: black}}
          />
        </TouchableOpacity>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(2),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          Suit
        </Text>
      </Body>
      <Right
        style={{
          flex: 0.3,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={goToFilter} style={{padding: 5}}>
          <Image
            source={charFilterIcon}
            style={{width: 20, height: 20}}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={goToHome} style={{padding: 5}}>
          <Icon
            type="Feather"
            name="home"
            style={{fontSize: RFPercentage(2.8), color: black}}
          />
        </TouchableOpacity>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(Headers);

export default props => <Wrapper {...props} />;
