import React, {useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import Colors from '../../../../utils//Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem, Icon} from 'native-base';

const {width, height} = Dimensions.get('window');
const {white, black, greyLine, mainRed, headerBorderBottom} = Colors;
const {book, medium} = FontType;

const AnimatedCard = new Animated.createAnimatedComponent(Card);
const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);

const CardProduct = props => {
  const {item, index, isLogin, runWishList, onPress} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  return (
    <Animated.View
      style={{
        flex: 1 / 2,
        padding: 5,
        paddingLeft: 2.5,
        paddingRight: 2.5,
      }}>
      <AnimatedTouch
        onPress={() => {
          onPress(item?.id);
        }}
        activeOpacity={1}
        onPressIn={pressIn}
        onPressOut={pressOut}>
        <AnimatedCard
          transparent
          style={{
            marginTop: 0,
            marginBottom: 0,
            shadowOpacity: 0,
            elevation: 0,
            borderColor: greyLine,
            borderWidth: 1,
            borderRadius: 2,
            transform: [{scale}],
          }}>
          <CardItem cardBody>
            <AsyncImage
              source={
                item?.featuredImageDynamicURL
                  ? {uri: `${item?.featuredImageDynamicURL}=h300-pp`}
                  : {uri: `${item?.featuredImageURL}`}
              }
              placeholderColor={'white'}
              resizeMode="cover"
              style={{
                flex: 1,
                width: '100%',
                height: width * 0.65,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
            />
          </CardItem>
          <CardItem style={{width: '100%', paddingLeft: 10, paddingRight: 5}}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontFamily: book,
                  color: black,
                  fontSize: RFPercentage(1.8),
                  letterSpacing: 0.3,
                  lineHeight: 18,
                }}
                numberOfLines={2}>
                {item.name}
              </Text>
            </View>
            {isLogin ? (
              <View
                style={{
                  flex: 0.3,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    runWishList(item?.id, item?.isWishlist);
                  }}
                  style={{
                    padding: 2.5,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type={
                      item?.isWishlist ? 'MaterialCommunityIcons' : 'Feather'
                    }
                    name="heart"
                    style={{
                      left: 10,
                      fontSize: RFPercentage(2.2),
                      color: item?.isWishlist ? mainRed : greyLine,
                    }}
                  />
                </TouchableOpacity>
              </View>
            ) : null}
          </CardItem>
        </AnimatedCard>
      </AnimatedTouch>
    </Animated.View>
  );
};

const Wrapper = compose(withApollo)(CardProduct);

export default props => <Wrapper {...props} />;
