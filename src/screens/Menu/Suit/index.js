import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  Animated,
  ActivityIndicator,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Modal,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Container, Content, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';

// Async Token
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../utils/AsyncstorageDataStructure/index';

// Components
import Header from './Components/Header';
import CardProduct from './Components/CardProduct';
import ModalLoaderWishList from './Components/ModalLoaderWishList';

// Query
import GetSuit from '../../../graphql/queries/productsPublic';

// Mutation
import MutationAddWishList from '../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../graphql/mutations/removeWishlist';

// Redux
import ReduxSuitFilter from '../../../redux/thunk/Suit/index';
import ReduxResetFilter from '../../../redux/thunk/Suit/reset';

const {
  black,
  white,
  overlayDim,
  greyLine,
  headerBorderBottom,
  mainGreen,
  mainRed,
} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const {asyncToken} = AsyncData;

const Suit = props => {
  console.log('Props Suit: ', props);
  const {
    navigation,
    client,
    route,
    product,
    resetProductFilter,
    positionYBottomNav,
  } = props;

  const listRef = useRef(null);

  const [showModalRemoveFilter, setShowModalRemoveFilter] = useState(false);

  const [isLogin, setIsLogin] = useState(false);

  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [list, setList] = useState([]);
  const [totalCount, setTotalCount] = useState(0);

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const pageSize = 10;
  const [pageNumber, setPageNumber] = useState(1);

  useEffect(() => {
    StatusBar.setTranslucent(false);
    checkIsLogin();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }

    fetch();

    if (isLoadMore) {
      shouldFetch();
    }

    if (refreshing) {
      onRefresh();
    }

    const subsBlur = navigation.addListener('blur', () => {
      resetAll();
    });

    const subs = navigation.addListener('focus', () => {
      console.log('PROPS LITSSSS: ', props?.lists);
      checkIsLogin();
      if (props?.lists?.length === 0) {
        console.log('MASSOOOOKKKKKKK SINIII');
        fetch();
      } else {
        console.log('WUCAAAAAAAUUUUUUU');
        refreshAfterFilter();
      }
    });

    return () => {
      subs();
      subsBlur();
    };
  }, [props?.lists, isLoadMore, showModalRemoveFilter, refreshing]);

  const refreshAfterFilter = useCallback(async () => {
    await resetAll();
    await setPageNumber(1);
    await setTimeout(async () => {
      await fetch();
    }, 2000);
  }, [showModalRemoveFilter, list, pageNumber, props?.lists, resetAll]);

  const resetAll = async () => {
    await setIsLoading(true);
    await setList([]);
  };

  const shouldFetch = useCallback(async () => {
    await setPageNumber(pageNumber + 1);
    await fetch();
  }, [isLoadMore]);

  const manipulateSelectedFilterRedux = filterType => {
    return new Promise(async (resolve, reject) => {
      try {
        const getListSelected = await Promise.all(
          props?.lists
            ?.map((d, i) => {
              if (d?.name === filterType) {
                return d;
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );

        if (getListSelected) {
          // console.log('getListSelected >>> ', getListSelected);
          const getChecked = await Promise.all(
            getListSelected[0]?.list
              ?.map((d, i) => {
                if (d?.checked) {
                  return d?.name;
                } else {
                  return false;
                }
              })
              .filter(Boolean),
          );

          if (getChecked) {
            resolve(getChecked);
          } else {
            resolve([]);
          }
        } else {
          resolve([]);
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  const onRefresh = useCallback(async () => {
    await setRefreshing();
    await setIsLoading(true);
    await setList([]);
    await setPageNumber(1);
    await setTimeout(async () => {
      await fetch();
    }, 2000);
  }, [refreshing]);

  const fetch = async () => {
    try {
      const getCategory = await manipulateSelectedFilterRedux('Category');
      const getColor = await manipulateSelectedFilterRedux('Color');

      const variables = {
        serviceType: 'Bridal',
        bridalType: ['Suit'],
        bridalCategory: getCategory?.length === 0 ? [] : getCategory,
        bridalColor: getColor?.length === 0 ? [] : getColor,
        pageSize,
        pageNumber,
      };

      await client
        .query({
          query: GetSuit,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Res Suit: ', res);
          const {data, errors} = res;
          const {productsPublic} = data;
          const {data: listSuit, error, totalData} = productsPublic;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            await setTotalCount(totalData);
            const oldData = list.concat(listSuit);
            const filtering = await _.uniqBy(oldData, 'id');
            await setList([...filtering]);
            await setIsError(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const checkIsLogin = async () => {
    try {
      const userIsLogin = await checkToken();
      if (userIsLogin) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const checkToken = () => {
    return new Promise(async resolve => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);

        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const runWishList = async (productId, statusWhislist, oldlist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId, oldlist);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId, oldlist);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = (productId, oldList) => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              const manipulate = await Promise.all(
                oldList?.map((d, i) => {
                  if (d?.id === productId) {
                    return {
                      ...d,
                      isWishlist: !d?.isWishlist,
                    };
                  } else {
                    return {...d};
                  }
                }),
              );
              console.log('MANIPULATED: ', manipulate);
              if (manipulate) {
                await setList([...manipulate]);
                await setIsLoadingIsWishList(false);
              } else {
                await setIsLoadingIsWishList(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const removeFilter = async (parentId, index) => {
    try {
      console.log('Parent ID and Index: ', {parentId, index});
      await setShowModalRemoveFilter(true);
      let oldList = props?.lists;
      oldList[parentId].list[index].checked =
        !oldList[parentId].list[index].checked;
      console.log('OLD LISTS: ', oldList);
      await props?.reduxSuitFilter(oldList);
      await setTimeout(async () => {
        await refreshAfterFilter();
        await setShowModalRemoveFilter(false);
      }, 1000);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goBack = async () => {
    try {
      await props?.reduxSuitReset();
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToFilter = () => {
    try {
      navigation.navigate('SuitFilter', {serviceType: 'Bridal'});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToHome = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const scrollToTop = () => {
    try {
      listRef?.current?.scrollToOffset({animated: true, offset: 0});
    } catch (error) {}
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Header goBack={goBack} goToFilter={goToFilter} goToHome={goToHome} />
        <Content contentContainerStyle={{paddingTop: 15}}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size="large" color={mainGreen} />
          </View>
        </Content>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Header goBack={goBack} goToFilter={goToFilter} goToHome={goToHome} />
        <Content
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          contentContainerStyle={{paddingTop: 15}}>
          <View
            style={{
              flex: 1,
              height: width * 1.5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: headerBorderBottom,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Products Not Found
            </Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <Header goBack={goBack} goToFilter={goToFilter} goToHome={goToHome} />
        <ModalRemoveFilterLoader visible={showModalRemoveFilter} />
        <ModalLoaderWishList isLoading={isLoadingWishList} />
        <ListFilteredData removeFilter={removeFilter} {...props} />
        <View style={{flex: 1, zIndex: -99}}>
          <ListSuit
            isLoading={isLoading}
            isError={isError}
            listRef={listRef}
            isLogin={isLogin}
            list={list}
            navigation={props?.navigation}
            runWishList={(productId, statusWhislist, oldList) => {
              runWishList(productId, statusWhislist, oldList);
            }}
            refreshing={refreshing}
            onRefresh={onRefresh}
            isLoadMore={isLoadMore}
            totalCount={totalCount}
            setIsLoadMore={async status => await setIsLoadMore(status)}
          />
        </View>
        <ButtonScrollToTop onPress={scrollToTop} />
      </View>
    );
  }
};

export const ButtonScrollToTop = props => {
  const {onPress} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 35,
        height: 35,
        position: 'absolute',
        bottom: 25,
        right: 25,
        borderRadius: 35 / 2,
        backgroundColor: white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <Icon
        type="Feather"
        name="arrow-up"
        style={{fontSize: RFPercentage(2.2), color: mainRed}}
      />
    </TouchableOpacity>
  );
};

export const Footer = props => {
  const {isLoadMore} = props;
  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // borderWidth: 1,
      }}>
      {isLoadMore ? (
        <ActivityIndicator
          size="small"
          color={mainGreen}
          style={{marginHorizontal: 5}}
        />
      ) : null}
      <Text
        style={{
          fontStyle: 'italic',
          fontSize: 12,
          color: greyLine,
          textAlign: 'center',
        }}>
        {isLoadMore ? 'Loading more...' : ''}
      </Text>
    </View>
  );
};

export const ModalRemoveFilterLoader = props => {
  const {visible} = props;
  return (
    <Modal visible={visible} animationType="fade" transparent>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: overlayDim,
        }}>
        <View
          style={{
            backgroundColor: white,
            padding: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginBottom: 10}}
          />
          <Text style={{color: greyLine, fontStyle: 'italic'}}>
            Removing...
          </Text>
        </View>
      </View>
    </Modal>
  );
};

export const ListSuit = props => {
  console.log('ListSuit >>>>> ', props);
  const {list, isLogin, runWishList, isLoading, isError} = props;

  const onPress = id => {
    try {
      props?.navigation?.push('BridalProductDetail', {
        id,
        menuName: 'SuitMenu',
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = useCallback(
    (item, index) => {
      return `${item.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <CardProduct
          item={item}
          index={index}
          isLogin={isLogin}
          runWishList={(id, isWishlist) => runWishList(id, isWishlist, list)}
          onPress={id => onPress(id)}
        />
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    <View
      style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color={mainGreen} />
    </View>;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0) {
      return (
        <View style={{flex: 1, backgroundColor: white}}>
          <Content
            refreshControl={
              <RefreshControl
                refreshing={props?.refreshing}
                onRefresh={() => props?.onRefresh()}
              />
            }
            contentContainerStyle={{paddingTop: 15}}>
            <View
              style={{
                flex: 1,
                height: width * 1.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: headerBorderBottom,
                  fontSize: RFPercentage(1.8),
                  letterSpacing: 0.3,
                  textAlign: 'center',
                }}>
                Products Not Found
              </Text>
            </View>
          </Content>
        </View>
      );
    } else {
      return (
        <FlatList
          ref={props?.listRef}
          initialNumToRender={4}
          refreshControl={
            <RefreshControl
              refreshing={props?.refreshing}
              onRefresh={() => props?.onRefresh()}
            />
          }
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={renderItem}
          numColumns={2}
          contentContainerStyle={{
            paddingBottom: 25,
            paddingTop: 2,
          }}
          columnWrapperStyle={{
            paddingLeft: 10,
            paddingRight: 10,
          }}
          onEndReached={() => {
            console.log('Masuk End Reached');
            if (list?.length < props?.totalCount) {
              props?.setIsLoadMore(true);
            } else {
              props?.setIsLoadMore(false);
            }
          }}
          onEndReachedThreshold={0.6}
          ListFooterComponent={() => {
            return <Footer isLoadMore={props?.isLoadMore} />;
          }}
        />
      );
    }
  }
};

export const ListFilteredData = props => {
  console.log('ListFilteredData >>> ', props);
  const {lists: list, removeFilter} = props;
  // this list from redux filter data

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <View style={{width, paddingTop: 5}}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal
          style={{
            flexDirection: 'row',
            width: '100%',
          }}
          contentContainerStyle={{
            padding: 2,
            paddingLeft: 7.5,
            paddingRight: 7.5,
          }}>
          {list?.map((item, index) => {
            return (
              <View key={`${item.name}`}>
                <ListFilterButton
                  removeFilter={removeFilter}
                  list={item?.list}
                  parentIndex={index}
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
};

export const ListFilterButton = props => {
  const {list, parentIndex, removeFilter} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${index}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      if (item?.checked) {
        return (
          <TouchableOpacity
            onPress={() => {
              try {
                removeFilter(parentIndex, index);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            style={{
              padding: 5,
              paddingLeft: 15,
              paddingRight: 15,
              borderRadius: 15,
              backgroundColor: headerBorderBottom,
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 15,
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: 'grey',
              }}>
              {item?.name}
            </Text>
            <Icon
              type="Feather"
              name="x"
              style={{
                top: 1,
                left: 5,
                color: 'grey',
                fontSize: RFPercentage(1.8),
              }}
            />
          </TouchableOpacity>
        );
      } else {
        return null;
      }
    },
    [list],
  );

  if (list?.length === 0) {
    return null;
  } else {
    return (
      <FlatList
        scrollEnabled={false}
        horizontal
        contentContainerStyle={{
          padding: 2.5,
        }}
        data={list}
        extraData={list}
        keyExtractor={keyExt}
        renderItem={renderItem}
      />
    );
  }
};

const mapStateToProps = state => {
  const {suitFilterList} = state;
  return {
    lists: suitFilterList?.list,
    positionYBottomNav: state?.positionYBottomNav,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    reduxSuitFilter: list => dispatch(ReduxSuitFilter(list)),
    reduxSuitReset: () => dispatch(ReduxResetFilter()),
  };
};

const ConnectComponent = connect(mapStateToProps, mapDispatchToProps)(Suit);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
