import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';
import Loader from '../../../components/Loader/circleLoader';
import QueryPublicProduct from '../../../graphql/queries/productsPublic';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';
import {connect} from 'react-redux';
import ReduxProductFilter from '../../../redux/thunk/ProductFilterThunk';
import ReduxProductFilterStatus from '../../../redux/thunk/ProductFilterStatusThunk';
const {width, height} = Dimensions.get('window');
const {black, mainRed} = Colors;
const {medium} = FontType;

const SmartHomeProduct = props => {
  console.log('SmartHomeProduct Props: ', props);
  const {
    client,
    navigation,
    merchantId,
    serviceType,
    bridalType,
    isLoading: loadingParent,
    productFilterStatus,
    productFilter,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [product, setProducts] = useState([]);

  useEffect(() => {
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return subscriber;
  }, [loadingParent]);

  const fetch = () => {
    try {
      const variables = {
        merchantId,
        serviceType,
        jewelleryType: [],
        pageSize: 2,
        pageNumber: 1,
      };
      client
        .query({
          query: QueryPublicProduct,
          variables,
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response Product Smart Home >>>>>: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProducts([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          height: height / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <>
        {product?.length === 0 ? null : (
          <Card transparent style={{marginLeft: 0, marginRight: 0}}>
            {product?.length > 0 ? (
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(2),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Products
                </Text>
                <Button
                  onPress={async () => {
                    await productFilterStatus(false);
                    await productFilter([], [], [], [], []);
                    navigation.navigate('Products', {
                      id: merchantId,
                      serviceType,
                      filtered: false,
                    });
                  }}
                  style={{
                    backgroundColor: 'transparent',
                    elevation: 0,
                    shadowOpacity: 0,
                    height: height / 20,
                    paddingLeft: 10,
                    paddingRight: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      right: -15,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.6),
                      color: mainRed,
                      letterSpacing: 0.3,
                    }}>
                    See All
                  </Text>
                  <Icon
                    type="Feather"
                    name="arrow-right"
                    style={{right: -10, fontSize: 18, color: mainRed}}
                  />
                </Button>
              </CardItem>
            ) : null}
            <FlatList
              legacyImplementation
              disableVirtualization
              numColumns={2}
              contentContainerStyle={{
                padding: 15,
                paddingBottom: 15,
                paddingTop: 0,
              }}
              columnWrapperStyle={{
                justifyContent: 'space-between',
              }}
              data={product}
              extraData={product}
              keyExtractor={item => String(item.id)}
              renderItem={({item}) => {
                const {
                  featuredImageDynamicURL,
                  featuredImageURL,
                  name,
                  id: productId,
                  merchantDetails,
                } = item;
                const source =
                  featuredImageDynamicURL === null
                    ? {uri: featuredImageURL}
                    : {uri: `${featuredImageDynamicURL}=h500`};
                const {id} = merchantDetails;
                return (
                  <CardProduct
                    label={'Product'}
                    name={name}
                    source={source}
                    merchantId={id}
                    productId={productId}
                    navigation={navigation}
                  />
                );
              }}
            />
          </Card>
        )}
      </>
    );
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {};
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxProductFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    productFilterStatus: status => dispatch(ReduxProductFilterStatus(status)),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SmartHomeProduct);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
