import React, {useEffect} from 'react';
import {Animated, Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Container, Header, Body, Left, Right, Icon, Button} from 'native-base';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import Product from '../../Merchant/Product/Products';
import ProductResetRedux from '../../../redux/thunk/ResetProductFilterThunk';

const {black, white, greyLine, transparent} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

export const Headers = props => {
  const {onPress} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => onPress()}
          style={{
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: transparent,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          Smart Home
        </Text>
      </Body>
      <Right style={{flex: 0.1}}>
        <Button
          onPress={() => onPress()}
          style={{
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: transparent,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Icon
            type="Feather"
            name="home"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Right>
    </Header>
  );
};

const SmartHome = props => {
  console.log('Jewellery Props: ', props);
  const {navigation, resetProduct, positionYBottomNav} = props;

  useEffect(() => {
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subscriber = navigation.addListener('focus', () => {
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subscriber;
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = async () => {
    try {
      await resetProduct();
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };
  return (
    <Container>
      <Headers onPress={goBack} {...props} />
      <Product {...props} />
    </Container>
  );
};

const mapToState = state => {
  return {
    positionYBottomNav: state?.positionYBottomNav,
  };
};

const mapToDispatch = dispatch => {
  return {
    resetProduct: () => dispatch(ProductResetRedux()),
  };
};

const connection = connect(
  mapToState,
  mapToDispatch,
)(SmartHome);

const Wrapper = compose(withApollo)(connection);

export default props => <Wrapper {...props} />;
