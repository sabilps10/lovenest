import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import ButtonBack from '../../../components/Button/buttonBack';
import ButtonReset from '../../../components/Button/buttonText';
import {hasNotch} from 'react-native-device-info';

// QUERY
import QUERY_PROJECT_FILTER from '../../../graphql/queries/projectsFilter';

// Redux
import {connect} from 'react-redux';
import ReduxProjectFilter from '../../../redux/thunk/ProjectFilterThunk';
import ReduxResetProjectFilter from '../../../redux/thunk/ResetProjectFilterThunk';
import ReduxProjectFilterStatus from '../../../redux/thunk/ProjectFilterStatusThunk';

import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFilter from '../../../components/Button/ButtonFilters/Unfilter';

const {
  black,
  white,
  transparent,
  greyLine,
  mainGreen,
  mainRed,
  lightSalmon,
} = Colors;
const {book, medium} = FontType;
const {small, regular} = FontSize;
const {width, height} = Dimensions.get('window');

const InteriorFilter = props => {
  console.log('InteriorFilter Props: ', props);
  const {
    navigation,
    client,
    route,
    project,
    projectFilter,
    resetProjectFilter,
    projectFilterStatus,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [filterList, setFilterList] = useState([]);
  const [selectedType, setSelectedType] = useState([]);
  const [selectedStyle, setSelectedStyle] = useState([]);

  useEffect(() => {
    navigationOptions();
    fetchAllIncludedReduxState();
    const subsciber = navigation.addListener('focus', () => {
      fetchAllIncludedReduxState();
    });

    return subsciber;
  }, [navigation, project]);

  const fetchAllIncludedReduxState = async () => {
    if (project.data.length === 0) {
      await fetch();
    } else {
      await setFilterList([...project.data]);
      await setSelectedType([...project.selectedType]);
      await setSelectedStyle([...project.selectedStyle]);
      await setIsError(false);
      await setIsLoading(false);
    }
  };

  const fetch = () => {
    try {
      client
        .query({
          query: QUERY_PROJECT_FILTER,
          variables: {
            serviceType,
            merchantId: params?.merchantId ? params.merchantId : null,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response filter project: ', response);
          const {errors, data} = response;
          const {projectsFilter} = data;
          const {data: list, error} = projectsFilter;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setFilterList([...list]);
              await setSelectedType([]);
              await setSelectedStyle([]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Interior Design',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return (
          <ButtonBack iconX={true} {...props} onPress={() => popStacking()} />
        );
      },
      headerRight: () => {
        return (
          <ButtonReset
            onPress={async () => {
              await await projectFilterStatus(false);
              await resetProjectFilter();
              navigation.pop();
            }}
            text={'Reset'}
          />
        );
      },
    });
  };

  const extKey = (_, index) => `${String(index)}`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };

  const checkList = async (parentIndex, index) => {
    let oldData = filterList;
    oldData[parentIndex].list[index].checked = !filterList[parentIndex].list[
      index
    ].checked;
    await setFilterList([...oldData]);

    if (filterList[parentIndex].name === 'Type') {
      let tempSelectedArray = selectedType;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedType(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Style') {
      let tempSelectedArray = selectedStyle;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedStyle(tempSelectedArray);
    }
  };

  const applyFilter = async () => {
    if (selectedType.length === 0 && selectedStyle.length === 0) {
      await projectFilterStatus(false);
    } else {
      await projectFilterStatus(true);
    }
    await projectFilter(filterList, selectedType, selectedStyle);
    navigation.pop();
  };

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          style={{
            borderWidth: 1,
            borderRadius: 10,
            backgroundColor: transparent,
            padding: 15,
            paddingTop: 5,
            paddingBottom: 5,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Text>Refresh</Text>
        </Button>
      </View>
    );
  } else {
    return (
      <Container style={{backgroundColor: white}}>
        <View style={{flex: 1}}>
          <Card
            style={{
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
              borderWidth: 0,
              borderColor: white,
            }}>
            <FlatList
              disableVirtualization
              decelerationRate="normal"
              legacyImplementation
              contentContainerStyle={{paddingBottom: 10}}
              data={filterList}
              extraData={filterList}
              keyExtractor={extKey}
              listKey={extKey}
              getItemLayout={getItemLayout}
              renderItem={({item, index}) => {
                const {name: label, list} = item;
                return (
                  <>
                    <CardItem
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: 15,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.8),
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        {label}
                      </Text>
                      <Text
                        style={{
                          fontFamily: medium,
                          fontSize: RFPercentage(1.4),
                          color: greyLine,
                          letterSpacing: 0.3,
                        }}>
                        You may select more than one
                      </Text>
                    </CardItem>
                    <CardItem
                      style={{
                        width: '100%',
                        flexWrap: 'wrap',
                        flexDirection: 'row',
                      }}>
                      <ListFilter
                        parentIndex={index}
                        list={list}
                        checkList={(parent, i) => {
                          checkList(parent, i);
                        }}
                      />
                    </CardItem>
                  </>
                );
              }}
            />
          </Card>
        </View>
        <Footer>
          <FooterTab style={{backgroundColor: mainGreen}}>
            <View style={{backgroundColor: mainGreen, width: '100%'}}>
              <TouchableOpacity
                onPress={() => applyFilter()}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  APPLY FILTER
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
};

export const ListFilter = props => {
  const {list, checkList, parentIndex} = props;

  const extKey = (_, index) => `${String(index)} Child`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };
  const RenderItem = () =>
    useMemo(() => {
      return (
        <FlatList
          getItemLayout={getItemLayout}
          disableVirtualization
          data={list}
          legacyImplementation
          scrollEnabled={false}
          extraData={list}
          keyExtractor={extKey}
          listKey={extKey}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            // borderWidth: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
          }}
          renderItem={({item, index}) => {
            const {name, checked} = item;
            return (
              <ListCardItem
                name={name}
                checked={checked}
                parentIndex={parentIndex}
                checkList={(p, c) => checkList(p, c)}
                index={index}
              />
            );
          }}
        />
      );
    }, [list, parentIndex, checkList]);
  if (list) {
    return RenderItem();
  } else {
    return null;
  }
};

export const ListCardItem = props => {
  // props indicates
  // name, checked, parentIndex, checkList, index
  const {checked} = props;

  return (
    <>
      {checked ? <ButtonFiltered {...props} /> : <ButtonUnFilter {...props} />}
    </>
  );
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    project: state.project,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    projectFilter: (data, selectedType, selectedStyle) =>
      dispatch(ReduxProjectFilter(data, selectedType, selectedStyle)),
    resetProjectFilter: () => dispatch(ReduxResetProjectFilter()),
    projectFilterStatus: status => dispatch(ReduxProjectFilterStatus(status)),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(InteriorFilter);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
