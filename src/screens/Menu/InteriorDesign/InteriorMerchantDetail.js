import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Dimensions,
  StatusBar,
  Animated,
  Image,
  FlatList,
  Platform,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import LinearGradient from 'react-native-linear-gradient';
import {
  Container,
  Footer,
  FooterTab,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
} from 'native-base';
import {charImage} from '../../../utils/Themes/Images';
import HighlightList from '../../../screens/Merchant/Highlight/ShortHighlight';
import ProBadge from '../../../components/Cards/Merchants/ProBadge';

// Common Utils
const {charBridalBadge} = charImage;
const {book, medium} = FontType;
const {regular} = FontSize;
const {white, transparent, black, greyLine, superGrey, mainGreen} = Colors;

// Query
import GET_MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

// Common Screen
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';
import Loader from '../../../components/Loader/circleLoader';
import SpecialOffer from '../Bridal/BridalDetail/Components/SpecialOffer';
import ProjectListCard from './ProjectInterior';

// Animated Utils
const {width, height} = Dimensions.get('window');
const HEADER_HEIGHT = hasNotch() ? 85 : 75;
const HEADER_CARD_IMAGE_HEIGHT = height / 2.94;
const SCROLL_HEIGHT = HEADER_CARD_IMAGE_HEIGHT - HEADER_HEIGHT;

const InteriorMerchantDetail = props => {
  console.log('InteriorMerchantDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {serviceType, id} = params;

  const [merchantDetailData, setMerchantDetailData] = useState([]);
  const [landscape, setLandscape] = useState([]);
  const [portrait, setPortrait] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [scrollY] = useState(new Animated.Value(0));

  useEffect(() => {
    StatusBar.setTranslucent(true);
    fetch();

    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(true);
      fetch();
    });

    return subscriber;
  }, [navigation, serviceType, id]);

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const setLanscapeImage = extraPhoto => {
    console.log('Extra Photo Pak: ', extraPhoto);
    return new Promise(async resolve => {
      try {
        const manipulate = await Promise.all(
          extraPhoto
            .map(d => {
              if (d.orientation === 'Landscape') {
                return {...d};
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );
        console.log('manipulate >>> ', manipulate);
        if (manipulate) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const setPortraitImage = extraPhoto => {
    return new Promise(async resolve => {
      try {
        const manipulate = await Promise.all(
          extraPhoto
            .map(d => {
              if (d.orientation === 'Potrait') {
                return {...d};
              } else {
                return false;
              }
            })
            .filter(Boolean),
        );
        console.log('manipulate >>> ', manipulate);
        if (manipulate) {
          resolve(manipulate);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const fetch = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response fetch bridal detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            console.log(
              'getMerchantVer2[0].extraPhoto >>>> ',
              getMerchantVer2[0].extraPhoto,
            );
            const extraPhoto =
              getMerchantVer2[0]?.extraPhoto.length > 0
                ? getMerchantVer2[0].extraPhoto
                : [];
            const getListImageLandscape = await setLanscapeImage(extraPhoto);
            const getListImagePortrait = await setPortraitImage(extraPhoto);
            await setLandscape([...getListImageLandscape]);
            await setPortrait([...getListImagePortrait]);
            await setMerchantDetailData([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          console.log('Failed fetch bridal detail: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefreshing(false);
        });
    } catch (error) {
      console.log('Error Fetch Merchant Detail: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
    }
  };

  if (isLoading && !isError) {
    // return loading
    return (
      <Container>
        <HeaderDefault
          serviceType={serviceType}
          onPressLeft={() => {}}
          onPressRight={() => {}}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loader />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    // return error screen
    return (
      <Container>
        <HeaderDefault
          serviceType={serviceType}
          onPressLeft={() => {}}
          onPressRight={() => {}}
        />
        <ErrorScreen message="Refresh" onPress={() => {}} />
      </Container>
    );
  } else {
    // return data
    const {
      id: merchantId,
      coverImageUrl,
      coverImageDynamicUrl,
      logoImageUrl,
      logoImageDynamicUrl,
      name,
      tagline,
    } = merchantDetailData[0];

    const imageSource =
      coverImageDynamicUrl === null
        ? {uri: coverImageUrl}
        : {uri: `${coverImageDynamicUrl}=h500`};
    const logoSource =
      logoImageDynamicUrl === null
        ? {uri: logoImageUrl}
        : {uri: `${logoImageDynamicUrl}=h200`};

    const HEADER_OPACITY = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [0.0, 0.5, 1.0],
    });

    const goBack = () => {
      try {
        navigation.goBack(null);
      } catch (error) {
        console.log('Error Go Back: ', error);
      }
    };

    const goToAboutMerchant = () => {
      try {
        navigation.push('AboutMerchant', {id});
      } catch (error) {
        console.log('Error: ', error);
      }
    };

    return (
      <Container>
        <StatusBar translucent backgroundColor="transparent" animated />
        <MiddleTextHeader
          {...props}
          merchantId={merchantId}
          name={name}
          titleOpacity={HEADER_OPACITY}
          leftSide={goBack}
          rightSide={goToAboutMerchant}
        />
        <WhiteBarHeader
          merchantId={merchantId}
          {...props}
          headerOpacity={HEADER_OPACITY}
        />
        <Animated.ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          bounces={true}
          decelerationRate="fast"
          scrollEventThrottle={Platform.OS === 'android' ? 16 : 1}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}
          style={{zIndex: 0}}>
          <HeaderImage
            logoSource={logoSource}
            imageSource={imageSource}
            name={name}
            tagline={tagline}
          />
          <View>
            <GalleryImages
              merchantDetailData={merchantDetailData}
              landscape={landscape}
              portrait={portrait}
            />
            {/* <ProductJewellery
              {...props}
              isLoading={refreshing}
              serviceType={serviceType}
              merchantId={id}
              bridalType={['Jewellery']}
            /> */}
            <ProjectListCard
              {...props}
              isLoadingParent={isLoading}
              serviceType={serviceType}
              merchantId={id}
            />
            {/* <HighlightList showShort={true} {...props} /> */}
            <SpecialOffer isLoading={refreshing} {...props} id={id} />
          </View>
        </Animated.ScrollView>
        <Footer>
          <FooterTab style={{backgroundColor: mainGreen}}>
            <View style={{backgroundColor: mainGreen, width: '100%'}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Enquiry', {serviceType, id});
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  SUBMIT ENQUIRY
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
};

export const GalleryImages = props => {
  console.log('ANU PROP image Gallery Bridal: ', props);
  const {merchantDetailData, landscape, portrait} = props;
  if (merchantDetailData[0]?.extraPhoto?.length > 0) {
    return (
      <View style={{marginTop: 15}}>
        <LandscapeImage
          imageSource={{
            uri: landscape[0]?.dynamicUrl
              ? `${landscape[0].dynamicUrl}=h500`
              : landscape[0].url,
          }}
        />
        <PortraitImages imageList={portrait} />
        <LandscapeImage
          imageSource={{
            uri: landscape[1]?.dynamicUrl
              ? `${landscape[1].dynamicUrl}=h500`
              : landscape[1].url,
          }}
        />
      </View>
    );
  } else {
    return null;
  }
};

export const PortraitImages = props => {
  const {imageList} = props;
  return (
    <FlatList
      scrollEnabled={false}
      data={imageList}
      extraData={imageList}
      keyExtractor={item => `${item.id} Portrait`}
      legacyImplementation
      disableVirtualization
      numColumns={2}
      columnWrapperStyle={{
        justifyContent: 'space-between',
        marginVertical: 4,
      }}
      renderItem={({item}) => {
        const imgSrc = item?.dynamicUrl
          ? {uri: `${item.dynamicUrl}=h500`}
          : {uri: item.url};
        return (
          <View
            style={{
              width: width / 2.02,
              height: height / 2.5,
              backgroundColor: greyLine,
            }}>
            <Image
              source={imgSrc}
              style={{flex: 1, width: '100%', height: height / 2.5}}
              resizeMode="cover"
            />
          </View>
        );
      }}
    />
  );
};

export const LandscapeImage = props => {
  const {imageSource} = props;

  return (
    <View
      style={{
        width,
        height: height / 3,
        backgroundColor: greyLine,
      }}>
      <Image
        source={imageSource}
        style={{flex: 1, width, height: height / 3}}
        resizeMode="cover"
      />
    </View>
  );
};

export const HeaderImage = props => {
  const {logoSource, imageSource, name, tagline} = props;

  return (
    <View
      style={{
        width,
        height: height / 3,
        backgroundColor: greyLine,
        flexDirection: 'column',
      }}>
      <View
        style={{
          flexDirection: 'column',
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 1,
        }}>
        <Image
          source={imageSource}
          style={{
            position: 'absolute',
            zIndex: 0,
            width,
            height: height / 3,
          }}
          resizeMode="cover"
        />
        <LinearGradient
          locations={Platform.OS === 'ios' ? [0.15, 0.78] : [0.5, 0.4, 0.8]}
          colors={
            Platform.OS === 'ios'
              ? [transparent, '#FFFFFF', '#FFFFFF']
              : [transparent, transparent, '#FFFFFF']
          }
          style={{
            position: 'absolute',
            zIndex: 2,
            width: '100%',
            height: '100%',
            bottom: 0,
          }}
        />
      </View>
      <View
        style={{
          paddingTop: 70,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          zIndex: 2,
          // backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            minWidth: width / 5,
            maxWidth: width / 5,
            minHeight: height / 11,
            maxHeight: height / 11,
            backgroundColor: white,
            borderRadius: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={logoSource}
            style={{
              // flex: 1,
              width: '100%',
              height: height / 15,
            }}
            resizeMode="contain"
          />
        </View>
        <View style={{marginVertical: 15, marginTop: 20}}>
          <ProBadge title={'Pro Interior Merchant'} />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 25,
            }}>
            {name}
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 23,
            }}>
            {tagline}
          </Text>
        </View>
      </View>
    </View>
  );
};

export const WhiteBarHeader = props => {
  const {headerOpacity} = props;

  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        zIndex: 1,
        height: HEADER_HEIGHT,
        width: width,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        flexDirection: 'row',
        backgroundColor: white,
        opacity: headerOpacity,
      }}>
      <Left style={{flex: 0.1}} />
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      />
      <Right style={{flex: 0.1}} />
    </Animated.View>
  );
};

export const MiddleTextHeader = props => {
  const {name, titleOpacity, leftSide, rightSide, merchantId} = props;

  return (
    <View
      style={{
        zIndex: 2,
        position: 'absolute',
        top: hasNotch() ? 50 : 30,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <View style={{flex: 0.1}}>
        <LeftButton onPress={leftSide} />
      </View>
      <View style={{flex: 0.15}} />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <Animated.Text
          numberOfLines={1}
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
            opacity: titleOpacity,
          }}>
          {name}
        </Animated.Text>
      </View>
      <View style={{flex: 0.15, flexDirection: 'row'}}>
        <ReviewButton merchantId={merchantId} {...props} />
      </View>
      <View style={{flex: 0.1, flexDirection: 'row'}}>
        <RightButton onPress={rightSide} />
      </View>
    </View>
  );
};

export const ReviewButton = props => {
  const {navigation, id, merchantId} = props;

  return (
    <Button
      onPress={() => navigation.navigate('ReviewList', {id: merchantId})}
      style={{
        // right: 40,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="Feather"
        name="star"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const RightButton = props => {
  const {onPress} = props;
  return (
    <Button
      onPress={() => onPress()}
      style={{
        // right: 30,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="MaterialCommunityIcons"
        name="store"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const LeftButton = props => {
  const {onPress} = props;
  return (
    <Button
      onPress={() => onPress()}
      style={{
        // zIndex: 2,
        // position: 'absolute',
        // top: hasNotch() ? 45 : 10,
        // left: 15,
        elevation: 0,
        shadowOpacity: 0,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        backgroundColor: white,
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{
          marginLeft: 0,
          marginRight: 0,
          fontSize: 24,
          color: black,
        }}
      />
    </Button>
  );
};

export const HeaderDefault = props => {
  const {serviceType} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          style={{
            elevation: 0,
            shadowOpacity: 0,
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: transparent,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          {serviceType}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(InteriorMerchantDetail);

export default props => <Wrapper {...props} />;
