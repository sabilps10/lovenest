import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  RefreshControl,
  Platform,
  Dimensions,
  TouchableOpacity,
  Animated
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
} from 'native-base';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {CommonActions} from '@react-navigation/native';
import {hasNotch} from 'react-native-device-info';

import {connect} from 'react-redux';
import ReduxProjectFilter from '../../../redux/thunk/ProjectFilterThunk';
import ReduxProjectResetFilter from '../../../redux/thunk/ResetProjectFilterThunk';

// component
import ButtonBack from '../../../components/Button/buttonBack';
import InteriorCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';

import GET_PUBLIC_PROJECT from '../../../graphql/queries/projectsPublic';

const {
  white,
  transparent,
  greyLine,
  black,
  lightSalmon,
  mainRed,
  mainGreen,
  headerBorderBottom,
} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;
const {width, height} = Dimensions.get('window');

const InteriorDesign = props => {
  console.log('InteriorDesign Props: ', props);
  const {navigation, client, route, project, resetProjectFilter, positionYBottomNav} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [projectList, setProjectList] = useState([]);

  const [interiorType, setInteriorType] = useState([]);
  const [interiorStyle, setInteriorStyle] = useState([]);

  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setIsRefreshing] = useState(false);

  useEffect(() => {
    navigationOptions();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      refetchAfterFilter();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subscriber;
  }, [navigation, project]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const refetchAfterFilter = async () => {
    await setIsRefreshing(true);
    await setPageSize(10);
    await fetch();
  };

  const onLoadMore = async status => {
    await setIsLoadMore(status);
    await setPageSize(pageSize + 10);
    await fetch();
  };

  const onRefresh = async () => {
    await setIsRefreshing(true);
    await setPageSize(10);
    await fetch();
  };

  const fetch = () => {
    try {
      client
        .query({
          query: GET_PUBLIC_PROJECT,
          variables: {
            serviceType,
            merchantId: params?.merchantId ? params.merchantId : null,
            venueType: [],
            interiorType: project.selectedType ? project.selectedType : [],
            interiorStyle: project.selectedStyle ? project.selectedStyle : [],
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          const {projectsPublic} = data;
          const {data: list, error, totalData: totalCount} = projectsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalData(totalCount);
              await setProjectList([...list]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
          setIsRefreshing(false);
          setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsRefreshing(false);
      setIsLoadMore(false);
    }
  };

  const popStacking = async () => {
    try {
      await props.resetProductFilter();
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Nest Lab',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
      headerRight: () => {
        return (
          <Button transparent onPress={() => resetToHome()}>
            <Icon
              type="Feather"
              name="home"
              style={{fontSize: 25, color: black}}
            />
          </Button>
        );
      },
    });
  };

  const resetToHome = async () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Main'}],
        }),
      );
    } catch (error) {
      console.log('Error reset to home from Products: ', error);
    }
  };

  const keyExt = (item, index) => `${item.id} Parent`;

  if (isLoading && !isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          style={{
            borderWidth: 1,
            borderRadius: 10,
            backgroundColor: transparent,
            padding: 15,
            paddingTop: 5,
            paddingBottom: 5,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Text>Refresh</Text>
        </Button>
      </View>
    );
  } else {
    return (
      <Container>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginVertical: 5,
                  }}>
                  <Text>Loading...</Text>
                </View>
              );
            } else {
              return null;
            }
          }}
          onEndReachedThreshold={0.01}
          onEndReached={() => {
            if (pageSize <= totalData) {
              onLoadMore(true);
            } else {
              onLoadMore(false);
            }
          }}
          contentContainerStyle={{
            paddingTop: 25,
            paddingBottom: 25,
            alignItems: 'center',
          }}
          data={projectList}
          extraData={projectList}
          keyExtractor={keyExt}
          listKey={keyExt}
          legacyImplementation
          initialNumToRender={5}
          renderItem={({item, index}) => {
            return (
              <InteriorCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          }}
        />
        <View
          style={{
            position: 'absolute',
            bottom:
              Platform.OS === 'android'
                ? hasNotch()
                  ? 35
                  : 35
                : hasNotch()
                ? 35
                : 20,
            zIndex: 2,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('InteriorFilter', {id: null, serviceType});
            }}>
            <View
              style={{
                backgroundColor: 'white',
                borderWidth: 1,
                borderColor: headerBorderBottom,
                borderRadius: 25,
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                minWidth: width / 16,
                height: height / 19,
                flexDirection: 'row',
                // flexWrap: 'wrap',
              }}>
              <Icon
                style={{fontSize: RFPercentage(1.8), color: black}}
                type="Feather"
                name="filter"
              />
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                  marginHorizontal: 5,
                }}>
                Filter
              </Text>
              {project?.status ? (
                <Icon
                  style={{fontSize: RFPercentage(1.8), color: mainRed}}
                  type="Feather"
                  name="check"
                />
              ) : null}
            </View>
          </TouchableOpacity>
        </View>
        {/* <View
          style={{
            width: Dimensions.get('window').width,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: Platform.OS === 'ios' ? (hasNotch() ? 25 : 15) : 20,
          }}>
          <Button
            activeOpacity={0.9}
            onPress={() => {
              navigation.navigate('InteriorFilter', {id: null, serviceType});
            }}
            style={{
              width: Dimensions.get('window').width / 3,
              borderRadius: 25,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
            }}>
            <Icon
              type="Feather"
              name="filter"
              style={{
                right: project.status === false ? 10 : 0,
                color: black,
                fontSize: RFPercentage(1.8),
              }}
            />
            <Text
              style={{
                right: project.status === false ? 10 : 0,
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: mainGreen,
                letterSpacing: 0.25,
              }}>
              Filter
            </Text>
            {project.status === false ? null : (
              <Icon
                type="Feather"
                name="check"
                style={{color: lightSalmon, fontSize: 18}}
              />
            )}
          </Button>
        </View> */}
      </Container>
    );
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    project: state.project,
    positionYBottomNav: state?.positionYBottomNav,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (data, selectedType, selectedStyle) =>
      dispatch(ReduxProjectFilter(data, selectedType, selectedStyle)),
    resetProductFilter: () => dispatch(ReduxProjectResetFilter()),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(InteriorDesign);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
