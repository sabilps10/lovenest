import React, {useState, useEffect} from 'react';
import {Text, View, TouchableOpacity, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem} from 'native-base';
import ProjectCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';
import GET_PUBLIC_PROJECT from '../../../graphql/queries/projectsPublic';

const {white, black, mainRed, mainGreen, greyLine} = Colors;
const {medium} = FontType;

const ProjectInterior = props => {
  console.log('ProjectInterior Props: ', props);
  const {navigation, client, serviceType, merchantId, isLoadingParent} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [projectList, setProjectlist] = useState([]);
  const [pageSize, setPageSize] = useState(3);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);

  useEffect(() => {
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return subscriber;
  }, [navigation, serviceType, merchantId, isLoadingParent]);

  const fetch = () => {
    try {
      client
        .query({
          query: GET_PUBLIC_PROJECT,
          variables: {
            serviceType,
            merchantId,
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Fetch Projectinterior only 3 items: ', response);
          const {data, errors} = response;
          const {projectsPublic} = data;
          const {data: list, error, totalData: totalCount} = projectsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setTotalData(totalCount);
              await setProjectlist([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Card transparent>
        <CardItem
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>Loading....</Text>
        </CardItem>
      </Card>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <Card transparent style={{marginLeft: 0, marginRight: 0}}>
        <CardItem style={{width: '100%'}}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {'Projects'}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.push('InteriorDesign', {merchantId, serviceType});
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: mainRed,
                    letterSpacing: 0.3,
                  }}>
                  See All
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </CardItem>
        <FlatList
          legacyImplementation
          contentContainerStyle={{
            paddingTop: 5,
            paddingBottom: 15,
            alignItems: 'center',
          }}
          data={projectList}
          extraData={projectList}
          keyExtractor={(item, index) => `${item.id} List Project`}
          renderItem={({item, index}) => {
            return (
              <ProjectCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          }}
        />
      </Card>
    );
  }
};

const Wrapper = compose(withApollo)(ProjectInterior);

export default props => <Wrapper {...props} />;
