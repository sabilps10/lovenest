import React, {useEffect, useState, useCallback} from 'react';
import {
  Text,
  Animated,
  FlatList,
  Dimensions,
  Image,
  View,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Container, Header, Left, Body, Right, Icon, Button} from 'native-base';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import _ from 'lodash';
import MerchantCards from '../../../components/Cards/Merchants/NewMerchantCard/NewMerchantCard';
import {charImage} from '../../../utils/Themes/Images';

// Query GQL
import GET_LIST_MERCHANT_BY_CATEGORY from '../../../graphql/queries/getMerchantByCategory';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {charComingSoon} = charImage;
const gradientColor = ['#FBF1E4', '#FFEDED', '#FACFC7', '#F6F6F6'];
const {width} = Dimensions?.get('screen');
const {black, greyLine} = Colors;
const {medium, book} = FontType;
const {regular} = FontSize;

const CategoryList2And3 = props => {
  console.log('CategoryList2And3 Props: ', props);
  const {navigation, positionYBottomNav} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);
  const [totalData, setTotalData] = useState(0);

  const itemDisplayed = 4;
  const [pageNumber, setPageNumber] = useState(1);

  // Pull and Refresh
  const [refreshing, setRefreshing] = useState(false);

  // Pagination
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }

    const firstFetch = fetchListMerchant();

    return () => {
      firstFetch;
    };
  }, []);

  // pull on refresh
  useEffect(() => {
    if (refreshing) {
      onRefresh();
    }
  }, [refreshing]);

  const onRefresh = () => {
    try {
      setPageNumber(1);
      fetchListMerchant();
    } catch (error) {
      console.log('Error onRefresh: ', error);
      setRefreshing(false);
    }
  };

  // Pagination infinite
  useEffect(() => {
    if (isLoadMore) {
      fetchMore();
    }
  }, [isLoadMore]);

  const fetchMore = () => {
    try {
      setPageNumber(pageNumber + 1);
    } catch (error) {
      setIsLoadMore(false);
    }
  };

  useEffect(() => {
    setTimeout(() => {
      fetchMoreBasePageNumberChange();
    }, 500);
  }, [pageNumber]);

  const fetchMoreBasePageNumberChange = useCallback(() => {
    fetchListMerchant();
  }, [pageNumber]);

  const fetchListMerchant = () => {
    try {
      console.log(
        'props?.route?.params?.menu >>> ',
        props?.route?.params?.data?.menu,
      );
      props?.client
        ?.query({
          query: GET_LIST_MERCHANT_BY_CATEGORY,
          variables: {
            category: props?.route?.params?.data?.menu,
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('Res get merchant list by category mobile: ', res);
          const {data, errors} = res;
          const {getMerchantsByCategory} = data;
          const {data: merchantList, error} = getMerchantsByCategory;

          if (errors || error) {
            throw new Error(errors ? errors : error);
          } else {
            if (pageNumber === 1) {
              setTotalData(getMerchantsByCategory?.totalData);
              setList([...merchantList]);
              setIsError(false);
              setIsLoading(false);
              setRefreshing(false);
              setIsLoadMore(false);
            } else {
              const oldList = list;
              const newArr = oldList?.concat(merchantList);
              const removeDuplication = _.unionBy(newArr, 'id');

              setTotalData(getMerchantsByCategory?.totalData);
              setList([...removeDuplication]);
              setIsError(false);
              setIsLoading(false);
              setRefreshing(false);
              setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
      setIsLoadMore(false);
    }
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Headers title={props?.route?.params?.data?.menuName} goBack={goBack} />
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator size="large" color={Colors?.mainGreen} />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Headers title={props?.route?.params?.data?.menuName} goBack={goBack} />
        <View style={{flex: 1}}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }>
            <View
              style={{
                width: '100%',
                height: width * 1.7,
                borderWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={charComingSoon}
                style={{width, height: width * 0.5, bottom: 15}}
                resizeMode="contain"
              />
              <TouchableOpacity
                onPress={() => setRefreshing(true)}
                style={{
                  backgroundColor: Colors?.mainGreen,
                  borderRadius: 5,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    color: Colors?.white,
                    textAlign: 'center',
                    letterSpacing: 0.3,
                  }}>
                  Refresh
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <Headers title={props?.route?.params?.data?.menuName} goBack={goBack} />
        <View
          style={{
            zIndex: -99,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }
            onEndReached={() => {
              if (pageNumber <= totalData) {
                setIsLoadMore(true);
              } else {
                setIsLoadMore(false);
              }
            }}
            ListFooterComponent={() => {
              if (isLoadMore) {
                return (
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingTop: 10,
                      paddingBottom: 10,
                    }}>
                    <Text>{isLoadMore ? 'Load more...' : ''}</Text>
                  </View>
                );
              } else {
                return null;
              }
            }}
            onEndReachedThreshold={0.01}
            ListEmptyComponent={() => {
              if (!isLoading) {
                return (
                  <View
                    style={{
                      width: '100%',
                      height: width * 1.6,
                      borderWidth: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={charComingSoon}
                      style={{width, height: width * 0.5}}
                      resizeMode="contain"
                    />
                    <Text
                      style={{
                        marginVertical: 15,
                        fontFamily: medium,
                        color: black,
                        textAlign: 'center',
                        fontSize: RFPercentage(2.5),
                      }}>
                      Coming Soon
                    </Text>
                    <Text
                      style={{
                        paddingLeft: 15,
                        paddingRight: 15,
                        lineHeight: 20,
                        fontFamily: book,
                        color: black,
                        textAlign: 'center',
                        fontSize: RFPercentage(2),
                      }}>
                      Keep updated with us, preparing our merchant very soon !
                    </Text>
                  </View>
                );
              } else {
                return null;
              }
            }}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
            data={list}
            extraData={list}
            keyExtractor={item => `${item?.id}`}
            renderItem={({item}) => {
              const id = item.id;
              const serviceType = item.serviceType;
              const iconSource =
                item.logoImageDynamicUrl === null
                  ? {uri: item.logoImageUrl}
                  : {uri: `${item.logoImageDynamicUrl}=h200`};
              const logoSource =
                item.coverImageDynamicUrl === null
                  ? {uri: item.coverImageUrl}
                  : {uri: `${item.coverImageDynamicUrl}=h500`};
              const name = item.name;
              const tagLine = item.tagline;
              const generatorColors =
                gradientColor[Math.floor(Math.random() * gradientColor.length)];
              return (
                <MerchantCards
                  navigation={navigation}
                  id={id}
                  serviceType={serviceType}
                  source={logoSource}
                  title={name}
                  tag={tagLine}
                  color={generatorColors}
                  iconSource={iconSource}
                />
              );
            }}
          />
        </View>
      </Container>
    );
  }
};

const mapStateToProps = state => {
  return {
    positionYBottomNav: state?.positionYBottomNav,
  };
};

export const Headers = props => {
  const {goBack, title} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="home"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Right>
    </Header>
  );
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {};
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryList2And3);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
