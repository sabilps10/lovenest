/* eslint-disable curly */
import React, {useEffect, useState} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  Platform,
  PermissionsAndroid,
  Modal,
  Alert
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';

const {width, height} = Dimensions.get('window');
const {white, black, mainGreen, overlayDim, mainRed} = Colors;
const {book, medium} = FontType;

import Pdf from 'react-native-pdf';
import RNBackgroundDownloader from 'react-native-background-downloader';
import RNFetchBlob from 'rn-fetch-blob';

const PDFPreview = props => {
  console.log('PDFPreview Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {data: attachment} = params;
  const {id, filename, url, dynamicUrl, mimetype} = attachment;

  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [percentage, setPercentage] = useState(0);

  let [showModal, setShowModal] = useState(false);
  const [successDownload, setSuccessDownload] = useState(false);

  const onDownload = async () => {
    try {
      await setShowModal(true);
      if (Platform.OS === 'ios') {
        // ios
        await iOSDownload();
      } else {
        // android
        await hasAndroidPermission();
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const androidDownloader = async () => {
    try {
      // handle downloader for android
      RNFetchBlob.config({
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          mediaScannable: true,
          title: `${filename}`,
          path: `${RNFetchBlob?.fs?.dirs?.DownloadDir}/${filename}`,
        },
      })
        .fetch(
          'GET',
          `${dynamicUrl ? `${dynamicUrl}` : `${url}`}`,
          {
            'Content-Type': 'octet-stream',
          },
          'base64DataString',
        )
        .progress({interval: 10}, async (received, total) => {
          console.log('progress', received / total);
          await setPercentage((received / total) * 100);
        })
        .then(async res => {
          console.log('RESS BROOO: ', res);
          // open the document directly
          await setSuccessDownload(true);
          await setTimeout(async () => {
            await setSuccessDownload(false);
            await setShowModal(false);
            await setTimeout(() => {
              RNFetchBlob.android.actionViewIntent(res.path(), mimetype);
            }, 1000);
          }, 1500);
          // or show options
          // RNFetchBlob.ios.openDocument(res.path())
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      await setShowModal(false);
    }
  };

  const iOSDownload = async () => {
    try {
      RNFetchBlob.config({
        fileCache: true,
        appendExt: 'pdf',
      })
        .fetch(
          'GET',
          `${dynamicUrl ? `${dynamicUrl}` : `${url}`}`,
          {
            'Content-Type': 'octet-stream',
          },
          'base64DataString',
        )
        .progress({interval: 10}, async (received, total) => {
          console.log('progress', received / total);
          await setPercentage((received / total) * 100);
        })
        .then(async res => {
          console.log('RESS BROOO: ', res);
          // open the document directly
          await setSuccessDownload(true);
          await setTimeout(async () => {
            await setSuccessDownload(false);
            await setShowModal(false);
            await setTimeout(() => {
              RNFetchBlob.ios.previewDocument(res.path());
            }, 1000);
          }, 1500);
          // or show options
          // RNFetchBlob.ios.openDocument(res.path())
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setShowModal(false);
    }
  };

  const cancelDownload = async () => {
    try {
      await setShowModal(!showModal);
    } catch (error) {
      await setShowModal(!showModal);
    }
  };

  const hasAndroidPermission = async () => {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ],
        {
          title: 'Photo Gallery',
          message:
            'Love Nest App needs access to your gallery to save the photo ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      console.log('granted: ', granted);
      if (granted) {
        await androidDownloader();
      } else {
        Alert.alert(JSON.stringify(granted, 10)); // need remove before deploy
      }
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Container style={{backgroundColor: black}}>
      <Headers goBack={() => goBack()} onDownload={() => onDownload()} />
      <View style={{flex: 1}}>
        <ModalDownloader
          visible={showModal}
          onClose={async () => cancelDownload()}
          status={successDownload}
          percentage={percentage}
        />
        <Pdf
          source={dynamicUrl ? {uri: dynamicUrl} : {uri: url}}
          onLoadProgress={percent => {
            setIsLoading(true);
            console.log('OnLoadProgress Percent: ', percent);
          }}
          onLoadComplete={async datas => {
            try {
              console.log('Datas Completed Loaded: ', datas);
              await setIsLoading(false);
            } catch (error) {
              console.log('Error: ', error);
              await setIsLoading(false);
            }
          }}
          onError={error => {
            console.log('Error: ', error);
            setIsError(true);
            setIsLoading(false);
          }}
          style={{flex: 1, width, height}}
        />
      </View>
    </Container>
  );
};

export const ModalDownloader = props => {
  const {visible, onClose, status, percentage} = props;
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: overlayDim,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Button
          onPress={() => onClose()}
          style={{
            position: 'absolute',
            top: 50,
            left: 25,
            zIndex: 99,
            backgroundColor: white,
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            borderRadius: 35 / 2,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="x"
            style={{
              color: black,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
            }}
          />
        </Button>
        {status ? (
          <View
            style={{
              padding: 10,
              width: width / 6,
              height: width / 6,
              borderWidth: 4,
              borderColor: mainRed,
              borderRadius: ((width / 6) * 0.5 + (width / 6) * 0.5) / 2,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: 40, color: mainGreen}}
            />
          </View>
        ) : (
          <View
            style={{
              minWidth: width / 4,
              borderRadius: 4,
              padding: 10,
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              size="large"
              color={mainGreen}
              style={{marginTop: 10}}
            />
            <Text style={{color: black, marginTop: 10}}>
              {percentage.toFixed(1)}%
            </Text>
            <Text style={{color: black, marginTop: 10}}>Downloading...</Text>
          </View>
        )}
      </View>
    </Modal>
  );
};

export const Headers = props => {
  const {goBack, onDownload} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle={'light-content'}
      androidStatusBarColor={'black'}
      style={{
        backgroundColor: black,
        borderBottomWidth: 1,
        borderBottomColor: white,
      }}>
      <Left style={{flex: 0.2}}>
        <Button
          onPress={() => goBack()}
          style={{
            right: 10,
            backgroundColor: black,
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: white}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            color: white,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          Preview
        </Text>
      </Body>
      <Right style={{flex: 0.2}}>
        <Button
          onPress={() => onDownload()}
          style={{
            backgroundColor: black,
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="download"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: white}}
          />
        </Button>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(PDFPreview);

export default props => <Wrapper {...props} />;
