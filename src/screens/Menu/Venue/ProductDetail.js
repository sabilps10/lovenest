/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Platform,
  Animated,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Header,
  Footer,
  FooterTab,
} from 'native-base';
import {hasNotch} from 'react-native-device-info';
import JustLoader from '../../../components/Loader/JustLoader';
import Loader from '../../../components/Loader/LoaderWithContainer';
import AsyncImage from '../../../components/Image/AsyncImage';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncModel from '../../../utils/AsyncstorageDataStructure';
import ModalImageViewer from './Components/ImageViewer';
import ModalImageAttatchmentViewer from './Components/ImageViewer';
import ModalLoader from './Components/ModalLoader';

// Query
import GET_PRODUCT_DETAIL from '../../../graphql/queries/fetchProduct';

// Mutation
import MutationAddWishList from '../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../graphql/mutations/removeWishlist';

const {asyncToken} = AsyncModel;
const {
  white,
  black,
  greyLine,
  headerBorderBottom,
  mainRed,
  mainGreen,
  brownCream,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');
const heightSlider = height / 1.58;

const ProductDetail = props => {
  console.log('ProductDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refresh, setRefresh] = useState(false);

  const [dataProduct, setDataProduct] = useState(null);

  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);
  const [isLogin, setIsLogin] = useState(true);

  const [openImageViewer, setOpenImageViewer] = useState(false);
  const [openImageAttatchmentViewer, setOpenImageAttachmentViewer] = useState(
    false,
  );

  useEffect(() => {
    checkIsLogin();
    fetchProduct();
    const subscriber = navigation.addListener('focus', () => {
      checkIsLogin();
      fetchProduct();
    });

    return subscriber;
  }, [navigation]);

  const openImageModal = async () => {
    try {
      await setOpenImageViewer(!openImageViewer);
    } catch (error) {
      console.log('Error: ', error);
      await setOpenImageViewer(false);
    }
  };

  const runCheckingLogin = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const token = await AsyncStorage.getItem(asyncToken);
        console.log('token >>> ', token);
        if (token) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const checkIsLogin = async () => {
    try {
      const login = await runCheckingLogin();
      console.log('Login >>>> : ', login);
      await setIsLogin(login);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefresh(true);
      await fetchProduct();
    } catch (error) {
      console.log('Error: ', error);
      await setRefresh(false);
    }
  };

  const fetchProduct = () => {
    try {
      client
        .query({
          query: GET_PRODUCT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response get product detail: ', response);
          const {data, errors} = response;
          const {fetchProduct: resFetchProduct} = data;
          const {data: product, error} = resFetchProduct;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefresh(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefresh(false);
            } else {
              await setDataProduct(product);
              await setIsError(false);
              await setIsLoading(false);
              await setRefresh(false);
            }
          }
        })
        .catch(error => {
          console.log('Error get product detail: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefresh(false);
        });
    } catch (error) {
      console.log('Error get product detail: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefresh(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un wishlist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {data: resp, error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {data: resp, error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  if (isLoading && !isError) {
    return <Loader />;
  } else if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <Headers goBack={goBack} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={async () => {
              try {
                await setIsLoading(true);
                await fetchProduct();
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            style={{
              borderRadius: 4,
              borderWidth: 1,
              height: height / 17,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <Text style={{fontFamily: medium, fontSize: RFPercentage(1.8)}}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: white}}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={'transparent'}
          barStyle={'dark-content'}
        />
        <ModalLoader isLoading={isLoadingWishList} />
        <ModalImageViewer
          visible={openImageViewer}
          onPress={openImageModal}
          images={dataProduct?.galleries}
        />
        <ModalImageAttatchmentViewer
          visible={openImageAttatchmentViewer}
          onPress={async () =>
            await setOpenImageAttachmentViewer(!openImageAttatchmentViewer)
          }
          images={
            dataProduct?.attachment
              ? [
                  {
                    id: dataProduct?.attachment?.id,
                    url: dataProduct?.attachment?.dynamicUrl
                      ? dataProduct?.attachment?.dynamicUrl
                      : dataProduct?.attachment?.url,
                  },
                ]
              : []
          }
        />
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
          }>
          <ProductImageSlider
            {...props}
            openImageModal={openImageModal}
            isLogin={isLogin}
            productId={dataProduct.id}
            isWishlist={dataProduct.isWishlist}
            runWishList={(productId, isWishlist) =>
              runWishList(productId, isWishlist)
            }
            gallery={dataProduct?.galleries ? dataProduct.galleries : []}
          />
          <HeaderTitleAndDescription
            visible={openImageAttatchmentViewer}
            onPress={async () =>
              await setOpenImageAttachmentViewer(!openImageAttatchmentViewer)
            }
            {...props}
            data={dataProduct}
          />
        </ScrollView>
        {isLogin ? (
          <ButtonAddToCart
            title={'ENQUIRY NOW'}
            onPress={() => {
              //  navigate to enquiry
              console.log('SUBMIT ENQUIRY: ', dataProduct);
              try {
                if (
                  dataProduct?.merchantDetails?.id &&
                  dataProduct?.merchantDetails?.serviceType
                ) {
                  navigation.navigate('Enquiry', {
                    id: dataProduct?.merchantDetails?.id,
                    serviceType: dataProduct?.merchantDetails?.serviceType,
                  });
                }
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
        ) : null}
      </View>
    );
  }
};

export const ButtonAddToCart = props => {
  const {onPress, title} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const HeaderTitleAndDescription = props => {
  console.log('HeaderTitleAndDescription: >>> ', props);
  const {data, navigation, visible, onPress} = props;
  const {
    name,
    description,
    hotelPrice,
    hotelCapacity,
    hotelPackageDetails,
    hotelPackage,
    hotelRegion,
    hotelAvailabilty,
    attachments,
    merchantDetails,
  } = data;

  return (
    <Card
      transparent
      style={{
        bottom: 15,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem style={{width: '100%'}}>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <View>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.5),
                color: black,
                letterSpacing: 0.39,
                lineHeight: 25,
                textAlign: 'left',
              }}>
              {name}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 0.5,
            flexDirection: 'column',
            // flexWrap: 'wrap',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.5),
              color: black,
              letterSpacing: 0.39,
              lineHeight: 25,
              textAlign: 'right',
            }}>
            ${hotelPrice ? hotelPrice.toFixed(2) : 'N/A'}
          </Text>
        </View>
      </CardItem>

      {/* Description */}
      <CardItem style={{paddingTop: 5}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {description}
        </Text>
      </CardItem>

      {/* Capacity and Rate Card */}
      <CardItem style={{width: '100%'}}>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: greyLine,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginBottom: 5,
            }}>
            Capacity
          </Text>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="users"
              style={{fontSize: RFPercentage(2.5), color: black}}
            />
            <Text
              style={{
                right: 10,
                fontFamily: book,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
                marginBottom: 10,
              }}>
              {hotelCapacity ? hotelCapacity : 'N/A'}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: greyLine,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginBottom: 5,
            }}>
            Rate Card
          </Text>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            {attachments?.length === 0 || !attachments ? (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="users"
                  style={{fontSize: RFPercentage(2.5), color: black}}
                />
                <Text
                  style={{
                    right: 10,
                    fontFamily: book,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                    marginBottom: 10,
                  }}>
                  N/A
                </Text>
              </View>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('RateCard', {
                    rates: props?.data?.attachments
                      ? props?.data?.attachments
                      : [],
                    merchantName: props?.data?.merchantDetails?.name
                      ? props?.data?.merchantDetails?.name
                      : 'Rate Card',
                    serviceType: props?.data?.merchantDetails?.serviceType,
                  });
                }}
                style={{
                  paddingTop: 8,
                  paddingBottom: 8,
                  backgroundColor: mainRed,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    left: 8,
                    fontFamily: medium,
                    color: white,
                    fontSize: RFPercentage(1.4),
                    letterSpacing: 0.3,
                  }}>
                  Rate Card{' '}
                  {`(${attachments?.length ? attachments?.length : 0})`}
                </Text>
                <Icon
                  type="Feather"
                  name="arrow-right"
                  style={{left: 15, fontSize: RFPercentage(1.6), color: white}}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </CardItem>

      {/* Region and Availability */}
      <CardItem style={{width: '100%'}}>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: greyLine,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginBottom: 5,
            }}>
            Region
          </Text>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
                marginBottom: 10,
              }}>
              {hotelRegion
                ? hotelRegion
                : props?.data?.merchantDetails?.region
                ? props?.data?.merchantDetails?.region
                : 'N/A'}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: greyLine,
              letterSpacing: 0.3,
              lineHeight: 20,
              marginBottom: 5,
            }}>
            Availability
          </Text>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              onPress={() => {}}
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
                marginBottom: 10,
              }}>
              {hotelAvailabilty ? hotelAvailabilty : 'N/A'}
            </Text>
          </View>
        </View>
      </CardItem>

      {/* Packages Details */}
      <CardItem style={{paddingBottom: 0}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: greyLine,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Package Detail
        </Text>
      </CardItem>
      <CardItem style={{paddingTop: 5}}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {hotelPackageDetails}
        </Text>
      </CardItem>
    </Card>
  );
};

export const CardImage = props => {
  const {item} = props;
  const {url, dynamicUrl} = item;
  const logoSource = dynamicUrl ? {uri: `${dynamicUrl}=h800`} : {uri: url};
  const RenderCard = () =>
    useMemo(() => {
      return (
        <View
          style={{
            elevation: 0,
            shadowOpacity: 0,
            marginLeft: 0,
            marginTop: 0,
            marginBottom: 0,
            marginRight: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            width,
          }}>
          <View style={{width, height: heightSlider}}>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                shadowOpacity: 0,
                elevation: 0,
                width,
                height: heightSlider,
                aspectRatio: width / heightSlider,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={'#f8f8f8'}
            />
          </View>
        </View>
      );
    }, [item]);

  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const BackButton = props => {
  const {navigation} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 35,
        left: 15,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          navigation.goBack(null);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 35,
          width: 35,
          justifyContent: 'center',
          borderRadius: 35 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        <Icon
          type="Feather"
          name="chevron-left"
          style={{
            right: 0.5,
            marginLeft: 0,
            marginRight: 0,
            fontSize: 25,
            color: black,
          }}
        />
      </TouchableOpacity>
    </Animated.View>
  );
};

export const HeartButton = props => {
  const {isWishlist, id, runWishList} = props;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 35,
        right: 15,
        zIndex: 99,
        transform: [{scale}],
      }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          runWishList(id, isWishlist);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
          height: 35,
          width: 35,
          justifyContent: 'center',
          borderRadius: 35 / 2,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        {isWishlist ? (
          <Icon
            type="MaterialCommunityIcons"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        ) : (
          <Icon
            type="Feather"
            name="heart"
            style={{
              top: 2,
              left: 0.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 20,
              color: mainRed,
            }}
          />
        )}
      </TouchableOpacity>
    </Animated.View>
  );
};

export const ProductImageSlider = props => {
  console.log;
  const {
    gallery,
    navigation,
    productId,
    isWishlist,
    runWishList,
    isLogin,
    openImageModal,
  } = props;
  const scrollX = new Animated.Value(0);
  const keyExt = (item, index) => `${String(item.id)} Images`;
  const RenderSlider = () =>
    useMemo(() => {
      return (
        <>
          <BackButton navigation={navigation} />
          {isLogin ? (
            <HeartButton
              id={productId}
              runWishList={(id, statusWhislist) =>
                runWishList(id, statusWhislist)
              }
              isWishlist={isWishlist}
            />
          ) : null}
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            initialNumToRender={3}
            legacyImplementation
            disableVirtualization
            decelerationRate="fast"
            data={gallery}
            extraData={gallery}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            pagingEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            initialScrollIndex={0}
            renderItem={({item, index}) => {
              return <CardImage item={item} />;
            }}
          />
          <View
            style={{
              position: 'relative',
              zIndex: 10,
              bottom: 50,
              right: 0,
              left: 0,
              width: '100%',
              flexDirection: 'row',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {gallery.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {gallery.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: 7,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 7 / 2,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 9,
                          width: 9,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 9 / 2,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
            <View
              style={{
                flex: 0.3,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  openImageModal();
                }}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: white,
                  paddingLeft: 10,
                  paddingRight: 10,
                  height: height / 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  View All
                </Text>
              </Button>
            </View>
          </View>
        </>
      );
    }, [gallery]);
  if (gallery) {
    return RenderSlider();
  } else {
    return null;
  }
};

export const Headers = props => {
  const {goBack} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      translucent={false}
      style={{
        zIndex: 99,
        backgroundColor: 'white',
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: greyLine,
        borderBottomWidth: 0.5,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => goBack()}
          style={{
            elevation: 0,
            backgroundColor: 'transparent',
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: regular,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Florist
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(ProductDetail);

export default props => <Wrapper {...props} />;