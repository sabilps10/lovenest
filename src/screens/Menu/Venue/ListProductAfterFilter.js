import React, {useState, useEffect, useRef} from 'react';
import {
  FlatList,
  Text,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  Image,
  View,
  RefreshControl,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
} from 'native-base';

import {charImage} from '../../../utils/Themes/Images';

const {
  charOfferNotAvailable,
  charDelivery,
  charSelfPickup,
  charFilterIcon,
} = charImage;

// Mutation
import MutationAddWishList from '../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../graphql/mutations/removeWishlist';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../utils/AsyncstorageDataStructure';
const {asyncToken} = AsyncData;

// Query
import GET_PRODUCTS from '../../../graphql/queries/floristCommonProducts';

// Components
import CardProduct from './Components/CardProduct';
import ModalLoader from './Components/ModalLoader';

// Navigation Action
import {CommonActions} from '@react-navigation/native';

const {
  white,
  black,
  greyLine,
  mainGreen,
  mainRed,
  headerBorderBottom,
  superGrey,
} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

// Redux Thunk
import thunkPrices from '../../../redux/thunk/VenueAndHotelPriceThunk';
import thunkShortPrices from '../../../redux/thunk/VenueAndHotelShotPriceThunk';
import thunkPackages from '../../../redux/thunk/VenueAndHotelPackagesThunk';
import thunkRegions from '../../../redux/thunk/VenueAndHotelRegionsThunk';
import thunkCapacities from '../../../redux/thunk/VenueAndHotelCapacitiesThunk';
import thunkAvailabilities from '../../../redux/thunk/VenueAndHotelAvailabilitiesThunk';

const ListPackagesAfterFilter = props => {
  console.log('ListPackagesAfterFilter: ', props);
  const {
    navigation,
    client,
    // id,
    route,
    // serviceType,
    // redux state
    prices: reduxPrices,
    shortPrices: reduxShortPrices,
    packages: reduxPackages,
    regions: reduxRegions,
    capacities: reduxCapacities,
    availabilities: reduxAvailabilities,
    // Redux dispatch
    setFilterPrices,
    setFilterShortPrices,
    setFilterPackages,
    setFilterRegions,
    setFilterCapacities,
    setFilterAvailabilities,
  } = props;
  const {params} = route;
  const {serviceType} = params;

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [pageSize, setPageSize] = useState(3);
  const pageNumber = 1;

  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    runCheckingLogin();
    fetchProducts();

    if (isLoadMore) {
      onLoadMore();
    }

    const subs = navigation.addListener('focus', () => {
      runCheckingLogin();
      fetchProducts();

      if (isLoadMore) {
        onLoadMore();
      }
    });

    const subs2 = navigation.addListener('tabPress', () => {
      runCheckingLogin();
      fetchProducts();

      if (isLoadMore) {
        onLoadMore();
      }
    });

    return () => {
      subs();
      subs2();
    };
  }, [
    navigation,
    isLoadMore,
    reduxPrices,
    reduxShortPrices,
    reduxPackages,
    reduxRegions,
    reduxCapacities,
    reduxAvailabilities,
  ]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        await setPageSize(pageSize + 10);
        await fetchProducts();
      }
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const getShortPrice = shortPriceData => {
    return new Promise(async (resolve, reject) => {
      try {
        if (shortPriceData && shortPriceData?.length !== 0) {
          const getData = await Promise.all(
            shortPriceData
              .map((d, i) => {
                if (d.selected) {
                  return d.name;
                } else {
                  return null;
                }
              })
              .filter(Boolean),
          );
          if (getData) {
            resolve(getData[0]);
          } else {
            resolve(null);
          }
        } else {
          resolve(null);
        }
      } catch (error) {
        resolve(null);
      }
    });
  };

  // manipulate data redux to variables needed
  const getStringDataRedux = reduxData => {
    console.log('KAMPRET: ', reduxData);
    return new Promise(async (resolve, reject) => {
      try {
        if (reduxData?.length === 0) {
          resolve([]);
        } else {
          const result = await Promise.all(
            reduxData
              .map((d, i) => {
                if (d.selected) {
                  return d.name;
                } else {
                  return null;
                }
              })
              .filter(Boolean),
          );

          if (result) {
            resolve(result);
          } else {
            resolve([]);
          }
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  const fetchProducts = async () => {
    try {
      const sortPrice = await getShortPrice(reduxShortPrices);
      console.log('sortPrice > ', sortPrice);
      const hotelPackage = await getStringDataRedux(reduxPackages);
      console.log('hotelPackage > ', hotelPackage);
      const hotelRegion = await getStringDataRedux(reduxRegions);
      console.log('hotelRegion > ', hotelRegion);
      const hotelCapacity = await getStringDataRedux(reduxCapacities);
      console.log('hotelCapacity > ', hotelCapacity);
      const hotelAvailabilty = await getStringDataRedux(reduxAvailabilities);
      console.log('hotelAvailabilty > ', hotelAvailabilty);

      const variables = {
        // merchantId: [parseInt(id, 10)],
        serviceType,
        pageNumber,
        pageSize,
        priceRange:
          reduxPrices[0] === 0 ||
          (reduxPrices[0] === 0.0 && reduxPrices[1] === 0) ||
          reduxPrices[1] === 0.0
            ? null
            : {
                min: parseFloat(reduxPrices[0]),
                max: parseFloat(reduxPrices[1]),
              },
        sortPrice: sortPrice ? sortPrice : null,
        hotelPackage,
        hotelRegion,
        hotelCapacity,
        hotelAvailabilty,
      };

      console.log('VARIABLES PRODUCT QUERY: ', variables);

      await client
        .query({
          query: GET_PRODUCTS,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetch product response: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: listProduct, error} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
            } else {
              await setList([...listProduct]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProducts();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchProducts();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkIsLogin = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const runCheckingLogin = async () => {
    try {
      const loginStatus = await checkIsLogin();
      if (loginStatus) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      await setIsLogin(false);
    }
  };

  const onPress = productId => {
    try {
      console.log('PRODUCT ID: ', productId);
      navigation.navigate('VenueAndHotelProductDetail', {id: productId});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Headers
        title={serviceType ? serviceType : 'Packages'}
        backPress={() => {
          try {
            navigation.goBack(null);
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
        searchPress={() => {
          try {
            navigation.goBack(null);
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
        homePress={() => {
          try {
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [{name: 'Home'}],
              }),
            );
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
      />
      <ModalLoader isLoading={isLoadingWishList} />
      <View style={{width: '100%', paddingTop: 15}}>
        <View style={{width: '100%'}}>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            horizontal
            style={{flexDirection: 'row', width: '100%'}}
            contentContainerStyle={{
              padding: 2,
              paddingLeft: 7.5,
              paddingRight: 7.5,
            }}>
            {/* Short Prices */}
            {!reduxShortPrices
              ? null
              : reduxShortPrices.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            reduxShortPrices.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === reduxShortPrices?.length) {
                            console.log('reduxShortPrices >>> ', updateData);
                            await setFilterShortPrices(updateData);
                            await setIsLoading(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Packages */}
            {reduxPackages?.length === 0
              ? null
              : reduxPackages.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            reduxPackages.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === reduxPackages?.length) {
                            console.log('reduxPackages >>> ', updateData);
                            await setFilterPackages(updateData);
                            await setIsLoading(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Region */}
            {reduxRegions?.length === 0
              ? null
              : reduxRegions.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            reduxRegions.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === reduxRegions?.length) {
                            console.log('reduxRegions >>> ', updateData);
                            await setFilterRegions(updateData);
                            await setIsLoading(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Capacity */}
            {reduxCapacities?.length === 0
              ? null
              : reduxCapacities.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            reduxCapacities.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (updateData?.length === reduxCapacities?.length) {
                            console.log('reduxCapacities >>> ', updateData);
                            await setFilterCapacities(updateData);
                            await setIsLoading(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}

            {/* Availability */}
            {reduxAvailabilities?.length === 0
              ? null
              : reduxAvailabilities.map((item, i) => {
                  if (item.selected) {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          const updateData = await Promise.all(
                            reduxAvailabilities.map((d, idx) => {
                              if (idx === i) {
                                return {
                                  ...d,
                                  selected: false,
                                };
                              } else {
                                return {
                                  ...d,
                                };
                              }
                            }),
                          );

                          if (
                            updateData?.length === reduxAvailabilities?.length
                          ) {
                            console.log('reduxAvailabilities >>> ', updateData);
                            await setFilterAvailabilities(updateData);
                            await setIsLoading(true);
                          }
                        }}
                        style={{
                          marginHorizontal: 7.5,
                          borderWidth: 1,
                          borderRadius: 25,
                          borderColor: headerBorderBottom,
                          backgroundColor: white,
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: superGrey,
                            letterSpacing: 0.3,
                          }}>
                          {item?.name}
                        </Text>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{
                            marginLeft: 5,
                            top: 1,
                            fontSize: 15,
                            color: superGrey,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  } else {
                    return null;
                  }
                })}
          </ScrollView>
        </View>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={async () => {
              await setIsLoading(true);
              await setIsError(false);
              await fetchProducts();
            }}
          />
        }
        ListFooterComponent={() => {
          return (
            <Card transparent>
              <CardItem>
                <View
                  style={{
                    opacity: isLoadMore ? 1 : 0,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator
                    size="small"
                    color={mainGreen}
                    style={{marginVertical: 5}}
                  />
                  <Text
                    style={{
                      fontStyle: 'italic',
                      fontSize: 12,
                      color: greyLine,
                      textAlign: 'center',
                    }}>
                    Loading more...
                  </Text>
                </View>
              </CardItem>
            </Card>
          );
        }}
        ListEmptyComponent={() => {
          if (!isLoading) {
            return (
              <View
                style={{
                  padding: 15,
                  height: height / 1.31,
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={charOfferNotAvailable}
                  style={{width, height: height / 3}}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginVertical: 15,
                    fontFamily: medium,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                  }}>
                  No Product Found Yet.
                </Text>
                <Text
                  style={{
                    lineHeight: 20,
                    fontFamily: book,
                    color: black,
                    textAlign: 'center',
                    fontSize: RFPercentage(2),
                  }}>
                  Keep updated with us, preparing our product very soon !
                </Text>
              </View>
            );
          } else {
            return null;
          }
        }}
        onEndReachedThreshold={0.001}
        legacyImplementation
        decelerationRate="fast"
        onEndReached={async () => {
          if (!isLoadMore) {
            await setIsLoadMore(true);
          }
        }}
        data={list}
        extraData={list}
        keyExtractor={item => `${item.id}`}
        renderItem={({item, index}) => {
          return (
            <CardProduct
              isLogin={isLogin}
              runWishList={(productId, isWishlist) =>
                runWishList(productId, isWishlist)
              }
              onPress={productId => onPress(productId)}
              {...props}
              item={item}
              index={index}
              {...props}
            />
          );
        }}
      />
    </View>
  );
};

const mapToState = state => {
  console.log('State REDUX YO MAN: ', state);
  const {venueAndHotel} = state;
  return {
    ...venueAndHotel,
  };
};

const mapToDispatch = dispatch => {
  return {
    setFilterPrices: prices => dispatch(thunkPrices(prices)),
    setFilterShortPrices: shortBy => dispatch(thunkShortPrices(shortBy)),
    setFilterPackages: packages => dispatch(thunkPackages(packages)),
    setFilterRegions: regions => dispatch(thunkRegions(regions)),
    setFilterCapacities: capacities => dispatch(thunkCapacities(capacities)),
    setFilterAvailabilities: availabilities =>
      dispatch(thunkAvailabilities(availabilities)),
  };
};

// HEADER
export const Headers = props => {
  const {title, backPress, searchPress, homePress} = props;

  return (
    <Header
      iosBarStyle={'dark-content'}
      androidStatusBarColor={white}
      translucent={false}
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.3}}>
        <HeaderButton
          iconType={'Feather'}
          iconName={'chevron-left'}
          onPress={backPress}
        />
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          {title}
        </Text>
      </Body>
      <Right
        style={{
          flex: 0.3,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        {/* <HeaderButton
            iconType={'Feather'}
            iconName={'search'}
            onPress={searchPress}
          /> */}
        <Button
          onPress={searchPress}
          style={{
            // display: 'none', // new add
            borderRadius: 35 / 2,
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Image
            source={charFilterIcon}
            style={{width: 20, height: 20}}
            resizeMode="contain"
          />
        </Button>
        <HeaderButton
          iconType={'Feather'}
          iconName={'home'}
          onPress={homePress}
        />
      </Right>
    </Header>
  );
};

export const HeaderButton = props => {
  const {onPress, iconType, iconName} = props;

  return (
    <Button
      onPress={onPress}
      style={{
        elevation: 0,
        shadowOpacity: 0,
        borderRadius: 35 / 2,
        backgroundColor: white,
        paddingTop: 0,
        paddingBottom: 0,
        height: 35,
        width: 35,
        justifyContent: 'center',
      }}>
      <Icon
        type={iconType}
        name={iconName}
        style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
      />
    </Button>
  );
};

const connector = connect(
  mapToState,
  mapToDispatch,
)(ListPackagesAfterFilter);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
