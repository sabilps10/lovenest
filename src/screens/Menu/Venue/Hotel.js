import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  RefreshControl,
  ActivityIndicator,
  ScrollView,
  Animated,
  SafeAreaView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {VenueAndHotelImages} from '../../../utils/Themes/Images';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import AsyncImage from '../../../components/Image/AsyncImage';
import {Card, CardItem} from 'native-base';

// Component
import ImageCard from './Components/ImageCard';
import LeftNavButton from './Components/LeftNavButton';
import HeaderImage from './Components/HeaderImage';
import ButtonScrollTo from './Components/ScrollToTopButton';
import {hasNotch} from 'react-native-device-info';

const {width, height} = Dimensions.get('window');
const {medium, book} = FontType;
const {white, black, mainRed, greyLine, mainGreen} = Colors;
const {
  venueWhiteIcon,
  venueBlackIcon,
  hotelWhiteIcon,
  hotelBlackIcon,
  venueImg,
  hotelImg,
  venueBg,
} = VenueAndHotelImages;

// Queries
import GET_PACKAGE from '../../../graphql/queries/getMerchantsByDisplayCard';

const Hotel = props => {
  console.log('Hotel Props: ', props);
  const {
    buttonSelected,
    onChangeButtonSelected,
    navigation,
    client,
    serviceType,
  } = props;

  const scrollRef = useRef(null);

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [pageSize, setPageSize] = useState(5);
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchPackage();

    // if (isLoadMore) {
    //   onLoadMore();
    // }
    const subs = navigation.addListener('focus', () => {
      fetchPackage();
      // if (isLoadMore) {
      //   onLoadMore();
      // }
    });

    return subs;
  }, [buttonSelected]);

  const onLoadMore = async () => {
    try {
      await setIsLoadMore(true);
      await setPageSize(pageSize + 10);
      await fetchPackage();
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const fetchPackage = async () => {
    try {
      await client
        .query({
          query: GET_PACKAGE,
          variables: {
            serviceType: 'Hotel',
            itemDisplayed: pageSize,
            pageNumber,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('Response Fetch Package Hotel: ', response);
          const {data, errors} = response;
          const {getMerchantsByDisplayCard} = data;
          const {data: packagesList, error} = getMerchantsByDisplayCard;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
            } else {
              await setList([...packagesList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
    }
  };

  const scrollToTop = () => {
    try {
      console.log('Pressed Brother >>><<<<');
      scrollRef.current.scrollToOffset({animated: true, offset: 0});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View
      style={{
        zIndex: -10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {list?.length === 0 ? null : (
        <ButtonScrollTo onPress={() => scrollToTop()} />
      )}
      <FlatList
        ref={scrollRef}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={async () => {
              await setIsLoading(true);
              await setIsError(false);
              await fetchPackage();
            }}
          />
        }
        disableVirtualization
        legacyImplementation
        decelerationRate="fast"
        onEndReachedThreshold={0.1}
        onEndReached={() => setIsLoadMore(false)}
        onMomentumScrollBegin={() => setIsLoadMore(false)}
        onMomentumScrollEnd={() => {
          setIsLoadMore(true);
          onLoadMore();
        }}
        ListFooterComponent={() => {
          return (
            <View
              style={{
                opacity: isLoadMore ? 1 : 0,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                // borderWidth: 1,
              }}>
              <ActivityIndicator
                size="small"
                color={mainGreen}
                style={{marginHorizontal: 5}}
              />
              <Text
                style={{
                  fontStyle: 'italic',
                  fontSize: 12,
                  color: greyLine,
                  textAlign: 'center',
                }}>
                Loading more...
              </Text>
            </View>
          );
        }}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={() => {
          return (
            <HeaderImage
              serviceType={serviceType}
              title={'Speak your beautiful promise in Our Hotels'}
              source={hotelImg}
              buttonSelected={buttonSelected}
              onChangeButtonSelected={text => onChangeButtonSelected(text)}
            />
          );
        }}
        contentContainerStyle={{
          padding: 15,
          paddingBottom: 25,
          zIndex: 1,
        }}
        data={list}
        extraData={list}
        keyExtractor={item => `${item.id}`}
        renderItem={({item, index}) => {
          console.log('ITEMSSS HOTELLLL: ', item);
          const {
            id,
            name,
            displayCard,
            coverImageDynamicUrl,
            coverImageUrl,
          } = item;
          const {dynamicUrl, url} = displayCard;
          const source =
            !dynamicUrl && !url
              ? coverImageDynamicUrl
                ? {uri: `${coverImageDynamicUrl}=h500`}
                : {uri: `${coverImageUrl}`}
              : dynamicUrl
              ? {uri: `${dynamicUrl}=h500`}
              : {uri: `${url}`};
          return (
            <ImageCard
              {...props}
              id={id}
              item={item}
              title={name}
              serviceType={'Hotel'}
              sourceImage={source}
              onPressRates={() => {
                try {
                  if (item?.rateCards?.length === 0 || !item?.rateCards) {
                    return null;
                  } else {
                    navigation.push('RateCard', {
                      rates: item?.rateCards,
                      merchantName: item?.name,
                      serviceType: item?.serviceType,
                    });
                  }
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              onPress={() => {
                navigation.push('VenueMerchantDetail', {
                  id,
                  serviceType: 'Hotel',
                });
              }}
              onPressPromotions={() => {
                try {
                  navigation.navigate('VenueMerchantDetail', {
                    id,
                    serviceType: 'Venue',
                  });
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
            />
          );
        }}
      />
    </View>
  );
};

const Wrapper = compose(withApollo)(Hotel);

export default props => <Wrapper {...props} />;
