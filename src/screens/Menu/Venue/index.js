import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Animated,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Header, Body, Left, Right, Icon, Button} from 'native-base';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import Packages from '../../Merchant/Package/Package';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {VenueAndHotelImages} from '../../../utils/Themes/Images';

// Screens
import Venues from './Venue';
import Hotels from './Hotel';

// Components
import Headers from './Components/Headers';

const {
  venueWhiteIcon,
  venueBlackIcon,
  hotelWhiteIcon,
  hotelBlackIcon,
  venueImg,
  hotelImg,
} = VenueAndHotelImages;
const {
  black,
  white,
  greyLine,
  transparent,
  headerBorderBottom,
  mainRed,
  mainGreen,
} = Colors;
const {medium, italiano} = FontType;
const {regular} = FontSize;
const {width, height} = Dimensions.get('window');

import {CommonActions} from '@react-navigation/native';

const VenueAndHotel = props => {
  console.log('VenueAndHotel Props: ', props);
  const {navigation, positionYBottomNav} = props;

  const [buttonSelected, setButtonSelected] = useState('venue');

  useEffect(() => {
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }

    const subs = navigation.addListener('focus', () => {
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return () => {
      subs();
    };
  }, [navigation, buttonSelected]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onChangeButtonSelected = async pressedButton => {
    try {
      await setButtonSelected(pressedButton);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToSearchScreen = () => {
    try {
      // navigation.navigate('VenueAndHotelSearch');
      navigation.navigate('VenueAndHotelFilter', {
        id: null,
        serviceType: buttonSelected === 'venue' ? 'Venue' : 'Hotel',
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goHome = () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Container>
      <Headers
        title={buttonSelected === 'venue' ? 'Venue' : 'Hotel'}
        backPress={() => goBack()}
        searchPress={() => goToSearchScreen()}
        homePress={() => goHome()}
      />
      {buttonSelected === 'venue' ? (
        <Venues
          serviceType={'Venue'}
          {...props}
          buttonSelected={buttonSelected}
          onChangeButtonSelected={onChangeButtonSelected}
        />
      ) : (
        <Hotels
          serviceType={'Hotel'}
          {...props}
          buttonSelected={buttonSelected}
          onChangeButtonSelected={onChangeButtonSelected}
        />
      )}
    </Container>
  );
};

const Wrapper = compose(withApollo)(VenueAndHotel);

export default props => <Wrapper {...props} />;
