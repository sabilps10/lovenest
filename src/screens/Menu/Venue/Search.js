import React, {useState, useEffect} from 'react';
import {
  Text,
  TextInput,
  StatusBar,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  RefreshControl
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';

// Queries
import GetSuggesstionQuery from '../../../graphql/queries/merchantMostSearch';
import SearchMerchants from '../../../graphql/queries/searchMerchantVenueAndHotel';

import {CommonActions} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
const {white, black, greyLine, headerBorderBottom, mainGreen, mainRed} = Colors;
const {book, medium} = FontType;
const {charNoOrder} = charImage;

// Components
import ImageCard from './Components/ImageCard';

const SearchScreen = props => {
  const {navigation, client} = props;
  const [searchText, setSearchText] = useState('');
  const [isLoadingSearch, setIsLoadingSearch] = useState(false);

  const [isLoadingSuggesstion, setIsLoadingSuggesstion] = useState(true);
  const [isErrorSuggesstion, setIsErrorSuggesstion] = useState(false);
  const [suggesstion, setSuggesstion] = useState([]);

  const [listSearchResult, setListSearchResult] = useState([]);
  const [itemDisplayed, setItemDisplayed] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);

  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetchSuggesstion();
    if (isLoadMore) {
      onLoadMore();
    }
  }, [isLoadMore]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        await setItemDisplayed(itemDisplayed + 10);
        await fetchSearchAPI();
      }
    } catch (error) {
      await setIsLoadMore(false);
    }
  };

  const fetchSuggesstion = async () => {
    try {
      await client
        .query({
          query: GetSuggesstionQuery,
          variables: {
            serviceType: ['Venue', 'Hotel'],
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetch suggesstion response: ', response);
          const {data, errors} = response;
          const {merchantMostSearch} = data;
          const {data: list, error} = merchantMostSearch;

          if (errors) {
            await setIsErrorSuggesstion(true);
            await setIsLoadingSuggesstion(false);
          } else {
            if (error) {
              await setIsErrorSuggesstion(true);
              await setIsLoadingSuggesstion(false);
            } else {
              await setSuggesstion([...list]);
              await setIsErrorSuggesstion(false);
              await setIsLoadingSuggesstion(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorSuggesstion(true);
      await setIsLoadingSuggesstion(false);
    }
  };

  const onChangeText = async e => {
    try {
      await setSearchText(e);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const fetchSearchAPI = async () => {
    try {
      await client
        .query({
          query: SearchMerchants,
          variables: {
            searchBy: searchText,
            serviceTypes: ['Venue', 'Hotel'],
            pageNumber,
            itemDisplayed,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Search Result Response: ', response);
          const {data, errors} = response;
          const {getMerchantsByDisplayCard} = data;
          const {data: list, error} = getMerchantsByDisplayCard;

          if (errors) {
            await setIsLoadingSearch(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsLoadingSearch(false);
              await setIsLoadMore(false);
            } else {
              await setListSearchResult([...list]);
              await setIsLoadingSearch(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingSearch(false);
      await setIsLoadMore(false);
    }
  };

  const onSubmit = async () => {
    try {
      if (searchText === '') {
        // not running search
        await setListSearchResult([]);
      } else {
        await setIsLoadingSearch(true);
        await fetchSearchAPI();
      }
    } catch (error) {
      console.log('error: ', error);
      await setIsLoadingSearch(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goHome = () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Container>
      <SearchHeader
        {...props}
        goBack={() => goBack()}
        goHome={() => goHome()}
        onSubmit={() => onSubmit()}
        value={searchText}
        onChangeSearch={e => onChangeText(e)}
        isLoading={isLoadingSearch}
      />
      {listSearchResult?.length === 0 ? (
        <HeaderList
          {...props}
          data={suggesstion}
          isLoading={isLoadingSuggesstion}
          isError={isErrorSuggesstion}
        />
      ) : (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={isLoadingSearch}
              onRefresh={async () => {
                await setIsLoadingSearch(true);
                await setItemDisplayed(10);
                await fetchSearchAPI();
              }}
            />
          }
          disableVirtualization
          onEndReachedThreshold={0.1}
          legacyImplementation
          decelerationRate="fast"
          onEndReached={async () => {
            if (!isLoadMore) {
              await setIsLoadMore(true);
            }
          }}
          ListFooterComponent={() => {
            return (
              <Card transparent>
                <CardItem>
                  <View
                    style={{
                      opacity: isLoadMore ? 1 : 0,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator
                      size="small"
                      color={mainGreen}
                      style={{marginVertical: 5}}
                    />
                    <Text
                      style={{
                        fontStyle: 'italic',
                        fontSize: 12,
                        color: greyLine,
                        textAlign: 'center',
                      }}>
                      Loading more...
                    </Text>
                  </View>
                </CardItem>
              </Card>
            );
          }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            padding: 15,
            paddingBottom: 25,
          }}
          data={listSearchResult}
          extraData={listSearchResult}
          keyExtractor={item => `${item.id}`}
          renderItem={({item, index}) => {
            const {
              id,
              name,
              displayCard,
              coverImageDynamicUrl,
              coverImageUrl,
            } = item;
            const {dynamicUrl, url} = displayCard;
            const source =
              !dynamicUrl && !url
                ? coverImageDynamicUrl
                  ? {uri: `${coverImageDynamicUrl}=h500`}
                  : {uri: `${coverImageUrl}`}
                : dynamicUrl
                ? {uri: `${dynamicUrl}=h500`}
                : {uri: `${url}`};
            return (
              <ImageCard
                {...props}
                id={id}
                title={name}
                serviceType={'Venue'}
                sourceImage={source}
                onPress={() => {
                  navigation.push('VenueMerchantDetail', {
                    id,
                    serviceType: 'Venue',
                  });
                }}
              />
            );
          }}
        />
      )}
    </Container>
  );
};

export const HeaderList = props => {
  const {data, isLoading, isError} = props;

  if (isLoading && !isError) {
    return (
      <Card transparent>
        <CardItem
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color={mainRed} />
        </CardItem>
      </Card>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (data?.length === 0 && !isLoading) {
      return <EmptyResult />;
    } else {
      return (
        <FlatList
          contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
          data={data}
          extraData={data}
          keyExtractor={(item, index) => `${item.serviceType}`}
          renderItem={({item, index}) => {
            return (
              <Card transparent style={{marginBottom: 0}}>
                <CardItem style={{paddingBottom: 0}}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(1.8),
                      letterSpacing: 0.3,
                    }}>
                    Most Search {item?.serviceType}
                  </Text>
                </CardItem>
                <CardItem style={{width: '100%', flexWrap: 'wrap'}}>
                  <SubList
                    client={props?.client}
                    navigation={props?.navigation}
                    data={item?.merchants}
                  />
                </CardItem>
              </Card>
            );
          }}
        />
      );
    }
  }
};

export const SubList = props => {
  const {data, navigation} = props;

  const goToMerchantDetail = (id, serviceType) => {
    try {
      navigation.navigate('VenueMerchantDetail', {id, serviceType});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <FlatList
      data={data}
      extraData={data}
      legacyImplementation
      scrollEnabled={false}
      // horizontal
      numColumns={20}
      showsHorizontalScrollIndicator={false}
      columnWrapperStyle={{flexWrap: 'wrap'}}
      contentContainerStyle={{
        // borderWidth: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
      }}
      keyExtractor={(item, index) => `${item.id}`}
      renderItem={({item, index}) => {
        return (
          <TouchableOpacity
            onPress={() => goToMerchantDetail(item?.id, item?.serviceType)}
            style={{
              marginBottom: 10,
              marginRight: 10,
              padding: 5,
              paddingLeft: 10,
              paddingRight: 10,
              borderWidth: 1,
              borderColor: mainRed,
              backgroundColor: '#FFEDED',
              borderRadius: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: mainRed,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
              }}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

export const EmptyResult = props => {
  return (
    <View
      style={{
        padding: 15,
        flex: 1,
        height: height / 1.09,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={charNoOrder}
        style={{bottom: 90, width, height: height / 3}}
        resizeMode="contain"
      />
      <Text
        style={{
          bottom: 75,
          fontFamily: medium,
          color: black,
          textAlign: 'center',
          fontSize: RFPercentage(2.5),
        }}>
        No Result Yet
      </Text>
      <Text
        style={{
          lineHeight: 20,
          bottom: 60,
          fontFamily: book,
          color: black,
          textAlign: 'center',
          fontSize: RFPercentage(2),
        }}>
        Type your search or use another key search!
      </Text>
    </View>
  );
};

export const SearchHeader = props => {
  const {goBack, goHome, value, onChangeSearch, isLoading, onSubmit} = props;
  return (
    <Header
      androidStatusBarColor={white}
      iosBarStyle="dark-content"
      style={{
        shadowOpacity: 0,
        elevation: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        backgroundColor: white,
      }}>
      <Left
        style={{
          flex: 0.2,
          borderWidth: 0,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Button
          onPress={() => goBack()}
          style={{
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            borderRadius: 35 / 2,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{
              right: 4.5,
              marginLeft: 0,
              marginRight: 0,
              fontSize: 24,
              color: black,
            }}
          />
        </Button>
      </Left>
      <Body style={{flex: 1}}>
        <View
          style={{
            width: '100%',
            height: 35,
            borderWidth: 1,
            borderRadius: 2,
            borderColor: greyLine,
          }}>
          <TextInput
            onSubmitEditing={() => onSubmit()}
            returnKeyLabel={'Search'}
            returnKeyType="search"
            value={value}
            placeholder="Search"
            onChangeText={e => onChangeSearch(e)}
            style={{
              padding: 2,
              paddingTop: 0.5,
              paddingLeft: 5,
              paddingRight: 5,
              borderWidth: 0,
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}
          />
        </View>
      </Body>
      <Right
        style={{
          flex: 0.3,
          borderWidth: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingLeft: 5,
        }}>
        <Button
          onPress={() => onSubmit()}
          disabled={isLoading ? true : false}
          style={{
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: white,
            borderRadius: 35 / 2,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          {isLoading ? (
            <ActivityIndicator size="small" color={mainGreen} />
          ) : (
            <Icon
              type="Feather"
              name="search"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          )}
        </Button>
        <Button
          onPress={() => goHome()}
          style={{
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: white,
            borderRadius: 35 / 2,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Icon
            type="Feather"
            name="home"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(SearchScreen);

export default props => <Wrapper {...props} />;
