import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Modal,
  SafeAreaView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import VideoPlayer from '../Components/VideoPlayer';
import Galleries from '../Components/ImageGalleries';
import PopularProduct from '../Components/PopularProducts';
import {hasNotch} from 'react-native-device-info';

// Queries
import GET_MERCHANT_DETAIL from '../../../../graphql/queries/getMerchantVer2';
import GET_POPULAR_PRODUCT from '../../../../graphql/queries/floristPopularProduct';
import GET_PROMOTION from '../../../../graphql/queries/getAllPromotionsByMerchantId';

const {width, height} = Dimensions.get('window');
const {white, black, overlayDim, mainRed, mainGreen} = Colors;
const {book, medium} = FontType;

// Mutation
import MutationAddWishList from '../../../../graphql/mutations/addWishlist';
import MutationRemoveWishList from '../../../../graphql/mutations/removeWishlist';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../../../utils/AsyncstorageDataStructure';
const {asyncToken} = AsyncData;

import ListPromotion from '../Components/ListPromotion';
import ModalLoader from '../Components/ModalLoader';

import {TabActions, CommonActions} from '@react-navigation/native';

const Home = props => {
  console.log('Home Tab Venue And Hotel: ', props);
  const {id, serviceType, navigation, client, route} = props;

  const [isLogin, setIsLogin] = useState(false);
  const [isLoadingWishList, setIsLoadingIsWishList] = useState(false);

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [isLoadingPopularProduct, setIsLoadingPopularProduct] = useState(true);
  const [isErrorPopularProduct, setIsErrorPopularProduct] = useState(false);
  const [listPopularProduct, setListPopularProduct] = useState([]);

  const [listPromotion, setListPromotion] = useState([]);
  const [isLoadingPromotion, setIsLoadingPromotion] = useState(true);
  const [isErrorPromotion, setIsErrorPromotion] = useState(false);
  const [itemDisplayedPromotion, setItemDisplayedPromotion] = useState(1);

  useEffect(() => {
    runCheckingLogin();
    fetchMerchantDetail();
    fetchPopularProduct();
    fetchPromotion();

    const subs1 = navigation.addListener('focus', () => {
      runCheckingLogin();
      fetchMerchantDetail();
      fetchPopularProduct();
      fetchPromotion();
    });

    // const subs2 = navigation.addListener('tabPress', () => {
    //   runCheckingLogin();
    //   fetchMerchantDetail();
    //   fetchPopularProduct();
    //   fetchPromotion();
    // });

    return () => {
      subs1();
      // subs2();
    };
  }, [navigation, id, serviceType]);

  const fetchPromotion = async () => {
    try {
      await client
        .query({
          query: GET_PROMOTION,
          variables: {
            upcoming: true,
            merchantId: parseInt(id, 10),
            itemDisplayed: parseInt(itemDisplayedPromotion, 10),
            pageNumber: 1,
          },
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchPromotion: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: list, error} = getAllPromotions;

          if (errors) {
            await setIsErrorPromotion(true);
            await setIsLoadingPromotion(false);
          } else {
            if (error) {
              await setIsErrorPromotion(true);
              await setIsLoadingPromotion(false);
            } else {
              await setListPromotion([...list]);
              await setIsErrorPromotion(false);
              await setIsLoadingPromotion(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPromotion(true);
      await setIsLoadingPromotion(false);
    }
  };

  const runWishList = async (productId, statusWhislist) => {
    try {
      if (statusWhislist) {
        // this should be un whislist
        await setIsLoadingIsWishList(true);
        await removeWishlistMutation(productId);
      } else {
        // this is should be wishlist
        await setIsLoadingIsWishList(true);
        await addWishListMutation(productId);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingIsWishList(false);
    }
  };

  const addWishListMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationAddWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {addWishlist} = data;
          const {error} = addWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const removeWishlistMutation = productId => {
    try {
      client
        .mutate({
          mutation: MutationRemoveWishList,
          variables: {
            productId: parseInt(productId, 10),
          },
        })
        .then(async response => {
          console.log('response add wishlist: ', response);
          const {data, errors} = response;
          const {removeWishlist} = data;
          const {error} = removeWishlist;

          if (errors) {
            await setIsLoadingIsWishList(false);
          } else {
            if (error) {
              await setIsLoadingIsWishList(false);
            } else {
              await fetchPopularProduct();
              await setIsLoadingIsWishList(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsLoadingIsWishList(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingIsWishList(false);
    }
  };

  const checkIsLogin = () => {
    return new Promise(async resolve => {
      try {
        const getToken = await AsyncStorage.getItem(asyncToken);
        if (getToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        resolve(false);
      }
    });
  };

  const runCheckingLogin = async () => {
    try {
      const loginStatus = await checkIsLogin();
      if (loginStatus) {
        await setIsLogin(true);
      } else {
        await setIsLogin(false);
      }
    } catch (error) {
      await setIsLogin(false);
    }
  };

  const fetchPopularProduct = async () => {
    try {
      await client
        .query({
          query: GET_POPULAR_PRODUCT,
          variables: {
            serviceType,
            merchantId: [id],
            pageSize: 3,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetch popular product response: ', response);
          const {data, errors} = response;
          const {productsPopular} = data;
          const {data: list, error} = productsPopular;

          if (errors) {
            await setIsErrorPopularProduct(true);
            await setIsLoadingPopularProduct(false);
          } else {
            if (error) {
              await setIsErrorPopularProduct(true);
              await setIsLoadingPopularProduct(false);
            } else {
              await setListPopularProduct([...list]);
              await setIsErrorPopularProduct(false);
              await setIsLoadingPopularProduct(false);
            }
          }
        })
        .catch(error => {
          console.log('Error fetch Popular Product: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error Fetch Popular Product: ', error);
      await setIsErrorPopularProduct(true);
      await setIsLoadingPopularProduct(false);
    }
  };

  const fetchMerchantDetail = async () => {
    try {
      await client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('response fetch merchant detail: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <ModalLoader isLoading={isLoadingWishList} />
      <ScrollView contentContainerStyle={{}}>
        <VideoPlayer
          url={
            detail[0]?.video[0]?.url ? {uri: detail[0]?.video[0]?.url} : null
          }
          isLoading={isLoading}
          isError={isError}
        />
        <Galleries images={detail[0]?.extraPhoto} />
        <PopularProduct
          detail={detail}
          isLogin={isLogin}
          runWishList={(productId, statusWhislist) =>
            runWishList(productId, statusWhislist)
          }
          {...props}
          list={listPopularProduct}
          isLoading={isLoadingPopularProduct}
          isError={isErrorPopularProduct}
        />
        <ListPromotion
          {...props}
          detail={detail}
          list={listPromotion}
          isLoading={isLoadingPromotion}
          isError={isErrorPromotion}
        />
        <TouchableOpacity
          onPress={() => {
            try {
              navigation.navigate('Enquiry', {id, serviceType});
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
          style={{
            marginTop: 15,
            justifyContent: 'center',
            alignItems: 'center',
            width,
            height: hasNotch() ? 60 : 55,
            backgroundColor: mainGreen,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: white,
            }}>
            ENQUIRY NOW
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const Wrapper = compose(withApollo)(Home);

export default props => <Wrapper {...props} />;
