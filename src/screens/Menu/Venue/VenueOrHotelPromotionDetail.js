import React, {useState, useEffect} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import AsyncImage from '../../../components/Image/AsyncImage';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import moment from 'moment';
import {charImage} from '../../../utils/Themes/Images';
import NonElepsisDesc from '../../../components/Event/EventDescription/nonElepsisDescription';

import {CommonActions} from '@react-navigation/native';

// Components
import Header from '../../../components/Header/Common/index';

// Query get Promo Detail by Promo Id
import GET_PROMO_DETAIL from '../../../graphql/queries/getPromotion';

const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  mainGreen,
  mainRed,
  greyLine,
  superGrey,
  headerBorderBottom,
} = Colors;
const {book, medium} = FontType;
const {charMainGreenStoreIcon} = charImage;

const VenueAndHotelPromotionDetail = props => {
  console.log('VenueAndHotelPromotionDetail: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetch();
    const subs = navigation.addListener('focus', () => {
      fetch();
    });

    return subs;
  }, []);

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_PROMO_DETAIL,
          variables: {
            id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response Special Promo Detail: ', response);
          const {data, errors} = response;
          const {getPromotion} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            await setDetail([...getPromotion]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onRefreshButton = async () => {
    try {
      await setIsError(false);
      await setIsLoading(true);
      await setRefreshing(false);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goHome = async () => {
    try {
      await navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <View style={{zIndex: 999}}>
          <Header
            title="Promotions"
            buttonLeft={() => goBack()}
            buttonRight={() => goHome()}
            showRightButton={true}
          />
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <View style={{zIndex: 999}}>
          <Header
            title="Promotions"
            buttonLeft={() => goBack()}
            buttonRight={() => goHome()}
            showRightButton={true}
          />
        </View>
        <Content
          contentContainerStyle={{flex: 1}}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>Failed to Load Data</Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <Container>
        <View style={{zIndex: 999}}>
          <Header
            title="Promotions"
            buttonLeft={() => goBack()}
            buttonRight={() => goHome()}
            showRightButton={true}
          />
        </View>
        <Content
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View style={{width, height: height / 3.5}}>
            <AsyncImage
              source={
                detail[0]?.promoImageDynamicURL
                  ? {uri: `${detail[0].promoImageDynamicURL}=h500`}
                  : {uri: `${detail[0].promoImageURL}`}
              }
              placeholderColor="white"
              style={{flex: 1, width: '100%', height: height / 3.5}}
              resizeMode="cover"
              loaderStyle={{width: width / 7, height: height / 7}}
            />
          </View>
          <Card transparent>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2.2),
                  letterSpacing: 0.3,
                  color: black,
                }}>
                {detail[0]?.name ? detail[0]?.name : ''}
              </Text>
            </CardItem>
            <CardItem>
              {moment(
                moment(detail[0]?.startDate)
                  .utc()
                  .format('YYYY-MM-DD'),
              ).isSame(
                moment(detail[0]?.endDate)
                  .utc()
                  .format('YYYY-MM-DD'),
              ) ? (
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                    color: greyLine,
                  }}>
                  {moment(detail[0]?.endDate)
                    .utc()
                    .format('ddd, DD MMMM YYYY')}
                </Text>
              ) : (
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                    color: greyLine,
                  }}>
                  {moment(detail[0]?.start)
                    .utc()
                    .format('DD MMM')}{' '}
                  -{' '}
                  {moment(detail[0]?.endDate)
                    .utc()
                    .format('DD MMMM YYYY')}
                </Text>
              )}
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  borderWidth: 1,
                  borderColor: mainGreen,
                  borderRadius: 15,
                  padding: 15,
                  paddingTop: 5,
                  paddingBottom: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={charMainGreenStoreIcon}
                  style={{width: 15, height: 15, marginRight: 5}}
                />
                <Text>{detail[0]?.merchantName}</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  type="Feather"
                  name="clock"
                  style={{left: 10, fontSize: 15, color: mainRed}}
                />
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.6),
                    letterSpacing: 0.3,
                    color: mainRed,
                  }}>
                  {moment(detail[0]?.endDate).diff(moment(), 'days')} Day(s)
                  Remaining
                </Text>
              </View>
            </CardItem>
            <CardItem>
              <NonElepsisDesc
                smallText={true}
                description={detail[0]?.description}
              />
            </CardItem>
          </Card>
        </Content>
        <ButtonFooter
          onPress={() => navigation.navigate('SpecialOfferForm', {id})}
        />
      </Container>
    );
  }
};

export const ButtonFooter = props => {
  const {onPress} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => onPress()}
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
                lineHeight: 18,
                fontFamily: medium,
              }}>
              SEND OFFER
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(VenueAndHotelPromotionDetail);

export default props => <Wrapper {...props} />;
