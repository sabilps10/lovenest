import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, TouchableOpacity, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Icon} from 'native-base';
import PackageCard from '../../../components/Cards/Merchants/PackageCard';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {medium} = FontType;
const {white, black, mainRed} = Colors;
// Query
import PUBLIC_QUERY from '../../../graphql/queries/productsPublic';

const PopularPackages = props => {
  console.log('PopularPackages Props: ', props);
  const {navigation, id, serviceType, client} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [popularPackages, setPopularPackages] = useState([]);
  const [pageSize, setPageSize] = useState(3);
  const [pageNumber, setPagNumber] = useState(1);

  useEffect(() => {
    fetch();
  }, [id, serviceType]);

  const fetch = () => {
    try {
      client
        .query({
          query: PUBLIC_QUERY,
          variables: {
            merchantId: id,
            serviceType,
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: packageList, error, totalData} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setPopularPackages([...packageList]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          top: 25,
          marginVertical: 50,
        }}>
        <Text>Loading...</Text>
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <Card transparent style={{ marginLeft: 0, marginRight: 0}}>
        <CardItem style={{width: '100%'}}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              paddingLeft: 5,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              Popular Packages
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.push('Packages', {id, serviceType});
              }}
              style={{
                left: 10,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  right: 5,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.6),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                See All
              </Text>
              <Icon
                type="Feather"
                name="arrow-right"
                style={{fontSize: RFPercentage(2.2), color: mainRed}}
              />
            </TouchableOpacity>
          </View>
        </CardItem>
        <ListPackages id={id} list={popularPackages} navigation={navigation} />
      </Card>
    );
  }
};

export const ListPackages = props => {
  const {id, list, navigation} = props;
  const extKey = (item, index) => `${String(item.id)} - Parent`;
  const RenderList = () =>
    useMemo(() => {
      return (
        <FlatList
          contentContainerStyle={{
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 15,
          }}
          initialNumToRender={10}
          decelerationRate="fast"
          data={list}
          extraData={list}
          disableVirtualization
          legacyImplementation
          listKey={extKey}
          keyExtractor={extKey}
          renderItem={({item, index}) => {
            return (
              <PackageCardItem
                item={item}
                label={'Package'}
                merchantId={id}
                navigation={navigation}
              />
            );
          }}
        />
      );
    }, [list]);
  if (list) {
    if (list.length === 0) {
      return null;
    } else {
      return RenderList();
    }
  } else {
    return null;
  }
};

export const PackageCardItem = props => {
  const {item, label, merchantId, navigation} = props;
  const RenderCard = () =>
    useMemo(() => {
      return (
        <PackageCard
          item={item}
          label={label}
          merchantId={merchantId}
          navigation={navigation}
        />
      );
    }, []);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(PopularPackages);

export default props => <Wrapper {...props} />;
