import React, {useState} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Icon,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');
const {white, black, greyLine, superGrey, mainRed, mainGreen} = Colors;
const {book, medium} = FontType;

// Components
import Headers from './Components/Headers';

import {CommonActions} from '@react-navigation/native';

import ModalImageAttatchmentViewer from './Components/ImageViewer';

const Rates = props => {
  console.log('RATES props: ', props);
  const {route, navigation, client} = props;
  const {params} = route;
  const {rates, merchantName, serviceType} = params;

  const [selectedItem, setSelectedItem] = useState([]);
  const [openImageAttatchmentViewer, setOpenImageAttachmentViewer] = useState(
    false,
  );

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goHome = () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = item => `${item.id}`;

  const goToPDFReader = async attachment => {
    try {
      navigation.navigate('VenueAndHotelPDFPreview', {data: attachment});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const openGallery = async itemImage => {
    try {
      if (openImageAttatchmentViewer) {
        await setOpenImageAttachmentViewer(false);
        await setSelectedItem([]);
      } else {
        await setSelectedItem([itemImage]);
        await setOpenImageAttachmentViewer(true);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Container>
      <Headers
        disabledSearchIcon={true}
        title={merchantName}
        backPress={() => goBack()}
        searchPress={() => {}}
        homePress={() => goHome()}
      />
      <ModalImageAttatchmentViewer
        visible={openImageAttatchmentViewer}
        onPress={async () => {
          try {
            openGallery();
          } catch (error) {
            console.log('Error: ', error);
          }
        }}
        images={selectedItem}
      />
      <Content contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}>
        <FlatList
          ListHeaderComponent={() => {
            return (
              <Card transparent>
                <CardItem>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    Rate Card
                  </Text>
                </CardItem>
              </Card>
            );
          }}
          data={rates}
          extraData={rates}
          keyExtractor={keyExt}
          renderItem={({item, index}) => {
            return (
              <Card transparent style={{marginTop: 0}}>
                <CardItem button onPress={() => {
                  if(item?.mimetype === 'application/pdf') {
                    goToPDFReader(item)
                  } else {
                    // images
                    openGallery(item);
                  }
                }}>
                  <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{flex: 0.1}}>
                      <Icon
                        type="Feather"
                        name="file"
                        style={{fontSize: RFPercentage(2.5), color: mainRed}}
                      />
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          textDecorationLine: 'underline',
                          color: mainRed,
                          fontFamily: medium,
                          fontSize: RFPercentage(1.6),
                          letterSpacing: 0.3,
                          lineHeight: 18,
                        }}>
                        {item?.filename}
                      </Text>
                    </View>
                  </View>
                </CardItem>
              </Card>
            );
          }}
        />
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Rates);

export default props => <Wrapper {...props} />;
