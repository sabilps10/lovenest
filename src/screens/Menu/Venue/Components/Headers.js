import React from 'react';
import {Text, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {charImage} from '../../../../utils/Themes/Images';

const {charFilterIcon} = charImage;
const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  headerBorderBottom,
  greyLine,
  superGrey,
  mainRed,
  mainGreen,
} = Colors;
const {medium, book} = FontType;

const Headers = props => {
  const {title, backPress, searchPress, homePress, disabledSearchIcon} = props;

  return (
    <Header
      iosBarStyle={'dark-content'}
      androidStatusBarColor={white}
      translucent={false}
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.3}}>
        <HeaderButton
          iconType={'Feather'}
          iconName={'chevron-left'}
          onPress={backPress}
        />
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          {title}
        </Text>
      </Body>
      <Right
        style={{
          flex: 0.3,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        {/* <HeaderButton
          iconType={'Feather'}
          iconName={'search'}
          onPress={searchPress}
        /> */}
        <Button
          disabled={disabledSearchIcon ? true : false}
          onPress={searchPress}
          style={{
            // display: 'none', // new add
            borderRadius: 35 / 2,
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: 0,
            shadowOpacity: 0,
            opacity: disabledSearchIcon ? 0 : 1,
          }}>
          <Image
            source={charFilterIcon}
            style={{width: 20, height: 20}}
            resizeMode="contain"
          />
        </Button>
        <HeaderButton
          iconType={'Feather'}
          iconName={'home'}
          onPress={homePress}
        />
      </Right>
    </Header>
  );
};

export const HeaderButton = props => {
  const {onPress, iconType, iconName} = props;

  return (
    <Button
      onPress={onPress}
      style={{
        elevation: 0,
        shadowOpacity: 0,
        borderRadius: 35 / 2,
        backgroundColor: white,
        paddingTop: 0,
        paddingBottom: 0,
        height: 35,
        width: 35,
        justifyContent: 'center',
      }}>
      <Icon
        type={iconType}
        name={iconName}
        style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
      />
    </Button>
  );
};

const Wrapper = compose(withApollo)(Headers);

export default props => <Wrapper {...props} />;
