import React, {useMemo} from 'react';
import {Text, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Card, CardItem} from 'native-base';

const {width, height} = Dimensions.get('window');
const {white, black, mainRed, mainGreen, superGrey, greyLine} = Colors;
const {book, medium} = FontType;

// Component
import LeftNavButton from './LeftNavButton';

const HeaderImage = props => {
  const {
    title,
    source,
    serviceType,
    buttonSelected,
    onChangeButtonSelected,
  } = props;

  const RenderImage = () => {
    return (
      <View
        style={{
          width: '100%',
          height: width + 200,
          borderWidth: 0,
          paddingTop: 25,
          marginBottom: 15,
        }}>
        <Card transparent style={{zIndex: 1, marginBottom: 10}}>
          <CardItem
            style={{
              width: '100%',
              paddingLeft: 25,
              paddingRight: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                lineHeight: 25,
                fontFamily: medium,
                color: black,
                fontSize: RFPercentage(2.5),
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {title}
            </Text>
          </CardItem>
        </Card>
        <AsyncImage
          source={source}
          style={{flex: 1, width: '100%', height: '100%', zIndex: 1}}
          placeholderColor={white}
          resizeMode="cover"
          loaderStyle={{width: width / 7, height: height / 7}}
        />
        <View
          style={{
            zIndex: 99,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}>
          <LeftNavButton
            title={'VENUE'}
            onPress={() => {
              onChangeButtonSelected('venue');
            }}
            buttonSelected={buttonSelected}
          />
          <LeftNavButton
            title={'HOTEL'}
            onPress={() => {
              onChangeButtonSelected('hotel');
            }}
            buttonSelected={buttonSelected}
          />
        </View>
      </View>
    );
  };

  return RenderImage();
};

const Wrapper = compose(withApollo)(HeaderImage);

export default props => <Wrapper {...props} />;
