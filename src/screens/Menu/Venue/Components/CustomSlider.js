import React from 'react';
import {Text, StatusBar, Dimensions, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Loader from '../../../../components/Loader/JustLoader';
import {Card, CardItem} from 'native-base';

const {width, height} = Dimensions.get('window');
const {white, black, mainRed, greyLine} = Colors;
const {medium, book} = FontType;

const CustomSlider = props => {
  console.log('CUSTOM SLIDER PROPS: ', props);
  const {
    isLoading,
    isError,
    title,
    min,
    max,
    step,
    enabledOne,
    selectedStyle,
    unselectedStyle,
    customMarker,
    trackStyle,
    minMarkerOverlapDistance,
    values,
    allowOverlap,
    sliderLength,
    onValuesChange,
    decimal,
  } = props;

  if (isError) {
    return null;
  } else {
    return (
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                }}>
                {title}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              {isLoading ? (
                <View style={{width: width / 10, height: height / 15}}>
                  <Loader />
                </View>
              ) : null}
            </View>
          </View>
          {isLoading ? null : (
            <View style={{width: '100%', paddingLeft: 10, marginTop: 10}}>
              <View>
                <MultiSlider
                  min={Number(min)}
                  max={Number(max)}
                  step={step}
                  enabledOne={enabledOne}
                  selectedStyle={{
                    ...selectedStyle,
                  }}
                  unselectedStyle={{
                    ...unselectedStyle,
                  }}
                  customMarker={() => customMarker()}
                  trackStyle={{...trackStyle}}
                  minMarkerOverlapDistance={minMarkerOverlapDistance}
                  values={values}
                  snapped
                  allowOverlap={allowOverlap}
                  sliderLength={sliderLength}
                  onValuesChange={e => {
                    onValuesChange(e);
                  }}
                />
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      right: 8,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                    }}>
                    {decimal ? values[0].toFixed(1) : values[0]}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                    }}>
                    {decimal ? values[1].toFixed(1) : values[1]}
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>
      </CardItem>
    );
  }
};

const Wrapper = compose(withApollo)(CustomSlider);

export default props => <Wrapper {...props} />;
