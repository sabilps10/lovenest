import React from 'react';
import {Text, StatusBar, Dimensions, FlatList, Image, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Loader from '../../../../components/Loader/JustLoader';

import ButtonFiltered from '../../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFilter from '../../../../components/Button/ButtonFilters/Unfilter';

const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  mainRed,
  mainGreen,
  greyLine,
  superGrey,
  headerBorderBottom,
} = Colors;
const {medium, book} = FontType;

const CustomListFilter = props => {
  const {
    title,
    subtitle,
    data,
    isLoading,
    isError,
    onSelectItem,
    showIcon,
  } = props;

  const keyExt = (item, index) => `${index} Type`;

  if (isLoading && !isError) {
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {title}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View style={{width: width / 10, height: height / 15}}>
                <Loader />
              </View>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (data?.length === 0) {
      return null;
    }
    return (
      <Card transparent>
        <CardItem style={{width: '100%'}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                {title}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.4),
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                {subtitle}
              </Text>
            </View>
          </View>
        </CardItem>
        <CardItem
          style={{
            width: '100%',
            flexWrap: 'wrap',
            flexDirection: 'row',
          }}>
          <FlatList
            data={data}
            extraData={data}
            keyExtractor={keyExt}
            legacyImplementation
            scrollEnabled={false}
            // horizontal
            numColumns={20}
            showsHorizontalScrollIndicator={false}
            columnWrapperStyle={{flexWrap: 'wrap'}}
            contentContainerStyle={{
              // borderWidth: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              width: '100%',
            }}
            renderItem={({item, index}) => {
              if (item?.selected) {
                return (
                  <ButtonFiltered
                    showIcon={showIcon ? true : false}
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              } else {
                return (
                  <ButtonUnFilter
                    showIcon={showIcon ? true : false}
                    name={item?.name}
                    checked={item?.selected}
                    parentIndex={index}
                    checkList={(p, c) => onSelectItem(c)}
                    index={index}
                  />
                );
              }
            }}
          />
        </CardItem>
      </Card>
    );
  }
};

const Wrapper = compose(withApollo)(CustomListFilter);

export default props => <Wrapper {...props} />;
