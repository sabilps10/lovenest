import React from 'react';
import {Dimensions, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {VideoPlayer} from '../../../../components/VideoPlayer/views';

const {width, height} = Dimensions.get('window');

const VideoPlayerWrapper = props => {
  const {isLoading, isError, url} = props;

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (url) {
      return (
        <View
          style={
            Platform.OS === 'android'
              ? {
                  width,
                  height: height / 3.5,
                  backgroundColor: 'red',
                  marginBottom: 3,
                }
              : {
                  width: Dimensions.get('window').width,
                  height: Dimensions.get('window').width * (9 / 14.6),
                  marginBottom: 3,
                }
          }>
          <VideoPlayer url={url} />
        </View>
      );
    } else {
      return null;
    }
  }
};

const Wrapper = compose(withApollo)(VideoPlayerWrapper);

export default props => <Wrapper {...props} />;
