import React from 'react';
import {
  Modal,
  ActivityIndicator,
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';

const {mainGreen, white} = Colors;

const ModalLoader = props => {
  const {isLoading} = props;

  if (isLoading) {
    return (
      <>
        <Modal visible={isLoading} transparent animationType="fade">
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}>
            <View
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,

                elevation: 3,
                backgroundColor: white,
                borderRadius: 4,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 15,
              }}>
              <ActivityIndicator color={mainGreen} />
            </View>
          </View>
        </Modal>
      </>
    );
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(ModalLoader);

export default props => <Wrapper {...props} />;
