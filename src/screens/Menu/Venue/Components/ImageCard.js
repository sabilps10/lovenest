import React, {useRef} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  Animated,
} from 'react-native';
import {Rating, AirbnbRating} from 'react-native-elements';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {Card, CardItem, Icon, Button} from 'native-base';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {charImage} from '../../../../utils/Themes/Images';

const {charRatingStar: ratingStar} = charImage;
const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  mainGreen,
  gold,
  superGrey,
  headerBorderBottom,
  greyLine,
} = Colors;
const {italiano, medium, book} = FontType;

const AnimatedTouch = new Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedCard = new Animated.createAnimatedComponent(Card);

const ImageCard = props => {
  console.log('ImageCard PROPS: ', props);
  const {navigation, id, title, serviceType, sourceImage, onPress, onPressRates, onPressPromotions} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  return (
    <AnimatedTouch
      onPress={() => {
        try {
          // navigation
          onPress();
        } catch (error) {
          console.log('Error: ', error);
        }
      }}
      activeOpacity={1}
      onPressIn={pressIn}
      onPressOut={pressOut}>
      <AnimatedCard
        style={{marginBottom: 15, borderRadius: 0, transform: [{scale}]}}>
        <CardItem style={{width: '100%', paddingLeft: 10, paddingRight: 10}}>
          <View
            style={{
              width: '100%',
              height: width / 3.5 + 50,
              backgroundColor: 'grey',
              borderRadius: 4,
            }}>
            <AsyncImage
              source={sourceImage}
              style={{flex: 1, width: '100%', height: '100%', borderRadius: 4}}
              placeholderColor={white}
              resizeMode="cover"
              loaderStyle={{width: width / 7, height: height / 7}}
            />
          </View>
        </CardItem>
        <CardItem style={{width: '100%', paddingTop: 0, paddingBottom: 5}}>
          <View style={{flex: 1}}>
            <Text
              numberOfLines={1}
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.4),
                color: 'grey',
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {props?.item?.tagline ? props?.item?.tagline : ''}
            </Text>
          </View>
          <View>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.4),
                color: 'grey',
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {props?.item?.serviceType
                ? props?.item?.serviceType.toUpperCase()
                : ''}
            </Text>
          </View>
        </CardItem>
        <CardItem style={{width: '100%', paddingTop: 5}}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {props?.item?.name ? props?.item?.name : 'N/A'}
          </Text>
        </CardItem>
        <CardItem style={{width: '100%', paddingTop: 5}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="users"
              style={{fontSize: RFPercentage(1.6), color: 'grey'}}
            />
            <Text
              style={{
                right: 13,
                fontFamily: book,
                color: 'grey',
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {props?.item?.capacity ? props?.item?.capacity : 'N/A'}
            </Text>
            <View
              style={{
                right: 7,
                width: 0.5,
                height: 15,
                borderWidth: 0.7,
                borderColor: headerBorderBottom,
                marginHorizontal: 5,
              }}
            />
            <Icon
              type="Feather"
              name="map-pin"
              style={{
                right: 3,
                fontSize: RFPercentage(1.6),
                color: 'grey',
              }}
            />
            <Text
              numberOfLines={1}
              style={{
                right: 15,
                fontFamily: book,
                color: 'grey',
                fontSize: RFPercentage(1.6),
                letterSpacing: 0.3,
              }}>
              {props?.item?.region ? props?.item?.region : 'N/A'}
            </Text>
          </View>
        </CardItem>
        {/* Rating */}
        {!props?.item?.averageRating ||
        props?.item?.averageRating === 0 ? null : (
          <CardItem
            style={{
              width: '100%',
              paddingTop: 0,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <AirbnbRating
              count={5}
              showRating={false}
              defaultRating={
                props?.item?.averageRating
                  ? props?.item?.averageRating.toFixed(1)
                  : 0
              }
              size={15}
              starStyle={{right: 5}}
            />
            <View>
              <Text
                style={{
                  marginLeft: 5,
                  fontFamily: book,
                  fontSize: RFPercentage(1.4),
                  color: 'grey',
                }}>
                {props?.item?.totalRating
                  ? `(+${props?.item?.totalRating})`
                  : '(0)'}
              </Text>
            </View>
          </CardItem>
        )}
        {!props?.item?.rateCards && !props?.item?.promotions ? null : (
          <CardItem style={{paddingTop: 5, width: '100%'}}>
            {!props?.item?.rateCards ||
            props?.item?.rateCards?.length === 0 ? null : (
              <TouchableOpacity
                onPress={() => onPressRates()}
                style={{
                  marginRight: 10,
                  padding: 7,
                  paddingLeft: 10,
                  paddingRight: 5,
                  backgroundColor: mainGreen,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.4),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  Rate Card{' '}
                  {`(${
                    props?.item?.rateCards?.length
                      ? props?.item?.rateCards?.length
                      : 0
                  })`}
                </Text>
              </TouchableOpacity>
            )}
            {!props?.item?.promotions ||
            props?.item?.promotions.length === 0 ? null : (
              <TouchableOpacity
                onPress={() => onPressPromotions()}
                style={{
                  padding: 7,
                  paddingLeft: 10,
                  paddingRight: 5,
                  backgroundColor: mainGreen,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.4),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  Promotion{' '}
                  {`(${
                    props?.item?.promotions?.length
                      ? props?.item?.promotions?.length
                      : 0
                  })`}
                </Text>
              </TouchableOpacity>
            )}
          </CardItem>
        )}
      </AnimatedCard>
    </AnimatedTouch>
  );
};

const Wrapper = compose(withApollo)(ImageCard);

export default props => <Wrapper {...props} />;
