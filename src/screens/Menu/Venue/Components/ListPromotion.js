import React from 'react';
import {Text, Dimensions, Image, View, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {charImage} from '../../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {black, transparent} = Colors;
const {medium} = FontType;
const {charGoldtag} = charImage;

// Tab Action
import {TabActions} from '@react-navigation/native';

// Components
import PromotionCard from './PromotionCard';

const ListPromotion = props => {
  const {list, isLoading, isError, navigation, detail} = props;

  const keyExt = item => `${item.id}`;

  const onPress = () => {
    try {
      const jumpToAction = TabActions.jumpTo('Promotion', {
        id: detail[0]?.id,
        serviceType: detail[0]?.serviceType,
      });
      navigation.dispatch(jumpToAction);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0) {
      return null;
    } else {
      return (
        <LinearGradient
          colors={['#FBF1E4', 'white', 'white', '#FBF1E4']}
          style={{
            paddingBottom: 15,
            zIndex: 0,
            flex: 1,
            marginVertical: 15,
          }}>
          <Image
            source={charGoldtag}
            style={{
              width: width / 8,
              height: height / 8,
              position: 'absolute',
              top: -10,
              left: 0,
              zIndex: 1,
            }}
            resizeMode="contain"
          />
          <View style={{flex: 1, zIndex: 3}}>
            <FlatList
              ListHeaderComponent={() => {
                return (
                  <Card transparent>
                    <CardItem
                      style={{backgroundColor: transparent, paddingLeft: 0}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          color: black,
                          fontSize: RFPercentage(2),
                          letterSpacing: 0.3,
                        }}>
                        Special Offer
                      </Text>
                    </CardItem>
                  </Card>
                );
              }}
              scrollEnabled={false}
              contentContainerStyle={{padding: 15}}
              disableVirtualization
              legacyImplementation
              data={list}
              extraData={list}
              keyExtractor={keyExt}
              renderItem={({item, index}) => {
                return (
                  <PromotionCard
                    item={item}
                    index={index}
                    onPress={() => {
                      onPress();
                    }}
                  />
                );
              }}
            />
          </View>
        </LinearGradient>
      );
    }
  }
};

const Wrapper = compose(withApollo)(ListPromotion);

export default props => <Wrapper {...props} />;
