import React, {useRef} from 'react';
import {Text, Dimensions, View, TouchableOpacity, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncImage from '../../../../components/Image/AsyncImage';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {Card, CardItem, Icon} from 'native-base';

const {width, height} = Dimensions.get('window');
const {white, black, superGrey, mainRed} = Colors;
const {book, medium} = FontType;

const TouchAnimated = new Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedCard = new Animated.createAnimatedComponent(Card);

const CardProduct = props => {
  const {item, onPress, runWishList, isLogin} = props;

  const animation = useRef(new Animated.Value(0)).current;
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.9,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    const {
      id,
      featuredImageDynamicURL,
      featuredImageURL,
      hotelPrice,
      hotelCapacity,
      isWishlist,
      galleries,
    } = item;
    const source = featuredImageDynamicURL
      ? {uri: `${featuredImageDynamicURL}=h500`}
      : {uri: `${featuredImageURL}`};
    return (
      <TouchAnimated
        activeOpacity={1}
        onPress={() => {
          onPress(id);
        }}
        onPressIn={pressIn}
        onPressOut={pressOut}
        transparent>
        <AnimatedCard
          transparent
          style={{marginLeft: 0, marginRight: 0, transform: [{scale}]}}>
          <CardItem style={{width: '100%'}}>
            <View style={{width: '100%', height: height / 4, borderRadius: 4}}>
              {galleries?.length === 0 || galleries?.length === 1 ? (
                <AsyncImage
                  source={source}
                  style={{
                    flex: 1,
                    width: '100%',
                    height: '100%',
                    borderRadius: 4,
                  }}
                  placeholderColor={white}
                  resizeMode="cover"
                  loaderStyle={{width: width / 7, height: height / 7}}
                />
              ) : (
                <DynamicImage images={galleries} />
              )}
            </View>
          </CardItem>
          <CardItem style={{paddingTop: 0, paddingLeft: 21}}>
            <View style={{width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 0.3, borderWidth: 0}}>
                <AsyncImage
                  source={
                    item?.merchantDetails?.logoImageDynamicUrl
                      ? {uri: item?.merchantDetails?.logoImageDynamicUrl}
                      : {uri: item?.merchantDetails?.logoImageUrl}
                  }
                  style={{
                    flex: 1,
                    right: 2,
                    // borderWidth: 1,
                    width: '100%',
                    height: '100%',
                  }}
                  placeholderColor={white}
                  resizeMode="cover"
                  loaderStyle={{width: width / 7, height: height / 7}}
                />
              </View>
              <View style={{flex: 1, paddingLeft: 10, paddingRight: 5}}>
                <View style={{marginBottom: 10}}>
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(1.8),
                      letterSpacing: 0.3,
                    }}>
                    {item?.name ? item?.name : 'N/A'}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="tag"
                    style={{fontSize: RFPercentage(1.4), color: black}}
                  />
                  <Text
                    style={{
                      right: 18,
                      fontFamily: book,
                      color: black,
                      fontSize: RFPercentage(1.4),
                      letterSpacing: 0.3,
                    }}>
                    {hotelPrice ? `$${hotelPrice}` : 'N/A'}
                  </Text>
                  <View
                    style={{
                      right: 12,
                      width: 0.5,
                      height: '100%',
                      borderWidth: 0.7,
                      borderColor: superGrey,
                      marginHorizontal: 5,
                    }}
                  />
                  <Icon
                    type="Feather"
                    name="users"
                    style={{
                      right: 3,
                      fontSize: RFPercentage(1.4),
                      color: black,
                    }}
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      right: 18,
                      fontFamily: book,
                      color: black,
                      fontSize: RFPercentage(1.4),
                      letterSpacing: 0.3,
                    }}>
                    {hotelCapacity ? `${hotelCapacity}` : 'N/A'}
                  </Text>
                </View>
                <View style={{marginTop: 10}}>
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: book,
                      color: 'grey',
                      fontSize: RFPercentage(1.3),
                      letterSpacing: 0.3,
                    }}>
                    {item?.merchantDetails?.serviceType
                      ? `${item?.merchantDetails?.serviceType} - `
                      : ''}
                    {`Singapore${
                      item?.merchantDetails?.region
                        ? `, ${item?.merchantDetails?.region}`
                        : ''
                    }`}
                  </Text>
                </View>
              </View>
              {isLogin ? (
                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => runWishList(id, isWishlist)}
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 6,
                      right: 5,
                    }}>
                    {isWishlist ? (
                      <Icon
                        type="MaterialCommunityIcons"
                        name="heart"
                        style={{
                          left: 10,
                          color: mainRed,
                          fontSize: RFPercentage(3),
                        }}
                      />
                    ) : (
                      <Icon
                        type="Feather"
                        name="heart"
                        style={{
                          left: 10,
                          color: superGrey,
                          fontSize: RFPercentage(2.7),
                        }}
                      />
                    )}
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          </CardItem>
        </AnimatedCard>
      </TouchAnimated>
    );
  } else {
    return null;
  }
};

export const DynamicImage = props => {
  const {images} = props;

  if (images && images?.length !== 0) {
    if (images?.length >= 3) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              paddingRight: 2,
              borderTopLeftRadius: 4,
              borderBottomLeftRadius: 4,
            }}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{
                width: '100%',
                height: '100%',
                flex: 1,
                borderTopLeftRadius: 4,
                borderBottomLeftRadius: 4,
              }}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{flex: 1, paddingLeft: 2, borderTopRightRadius: 4}}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{
                  width: '100%',
                  height: '100%',
                  flex: 1,
                  borderTopRightRadius: 4,
                }}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
            <View
              style={{
                flex: 1,
                paddingLeft: 2,
                paddingTop: 4,
                borderBottomRightRadius: 4,
              }}>
              <AsyncImage
                source={
                  images[2]?.dynamicUrl
                    ? {uri: `${images[2]?.dynamicUrl}=h500`}
                    : {uri: `${images[2]?.url}`}
                }
                resizeMode="cover"
                style={{
                  width: '100%',
                  height: '100%',
                  flex: 1,
                  borderBottomRightRadius: 4,
                }}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length >= 2) {
      return (
        <View style={{flex: 1, width: '100%', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              paddingRight: 2,
              borderTopLeftRadius: 4,
              borderBottomLeftRadius: 4,
            }}>
            <AsyncImage
              source={
                images[0]?.dynamicUrl
                  ? {uri: `${images[0]?.dynamicUrl}=h500`}
                  : {uri: `${images[0]?.url}`}
              }
              resizeMode="cover"
              style={{
                width: '100%',
                height: '100%',
                flex: 1,
                borderTopLeftRadius: 4,
                borderBottomLeftRadius: 4,
              }}
              placeholderColor={white}
              loaderStyle={{width: width / 15, height: height / 15}}
            />
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View
              style={{
                flex: 1,
                paddingLeft: 2,
                borderBottomRightRadius: 4,
                borderTopRightRadius: 4,
              }}>
              <AsyncImage
                source={
                  images[1]?.dynamicUrl
                    ? {uri: `${images[1]?.dynamicUrl}=h500`}
                    : {uri: `${images[1]?.url}`}
                }
                resizeMode="cover"
                style={{
                  width: '100%',
                  height: '100%',
                  flex: 1,
                  borderBottomRightRadius: 4,
                  borderTopRightRadius: 4,
                }}
                placeholderColor={white}
                loaderStyle={{width: width / 15, height: height / 15}}
              />
            </View>
          </View>
        </View>
      );
    } else if (images?.length === 1) {
      return (
        <View style={{flex: 1}}>
          <AsyncImage
            source={
              images[0]?.dynamicUrl
                ? {uri: `${images[0]?.dynamicUrl}=h500`}
                : {uri: `${images[0]?.url}`}
            }
            resizeMode="cover"
            style={{width: '100%', height: '100%', flex: 1, borderRadius: 4}}
            placeholderColor={white}
            loaderStyle={{width: width / 15, height: height / 15}}
          />
        </View>
      );
    }
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(CardProduct);

export default props => <Wrapper {...props} />;
