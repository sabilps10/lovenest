import React from 'react';
import {Text, Dimensions, Image, View, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {VenueAndHotelImages} from '../../../../utils/Themes/Images';

const {width, height} = Dimensions.get('window');
const {medium} = FontType;
const {white, black, mainRed} = Colors;
const {
  venueWhiteIcon,
  venueBlackIcon,
  hotelWhiteIcon,
  hotelBlackIcon,
} = VenueAndHotelImages;

const LeftNavButton = props => {
  const {title, onPress, buttonSelected} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        position: 'absolute',
        top: title === 'VENUE' ? 140 : 240,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 99,
      }}>
      <View
        style={{
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          backgroundColor:
            title === 'VENUE'
              ? buttonSelected === 'venue'
                ? mainRed
                : white
              : buttonSelected === 'hotel'
              ? mainRed
              : white,
          width: (width / 4) * 0.5,
          height: (width / 4) * 0.5,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: ((width / 4) * 0.5 + (width / 4) * 0.5) / 2,
        }}>
        <Image
          source={
            title === 'VENUE'
              ? buttonSelected === 'venue'
                ? venueWhiteIcon
                : venueBlackIcon
              : buttonSelected === 'hotel'
              ? hotelWhiteIcon
              : hotelBlackIcon
          }
          style={{flex: 1, width: width / 15, height: height / 15}}
          resizeMode="contain"
        />
      </View>
      <Text
        style={{
          textAlign: 'center',
          top: 10,
          fontSize: RFPercentage(1.7),
          color:
            title === 'VENUE'
              ? buttonSelected === 'venue'
                ? mainRed
                : black
              : buttonSelected === 'hotel'
              ? mainRed
              : black,
          fontFamily: medium,
          letterSpacing: 0.3,
          lineHeight: 20,
        }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const Wrapper = compose(withApollo)(LeftNavButton);

export default props => <Wrapper {...props} />;
