import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  ActivityIndicator,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../../utils/Themes/Fonts';
import AsyncImage from '../../../../components/Image/AsyncImage';
import CardProduct from './CardProduct';

const {width, height} = Dimensions.get('window');
const {mainRed, white, black, greyLine, superGrey, headerBorderBottom} = Colors;
const {medium, book} = FontType;

// Tab Action
import {TabActions} from '@react-navigation/native';

const PopularProduct = props => {
  const {
    list,
    isLoading,
    isError,
    detail,
    runWishList,
    isLogin,
    navigation,
  } = props;

  const keyExt = (item, index) => `${item.id}`;

  const onPress = id => {
    try {
      console.log('PRODUCT ID: ', id);
      navigation.push('VenueAndHotelProductDetail', {id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const RenderList = () => {
    return useMemo(() => {
      return (
        <FlatList
          ListHeaderComponent={() => {
            return (
              <Card
                transparent
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  marginBottom: 0,
                  marginTop: 15,
                }}>
                <CardItem style={{width: '100%', paddingBottom: 0}}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <Text
                        style={{
                          fontSize: RFPercentage(2),
                          color: black,
                          fontFamily: medium,
                          letterSpacing: 0.3,
                        }}>
                        Popular Products
                      </Text>
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        flexDirection: 'row',
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          try {
                            const jumpToAction = TabActions.jumpTo('Products', {
                              id: detail[0]?.id,
                              serviceType: detail[0]?.serviceType,
                            });
                            navigation.dispatch(jumpToAction);
                          } catch (error) {
                            console.log('Error: ', error);
                          }
                        }}
                        style={{
                          left: 20,
                          alignSelf: 'flex-end',
                          flexDirection: 'row',
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: RFPercentage(1.8),
                            color: mainRed,
                            fontFamily: medium,
                            letterSpacing: 0.3,
                          }}>
                          See All
                        </Text>
                        <View style={{marginLeft: 10}}>
                          <Icon
                            type="Feather"
                            name="arrow-right"
                            style={{
                              color: mainRed,
                              fontSize: RFPercentage(2.2),
                            }}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </CardItem>
              </Card>
            );
          }}
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          renderItem={({item, index}) => {
            return (
              <CardProduct
                isLogin={isLogin}
                runWishList={(id, isWishlist) => runWishList(id, isWishlist)}
                onPress={id => onPress(id)}
                {...props}
                item={item}
                index={index}
                {...props}
              />
            );
          }}
        />
      );
    }, [list]);
  };

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={mainRed} />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    if (list?.length === 0 && !isLoading) {
      return null;
    } else {
      return RenderList();
    }
  }
};

const Wrapper = compose(withApollo)(PopularProduct);

export default props => <Wrapper {...props} />;
