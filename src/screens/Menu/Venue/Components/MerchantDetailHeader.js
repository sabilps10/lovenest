import React from 'react';
import {Text, StatusBar, Dimensions, Image, View, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';
import {FontType} from '../../../../utils/Themes/Fonts';
import Colors from '../../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';
import {connect} from 'react-redux';

const {width, height} = Dimensions.get('window');
const {white, black, headerBorderBottom, greyLine, superGrey} = Colors;
const {book, medium} = FontType;

import {charImage} from '../../../../utils/Themes/Images';

const {
  charFilterIcon,
  charFilterIconRed,
  charIconAbout,
  charIconReview,
} = charImage;

import {CommonActions} from '@react-navigation/native';

// Redux Thunk
import thunkPrices from '../../../../redux/thunk/VenueAndHotelPriceThunk';
import thunkShortPrices from '../../../../redux/thunk/VenueAndHotelShotPriceThunk';
import thunkPackages from '../../../../redux/thunk/VenueAndHotelPackagesThunk';
import thunkRegions from '../../../../redux/thunk/VenueAndHotelRegionsThunk';
import thunkCapacities from '../../../../redux/thunk/VenueAndHotelCapacitiesThunk';
import thunkAvailabilities from '../../../../redux/thunk/VenueAndHotelAvailabilitiesThunk';

const MerchantDetailHeader = props => {
  const {
    activeTab,
    detail,
    navigation,
    // Redux dispatch
    setFilterPrices,
    setFilterShortPrices,
    setFilterPackages,
    setFilterRegions,
    setFilterCapacities,
    setFilterAvailabilities,
  } = props;

  const goBack = async () => {
    try {
      await setFilterPrices([0.0, 0.0]);
      await setFilterShortPrices(null);
      await setFilterPackages([]);
      await setFilterRegions([]);
      await setFilterCapacities([]);
      await setFilterAvailabilities([]);
      await navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToAbout = () => {
    try {
      navigation.navigate('VenueAndHotelAbout', {
        id: detail[0]?.id,
        serviceType: detail[0]?.serviceType,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToHome = async () => {
    try {
      await setFilterPrices([0.0, 0.0]);
      await setFilterShortPrices(null);
      await setFilterPackages([]);
      await setFilterRegions([]);
      await setFilterCapacities([]);
      await setFilterAvailabilities([]);
      await navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Home'}],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToFilter = () => {
    try {
      navigation.navigate('VenueAndHotelFilter', {
        id: detail[0]?.id,
        serviceType: detail[0]?.serviceType,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToReview = () => {
    try {
      navigation.navigate('ReviewList', {id: detail[0]?.id});
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor={activeTab === 'Home' ? 'transparent' : 'white'}
      translucent={activeTab === 'Home' ? true : false}
      style={
        activeTab === 'Home'
          ? {
              paddingTop:
                Platform.OS === 'android'
                  ? hasNotch()
                    ? 55
                    : 45
                  : hasNotch()
                  ? 0
                  : 0,
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
            }
          : {
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: activeTab === 'Home' ? 'transparent' : white,
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: activeTab === 'Home' ? 0 : 1,
              marginBottom: 10,
            }
      }>
      <Left style={{flex: 0.4, paddingLeft: 10}}>
        <Button
          onPress={goBack}
          style={{
            borderRadius: 35 / 2,
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={
              Platform.OS === 'ios'
                ? {marginLeft: 0, marginRight: 0, fontSize: 24, color: black}
                : {
                    right: 4,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: black,
                  }
            }
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {activeTab !== 'Home' ? (
          <>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              {activeTab}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: medium,
                marginVertical: 5,
                fontSize: RFPercentage(1.3),
                color: 'grey',
                letterSpacing: 0.3,
              }}>
              {detail[0]?.name}
            </Text>
          </>
        ) : null}
      </Body>
      <Right
        style={{
          flex: 0.4,
          justifyContent:
            activeTab === 'Home' || activeTab === 'Products'
              ? 'space-between'
              : 'flex-end',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        {activeTab === 'Home' ? (
          <Button
            onPress={goToReview}
            style={{
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity:
                activeTab === 'Home'
                  ? Platform.OS === 'android'
                    ? 1
                    : 0.2
                  : 0,
              shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowRadius: 1.41,
            }}>
            <Image
              source={charIconReview}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : activeTab === 'Products' ? (
          <Button
            disabled
            onPress={goToFilter}
            style={{
              opacity: 0,
              borderRadius: 35 / 2,
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              elevation: activeTab === 'Home' ? 2 : 0,
              shadowOpacity: activeTab === 'Home' ? 1 : 0,
            }}>
            <Image
              source={charFilterIcon}
              style={{width: 20, height: 20}}
              resizeMode="contain"
            />
          </Button>
        ) : null}
        <Button
          onPress={() => {
            if (activeTab === 'Home') {
              goToAbout();
            } else {
              goToHome();
            }
          }}
          style={{
            borderRadius: 35 / 2,
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: white,
            elevation: activeTab === 'Home' ? 2 : 0,
            shadowOpacity:
              activeTab === 'Home' ? (Platform.OS === 'android' ? 1 : 0.2) : 0,
            shadowColor: activeTab === 'Home' ? '#000' : '#FFFFFF',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowRadius: 1.41,
          }}>
          {activeTab === 'Home' ? (
            <Image
              source={charIconAbout}
              style={{bottom: 2, width: 25, height: 25}}
              resizeMode="contain"
            />
          ) : (
            <Icon
              type="Feather"
              name="home"
              style={{
                bottom: 0.5,
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          )}
        </Button>
      </Right>
    </Header>
  );
};

const mapToState = state => {
  const {venueAndHotel} = state;
  return {
    ...venueAndHotel,
  };
};

const mapToDispatch = dispatch => {
  return {
    setFilterPrices: prices => dispatch(thunkPrices(prices)),
    setFilterShortPrices: shortBy => dispatch(thunkShortPrices(shortBy)),
    setFilterPackages: packages => dispatch(thunkPackages(packages)),
    setFilterRegions: regions => dispatch(thunkRegions(regions)),
    setFilterCapacities: capacities => dispatch(thunkCapacities(capacities)),
    setFilterAvailabilities: availabilities =>
      dispatch(thunkAvailabilities(availabilities)),
  };
};

const connector = connect(mapToState, mapToDispatch)(MerchantDetailHeader);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
