import React from 'react';
import {
  Text,
  StatusBar,
  ActivityIndicator,
  Dimensions,
  Image,
  View,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../../utils/Themes/Colors';
import {FontType} from '../../../../utils/Themes/Fonts';
import {Header, Left, Body, Right, Button, Icon} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {white, black, mainRed, headerBorderBottom} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

const HeaderFilter = props => {
  const {title, goBack, reset, isLoadingReset} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        backgroundColor: white,
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <Left style={{flex: 0.25}}>
        <Button
          onPress={goBack}
          style={{
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: white,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 25, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          {title}
        </Text>
      </Body>
      <Right style={{flex: 0.25}}>
        <Button
          onPress={reset}
          style={{
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            justifyContent: 'center',
            backgroundColor: white,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          {isLoadingReset ? (
            <ActivityIndicator color={mainRed} size="large" />
          ) : (
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainRed,
                letterSpacing: 0.3,
              }}>
              Reset
            </Text>
          )}
        </Button>
      </Right>
    </Header>
  );
};

const Wrapper = compose(withApollo)(HeaderFilter);

export default props => <Wrapper {...props} />;
