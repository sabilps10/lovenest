import React, {useState, useEffect} from 'react';
import {
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  Image,
  View,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {connect} from 'react-redux';

const {regular} = FontSize;
const {white, greyLine, headerBorderBottom, black, mainRed, mainGreen} = Colors;
const {medium, book} = FontType;
const {width, height} = Dimensions.get('window');

// Queries
import VenueHotelPriceRange from '../../../graphql/queries/productPriceRange';
import VenueHotelPackagesFilterList from '../../../graphql/queries/venueHotelPackageFilter';
import VenueHotelRegionFilterList from '../../../graphql/queries/venueHotelRegionFilter';
import VenueHotelCapacityFilterList from '../../../graphql/queries/venueHotelCapacityFilter';

// Components
import HeaderFilter from './Components/HeaderFilter';
import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFilter from '../../../components/Button/ButtonFilters/Unfilter';
import CustomSlider from './Components/CustomSlider';
import CustomListFilter from './Components/CustomListFilter';

// Redux Thunk
import thunkPrices from '../../../redux/thunk/VenueAndHotelPriceThunk';
import thunkShortPrices from '../../../redux/thunk/VenueAndHotelShotPriceThunk';
import thunkPackages from '../../../redux/thunk/VenueAndHotelPackagesThunk';
import thunkRegions from '../../../redux/thunk/VenueAndHotelRegionsThunk';
import thunkCapacities from '../../../redux/thunk/VenueAndHotelCapacitiesThunk';
import thunkAvailabilities from '../../../redux/thunk/VenueAndHotelAvailabilitiesThunk';

const VenueAndHotelFilter = props => {
  console.log('VenueAndHotelFilter Props: ', props);
  const {
    navigation,
    client,
    route,
    // Redux State
    prices: reduxPrice,
    shortPrices: reduxShortPrice,
    packages: reduxPackages,
    regions: reduxRegions,
    capacities: reduxCapacities,
    availabilities: reduxAvailabilities,
    // Redux dispatch
    setFilterPrices,
    setFilterShortPrices,
    setFilterPackages,
    setFilterRegions,
    setFilterCapacities,
    setFilterAvailabilities,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoadingSubmitFilter, setIsLoadingSubmitFilter] = useState(false);
  const [isLoadingReset, setIsLoadingReset] = useState(false);

  // Price range
  const [minPrice, setMinPrice] = useState(0.0);
  const [maxPrice, setMaxPrice] = useState(0.0);
  const [valuePrice, setValuePrice] = useState([0.0, 0.0]);
  const [isLoadingPrice, setIsLoadingPrice] = useState(true);
  const [isErrorPrice, setIsErrorPrice] = useState(false);

  // Short Price (Low/High)
  const [shortPrice, setShortPrice] = useState([]);
  const [isLoadingShortPrice, setIsLoadingShortPrice] = useState(false);
  const [isErrorShortPrice, setIsErrorShortPrice] = useState(false);

  // Packages
  const [packages, setPackages] = useState([]);
  const [isLoadingPackages, setIsLoadingPackage] = useState(true);
  const [isErrorPackages, setIsErrorPackages] = useState(false);

  // Region
  const [region, setRegion] = useState([]);
  const [isLoadingRegion, setIsLoadingRegion] = useState(true);
  const [isErrorRegion, setIsErrorRegion] = useState(false);

  // Capacity
  const [capacity, setCapacity] = useState([]);
  const [isLoadingCapacity, setIsLoadingCapacity] = useState(true);
  const [isErrorCapacity, setIsErrorCapacity] = useState(false);

  // Availablity
  const [availability, setAvailability] = useState([]);
  const [isLoadingAvailability, setIsLoadingAvalability] = useState(false);
  const [isErrorAvailability, setIsErrorAvailability] = useState(false);

  useEffect(() => {
    globalFetch();

    const subs = navigation.addListener('focus', () => {
      globalFetch();
    });

    return subs;
  }, [
    id,
    serviceType,
    navigation,
    reduxPrice,
    reduxShortPrice,
    reduxPackages,
    reduxRegions,
    reduxCapacities,
    reduxAvailabilities,
  ]);

  const globalFetch = async () => {
    try {
      await fetchPriceRange();
      await fetchPrices(reduxPrice);
      await fetchShortPrice(reduxShortPrice);
      await fetchPackages(reduxPackages);
      await fetchRegion(reduxRegions);
      await fetchCapacity(reduxCapacities);
      await fetchAvailability(reduxAvailabilities);
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const manipulate = list => {
    return new Promise(async (resolve, reject) => {
      try {
        if (list?.length === 0) {
          resolve([]);
        } else {
          const manipulating = await Promise.all(
            list.map((d, i) => {
              return {
                id: i + 1,
                name: d,
                selected: false,
              };
            }),
          );

          if (manipulating) {
            if (manipulating?.length === list?.length) {
              resolve([...manipulating]);
            } else {
              resolve([]);
            }
          } else {
            resolve([]);
          }
        }
      } catch (error) {
        resolve([]);
      }
    });
  };

  // fetch Price
  const fetchPrices = async reduxPriceData => {
    try {
      if (reduxPriceData[1] !== 0.0 || reduxPriceData[1] !== 0) {
        await setValuePrice(reduxPriceData);
        await setIsErrorPrice(false);
        await setIsLoadingPrice(false);
      } else {
        await setValuePrice([0.0, 10.0]);
        await setIsErrorPrice(false);
        await setIsLoadingPrice(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPrice(true);
      await setIsLoadingPrice(false);
    }
  };

  // fetch availability
  const fetchAvailability = async reduxAvail => {
    try {
      console.log('ANU KUNTUL: ', reduxAvail);
      if (reduxAvail?.length === 0) {
        const defaultAvail = [
          {
            id: 1,
            name: 'Weekday',
            selected: false,
          },
          {
            id: 2,
            name: 'Weekend',
            selected: false,
          },
        ];
        await setAvailability(defaultAvail);
        await setTimeout(async () => {
          console.log('defaultAvail >>> ', defaultAvail);
          await setIsErrorAvailability(false);
          await setIsLoadingAvalability(false);
        }, 1000);
      } else {
        await setAvailability([...reduxAvail]);
        await setIsErrorAvailability(false);
        await setIsLoadingAvalability(false);
      }
    } catch (error) {
      await setIsErrorAvailability(true);
      await setIsLoadingAvalability(false);
    }
  };

  // fetch short prices
  const fetchShortPrice = async reduxShortPriceData => {
    try {
      console.log('ANU VANGKE: ', reduxShortPriceData);
      if (reduxShortPriceData) {
        await setShortPrice([...reduxShortPriceData]);
        await setIsErrorShortPrice(false);
        await setIsLoadingShortPrice(false);
      } else {
        setTimeout(async () => {
          const defaultShortPriceData = [
            {
              id: 1,
              name: 'LOW',
              selected: false,
            },
            {
              id: 2,
              name: 'HIGH',
              selected: false,
            },
          ];
          await setShortPrice([...defaultShortPriceData]);
          await setIsErrorShortPrice(false);
          await setIsLoadingShortPrice(false);
        }, 1000);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorShortPrice(true);
      await setIsLoadingShortPrice(false);
    }
  };

  // Fetch Price Range Slider
  const fetchPriceRange = async () => {
    try {
      await client
        .query({
          query: VenueHotelPriceRange,
          variables: {
            type: serviceType,
            merchantId: id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('fetch Price Range: ', response);
          const {data, errors} = response;
          const {productPriceRange} = data;
          const {data: obj, error} = productPriceRange;

          if (errors) {
            await setIsErrorPrice(true);
            await setIsLoadingPrice(false);
          } else {
            if (error) {
              await setIsErrorPrice(true);
              await setIsLoadingPrice(false);
            } else {
              await setMaxPrice(parseFloat(obj?.max));
              await setMinPrice(parseFloat(obj?.min));
              await setIsErrorPrice(false);
              await setIsLoadingPrice(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPrice(true);
      await setIsLoadingPrice(false);
    }
  };

  // fetch packages list
  const fetchPackages = async reduxPackagesData => {
    try {
      if (reduxPackagesData?.length === 0) {
        await client
          .query({
            query: VenueHotelPackagesFilterList,
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('fetch Packages list: ', response);
            const {data, errors} = response;
            const {venueHotelPackages} = data;
            const {data: list, error} = venueHotelPackages;

            if (errors) {
              await setIsErrorPackages(true);
              await setIsLoadingPackage(false);
            } else {
              if (error) {
                await setIsErrorPackages(true);
                await setIsLoadingPackage(false);
              } else {
                const resultList = await manipulate(list);
                console.log('resultList >> ', resultList);
                if (resultList?.length === list?.length) {
                  await setPackages([...resultList]);
                  await setIsErrorPackages(false);
                  await setIsLoadingPackage(false);
                } else {
                  throw new Error('Data is Empty');
                }
              }
            }
          })
          .catch(error => {
            console.log('error: ', error);
            throw error;
          });
      } else {
        await setPackages([...reduxPackagesData]);
        await setIsErrorPackages(false);
        await setIsLoadingPackage(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorPackages(true);
      await setIsLoadingPackage(false);
    }
  };

  // fetch region list
  const fetchRegion = async reduxRegionData => {
    try {
      if (reduxRegionData?.length === 0) {
        await client
          .query({
            query: VenueHotelRegionFilterList,
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('fetch Region list: ', response);
            const {data, errors} = response;
            const {venueHotelRegions} = data;
            const {data: list, error} = venueHotelRegions;

            if (errors) {
              await setIsErrorRegion(true);
              await setIsLoadingRegion(false);
            } else {
              if (error) {
                await setIsErrorRegion(true);
                await setIsLoadingRegion(false);
              } else {
                const resultList = await manipulate(list);
                console.log('resultList >> ', resultList);
                if (resultList?.length === list?.length) {
                  await setRegion([...resultList]);
                  await setIsErrorRegion(false);
                  await setIsLoadingRegion(false);
                } else {
                  throw new Error('Data is Empty');
                }
              }
            }
          })
          .catch(error => {
            console.log('error: ', error);
            throw error;
          });
      } else {
        await setRegion([...reduxRegionData]);
        await setIsErrorRegion(false);
        await setIsLoadingRegion(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorRegion(true);
      await setIsLoadingRegion(false);
    }
  };

  // fetch capacity list
  const fetchCapacity = async reduxCapacityData => {
    try {
      if (reduxCapacityData?.length === 0) {
        await client
          .query({
            query: VenueHotelCapacityFilterList,
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('fetch capacity list: ', response);
            const {data, errors} = response;
            const {venueHotelCapacity} = data;
            const {data: list, error} = venueHotelCapacity;

            if (errors) {
              await setIsErrorCapacity(true);
              await setIsLoadingCapacity(false);
            } else {
              if (error) {
                await setIsErrorCapacity(true);
                await setIsLoadingCapacity(false);
              } else {
                const resultList = await manipulate(list);
                console.log('resultList >> ', resultList);
                if (resultList?.length === list?.length) {
                  await setCapacity([...resultList]);
                  await setIsErrorCapacity(false);
                  await setIsLoadingCapacity(false);
                } else {
                  throw new Error('Data is Empty');
                }
              }
            }
          })
          .catch(error => {
            console.log('error: ', error);
            throw error;
          });
      } else {
        await setCapacity([...reduxCapacityData]);
        await setIsErrorCapacity(false);
        await setIsLoadingCapacity(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorCapacity(true);
      await setIsLoadingCapacity(false);
    }
  };

  const goBack = async () => {
    try {
      console.log('Masuk on Reset button');
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onReset = async () => {
    try {
      console.log('Masuk on Reset button');
      await setIsLoadingReset(true);
      await setFilterPrices([0.0, 0.0]);
      await setFilterShortPrices(null);
      await setFilterPackages([]);
      await setFilterRegions([]);
      await setFilterCapacities([]);
      await setFilterAvailabilities([]);
      await setTimeout(async () => {
        await setIsLoadingSubmitFilter(false);
        // await goBack();
        await navigation.navigate('VenueAndHotelListProductAfterFilter', {
          serviceType,
        });
      }, 2000);
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingReset(false);
    }
  };

  const onFiltering = async () => {
    try {
      console.log('masuk ke filter button', {
        valuePrice,
        shortPrice,
        packages,
        region,
        capacity,
        availability,
      });
      await setIsLoadingSubmitFilter(true);
      await setFilterPrices(valuePrice);
      await setFilterShortPrices(shortPrice);
      await setFilterPackages(packages);
      await setFilterRegions(region);
      await setFilterCapacities(capacity);
      await setFilterAvailabilities(availability);
      await setTimeout(async () => {
        await setIsLoadingSubmitFilter(false);
        // await goBack();
        await navigation.navigate('VenueAndHotelListProductAfterFilter', {
          serviceType,
        });
      }, 2000);
    } catch (error) {
      await setIsLoadingSubmitFilter(false);
    }
  };

  return (
    <Container>
      <HeaderFilter
        title={'Filter'}
        goBack={() => goBack()}
        reset={() => onReset()}
        isLoadingReset={isLoadingReset}
      />
      <Content>
        <Card transparent>
          {/* Price Slider */}
          <CustomSlider
            isLoading={isLoadingPrice}
            isError={isErrorPrice}
            title={'Price (SGD)'}
            min={0.0}
            max={
              maxPrice === 0.0 || maxPrice === 0 || isNaN(maxPrice)
                ? 500.0
                : maxPrice
            }
            step={0.1}
            enabledOne={false}
            selectedStyle={{
              backgroundColor: mainRed,
            }}
            unselectedStyle={{
              backgroundColor: headerBorderBottom,
            }}
            customMarker={() => {
              return (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: white,
                    width: 25,
                    height: 25,
                    backgroundColor: mainRed,
                    borderRadius: 25 / 2,
                  }}
                />
              );
            }}
            trackStyle={{backgroundColor: headerBorderBottom}}
            minMarkerOverlapDistance={5}
            values={valuePrice}
            allowOverlap={true}
            sliderLength={width / 1.16}
            onValuesChange={e => setValuePrice([...e])}
            decimal={true}
          />

          {/* Shot Price */}
          <CustomListFilter
            data={shortPrice}
            title={'Short Price'}
            subtitle={''}
            isLoading={isLoadingShortPrice}
            isError={isErrorShortPrice}
            onSelectItem={async index => {
              try {
                const selectedItem = shortPrice[index]?.name;
                const resetNewValue = await Promise.all(
                  shortPrice.map((d, i) => {
                    if (d?.name === selectedItem) {
                      if (d?.selected) {
                        return {
                          ...d,
                          selected: false,
                        };
                      } else {
                        return {
                          ...d,
                          selected: true,
                        };
                      }
                    } else {
                      return {
                        ...d,
                        selected: false,
                      };
                    }
                  }),
                );

                if (resetNewValue?.length === shortPrice?.length) {
                  await setShortPrice([...resetNewValue]);
                }
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />

          {/* Packages */}
          <CustomListFilter
            data={packages}
            title={'Packages'}
            subtitle={'You may select more than one'}
            isLoading={isLoadingPackages}
            isError={isErrorPackages}
            onSelectItem={index => {
              let oldData = packages;
              oldData[index].selected = !packages[index].selected;
              setPackages([...oldData]);
            }}
          />

          {/* Region */}
          <CustomListFilter
            data={region}
            title={'Region'}
            subtitle={'You may select more than one'}
            isLoading={isLoadingRegion}
            isError={isErrorRegion}
            onSelectItem={index => {
              let oldData = region;
              oldData[index].selected = !region[index].selected;
              setRegion([...oldData]);
            }}
          />

          {/* Capacity */}
          <CustomListFilter
            showIcon={true}
            data={capacity}
            title={'Capacity'}
            subtitle={'You may select more than one'}
            isLoading={isLoadingCapacity}
            isError={isErrorCapacity}
            onSelectItem={index => {
              let oldData = capacity;
              oldData[index].selected = !capacity[index].selected;
              setCapacity([...oldData]);
            }}
          />

          {/* Availability */}
          <CustomListFilter
            data={availability}
            title={'Availability'}
            subtitle={'You may select more than one'}
            isLoading={isLoadingAvailability}
            isError={isErrorAvailability}
            onSelectItem={index => {
              let oldData = availability;
              oldData[index].selected = !availability[index].selected;
              setAvailability([...oldData]);
            }}
          />
        </Card>
      </Content>
      <FooterButton onPress={onFiltering} isLoading={isLoadingSubmitFilter} />
    </Container>
  );
};

export const FooterButton = props => {
  const {onPress, isLoading} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen, width: '100%'}}>
        <View style={{width: '100%', backgroundColor: mainGreen}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: regular,
                letterSpacing: 0.3,
              }}>
              APPLY FILTER
            </Text>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                color={white}
                style={{marginLeft: 10}}
              />
            ) : null}
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const mapToState = state => {
  console.log('MapState Bro: ', state);
  const {venueAndHotel} = state;
  return {
    ...venueAndHotel,
  };
};

const mapToDispatch = dispatch => {
  return {
    setFilterPrices: prices => dispatch(thunkPrices(prices)),
    setFilterShortPrices: shortBy => dispatch(thunkShortPrices(shortBy)),
    setFilterPackages: packages => dispatch(thunkPackages(packages)),
    setFilterRegions: regions => dispatch(thunkRegions(regions)),
    setFilterCapacities: capacities => dispatch(thunkCapacities(capacities)),
    setFilterAvailabilities: availabilities =>
      dispatch(thunkAvailabilities(availabilities)),
  };
};

const connector = connect(
  mapToState,
  mapToDispatch,
)(VenueAndHotelFilter);

const Wrapper = compose(withApollo)(connector);

export default props => <Wrapper {...props} />;
