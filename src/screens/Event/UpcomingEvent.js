import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  RefreshControl,
  FlatList,
  Dimensions,
  Image,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ButtonBack from '../../components/Button/buttonBack';
import {FontType, FontSize} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {Card, CardItem, Container} from 'native-base';
import BigCardEvent from '../../components/Event/Card/BigCardEvent';
import {charImage} from '../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';

import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';

// Graphql QUERY
import GET_UPCOMING_EVENT_QUERY from '../../graphql/queries/getAllUpComingEvents';

// Shimmer
import UpcomingShimmer from '../../components/Loader/Event/EventListShimmer';

const {charNoEvent} = charImage;
const {black, greyLine, newContainerColor, white} = Colors;
const {regular} = FontSize;
const {medium, book} = FontType;

const shimmerSize = [1, 2, 3, 4, 5, 6];
const pageNumber = 1;

const {height} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

export const UpcomingCardList = props => {
  const {
    eventList,
    isLoadMore,
    settingIsLoadMore,
    totalCount,
    navigation,
    client,
    pageSize,
    isPullOnRefresh,
    pullToRefresh,
  } = props;

  const keyExt = (item, _) => String(item.id);
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderLst = () =>
    useMemo(() => {
      if (eventList) {
        return (
          <FlatList
            refreshControl={
              <RefreshControl
                onRefresh={() => pullToRefresh()}
                refreshing={isPullOnRefresh}
              />
            }
            contentContainerStyle={{
              paddingTop: 16,
              paddingBottom: 25,
              paddingLeft: 15,
              paddingRight: 15,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            ListEmptyComponent={() => {
              return (
                <Card
                  transparent
                  style={{
                    marginLeft: 0,
                    marginRight: 0,
                    width: Dimensions.get('window').width,
                  }}>
                  <CardItem cardBody>
                    <Image
                      source={charNoEvent}
                      style={{flex: 1, width: null, height: height / 2.5}}
                      resizeMode="contain"
                    />
                  </CardItem>
                  <CardItem
                    style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: medium,
                        fontSize: RFPercentage(2.5),
                        color: black,
                        letterSpacing: 0.3,
                        lineHeight: 18,
                      }}>
                      No Upcoming Event
                    </Text>
                  </CardItem>
                  <CardItem
                    style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: book,
                        fontSize: RFPercentage(2),
                        color: black,
                        letterSpacing: 0.3,
                        lineHeight: 20,
                      }}>
                      You will be notified when new event available.
                    </Text>
                  </CardItem>
                </Card>
              );
            }}
            ListFooterComponent={() => {
              if (isLoadMore) {
                return (
                  <View
                    style={{
                      width: '100%',
                      height: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text>Load More...</Text>
                  </View>
                );
              } else {
                return null;
              }
            }}
            onEndReached={() => {
              if (pageSize <= totalCount) {
                settingIsLoadMore(true);
              } else {
                settingIsLoadMore(false);
              }
            }}
            onEndReachedThreshold={0.01}
            extraData={eventList}
            data={eventList}
            keyExtractor={keyExt}
            getItemLayout={getItemLayout}
            renderItem={({item}) => {
              return (
                <BigCardEvent
                  data={item}
                  navigation={navigation}
                  clinet={client}
                />
              );
            }}
          />
        );
      } else {
        return null;
      }
    }, [isLoadMore, isPullOnRefresh, eventList]);

  if (eventList) {
    return RenderLst();
  } else {
    return null;
  }
};

const UpcomingEvent = props => {
  console.log('UpcomingEvent Props: ', props);
  const {navigation, client, positionYBottomNav} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [upcomingEventList, setUpcomingEventList] = useState([]);

  const [pageSize, setPageSize] = useState(10);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [totalCount, setTotalCount] = useState(0);

  const [isPullOnRefresh, setIsPullOnRefresh] = useState(false);

  useEffect(() => {
    navigationOptions();
    fetchUpComingEvent();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subs = navigation.addListener('focus', () => {
      navigationOptions();
      fetchUpComingEvent();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return () => {
      subs();
    };
  }, [navigation]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const pullToRefresh = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await fetchUpComingEvent();
  };

  const settingIsLoadMore = async value => {
    await setIsLoadMore(value);
    let oldPageSize = pageSize;
    let newPageSize = oldPageSize + 10;
    await setPageSize(newPageSize);
    await fetchUpComingEvent();
  };

  const fetchUpComingEvent = () => {
    try {
      client
        .query({
          query: GET_UPCOMING_EVENT_QUERY,
          variables: {
            pageSize: pageSize,
            pageNumber: pageNumber,
          },
        })
        .then(async response => {
          console.log('Response Fetch Upcoming Event: ', response);
          const {data, errors} = response;
          const {getAllUpcomingEvents} = data;
          const {error, data: listEvents, totalData} = getAllUpcomingEvents;

          if (errors === undefined) {
            if (error === null) {
              await setTotalCount(totalData);
              await setUpcomingEventList(listEvents);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setIsPullOnRefresh(false);
            } else {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setIsPullOnRefresh(false);
            }
          } else {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setIsPullOnRefresh(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsLoadMore(false);
          await setIsPullOnRefresh(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsLoadMore(false);
      setIsPullOnRefresh(false);
    }
  };

  const popStacking = () => {
    try {
      navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Events',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          {shimmerSize.map((_, index) => {
            return <UpcomingShimmer key={`${String(index)} shimmer`} />;
          })}
        </View>
      </Container>
    );
  } else if (!isLoading && !isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <View style={{flex: 1}}>
          <UpcomingCardList
            eventList={upcomingEventList}
            navigation={navigation}
            client={client}
            isLoadMore={isLoadMore}
            settingIsLoadMore={value => settingIsLoadMore(value)}
            totalCount={totalCount}
            pageSize={pageSize}
            isPullOnRefresh={isPullOnRefresh}
            pullToRefresh={() => pullToRefresh()}
          />
        </View>
      </Container>
    );
  } else {
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1}}>
        <ErrorScreen message="Refresh" onPress={() => {}} />
      </View>
    </Container>;
  }
};

const Wrapper = compose(withApollo)(UpcomingEvent);

export default props => <Wrapper {...props} />;
