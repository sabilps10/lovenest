import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  View,
  Animated,
  Dimensions,
  Platform,
  StatusBar,
  Linking,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import GET_EVENT_DETAIL_QUERY from '../../graphql/queries/getEventDetail';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import Loader from '../../components/Loader/circleLoader';
import {
  Container,
  Content,
  Card,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Button,
} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontType, FontSize} from '../../utils/Themes/Fonts';
import {hasNotch} from 'react-native-device-info';
import moment from 'moment';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-community/async-storage';
import AuthKey from '../../utils/AsyncstorageDataStructure';
import Map from '../../components/Map/MapWithNavigationButton';

// Animated Component
import BackButton from '../../components/Event/Button/BackButton';
import HeaderEvent from '../../components/Event/Header';
import AnimatedHeaderImage from '../../components/Event/Header/HeaderImage';

// Common Components
import TitleEvent from '../../components/Event/Item/TitleEvent';
import ScheduleEvent from '../../components/Event/Item/ScheduleEvent';
import AddressEvent from '../../components/Event/Item/AddressEvent';
import DescriptionEvent from '../../components/Event/Item/DescriptionEvent';
import FooterButton from '../../components/Event/Button/FooterButton';

// Modal
import ModalRSVP from '../../components/Event/Modal/ModalRSVP';
import ModalLogin from '../../components/Event/Modal/ModalLogin';

const {asyncToken} = AuthKey;
const {newContainerColor, black, greyLine} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const EventDetail = props => {
  console.log('EventDetail Props: ', props);
  const {navigation, client, route, positionYBottomNav} = props;
  const {params} = route;
  const {id} = params;

  const eventDetailRef = useRef();

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [detail, setDetail] = useState([]);

  const [seeMore, setSeeMore] = useState(false);
  const [descriptionLength, setDescriptionLength] = useState(0);

  const [showModalRSVP, setShowModalRSVP] = useState(false);
  const [showModalLogin, setShowModalLogin] = useState(false);

  useEffect(() => {
    StatusBar.setBackgroundColor('transparent');
    StatusBar.setBarStyle('dark-content');
    StatusBar.setTranslucent(true);
    fetchEventDetail();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subs = navigation.addListener('focus', () => {
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle('dark-content');
      StatusBar.setTranslucent(true);
      fetchEventDetail();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subs;
  }, [navigation]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const showRSVP = async () => {
    try {
      const checkAuth = await AsyncStorage.getItem(asyncToken);
      console.log('checkAuth: ', checkAuth);
      if (checkAuth) {
        setShowModalRSVP(true);
      } else {
        setShowModalLogin(true);
      }
    } catch (error) {
      setShowModalLogin(false);
      setShowModalRSVP(false);
    }
  };

  const closeModal = () => {
    setShowModalLogin(false);
    setShowModalRSVP(false);
  };

  const fetchEventDetail = () => {
    try {
      client
        .query({
          query: GET_EVENT_DETAIL_QUERY,
          variables: {
            id: parseInt(id, 10),
          },
        })
        .then(async response => {
          console.log('Response Event Detail: ', response);
          const {data, errors} = response;
          const {getEvent} = data;
          const {data: eventDetail, error} = getEvent;

          if (errors === undefined) {
            if (error === null) {
              await setDetail([...eventDetail]);
              await setIsError(false);
              await setIsLoading(false);
            } else {
              await setIsError(true);
              await setIsLoading(false);
            }
          } else {
            await setIsError(true);
            await setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error Query: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading === true && isError === false && detail.length === 0) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loader />
        </Content>
      </Container>
    );
  } else if (isLoading === false && isError === false && detail.length !== 0) {
    const IMAGE_HEIGHT = 188;
    const HEADER_HEIGHT =
      Platform.OS === 'android' ? (hasNotch() ? 80 : 70) : hasNotch() ? 80 : 70;
    const SCROLL_HEIGHT = IMAGE_HEIGHT - HEADER_HEIGHT;

    const TOP_BUTTON_DEFAULT_POSITION = hasNotch() ? 35 : 30;
    const TOP_BUTTON_POSITION_MINUS = hasNotch() ? 40 : 35;

    const scrollY = new Animated.Value(0);

    const headerOpacity = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [0.0, 0.5, 1.0],
    });

    const headerImageOpacity = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [1.0, 0.9, 0],
    });

    const imageScale = scrollY.interpolate({
      inputRange: [-25, 0],
      outputRange: [1.1, 1],
      extrapolateRight: 'clamp',
    });

    const buttonPosition = scrollY.interpolate({
      inputRange: [0, HEADER_HEIGHT],
      outputRange: [
        TOP_BUTTON_DEFAULT_POSITION,
        HEADER_HEIGHT - TOP_BUTTON_POSITION_MINUS,
      ],
      extrapolate: 'clamp',
    });

    const {
      id: eventId,
      description,
      eventBannerURL,
      eventBannerDynamicURL,
      name,
      freeGift,
      venueName,
      address1,
      address2,
      country,
      postCode,
      date,
      endDate,
      startTime,
      endTime,
      latitude,
      longitude,
    } = detail[0];

    const startDateEvent = moment.utc(date).local().format('ddd, DD MMM YYYY');
    const endDateEvent = moment.utc(endDate).local().format('ddd, DD MMM YYYY');
    const isSameDate = moment(startDateEvent).local().isSame(endDateEvent);

    const startTimeEvent = moment.utc(startTime).local().format('hh:mm a');
    const endTimeEvent = moment.utc(endTime).local().format('hh:mm a');

    const imageSource =
      eventBannerDynamicURL === null
        ? {uri: eventBannerURL}
        : {uri: `${eventBannerDynamicURL}=h300`};

    const openMap = () => {
      try {
        console.log('DETAIL OPEN MAP: ', detail[0]);
        const label = `${venueName}, ${country}`;

        const url = Platform.select({
          ios: `https://www.google.com/maps/search/?api=1&query=${label}&center=${latitude},${longitude}`,
          android: `geo:${latitude},${longitude}?q=${label}`,
        });
        Linking.canOpenURL(url)
          .then(supported => {
            if (!supported) {
              const browser_url = `https://www.google.de/maps/@${latitude},${longitude}?q=${label}`;
              return Linking.openURL(browser_url);
            } else {
              return Linking.openURL(url);
            }
          })
          .catch(error => console.log('Error: ', error));
      } catch (error) {
        console.log('Error: ', error);
      }
    };

    const goBack = () => {
      try {
        navigation.goBack(null);
      } catch (error) {
        navigation.goBack(null);
      }
    };

    return (
      <Container>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="dark-content"
          animated
        />
        <ModalLogin
          closeModal={closeModal}
          status={showModalLogin}
          navigation={navigation}
        />
        <ModalRSVP
          data={detail[0]}
          closeModal={closeModal}
          status={showModalRSVP}
          eventName={name}
          eventId={eventId}
          navigation={navigation}
        />
        <BackButton buttonPosition={buttonPosition} onPress={() => goBack()} />
        <HeaderEvent
          headerOpacity={headerOpacity}
          name={name}
          headerHeight={HEADER_HEIGHT}
        />
        <Animated.ScrollView
          ref={eventDetailRef}
          contentContainerStyle={{paddingBottom: 15}}
          scrollEventThrottle={Platform.OS === 'android' ? 16 : 1}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}>
          <View style={{flex: 1}}>
            <AnimatedHeaderImage
              containerStyle={{
                opacity: headerImageOpacity,
                transform: [
                  {translateY: Animated.multiply(scrollY, 0.65)},
                  {scale: imageScale},
                ],
              }}
              imageStyle={{
                aspectRatio: Dimensions.get('window').width / 188,
                width: Dimensions.get('window').width,
                height: 188,
                opacity: headerImageOpacity,
                transform: [{scale: imageScale}],
              }}
              imageSource={imageSource}
              placeholderColor={newContainerColor}
            />
            <Card
              transparent
              style={{
                marginLeft: 0,
                marginRight: 0,
              }}>
              <TitleEvent title={name} />
              <ScheduleEvent
                isSameDate={isSameDate}
                startDateEvent={startDateEvent}
                endDateEvent={endDateEvent}
                startTimeEvent={startTimeEvent}
                endTimeEvent={endTimeEvent}
              />
              <AddressEvent
                venueName={venueName}
                address1={address1}
                address2={address2}
                country={country}
                postCode={postCode}
              />
              <DescriptionEvent
                descriptionLength={descriptionLength}
                description={description}
                seeMore={seeMore}
                setSeeMore={() => setSeeMore(!seeMore)}
                setDescriptionLength={length => setDescriptionLength(length)}
              />
            </Card>
            <Map
              buttonOnTheRight={false}
              withDirectionText={true}
              forMapAddresses={`${venueName}, ${country}`}
              onNavigate={() => openMap()}
            />
          </View>
        </Animated.ScrollView>
        <FooterButton
          detail={detail[0]}
          showRSVP={() => showRSVP()}
          freeGift={freeGift}
        />
      </Container>
    );
  } else if (isLoading === false && isError === false && detail.length === 0) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <Header>
          <Left style={{flex: 0.2}}>
            <Button onPress={() => navigation.goBack(null)} transparent>
              <Icon
                type="Feather"
                name="chevron-left"
                style={{fontSize: 25, color: black}}
              />
            </Button>
          </Left>
          <Body
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
              Event
            </Text>
          </Body>
          <Right style={{flex: 0.2}} />
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.5),
              color: greyLine,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            Event Not Found
          </Text>
        </Content>
      </Container>
    );
  } else {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <Header>
          <Left style={{flex: 0.2}}>
            <Button onPress={() => navigation.goBack(null)} transparent>
              <Icon
                type="Feather"
                name="chevron-left"
                style={{fontSize: 25, color: black}}
              />
            </Button>
          </Left>
          <Body
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
              Event
            </Text>
          </Body>
          <Right style={{flex: 0.2}} />
        </Header>
        <ErrorScreen
          message="Refresh"
          onPress={async () => {
            try {
              await setDetail([]);
              await setIsError(false);
              await setIsLoading(true);
              await fetchEventDetail();
            } catch (error) {
              await setDetail([]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }}
        />
      </Container>
    );
  }
};

const Wrapper = compose(withApollo)(EventDetail);

export default props => <Wrapper {...props} />;
