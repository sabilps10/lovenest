import React, {useEffect} from 'react';
import {View, StyleSheet, Linking} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import getCustomers from '../graphql/queries/getCustomer';
import checkTokenExpired from '../graphql/queries/checkTokenExpired';
import Loader from '../components/Loader/circleLoader';
import {Container, Content} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import DynamicLinks from '@react-native-firebase/dynamic-links';

const Main = props => {
  console.log('Props Main: ', props);
  const {navigation} = props;
  useEffect(() => {
    // checkingUserlogin();
    handleDeeplink();
    const subs = navigation.addListener('focus', () => {
      handleDeeplink();
    });
    return subs;
  }, []);

  const handleDeeplink = async () => {
    try {
      const link = await processLink();
      console.log('BAPAKAKAKAKAKAKAKAKAKA ', link);
      if (link === '') {
        console.log('MASUK CHECK LOGIN');
        checkingUserlogin();
      } else {
        const trimed = link?.replace(
          'https://lovenest.page.link/app/?link=',
          '',
        );
        if (trimed) {
          Linking.canOpenURL(trimed).then(supported => {
            if (supported) {
              Linking.openURL(trimed?.replace('%3D', '='));
            } else {
              console.log("Don't know how to open URI: " + trimed);
            }
          });
        } else {
          console.log('MASUK CHECK LOGIN LAGI');
          checkingUserlogin();
        }
      }
    } catch (error) {
      console.log('Error Anu: ', error);
    }
  };

  const processLink = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const link = await DynamicLinks().getInitialLink();
        console.log('link: Main.js >> ', link);
        if (link) {
          resolve(link?.url ? link?.url : '');
        } else {
          const links = await DynamicLinks().onLink();
          console.log('LINKSSSSSSSSSSSSSSS: ', links);
          if (links) {
            resolve(links?.url ? links?.url : '');
          } else {
            resolve('');
          }
        }
      } catch (error) {
        resolve('');
      }
    });
  };

  // const getInitialLink = async () => {
  //   try {
  //     const link = await DynamicLinks().getInitialLink();
  //     console.log('getInitialLink: ', link);

  //     if (link) {
  //       const trimed = link?.url?.replace(
  //         'https://lovenest.page.link/app/?link=',
  //         '',
  //       );
  //       console.log('getInitialLink trimmed: ', trimed);
  //       if (trimed) {
  //         if (trimed) {
  //           Linking.canOpenURL(trimed).then(supported => {
  //             if (supported) {
  //               Linking.openURL(trimed?.replace('%3D', '='));
  //             } else {
  //               console.log("Don't know how to open URI: " + trimed);
  //             }
  //           });
  //         }
  //       }
  //     } else {
  //       console.log('MASUK ELSE GETINITIAL');
  //       checkingUserlogin();
  //     }
  //   } catch (error) {
  //     console.log('Error OnLink: ', error);
  //   }
  // };

  // const onLink = async () => {
  //   try {
  //     const link = await DynamicLinks().onLink();
  //     console.log('onLink: ', link);

  //     if (link) {
  //       const trimed = link?.url?.replace(
  //         'https://lovenest.page.link/app/?link=',
  //         '',
  //       );
  //       console.log('onLink trimmed: ', trimed);
  //       if (trimed) {
  //         if (trimed) {
  //           Linking.canOpenURL(trimed).then(supported => {
  //             if (supported) {
  //               Linking.openURL(trimed?.replace('%3D', '='));
  //             } else {
  //               console.log("Don't know how to open URI: " + trimed);
  //             }
  //           });
  //         }
  //       }
  //     } else {
  //       console.log('MASUK ELSE ONLINK');
  //       checkingUserlogin();
  //     }
  //   } catch (error) {
  //     console.log('Error OnLink: ', error);
  //   }
  // };

  // const openDeepLink = () => {
  //   getInitialLink();
  //   onLink();
  // };

  const checkingUserlogin = async () => {
    try {
      const getToken = await AsyncStorage.getItem('tsenevolpmibewliah');
      console.log('getToken: ', getToken);
      const getPhoneNumber = await AsyncStorage.getItem(
        'asyncConcatedPhoneNumber',
      );

      if (getToken && getPhoneNumber) {
        props.client
          .query({
            query: checkTokenExpired,
            fetchPolicy: 'no-cache',
            notifyOnNetworkStatusChange: true,
          })
          .then(result => {
            console.log('result: ', result);
            const {data} = result;
            const {checkTokenExpired: checkTokenExpiredResponse} = data;
            const {status} = checkTokenExpiredResponse;

            if (status) {
              props.client
                .query({
                  query: getCustomers,
                  fetchPolicy: 'no-cache',
                  notifyOnNetworkStatusChange: true,
                })
                .then(response => {
                  console.log('response: ', response);
                  const {data: getCustomerData} = response;
                  const {getCustomer: detailCustomer} = getCustomerData;

                  if (
                    detailCustomer.length === 1 &&
                    detailCustomer[0].name !== null
                  ) {
                    // Has token / already login
                    navigation.reset({
                      routes: [
                        {
                          name: 'MainHome',
                          params: {
                            name: 'jane',
                          },
                        },
                      ],
                    });
                  } else {
                    // No Token
                    navigation.navigate({
                      name: 'WelcomeScreen',
                      params: {name: 'jane'},
                    });
                  }
                })
                .catch(error => {
                  throw error;
                });
            } else {
              //  No Token
              navigation.reset({
                routes: [
                  {
                    name: 'WelcomeScreen',
                    params: {name: 'jane'},
                  },
                ],
              });
            }
          })
          .catch(error => {
            throw error;
          });
      } else {
        navigation.reset({
          routes: [
            {
              name: 'WelcomeScreen',
              params: {name: 'jane'},
            },
          ],
        });
      }
    } catch (error) {
      console.log('Error Main: ', error);
      navigation.reset({
        routes: [
          {
            name: 'WelcomeScreen',
            params: {name: 'jane'},
          },
        ],
      });
    }
  };
  return (
    <Container style={styles.container}>
      <Content
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{bottom: 30}}>
          <Loader />
        </View>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Wrapper = compose(withApollo)(Main);

export default props => <Wrapper {...props} />;
