import React from 'react';
import {Image, TouchableOpacity, Dimensions, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../utils/Themes/Colors';
import {Icon} from 'native-base';
import HomeMainSliderCard from '../../../components/Home/MainHomeSlider/MainHomeSlider';
import {QRCode, commonImage} from '../../../utils/Themes/Images';
const {QRCodePointViewFrame} = QRCode;
const {bellNotificationIcon, homeTextLogin, lnTextHome} = commonImage;
import {hasNotch} from 'react-native-device-info';
import {Platform} from 'react-native';

const gradientColor = ['#FFEDED', '#FACFC7', '#FACFC7'];
const {width, height} = Dimensions.get('window');
const {black, mainRed} = Colors;

const HeaderSlider = props => {
  const {
    onPress, //  navigate to login screen
    banners,
    isLoadingBanner,
    isErrorBanner,
    navigation,
    badge,
    isLogin,
  } = props;

  const goToNotificationScreen = () => {
    try {
      navigation.navigate('Notification');
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const goToQRScanScreen = () => {
    try {
      navigation.navigate('QR');
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View
      style={{
        width,
        flexDirection: 'column',
        height:
          Platform.OS === 'ios'
            ? hasNotch()
              ? height / 2.7
              : height / 2.45
            : hasNotch()
            ? height / 2.5
            : height / 2.45,
      }}>
      <View style={{flex: 1}}>
        <LinearGradient
          colors={gradientColor}
          style={{
            flexBasis: '65%',
            zIndex: 1,
          }}
        />
        <View
          style={{
            position: 'absolute',
            zIndex: 99,
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'space-between',
            paddingTop:
              Platform.OS === 'ios'
                ? hasNotch()
                  ? 60
                  : 40
                : hasNotch()
                ? 50
                : 40, // adjust height with notch or without
          }}>
          <View
            style={{
              width: '100%',
              paddingLeft: 15,
              paddingRight: 15,
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
            <View
              style={{
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Image
                source={lnTextHome}
                style={{width: width / 4, height: height / 25}}
                resizeMode="contain"
              />
            </View>
            <View
              style={{
                // borderWidth: 1,
                flexDirection: 'row',
                paddingRight: 6,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => goToQRScanScreen()}
                style={{marginHorizontal: 7}}>
                <Image
                  source={QRCodePointViewFrame}
                  style={{width: 24, height: 24}}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => goToNotificationScreen()}
                style={{marginHorizontal: 7}}>
                <Image
                  source={bellNotificationIcon}
                  style={{width: 24, height: 24}}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              {isLogin ? null : (
                <TouchableOpacity
                  onPress={() => onPress()}
                  style={{marginLeft: 7}}>
                  <Image
                    source={homeTextLogin}
                    style={{width: 43, height: 24}}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              )}
              {!isLogin ? null : (
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('FloristCart');
                  }}
                  style={{marginLeft: 7}}>
                  {badge > 0 ? (
                    <View
                      style={{
                        zIndex: 99,
                        borderWidth: 1,
                        borderColor: 'white',
                        width: 10,
                        height: 10,
                        borderRadius: 10 / 2,
                        backgroundColor: mainRed,
                        position: 'absolute',
                        top: 0,
                        right: 0,
                      }}
                    />
                  ) : null}
                  <Icon
                    type="Feather"
                    name="shopping-bag"
                    style={{fontSize: 17, color: black}}
                  />
                </TouchableOpacity>
              )}
            </View>
          </View>
          <HomeMainSliderCard
            navigation={navigation}
            banners={banners}
            isLoadingBanner={isLoadingBanner}
            isErrorBanner={isErrorBanner}
          />
        </View>
      </View>
    </View>
  );
};

const Wrapper = compose(withApollo)(HeaderSlider);

export default props => <Wrapper {...props} />;
