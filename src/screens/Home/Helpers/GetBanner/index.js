// TagId: 1 -> this tag for banners
import GetBannerQuery from '../../../../graphql/queries/banners';

const getBannerHelpers = client => {
  return new Promise((resolve, reject) => {
    try {
      client
        .query({
          query: GetBannerQuery,
          variables: {
            tagId: 1,
            itemDisplayed: 0,
            pageNumber: 1,
            searchBy: '',
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(response => {
          const {data, errors} = response;
          const {bannersMobile} = data;
          const {data: bannerList, error, totalCount} = bannersMobile;

          if (errors === undefined) {
            if (error === null) {
              resolve({
                data: bannerList,
                totalCount,
                error: null,
              });
            } else {
              resolve({
                data: null,
                totalCount: 0,
                error: String(error),
              });
            }
          } else {
            resolve({
              data: null,
              totalCount: 0,
              error: String(errors.message),
            });
          }
        })
        .catch(bannerError => {
          resolve({
            data: null,
            totalCount: 0,
            error: String(bannerError),
          });
        });
    } catch (error) {
      resolve({
        data: null,
        totalCount: 0,
        error: String(error),
      });
    }
  });
};

export default getBannerHelpers;
