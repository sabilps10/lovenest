// Benefit use tagId: 3
import GetBenefitQuery from '../../../../graphql/queries/banners';

const getBenefitHelper = client => {
  return new Promise((resolve, reject) => {
    try {
      client
        .query({
          query: GetBenefitQuery,
          variables: {
            tagId: 3,
            itemDisplayed: 4,
            pageNumber: 1,
            searchBy: '',
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(response => {
          const {data, errors} = response;
          const {bannersMobile} = data;
          const {data: benefitList, error, totalCount} = bannersMobile;

          if (errors === undefined) {
            if (error === null) {
              resolve({
                data: benefitList,
                totalCount,
                error: null,
              });
            } else {
              resolve({
                data: null,
                totalCount: 0,
                error: String(error),
              });
            }
          } else {
            resolve({
              data: null,
              totalCount: 0,
              error: String(errors.message),
            });
          }
        })
        .catch(bannerError => {
          resolve({
            data: null,
            totalCount: 0,
            error: String(bannerError),
          });
        });
    } catch (error) {
      resolve({
        data: null,
        totalCount: 0,
        error: String(error),
      });
    }
  });
};

export default getBenefitHelper;
