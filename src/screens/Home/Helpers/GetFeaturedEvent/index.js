import GetAllFeaturedEvent from '../../../../graphql/queries/getAllFeaturedEvents';

const getAllFeaturedEvent = client => {
  return new Promise((resolve, reject) => {
    try {
      client
        .query({
          query: GetAllFeaturedEvent,
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(response => {
          console.log('FEATURED EVENT: ', response);
          const {data, errors} = response;
          const {getAllFeaturedEvents} = data;
          const {
            data: featuredEvent,
            totalData: totalCount,
            error,
          } = getAllFeaturedEvents;

          if (errors === undefined) {
            if (error === null) {
              resolve({
                data: featuredEvent,
                totalCount,
                error: null,
              });
            } else {
              resolve({
                data: null,
                totalCount: 0,
                error: String(error),
              });
            }
          } else {
            resolve({
              data: null,
              totalCount: 0,
              error: String(errors.message),
            });
          }
        })
        .catch(upComingEventError => {
          resolve({
            data: null,
            totalCount: 0,
            error: String(upComingEventError),
          });
        });
    } catch (error) {
      resolve({
        data: null,
        totalCount: 0,
        error: String(error),
      });
    }
  });
};

export default getAllFeaturedEvent;
