import GetSpecialOfferQuery from '../../../../graphql/queries/getAllPromotions';

const getSpecialOfferHelper = client => {
  return new Promise((resolve, reject) => {
    try {
      client
        .query({
          query: GetSpecialOfferQuery,
          variables: {
            upcoming: true,
            itemDisplayed: 5,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(response => {
          console.log('getSpecialOfferHelper: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: specialOfferList, error, totalData} = getAllPromotions;

          if (errors === undefined) {
            if (error === null) {
              resolve({
                data: specialOfferList,
                totalCount: totalData,
                error: null,
              });
            } else {
              resolve({
                data: null,
                error: String(error),
              });
            }
          } else {
            resolve({
              data: null,
              error: String(errors),
            });
          }
        })
        .catch(error => {
          console.log('Get Special Offer Helper Error: ', error);
          resolve({
            data: null,
            error: String(error),
          });
        });
    } catch (error) {
      console.log('Special Offer Helper Error: ', error);
      resolve({
        data: null,
        error: String(error),
      });
    }
  });
};

export default getSpecialOfferHelper;
