import GetAllUpComingEventQuery from '../../../../graphql/queries/getAllUpComingEvents';

const getAllUpComingEventHelper = client => {
  return new Promise((resolve, reject) => {
    try {
      const pageSize = 5;
      const pageNumber = 1;

      client
        .query({
          query: GetAllUpComingEventQuery,
          variables: {
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(response => {
          const {data, errors} = response;
          const {getAllUpcomingEvents} = data;
          const {
            data: upComingEventList,
            totalData: totalCount,
            error,
          } = getAllUpcomingEvents;

          if (errors === undefined) {
            if (error === null) {
              resolve({
                data: upComingEventList,
                totalCount,
                error: null,
              });
            } else {
              resolve({
                data: null,
                totalCount: 0,
                error: String(error),
              });
            }
          } else {
            resolve({
              data: null,
              totalCount: 0,
              error: String(errors.message),
            });
          }
        })
        .catch(upComingEventError => {
          resolve({
            data: null,
            totalCount: 0,
            error: String(upComingEventError),
          });
        });
    } catch (error) {
      resolve({
        data: null,
        totalCount: 0,
        error: String(error),
      });
    }
  });
};

export default getAllUpComingEventHelper;
