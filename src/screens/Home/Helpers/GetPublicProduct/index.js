import GET_PUBLIC_PRODUCT_QUERY from '../../../../graphql/queries/productsPublic';

const getPublicProducts = client => {
  return new Promise((resolve, reject) => {
    try {
      client
        .query({
          query: GET_PUBLIC_PRODUCT_QUERY,
          variables: {
            serviceType: 'Bridal',
            pageSize: 2,
            pageNumber: 1,
          },
        })
        .then(response => {
          console.log('Response Public Product Helpers: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: listPublicProduct, error, totalData} = productsPublic;

          if (errors) {
            resolve({
              data: null,
              totalCount: 0,
              error: String(errors),
            });
          } else {
            if (error) {
              resolve({
                data: null,
                totalCount: 0,
                error: String(error),
              });
            } else {
              resolve({
                data: listPublicProduct,
                totalCount: totalData,
                error: null,
              });
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          resolve({
            data: null,
            totalCount: 0,
            error: String(error),
          });
        });
    } catch (error) {
      resolve({
        data: null,
        totalCount: 0,
        error: String(error),
      });
    }
  });
};

export default getPublicProducts;