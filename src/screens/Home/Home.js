import React, {useEffect, useState} from 'react';
import {
  RefreshControl,
  StatusBar,
  ScrollView,
  Animated,
  Platform,
  PermissionsAndroid,
  Alert,
  Linking,
  View,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {Container} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import {getAppstoreAppMetadata} from 'react-native-appstore-version-checker';
import Axios from 'axios';

// QUERY
import FloristCartBadge from '../../graphql/queries/floristCartBadge';

// QUERY HELPERS
import GET_PUBLIC_PRODUCT from './Helpers/GetPublicProduct';
import GET_BANNER_HELPER from './Helpers/GetBanner';
import GET_UP_Coming_EVENT_HELPER from './Helpers/GetUpComingEvent';
import GET_BENEFIT_HELPER from './Helpers/GetBenefit';
import GET_SPECIAL_OFFER_HELPER from './Helpers/GetSpecialOffer';

// Component
import Headers from './SpecialComponents/HeaderSlider';
import Menu from '../../components/Home/Menu/Menu';
import Events from '../../components/Home/Event/Event';
import Benefit from '../../components/Home/Benefit/Benefit';
import BenefitShimmer from '../../components/Loader/Home/benefitShimmer';
import SpecialOfferShimmer from '../../components/Loader/Home/specialOfferShimmer';
import SpecialOffers from '../../components/Home/SpecialOffer/Slider';

import AsyncStorage from '@react-native-community/async-storage';
import AsyncToken from '../../utils/AsyncstorageDataStructure/index';

const {white} = Colors;
const {asyncToken} = AsyncToken;

const Home = props => {
  console.log('Home Props', props);
  const {navigation, homeTabRef, client, positionYBottomNav} = props;

  const appleLink = 'https://apps.apple.com/us/app/love-nest/id1463134198';
  const androidLink =
    'https://play.google.com/store/apps/details?id=sg.com.lovenest.android&hl=en';

  // Banners
  const [isLoadingBanner, setIsLoadingBanner] = useState(true);
  const [isErrorBanner, setIsErrorBanner] = useState(false);
  const [banners, setBanners] = useState([]);

  // UpCooming Event
  const [isLoadingUpComingEvent, setIsLoadingUpComingEvent] = useState(true);
  const [isErrorUpComingEvent, setIsErrorUpComingEvent] = useState(false);
  const [upComingEvent, setUpComingEvent] = useState([]);

  // Public Products
  const [, setIsLoadingPublicProduct] = useState(true);
  const [, setIsErrorPublicProduct] = useState(false);
  const [, setPublicProduct] = useState([]);

  // Benefit
  const [isLoadingBenefit, setIsLoadingBenefit] = useState(true);
  const [isErrorBenefit, setIsErrorBenefit] = useState(false);
  const [benefit, setBenefit] = useState([]);
  const [totalCountBenefit, setTotalCountBenefit] = useState(0);

  // Special offers
  const [isLoadingSpecialOffer, setIsLoadingSpecialOffer] = useState(true);
  const [isErrorSpecialOffer, setIsErrorSpecialOffer] = useState(false);
  const [specialOffer, setSpecialOffer] = useState([]);
  const [totalCountSpecialOffer, setTotalCountSpecialOffer] = useState(0);

  // Badge
  const [badge, setBadge] = useState(0);

  // Refreshner
  const [pullToRefresh, setPullToRefresh] = useState(false);

  //  isLogin
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor('transparent');
    StatusBar.setBarStyle('dark-content');
    checkIsLogin();
    requestCameraPermission();
    navigationOptions();
    checkVersion();
    fetchBanner();
    fetchUpComingEvent();
    fetchPublicProduct();
    fetchBenefit();
    fetchSpecialOffer();
    fetchBadge();
    onChangeOpacity(true);
    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle('dark-content');
      checkIsLogin();
      requestCameraPermission();
      navigationOptions();
      fetchBanner();
      fetchUpComingEvent();
      fetchPublicProduct();
      fetchBenefit();
      fetchSpecialOffer();
      fetchBadge();
      onChangeOpacity(true);
    });
    return subscriber;
  }, [navigation]);

  const fetchBadge = () => {
    try {
      client
        .query({
          query: FloristCartBadge,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          const {data, errors} = response;
          const {cartBadge} = data;
          const {totalCount, error} = cartBadge;

          if (errors) {
            await setBadge(0);
          } else {
            if (error) {
              await setBadge(0);
            } else {
              if (totalCount) {
                await setBadge(totalCount);
              } else {
                await setBadge(0);
              }
            }
          }
        })
        .catch(async error => {
          console.log('error: ', error);
          await setBadge(0);
        });
    } catch (error) {
      console.log('Error: ', error);
      setBadge(0);
    }
  };

  const fetchSpecialOffer = async () => {
    try {
      const getSpecialOffer = await GET_SPECIAL_OFFER_HELPER(client);
      const {error, data, totalCount} = getSpecialOffer;
      if (error === null) {
        await setTotalCountSpecialOffer(totalCount);
        await setSpecialOffer([...data]);
        await setIsErrorSpecialOffer(false);
        await setIsLoadingSpecialOffer(false);
      } else {
        await setIsErrorSpecialOffer(false);
        await setIsLoadingSpecialOffer(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      console.log('Error Special Offer: ', error);
      setIsErrorSpecialOffer(false);
      setIsErrorSpecialOffer(false);
      setPullToRefresh(false);
    }
  };

  const fetchBenefit = async () => {
    try {
      const getBenefit = await GET_BENEFIT_HELPER(client);
      const {error, data, totalCount} = getBenefit;
      if (error === null) {
        await setTotalCountBenefit(totalCount);
        await setBenefit([...data]);
        await setIsErrorBenefit(false);
        await setIsLoadingBenefit(false);
      } else {
        await setIsErrorBenefit(false);
        await setIsLoadingBenefit(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorBenefit(false);
      setIsLoadingBenefit(false);
      setPullToRefresh(false);
    }
  };

  const checkVersion = () => {
    const versionApps = DeviceInfo.getVersion();
    console.log('VERSION: ', versionApps);
    if (Platform.OS === 'android') {
      getAppstoreAppMetadata('sg.com.lovenest.android') // put any apps packageId here
        .then(metadata => {
          console.log(
            'clashofclans android app version on playstore',
            metadata,
          );
          if (
            metadata.version !== undefined &&
            metadata.version !== versionApps
          ) {
            Alert.alert(
              `New Version ${metadata.version}`,
              'Please update the apps',
              [
                {
                  text: 'Ask me later',
                  onPress: () => console.log('Ask me later pressed'),
                },
                {
                  text: 'Update',
                  onPress: () => {
                    Linking.canOpenURL(androidLink)
                      .then(supported => {
                        if (supported) {
                          return Linking.openURL(androidLink);
                        }
                      })
                      .catch(error => {
                        console.log('Error: ', error);
                      });
                  },
                },
              ],
              {cancelable: false},
            );
          }
        })
        .catch(err => {
          console.log('error occurred', err);
        });
    }

    if (Platform.OS === 'ios') {
      const storeSpecificId =
        'https://itunes.apple.com/lookup?bundleId=sg.com.lovenest.customer&country=sg';
      Axios.get(storeSpecificId)
        .then(appVersion => {
          console.log(
            'clashofclans android app version on appstore',
            appVersion,
          );
          if (
            appVersion.data.resultCount === 1 &&
            appVersion.data.results[0].version !== versionApps
          ) {
            Alert.alert(
              `New Version ${appVersion.data.results[0].version}`,
              'Please update the apps',
              [
                {
                  text: 'Ask me later',
                  onPress: () => console.log('Ask me later pressed'),
                },
                {
                  text: 'Update',
                  onPress: () => {
                    Linking.canOpenURL(appleLink)
                      .then(supported => {
                        if (supported) {
                          return Linking.openURL(appleLink);
                        }
                      })
                      .catch(error => {
                        console.log('Error: ', error);
                      });
                  },
                },
              ],
              {cancelable: false},
            );
          }
        })
        .catch(err => {
          console.log('error occurred', err);
        });
    }
  };

  const checkIsLogin = async () => {
    const userToken = await AsyncStorage.getItem(asyncToken);
    if (userToken) {
      await setIsLogin(true);
      // await checkPermission(client, navigation);
    }
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const settingPullToRefresh = async () => {
    await setPullToRefresh(true);
    await refetch();
  };

  const refetch = async () => {
    try {
      await fetchBanner();
      await fetchUpComingEvent();
      await setPullToRefresh(false);
    } catch (error) {
      setPullToRefresh(false);
      setIsLoadingBanner(false);
      setIsLoadingUpComingEvent(false);

      setIsErrorBanner(false);
      setIsErrorUpComingEvent(false);
    }
  };

  const fetchPublicProduct = async () => {
    try {
      const publicProductsAPI = await GET_PUBLIC_PRODUCT(client);
      console.log('Dapet Bro Public Product: ', publicProductsAPI);
      const {error, data} = publicProductsAPI;

      if (error) {
        // Failed
        await setPublicProduct([]);
        await setIsErrorPublicProduct(true);
        await setIsLoadingPublicProduct(false);
      } else {
        // success
        await setPublicProduct([...data]);
        await setIsErrorPublicProduct(false);
        await setIsLoadingPublicProduct(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      setPublicProduct([]);
      setIsErrorPublicProduct(true);
      setIsLoadingPublicProduct(false);
    }
  };

  const fetchBanner = async () => {
    try {
      const getBanner = await GET_BANNER_HELPER(client);
      const {error, data} = getBanner;
      if (error === null) {
        await setBanners([...data]);
        await setIsErrorBanner(false);
        await setIsLoadingBanner(false);
      } else {
        await setIsErrorBanner(true);
        await setIsLoadingBanner(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorBanner(true);
      setIsLoadingBanner(false);
      setPullToRefresh(false);
    }
  };

  const fetchUpComingEvent = async () => {
    try {
      const getUpComingEvent = await GET_UP_Coming_EVENT_HELPER(client);
      const {error, data} = getUpComingEvent;
      if (error === null) {
        await setUpComingEvent([...data]);
        await setIsErrorUpComingEvent(false);
        await setIsLoadingUpComingEvent(false);
      } else {
        await setIsErrorUpComingEvent(true);
        await setIsLoadingUpComingEvent(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorUpComingEvent(true);
      setIsLoadingUpComingEvent(false);
      setPullToRefresh(false);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerShown: false,
    });
  };

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'QR Code Scanner',
          message:
            'Love Nest App needs access to your camera to use QR Code Scanner ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
        return true;
      }
      console.log('Camera permission denied');
      return false;
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container style={{backgroundColor: white}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle={'dark-content'}
        animated
        showHideTransition="fade"
      />
      <ScrollView
        onScrollBeginDrag={() => onChangeOpacity(false)}
        onScrollEndDrag={() => onChangeOpacity(true)}
        refreshControl={
          <RefreshControl
            title="Refreshing"
            refreshing={pullToRefresh}
            onRefresh={() => settingPullToRefresh()}
          />
        }
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 30}}
        ref={homeTabRef}>
        <View style={{flex: 1}}>
          {/* Headers */}
          <Headers
            onPress={() => navigation.navigate('Login', {showXIcon: true})}
            isLogin={isLogin}
            badge={badge}
            navigation={props?.navigation}
            banners={banners}
            isLoadingBanner={isLoadingBanner}
            isErrorBanner={isErrorBanner}
          />

          {/* Menu */}
          <Menu {...props} />

          {/* Event */}
          <Events
            isLoadingUpComingEvent={isLoadingUpComingEvent}
            isErrorUpComingEvent={isErrorUpComingEvent}
            upComingEvent={upComingEvent}
            {...props}
          />

          {/* Special Offer */}
          <View>
            {isLoadingSpecialOffer ? (
              <SpecialOfferShimmer />
            ) : isErrorSpecialOffer ? null : (
              <SpecialOffers
                {...props}
                data={specialOffer}
                totalCount={totalCountSpecialOffer}
              />
            )}
          </View>

          {/* Benefit */}
          <View>
            {isLoadingBenefit ? (
              <BenefitShimmer />
            ) : isErrorBenefit ? null : (
              <Benefit
                {...props}
                data={benefit}
                totalCount={totalCountBenefit}
              />
            )}
          </View>
        </View>
      </ScrollView>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Home);

export default props => <Wrapper {...props} />;
