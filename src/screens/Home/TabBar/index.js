import React, {useRef} from 'react';
import {View, FlatList, Dimensions, StyleSheet} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {
  topNavigationActive,
  topNavigationInactive,
} from '../../../utils/Themes/Images';
import TabBarButton from './TabBarButtons/TabBarButton';
import {FontType} from '../../../utils/Themes/Fonts';

const {bold, medium} = FontType;
const {white, black, lightSalmon, veryLightPinkTwo} = Colors;

const {
  TopNavActiveToday,
  TopNavActiveEvent,
  TopNavActiveBridal,
  TopNavActiveJewellery,
  TopNavActiveInteriorDesign,
  TopNavActiveVenue,
  TopNavActiveSmartHome,
  TopNavActiveCafe,
} = topNavigationActive;

const {
  TopNavInactiveToday,
  TopNavInactiveEvent,
  TopNavInactiveBridal,
  TopNavInactiveJewellery,
  TopNavInactiveVenue,
  TopNavInactiveInteriorDesign,
  TopNavInactiveSmartHome,
  TopNavInactiveCafe,
} = topNavigationInactive;

const TabBar = props => {
  const tabBarRef = useRef();

  const {state, descriptors, navigation} = props;
  const {routes} = state;
  return (
    <View style={styles.container}>
      <FlatList
        data={routes}
        ref={tabBarRef}
        getItemLayout={(data, index) => {
          // Max 5 items visibles at once
          return {
            length: Dimensions.get('window').width / 5,
            offset: (Dimensions.get('window').width / 5) * index,
            index,
          };
        }}
        snapToAlignment={'center'}
        snapToInterval={Dimensions.get('window').width / 5}
        horizontal
        showsHorizontalScrollIndicator={false}
        scrollEnabled
        listKey={(item, index) => `${String(index)} TabBarKey`}
        renderItem={({item: route, index}) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              tabBarRef.current.scrollToIndex({
                animated: true,
                index,
                viewOffset: Dimensions.get('window').width / 3,
              });
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          const icon =
            label === 'Today'
              ? isFocused
                ? TopNavActiveToday
                : TopNavInactiveToday
              : label === 'Events'
              ? isFocused
                ? TopNavActiveEvent
                : TopNavInactiveEvent
              : label === 'Bridal'
              ? isFocused
                ? TopNavActiveBridal
                : TopNavInactiveBridal
              : label === 'Jewellery'
              ? isFocused
                ? TopNavActiveJewellery
                : TopNavInactiveJewellery
              : label === 'Venue'
              ? isFocused
                ? TopNavActiveVenue
                : TopNavInactiveVenue
              : label === 'Interior'
              ? isFocused
                ? TopNavActiveInteriorDesign
                : TopNavInactiveInteriorDesign
              : label === 'Smart Home'
              ? isFocused
                ? TopNavActiveSmartHome
                : TopNavInactiveSmartHome
              : isFocused
              ? TopNavActiveCafe
              : TopNavInactiveCafe;

          const color =
            label === 'Today'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Events'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Bridal'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Jewellery'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Venue'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Interior'
              ? isFocused
                ? lightSalmon
                : black
              : label === 'Smart Home'
              ? isFocused
                ? lightSalmon
                : black
              : isFocused
              ? lightSalmon
              : black;

          const borderWidthStyle =
            label === 'Today'
              ? isFocused
                ? 4
                : 0
              : label === 'Events'
              ? isFocused
                ? 4
                : 0
              : label === 'Bridal'
              ? isFocused
                ? 4
                : 0
              : label === 'Jewellery'
              ? isFocused
                ? 4
                : 0
              : label === 'Venue'
              ? isFocused
                ? 4
                : 0
              : label === 'Interior'
              ? isFocused
                ? 4
                : 0
              : label === 'Smart Home'
              ? isFocused
                ? 4
                : 0
              : isFocused
              ? 4
              : 0;

          const fontFamilys =
            label === 'Today'
              ? isFocused
                ? bold
                : medium
              : label === 'Events'
              ? isFocused
                ? bold
                : medium
              : label === 'Bridal'
              ? isFocused
                ? bold
                : medium
              : label === 'Jewellery'
              ? isFocused
                ? bold
                : medium
              : label === 'Venue'
              ? isFocused
                ? bold
                : medium
              : label === 'Interior'
              ? isFocused
                ? bold
                : medium
              : label === 'Smart Home'
              ? isFocused
                ? bold
                : medium
              : isFocused
              ? bold
              : medium;

          return (
            <TabBarButton
              key={String(route.key)}
              accessibilityRole={'button'}
              accessibilityStates={['selected']}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={() => onPress()}
              onLongPress={() => onLongPress()}
              image={icon}
              color={color}
              label={label}
              borderWidthStyle={borderWidthStyle}
              fontFamilys={fontFamilys}
            />
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 75,
    width: Dimensions.get('window').width,
    backgroundColor: 'white',
    borderBottomColor: veryLightPinkTwo,
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderTopColor: white,
    paddingTop: 5,
  },
});

const Wrapper = compose(withApollo)(TabBar);

export default props => <Wrapper {...props} />;
