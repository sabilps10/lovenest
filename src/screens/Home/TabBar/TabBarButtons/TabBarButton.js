import React from 'react';
import {Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {medium} = FontType;

const TabBarButtonButton = props => {
  const {
    key,
    accessibilityRole,
    accessibilityStates,
    accessibilityLabel,
    testID,
    onPress,
    onLongPress,
    image,
    label,
    color,
    borderWidthStyle,
    fontFamilys,
  } = props;

  return (
    <>
      <TouchableOpacity
        key={key}
        accessibilityRole={accessibilityRole}
        accessibilityStates={accessibilityStates}
        accessibilityLabel={accessibilityLabel}
        testID={testID}
        onPress={onPress}
        onLongPress={onLongPress}
        style={{
          height: '100%',
          minWidth:
            label === 'Smart Home'
              ? Dimensions.get('window').width / 3.9
              : Dimensions.get('window').width / 4.9,
          flexWrap: 'wrap',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          borderBottomColor: color,
          borderBottomWidth: borderWidthStyle,
          marginHorizontal: 2,
        }}>
        <Image
          source={image}
          style={
            label === 'Today'
              ? {
                  position: 'absolute',
                  width: 20,
                  height: 20,
                  top: 15,
                  aspectRatio: 20 / 20,
                }
              : {
                  position: 'absolute',
                  width: 25,
                  height: 25,
                  top: 10,
                  aspectRatio: 25 / 25,
                }
          }
        />
        <Text
          style={{
            position: 'absolute',
            // textAlign: 'center',
            bottom: label === 'Today' ? 5 : 5,
            color: color,
            fontFamily: fontFamilys,
            fontSize: RFPercentage(1.75),
            letterSpacing: 0.2,
          }}>
          {label}
        </Text>
      </TouchableOpacity>
    </>
  );
};

const Wrapper = compose(withApollo)(TabBarButtonButton);

export default props => <Wrapper {...props} />;
