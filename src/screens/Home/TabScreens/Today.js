import React, {useState, useEffect} from 'react';
import {Dimensions, RefreshControl, ScrollView, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

// QUERY HELPERS
import GET_BANNER_HELPER from '../Helpers/GetBanner';
import GET_UP_Coming_EVENT_HELPER from '../Helpers/GetUpComingEvent';
import GET_SPECIAL_OFFER_HELPER from '../Helpers/GetSpecialOffer';
import GET_BENEFIT_HELPER from '../Helpers/GetBenefit';

// Components
import BannerSlider from '../../../components/Home/Banner/Slider';
import EventSlider from '../../../components/Event/SliderEvent';
import SpecialOfferSlider from '../../../components/Home/SpecialOffer/Slider';
import Benefit from '../../../components/Home/Benefit/Benefit';

// Shimmer
import BannerLoader from '../../../components/Loader/Home/bannerShimmer';
import UpComingEventShimmer from '../../../components/Loader/Home/upComingEventShimmer';
import SpecialOfferShimmer from '../../../components/Loader/Home/specialOfferShimmer';
import BenefitShimmer from '../../../components/Loader/Home/benefitShimmer';

const {newContainerColor} = Colors;

const Today = props => {
  console.log('Today Props: >>>>>>> ', props);
  const {navigation, client, homeTabRef} = props;

  const [isLoadingBanner, setIsLoadingBanner] = useState(true);
  const [isLoadingUpComingEvent, setIsLoadingUpComingEvent] = useState(true);
  const [isLoadingSpecialOffer, setIsLoadingSpecialOffer] = useState(true);
  const [isLoadingBenefit, setIsLoadingBenefit] = useState(true);

  const [isErrorBanner, setIsErrorBanner] = useState(false);
  const [isErrorUpComingEvent, setIsErrorUpComingEvent] = useState(false);
  const [isErrorSpecialOffer, setIsErrorSpecialOffer] = useState(false);
  const [isErrorBenefit, setIsErrorBenefit] = useState(false);

  const [banners, setBanners] = useState([]);
  const [upComingEvent, setUpComingEvent] = useState([]);
  const [specialOffer, setSpecialOffer] = useState([]);
  const [benefit, setBenefit] = useState([]);

  const [pullToRefresh, setPullToRefresh] = useState(false);

  useEffect(() => {
    StatusBar.setBackgroundColor('white');
    fetchBanner();
    fetchUpComingEvent();
    fetchSpecialOffer();
    fetchBenefit();

    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setBackgroundColor('white');
      settingPullToRefresh();
    });

    const subscriberTabPress = navigation.addListener('tabPress', () => {
      StatusBar.setBackgroundColor('white');
      settingPullToRefresh();
    });

    return () => {
      subscriber();
      subscriberTabPress();
    };
  }, []);

  const settingPullToRefresh = () => {
    setPullToRefresh(true);
    refetch();
  };

  const refetch = async () => {
    try {
      await fetchBanner();
      await fetchUpComingEvent();
      await fetchSpecialOffer();
      await fetchBenefit();
      await setPullToRefresh(false);
    } catch (error) {
      setPullToRefresh(false);
      setIsLoadingBanner(false);
      setIsLoadingUpComingEvent(false);
      setIsLoadingSpecialOffer(false);
      setIsLoadingBenefit(false);

      setIsErrorBanner(false);
      setIsErrorUpComingEvent(false);
      setIsErrorSpecialOffer(false);
      setIsErrorBenefit(false);
    }
  };

  const fetchBanner = async () => {
    try {
      const getBanner = await GET_BANNER_HELPER(client);
      const {error, data} = getBanner;
      if (error === null) {
        await setBanners([...data]);
        await setIsErrorBanner(false);
        await setIsLoadingBanner(false);
      } else {
        await setIsErrorBanner(true);
        await setIsLoadingBanner(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorBanner(true);
      setIsLoadingBanner(false);
      setPullToRefresh(false);
    }
  };

  const fetchUpComingEvent = async () => {
    try {
      const getUpComingEvent = await GET_UP_Coming_EVENT_HELPER(client);
      const {error, data} = getUpComingEvent;
      if (error === null) {
        await setUpComingEvent([...data]);
        await setIsErrorUpComingEvent(false);
        await setIsLoadingUpComingEvent(false);
      } else {
        await setIsErrorUpComingEvent(true);
        await setIsLoadingUpComingEvent(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorUpComingEvent(true);
      setIsLoadingUpComingEvent(false);
      setPullToRefresh(false);
    }
  };

  const fetchSpecialOffer = async () => {
    try {
      const getSpecialOffer = await GET_SPECIAL_OFFER_HELPER(client);
      const {error, data} = getSpecialOffer;
      if (error === null) {
        await setSpecialOffer([...data]);
        await setIsErrorSpecialOffer(false);
        await setIsLoadingSpecialOffer(false);
      } else {
        await setIsErrorSpecialOffer(true);
        await setIsLoadingSpecialOffer(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorSpecialOffer(true);
      setIsLoadingSpecialOffer(false);
      setPullToRefresh(false);
    }
  };

  const fetchBenefit = async () => {
    try {
      const getBenefit = await GET_BENEFIT_HELPER(client);
      const {error, data} = getBenefit;
      if (error === null) {
        await setBenefit([...data]);
        await setIsErrorBenefit(false);
        await setIsLoadingBenefit(false);
      } else {
        await setIsErrorBenefit(false);
        await setIsLoadingBenefit(false);
        await setPullToRefresh(false);
      }
    } catch (error) {
      setIsErrorBenefit(false);
      setIsLoadingBenefit(false);
      setPullToRefresh(false);
    }
  };

  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <ScrollView
        ref={homeTabRef}
        refreshControl={
          <RefreshControl
            refreshing={pullToRefresh}
            onRefresh={settingPullToRefresh}
          />
        }>
        {isLoadingBanner ? (
          <BannerLoader />
        ) : isErrorBanner ? null : (
          <BannerSlider
            showIndicator={true}
            containerStyle={{
              shadowOpacity: 0,
              elevation: 0,
              width: Dimensions.get('window').width,
              height: 188,
            }}
            imageStyle={{
              width: Dimensions.get('window').width,
              height: 188,
              aspectRatio: Dimensions.get('window').width / 188,
            }}
            imageLoaderStyle={{
              width: Dimensions.get('window').width / 7,
              height: Dimensions.get('window').width / 7,
            }}
            data={banners}
            timer={3000}
          />
        )}
        {isLoadingUpComingEvent ? (
          <UpComingEventShimmer />
        ) : isErrorUpComingEvent ? null : (
          <EventSlider data={upComingEvent} {...props} />
        )}
        {isLoadingSpecialOffer ? (
          <SpecialOfferShimmer />
        ) : isErrorSpecialOffer ? null : (
          <SpecialOfferSlider data={specialOffer} />
        )}
        {isLoadingBenefit ? (
          <BenefitShimmer />
        ) : isErrorBenefit ? null : (
          <Benefit data={benefit} />
        )}
      </ScrollView>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Today);

export default props => <Wrapper {...props} />;
