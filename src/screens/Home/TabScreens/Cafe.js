import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, FlatList, Dimensions, RefreshControl} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

// Common Screen
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';
import CardWithoutSpecialOffer from '../../../components/Cards/Merchants/CardWithoutSpecialOffer';
import CardWithSpecialOffer from '../../../components/Cards/Merchants/CardWithSpecialOffer';

// Shimmer
import MerchantShimmerComponent from '../../../components/Loader/Merchant/MerchantListShimmer';

// Query Graphql
import GET_MERCHANT_BY_SERVICE_TYPE from '../../../graphql/queries/getMerchantByServiceType';

// Empty Merchant
import EmptyMerchant from '../../../components/Cards/Merchants/EmptyMerchant';

const {newContainerColor} = Colors;

const TAB_TYPE = 'Cafe';
const pageNumber = 1;

const {height} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

export const ListMerchant = props => {
  const {
    merchantList,
    refreshing,
    onRefresh,
    settingIsLoadMore,
    isLoadMore,
    pageSize,
    totalCount,
    navigation,
  } = props;
  console.log('merchantList ListMerchant Props: ', merchantList);

  const keyExt = (item, _) => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };
  const ListCard = () =>
    useMemo(() => {
      return (
        <FlatList
          disableVirtualization
          decelerationRate="fast"
          legacyImplementation
          ListEmptyComponent={() => {
            return <EmptyMerchant />;
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          contentContainerStyle={{
            paddingTop: 15,
            paddingBottom: 15,
            alignItems: 'center',
          }}
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <View
                  style={{
                    width: '100%',
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text>Load More...</Text>
                </View>
              );
            } else {
              return null;
            }
          }}
          onEndReached={() => {
            if (pageSize <= totalCount) {
              settingIsLoadMore(true);
            } else {
              settingIsLoadMore(false);
            }
          }}
          onEndReachedThreshold={0.01}
          extraData={merchantList}
          data={merchantList}
          keyExtractor={keyExt}
          getItemLayout={getItemLayout}
          renderItem={({item}) => {
            const id = item.id;
            const serviceType = item.serviceType;
            const logoSource =
              item.coverImageDynamicUrl === null
                ? {uri: item.coverImageUrl}
                : {uri: `${item.coverImageDynamicUrl}=h500`};
            const iconSource =
              item.logoImageDynamicUrl === null
                ? {uri: item.logoImageUrl}
                : {uri: `${item.logoImageDynamicUrl}=h200`};
            const specialOffersData =
              item.promotion === null || item.promotion === undefined
                ? null
                : item.promotion;
            const name = item.name;
            const tagLine = item.tagline;

            if (specialOffersData && merchantList.length === 1) {
              return (
                <>
                  <CardWithSpecialOffer
                    id={id}
                    serviceType={serviceType}
                    logoSource={logoSource}
                    iconSource={iconSource}
                    name={name}
                    specialOffersData={specialOffersData}
                    tagLine={tagLine}
                    navigation={navigation}
                  />
                </>
              );
            } else {
              return (
                <>
                  <CardWithoutSpecialOffer
                    id={id}
                    serviceType={serviceType}
                    logoSource={logoSource}
                    iconSource={iconSource}
                    name={name}
                    tagLine={tagLine}
                    navigation={navigation}
                  />
                </>
              );
            }
          }}
        />
      );
    }, [isLoadMore, merchantList, refreshing]);

  if (merchantList) {
    return ListCard();
  } else {
    return null;
  }
};

const Cafe = props => {
  console.log('Bridal Props: ', props);
  const {navigation, client} = props;

  const [pageSize, setPageSize] = useState(10);
  const [totalCount, setTotalCount] = useState(0);
  const [isPullOnRefresh, setIsPullOnRefresh] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [merchantList, setMerchantList] = useState([]);

  useEffect(() => {
    fetchMerchantByServiceType();
  }, []);

  const pullToRefresh = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await fetchMerchantByServiceType();
  };

  const settingIsLoadMore = async value => {
    await setIsLoadMore(value);
    let oldPageSize = pageSize;
    let newPageSize = oldPageSize + 10;
    await setPageSize(newPageSize);
    await fetchMerchantByServiceType();
  };

  const fetchMerchantByServiceType = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_BY_SERVICE_TYPE,
          variables: {
            serviceType: TAB_TYPE,
            itemDisplayed: parseInt(pageSize, 10),
            pageNumber: parseInt(pageNumber, 10),
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Bridal Response: ', response);
          const {data, errors} = response;
          const {getMerchantsByServiceType} = data;
          const {data: list, error, totalData} = getMerchantsByServiceType;

          if (errors === undefined) {
            if (error === null) {
              await setTotalCount(totalData);
              await setMerchantList([...list]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsPullOnRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setIsError(true);
              await setIsLoading(false);
              await setIsPullOnRefresh(false);
              await setIsLoadMore(false);
            }
          } else {
            await setIsError(true);
            await setIsLoading(false);
            await setIsPullOnRefresh(false);
            await setIsLoadMore(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsPullOnRefresh(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsPullOnRefresh(false);
      setIsLoadMore(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <MerchantShimmerComponent />
      </Container>
    );
  } else if (!isLoading && !isError) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <View style={{flex: 1}}>
          <ListMerchant
            merchantList={merchantList}
            refreshing={isPullOnRefresh}
            onRefresh={() => pullToRefresh()}
            settingIsLoadMore={e => settingIsLoadMore(e)}
            isLoadMore={isLoadMore}
            pageSize={pageSize}
            totalCount={totalCount}
            navigation={navigation}
          />
        </View>
      </Container>
    );
  } else {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <ErrorScreen
          message="Refresh"
          onPress={async () => {
            await setIsLoading(true);
            await setIsError(true);
            await fetchMerchantByServiceType();
          }}
        />
      </Container>
    );
  }
};

const Wrapper = compose(withApollo)(Cafe);

export default props => <Wrapper {...props} />;
