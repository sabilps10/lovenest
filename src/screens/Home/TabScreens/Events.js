import React, {useState, useEffect, useMemo, useCallback} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import CardEvent from '../../../components/Event/Card/BigCardEvent';
import Colors from '../../../utils/Themes/Colors';
import {Container, Card, CardItem} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';

// Slider Featured Event
import Slider from '../../../components/Event/Slider/FeaturedEventSlider';

// Shimmer
import FeaturedEventShimmer from '../../../components/Loader/Event/FeaturedShimmer';
import EvenLisShimmer from '../../../components/Loader/Event/EventListShimmer';

//Query
import GET_ALL_FEATURED_EVENT from '../Helpers/GetFeaturedEvent';
import GET_UP_Coming_EVENT_HELPER from '../Helpers/GetUpComingEvent';

// Empty Event
import EmptyEvent from '../../../components/Cards/Event/EmptyEvent';

// Error Screen 
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';

const {newContainerColor, black, transparent} = Colors;
const {medium} = FontType;

export const ListBigCardEvent = props =>
  useMemo(() => {
    const {upComingEvent, navigation, client} = props;
    if (upComingEvent) {
      return (
        <>
          {upComingEvent.length === 0 ? null : (
            <Card transparent style={{marginTop: 0, marginBottom: 0}}>
              <CardItem
                style={{backgroundColor: transparent, paddingBottom: 0}}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(2.4),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Upcoming Events
                </Text>
              </CardItem>
            </Card>
          )}
          <FlatList
            ListFooterComponent={() => {
              if (upComingEvent.length > 3) {
                return (
                  <Card transparent>
                    <CardItem
                      button
                      onPress={() => {
                        navigation.navigate('UpcomingEvent');
                      }}
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: transparent,
                      }}>
                      <Text
                        style={{
                          fontFamily: medium,
                          color: black,
                          letterSpacing: 0.3,
                          fontSize: RFPercentage(1.8),
                        }}>
                        View More
                      </Text>
                    </CardItem>
                  </Card>
                );
              } else {
                return null;
              }
            }}
            contentContainerStyle={{
              paddingTop: 16,
              paddingBottom: 16,
              paddingLeft: 15,
              paddingRight: 15,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            data={upComingEvent}
            keyExtractor={(_, index) => String(index)}
            renderItem={({item}) => {
              return (
                <CardEvent
                  data={item}
                  navigation={navigation}
                  client={client}
                />
              );
            }}
          />
        </>
      );
    } else {
      return null;
    }
  }, []);

const Events = props => {
  console.log('Events Props: ', props);
  const {navigation, client} = props;
  const shimmerSize = [1, 2, 3, 4, 5];

  const [isLoadingUpComingEvent, setIsLoadingUpComingEvent] = useState(true);
  const [isErrorUpComingEvent, setIsErrorUpComingEvent] = useState(false);
  const [upComingEvent, setUpComingEvent] = useState([]);

  const [isLoadingFeaturedEvent, setIsLoadingFeaturedEvent] = useState(true);
  const [isErrorFeaturedEvent, setIsErrorFeaturedEvent] = useState(false);
  const [featuredEvent, setFeaturedEvent] = useState([]);

  const [refetch, setIsRefretch] = useState(false);

  useEffect(() => {
    fetchFeaturedEvent();
    fetchUpComingEvent();
    const subscriberFocus = navigation.addListener('focus', () => {
      fetchFeaturedEvent();
      fetchUpComingEvent();
    });

    const subscriberTabPress = navigation.addListener('tabPress', () => {
      // fetchFeaturedEvent();
      // fetchUpComingEvent();
      onRefresh();
    });

    return () => {
      subscriberFocus();
      subscriberTabPress();
    };
  }, [navigation]);

  const onRefresh = useCallback(async () => {
    await setIsRefretch(true);
    await fetchFeaturedEvent();
    await fetchUpComingEvent();
    await setIsRefretch(false);
  }, []);

  const fetchFeaturedEvent = async () => {
    try {
      const getFeaturedEvent = await GET_ALL_FEATURED_EVENT(client);
      console.log('getFeaturedEvent: ', getFeaturedEvent);
      const {error, data} = getFeaturedEvent;
      if (error === null) {
        await setFeaturedEvent([...data]);
        await setIsErrorFeaturedEvent(false);
        await setIsLoadingFeaturedEvent(false);
      } else {
        await setIsErrorFeaturedEvent(true);
        await setIsLoadingFeaturedEvent(false);
      }
    } catch (error) {
      setIsErrorFeaturedEvent(true);
      setIsLoadingFeaturedEvent(false);
    }
  };

  const fetchUpComingEvent = async () => {
    try {
      const getUpComingEvent = await GET_UP_Coming_EVENT_HELPER(client);
      console.log('getUpComingEvent: ', getUpComingEvent);
      const {error, data} = getUpComingEvent;
      if (error === null) {
        await setUpComingEvent([...data]);
        await setIsErrorUpComingEvent(false);
        await setIsLoadingUpComingEvent(false);
      } else {
        await setIsErrorUpComingEvent(true);
        await setIsLoadingUpComingEvent(false);
      }
    } catch (error) {
      setIsErrorUpComingEvent(true);
      setIsLoadingUpComingEvent(false);
    }
  };

  if (
    featuredEvent.length === 0 &&
    upComingEvent.length === 0 &&
    !isLoadingFeaturedEvent &&
    !isLoadingFeaturedEvent &&
    !isErrorFeaturedEvent &&
    !isErrorUpComingEvent
  ) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <EmptyEvent />
        </View>
      </Container>
    );
  }

  if (
    featuredEvent.length === 0 &&
    upComingEvent.length === 0 &&
    !isLoadingFeaturedEvent &&
    !isLoadingFeaturedEvent &&
    isErrorFeaturedEvent &&
    isErrorUpComingEvent
  ) {
    return (
      <Container>
        <ErrorScreen
          message="Refresh"
          onPress={() => {
            onRefresh();
          }}
        />
      </Container>
    );
  }

  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refetch} onRefresh={() => onRefresh()} />
        }
        scrollEnabled={isLoadingUpComingEvent ? false : true}>
        {isErrorFeaturedEvent ? null : isLoadingFeaturedEvent ? (
          <FeaturedEventShimmer />
        ) : (
          <Slider
            navigation={navigation}
            showIndicator={true}
            containerStyle={{
              shadowOpacity: 0,
              elevation: 0,
              width: Dimensions.get('window').width,
              height: 188,
            }}
            imageStyle={{
              width: Dimensions.get('window').width,
              height: 188,
              aspectRatio: Dimensions.get('window').width / 188,
            }}
            imageLoaderStyle={{
              width: Dimensions.get('window').width / 7,
              height: Dimensions.get('window').width / 7,
            }}
            data={featuredEvent}
            timer={3000}
          />
        )}
        {isErrorUpComingEvent ? null : isLoadingUpComingEvent ? (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 15,
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            {shimmerSize.map((data, i) => {
              return <EvenLisShimmer key={`${String(i)} Shimmer`} />;
            })}
          </View>
        ) : (
          <ListBigCardEvent
            upComingEvent={upComingEvent}
            // upComingEvent={[]}
            navigation={navigation}
            client={client}
          />
        )}
      </ScrollView>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Events);

export default props => <Wrapper {...props} />;
