/* eslint-disable curly */
import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  Text,
  Image,
  Dimensions,
  Animated,
  Easing,
  Platform,
  PermissionsAndroid,
  Alert,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import AsyncImage from '../../components/Image/AsyncImage';
import RNBackgroundDownloader from 'react-native-background-downloader';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import CameraRoll from '@react-native-community/cameraroll';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonBack from '../../components/Button/buttonBack';
import {QRCode} from '../../utils/Themes/Images';
import {Container, Card, CardItem, Icon, Button} from 'native-base';
import {ActivityIndicator} from 'react-native';
import {ProgressBar as AndroidProgressBar} from '@react-native-community/progress-bar-android';

const {
  black,
  headerBorderBottom,
  greyLine,
  white,
  transparent,
  mainRed,
  overlayDim,
  mainGreen,
} = Colors;
const {medium, book, handStyle, handStyle1} = FontType;
const {regular} = FontSize;
const {QRBG} = QRCode;
const {width, height} = Dimensions.get('window');

const {width: DEVICE_WIDTH} = Dimensions.get('window');

const DownloadScreen = props => {
  console.log('Props >>>> ', props);
  const {route, navigation} = props;
  const {params} = route;
  const {selectedPhotos, profile} = params;

  const [loading, setLoading] = useState(true);
  const [files, setFiles] = useState([]);

  const fetchData = async () => {
    await setFiles([...selectedPhotos]);
    await setLoading(false);
  };
  useEffect(() => {
    if (Platform.OS === 'android') {
      hasAndroidPermission();
    }
    navigationOptions();
    fetchData();
  }, []);

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Download',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const hasAndroidPermission = async () => {
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    }

    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
  };

  const keyExt = useCallback(
    item => {
      return `${item.id}`;
    },
    [files],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return <ItemCard item={item} index={index} />;
    },
    [files],
  );

  const listHeaderComponent = useCallback(() => {
    return (
      <>
        <HeaderWithImage {...props} profile={profile ? profile : null} />
        <CardText
          text={
            selectedPhotos.length > 0
              ? `Total photo selected ${selectedPhotos.length}`
              : null
          }
          {...props}
        />
      </>
    );
  }, []);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: white}}>
      <View style={{flex: 1, zIndex: -99}}>
        <FlatList
          ListHeaderComponent={listHeaderComponent}
          removeClippedSubviews
          initialNumToRender={6}
          maxToRenderPerBatch={6}
          legacyImplementation
          disableVirtualization
          scrollEnabled={true}
          onRefresh={() => null}
          refreshing={loading}
          contentContainerStyle={styles.contentContainer}
          data={files}
          extraData={files}
          renderItem={renderItem}
          keyExtractor={keyExt}
        />
      </View>
    </SafeAreaView>
  );
};

export const ItemCard = props => {
  console.log('ITEM CARD PROPS: ', props?.item);
  const {item, index} = props;

  const [taskId, setTaskId] = useState(null);
  const [totalFileSize, setTotalFileSize] = useState(0);
  const [status, setStatus] = useState('standby');
  const [percent, setPercent] = useState(0);

  const dirs = RNFetchBlob?.fs?.dirs;
  const finalDir = `${
    Platform?.OS === 'ios' ? dirs?.DocumentDir : dirs?.DownloadDir
  }/${item?.filename}`;

  const config =
    Platform?.OS === 'ios'
      ? {
          IOSBackgroundTask: true,
          indicator: true,
          overwrite: true,
          path: finalDir,
        }
      : {
          path: `${dirs?.DownloadDir}/${item?.filename}`,
          addAndroidDownloads: {
            path: `${dirs?.DownloadDir}/${item?.filename}`,
            useDownloadManager: true, // <-- this is the only thing required
            // Optional, override notification setting (default to true)
            notification: true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be text/plain
            description: `Download ${item?.filename}`,
          },
        };

  const [task, setTask] = useState(null);

  useEffect(() => {
    if (status === 'begin' && task) {
      progressPercentage();
      startDownload();
    }

    if (status === 'pause') {
      pauseDownload();
    }

    if (status === 'cancel') {
      stopDownload();
    }
  }, [status]);

  const progressPercentage = useCallback(() => {
    try {
      setTimeout(() => {
        task?.progress({interval: 1000}, (received, total) => {
          console.log('progress', {received, total});
          setPercent((received / total) * 100);

          // Calculation file size
          var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
          if (total === 0) return 'n/a';
          var i = parseInt(Math.floor(Math.log(total) / Math.log(1024)), 10);
          if (i === 0) return total + ' ' + sizes[i];
          setTotalFileSize(
            (total / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i],
          );
        });
      }, 0);
    } catch (error) {
      console.log('Error progressPercentage: ', error);
    }
  }, [status]);

  const stopDownload = useCallback(async () => {
    try {
      await task.cancel(err => {
        console.log('stopDownload: ', err);
        setPercent(0);
      });
    } catch (error) {
      console.log('Error Pause Download: ', error);
    }
  }, [status]);

  const pauseDownload = useCallback(async () => {
    try {
      await task.cancel(err => {
        console.log('pauseDownload: ', err);
      });
    } catch (error) {
      console.log('Error Pause Download: ', error);
    }
  }, [status]);

  const startDownload = useCallback(async () => {
    try {
      await task
        ?.then(async res => {
          console.log('Res Begin: ', res);
          setTaskId(res?.taskId);

          if (Platform?.OS === 'ios') {
            CameraRoll.save(
              Platform?.OS === 'ios' ? item?.url : `${res?.data}`,
              {
                type: 'photo',
                album: 'Love Nest',
              },
            )
              .then(async resx => {
                console.log('Resss: ', resx);
                await setPercent(100.0);
                await setStatus('stop');
              })
              .catch(error => {
                console.log('Camera Roll Error: ', error);
                throw new Error(error.message);
              });
          } else {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'Love Nest Camera Permission',
                message:
                  'Love Nest needs access to your Gallery to save the Image Download ',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
              },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('Android Access Granted and Saving: ', {
                finalDir,
                result: res?.data,
              });
              const internalStorage = res?.data;
              const externalStorage = dirs?.DCIMDir + `/${item?.filename}`;
              console.log('Storage: ', {internalStorage, externalStorage});

              // move file into DCIM folder
              await RNFetchBlob.fs
                .mv(internalStorage, externalStorage)
                .then(async result => {
                  console.log('File has been saved to:' + result);
                  await RNFetchBlob?.fs
                    ?.stat(externalStorage)
                    .then(async statResult => {
                      console.log('statResult: ', statResult);
                      var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                      if (statResult?.size === 0) return 'n/a';
                      var i = parseInt(
                        Math.floor(Math.log(statResult?.size) / Math.log(1024)),
                        10,
                      );
                      if (i === 0) return statResult?.size + ' ' + sizes[i];
                      await setTotalFileSize(
                        (statResult?.size / Math.pow(1024, i)).toFixed(1) +
                          ' ' +
                          sizes[i],
                      );
                      await setPercent(100.0);
                      await setStatus('stop');
                    })
                    .catch(error => {
                      throw error;
                    });
                })
                .catch(error => {
                  throw error;
                });
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error Start Download: ', error);
      await setPercent(0);
      await setStatus('stop');
    }
  }, [status]);

  const begin = async () => {
    try {
      if (Platform?.OS === 'android') {
        const isGranted = await androidAskPermission();
        if (isGranted) {
          await setTask(
            RNFetchBlob?.config({
              ...config,
            })?.fetch('GET', `${item.url}`),
          );
          setStatus('begin');
        }
      } else {
        await setTask(
          RNFetchBlob?.config({
            ...config,
          })?.fetch('GET', `${item.url}`),
        );
        setStatus('begin');
      }
    } catch (error) {
      console.log('Error Begin Download: ', error);
      stop();
    }
  };

  const pause = () => {
    try {
      setStatus('pause');
    } catch (error) {
      console.log('Error Pause: ', error);
    }
  };

  const stop = () => {
    try {
      setStatus('cancel');
    } catch (error) {
      console.log('Error stop: ', error);
    }
  };

  const androidAskPermission = async () => {
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    }

    const granted = await PermissionsAndroid.request(permission);
    return granted === 'granted';
  };

  return (
    <View style={{paddingLeft: 10, paddingRight: 10, marginBottom: 10}}>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          padding: 5,
          borderWidth: 1,
          borderColor: greyLine,
          borderRadius: 4,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          backgroundColor: white,
          elevation: 5,
        }}>
        <View style={{flex: 0.2}}>
          <AsyncImage
            source={{uri: `${item?.dynamicUrl}=h100-pp`}}
            style={{
              flex: 1,
              width: '100%',
              height: width * 0.12,
              borderRadius: 4,
            }}
            resizeMode="cover"
          />
        </View>
        <View style={{flex: 1, borderWidth: 0, flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              paddingLeft: 10,
              paddingRight: 10,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text
              numberOfLines={status === 'stop' ? 2 : 1}
              style={{
                marginBottom: status === 'stop' ? 0 : 10,
                fontFamily: medium,
                color: black,
                letterSpacing: 0.3,
                fontSize: RFPercentage(1.6),
              }}>
              {item?.filename}
            </Text>
            {status === 'stop' ? null : (
              <>
                {Platform?.OS === 'ios' ? (
                  <ProgressBar
                    status={status}
                    progress={percent}
                    backgroundColor={greyLine}
                  />
                ) : (
                  <View style={{width: '100%'}}>
                    <AndroidProgressBar
                      color={mainRed}
                      styleAttr="Horizontal"
                      indeterminate={false}
                      progress={percent}
                    />
                  </View>
                )}
              </>
            )}
          </View>
          <View style={{flex: 0.8, borderWidth: 0, flexDirection: 'row'}}>
            {status === 'stop' ? (
              <View
                style={{
                  alignSelf: 'center',
                  backgroundColor: mainGreen,
                  borderWidth: 1,
                  borderColor: mainGreen,
                  borderRadius: 4,
                  padding: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginHorizontal: 10,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.2),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  Done
                </Text>
              </View>
            ) : (
              <>
                <TouchableOpacity
                  onPress={() => {
                    if (status === 'begin') {
                      pause();
                    } else if (status === 'standby') {
                      begin();
                    } else if (status === 'pause') {
                      begin();
                    } else if (status === 'cancel') {
                      begin();
                    }
                  }}
                  style={{
                    padding: 10,
                    borderWidth: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name={status === 'begin' ? 'pause-circle' : 'play-circle'}
                    style={{fontSize: RFPercentage(2.5), color: black}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    stop();
                  }}
                  style={{
                    padding: 10,
                    borderWidth: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="x-circle"
                    style={{
                      right: 5,
                      fontSize: RFPercentage(2.5),
                      color: black,
                    }}
                  />
                </TouchableOpacity>
              </>
            )}
            <View
              style={{
                flex: 1,
                borderLeftWidth: 1,
                borderColor: greyLine,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 5,
              }}>
              <Text
                style={{
                  fontSize: RFPercentage(1.1),
                  fontStyle: 'italic',
                  color: black,
                  textAlign: 'center',
                }}>
                {totalFileSize === 0 ? 'N/A' : totalFileSize}
                {status === 'begin' ? ` / ${percent?.toFixed(1)}%` : ''}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export const CardText = props => {
  const {text} = props;
  return (
    <Card transparent>
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        <Text
          style={{
            top: 5,
            textAlign: 'center',
            fontFamily: book,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {text}
        </Text>
      </CardItem>
    </Card>
  );
};

export const HeaderWithImage = props => {
  const {profile} = props;
  const {name, partnerName} = profile;
  return (
    <View style={{width, height: height / 4.5, backgroundColor: greyLine}}>
      <Image
        source={QRBG}
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          flex: 1,
          height: '100%',
          width: '100%',
          zIndex: 1,
        }}
        resizeMode="cover"
      />
      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
          top: 20,
          backgroundColor: 'transparent',
          zIndex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{justifyContent: 'flex-start', alignItems: 'flex-end'}}>
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 35,
              marginBottom: 5,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 15,
              borderRadius: 4,
            }}
          />
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 10,
            paddingTop: 6,
            paddingBottom: 6,
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: white,
              fontSize: RFPercentage(1.7),
            }}>
            The Wedding Of
          </Text>
        </View>
        <View style={{justifyContent: 'flex-end', alignItems: 'flex-start'}}>
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 35,
              marginBottom: 5,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 15,
              borderRadius: 4,
            }}
          />
        </View>
      </View>
      <View
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 4,
          backgroundColor: transparent,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 15,
          paddingRight: 15,
          paddingTop: 60,
        }}>
        <Text
          style={{
            fontFamily: handStyle,
            fontSize: RFPercentage(8),
            color: white,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {profile ? (name ? `${name.split(' ')[0]}` : 'Unknown') : 'Unknown'}
        </Text>
        <Text
          style={{
            fontFamily: handStyle1,
            fontSize: RFPercentage(4.5),
            color: white,
            textAlign: 'center',
            marginHorizontal: 10,
            letterSpacing: 0.3,
          }}>
          &
        </Text>
        <Text
          style={{
            fontFamily: handStyle,
            fontSize: RFPercentage(8),
            color: white,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {profile
            ? partnerName
              ? `${partnerName.split(' ')[0]}`
              : 'Unknown'
            : 'Unknown'}
        </Text>
      </View>
    </View>
  );
};

export const ProgressBar = ({progress, status, backgroundColor = '#fff'}) => {
  console.log('MASUK PROGRESS BAR: ', progress);
  const animation = useRef(new Animated.Value(0));

  useEffect(() => {
    Animated.timing(animation.current, {
      toValue: progress,
      duration: 250,
      useNativeDriver: false,
      easing: Easing.quad,
    }).start();
  }, [progress]);

  const widths = animation.current.interpolate({
    inputRange: [0, 100],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });
  return (
    <View style={[progressStyle.progressBar, {backgroundColor}]}>
      <Animated.View
        style={
          ([StyleSheet.absoluteFill],
          {
            backgroundColor: status === 'pause' ? 'grey' : mainRed,
            width: widths,
            borderRadius: 4,
          })
        }
      />
    </View>
  );
};

const progressStyle = StyleSheet.create({
  progressBar: {
    flexDirection: 'row',
    height: 5,
    borderRadius: 4,
    width: '100%',
  },
});

const styles = StyleSheet.create({
  modalTitle: {
    fontSize: 20,
    marginBottom: 16,
    fontWeight: 'bold',
  },
  modalSubTitle: {
    color: '#333',
    fontSize: 13,
    marginBottom: 16,
  },
  modalHint: {
    color: '#333',
    fontSize: 13,
    marginBottom: 8,
    textAlign: 'center',
  },

  modalWrapper: {
    paddingHorizontal: 16 * 2,
    paddingVertical: 20,
    width: DEVICE_WIDTH - 2 * 16,
    backgroundColor: '#fff',
    borderRadius: 4,
    elevation: 2,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1,
    shadowRadius: 4,
  },
  modalContainer: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
  },
  headerWrapper: {
    backgroundColor: '#eee',
    paddingTop: 52,
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: '#009688',
    fontSize: 20,
  },
  time: {
    fontSize: 14,
    color: '#707070',
  },
  itemTextWrapper: {
    marginRight: 16,
    width: 0,
    flexGrow: 1,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  itemTitle: {
    fontSize: RFPercentage(1.7),
    textAlign: 'left',
    color: '#333',
    lineHeight: 25,
  },
  itemTitleWrapper: {width: 0, flexGrow: 1},
  downloadIcon: {
    marginRight: 'auto',
  },
  contentContainer: {
    backgroundColor: 'white',
    paddingBottom: 25,
  },
  itemWrapper: {
    borderWidth: 1,
    borderColor: greyLine,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingHorizontal: 12,
    paddingVertical: 0,
    borderRadius: 4,
    marginTop: 8,
    marginHorizontal: 8,
  },
  itemImage: {
    height: 50,
    width: 75,
    borderRadius: 5,
  },
});

export default DownloadScreen;
