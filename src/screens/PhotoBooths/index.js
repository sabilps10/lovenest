/* eslint-disable no-unused-vars */
import React, {useEffect, useState, useRef, useMemo, useCallback} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
  PermissionsAndroid,
  RefreshControl,
  ActivityIndicator,
  Animated,
  SafeAreaView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonBack from '../../components/Button/buttonBack';
import {QRCode, charImage} from '../../utils/Themes/Images';
import {
  Container,
  Card,
  CardItem,
  Content,
  Icon,
  Footer,
  FooterTab,
  Button,
} from 'native-base';
import Loader from '../../components/Loader/JustLoader';
import RNFetchBlob from 'rn-fetch-blob';
import CameraRoll from '@react-native-community/cameraroll';
import ImageViewer from './ImagePreview';
import AsyncImage from '../../components/Image/AsyncImage';
import {connect} from 'react-redux';
import _ from 'lodash';

// Query
import GetListPhotoQuery from '../../graphql/queries/mobileFolderImage';
import {Platform} from 'react-native';

const {
  black,
  greyLine,
  white,
  transparent,
  mainGreen,
  overlayDim,
  darkGrey,
  mainRed,
  headerBorderBottom,
} = Colors;
const {medium, book, handStyle, handStyle1} = FontType;
const {regular} = FontSize;
const {QRBG} = QRCode;
const {charTheWeddingOf} = charImage;
const {width, height} = Dimensions.get('window');
const defaultImage =
  'https://erekrut.panaceaebizz.com/wp-content/uploads/2020/05/blank-profile-picture-973460_1280.png';

const PhotoBooths = props => {
  console.log('PhotoBooths Props: ', props);
  const {client, navigation, route, positionYBottomNav} = props;
  const {params} = route;
  const {hashId, profile} = params;

  const defaultText =
    'Select photo you want to download and hold photo if you want to see full size';

  const [list, setList] = useState([]);
  let postArray = useRef(list).current;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [itemDisplayed, setItemDisplayed] = useState(12);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);
  const [selectedPhotos, setSelectedPhotos] = useState([]);
  const [isDownloading, setIsDownloading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [open, setOpen] = useState(false);
  const [indexPrev, setIndexPrev] = useState(0);

  useEffect(() => {
    navigationOptions();
    fetch();
    onChangeOpacity(false);

    if (isLoadMore) {
      onLoadMore();
    }

    if (refreshing) {
      onRefresh();
    }
  }, [isLoadMore, refreshing]);

  const onRefresh = useCallback(async () => {
    try {
      await setPageNumber(1);
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  }, [refreshing]);

  const onLoadMore = useCallback(async () => {
    try {
      await setPageNumber(pageNumber + 1);
      await fetch();
    } catch (error) {
      await setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const fetch = () => {
    try {
      if (hashId) {
        const variables = {
          hashId,
          itemDisplayed,
          pageNumber,
        };
        console.log('variables >>>> ', variables);
        client
          .query({
            query: GetListPhotoQuery,
            variables,
            ssr: false,
            fetchPolicy: 'no-cache',
          })
          .then(async response => {
            console.log('Response: QR RESULT ', response);
            const {data, errors} = response;
            const {mobileFolderImages} = data;
            const {data: photos, error, totalCount} = mobileFolderImages;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              } else {
                await setTotalData(totalCount);
                let oldData = list?.concat(photos);
                const removeDuplicatedData = await _.uniqBy(oldData, 'id');
                console.log('removeDuplicatedData: ', removeDuplicatedData);

                await setList([...removeDuplicatedData]);
                await setIsError(false);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              }
            }
          })
          .catch(async error => {
            console.log('Error: ', error);
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          });
      } else {
        // no has id
        setIsError(true);
        setIsLoading(false);
        setRefreshing(false);
        setIsLoadMore(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
      setIsLoadMore(false);
    }
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'QR Photo Download',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const download = async () => {
    try {
      console.log('Download di mulai', selectedPhotos);
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          ],
          {
            title: 'Photo Gallery',
            message:
              'Love Nest App needs access to your gallery to save the photo ',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        console.log('granted >>>>> ', granted);
        if (granted) {
          const filterSelectedOnly = await Promise.all(
            list
              ?.map((item, index) => {
                if (item?.selected) {
                  return {
                    ...item,
                  };
                } else {
                  return null;
                }
              })
              .filter(Boolean),
          );

          if (filterSelectedOnly) {
            navigation.navigate('DownloadScreen', {
              selectedPhotos: filterSelectedOnly,
              profile,
            });
          }
        }
      } else {
        const filterSelectedOnly = await Promise.all(
          list
            ?.map((item, index) => {
              if (item?.selected) {
                return {
                  ...item,
                };
              } else {
                return null;
              }
            })
            .filter(Boolean),
        );

        if (filterSelectedOnly) {
          navigation.navigate('DownloadScreen', {
            selectedPhotos: filterSelectedOnly,
            profile,
          });
        }
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              width: width / 4,
              height: height / 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Loader />
          </View>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Error</Text>
        </View>
      </Container>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1}}>
          {/* Image Viewer */}
          <ImageViewer
            index={indexPrev}
            images={list}
            visible={open}
            onPress={async () => {
              if (open) {
                await setIndexPrev(0);
                await setOpen(false);
              } else {
                await setOpen(true);
              }
            }}
          />
          {/* List photo */}
          <ListPhoto
            {...props}
            defaultText={defaultText}
            selectedPhotos={() => {
              return _.countBy(list, obj => obj.selected).true;
            }}
            profile={profile}
            list={list}
            total={totalData}
            isLoadMore={isLoadMore}
            setIsLoadMore={async status => await setIsLoadMore(status)}
            refreshing={refreshing}
            onRefresh={() => setRefreshing(true)}
            onOpenViewer={async index => {
              try {
                await setIndexPrev(index);
                await setOpen(!open);
              } catch (error) {
                console.log('Error onOpenViewer: ', error);
              }
            }}
            onSelect={async index => {
              try {
                let newArray = list;
                newArray[index].selected = !list[index].selected;
                await setList([...newArray]);
              } catch (error) {
                console.log('Error onSelect: ', error);
              }
            }}
          />
          <ButtonDownload
            goToDownload={download}
            selectedPhotos={() => _.countBy(list, obj => obj.selected).true}
          />
        </View>
      </SafeAreaView>
    );
  }
};

export const ButtonDownload = props => {
  return (
    <TouchableOpacity
      disabled={props?.selectedPhotos() > 0 ? false : true}
      onPress={() => props?.goToDownload()}
      style={{
        backgroundColor:
          props?.selectedPhotos() > 0 ? mainGreen : headerBorderBottom,
        position: 'absolute',
        bottom: 0,
        borderWidth: 0,
        padding: 15,
        paddingTop: 18,
        paddingBottom: 18,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: props?.selectedPhotos() > 0 ? white : greyLine,
        }}>
        Download
      </Text>
    </TouchableOpacity>
  );
};

export const ListPhoto = props => {
  console.log('ListPhoto props: ', props);
  const {
    profile,
    selectedPhotos,
    list,
    total,
    isLoadMore,
    setIsLoadMore,
    refreshing,
    onRefresh,
    onOpenViewer,
    onSelect,
  } = props;

  const keyExtractor = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <CardPhoto
          onSelect={() => onSelect(index)}
          onOpenViewer={() => onOpenViewer(index)}
          item={item}
          index={index}
        />
      );
    },
    [list],
  );

  return (
    <FlatList
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      ListHeaderComponent={() => {
        return (
          <>
            {/* Header Cover */}
            <HeaderWithImage {...props} profile={profile ? profile : null} />
            {/* Card Header Text */}
            <CardText
              text={
                selectedPhotos() > 0
                  ? `Total photo selected ${selectedPhotos()}`
                  : props?.defaultText
              }
              {...props}
            />
          </>
        );
      }}
      onEndReached={() => {
        if (list?.length < total) {
          console.log('Masuk True isLoadMore');
          setIsLoadMore(true);
        } else {
          console.log('Masuk False isLoadMore');
          setIsLoadMore(false);
        }
      }}
      onEndReachedThreshold={0.05}
      contentContainerStyle={{
        paddingBottom: 60,
      }}
      numColumns={3}
      initialNumToRender={12}
      legacyImplementation
      data={list}
      extraData={list}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      ListFooterComponent={() => {
        if (isLoadMore) {
          return <FooterList />;
        } else {
          return null;
        }
      }}
    />
  );
};

export const FooterList = props => {
  return (
    <View
      style={{
        width: '100%',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{color: greyLine, fontStyle: 'italic'}}>Load more...</Text>
    </View>
  );
};

export const CardPhoto = props => {
  const {item, index, onOpenViewer, onSelect} = props;

  return (
    <TouchableOpacity
      onPress={onSelect}
      onLongPress={onOpenViewer}
      style={{flex: 1 / 3, height: width * 0.5, borderWidth: 0, padding: 5}}>
      <View style={{flex: 1, backgroundColor: headerBorderBottom}}>
        <AsyncImage
          source={item?.dynamicUrl ? {uri: item?.dynamicUrl} : {uri: item?.url}}
          style={{position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}}
        />
        {item?.selected ? (
          <View
            style={{
              position: 'absolute',
              zIndex: 99,
              backgroundColor: overlayDim,
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              type="Feather"
              name="check"
              style={{fontSize: RFPercentage(2.2), color: white}}
            />
          </View>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

export const CardText = props => {
  const {text} = props;
  return (
    <Card transparent>
      <CardItem
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: book,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
            lineHeight: 25,
          }}>
          {text}
        </Text>
      </CardItem>
    </Card>
  );
};

export const HeaderWithImage = props => {
  console.log('Header Ma Bruh: ', props);
  const {profile} = props;
  const {name, profileImage, partnerName} = profile;

  return (
    <View style={{width, height: height / 4.5, backgroundColor: greyLine}}>
      <Image
        source={QRBG}
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          flex: 1,
          height: '100%',
          width: '100%',
          zIndex: 1,
        }}
        resizeMode="cover"
      />
      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
          top: 20,
          backgroundColor: 'transparent',
          zIndex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{justifyContent: 'flex-start', alignItems: 'flex-end'}}>
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 35,
              marginBottom: 5,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 15,
              borderRadius: 4,
            }}
          />
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 10,
            paddingTop: 6,
            paddingBottom: 6,
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: white,
              fontSize: RFPercentage(1.7),
            }}>
            The Wedding Of
          </Text>
        </View>
        <View style={{justifyContent: 'flex-end', alignItems: 'flex-start'}}>
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 35,
              marginBottom: 5,
              borderRadius: 4,
            }}
          />
          <View
            style={{
              height: 2,
              backgroundColor: 'white',
              width: 15,
              borderRadius: 4,
            }}
          />
        </View>
      </View>
      <View
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 4,
          backgroundColor: transparent,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 15,
          paddingRight: 15,
          paddingTop: 60,
        }}>
        <Text
          style={{
            fontFamily: handStyle,
            fontSize: RFPercentage(8),
            color: white,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {profile ? (name ? `${name.split(' ')[0]}` : 'Unknown') : 'Unknown'}
        </Text>
        <Text
          style={{
            fontFamily: handStyle1,
            fontSize: RFPercentage(4.5),
            color: white,
            textAlign: 'center',
            marginHorizontal: 10,
            letterSpacing: 0.3,
          }}>
          &
        </Text>
        <Text
          style={{
            fontFamily: handStyle,
            fontSize: RFPercentage(8),
            color: white,
            textAlign: 'center',
            letterSpacing: 0.3,
          }}>
          {profile
            ? partnerName
              ? `${partnerName.split(' ')[0]}`
              : 'Unknown'
            : 'Unknown'}
        </Text>
      </View>
    </View>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(PhotoBooths);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
