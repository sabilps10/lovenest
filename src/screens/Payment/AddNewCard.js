import React, {useState, useEffect, useRef} from 'react';
import {Text, View, StatusBar, TextInput, Switch} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonBack from '../../components/Button/buttonBack';
import valid from 'card-validator';
import CardIcons from '../../components/CreditCards/Icons';
import CreditCardView from '../../components/CreditCards/CardView';
import CardTypeImage from '../../components/CreditCards/CardTypeImage';
import {RFPercentage} from 'react-native-responsive-fontsize';
import CardTitle from '../../components/CreditCards/CardTitle';
import AddPaymentMethodMutation from '../../graphql/mutations/addPaymentMethod';
import Loader from '../../components/Loader/JustLoader';
import {CommonActions} from '@react-navigation/native';
import ModalAddNewCardLoader from '../../components/CreditCards/Modals/ModalAddNewCardLoader';
import moment from 'moment';

// Query
import GET_CARD_TOKEN from '../../graphql/queries/getCardToken';

const {
  americanexpress,
  dinersclub,
  mastercard,
  discover,
  jcb,
  placeholder,
  visa,
  invalidIcon,
} = CardIcons;
const {
  black,
  mainRed: lightSalmon,
  greyLine,
  newContainerColor,
  transparent,
  white,
} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

const AddNewCard = props => {
  const {navigation, client} = props;

  const ccNumberRef = useRef();
  const ccDateRef = useRef();
  const ccYearRef = useRef();
  const cvcRef = useRef();

  const [ccNumber, setCCNumber] = useState('');
  const [ccDate, setCCDate] = useState('');
  const [ccYear, setCCYear] = useState('');
  const [cvc, setCVC] = useState('');
  const [, setCCType] = useState('');
  const [ccTypeImage, setCCTypeImage] = useState(placeholder);
  const [inValidCard, setInvalidCard] = useState(false);
  const [maxLengthCardNumber] = useState(19);
  const [flip, setFlip] = useState(false);
  const [isPrimary, setIsPrimary] = useState(false);

  const [isLoadingAddCC, setIsLoadingAddCC] = useState(false);
  const [isSuccessAddCC, setIsSuccessAddCC] = useState(false);
  const [isFailedAddCC, setIsFailedAddCC] = useState(false);
  const [isSuccessMessage, setIsSuccessMessage] = useState('');
  const [isFailedMessage, setIsFailedMessage] = useState('');

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
  }, [navigation, ccNumber, ccDate, ccYear, cvc, isPrimary, isLoadingAddCC]);

  const registerCC = async () => {
    try {
      console.log('CC NUMBER: ', ccNumber);
      console.log('CC Exp Date: ', ccDate);
      console.log('CC Exp Year: ', ccYear);
      console.log('CC CVC: ', cvc);
      console.log('CC IsPrimary: ', isPrimary);

      await setIsLoadingAddCC(true);
      await setIsFailedAddCC(false);
      await setIsFailedMessage('');
      await setIsSuccessAddCC(false);
      await setIsSuccessMessage('');

      if (ccNumber === '' || ccDate === '' || ccYear === '' || cvc === '') {
        console.log('Please fill the form');
        await setIsFailedAddCC(true);
        await setIsFailedMessage('Check your input!');
        await setIsSuccessAddCC(false);
        await setIsSuccessMessage('');
        setTimeout(async () => {
          await setIsFailedAddCC(false);
          await setIsFailedMessage('');
          await setIsSuccessAddCC(false);
          await setIsSuccessMessage('');
          await setIsLoadingAddCC(false);
        }, 3000);
      } else {
        const params = {
          // mandatory
          number: ccNumber.replace(' ', ''),
          expMonth: parseInt(ccDate, 10),
          expYear: parseInt(ccYear, 10),
          cvc,
        };

        await client
          .query({
            query: GET_CARD_TOKEN,
            variables: {
              ...params,
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('RESPONSE STRIPE: ', response);
            const {data: d, errors} = response;
            const {getCardToken} = d;
            const {data: datas, error} = getCardToken;
            const {id} = datas;

            if (errors) {
              await setIsFailedAddCC(true);
              await setIsFailedMessage('Failed to add your card!');
              await setIsSuccessAddCC(false);
              await setIsSuccessMessage('');

              setTimeout(async () => {
                await setIsFailedAddCC(false);
                await setIsFailedMessage('');
                await setIsSuccessAddCC(false);
                await setIsSuccessMessage('');
                await setIsLoadingAddCC(false);
              }, 3000);
            } else {
              if (error) {
                await setIsFailedAddCC(true);
                await setIsFailedMessage('Failed to add your card!');
                await setIsSuccessAddCC(false);
                await setIsSuccessMessage('');

                setTimeout(async () => {
                  await setIsFailedAddCC(false);
                  await setIsFailedMessage('');
                  await setIsSuccessAddCC(false);
                  await setIsSuccessMessage('');
                  await setIsLoadingAddCC(false);
                }, 3000);
              } else {
                await client
                  .mutate({
                    mutation: AddPaymentMethodMutation,
                    variables: {
                      token: id,
                      isPrimary,
                    },
                  })
                  .then(async responseBEAPI => {
                    console.log('responseBEAPI: ', responseBEAPI);
                    const {data, errors: err1} = responseBEAPI;
                    const {addPaymentMethod} = data;
                    const {error: err} = addPaymentMethod;

                    if (err1) {
                      await setIsFailedAddCC(true);
                      await setIsFailedMessage('Failed to add your card!');
                      await setIsSuccessAddCC(false);
                      await setIsSuccessMessage('');

                      setTimeout(async () => {
                        await setIsFailedAddCC(false);
                        await setIsFailedMessage('');
                        await setIsSuccessAddCC(false);
                        await setIsSuccessMessage('');
                        await setIsLoadingAddCC(false);
                      }, 3000);
                    } else {
                      if (err) {
                        await setIsFailedAddCC(true);
                        await setIsFailedMessage('Failed to add your card!');
                        await setIsSuccessAddCC(false);
                        await setIsSuccessMessage('');

                        setTimeout(async () => {
                          await setIsFailedAddCC(false);
                          await setIsFailedMessage('');
                          await setIsSuccessAddCC(false);
                          await setIsSuccessMessage('');
                          await setIsLoadingAddCC(false);
                        }, 3000);
                      } else {
                        await setIsFailedAddCC(false);
                        await setIsFailedMessage('');
                        await setIsSuccessAddCC(true);
                        await setIsSuccessMessage('Success Add New Card');

                        setTimeout(async () => {
                          await setIsFailedAddCC(false);
                          await setIsFailedMessage('');
                          await setIsSuccessAddCC(false);
                          await setIsSuccessMessage('');
                          await setIsLoadingAddCC(false);
                          if (props?.route?.params?.paymentType === 'Florist') {
                            navigation.pop(1);
                          } else {
                            navigation.dispatch(
                              CommonActions.navigate({
                                name: 'PaymentMethod',
                              }),
                            );
                          }
                        }, 3000);
                      }
                    }
                  })
                  .catch(async errorBEAPI => {
                    console.log('Error BE API: ', errorBEAPI);
                    await setIsFailedAddCC(true);
                    await setIsFailedMessage('Failed to add your card!');
                    await setIsSuccessAddCC(false);
                    await setIsSuccessMessage('');

                    setTimeout(async () => {
                      await setIsFailedAddCC(false);
                      await setIsFailedMessage('');
                      await setIsSuccessAddCC(false);
                      await setIsSuccessMessage('');
                      await setIsLoadingAddCC(false);
                    }, 3000);
                  });
              }
            }
          })
          .catch(async error => {
            console.log('STRIPE ERROR: ', error.message);
            if (error.message === 'Your card was declined') {
              await setIsFailedAddCC(true);
              await setIsFailedMessage(String(error.message));
              await setIsSuccessAddCC(false);
              await setIsSuccessMessage('');

              setTimeout(async () => {
                await setIsFailedAddCC(false);
                await setIsFailedMessage('');
                await setIsSuccessAddCC(false);
                await setIsSuccessMessage('');
                await setIsLoadingAddCC(false);
              }, 3000);
            } else {
              await setIsFailedAddCC(true);
              await setIsFailedMessage('Failed to add your card!');
              await setIsSuccessAddCC(false);
              await setIsSuccessMessage('');

              setTimeout(async () => {
                await setIsFailedAddCC(false);
                await setIsFailedMessage('');
                await setIsSuccessAddCC(false);
                await setIsSuccessMessage('');
                await setIsLoadingAddCC(false);
              }, 3000);
            }
          });
      }
    } catch (error) {
      console.log('registerCC Error: ', error);
      setIsFailedAddCC(true);
      setIsFailedMessage('Failed to add your card!');
      setIsSuccessAddCC(false);
      setIsSuccessMessage('');

      setTimeout(async () => {
        await setIsFailedAddCC(false);
        await setIsFailedMessage('');
        await setIsSuccessAddCC(false);
        await setIsSuccessMessage('');
        await setIsLoadingAddCC(false);
      }, 3000);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Add New Card',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
      headerRight: () => {
        return (
          <Button
            disabled={isLoadingAddCC ? true : false}
            style={{
              width: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            transparent
            onPress={() => registerCC()}>
            {isLoadingAddCC ? (
              <View
                style={{
                  left: 10,
                  width: 35,
                  height: 35,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Loader />
              </View>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: regular,
                  color: lightSalmon,
                }}>
                Add Card
              </Text>
            )}
          </Button>
        );
      },
    });
  };

  const settingCCNumber = value => {
    console.log('CC Number Value: ', value);

    setInvalidCard(false);

    const ccFormatter = value
      .replace(/\s?/g, '')
      .replace(/(\d{4})/g, '$1 ')
      .trim();

    var numberValidation = valid.number(value);

    if (!numberValidation.isPotentiallyValid) {
      console.log('Invalid Card: ', numberValidation.isPotentiallyValid);
      setInvalidCard(true);
      setCCTypeImage(invalidIcon);
    }

    if (numberValidation.card) {
      console.log('cardValidation', numberValidation); // 'visa or another card type'
      if (numberValidation.card.type) {
        if (numberValidation.card.type === 'visa') {
          setCCType('visa');
          setCCTypeImage(visa);
        } else if (numberValidation.card.type === 'mastercard') {
          setCCType('mastercard');
          setCCTypeImage(mastercard);
        } else if (numberValidation.card.type === 'american-express') {
          setCCType('americanexpress');
          setCCTypeImage(americanexpress);
        } else if (numberValidation.card.type === 'discover') {
          setCCType('discover');
          setCCTypeImage(discover);
        } else if (numberValidation.card.type === 'jcb') {
          setCCType('jcb');
          setCCTypeImage(jcb);
        } else if (numberValidation.card.type === 'diners-club') {
          setCCType('dinersclub');
          setCCTypeImage(dinersclub);
        } else {
          setCCType('default');
          setCCTypeImage(placeholder);
        }
      }
    }

    setCCNumber(ccFormatter);
    if (ccFormatter.length === 19) {
      ccDateRef.current.focus();
    }

    if (ccFormatter === '' && value === '') {
      setInvalidCard(false);
      setCCTypeImage(placeholder);
    }
  };

  return (
    <Container style={{backgroundColor: white}}>
      <ModalAddNewCardLoader
        showModal={isLoadingAddCC}
        isSuccess={isSuccessAddCC}
        isFailed={isFailedAddCC}
        isSuccessMsg={isSuccessMessage}
        isFailedMsg={isFailedMessage}
      />
      <Content>
        <Card
          transparent
          style={{marginTop: 15, elevation: 0, shadowOpacity: 0}}>
          <CardTitle />
          <CreditCardView
            cardImageType={ccTypeImage}
            flip={flip}
            ccNumber={ccNumber}
            cvc={cvc}
            expiryDate={ccDate}
            expiryYear={ccYear}
          />
          <CardItem style={{backgroundColor: transparent}}>
            <CardTypeImage ccTypeImage={ccTypeImage} />
            <TextInput
              onFocus={() => {
                setFlip(false);
              }}
              maxLength={maxLengthCardNumber}
              returnKeyType="next"
              onSubmitEditing={() => ccDateRef.current.focus()}
              ref={ccNumberRef}
              style={{
                flex: 0.8,
                padding: 6,
                paddingLeft: 5,
                paddingRight: 5,
                borderBottomColor: inValidCard ? 'red' : greyLine,
                borderBottomWidth: 0.5,
                fontSize: RFPercentage(1.9),
              }}
              value={ccNumber}
              placeholder={'4242 4242 4242 4242'}
              keyboardType="numeric"
              onChangeText={e => {
                settingCCNumber(e);
              }}
            />
            <View
              style={{
                flex: 0.4,
                flexDirection: 'row',
                borderBottomColor: inValidCard ? 'red' : greyLine,
                borderBottomWidth: 0.5,
              }}>
              <TextInput
                onFocus={() => {
                  setFlip(false);
                }}
                maxLength={2}
                ref={ccDateRef}
                style={{
                  padding: 6,
                  paddingLeft: 5,
                  paddingRight: 5,
                  fontSize: RFPercentage(1.9),
                }}
                returnKeyType="next"
                onSubmitEditing={() => ccYearRef.current.focus()}
                value={ccDate}
                placeholder={'MM'}
                keyboardType="number-pad"
                onChangeText={e => {
                  if (e.length === 2) {
                    ccYearRef.current.focus();
                  }

                  setCCDate(e);
                }}
                onKeyPress={e => {
                  if (e.nativeEvent.key === 'Backspace' && ccDate === '') {
                    ccNumberRef.current.focus();
                  }
                }}
              />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color:
                      ccDate.length === 2 && ccYear.length === 2
                        ? black
                        : greyLine,
                    fonstSize: RFPercentage(1.9),
                  }}>
                  {'/'}
                </Text>
              </View>
              <TextInput
                onFocus={() => {
                  setFlip(false);
                }}
                maxLength={2}
                ref={ccYearRef}
                style={{
                  padding: 6,
                  paddingLeft: 5,
                  paddingRight: 5,
                  fontSize: RFPercentage(1.9),
                }}
                returnKeyType="next"
                onSubmitEditing={() => cvcRef.current.focus()}
                value={ccYear}
                placeholder={'YY'}
                keyboardType="number-pad"
                onChangeText={e => {
                  if (e === '') {
                    ccDateRef.current.focus();
                  }
                  if (e.length === 2) {
                    cvcRef.current.focus();
                  }
                  setCCYear(e);
                }}
                onKeyPress={e => {
                  if (e.nativeEvent.key === 'Backspace' && ccYear === '') {
                    ccDateRef.current.focus();
                  }
                }}
              />
            </View>
            <TextInput
              maxLength={3}
              onFocus={() => {
                setFlip(true);
              }}
              onBlur={() => {
                setFlip(false);
              }}
              ref={cvcRef}
              style={{
                fontSize: RFPercentage(1.9),
                flex: 0.2,
                padding: 6,
                paddingLeft: 5,
                paddingRight: 5,
                borderBottomColor: inValidCard ? 'red' : greyLine,
                borderBottomWidth: 0.5,
              }}
              value={cvc}
              placeholder={'123'}
              keyboardType="number-pad"
              onChangeText={e => {
                if (e === '') {
                  ccYearRef.current.focus();
                }
                setCVC(e);
              }}
              onKeyPress={e => {
                if (e.nativeEvent.key === 'Backspace' && cvc === '') {
                  ccYearRef.current.focus();
                }
              }}
            />
          </CardItem>
          <CardItem
            style={{
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: transparent,
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.9),
                color: black,
                letterSpacing: 0.3,
              }}>
              Set as primary payment method
            </Text>
            <Switch
              trackColor={{false: '#767577', true: '#44D363'}}
              thumbColor={isPrimary ? '#ffffff' : '#f4f3f4'}
              ios_backgroundColor="#D3D3D3"
              onValueChange={e => {
                console.log('Swtich: ', e);
                setIsPrimary(e);
              }}
              value={isPrimary}
            />
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(AddNewCard);

export default props => <Wrapper {...props} />;
