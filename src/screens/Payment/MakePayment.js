import React, {useState, useEffect, useLayoutEffect} from 'react';
import {Text, View, TextInput, TouchableOpacity, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import Buttons from '../../components/Button/buttonFixToBottom';
import ErrorToaster from '../../components/Toaster/errorToaster';
import {RFPercentage} from 'react-native-responsive-fontsize';
import CardPayment from '../../components/Cards/Payments/PaymentCard';
import {hasNotch} from 'react-native-device-info';

const {black, greyLine, white, mainRed, mainGreen} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const MakePayment = props => {
  console.log('MakePayment Props: ', props);
  const {navigation, route, positionYBottomNav} = props;
  const {params} = route;
  const {paymentDetail} = params;
  const {order} = paymentDetail;

  const [amount, setAmount] = useState('');
  const [description, setDescription] = useState('');
  const [isError, setIsError] = useState(false);
  const [isErrorMsg, setIsErrorMsg] = useState('');

  useEffect(() => {
    // navigation.dangerouslyGetParent()?.setOptions({ tabBarVisible: false })
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
  }, [navigation, isError]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const popStacking = () => {
    try {
      navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const goToSelectPaymentMethod = async () => {
    await setIsError(false);
    await setIsErrorMsg('');
    if (amount === '' || description === '') {
      // show toaster error
      await setIsError(true);
      await setIsErrorMsg('Please fill the form!');

      setTimeout(async () => {
        await setIsError(false);
        await setIsErrorMsg('');
      }, 3000);
    } else {
      const parseAmount = amount.replace(',', '.');
      console.log('AMOUNT: ', parseFloat(parseAmount));
      if (parseFloat(parseAmount) < 1) {
        await setIsError(true);
        await setIsErrorMsg('Minimum amount should $1');

        setTimeout(async () => {
          await setIsError(false);
          await setIsErrorMsg('');
        }, 3000);
      } else {
        navigation.navigate('SelectPaymentMethod', {
          amount: parseFloat(parseAmount),
          description,
          orderId: parseInt(order.id, 10),
        });
      }
    }
  };

  return (
    <Container style={{backgroundColor: white}}>
      {isError ? (
        <View
          style={{
            zIndex: 99,
            position: 'absolute',
            top: hasNotch() ? 115 : 90,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ErrorToaster text={isErrorMsg} />
        </View>
      ) : null}
      <Headers
        orderNumber={order?.number ? order.number : ''}
        onPress={() => {
          popStacking();
        }}
      />
      <Content contentContainerStyle={{paddingTop: 15}}>
        <CardPayment
          item={{
            order: order,
            merchant: order.merchant,
            totalPaid: order.totalPaid,
            balance: order.balance,
            paymentList: order.paymentList,
          }}
          fullWidth={true}
        />
        <Card transparent style={{marginTop: 0, paddingTop: 0, bottom: 15}}>
          <CardItem
            style={{
              backgroundColor: 'transparent',
              paddingTop: 0,
              paddingBottom: 0,
              marginTop: 0,
            }}
            button
            onPress={() => {
              navigation.navigate('PaymentHistory', {data: order});
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: mainRed,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              View Payment History
            </Text>
            <View
              style={{
                marginHorizontal: 5,
                width: 20,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 30 / 2,
                borderWidth: 1,
                borderColor: mainRed,
              }}>
              <Icon
                type="Feather"
                name="arrow-right"
                style={{left: 9.5, top: 1, fontSize: 15, color: mainRed}}
              />
            </View>
          </CardItem>
        </Card>
        <Card
          transparent
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: 35,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <CardItem
            style={{
              flexDirection: 'column',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                marginBottom: 5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                <Text style={{color: mainRed, fontSize: RFPercentage(1.9)}}>
                  *
                </Text>
                Amount
              </Text>
            </View>
            <TextInput
              returnKeyType="done"
              style={{
                width: '100%',
                minHeight: 30,
                borderBottomWidth: 0.5,
                borderBottomColor: greyLine,
                paddingTop: 5,
                paddingBottom: 5,
              }}
              keyboardType="numeric"
              placeholder={'$500'}
              value={amount}
              onChangeText={e => setAmount(e)}
            />
          </CardItem>
          <CardItem
            style={{
              flexDirection: 'column',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                marginBottom: 5,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.9),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                <Text style={{color: mainRed, fontSize: RFPercentage(1.9)}}>
                  *
                </Text>
                Description
              </Text>
            </View>
            <TextInput
              style={{
                minHeight: 100,
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: greyLine,
                paddingTop: 5,
                paddingBottom: 5,
              }}
              textAlignVertical={'top'}
              multiline={true}
              placeholder={'E.g.: Payment For Father Suit'}
              value={description}
              onChangeText={e => setDescription(e)}
            />
          </CardItem>
        </Card>
      </Content>
      <ButtonSelectPaymentMethod
        {...props}
        onPress={() => goToSelectPaymentMethod()}
      />
    </Container>
  );
};

export const ButtonSelectPaymentMethod = props => {
  const {onPress} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => onPress()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              SELECT PAYMENT METHOD
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const Headers = props => {
  const {orderNumber, onPress} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: 'white',
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.4,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          transparent
          onPress={onPress}
          style={{
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          Make Payment
        </Text>
        <Text
          style={{
            lineHeight: 18,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: '#999999',
            letterSpacing: 0.3,
          }}>
          {orderNumber}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(MakePayment);

export default props => <Wrapper {...props} />;
