import React, {useState, useEffect} from 'react';
import {Text, View, StatusBar, Switch, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button, Footer, FooterTab} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonBack from '../../components/Button/buttonBack';
import CardIcons from '../../components/CreditCards/Icons';
import CreditCardView from '../../components/CreditCards/CardView';
import {RFPercentage} from 'react-native-responsive-fontsize';
import CardTitle from '../../components/CreditCards/CardTitle';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import DeleteCardMutation from '../../graphql/mutations/deletePaymentMethod';
import UpdateCardMutation from '../../graphql/mutations/updatePaymentMethod';
import {CommonActions} from '@react-navigation/native';
import ToasterSuccess from '../../components/Toaster/successToaster';
import ToasterError from '../../components/Toaster/errorToaster';
import ModalRemoveLoader from '../../components/CreditCards/Modals/ModalRemoveLoader';
import ModalAlert from '../../components/CreditCards/Modals/ModalAlert';

const {
  americanexpress,
  dinersclub,
  mastercard,
  discover,
  jcb,
  placeholder,
  visa,
} = CardIcons;
const {black, lightSalmon, greyLine, newContainerColor, white, mainGreen, transparent, mainRed} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

const CardDetail = props => {
  console.log('CardDetail Props: ', props);
  const {navigation, route, client} = props;
  const {params} = route;
  const {card} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [cardId, setCardId] = useState(false);
  const [cardDetail, setCardDetail] = useState(null);
  const [ccNumber, setCCNumber] = useState('');
  const [ccDate, setCCDate] = useState('');
  const [ccYear, setCCYear] = useState('');
  const [cvc, setCVC] = useState('');
  const [, setCCType] = useState('');
  const [ccTypeImage, setCCTypeImage] = useState(placeholder);
  const [] = useState(false);
  const [] = useState(19);
  const [flip] = useState(false);
  const [isPrimary, setIsPrimary] = useState(false);

  const [showAlertRemoveCard, setAlertRemoveCard] = useState(false);
  const [showModalRemoveCard, setShowModalRemoveCard] = useState(false);
  const [isSuccessRemove, setIsSuccessRemove] = useState(false);
  const [isFailedRemove, setIsFailedRemove] = useState(false);

  const [switchToPrimarySuccess, setSwictToPrimarySuccess] = useState(false);
  const [swicthToPrimaryFailed, setSwictToPrimaryFailed] = useState(false);

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
    setCardDetailToState();

    const subscriber = navigation.addListener('focus', () => {
      settingStatusBar();
      navigationOptions();
      setCardDetailToState();
    });

    return subscriber;
  }, [navigation]);

  const setCardDetailToState = async () => {
    try {
      if (card) {
        const {
          id,
          brand,
          expMonth,
          expYear,
          last4,
          isPrimary: cardIsPrimary,
        } = card;

        const manipulatedMonth =
          expMonth.length > 1 ? `${String(expMonth)}` : `0${expMonth}`;
        const manipulatedYear = String(expYear).slice(2, 4);

        if (brand === 'Visa') {
          setCCType('visa');
          setCCTypeImage(visa);
        } else if (brand === 'MasterCard') {
          setCCType('mastercard');
          setCCTypeImage(mastercard);
        } else if (brand === 'American Express') {
          setCCType('americanexpress');
          setCCTypeImage(americanexpress);
        } else if (brand === 'Discover') {
          setCCType('discover');
          setCCTypeImage(discover);
        } else if (brand === 'JCB') {
          setCCType('jcb');
          setCCTypeImage(jcb);
        } else if (brand === 'Diners Club') {
          setCCType('dinersclub');
          setCCTypeImage(dinersclub);
        } else {
          setCCType('default');
          setCCTypeImage(placeholder);
        }

        await setIsPrimary(cardIsPrimary);
        await setCardId(id);
        await setCCNumber(`**** **** **** ${last4}`);
        await setCCDate(manipulatedMonth);
        await setCCYear(manipulatedYear);
        await setCVC('***');
        await setCardDetail(card);
        await setIsError(false);
        await setIsLoading(false);
      } else {
        await setCardDetail(card);
        await setIsError(false);
        await setIsLoading(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: `${card.brand}`,
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
      headerRight: () => {
        return (
          <Button
            style={{width: 100, justifyContent: 'center', alignItems: 'center'}}
            transparent
            onPress={() => {
              navigation.navigate('EditCard', {card});
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: mainRed,
              }}>
              Edit
            </Text>
          </Button>
        );
      },
    });
  };

  const onSwitchToPrimary = () => {
    try {
      console.log('Card Id Switch: ', cardId);
      client
        .mutate({
          mutation: UpdateCardMutation,
          variables: {
            cardId,
          },
        })
        .then(response => {
          console.log('Switch Reponse: ', response);
          const {data} = response;
          const {updatePaymentMethod} = data;
          const {error} = updatePaymentMethod;

          if (error === null) {
            setSwictToPrimarySuccess(true);

            setTimeout(() => {
              setSwictToPrimarySuccess(false);
              navigation.dispatch(
                CommonActions.navigate({
                  name: 'PaymentMethod',
                }),
              );
            }, 3000);
          } else {
            setSwictToPrimaryFailed(true);

            setTimeout(() => {
              setSwictToPrimaryFailed(false);
            }, 3000);
          }
        })
        .catch(error => {
          console.log('Error to Switch Catch Promise: ', error);
          setSwictToPrimaryFailed(true);

          setTimeout(() => {
            setSwictToPrimaryFailed(false);
          }, 3000);
        });
    } catch (error) {
      console.log('Switch False: ', error);
      setSwictToPrimaryFailed(true);

      setTimeout(() => {
        setSwictToPrimaryFailed(false);
      }, 3000);
    }
  };

  const onRemoveCard = async () => {
    try {
      await setAlertRemoveCard(false);
      await setShowModalRemoveCard(true);
      await setIsSuccessRemove(false);
      await setIsFailedRemove(false);

      const creditCardId = String(card.id);

      if (creditCardId) {
        await client
          .mutate({
            mutation: DeleteCardMutation,
            variables: {
              cardId: creditCardId,
            },
          })
          .then(async response => {
            console.log('response DeleteCardMutation: ', response);
            const {data} = response;
            const {deletePaymentMethod} = data;
            const {error} = deletePaymentMethod;

            if (error === null) {
              await setIsSuccessRemove(true);
              await setIsFailedRemove(false);

              setTimeout(async () => {
                await setIsSuccessRemove(false);
                await setIsFailedRemove(false);
                await setShowModalRemoveCard(false);
                navigation.dispatch(
                  CommonActions.navigate({
                    name: 'PaymentMethod',
                  }),
                );
              }, 3000);
            } else {
              await setIsSuccessRemove(false);
              await setIsFailedRemove(true);

              setTimeout(async () => {
                await setIsSuccessRemove(false);
                await setIsFailedRemove(false);
                await setShowModalRemoveCard(false);
              }, 3000);
            }
          })
          .catch(async error => {
            console.log('Error DeleteCardMutation: ', error);
            await setIsSuccessRemove(false);
            await setIsFailedRemove(true);

            setTimeout(async () => {
              await setIsSuccessRemove(false);
              await setIsFailedRemove(false);
              await setShowModalRemoveCard(false);
            }, 3000);
          });
      } else {
        await setIsSuccessRemove(false);
        await setIsFailedRemove(true);

        setTimeout(async () => {
          await setIsSuccessRemove(false);
          await setIsFailedRemove(false);
          await setShowModalRemoveCard(false);
        }, 3000);
      }
    } catch (error) {
      console.log('Error Try Catch on Remove Card: ', error);
      setIsSuccessRemove(false);
      setIsFailedRemove(true);

      setTimeout(async () => {
        await setIsSuccessRemove(false);
        await setIsFailedRemove(false);
        await setShowModalRemoveCard(false);
      }, 3000);
    }
  };

  if (isLoading === true && isError === false && cardDetail === null) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else if (isLoading === false && isError === false && cardDetail !== null) {
    return (
      <Container style={{backgroundColor: white}}>
        {switchToPrimarySuccess ? (
          <ToasterSuccess text="Set as primary method" />
        ) : null}
        {swicthToPrimaryFailed ? (
          <ToasterError text="Failed set to primary method" />
        ) : null}
        <ModalAlert
          visible={showAlertRemoveCard}
          onRemove={() => onRemoveCard()}
          onCancel={() => setAlertRemoveCard(false)}
        />
        <ModalRemoveLoader
          showModalRemoveCard={showModalRemoveCard}
          isSuccessRemove={isSuccessRemove}
          isFailedRemove={isFailedRemove}
        />
        <Content>
          <Card
            transparent
            style={{marginTop: 15, elevation: 0, shadowOpacity: 0}}>
            <CardTitle />
            <CreditCardView
              cardImageType={ccTypeImage}
              flip={flip}
              ccNumber={ccNumber}
              cvc={cvc}
              expiryDate={ccDate}
              expiryYear={ccYear}
            />
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Set as primary payment method
              </Text>
              <Switch
                trackColor={{false: '#767577', true: '#44D363'}}
                thumbColor={isPrimary ? '#ffffff' : '#f4f3f4'}
                ios_backgroundColor="#D3D3D3"
                onValueChange={e => {
                  console.log('Swtich: ', e);
                  setIsPrimary(e);
                  onSwitchToPrimary();
                }}
                value={isPrimary}
              />
            </CardItem>
            {/* <CardItem>
              <Button onPress={() => setAlertRemoveCard(true)} transparent>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.9),
                    color: lightSalmon,
                    letterSpacing: 0.3,
                  }}>
                  Remove Card
                </Text>
              </Button>
            </CardItem> */}
          </Card>
        </Content>
        <ButtonRemoveCard onPress={() => setAlertRemoveCard(true)} />
      </Container>
    );
  } else {
    return (
      <ErrorScreen
        message="Go Back"
        onPress={() => {
          // refetch function
          navigation.goBack(null);
        }}
      />
    );
  }
};

export const ButtonRemoveCard = props => {
  const {onPress} = props;
  return (
    <Footer style={{backgroundColor: white, borderTopColor: white}}>
      <FooterTab style={{backgroundColor: white}}>
        <View style={{backgroundColor: white, width: '100%'}}>
          <TouchableOpacity
            onPress={() => onPress()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: mainGreen,
                letterSpacing: 0.3,
              }}>
              REMOVE CARD
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(CardDetail);

export default props => <Wrapper {...props} />;
