import React, {useEffect} from 'react';
import {StatusBar, Animated, TouchableOpacity, Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Footer, FooterTab, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonBack from '../../components/Button/buttonBack';
import CardPayment from '../../components/Cards/Payments/PaymentCard';
import PaymentHistory from '../../components/Cards/Payments/PaymentHistory';
import {connect} from 'react-redux';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, white, greyLine, mainGreen} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const PaymentDetails = props => {
  console.log('PaymentDetails Props: ', props);
  const {route, navigation, positionYBottomNav} = props;
  const {params} = route;
  const {paymentDetail: item} = params;

  useEffect(() => {
    settingStatusBar();
    onChangeOpacity(false);
    navigationOptions();

    const subscriber = navigation.addListener('focus', () => {
      settingStatusBar();
      onChangeOpacity(false);
      navigationOptions();
    });

    return subscriber;
  }, []);

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const popStacking = () => {
    try {
      // navigation.dangerouslyGetParent().setOptions({
      //   tabBarVisible: false,
      // });
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Payment Details',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  if (item === undefined || item === null) {
    return null;
  } else {
    const {order} = item;
    return (
      <Container style={{backgroundColor: white}}>
        <Content contentContainerStyle={{paddingTop: 15}}>
          <CardPayment item={item} fullWidth={true} />
          <PaymentHistory
            showTextLastPayment={true}
            singlePreview
            {...props}
            orderDetailData={order}
            paymentList={order?.paymentList ? order.paymentList : []}
          />
        </Content>
        <ButtonMakePayment orderDetailData={order} {...props} />
      </Container>
    );
  }
};

export const ButtonMakePayment = props => {
  const {orderDetailData, navigation} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('MakePayment', {
                paymentDetail: {
                  order: orderDetailData,
                  merchant: orderDetailData.merchant,
                  totalPaid: orderDetailData.totalPaid,
                  balance: orderDetailData.balance,
                  paymentList: orderDetailData.paymentList,
                },
              });
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              MAKE PAYMENT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = dispatch => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(PaymentDetails);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
