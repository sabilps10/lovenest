import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  StatusBar,
  RefreshControl,
  Dimensions,
  Animated,
} from 'react-native';
import {connect} from 'react-redux';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Card, CardItem} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import PaymentHistoryQuery from '../../graphql/queries/getHistoryOrderPaymentGroupByDate';
import ButtonBack from '../../components/Button/buttonBack';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {commonImage} from '../../utils/Themes/Images';
import JustLoader from '../../components/Loader/JustLoader';
import HeaderCardLabel from '../../components/Cards/Payments/PaymentCard/HeaderCardLabel';
import Separator from '../../components/Cards/Payments/PaymentCard/Separator';

const {lnLogoNotif} = commonImage;
const {black, greyLine, white, transparent, newContainerColor} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;

export const PaymentHistoryCard = props => {
  console.log('PaymentHistoryCard: ', props);
  const {list} = props;
  if (list?.payments === undefined || list?.payments === null) {
    return null;
  } else {
    return (
      <FlatList
        data={list.payments}
        extraData={list.payments}
        keyExtractor={(item, index) => {
          `${String(index)} Payment History`;
        }}
        listKey={(item, index) => {
          `${String(index)} Payment History`;
        }}
        renderItem={({item}) => {
          const {merchant, orderPayment, order} = item;
          const {preAmount} = orderPayment;
          const logoSource = !merchant
            ? {lnLogoNotif}
            : merchant.logoImageDynamicUrl === null ||
              merchant.logoImageDynamicUrl === undefined
            ? {uri: merchant.logoImageUrl}
            : {uri: `${merchant.logoImageDynamicUrl}=h500`};
          return (
            <Card
              style={{
                marginLeft: 15,
                marginRight: 15,
                // elevation: 0,
                // shadowOpacity: 0,
                borderRadius: 4,
              }}>
              <HeaderCardLabel
                logoSource={logoSource}
                merchant={merchant}
                order={order}
              />
              <Separator />
              <CardItem
                style={{
                  backgroundColor: 'transparent',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.6),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  AMOUNT
                </Text>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  {preAmount ? `$ ${preAmount.toFixed(2)}` : 'N/A'}
                </Text>
              </CardItem>
            </Card>
          );
        }}
      />
    );
  }
};

const PaymentHistoryList = props => {
  console.log('PaymentHistoryList Props: ', props);
  const {navigation, client, positionYBottomNav} = props;

  const pageNumber = 1;

  const [paymentHistory, setPaymentHistory] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize, setPageSize] = useState(10);
  const [totalCount, setIsTotalCount] = useState(0);

  const [isPullToRefresh, setIsPullToRefresh] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
    onChangeOpacity(false);
    fetch();

    const subscriber = navigation.addListener('focus', () => {
      settingStatusBar();
      navigationOptions();
      onChangeOpacity(false);
      fetch();
    });

    return subscriber;
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const settingIsLoadMore = async value => {
    try {
      console.log('settingIsLoadMore VALUEEE: ', value);
      await setIsLoadMore(value);
      let oldPageSize = pageSize;
      let newPageSize = (oldPageSize += 10);
      await setPageSize(newPageSize);
      await fetch();
    } catch (error) {
      setIsLoadMore(false);
    }
  };

  const pullToRefresh = async () => {
    try {
      await setIsPullToRefresh(true);
      await setPageSize(10);
      await fetch();
    } catch (error) {
      console.log('Erro pull to Refresh: ', error);
      setIsPullToRefresh(true);
    }
  };

  const fetch = () => {
    try {
      console.log('IS LOAD MORE: ', isLoadMore);
      client
        .query({
          query: PaymentHistoryQuery,
          variables: {
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('response payment history list: ', response);
          const {data, errors} = response;
          const {getHistoryOrderPaymentsGroupByDate} = data;
          const {
            error,
            data: paymentList,
            totalCount: isTotalCount,
          } = getHistoryOrderPaymentsGroupByDate;

          if (errors) {
            await setIsError(true);
            await setIsPullToRefresh(false);
            await setIsLoadMore(false);
            await setIsLoading(false);
          } else {
            if (error === null) {
              await setIsTotalCount(isTotalCount);
              await setPaymentHistory([...paymentList]);
              // await setPaymentHistory([]);
              await setIsError(false);
              await setIsPullToRefresh(false);
              await setIsLoadMore(false);
              await setIsLoading(false);
            } else {
              await setIsError(true);
              await setIsPullToRefresh(false);
              await setIsLoadMore(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async errorQuery => {
          console.log('Error Query: ', errorQuery);
          await setIsError(true);
          await setIsPullToRefresh(false);
          await setIsLoadMore(false);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('fetch error: ', error);
      setIsError(true);
      setIsPullToRefresh(false);
      setIsLoadMore(false);
      setIsLoading(false);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Payment History',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  if (isLoading === true && isError === false) {
    console.log('MASUK LOADING');
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <StatusBar barStyle="dark-content" />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loader />
        </View>
      </Container>
    );
  } else if (isLoading === false && isError === false) {
    console.log('MASUK NO ERROR');
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <FlatList
          contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  flex: 1,
                  height: Dimensions.get('window').height / 1.19,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(2),
                    color: greyLine,
                  }}>
                  No Payment History Yet.
                </Text>
              </View>
            );
          }}
          ListFooterComponent={() => {
            return (
              <CardItem
                style={{
                  paddingTop: 0,
                  paddingBottom: 0,
                  backgroundColor: transparent,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 15,
                }}>
                <View style={{height: 40, width: 40}}>
                  {isLoadMore ? <JustLoader /> : null}
                </View>
              </CardItem>
            );
          }}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            if (pageSize <= totalCount) {
              settingIsLoadMore(true);
            } else {
              setIsLoadMore(false);
            }
          }}
          refreshControl={
            <RefreshControl
              refreshing={isPullToRefresh}
              onRefresh={() => pullToRefresh()}
            />
          }
          data={paymentHistory}
          extraData={paymentHistory}
          keyExtractor={(item, index) => `${String(index)} Header List`}
          listKey={(item, index) => `${String(index)} Header List`}
          renderItem={({item, index}) => {
            return (
              <Card
                transparent
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  elevation: 0,
                  shadowOpacity: 0,
                }}>
                <CardItem style={{backgroundColor: transparent}}>
                  <Text
                    style={{
                      left: 2,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.7),
                      color: '#999999',
                      letterSpacing: 0.3,
                    }}>
                    {item.date}
                  </Text>
                </CardItem>
                <PaymentHistoryCard list={item} parentIndex={index} />
              </Card>
            );
          }}
        />
      </Container>
    );
  } else {
    console.log('MASUK ELSE');
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <StatusBar barStyle="dark-content" />
        <ErrorScreen
          message="Refresh"
          onPress={async () => {
            // refetch function
          }}
        />
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = dispatch => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(PaymentHistoryList);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
