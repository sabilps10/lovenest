import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  TextInput,
  Switch,
  Dimensions,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonBack from '../../components/Button/buttonBack';
import valid from 'card-validator';
import CardIcons from '../../components/CreditCards/Icons';
import CardTypeImage from '../../components/CreditCards/CardTypeImage';
import {RFPercentage} from 'react-native-responsive-fontsize';
import CardTitle from '../../components/CreditCards/CardTitle';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import DeleteCardMutation from '../../graphql/mutations/deletePaymentMethod';
import CreateNewCardMutation from '../../graphql/mutations/addPaymentMethod';
import {CommonActions} from '@react-navigation/native';
import ModalLoaderUpdateCard from '../../components/CreditCards/Modals/ModalAddNewCardLoader';
import JustLoader from '../../components/Loader/JustLoader';

// Query
import GEt_CARD_TOKEN from '../../graphql/queries/getCardToken';

const {
  americanexpress,
  dinersclub,
  mastercard,
  discover,
  jcb,
  placeholder,
  visa,
  invalidIcon,
} = CardIcons;
const {
  black,
  lightSalmon,
  greyLine,
  newContainerColor,
  transparent,
  white,
  mainRed,
} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

class EditCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isError: false,
      ccNumber: '',
      ccDate: '',
      ccYear: '',
      cvc: '',
      ccTypeImage: '',
      isPrimary: false,
      cardId: '',
      cardDetail: null,
      maxLengthCardNumber: 19,
      inValidCard: false,
      isLoadingUpdateCC: false,
      isSuccessUpdateCC: false,
      isSuccessUpdateCCMsg: '',
      isFailedUpdateCC: false,
      isFailedUpdateCCMsg: '',
    };
    this.ccNumberRef = React.createRef();
    this.ccDateRef = React.createRef();
    this.ccYearRef = React.createRef();
    this.cvcRef = React.createRef();

    this.subscriber = this.props.navigation;
  }

  componentDidMount() {
    const {props} = this;
    const {route} = props;
    const {params} = route;
    const {card} = params;

    this.navigationOptions();

    if (card) {
      this.fetchFirstLoadCardData(card);
    }
  }

  popStacking = () => {
    const {props} = this;
    const {navigation} = props;
    try {
      navigation.goBack(null);
    } catch (error) {
      navigation.goBack(null);
    }
  };

  navigationOptions = () => {
    const {props} = this;
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'Edit Credit Card',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => this.popStacking()} />;
      },
      headerRight: () => {
        return (
          <Button
            style={{
              width: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            transparent
            onPress={() => this.saveCard()}>
            {this.state.isLoadingUpdateCC ? (
              <View
                style={{
                  left: 10,
                  width: 35,
                  height: 35,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <JustLoader />
              </View>
            ) : (
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: regular,
                  color: mainRed,
                }}>
                Save
              </Text>
            )}
          </Button>
        );
      },
    });
  };

  fetchFirstLoadCardData = cardData => {
    if (cardData) {
      const {
        id,
        brand,
        expMonth,
        expYear,
        last4,
        isPrimary: cardIsPrimary,
      } = cardData;

      const manipulatedMonth =
        String(expMonth).length > 1
          ? `${String(expMonth)}`
          : `0${String(expMonth)}`;
      const manipulatedYear = String(expYear).slice(2, 4);

      this.setBrandCard(brand);

      this.setState(prevState => ({
        ...prevState,
        cardId: id,
        cardDetail: cardData,
        ccNumber: `**** **** **** ${last4}`,
        ccDate: manipulatedMonth,
        ccYear: manipulatedYear,
        cvc: '***',
        isPrimary: cardIsPrimary,
        isLoading: false,
        isError: false,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        isLoading: false,
        isError: true,
      }));
    }
  };

  setBrandCard = brand => {
    let updatedBrand = placeholder;
    if (brand === 'Visa') {
      updatedBrand = visa;
    } else if (brand === 'MasterCard') {
      updatedBrand = mastercard;
    } else if (brand === 'American Express') {
      updatedBrand = americanexpress;
    } else if (brand === 'Discover') {
      updatedBrand = discover;
    } else if (brand === 'JCB') {
      updatedBrand = jcb;
    } else if (brand === 'Diners Club') {
      updatedBrand = dinersclub;
    } else {
      updatedBrand = placeholder;
    }

    this.setState(prevState => ({
      ...prevState,
      ccTypeImage: updatedBrand,
    }));
  };

  setCCNumber = e => {
    this.setState(prevState => ({
      ...prevState,
      inValidCard: false,
    }));

    const ccFormatter = e
      .replace(/\s?/g, '')
      .replace(/(\d{4})/g, '$1 ')
      .trim();

    var numberValidation = valid.number(ccFormatter);

    if (!numberValidation.isPotentiallyValid) {
      this.setState(prevState => ({
        ...prevState,
        ccTypeImage: invalidIcon,
      }));
    }

    if (numberValidation.card) {
      this.updatingBrandCard(numberValidation.card);
    }

    if (ccFormatter === '' && e === '') {
      this.setState(prevState => ({
        ...prevState,
        ccTypeImage: placeholder,
      }));
    }

    this.setState(
      prevState => ({
        ...prevState,
        ccNumber: ccFormatter,
      }),
      () => {
        const {ccNumber} = this.state;
        if (ccNumber.length === 19) {
          this.ccDateRef.current.focus();
        }
      },
    );
  };

  updateBrandWhentyping = type => {
    console.log('Type updateBrandWhentyping: ', type);
    if (type) {
      if (type === 'visa') {
        return visa;
      } else if (type === 'mastercard') {
        return mastercard;
      } else if (type === 'american-express') {
        return americanexpress;
      } else if (type === 'discover') {
        return discover;
      } else if (type === 'jcb') {
        return jcb;
      } else if (type === 'diners-club') {
        return dinersclub;
      } else {
        return placeholder;
      }
    }
  };

  updatingBrandCard = async typeCard => {
    const {type} = typeCard;
    const typeCC = await this.updateBrandWhentyping(type);
    if (typeCC !== null || typeCC !== undefined) {
      this.setState(prevState => ({
        ...prevState,
        ccTypeImage: typeCC,
      }));
    }
  };

  saveCard = async () => {
    try {
      console.log('State: ', this.state);
      const {props, state} = this;
      const {client, navigation} = props;
      const {ccNumber, ccDate, ccYear, cvc, cardId, isPrimary} = state;

      const stripeParams = {
        number: ccNumber.replace(' ', ''),
        expMonth: parseInt(ccDate, 10),
        expYear: parseInt(ccYear, 10),
        cvc: String(cvc),
      };

      await this.setState(prevState => ({
        ...prevState,
        isLoadingUpdateCC: true,
        isSuccessUpdateCC: false,
        isSuccessUpdateCCMsg: '',
        isFailedUpdateCC: false,
        isFailedUpdateCCMsg: '',
      }));

      if (ccNumber === '' || ccDate === '' || ccYear === '' || cvc === '') {
        await this.setState(prevState => ({
          ...prevState,
          isFailedUpdateCC: true,
          isFailedUpdateCCMsg: 'Please fill the form!',
        }));

        setTimeout(() => {
          this.setState(prevState => ({
            ...prevState,
            isLoadingUpdateCC: false,
            isSuccessUpdateCC: false,
            isSuccessUpdateCCMsg: '',
            isFailedUpdateCC: false,
            isFailedUpdateCCMsg: '',
          }));
        }, 3000);
      } else {
        if (ccNumber[0] === '*' || cvc[0] === '*') {
          await this.setState(prevState => ({
            ...prevState,
            isFailedUpdateCC: true,
            isFailedUpdateCCMsg: 'Please re type your card detail!',
          }));

          setTimeout(() => {
            this.setState(prevState => ({
              ...prevState,
              isLoadingUpdateCC: false,
              isSuccessUpdateCC: false,
              isSuccessUpdateCCMsg: '',
              isFailedUpdateCC: false,
              isFailedUpdateCCMsg: '',
            }));
          }, 3000);
        } else {
          await client
            .mutate({
              mutation: DeleteCardMutation,
              variables: {
                cardId,
              },
            })
            .then(async responseDeleteCard => {
              console.log('responseDeleteCard: ', responseDeleteCard);
              const {data} = responseDeleteCard;
              const {deletePaymentMethod} = data;
              const {error} = deletePaymentMethod;

              if (error === null) {
                await client
                  .query({
                    query: GEt_CARD_TOKEN,
                    variables: {
                      ...stripeParams,
                    },
                    fetchPolicy: 'no-cache',
                    ssr: false,
                  })
                  .then(async res => {
                    const {data: d, errors: errorsGetCardToken} = res;
                    const {getCardToken} = d;
                    const {data: datas, error: errorGetcardToken} =
                      getCardToken;
                    const {id} = datas;

                    if (errorsGetCardToken) {
                      // fail
                      await this.setState(prevState => ({
                        ...prevState,
                        isFailedUpdateCC: true,
                        isFailedUpdateCCMsg: 'Failed to update your card!',
                      }));

                      setTimeout(() => {
                        this.setState(prevState => ({
                          ...prevState,
                          isLoadingUpdateCC: false,
                          isSuccessUpdateCC: false,
                          isSuccessUpdateCCMsg: '',
                          isFailedUpdateCC: false,
                          isFailedUpdateCCMsg: '',
                        }));
                      }, 3000);
                    } else {
                      if (errorGetcardToken) {
                        // fail
                        await this.setState(prevState => ({
                          ...prevState,
                          isFailedUpdateCC: true,
                          isFailedUpdateCCMsg: 'Failed to update your card!',
                        }));

                        setTimeout(() => {
                          this.setState(prevState => ({
                            ...prevState,
                            isLoadingUpdateCC: false,
                            isSuccessUpdateCC: false,
                            isSuccessUpdateCCMsg: '',
                            isFailedUpdateCC: false,
                            isFailedUpdateCCMsg: '',
                          }));
                        }, 3000);
                      } else {
                        // success
                        await client
                          .mutate({
                            mutation: CreateNewCardMutation,
                            variables: {
                              token: id,
                              isPrimary,
                            },
                          })
                          .then(async responseCreateNewCard => {
                            console.log(
                              'responseCreateNewCard: ',
                              responseCreateNewCard,
                            );
                            const {data: newCardResponse} =
                              responseCreateNewCard;
                            const {addPaymentMethod} = newCardResponse;
                            const {error: newCardError} = addPaymentMethod;

                            if (newCardError === null) {
                              await this.setState(prevState => ({
                                ...prevState,
                                isSuccessUpdateCC: true,
                                isSuccessUpdateCCMsg:
                                  'Successfully update your card!',
                              }));

                              setTimeout(() => {
                                this.setState(
                                  prevState => ({
                                    ...prevState,
                                    isLoadingUpdateCC: false,
                                    isSuccessUpdateCC: false,
                                    isSuccessUpdateCCMsg: '',
                                    isFailedUpdateCC: false,
                                    isFailedUpdateCCMsg: '',
                                  }),
                                  () => {
                                    navigation.dispatch(
                                      CommonActions.navigate({
                                        name: 'PaymentMethod',
                                      }),
                                    );
                                  },
                                );
                              }, 3000);
                            } else {
                              await this.setState(prevState => ({
                                ...prevState,
                                isFailedUpdateCC: true,
                                isFailedUpdateCCMsg:
                                  'Failed to update your card!',
                              }));

                              setTimeout(() => {
                                this.setState(prevState => ({
                                  ...prevState,
                                  isLoadingUpdateCC: false,
                                  isSuccessUpdateCC: false,
                                  isSuccessUpdateCCMsg: '',
                                  isFailedUpdateCC: false,
                                  isFailedUpdateCCMsg: '',
                                }));
                              }, 3000);
                            }
                          })
                          .catch(async errorCreateNewCard => {
                            console.log(
                              'errorCreateNewCard Error: ',
                              errorCreateNewCard,
                            );
                            await this.setState(prevState => ({
                              ...prevState,
                              isFailedUpdateCC: true,
                              isFailedUpdateCCMsg:
                                'Failed to update your card!',
                            }));

                            setTimeout(() => {
                              this.setState(prevState => ({
                                ...prevState,
                                isLoadingUpdateCC: false,
                                isSuccessUpdateCC: false,
                                isSuccessUpdateCCMsg: '',
                                isFailedUpdateCC: false,
                                isFailedUpdateCCMsg: '',
                              }));
                            }, 3000);
                          });
                      }
                    }
                  })
                  .catch(async errorss => {
                    await this.setState(prevState => ({
                      ...prevState,
                      isFailedUpdateCC: true,
                      isFailedUpdateCCMsg: 'Failed to update your card!',
                    }));

                    setTimeout(() => {
                      this.setState(prevState => ({
                        ...prevState,
                        isLoadingUpdateCC: false,
                        isSuccessUpdateCC: false,
                        isSuccessUpdateCCMsg: '',
                        isFailedUpdateCC: false,
                        isFailedUpdateCCMsg: '',
                      }));
                    }, 3000);
                  });
              } else {
                await this.setState(prevState => ({
                  ...prevState,
                  isFailedUpdateCC: true,
                  isFailedUpdateCCMsg: 'Failed to update your card!',
                }));

                setTimeout(() => {
                  this.setState(prevState => ({
                    ...prevState,
                    isLoadingUpdateCC: false,
                    isSuccessUpdateCC: false,
                    isSuccessUpdateCCMsg: '',
                    isFailedUpdateCC: false,
                    isFailedUpdateCCMsg: '',
                  }));
                }, 3000);
              }
            })
            .catch(async errorDeleteCard => {
              console.log('Error Delete Card: ', errorDeleteCard);
              await this.setState(prevState => ({
                ...prevState,
                isFailedUpdateCC: true,
                isFailedUpdateCCMsg: 'Failed to update your card!',
              }));

              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  isLoadingUpdateCC: false,
                  isSuccessUpdateCC: false,
                  isSuccessUpdateCCMsg: '',
                  isFailedUpdateCC: false,
                  isFailedUpdateCCMsg: '',
                }));
              }, 3000);
            });
        }
      }
    } catch (error) {
      console.log('Save Card Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        isFailedUpdateCC: true,
        isFailedUpdateCCMsg: 'Please re type your card detail!',
      }));

      setTimeout(() => {
        this.setState(prevState => ({
          ...prevState,
          isLoadingUpdateCC: false,
          isSuccessUpdateCC: false,
          isSuccessUpdateCCMsg: '',
          isFailedUpdateCC: false,
          isFailedUpdateCCMsg: '',
        }));
      }, 3000);
    }
  };

  render() {
    const {navigation, client, route} = this.props;
    const {
      isLoading,
      isError,
      ccNumber,
      ccDate,
      ccYear,
      cvc,
      ccTypeImage,
      isPrimary,
      maxLengthCardNumber,
      inValidCard,
      isLoadingUpdateCC,
      isSuccessUpdateCC,
      isSuccessUpdateCCMsg,
      isFailedUpdateCC,
      isFailedUpdateCCMsg,
      cardDetail,
    } = this.state;
    if (isLoading === true && isError === false && cardDetail === null) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <StatusBar barStyle="dark-content" />
          <Loader />
        </View>
      );
    } else if (
      isLoading === false &&
      isError === false &&
      cardDetail !== null
    ) {
      return (
        <Container style={{backgroundColor: white}}>
          <ModalLoaderUpdateCard
            showModal={isLoadingUpdateCC}
            isSuccess={isSuccessUpdateCC}
            isFailed={isFailedUpdateCC}
            isSuccessMsg={isSuccessUpdateCCMsg}
            isFailedMsg={isFailedUpdateCCMsg}
          />
          <Content contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}>
            <Card
              transparent
              style={{
                marginLeft: 0,
                marginRight: 0,
                height: Dimensions.get('window').height / 1.33,
                elevation: 0,
                shadowOpacity: 0,
              }}>
              <CardItem
                style={{
                  paddingTop: 25,
                  paddingBottom: 25,
                  paddingLeft: 0,
                  paddingRight: 0,
                }}>
                <CardTitle />
              </CardItem>
              <CardItem style={{backgroundColor: transparent}}>
                <CardTypeImage ccTypeImage={ccTypeImage} />
                <TextInput
                  maxLength={maxLengthCardNumber}
                  returnKeyType="next"
                  onSubmitEditing={() => this.ccDateRef.current.focus()}
                  ref={this.ccNumberRef}
                  style={{
                    flex: 0.8,
                    padding: 6,
                    paddingLeft: 5,
                    paddingRight: 5,
                    borderBottomColor: inValidCard ? 'red' : greyLine,
                    borderBottomWidth: 0.5,
                    fontSize: RFPercentage(1.9),
                  }}
                  value={ccNumber}
                  placeholder={'4242 4242 4242 4242'}
                  keyboardType="numeric"
                  onChangeText={e => {
                    this.setCCNumber(e);
                  }}
                />
                <View
                  style={{
                    flex: 0.4,
                    flexDirection: 'row',
                    borderBottomColor: inValidCard ? 'red' : greyLine,
                    borderBottomWidth: 0.5,
                  }}>
                  <TextInput
                    maxLength={2}
                    ref={this.ccDateRef}
                    style={{
                      padding: 6,
                      paddingLeft: 5,
                      paddingRight: 5,
                      fontSize: RFPercentage(1.9),
                    }}
                    returnKeyType="next"
                    onSubmitEditing={() => this.ccYearRef.current.focus()}
                    value={ccDate}
                    placeholder={'MM'}
                    keyboardType="number-pad"
                    onChangeText={e => {
                      if (e.length === 2) {
                        this.ccYearRef.current.focus();
                      }
                      // function here
                      this.setState(prevState => ({
                        ...prevState,
                        ccDate: e,
                      }));
                    }}
                    onKeyPress={e => {
                      if (e.nativeEvent.key === 'Backspace' && ccDate === '') {
                        this.ccNumberRef.current.focus();
                      }
                    }}
                  />
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color:
                          ccDate.length === 2 && ccYear.length === 2
                            ? black
                            : greyLine,
                        fonstSize: RFPercentage(1.9),
                      }}>
                      {'/'}
                    </Text>
                  </View>
                  <TextInput
                    maxLength={2}
                    ref={this.ccYearRef}
                    style={{
                      padding: 6,
                      paddingLeft: 5,
                      paddingRight: 5,
                      fontSize: RFPercentage(1.9),
                    }}
                    returnKeyType="next"
                    onSubmitEditing={() => this.cvcRef.current.focus()}
                    value={ccYear}
                    placeholder={'YY'}
                    keyboardType="number-pad"
                    onChangeText={e => {
                      if (e === '') {
                        this.ccDateRef.current.focus();
                      }
                      if (e.length === 2) {
                        this.cvcRef.current.focus();
                      }
                      // function here
                      this.setState(prevState => ({
                        ...prevState,
                        ccYear: e,
                      }));
                    }}
                    onKeyPress={e => {
                      if (e.nativeEvent.key === 'Backspace' && ccYear === '') {
                        this.ccDateRef.current.focus();
                      }
                    }}
                  />
                </View>
                <TextInput
                  maxLength={3}
                  ref={this.cvcRef}
                  style={{
                    fontSize: RFPercentage(1.9),
                    flex: 0.2,
                    padding: 6,
                    paddingLeft: 5,
                    paddingRight: 5,
                    borderBottomColor: inValidCard ? 'red' : greyLine,
                    borderBottomWidth: 0.5,
                  }}
                  value={cvc}
                  placeholder={'123'}
                  keyboardType="number-pad"
                  onChangeText={e => {
                    if (e === '') {
                      this.ccYearRef.current.focus();
                    }
                    // function here
                    this.setState(prevState => ({
                      ...prevState,
                      cvc: e,
                    }));
                  }}
                  onKeyPress={e => {
                    if (e.nativeEvent.key === 'Backspace' && cvc === '') {
                      this.ccYearRef.current.focus();
                    }
                  }}
                />
              </CardItem>
              <CardItem
                style={{
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: transparent,
                }}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.9),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Set as primary payment method
                </Text>
                <Switch
                  trackColor={{false: '#767577', true: '#44D363'}}
                  thumbColor={isPrimary ? '#ffffff' : '#f4f3f4'}
                  ios_backgroundColor="#D3D3D3"
                  onValueChange={e => {
                    console.log('Swtich: ', e);
                    this.setState(prevState => ({
                      ...prevState,
                      isPrimary: e,
                    }));
                  }}
                  value={isPrimary}
                />
              </CardItem>
            </Card>
          </Content>
        </Container>
      );
    } else {
      return (
        <ErrorScreen
          message="Go Back"
          onPress={() => {
            // refetch function
            navigation.goBack(null);
          }}
        />
      );
    }
  }
}

const Wrapper = compose(withApollo)(EditCard);

export default props => <Wrapper {...props} />;
