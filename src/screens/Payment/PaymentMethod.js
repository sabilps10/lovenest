import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  RefreshControl,
  Image,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Container, Content, Card, CardItem, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import AsyncImage from '../../components/Image/AsyncImage';
import GetListCardQuery from '../../graphql/queries/getPaymentMethod';
import {creditCard, charImage} from '../../utils/Themes/Images';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import Loader from '../../components/Loader/circleLoader';
import ButtonBack from '../../components/Button/buttonBack';
import CardIcons from '../../components/CreditCards/Icons';

const {jcb} = CardIcons;
const {charGreenCheckedTimeLine} = charImage;
const {mastercard, visa, amex, discover, unknownCard} = creditCard;
const {black, greyLine, newContainerColor, white} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

export const AddCreditCard = ({onPress}) => {
  return (
    <CardItem
      button
      onPress={() => {
        onPress();
      }}
      style={{flexDirection: 'column'}}>
      <View style={{width: '100%', flexDirection: 'row'}}>
        <View style={{flex: 0.25}}>
          <View
            style={{
              height: 31,
              width: 49,
              aspectRatio: 49 / 31,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: black,
              borderStyle: 'dashed',
            }}>
            <Text>+</Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
            }}>
            Add New Card
          </Text>
        </View>
      </View>
    </CardItem>
  );
};

const PaymentMethod = props => {
  console.log('PaymentMethod Props: ', props);
  const {route, navigation, client, positionYBottomNav} = props;

  const [listCard, setlistCard] = useState([]);
  const [isLoading, setIsloading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isRefetch, setIsRefetch] = useState(false);

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
    onChangeOpacity(false);
    fetchCardList();

    const subscriber = navigation.addListener('focus', () => {
      // settingStatusBar();
      // navigationOptions();
      // fetchCardList();
      onChangeOpacity(false);
      refetchCardList();
    });

    return subscriber;
  }, [navigation]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const refetchCardList = async () => {
    setIsRefetch(true);
    fetchCardList();
  };

  const fetchCardList = () => {
    try {
      client
        .query({
          query: GetListCardQuery,
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(response => {
          console.log('Response Fetch Card List: ', response);
          const {data} = response;
          const {getPaymentMethods, errors} = data;

          if (errors === undefined) {
            const {data: cards, error} = getPaymentMethods;
            if (error === null) {
              setlistCard([...cards]);
              // setlistCard([]);
              setIsError(false);
              setIsRefetch(false);
              setIsloading(false);
            } else if (error === 'Payment method not found!') {
              setIsError(false);
              setIsRefetch(false);
              setIsloading(false);
            } else {
              setIsError(true);
              setIsRefetch(false);
              setIsloading(false);
            }
          } else {
            setIsError(true);
            setIsRefetch(false);
            setIsloading(false);
          }
        })
        .catch(error => {
          console.log('Error Query Fetch Card List: ', error);
          setIsError(true);
          setIsRefetch(false);
          setIsloading(false);
        });
    } catch (error) {
      console.log('Error Fetch Card List: ', error);
      setIsError(true);
      setIsRefetch(false);
      setIsloading(false);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Payment Method',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  if (isLoading === false && isError === true) {
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
          fetchCardList();
        }}
      />
    );
  } else if (isLoading === true && isError === false) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else {
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <Content
          refreshControl={
            <RefreshControl
              refreshing={isRefetch}
              onRefresh={refetchCardList}
            />
          }>
          <Card
            transparent
            style={{
              marginLeft: 0,
              marginRight: 0,
              marginTop: 15,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <FlatList
              scrollEnabled={false}
              data={listCard}
              extraData={listCard}
              listKey={(item, index) => `${index} CC`}
              keyExtractor={(item, index) => `${index} CC`}
              ListFooterComponent={() => {
                if (listCard.length > 0) {
                  return (
                    <AddCreditCard
                      onPress={() => {
                        navigation.navigate('AddNewCard');
                      }}
                    />
                  );
                } else {
                  return null;
                }
              }}
              ListEmptyComponent={() => {
                return (
                  <AddCreditCard
                    onPress={() => {
                      navigation.navigate('AddNewCard');
                    }}
                  />
                );
              }}
              renderItem={({item}) => {
                const logoSource =
                  item.brand === 'Visa'
                    ? visa
                    : item.brand === 'MasterCard'
                    ? mastercard
                    : item.brand === 'American Express'
                    ? amex
                    : item.brand === 'Discover'
                    ? discover
                    : item.brand === 'JCB'
                    ? jcb
                    : unknownCard;
                return (
                  <CardItem
                    button
                    onPress={() => {
                      navigation.navigate('CardDetail', {
                        card: item,
                      });
                    }}
                    style={{flexDirection: 'column'}}>
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                      }}>
                      <View style={{flex: 0.3}}>
                        <AsyncImage
                          style={{
                            flex: 1,
                            width: 49,
                            height: 31,
                            aspectRatio: 49 / 31,
                            borderRadius: 2,
                            backgroundColor: '#F7F7F7',
                          }}
                          resizeMode="contain"
                          loaderStyle={{
                            width: 49 / 1.5,
                            height: 31 / 1.5,
                          }}
                          source={logoSource}
                          placeholderColor={newContainerColor}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}>
                        <Text style={{top: 3.3}}>**** **** ****</Text>
                        <Text
                          style={{
                            top: 2,
                            fontFamily: book,
                            fontSize: regular,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {` ${item.last4}`}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'flex-end',
                        }}>
                        {item.isPrimary ? (
                          // <Icon
                          //   type="Feather"
                          //   name="check"
                          //   style={{
                          //     right: -10,
                          //     fontSize: 15,
                          //     color: black,
                          //   }}
                          // />
                          <View style={{width: 25, height: 25, top: 2,}}>
                            <Image
                              source={charGreenCheckedTimeLine}
                              style={{width: 20, height: 20}}
                              resizeMode="contain"
                            />
                          </View>
                        ) : null}
                      </View>
                    </View>
                    <View
                      style={{
                        position: 'absolute',
                        bottom: 0,
                        borderBottomWidth: 0.5,
                        borderBottomColor: greyLine,
                        height: 1,
                        width: '100%',
                      }}
                    />
                  </CardItem>
                );
              }}
            />
          </Card>
        </Content>
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(PaymentMethod);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
