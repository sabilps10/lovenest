import React, {useEffect} from 'react';
import {Text, View, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ButtonBack from '../../components/Button/buttonBack';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {Container, Content, Card, CardItem} from 'native-base';
import PaymentDetail from '../../components/Cards/Payments/PaymentDetail';
import PaymentHistoryLine from '../../components/Cards/Payments/TimeLine/PaymentHistoryTimeLine';
import OrderDetailHeader from '../../components/Cards/OrderDetail/OrderDetailHeader';

const {
  transparent,
  black,
  greyLine,
  lightSalmon,
  white,
  disablePaymentText,
  refundBgColor,
  newContainerColor,
} = Colors;
const {regular, small} = FontSize;
const {book, medium} = FontType;

const PaymentHistory = props => {
  console.log('PaymentHistory Props: ', props);
  const {navigation, route} = props;
  const {params} = route;
  const {data} = params;

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
  }, [navigation]);

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      // await navigation.dangerouslyGetParent().setOptions({
      //   tabBarVisible: true,
      // });
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Payment History',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  if (data === null || data === undefined) {
    return null;
  } else {
    return (
      <Container style={{backgroundColor: 'white'}}>
        <Content contentContainerStyle={{paddingBottom: 15}}>
          <OrderDetailHeader orderDetailData={data} />
          {data.paymentList.length === 0 ? null : (
            <Card
              transparent
              style={{
                marginLeft: 0,
                marginRight: 0,
                elevation: 0,
                shadowOpacity: 0,
                paddingTop: 15,
              }}>
              <PaymentHistoryLine
                // singlePreview
                {...props}
                paymentList={data.paymentList}
              />
              <CardItem>
                <View
                  style={{
                    width: '100%',
                    height: 1,
                    borderBottomWidth: 0.5,
                    borderBottomColor: greyLine,
                  }}
                />
              </CardItem>
            </Card>
          )}
          <PaymentDetail label={'PAYMENT DETAILS'} orderDetailData={data} />
        </Content>
      </Container>
    );
  }
};

const Wrapper = compose(withApollo)(PaymentHistory);

export default props => <Wrapper {...props} />;
