import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Dimensions,
  Modal,
  Image,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import AsyncImage from '../../components/Image/AsyncImage';
import GetListCardQuery from '../../graphql/queries/getPaymentMethod';
import {creditCard, charImage} from '../../utils/Themes/Images';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import Loader from '../../components/Loader/circleLoader';
import ButtonBack from '../../components/Button/buttonBack';
import CardIcons from '../../components/CreditCards/Icons';
import Button from '../../components/Button/buttonFixToBottom';
import CreatePaymentMutation from '../../graphql/mutations/createPaymentMobile';
import {CommonActions} from '@react-navigation/native';
import {RFPercentage} from 'react-native-responsive-fontsize';
import FloristPaymentAPI from '../../graphql/mutations/productPaymentMobile';

const {jcb} = CardIcons;
const {mastercard, visa, amex, discover, unknownCard} = creditCard;
const {
  black,
  greyLine,
  newContainerColor,
  overlayDim,
  white,
  mainGreen,
} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;
const {charGreenCheckedTimeLine} = charImage;
const {width, height} = Dimensions.get('window');

export const LoaderPayment = props => {
  const {isPayNowLoading, isSuccessPay, isFailedPayNow} = props;
  return (
    <>
      <Modal visible={isPayNowLoading} transparent>
        <View
          style={{
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            zIndex: 999,
            backgroundColor: overlayDim,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 100,
              width: 100,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              borderRadius: 10,
              flexDirection: 'column',
            }}>
            {isPayNowLoading && !isSuccessPay && !isFailedPayNow ? (
              <ActivityIndicator
                style={{top: 5}}
                animating
                size="large"
                color="black"
              />
            ) : isPayNowLoading && isSuccessPay && !isFailedPayNow ? (
              <Icon
                type="Feather"
                name="check"
                style={{top: 9, fontSize: 30, color: '#44D363'}}
              />
            ) : (
              <Icon
                type="Feather"
                name="x"
                style={{top: 9, fontSize: 30, color: 'red'}}
              />
            )}
            <Text
              style={{
                top: 10,
                fontFamily: book,
                fontSize: regular,
                color: black,
                marginVertical: 10,
                textAlign: 'center',
              }}>
              {isPayNowLoading && !isSuccessPay && !isFailedPayNow
                ? 'Verifying'
                : isPayNowLoading && isSuccessPay && !isFailedPayNow
                ? 'Payment Success'
                : 'Payment Failed'}
            </Text>
          </View>
        </View>
      </Modal>
    </>
  );
};

export const AddCreditCard = ({onPress}) => {
  return (
    <CardItem
      button
      onPress={() => {
        onPress();
      }}
      style={{flexDirection: 'column'}}>
      <View style={{width: '100%', flexDirection: 'row'}}>
        <View style={{flex: 0.25}}>
          <View
            style={{
              height: 31,
              width: 49,
              aspectRatio: 49 / 31,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: black,
              borderStyle: 'dashed',
            }}>
            <Text>+</Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
            }}>
            Add New Card
          </Text>
        </View>
      </View>
    </CardItem>
  );
};

const SelectPaymentMethod = props => {
  console.log('SelectPaymentMethod Props: ', props);
  const {route, navigation, client} = props;
  const {params} = route;
  const {amount, description, orderId, paymentType} = params;

  const [listCard, setlistCard] = useState([]);
  const [isLoading, setIsloading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isRefetch, setIsRefetch] = useState(false);

  const [isPayNowLoading, setIsPayNowLoading] = useState(false);
  const [isSuccessPay, setIsSuccessPay] = useState(true);
  const [isSuccessPayMsg, setIsSuccessPayMsg] = useState('');
  const [isFailedPayNow, setIsFailedPayNow] = useState(false);
  const [isFailedPayNowMsg, setIsFailedPayNowMsg] = useState('');

  useEffect(() => {
    settingStatusBar();
    navigationOptions();
    fetchCardList();

    const subscriber = navigation.addListener('focus', () => {
      // settingStatusBar();
      // navigationOptions();
      // fetchCardList();
      refetchCardList();
    });

    return subscriber;
  }, [navigation]);

  const refetchCardList = async () => {
    setIsRefetch(true);
    fetchCardList();
  };

  const fetchCardList = () => {
    try {
      client
        .query({
          query: GetListCardQuery,
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(response => {
          console.log('Response Fetch Card List: ', response);
          const {data} = response;
          const {getPaymentMethods, errors} = data;

          if (errors === undefined) {
            const {data: cards, error} = getPaymentMethods;
            if (error === null) {
              setlistCard([...cards]);
              // setlistCard([]);
              setIsError(false);
              setIsRefetch(false);
              setIsloading(false);
            } else if (error === 'Payment method not found!') {
              setIsError(false);
              setIsRefetch(false);
              setIsloading(false);
            } else {
              setIsError(true);
              setIsRefetch(false);
              setIsloading(false);
            }
          } else {
            setIsError(true);
            setIsRefetch(false);
            setIsloading(false);
          }
        })
        .catch(error => {
          console.log('Error Query Fetch Card List: ', error);
          setIsError(true);
          setIsRefetch(false);
          setIsloading(false);
        });
    } catch (error) {
      console.log('Error Fetch Card List: ', error);
      setIsError(true);
      setIsRefetch(false);
      setIsloading(false);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Select Payment Method',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack iconX {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const onSelectingCard = async (_, index) => {
    try {
      let newListCard = listCard;
      const manipulatedOfSelectedCard = await Promise.all(
        newListCard.map((d, i) => {
          if (i === index) {
            return {
              ...d,
              isPrimary: true,
            };
          } else {
            return {
              ...d,
              isPrimary: false,
            };
          }
        }),
      );

      console.log('manipulatedOfSelectedCard: ', manipulatedOfSelectedCard);

      if (manipulatedOfSelectedCard.length === listCard.length) {
        await setlistCard(manipulatedOfSelectedCard);
      }
    } catch (error) {
      console.log('Error On Selecting Card: ', error);
    }
  };

  const onPayNow = async () => {
    try {
      await setIsPayNowLoading(true);
      await setIsSuccessPay(false);
      await setIsSuccessPayMsg('');
      await setIsFailedPayNow(false);
      await setIsFailedPayNowMsg('');

      console.log('LIST CARD READY TO PAY: ', listCard);
      const isSelectedCardId = await listCard.filter(cardDetail => {
        if (cardDetail.isPrimary) {
          return cardDetail.id;
        }
      });
      console.log('isSelectedCardId: ', isSelectedCardId);

      if (isSelectedCardId) {
        const {id: cardId} = isSelectedCardId[0];
        const mutationParams =
          paymentType === 'Florist'
            ? {
                cardId: String(cardId),
                amount: parseFloat(amount),
                description: String(description ? description : ''),
              }
            : {
                orderId: parseInt(orderId, 10),
                cardId: String(cardId),
                amount: parseFloat(amount),
                description: String(description ? description : ''),
              };

        console.log('mutationParams >>> ', mutationParams);

        if (paymentType === 'Florist' && paymentType) {
          // FLORIST
          await client
            .mutate({
              mutation: FloristPaymentAPI,
              variables: mutationParams,
              fetchPolicy: 'no-cache',
            })
            .then(async response => {
              console.log('Payment Response: ', response);
              const {data, errors} = response;
              const {productPaymentMobile} = data;
              const {data: result, error} = productPaymentMobile;
              if (errors) {
                setIsSuccessPay(false);
                setIsSuccessPayMsg('');
                setIsFailedPayNow(true);
                setIsFailedPayNowMsg('Payment Failed');

                setTimeout(async () => {
                  await setIsFailedPayNow(false);
                  await setIsFailedPayNowMsg('');
                  await setIsPayNowLoading(false);
                }, 3000);
              } else {
                if (!error) {
                  await setIsSuccessPay(true);
                  await setIsSuccessPayMsg('Payment Success');
                  await setIsFailedPayNow(false);
                  await setIsFailedPayNowMsg('');

                  setTimeout(async () => {
                    await setIsSuccessPay(false);
                    await setIsSuccessPayMsg('');
                    await setIsPayNowLoading(false);

                    // await navigation.dispatch(
                    //   CommonActions.navigate({
                    //     name: 'Florist',
                    //   }),
                    // );
                    if (props?.route?.params?.from) {
                      await navigation.pop(3);
                    } else {
                      await navigation.pop(2);
                    }
                  }, 3000);
                } else {
                  setIsSuccessPay(false);
                  setIsSuccessPayMsg('');
                  setIsFailedPayNow(true);
                  setIsFailedPayNowMsg('Payment Failed');

                  setTimeout(async () => {
                    await setIsFailedPayNow(false);
                    await setIsFailedPayNowMsg('');
                    await setIsPayNowLoading(false);
                  }, 3000);
                }
              }
            })
            .catch(error => {
              console.log('Error Payment: ', error);
              setIsSuccessPay(false);
              setIsSuccessPayMsg('');
              setIsFailedPayNow(true);
              setIsFailedPayNowMsg('Payment Failed');

              setTimeout(async () => {
                await setIsFailedPayNow(false);
                await setIsFailedPayNowMsg('');
                await setIsPayNowLoading(false);
              }, 3000);
            });
          // FLORIST
        } else {
          await client
            .mutate({
              mutation: CreatePaymentMutation,
              variables: mutationParams,
            })
            .then(async response => {
              console.log('Payment Response: ', response);
              const {data} = response;
              const {createPaymentMobile} = data;
              const {data: result, error} = createPaymentMobile;
              if (error === null) {
                await setIsSuccessPay(true);
                await setIsSuccessPayMsg('Payment Success');
                await setIsFailedPayNow(false);
                await setIsFailedPayNowMsg('');

                setTimeout(async () => {
                  await setIsSuccessPay(false);
                  await setIsSuccessPayMsg('');
                  await setIsPayNowLoading(false);

                  navigation.dispatch(
                    CommonActions.navigate({
                      name: 'Payment',
                    }),
                  );
                }, 3000);
              } else {
                await setIsSuccessPay(false);
                await setIsSuccessPayMsg('');
                await setIsFailedPayNow(true);
                await setIsFailedPayNowMsg(error);

                setTimeout(async () => {
                  await setIsFailedPayNow(false);
                  await setIsFailedPayNowMsg('');
                  await setIsPayNowLoading(false);
                }, 3000);
              }
            })
            .catch(error => {
              console.log('Error Payment: ', error);
              setIsSuccessPay(false);
              setIsSuccessPayMsg('');
              setIsFailedPayNow(true);
              setIsFailedPayNowMsg('Payment Failed');

              setTimeout(async () => {
                await setIsFailedPayNow(false);
                await setIsFailedPayNowMsg('');
                await setIsPayNowLoading(false);
              }, 3000);
            });
        }
      } else {
        await setIsSuccessPay(false);
        await setIsSuccessPayMsg('');
        await setIsFailedPayNow(true);
        await setIsFailedPayNowMsg(
          'Something gone wrong!, you will not be charged',
        );

        setTimeout(async () => {
          await setIsSuccessPay(false);
          await setIsSuccessPayMsg('');
          await setIsFailedPayNow(false);
          await setIsFailedPayNowMsg('');
          await setIsPayNowLoading(false);
        }, 3000);
      }
    } catch (error) {
      console.log('TRY CATCH ERROR: ', error);
      setIsSuccessPay(false);
      setIsSuccessPayMsg('');
      setIsFailedPayNow(true);
      setIsFailedPayNowMsg('Something gone wrong!, you will not be charged');

      setTimeout(async () => {
        await setIsSuccessPay(false);
        await setIsSuccessPayMsg('');
        await setIsFailedPayNow(false);
        await setIsFailedPayNowMsg('');
        await setIsPayNowLoading(false);
      }, 3000);
    }
  };

  if (isLoading === false && isError === true) {
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
          fetchCardList();
        }}
      />
    );
  } else if (isLoading === true && isError === false) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else {
    return (
      <Container style={{backgroundColor: white}}>
        <LoaderPayment
          isPayNowLoading={isPayNowLoading}
          isSuccessPay={isSuccessPay}
          isFailedPayNow={isFailedPayNow}
        />
        <Content
          refreshControl={
            <RefreshControl
              refreshing={isRefetch}
              onRefresh={refetchCardList}
            />
          }>
          <Card
            transparent
            style={{
              marginLeft: 0,
              marginRight: 0,
              marginTop: 15,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <FlatList
              scrollEnabled={false}
              data={listCard}
              extraData={listCard}
              listKey={(item, index) => `${index} CC`}
              keyExtractor={(item, index) => `${index} CC`}
              ListFooterComponent={() => {
                if (listCard.length > 0) {
                  return (
                    <AddCreditCard
                      onPress={() => {
                        navigation.navigate('AddNewCard', {paymentType});
                      }}
                    />
                  );
                } else {
                  return null;
                }
              }}
              ListEmptyComponent={() => {
                return (
                  <AddCreditCard
                    onPress={() => {
                      navigation.navigate('AddNewCard', {paymentType});
                    }}
                  />
                );
              }}
              renderItem={({item, index}) => {
                const logoSource =
                  item.brand === 'Visa'
                    ? visa
                    : item.brand === 'MasterCard'
                    ? mastercard
                    : item.brand === 'American Express'
                    ? amex
                    : item.brand === 'Discover'
                    ? discover
                    : item.brand === 'JCB'
                    ? jcb
                    : unknownCard;
                return (
                  <CardItem
                    button
                    onPress={() => onSelectingCard(item, index)}
                    style={{flexDirection: 'column'}}>
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                      }}>
                      <View style={{flex: 0.3}}>
                        <AsyncImage
                          style={{
                            flex: 1,
                            width: 49,
                            height: 31,
                            aspectRatio: 49 / 31,
                            borderRadius: 2,
                            backgroundColor: '#F7F7F7',
                          }}
                          resizeMode="contain"
                          loaderStyle={{
                            width: 49 / 1.5,
                            height: 31 / 1.5,
                          }}
                          source={logoSource}
                          placeholderColor={newContainerColor}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}>
                        <Text style={{top: 3.3}}>**** **** ****</Text>
                        <Text
                          style={{
                            top: 2,
                            fontFamily: book,
                            fontSize: regular,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {` ${item.last4}`}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'flex-end',
                        }}>
                        {item.isPrimary ? (
                          // <Icon
                          //   type="Feather"
                          //   name="check"
                          //   style={{
                          //     right: -10,
                          //     fontSize: 15,
                          //     color: black,
                          //   }}
                          // />
                          <View style={{width: 25, height: 25, top: 2}}>
                            <Image
                              source={charGreenCheckedTimeLine}
                              style={{width: 20, height: 20}}
                              resizeMode="contain"
                            />
                          </View>
                        ) : null}
                      </View>
                    </View>
                    <View
                      style={{
                        position: 'absolute',
                        bottom: 0,
                        borderBottomWidth: 0.5,
                        borderBottomColor: greyLine,
                        height: 1,
                        width: '100%',
                      }}
                    />
                  </CardItem>
                );
              }}
            />
          </Card>
        </Content>
        {/* <Button
          disable={isPayNowLoading}
          isLoading={isPayNowLoading}
          text={'PAY NOW'}
          onPress={() => {
            onPayNow();
          }}
        /> */}
        <ButtonMakePayment
          onPress={onPayNow}
          isPayNowLoading={isPayNowLoading}
        />
      </Container>
    );
  }
};

export const ButtonMakePayment = props => {
  const {isPayNowLoading, onPress} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            disabled={isPayNowLoading ? true : false}
            onPress={() => onPress()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            {isPayNowLoading ? (
              <ActivityIndicator
                color={white}
                size="large"
                style={{marginRight: 5}}
              />
            ) : null}
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              MAKE PAYMENT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(SelectPaymentMethod);

export default props => <Wrapper {...props} />;
