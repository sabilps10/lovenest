/**
unpaid = balance
paid = totalPaid
total = order.total
 */

import React, {useEffect, useState} from 'react';
import {View, StatusBar, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content} from 'native-base';
import {connect} from 'react-redux';
import Colors from '../../utils/Themes/Colors';
import GetPaymentListQuery from '../../graphql/queries/getOrderPayments';
import NoLoginYet from '../../components/CommonScreens/NoLoginYetPayment';
import NoTransactionYet from '../../components/CommonScreens/NoTransactionPayment';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {CommonActions} from '@react-navigation/native';
import PaymentCardList from '../../components/Cards/Payments/PaymentcardList.js';
import HeaderPayment from '../../components/Header/Payments/HeaderPaymentCustom';
import DefaultHeader from '../../components/Header/Payments/DefaultHeader';
import LoadingScreen from '../../components/Loader/Payment/PaymentLoader';

const {newContainerColor, white} = Colors;

const Payments = props => {
  console.log('Payments Props: ', props);
  const {navigation, client, paymentTabRef, positionYBottomNav} = props;

  const isCompleted = false;
  const pageNumber = 1;

  const [isLoading, setIsLoading] = useState(true);
  const [noConnection, setNoConnection] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [isPullRefresh, setIsPullRefresh] = useState(false);

  const [paymentList, setPaymentList] = useState([]);
  const [pageSize, setPageSize] = useState(4);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    StatusBar.setTranslucent(false);
    StatusBar.setBarStyle('dark-content');
    StatusBar.setBackgroundColor('white');
    onChangeOpacity(true);
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(false);
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('white');
      onChangeOpacity(true);
      pullToRefresh();
    });

    return subscriber;
  }, [navigation, isLoading]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const pullToRefresh = async () => {
    try {
      await setIsPullRefresh(true);
      await setPageSize(10);
      await fetch();
    } catch (error) {
      console.log('Error Pull To Refresh: ', error);
    }
  };

  const goToLogin = () => {
    try {
      navigation.dispatch(
        CommonActions.navigate({name: 'Login', params: {showXIcon: true}}),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  const settingIsLoadMore = async value => {
    try {
      await setIsLoadMore(value);
      let oldPageSize = pageSize;
      let newPageSize = oldPageSize + 10;
      await setPageSize(newPageSize);
      await fetch();
    } catch (error) {
      console.log('Setting Loader Active Error: ', error);
    }
  };

  const fetch = async () => {
    try {
      client
        .query({
          query: GetPaymentListQuery,
          variables: {
            pageNumber: pageNumber,
            pageSize: pageSize,
            isCompleted: isCompleted,
          },
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Response GetPaymentListQuery: ', response);
          const {data} = response;
          const {getOrderPayments, errors} = data;
          if (errors === undefined) {
            const {data: payment, error, totalCount: count} = getOrderPayments;
            if (error === null) {
              await setPaymentList([...payment]);
              // await setPaymentList([]);
              await setTotalCount(count);
              await setNoConnection(false);
              await setIsLogin(true);
              await setIsLoadMore(false);
              await setIsPullRefresh(false);
              await setIsLoading(false);
            } else {
              await setNoConnection(false);
              await setIsLogin(false);
              await setIsLoadMore(false);
              await setIsPullRefresh(false);
              await setIsLoading(false);
            }
          } else {
            await setNoConnection(true);
            await setIsLogin(false);
            await setIsLoadMore(false);
            await setIsPullRefresh(false);
            await setIsLoading(false);
          }
        })
        .catch(async error => {
          console.log('Error GetPaymentListQuery: ', error);
          await setNoConnection(true);
          await setIsLogin(false);
          await setIsLoadMore(false);
          await setIsPullRefresh(false);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setNoConnection(true);
      await setIsLogin(false);
      await setIsLoadMore(false);
      await setIsPullRefresh(false);
      await setIsLoading(false);
    }
  };

  if (
    isLoading === true &&
    isLogin === false &&
    noConnection === false &&
    paymentList.length === 0
  ) {
    // show loader
    return <LoadingScreen />;
  } else if (
    isLoading === false &&
    isLogin === true &&
    noConnection === false &&
    paymentList.length === 0
  ) {
    // show no transaction
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <HeaderPayment
          leftButton={() => {
            navigation.navigate('PaymentHistoryList');
          }}
          rightButton={() => {
            navigation.navigate('PaymentMethod');
          }}
          Title="Payment"
        />
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 16,
            paddingBottom: 16,
          }}>
          <NoTransactionYet
            titleText="No Payments yet."
            subText="Your payments will appear here once you've made purchase. Login to view and manage them with ease."
          />
        </Content>
      </Container>
    );
  } else if (
    isLoading === false &&
    isLogin === false &&
    noConnection === false &&
    paymentList.length === 0
  ) {
    // show login screen
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <DefaultHeader Title="Payment" />
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 16,
            paddingBottom: 16,
          }}>
          <NoLoginYet
            titleText="No Payments yet."
            subText="Your payments will appear here once you've made purchase. Login to view and manage them with ease."
            onPress={() => {
              // Login
              goToLogin();
            }}
          />
        </Content>
      </Container>
    );
  } else if (
    isLoading === false &&
    isLogin === true &&
    noConnection === false &&
    paymentList.length > 0
  ) {
    return (
      <Container style={{backgroundColor: white}}>
        <HeaderPayment
          leftButton={() => {
            navigation.navigate('PaymentHistoryList');
          }}
          rightButton={() => {
            navigation.navigate('PaymentMethod');
          }}
          Title="Payment"
        />
        <View style={{flex: 1}}>
          <PaymentCardList
            onChangeOpacity={e => onChangeOpacity(e)}
            paymentTabRef={paymentTabRef}
            paymentList={paymentList}
            pullToRefresh={() => pullToRefresh()}
            isPullRefresh={isPullRefresh}
            pageSize={pageSize}
            totalCount={totalCount}
            settingIsLoadMore={e => settingIsLoadMore(e)}
            isLoadMore={isLoadMore}
            navigation={navigation}
          />
        </View>
      </Container>
    );
  } else {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <DefaultHeader Title="Payment" />
        <ErrorScreen
          message="Refresh"
          onPress={async () => {
            // refetch function
            await setIsLoading(true);
            await fetch();
          }}
        />
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(Payments);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
