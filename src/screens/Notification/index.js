import React, {useEffect} from 'react';
import {Platform, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonBack from '../../components/Button/buttonBack';
import {FontType, FontSize} from '../../utils/Themes/Fonts';

// Screen Tab
import News from './News';
import Appointments from './Appointments';
import Orders from './OrderNotification';

const Tab = createMaterialTopTabNavigator();
const {mainRed, black, greyLine} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const Notification = props => {
  console.log('Notification Props: ', props);
  const {navigation, positionYBottomNav} = props;

  useEffect(() => {
    navigationOptions();

    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return () => {
      subscriber();
    };
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const popStacking = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Notifications',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.2,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  return (
    <Tab.Navigator
      lazy
      initialRouteName="News"
      tabBarPosition="top"
      tabBarOptions={{
        upperCaseLabel: false,
        indicatorStyle: {
          backgroundColor: mainRed,
        },
        labelStyle: {
          fontFamily: medium,
          letterSpacing: 0.25,
          fontSize: RFPercentage(1.6),
          textTransform: 'none',
        },
        activeTintColor: mainRed,
        inactiveTintColor: greyLine,
        allowFontScaling: true,
      }}>
      <Tab.Screen
        name="Updates"
        // component={News}
        options={{tabBarLabel: 'Updates'}}
        children={childProps => <News {...props} {...childProps} />}
      />
      <Tab.Screen
        name="Appointments"
        // component={Appointments}
        options={{tabBarLabel: 'Appointments'}}
        children={childProps => <Appointments {...props} {...childProps} />}
      />
      <Tab.Screen
        name="Orders"
        // component={Orders}
        options={{tabBarLabel: 'Orders'}}
        children={childProps => <Orders {...props} {...childProps} />}
      />
    </Tab.Navigator>
  );
};

const Wrapper = compose(withApollo)(Notification);

export default props => <Wrapper {...props} />;
