import React from 'react';
import {Text, Image, Dimensions, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {hasNotch} from 'react-native-device-info';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import {commonImage} from '../../utils/Themes/Images';

const {black} = Colors;
const {medium, book} = FontType;
const {height} = Dimensions.get('window');
const {BigLNLogo} = commonImage;

const LNNotifScreen = props => {
  console.log('LNNotifScreen Props: ', props);
  const {navigation, route} = props;
  const {params} = route;
  const {title, description, source, type} = params;
  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };
  return (
    <Container>
      <Button
        onPress={() => goBack()}
        style={{
          elevation: 0,
          shadowOpacity: 0,
          backgroundColor: 'transparent',
          position: 'absolute',
          zIndex: 10,
          right: 15,
          top:
            Platform.OS === 'ios'
              ? hasNotch()
                ? 45
                : 25
              : hasNotch()
              ? 35
              : 25,
          alignSelf: 'flex-end',
          height: 45,
          width: 45,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon
          type="Feather"
          name="x"
          style={{width: 25, height: 25, color: black}}
        />
      </Button>
      <Content contentContainerStyle={{paddingTop: 55}}>
        <Card transparent>
          <CardItem header style={{marginBottom: 15}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(2.5),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {title}
            </Text>
          </CardItem>
          <CardItem cardBody>
            {type === 'Promo' ? (
              <Image
                source={
                  !source?.promoImageDynamicURL && source?.promoImageURL
                    ? BigLNLogo
                    : source?.promoImageDynamicURL
                    ? {uri: `${source.promoImageDynamicURL}=h500`}
                    : {uri: source.promoImageURL}
                }
                style={{flex: 1, width: null, height: height / 2.5}}
                resizeMode="contain"
              />
            ) : (
              <Image
                source={BigLNLogo}
                style={{flex: 1, width: null, height: height / 2.5}}
                resizeMode="contain"
              />
            )}
          </CardItem>
          <CardItem style={{marginTop: 25}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(2.1),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {description}
            </Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(LNNotifScreen);

export default props => <Wrapper {...props} />;
