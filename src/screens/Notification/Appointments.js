import React, {useState, useEffect, useMemo, useRef} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  Image,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {Container, Card, CardItem, Icon, Button} from 'native-base';
import {
  charImage,
  commonImage,
  NotificationIcon,
  BigIconNotification,
} from '../../utils/Themes/Images';
import moment from 'moment';
import _ from 'lodash';

// Query
import FETCH_APPOINTMENT_NOTIF from '../../graphql/queries/notificationsMobile';

import CardAppointmentRescheduled from '../../components/Notification/AppointmentTabCards/CardRescheduled';
import CardAppointmentConfirmed from '../../components/Notification/AppointmentTabCards/CardConfirmed';
import CardAppointmentReminder from '../../components/Notification/AppointmentTabCards/CardReminder';
import CardAppointmentUpdate from '../../components/Notification/AppointmentTabCards/CardUpdate';
import FilterList from '../../components/Notification/AppointmentTabCards/FilterList';

const {charNoNotification} = charImage;
const {BigIconNewsEvent, BigIconNewsPromo} = BigIconNotification;
const {
  InfoIcon,
  PromoIcon,
  ActivityIcon,
  EventIcon,
  RescheduledIcon,
  ConfirmedIcon,
  ReminderIcon,
  UpdateIcon,
} = NotificationIcon;
const {medium, book} = FontType;
const {black, white, greyLine, mainGreen, notifNotReadColor} = Colors;
const {lnLogoNotif} = commonImage;
const {width, height} = Dimensions.get('window');
const widthImgLabel = width / 7;
const heightImgLabel = height / 15.5;

const FILTERLIST = [
  {
    id: 1,
    name: 'Update',
  },
  {
    id: 2,
    name: 'Confirmed',
  },
  {
    id: 3,
    name: 'Rescheduled',
  },
  {
    id: 4,
    name: 'Reminder',
  },
];

const AppointmentTab = props => {
  console.log('AppointmentTab Props: ', props);
  const {client, navigation} = props;
  const listRef = React.useRef();

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);
  const [totalCount, setTotalCount] = useState(0);
  const [pageSize, setPageSize] = useState(30);
  const pageNumber = 0;

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [filterText, setFilterText] = useState('');

  useEffect(() => {
    fetchNotificationAppointment();

    const subscriber = navigation.addListener('tabPress', () => {
      onRefreshing();
    });

    const subcriberWhenBack = navigation.addListener('focus', () => {
      onRefreshing();
    });

    return () => {
      subscriber();
      subcriberWhenBack();
      onLoadMore();
    };
  }, [isLoadMore, onLoadMore, filterText, onRefresh]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        let oldPageSize = pageSize;
        let newPageSize = (oldPageSize += 10);
        await setPageSize(newPageSize);
        await fetchNotificationAppointment();
      }
    } catch (error) {
      console.log('Error onLoadMore: ', error);
      setIsLoadMore(false);
      setPageSize(30);
    }
  };

  const onRefreshing = async () => {
    try {
      await setRefreshing(true);
      await setPageSize(30);
      await fetchNotificationAppointment();
    } catch (error) {
      console.log('Error onRefresh: ', error);
      await setRefreshing(false);
    }
  };

  const fetchNotificationAppointment = () => {
    try {
      console.log('PAGE SIZE ANU: ', pageSize);
      console.log('TOTAL ANU: ', totalCount);
      client
        .query({
          query: FETCH_APPOINTMENT_NOTIF,
          variables: {
            type: 'Appointments',
            filter: filterText,
            pageSize: parseInt(pageSize, 10),
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Response Fetch Appointment Notification: ', response);
          const {data, errors} = response;
          const {notificationsMobile} = data;
          const {
            data: notifList,
            totalCount: total,
            error,
          } = notificationsMobile;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              if(error === 'You are not Authenticated. Please provide token authentication!') {
                await setList([...notifList]);
                await setTotalCount(total);
                await setIsError(false);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              } else {
                await setIsError(true);
                await setIsLoading(false);
                await setRefreshing(false);
                await setIsLoadMore(false);
              }
            } else {
              await setList([...notifList]);
              await setTotalCount(total);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error Fetch Appointment Notification: ', error);
          setIsError(true);
          setIsLoading(false);
          setRefreshing(false);
          setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error Fetch Appointment Notification: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
      setIsLoadMore(false);
    }
  };

  const onRefresh = async () => {
    try {
      await setIsLoading(true);
      await setIsError(false);
      await setPageSize(10);
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoading(false);
      await setIsError(false);
      await setPageSize(30);
    }
  };

  if (isLoading && !isError) {
    return <Loader isLoading={isLoading} />;
  } else if (!isLoading && isError) {
    return <RefreshButton onPress={() => onRefresh()} />;
  } else {
    return (
      <Container>
        <View style={{flex: 1, height: height}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <FilterList
              filterText={filterText}
              list={FILTERLIST}
              onChange={async e => {
                try {
                  if (e === filterText) {
                    await setFilterText('');
                    await setRefreshing(true);
                  } else {
                    await setFilterText(e);
                    await setRefreshing(true);
                  }
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
            />
          </View>
          <List
            {...props}
            listRef={listRef}
            list={list}
            setIsLoadMore={() => setIsLoadMore(true)}
            isLoadMore={isLoadMore}
            refreshing={refreshing}
            onRefreshing={() => onRefreshing()}
            onLoadMore={() => onLoadMore()}
          />
          {list.length > 3 ? (
            <Button
              onPress={() => {
                if (listRef?.current?.scrollToOffset) {
                  listRef.current.scrollToOffset({animated: true, offset: 0});
                }
              }}
              style={{
                backgroundColor: mainGreen,
                position: 'absolute',
                bottom: 55,
                right: 25,
                zIndex: 10,
                alignSelf: 'flex-end',
                height: 35,
                width: 35,
                borderRadius: 35 / 2,
                justifyContent: 'center',
              }}>
              <Icon
                type="Feather"
                name="arrow-up"
                style={{width: 24, height: 24, color: white}}
              />
            </Button>
          ) : null}
        </View>
      </Container>
    );
  }
};

export const List = props => {
  const {
    listRef,
    list,
    setIsLoadMore,
    isLoadMore,
    onLoadMore,
    refreshing,
    onRefreshing,
  } = props;

  const keyExt = (_, index) => `${String(index)} Parent`;

  const RenderList = () => {
    return useMemo(() => {
      return (
        <FlatList
          ref={listRef}
          ListEmptyComponent={() => {
            return <EmptyDataList />;
          }}
          initialNumToRender={15}
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <ActivityIndicator
                  size="small"
                  color={mainGreen}
                  style={{marginVertical: 15}}
                />
              );
            } else {
              return null;
            }
          }}
          refreshControl={
            <RefreshControl
              title={refreshing ? 'Refreshing' : 'Pull to Refresh'}
              onRefresh={() => onRefreshing()}
              refreshing={refreshing}
            />
          }
          onEndReached={({distanceFromEnd}) => {
            if (distanceFromEnd < 0) {
              onLoadMore();
            }
          }}
          onMomentumScrollBegin={() => setIsLoadMore()}
          onEndReachedThreshold={0.1}
          contentContainerStyle={{paddingBottom: 15, paddingTop: 15}}
          legacyImplementation
          disableVirtualization
          data={list}
          extraData={list}
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item}) => {
            // console.log('ITEMS NEWS : ', item);
            if (item.type.toLowerCase() === 'rescheduled') {
              return <CardAppointmentRescheduled item={item} {...props} />;
            } else if (item.type.toLowerCase() === 'confirmed') {
              return <CardAppointmentConfirmed item={item} {...props} />;
            } else if (item.type.toLowerCase() === 'reminder') {
              return <CardAppointmentReminder item={item} {...props} />;
            } else if (item.type.toLowerCase() === 'update') {
              return <CardAppointmentUpdate item={item} {...props} />;
            } else {
              return null;
            }
          }}
        />
      );
    }, [list, isLoadMore, onLoadMore, refreshing, onRefreshing]);
  };

  return RenderList();
};

export const Loader = props => {
  const {isLoading} = props;
  return (
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1, paddingTop: 15}}>
        <ActivityIndicator
          animating={isLoading}
          color={mainGreen}
          size="large"
        />
      </View>
    </Container>
  );
};

export const RefreshButton = props => {
  const {onPress} = props;

  return (
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          onPress={() => onPress()}
          style={{
            backgroundColor: mainGreen,
            alignItems: 'center',
            width: width / 3,
            borderRadius: 20,
          }}>
          <Text
            style={{
              left: 10,
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: white,
            }}>
            Refresh
          </Text>
          <Icon
            type="Feather"
            name="refresh-cw"
            style={{width: 24, color: white}}
          />
        </Button>
      </View>
    </Container>
  );
};

export const EmptyDataList = () => {
  return (
    <Card transparent style={{height: '100%', paddingTop: 50}}>
      <CardItem style={{width: '100%'}}>
        <Image
          source={charNoNotification}
          style={{
            width: width / 1.1,
            height: height / 3,
            // aspectRatio: 1.5,
          }}
        />
      </CardItem>
      <CardItem style={{width: '100%'}}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2.2),
              color: black,
              lineHeight: 20,
              letterSpacing: 0.3,
              textAlign: 'center',
              marginVertical: 15,
            }}>
            No notifications yet.
          </Text>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
              textAlign: 'center',
            }}>
            Your notification will appear here once you've made purchase.
          </Text>
        </View>
      </CardItem>
    </Card>
  );
};

const Wrapper = compose(withApollo)(AppointmentTab);

export default props => <Wrapper {...props} />;
