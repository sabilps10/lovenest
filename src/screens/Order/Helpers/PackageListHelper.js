const manipulatePackageList = orderDetailData => {
  return new Promise(async (resolve, reject) => {
    if (orderDetailData?.packageLists?.length > 0) {
      const newPackageList = await Promise.all(
        orderDetailData.packageLists.map((records, i) => {
          return {
            ...records,
            collapse: false,
          };
        }),
      );

      if (newPackageList.length === orderDetailData.packageLists.length) {
        resolve(newPackageList);
      }
    } else {
      resolve([]);
    }
  });
};

export default manipulatePackageList;
