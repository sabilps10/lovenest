const manipulateInteriorPackageList = orderDetailData => {
  return new Promise(async (resolve, reject) => {
    if (orderDetailData?.interiorPackageList) {
      if (orderDetailData?.interiorPackageList?.length > 0) {
        const manipulate = await Promise.all(
          orderDetailData.interiorPackageList.map((records, i) => {
            return {
              ...records,
              collapse: false,
            };
          }),
        );

        if (manipulate.length === orderDetailData.interiorPackageList.length) {
          resolve(manipulate);
        }
      } else {
        resolve([]);
      }
    } else {
      resolve([]);
    }
  });
};

export default manipulateInteriorPackageList;
