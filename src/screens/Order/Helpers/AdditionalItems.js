const manipulateAdditionalItems = orderDetailData => {
  return new Promise(async (resolve, reject) => {
    if (orderDetailData?.additionalItems) {
      if (orderDetailData?.additionalItems?.length > 0) {
        const manipulate = await Promise.all(
          orderDetailData.additionalItems.map((records, i) => {
            return {
              ...records,
              collapse: false,
            };
          }),
        );

        if (manipulate.length === orderDetailData.additionalItems.length) {
          resolve(manipulate);
        }
      } else {
        resolve([]);
      }
    } else {
      resolve([]);
    }
  });
};

export default manipulateAdditionalItems;
