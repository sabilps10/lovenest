import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  RefreshControl,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Content,
  Footer,
  FooterTab,
} from 'native-base';
import {charImage} from '../../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import moment from 'moment';

// Query
import getorderDetailQuery from '../../../graphql/queries/getOrderDetail';

// Common Component
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';
import Loader from '../../../components/Loader/circleLoader';
import PaymentDetail from '../../../components/Cards/Payments/PaymentDetail';
import PaymentHistory from '../../../components/Cards/Payments/PaymentHistory';
import OrderDetailHeader from '../../../components/Cards/OrderDetail/OrderDetailHeader';

/**
 STATUS PROGRESS
 * InProgress
 * Pending
 * Waiting
 * Completed
 */

const {
  black,
  white,
  mainGreen,
  mainRed,
  overlayGreen,
  overlayRed,
  strongRed,
  strongGReen,
  greyLine,
  superGrey,
} = Colors;
const {regular} = FontSize;
const {charChekcedTimeline, charGreyCheckedTimeLine} = charImage;
const {book, medium, bold} = FontType;
const {width, height} = Dimensions.get('window');

const InteriorProgress = props => {
  console.log('InteriorProgress Props: ', props);
  const {navigation, route, client} = props;
  const {params} = route;
  const {id} = params;

  const [orderDetailData, setOrderDetailData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isPullToRefresh, setPullToRefresh] = useState(false);

  useEffect(() => {
    fetch();
    const subcriber = navigation.addListener('focus', () => {
      fetch();
    });

    return () => {
      subcriber();
    };
  }, []);

  const settingPullToRefresh = async () => {
    try {
      await setPullToRefresh(true);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setPullToRefresh(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: getorderDetailQuery,
          variables: {id},
          fetchPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Response Order Progress CUKK Detail: ', response);
          const {data} = response;
          const {getOrderDetails, errors} = data;
          if (errors === undefined) {
            const {data: orderDetailDataApi, error} = getOrderDetails;
            if (error === null) {
              await setOrderDetailData(orderDetailDataApi);
              await setIsError(false);
              await setIsLoading(false);
              await setPullToRefresh(false);
            } else {
              // refresh screen
              await setIsError(true);
              await setIsLoading(false);
              await setPullToRefresh(false);
            }
          } else {
            // refresh the screen
            await setIsError(true);
            await setIsLoading(false);
            await setPullToRefresh(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setPullToRefresh(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setPullToRefresh(false);
    }
  };

  const goToPrevScreen = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    // return loading Component
    return <LoaderIndicator />;
  } else if (!isLoading && isError) {
    // return Refresh Button Component
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
        }}
      />
    );
  } else {
    // render order detail data and there is no error
    return (
      <Container>
        <View style={{zIndex: 99}}>
          <Headers
            onPress={() => goToPrevScreen()}
            orderNumber={orderDetailData?.number ? orderDetailData.number : ''}
          />
        </View>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={isPullToRefresh}
              onRefresh={settingPullToRefresh}
            />
          }>
          <OrderDetailHeader orderDetailData={orderDetailData} />
          <PaymentDetail
            orderDetailData={orderDetailData}
            label="PAYMENT DETAILS"
          />
          <PaymentHistory
            singlePreview
            {...props}
            orderDetailData={orderDetailData}
            paymentList={orderDetailData.paymentList}
          />
          <ListStages orderDetailData={orderDetailData} {...props} />
        </Content>
        {orderDetailData?.paymentList?.length > 0 ? (
          <ButtonMakePaymentForInteior
            {...props}
            orderDetailData={orderDetailData}
          />
        ) : null}
      </Container>
    );
  }
};

export const ButtonMakePaymentForInteior = props => {
  const {navigation, orderDetailData} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('MakePayment', {
                paymentDetail: {
                  order: orderDetailData,
                  merchant: orderDetailData.merchant,
                  totalPaid: orderDetailData.totalPaid,
                  balance: orderDetailData.balance,
                  paymentList: orderDetailData.paymentList,
                },
              });
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              MAKE PAYMENT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const ListStages = props => {
  const {orderDetailData} = props;
  const keyExt = item => `${item.id} List Stages Parent`;

  const RenderList = () => {
    return useMemo(() => {
      return (
        <FlatList
          legacyImplementation
          disableVirtualization
          scrollEnabled={false}
          data={
            orderDetailData?.interiorStages?.length > 0
              ? orderDetailData.interiorStages
              : []
          }
          extraData={
            orderDetailData?.interiorStages?.length > 0
              ? orderDetailData.interiorStages
              : []
          }
          contentContainerStyle={{
            margin: 15,
            marginTop: 5,
          }}
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item, index}) => {
            return (
              <View style={{flexDirection: 'row', width: '100%'}}>
                <View
                  style={{
                    flex: 0.1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                  }}>
                  <Image
                    source={
                      item?.progress?.length > 0
                        ? charChekcedTimeline
                        : charGreyCheckedTimeLine
                    }
                    style={{width: width / 18, height: height / 18, top: -4}}
                    resizeMode="contain"
                  />
                  {index ===
                  orderDetailData?.interiorStages?.length - 1 ? null : (
                    <View
                      style={{
                        flex: 1,
                        width: 1,
                        borderWidth: 0.8,
                        borderColor:
                          item?.progress?.length > 0 ? mainRed : superGrey,
                        top: -5,
                      }}
                    />
                  )}
                </View>
                <View
                  style={{
                    flex: 1,
                    minHeight: 50,
                    paddingTop: 5,
                    paddingBottom: 5,
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}>
                    <Text
                      style={{
                        fontFamily: bold,
                        fontSize: RFPercentage(2),
                        color: black,
                        letterSpacing: 0.3,
                      }}>
                      {item?.name ? item.name.replace(' ', ' #') : ''}
                    </Text>
                  </View>
                  <View style={{width: '100%', flexDirection: 'column'}}>
                    {item?.progress?.length > 0 ? (
                      <UpdatedProgress
                        progressName={item.name}
                        progress={
                          item?.progress?.length > 0 ? item.progress : []
                        }
                        {...props}
                      />
                    ) : (
                      <Text
                        style={{
                          marginTop: 10,
                          fontFamily: book,
                          textDecorationLine: 'underline',
                          fontSize: RFPercentage(1.9),
                          color: superGrey,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        No progress yet.
                      </Text>
                    )}
                  </View>
                </View>
              </View>
            );
          }}
        />
      );
    }, [orderDetailData]);
  };
  return RenderList();
};

export const UpdatedProgress = props => {
  const {progress, progressName} = props;
  const keyExt = item => `${item.id} Progress`;
  if (progress) {
    return (
      <FlatList
        data={progress}
        extraData={progress}
        keyExtractor={keyExt}
        listKey={keyExt}
        renderItem={({item}) => {
          const date = item?.createdOn
            ? moment(item.createdOn).format('DD MMMM YYYY')
            : 'N/A';

          const colorTextStatus =
            item.status === 'Approved' ? strongGReen : strongRed;

          const backgroundColorStatus =
            item.status === 'Approved' ? overlayGreen : overlayRed;

          return (
            <View style={{width: '100%', marginBottom: 15}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.9),
                    color: superGrey,
                    letterSpacing: 0.3,
                  }}>
                  {date}
                </Text>
                <View
                  style={{
                    marginHorizontal: 10,
                    minHeight: 30,
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: backgroundColorStatus,
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 7,
                    paddingBottom: 5,
                  }}>
                  <Text
                    style={{
                      fontFamily: bold,
                      fontSize: RFPercentage(1.8),
                      color: colorTextStatus,
                      letterSpacing: 0.3,
                    }}>
                    {item?.status ? item.status : 'N/A'}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                }}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.9),
                    color: black,
                    letterSpacing: 0.3,
                    lineHeight: 20,
                  }}>
                  {item?.note ? item.note : 'N/A'}
                </Text>
              </View>
              {item?.gallery?.length > 0 ? (
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate('ProgressGallery', {
                      id: item.id,
                      name: progressName,
                      gallery: item?.gallery,
                    });
                  }}
                  style={{
                    marginTop: 5,
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="image"
                    style={{fontSize: 18, color: mainRed}}
                  />
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.6),
                      color: mainRed,
                      letterSpacing: 0.3,
                      marginHorizontal: 5,
                      textDecorationLine: 'underline',
                    }}>
                    View Album
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View>
          );
        }}
      />
    );
  } else {
    return null;
  }
};

export const Headers = props => {
  const {orderNumber, onPress} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: 'white',
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.4,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          transparent
          onPress={onPress}
          style={{
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          Development Progress
        </Text>
        <Text
          style={{
            lineHeight: 18,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: '#999999',
            letterSpacing: 0.3,
          }}>
          {orderNumber}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

export const LoaderIndicator = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <StatusBar barStyle="dark-content" />
      <Loader />
    </View>
  );
};

const Wrapper = compose(withApollo)(InteriorProgress);

export default props => <Wrapper {...props} />;
