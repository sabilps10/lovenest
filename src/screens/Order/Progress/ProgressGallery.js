import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Modal,
  Dimensions,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

// Mutation
import REVIEW_PROGRESS_SUBMIT from '../../../graphql/mutations/reviewStageProgress';

const {black, white, mainGreen, overlayDim, strongGReen, strongRed} = Colors;
const {book, medium, bold} = FontType;
const {regular} = FontSize;
const {width, height} = Dimensions.get('window');

const ProgressGallery = props => {
  console.log('ProgressGalley Props: ', props);
  const {navigation, client} = props;

  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [isSubmitSuccess, setIsSumbitSuccess] = useState(false);
  const [isSubmitError, setIsSubmitError] = useState(false);

  const goToPrevScreen = () => {
    try {
      StatusBar.setBackgroundColor(white);
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const keyExt = (item, index) => `${item.id} Image Gallery`;

  const onSubmitReview = async () => {
    try {
      await setIsLoadingSubmit(true);
      await setIsSumbitSuccess(false);
      await setIsSubmitError(false);

      const id = props.route.params.id;
      await client
        .mutate({
          mutation: REVIEW_PROGRESS_SUBMIT,
          variables: {
            progressId: parseInt(id, 10),
            status: 'Approve',
          },
        })
        .then(async response => {
          console.log('Response ma bruh: ', response);
          const {data, errors} = response;
          const {reviewStageProgress} = data;
          const {error} = reviewStageProgress;

          if (errors) {
            // error submit
            await setIsLoadingSubmit(false);
            await setIsSubmitError(true);
            setTimeout(async () => {
              await setIsSubmitError(false);
            }, 2000);
          } else {
            if (error) {
              // error submit
              await setIsLoadingSubmit(false);
              await setIsSubmitError(true);
              setTimeout(async () => {
                await setIsSubmitError(false);
              }, 2000);
            } else {
              // success submit
              await setIsLoadingSubmit(false);
              await setIsSumbitSuccess(true);
              setTimeout(async () => {
                await setIsSumbitSuccess(false);
              }, 2000);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsLoadingSubmit(false);
          await setIsSubmitError(true);
          setTimeout(async () => {
            await setIsSubmitError(false);
          }, 2000);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsLoadingSubmit(false);
      await setIsSubmitError(true);
      setTimeout(async () => {
        await setIsSubmitError(false);
      }, 2000);
    }
  };

  return (
    <Container style={{backgroundColor: black}}>
      <Headers
        title={
          props?.route?.params?.name ? props.route.params.name : 'Galleries'
        }
        onPress={() => goToPrevScreen()}
      />
      {isLoadingSubmit ? <SubmittingReviewLoader /> : null}
      {isSubmitSuccess ? <SuccessSubmit /> : null}
      {isSubmitError ? <ErrorSubmit /> : null}
      <FlatList
        legacyImplementation
        disableVirtualization
        contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}
        data={
          props?.route?.params?.gallery?.length > 0
            ? props.route.params.gallery
            : []
        }
        extraData={
          props?.route?.params?.gallery?.length > 0
            ? props.route.params.gallery
            : []
        }
        keyExtractor={keyExt}
        listKey={keyExt}
        renderItem={({item, index}) => {
          const source = item?.dynamicUrl
            ? {uri: item.dynamicUrl}
            : {uri: item.url};
          return (
            <View style={{width: width, height: 200, marginVertical: 15}}>
              <Image
                source={source}
                style={{flex: 1, width: null, height: 200}}
                resizeMode="contain"
              />
            </View>
          );
        }}
      />
      <ButtonReviewProgress
        isLoadingSubmit={isLoadingSubmit}
        onSubmitReview={() => onSubmitReview()}
      />
    </Container>
  );
};

export const SubmittingReviewLoader = props => {
  return (
    <View
      style={{
        width,
        height,
        backgroundColor: overlayDim,
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 99,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: width / 1.5,
          height: height / 5,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator
          size="large"
          color={mainGreen}
          style={{marginVertical: 5}}
        />
        <Text>Submitting Review...</Text>
      </View>
    </View>
  );
};

export const SuccessSubmit = props => {
  return (
    <View
      style={{
        width,
        height,
        backgroundColor: overlayDim,
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 99,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: width / 1.5,
          height: height / 5,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="check"
          style={{fontSize: 35, color: strongGReen}}
        />
        <Text>Successful Submit</Text>
      </View>
    </View>
  );
};

export const ErrorSubmit = props => {
  return (
    <View
      style={{
        width,
        height,
        backgroundColor: overlayDim,
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 99,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: width / 1.5,
          height: height / 5,
          backgroundColor: white,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon
          type="Feather"
          name="x"
          style={{fontSize: 35, color: strongRed}}
        />
        <Text>Failed to Submit</Text>
      </View>
    </View>
  );
};

export const Headers = props => {
  const {onPress, title} = props;

  return (
    <Header
      iosBarStyle="light-content"
      androidStatusBarColor={black}
      style={{
        backgroundColor: black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: white,
        borderBottomWidth: 1,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={onPress}
          transparent
          style={{
            alignSelf: 'flex-start',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: white}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: white,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {title ? title.replace(' ', ' #') : 'Gallery'}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

export const ButtonReviewProgress = props => {
  const {onSubmitReview, isLoadingSubmit} = props;
  return (
    <Footer style={{backgroundColor: black}}>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => onSubmitReview()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            {isLoadingSubmit ? (
              <ActivityIndicator
                size="large"
                color="white"
                style={{marginHorizontal: 5}}
              />
            ) : null}
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              MAKE DEPOSIT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(ProgressGallery);

export default props => <Wrapper {...props} />;
