/* eslint-disable no-alert */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Text,
  StatusBar,
  ScrollView,
  Dimensions,
  Image,
  View,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Platform,
  PermissionsAndroid,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
  Footer,
  FooterTab,
} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {NavigationActions} from '@react-navigation/native';
import {charImage} from '../../utils/Themes/Images';
import QRCode from 'react-native-qrcode-svg';
import RNFetchBlob from 'rn-fetch-blob';
import RNBackgroundDownloader from 'react-native-background-downloader';
import RNFS from 'react-native-fs';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../utils/AsyncstorageDataStructure/index';
import {CommonActions, StackActions} from '@react-navigation/native';
import {connect} from 'react-redux';

// Query
import GET_ORDER_DETAIL from '../../graphql/queries/getOrderDetail';
import GET_ALBUM_PROGRESS from '../../graphql/queries/getCustomerAlbumActivity';
import GET_CUSTOMER from '../../graphql/queries/getCustomer';

// Components
import MerchantBar from '../../components/Cards/OrderDetail/OrderDetailHeader';

const {asyncToken} = AsyncData;
const {charGreyCheckedTimeLine, charChekcedTimeline} = charImage;
const {
  black,
  greyLine,
  mainGreen,
  white,
  mainRed,
  headerBorderBottom,
  disablePaymentText,
  transparent,
  lightSalmon,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

const PhotoAndAlbumProgress = props => {
  console.log('PhotoAndAlbumProgress Props: ', props);
  const {navigation, client, positionYBottomNav} = props;
  const [orderDetail, setOrderDetail] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [listProgress, setListProgress] = useState([]);
  const [isLoadingProgress, setIsLoadingProgress] = useState(true);
  const [isErrorProgress, setIsErrorProgress] = useState(false);

  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetchOrderDetail();
    fetchProgress();
    onChangeOpacity(false);

    if (refreshing) {
      fetchOrderDetail();
      fetchProgress();
    }

    const subscriber = navigation.addListener('focus', () => {
      if (Platform?.OS === 'android') {
        StatusBar.setBarStyle('dark-content');
        StatusBar.setTranslucent(false);
      }
      onChangeOpacity(false);
    });
    return subscriber;
  }, [refreshing]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onRefresh = () => {
    StatusBar.setBarStyle('dark-content');
    StatusBar.setTranslucent(false);
    setRefreshing(true);
  };

  const popStacking = () => {
    try {
      navigation.goBack();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'Home'})],
      });
      this.props.navigation.dispatch(resetAction);
    }
  };

  const fetchOrderDetail = async () => {
    try {
      await props?.client
        ?.query({
          query: GET_ORDER_DETAIL,
          variables: {
            id: props?.route?.params?.orderId,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Order Detail Res: ', res);
          const {data, errors} = res;
          const {getOrderDetails} = data;
          const {data: detail, error} = getOrderDetails;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              await setOrderDetail(detail);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error Fetch Order Detail: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const fetchProgress = async () => {
    try {
      if (props?.route?.params?.orderId) {
        await props?.client
          ?.query({
            query: GET_ALBUM_PROGRESS,
            variables: {
              orderId: parseInt(props?.route?.params?.orderId, 10),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async res => {
            console.log('Album Progress Res: ', res);
            const {data, errors} = res;
            const {getCustomerAlbumActivity} = data;
            const {data: listAlbum, error} = getCustomerAlbumActivity;

            if (errors) {
              await setIsErrorProgress(true);
              await setIsLoadingProgress(false);
              await setRefreshing(false);
            } else {
              if (error) {
                await setIsErrorProgress(true);
                await setIsLoadingProgress(false);
                await setRefreshing(false);
              } else {
                await setListProgress([...listAlbum]);
                await setIsErrorProgress(false);
                await setIsLoadingProgress(false);
                await setRefreshing(false);
              }
            }
          })
          .catch(async error => {
            console.log('Error: ', error);
            await setIsErrorProgress(true);
            await setIsLoadingProgress(false);
            await setRefreshing(false);
          });
      } else {
        await setIsErrorProgress(true);
        await setIsLoadingProgress(false);
        await setRefreshing(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorProgress(true);
      await setIsLoadingProgress(false);
      await setRefreshing(false);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: white}}>
      <Headers
        title={'Photo & Albums'}
        orderNumber={orderDetail?.number ? orderDetail.number : ''}
        onPress={() => popStacking()}
      />
      {isLoading && !isError ? null : !isLoading && isError ? null : (
        <MerchantBar orderDetailData={orderDetail} />
      )}
      <ListProgressActivity
        {...props}
        orderNumber={
          orderDetail?.number ? String(orderDetail.number) : 'qrcode'
        }
        refreshing={refreshing}
        onRefresh={onRefresh}
        list={listProgress}
        isLoading={isLoadingProgress}
        isError={isErrorProgress}
      />
    </View>
  );
};

export const TrackerLine = props => {
  const {item, index} = props;

  if (index === 5) {
    if (item?.isPassInspection || item?.isGoToStore || item?.isCompleted) {
      return (
        <View style={{flex: 0.1, alignItems: 'center'}}>
          <Image
            source={charChekcedTimeline}
            style={{width: width * 0.05, height: width * 0.05}}
            resizeMode="contain"
          />
          <View
            style={{
              flex: 1,
              width: 1,
              borderRightWidth: 1.5,
              borderRightColor: mainRed,
            }}
          />
        </View>
      );
    } else {
      return (
        <View style={{flex: 0.1, alignItems: 'center'}}>
          <Image
            source={charGreyCheckedTimeLine}
            style={{width: width * 0.05, height: width * 0.05}}
            resizeMode="contain"
          />
          <View
            style={{
              flex: 1,
              width: 1,
              borderRightWidth: 1.5,
              borderRightColor: greyLine,
            }}
          />
        </View>
      );
    }
  } else {
    if (item?.isCompleted) {
      return (
        <View style={{flex: 0.1, alignItems: 'center'}}>
          <Image
            source={charChekcedTimeline}
            style={{width: width * 0.05, height: width * 0.05}}
            resizeMode="contain"
          />
          <View
            style={{
              flex: 1,
              width: 1,
              borderRightWidth: 1.5,
              borderRightColor: mainRed,
            }}
          />
        </View>
      );
    } else {
      return (
        <View style={{flex: 0.1, alignItems: 'center'}}>
          <Image
            source={charGreyCheckedTimeLine}
            style={{width: width * 0.05, height: width * 0.05}}
            resizeMode="contain"
          />
          <View
            style={{
              flex: 1,
              width: 1,
              borderRightWidth: 1.5,
              borderRightColor: greyLine,
            }}
          />
        </View>
      );
    }
  }
};

export const ListProgressActivity = props => {
  const {list, isLoading, isError, onRefresh, refreshing, orderNumber} = props;

  const keyExt = useCallback(
    (item, index) => {
      return `${item?.id}`;
    },
    [list],
  );

  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <StepperCard
          navigation={props?.navigation}
          client={props?.client}
          item={item}
          index={index}
          orderNumber={orderNumber}
        />
      );
    },
    [list],
  );

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: '100%',
          paddingTop: 15,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={mainGreen} />
      </View>
    );
  } else if (!isLoading && isError) {
    return (
      <View
        style={{
          flex: 1,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(2),
            color: greyLine,
            textAlign: 'center',
            bottom: 100,
          }}>
          Activity Progress Not Found!
        </Text>
        <TouchableOpacity
          onPress={onRefresh}
          style={{
            padding: 10,
            paddingLeft: 35,
            paddingRight: 35,
            backgroundColor: mainGreen,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            bottom: 50,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: white,
            }}>
            Refresh
          </Text>
        </TouchableOpacity>
      </View>
    );
  } else {
    if (list?.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(2),
              color: greyLine,
              textAlign: 'center',
              bottom: 100,
            }}>
            Activity Progress Not Found!
          </Text>
          <TouchableOpacity
            onPress={onRefresh}
            style={{
              padding: 10,
              paddingLeft: 35,
              paddingRight: 35,
              backgroundColor: mainGreen,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              bottom: 50,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: white,
              }}>
              Refresh
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, width}}>
          <FlatList
            ListEmptyComponent={() => {
              return (
                <View
                  style={{
                    flex: 1,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: width * 1.5,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(2),
                      color: greyLine,
                      textAlign: 'center',
                      bottom: 100,
                    }}>
                    Activity Progress Not Found!
                  </Text>
                  <TouchableOpacity
                    onPress={onRefresh}
                    style={{
                      padding: 10,
                      paddingLeft: 35,
                      paddingRight: 35,
                      backgroundColor: mainGreen,
                      borderRadius: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                      bottom: 50,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: white,
                      }}>
                      Refresh
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            contentContainerStyle={{paddingBottom: 35}}
            scrollEnabled
            data={list}
            extraData={list}
            keyExtractor={keyExt}
            renderItem={renderItem}
          />
        </View>
      );
    }
  }
};

export const StepperCard = props => {
  const {item, index, orderNumber} = props;
  let qrRef = useRef();
  const [isLoadingNavigate, setIsLoadingNavigate] = useState(false);
  const [isLoadingDownload, setIsLoadingDownload] = useState(false);

  useEffect(() => {
    props?.navigation.addListener('blur', () => {
      removeLoadingNavigation();
    });
  }, []);

  const removeLoadingNavigation = () => {
    setIsLoadingNavigate(false);
  };

  const navigateToPhotoBooth = async items => {
    try {
      await setIsLoadingNavigate(true);
      if (items?.imageFolderHash || items?.imageFolderHash !== '') {
        // hashid is available
        const token = await AsyncStorage.getItem(asyncToken);
        if (token) {
          await props?.client
            ?.query({
              query: GET_CUSTOMER,
              fetchPolicy: 'no-cache',
              notifyOnNetworkStatusChange: true,
            })
            .then(async res => {
              const {data, errors} = res;
              const {getCustomer} = data;

              if (errors) {
                await setIsLoadingNavigate(false);
              } else {
                const {name, profileImage, partnerName} = getCustomer[0];

                await props?.navigation.dispatch(
                  CommonActions.navigate({
                    name: 'PhotoBooths',
                    params: {
                      hashId: item?.imageFolderHash,
                      profile: {
                        name,
                        profileImage,
                        partnerName,
                      },
                    },
                  }),
                );
              }
            })
            .catch(async err => {
              console.log('err: ', err);
              await setIsLoadingNavigate(false);
            });
        } else {
          await setIsLoadingNavigate(false);
        }
      } else {
        await setIsLoadingNavigate(false);
      }
    } catch (error) {
      await setIsLoadingNavigate(false);
    }
  };

  const getQRBinary = async () => {
    try {
      await qrRef?.current?.toDataURL(async url => {
        console.log('URL BASE64: ', url);
        if (url) {
          await setIsLoadingDownload(true);
          if (Platform?.OS === 'android') {
            const granted = await PermissionsAndroid.requestMultiple(
              [
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              ],
              {
                title: 'Photo Gallery',
                message:
                  'Love Nest App needs access to your gallery to save the photo ',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
              },
            );
            console.log('granted >>> ', granted);
            if (granted) {
              // console.log('ANUUUUU: ', RNFetchBlob.base64.encode(base64Str));
              let base64Str = url;
              let dirs = await RNFetchBlob.fs.dirs.DownloadDir;
              let path = `${dirs}/${orderNumber}${moment().format(
                'DDMMYYYY',
              )}${moment().format('hhmm')}.png`;
              await RNFetchBlob.fs
                .writeFile(path, base64Str, 'base64')
                .then(async res => {
                  console.log('RESULT >>> ', res);
                  await setIsLoadingDownload(false);
                  await RNFetchBlob.android.actionViewIntent(path, 'image/png');
                })
                .catch(error => {
                  console.log('Error BASE 64 GENERATE: ', error);
                });
            } else {
              await setIsLoadingDownload(false);
            }
          } else {
            // IOS
            let base64Str = url;
            let dir = await RNFS.DocumentDirectoryPath;
            console.log('dir: ', dir);
            let paths = `${dir}/${orderNumber}${moment().format(
              'DDMMYYYY',
            )}${moment().format('hhmm')}.png`;
            console.log('Pathex: ', paths);

            await RNFS.writeFile(paths, base64Str, 'base64')
              .then(async path => {
                console.log('path: ', path);
                await setIsLoadingDownload(false);
                await RNFetchBlob.ios.openDocument(paths);
              })
              .catch(async errorPath => {
                console.log('Error Path: ', errorPath);
                await setIsLoadingDownload(false);
              });
          }
        } else {
          console.log('Cant get QR Data Base64 Format!');
          await setIsLoadingDownload(false);
        }
      });
    } catch (error) {
      console.log('Error Get QR Binary: ', error);
      await setIsLoadingDownload(false);
    }
  };

  return (
    <View
      style={{
        width: '100%',
        borderWidth: 0,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <TrackerLine item={item} index={index} />
      <View style={{flex: 1, paddingTop: 3, paddingLeft: 20, paddingRight: 15}}>
        <Text
          style={{
            marginBottom: 8,
            fontFamily: book,
            color: greyLine,
            fontSize: RFPercentage(1.3),
            textAlign: 'left',
          }}>
          {item?.date || item?.date === '' ? item?.date : 'N/A'}
        </Text>
        <Text
          style={{
            marginBottom: 3,
            fontFamily: medium,
            color: item?.isCompleted ? black : greyLine,
            fontSize: RFPercentage(1.8),
            textAlign: 'left',
          }}>
          {item?.label || item?.label === '' ? item?.label : 'N/A'}
        </Text>
        <Text
          style={{
            marginBottom: 3,
            fontFamily: book,
            color: item?.isCompleted ? black : greyLine,
            fontSize: RFPercentage(1.6),
            textAlign: 'left',
            lineHeight: 18,
          }}>
          {item?.name || item?.name === '' ? item?.name : 'N/A'}
        </Text>
        {item?.remarks && item?.isCompleted ? (
          <View
            style={{
              marginTop: 10,
              backgroundColor: headerBorderBottom,
              borderRadius: 5,
              width: '100%',
              padding: 10,
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}>
            <Text
              style={{
                marginBottom: 3,
                fontFamily: book,
                color: item?.isCompleted ? black : greyLine,
                fontSize: RFPercentage(1.6),
                textAlign: 'left',
                lineHeight: 18,
              }}>
              {item?.remarks ? item?.remarks : ''}
            </Text>
          </View>
        ) : null}
        {item?.isCompleted ? (
          <>
            {item?.albumCollectionDate ? (
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  width: '100%',
                  padding: 1,
                }}>
                <View
                  style={{
                    borderRadius: 5,
                    marginTop: 10,
                    marginBottom: 3,
                    padding: 5,
                    paddingLeft: 10,
                    paddingRight: 10,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      right: 10,
                      marginTop: 10,
                      marginBottom: 3,
                      fontFamily: book,
                      color: item?.isCompleted ? black : greyLine,
                      fontSize: RFPercentage(1.6),
                      textAlign: 'left',
                      lineHeight: 18,
                    }}>
                    Date Collection:{' '}
                  </Text>
                </View>

                <View
                  style={{
                    right: 10,
                    justifyContent: 'center',
                    borderRadius: 5,
                    marginTop: 10,
                    marginBottom: 3,
                    backgroundColor: item?.albumCollectionDate
                      ? headerBorderBottom
                      : 'transparent',
                    padding: 2.5,
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text
                    style={{
                      fontFamily: book,
                      color: item?.isCompleted ? black : greyLine,
                      fontSize: RFPercentage(1.6),
                      textAlign: 'left',
                      lineHeight: 18,
                    }}>
                    {item?.albumCollectionDate
                      ? `${item?.albumCollectionDate}`
                      : ''}
                  </Text>
                </View>
              </View>
            ) : null}
          </>
        ) : null}
        {item?.isCompleted ? (
          <>
            {item?.imageFolderHash || item?.imageFolderHash !== '' ? (
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <View style={{marginVertical: 10, marginTop: 20}}>
                  {item?.imageFolderHash || item?.imageFolderHash === '' ? (
                    <QRCode getRef={qrRef} value={item?.imageFolderHash} />
                  ) : null}
                </View>
                <View>
                  {item?.invoice ? (
                    <Text
                      style={{
                        marginVertical: 5,
                        fontFamily: medium,
                        color: item?.isCompleted ? black : greyLine,
                        fontSize: RFPercentage(1.4),
                        textAlign: 'left',
                      }}>
                      Invoice No: {item?.invoice}
                    </Text>
                  ) : null}
                  {item?.totalImage ? (
                    <Text
                      style={{
                        marginVertical: 5,
                        fontFamily: medium,
                        color: item?.isCompleted ? black : greyLine,
                        fontSize: RFPercentage(1.4),
                        textAlign: 'left',
                      }}>
                      Total Photo(s): {item?.totalImage}
                    </Text>
                  ) : null}
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity
                    onPress={() => {
                      getQRBinary();
                    }}
                    style={{
                      padding: 10,
                      borderRadius: 5,
                      marginTop: 10,
                      backgroundColor: mainGreen,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.7),
                        color: white,
                      }}>
                      Download QR
                    </Text>
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginHorizontal: 10,
                      fontSize: RFPercentage(1.6),
                      top: 3,
                    }}>
                    OR
                  </Text>
                  <TouchableOpacity
                    onPress={() => navigateToPhotoBooth(item)}
                    style={{
                      padding: 10,
                      borderRadius: 5,
                      marginTop: 10,
                      backgroundColor: mainGreen,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.7),
                        color: white,
                      }}>
                      Go To Album
                    </Text>
                    {isLoadingNavigate ? (
                      <ActivityIndicator
                        size="small"
                        color="white"
                        style={{marginLeft: 10}}
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
              </View>
            ) : null}
          </>
        ) : null}
        <View style={{width: '100%', height: width * 0.1, borderWidth: 0}} />
      </View>
    </View>
  );
};

export const Headers = props => {
  const {orderNumber, onPress, title} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: 'white',
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.4,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          transparent
          onPress={onPress}
          style={{
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
        <Text
          style={{
            lineHeight: 18,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: '#999999',
            letterSpacing: 0.3,
          }}>
          {orderNumber}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(
  mapToState,
  mapToDispatch,
)(PhotoAndAlbumProgress);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
