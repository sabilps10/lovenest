import React from 'react';
import {Modal, Dimensions, View, ActivityIndicator} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import ImageViewer from 'react-native-image-zoom-viewer';

const {width, height} = Dimensions.get('window');
const {black} = Colors;

const ModalImageViewer = props => {
  const {onPress, visible, images, index} = props;

  return (
    <Modal
      visible={visible}
      animationType="fade"
      style={{backgroundColor: black}}>
      <View style={{flex: 1}}>
        <ImageViewer
          index={index}
          enablePreload
          useNativeDriver
          enableSwipeDown
          style={{width, height}}
          imageUrls={images}
          onSwipeDown={onPress}
          loadingRender={() => {
            return (
              <View
                style={{
                  padding: 15,
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}>
                <ActivityIndicator size={'large'} color={black} />
              </View>
            );
          }}
          pageAnimateTime={100}
          swipeDownThreshold={100}
          flipThreshold={20}
        />
      </View>
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ModalImageViewer);

export default props => <Wrapper {...props} />;
