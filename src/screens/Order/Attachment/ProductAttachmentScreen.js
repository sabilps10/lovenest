import React, {useEffect} from 'react';
import {Text, View, Modal, StatusBar} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button, Icon} from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import {hasNotch} from 'react-native-device-info'

const ProoductAttachment = props => {
  console.log('ProoductAttachment Props: ', props);
  const {route, navigation} = props;
  const {params} = route;
  const {images} = params;

  useEffect(() => {
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor('black');
    StatusBar.setBarStyle('dark-content');
  }, []);

  const backPrevScreen = () => {
    try {
      StatusBar.setTranslucent(false);
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('white');
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Modal style={{margin: 0}} visible={true} transparent>
      <ImageViewer
        onSwipeDown={backPrevScreen}
        enableSwipeDown
        backgroundColor="black"
        imageUrls={images}
        renderHeader={() => {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 15,
                zIndex: 999,
              }}>
              <Button
                onPress={backPrevScreen}
                transparent
                style={{
                  top: hasNotch() ? 18 : 10,
                  paddingTop: 0,
                  paddingBottom: 0,
                  height: 35,
                  width: 35,
                  justifyContent: 'center',
                  backgroundColor: 'transparent',
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 24,
                    color: 'white',
                  }}
                />
              </Button>
            </View>
          );
        }}
      />
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ProoductAttachment);

export default props => <Wrapper {...props} />;
