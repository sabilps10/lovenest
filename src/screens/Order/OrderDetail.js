import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  Animated,
  RefreshControl,
  Image,
  Dimensions,
  FlatList,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {connect} from 'react-redux';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button,
  Footer,
  FooterTab,
} from 'native-base';

// Query
import getorderDetailQuery from '../../graphql/queries/getOrderDetail';

import Imageviewer from './Attachment/ImageViewer';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {commonImage} from '../../utils/Themes/Images';
import moment from 'moment';
import OrderDetailHeader from '../../components/Cards/OrderDetail/OrderDetailHeader';
import OrderPackageList from '../../components/Cards/OrderPackageList';
import OrderVenueAndHotelPackageList from '../../components/Cards/VenueAndHotelOrderPackageList';
import AdditionalItemList from '../../components/Cards/AdditionalItemList';
import PaymentDetail from '../../components/Cards/Payments/PaymentDetail';
import PaymentHistory from '../../components/Cards/Payments/PaymentHistory';
import EventDetail from '../../components/Cards/Event/EventDetail';
import SuccessToaster from '../../components/Toaster/successToaster';
import {RFPercentage} from 'react-native-responsive-fontsize';
import InteriorPackageList from '../../components/Cards/Orders/InteriorList';
import AsyncImage from '../../components/Image/AsyncImage';

//Helpers
import PackageListHelper from './Helpers/PackageListHelper';
import InteriorPackageListHelper from './Helpers/InteriorPackageList';
import AdditionalItemsHelper from './Helpers/AdditionalItems';

//  special cases
import {NavigationActions} from '@react-navigation/native';
import MapView from '../../components/Map/index';

// Order Florist Stepper
import {OrderStepper, charImage} from '../../utils/Themes/Images';
const {
  orderConfirmedRed,
  orderShippedRed,
  preparingYourOrderRed,
  orderConfirmedGrey,
  orderShippedGrey,
  preparingYourOrderGrey,
  orderAccesptedGrey,
  orderAccesptedRed,
} = OrderStepper;
const {charDelivery, charSelfPickup} = charImage;
const steppersData = [
  {
    id: 1,
    value: 'Order Accepted',
    title: 'Order Confirmed',
    imageGrey: orderConfirmedGrey,
    imageRed: orderConfirmedRed,
  },
  {
    id: 2,
    value: 'Preparing Order',
    title: 'Preparing Your Order',
    imageGrey: preparingYourOrderGrey,
    imageRed: preparingYourOrderRed,
  },
  {
    id: 3,
    value: 'Order Shipped',
    title: 'Order Shipped',
    imageGrey: orderShippedGrey,
    imageRed: orderShippedRed,
  },
  {
    id: 4,
    value: 'Confirmed',
    title: 'Order Accepted',
    imageGrey: orderAccesptedGrey,
    imageRed: orderAccesptedRed,
  },
];

import SubmitConfirmation from '../../graphql/mutations/productOrderStatusConfirmation';

const {
  black,
  greyLine,
  mainGreen,
  white,
  mainRed,
  headerBorderBottom,
  disablePaymentText,
  transparent,
  lightSalmon,
  overlayDim,
} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

const OrderDetail = props => {
  console.log('OrderDetail Props: ', props);
  const {navigation, client, route, positionYBottomNav} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [orderDetailData, setOrderDetailData] = useState(null);
  const [isNoConnection, setIsNoConnection] = useState(false);
  const [isErrorStatus, setIsErrorStatus] = useState(false);

  // eslint-disable-next-line no-unused-vars
  const [, setIsErrorMessage] = useState('');

  const [isPullToRefresh, setPullToRefresh] = useState(false);

  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [isErrorSubmit, setIsErrorSubmit] = useState(false);
  const [isSuccessSubmit, setIsSuccess] = useState(false);

  const [showModalComfimFlorist, setShowModalConfirmFlorist] = useState(false);

  const [showImg, setShowImg] = useState(false);
  const [listPhotos, setListPhotos] = useState([]);
  const [selectedIndexPhotos, setSelectedIndexPhotos] = useState(0);

  useEffect(() => {
    settingStatusBar();
    fetch();
    onChangeOpacity(false);
    const subscriber = navigation.addListener('focus', () => {
      console.log('Subscriber Order Detail');
      settingStatusBar();
      onChangeOpacity(false);
      fetch();
    });
    return subscriber;
  }, [navigation, isLoading, id]);

  const turnOnImageViewer = async (photos, index) => {
    try {
      await setListPhotos([...photos]);
      await setSelectedIndexPhotos(index ? index : 0);
      await setShowImg(!showImg);
    } catch (error) {
      await setShowImg(false);
      await setListPhotos([]);
    }
  };

  const submitConfirmationFloristOrder = async () => {
    try {
      await setIsLoadingSubmit(true);
      await client
        .mutate({
          mutation: SubmitConfirmation,
          variables: {
            orderId: orderDetailData?.id && parseInt(orderDetailData.id, 10),
            orderStatus: 'Confirmed',
          },
        })
        .then(async response => {
          console.log('Response confirmation: ', response);
          const {data, errors} = response;
          const {productOrderStatus} = data;
          const {error} = productOrderStatus;

          if (errors) {
            await setIsLoadingSubmit(false);
            await setShowModalConfirmFlorist(false);
          } else {
            if (error) {
              await setIsLoadingSubmit(false);
              await setShowModalConfirmFlorist(false);
            } else {
              await fetch();
              await setIsLoadingSubmit(false);
              await setShowModalConfirmFlorist(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsLoadingSubmit(false);
          await setShowModalConfirmFlorist(false);
        });
    } catch (error) {
      await setIsLoadingSubmit(false);
      await setShowModalConfirmFlorist(false);
    }
  };

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const settingPullToRefresh = async () => {
    try {
      await setPullToRefresh(true);
      await fetch();
    } catch (error) {
      await setIsLoading(false);
      await setPullToRefresh(false);
      await setIsErrorStatus(true);
    }
  };

  const fetch = async () => {
    try {
      await setIsErrorMessage('');
      await client
        .query({
          query: getorderDetailQuery,
          variables: {id},
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Response Order Detail: ', response);
          const {data} = response;
          const {getOrderDetails, errors} = data;
          if (errors === undefined) {
            const {data: orderDetailDataApi, error} = getOrderDetails;
            if (error === null) {
              const packageLists = await PackageListHelper(orderDetailDataApi);
              orderDetailDataApi.packageLists = packageLists;
              const interiorPackageList = await InteriorPackageListHelper(
                orderDetailDataApi,
              );
              orderDetailDataApi.interiorPackageList = interiorPackageList;
              const additionalItems = await AdditionalItemsHelper(
                orderDetailDataApi,
              );
              orderDetailDataApi.additionalItems = additionalItems;

              console.log('NEW ORDER BRO: ', orderDetailDataApi);

              await setOrderDetailData(orderDetailDataApi);
              await setIsErrorStatus(false);
              await setIsNoConnection(false);
              await setIsLoading(false);
              await setPullToRefresh(false);
            } else {
              // refresh screen
              await setIsErrorMessage(error);
              await setIsErrorStatus(true);
              await setIsNoConnection(false);
              await setIsLoading(false);
              await setPullToRefresh(false);
            }
          } else {
            // refresh the screen
            await setIsErrorMessage('Order not found');
            await setIsErrorStatus(false);
            await setIsNoConnection(true);
            await setIsLoading(false);
            await setPullToRefresh(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorMessage('Order not found');
          await setIsErrorStatus(false);
          await setIsNoConnection(true);
          await setIsLoading(false);
          await setPullToRefresh(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorMessage('Order not found');
      await setIsErrorStatus(false);
      await setIsNoConnection(true);
      await setIsLoading(false);
      await setPullToRefresh(false);
    }
  };

  const settingStatusBar = () => {
    StatusBar.setBarStyle('dark-content');
    StatusBar.setTranslucent(false);
  };

  const popStacking = () => {
    try {
      navigation.goBack();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'Home'})],
      });
      this.props.navigation.dispatch(resetAction);
    }
  };

  const setCollapsePackageList = async index => {
    try {
      console.log('Index collapse >>> ', index);
      let oldData = {...orderDetailData};
      oldData.packageLists[index].collapse =
        !oldData.packageLists[index].collapse;
      await setOrderDetailData(oldData);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const setCollapseAdditionalItem = async index => {
    try {
      let oldData = {...orderDetailData};
      oldData.additionalItems[index].collapse =
        !oldData.additionalItems[index].collapse;

      await setOrderDetailData(oldData);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const setCollapseInteriorPackageList = async index => {
    try {
      let oldData = {...orderDetailData};
      oldData.interiorPackageList[index].collapse =
        !oldData.interiorPackageList[index].collapse;

      await setOrderDetailData(oldData);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (
    isNoConnection === true &&
    isLoading === false &&
    isErrorStatus === false &&
    orderDetailData === null
  ) {
    // refresh button
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
        }}
      />
    );
  } else if (
    isNoConnection === false &&
    isLoading === false &&
    isErrorStatus === true &&
    orderDetailData === null
  ) {
    // show refresh button and error toaster
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
        }}
      />
    );
  } else if (
    isNoConnection === false &&
    isLoading === true &&
    isErrorStatus === false &&
    orderDetailData === null
  ) {
    // show loader
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <StatusBar barStyle="dark-content" translucent={false} />
        <Loader />
      </View>
    );
  } else if (
    isNoConnection === false &&
    isLoading === false &&
    isErrorStatus === false &&
    orderDetailData !== null
  ) {
    // show data
    return (
      <Container style={{backgroundColor: 'white'}}>
        <Imageviewer
          index={selectedIndexPhotos}
          visible={showImg}
          images={listPhotos}
          onPress={(photos, index) => turnOnImageViewer(photos, index)}
        />
        <Modal
          visible={showModalComfimFlorist}
          transparent
          animationType="slide">
          <View
            style={{
              flex: 1,
              backgroundColor: overlayDim,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 15,
            }}>
            <View
              style={{
                width: '100%',
                height: height / 3,
                borderRadius: 5,
                backgroundColor: white,
              }}>
              <View
                style={{
                  paddingLeft: 10,
                  paddingRight: 10,
                  flex: 1,
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: medium,
                    color: black,
                    fontSize: RFPercentage(1.8),
                    letterSpacing: 0.3,
                    lineHeight: 18,
                  }}>
                  Order Confirmed
                </Text>
                <Button
                  onPress={() => setShowModalConfirmFlorist(false)}
                  transparent
                  style={{
                    alignSelf: 'center',
                    paddingTop: 0,
                    paddingBottom: 0,
                    height: 35,
                    width: 35,
                    justifyContent: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: black,
                    }}
                  />
                </Button>
              </View>
              <View
                style={{
                  height: 1,
                  width: '95%',
                  alignSelf: 'center',
                  borderBottomWidth: 1,
                  borderBottomColor: mainRed,
                }}
              />
              <View
                style={{
                  flex: 1.5,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={
                    orderDetailData?.orderType === 'Pickup'
                      ? charSelfPickup
                      : charDelivery
                  }
                  style={{width: width / 4, height: height / 10}}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  flex: 1,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                {orderDetailData?.orderType === 'Pickup' ? (
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(1.5),
                      letterSpacing: 0.3,
                      lineHeight: 18,
                    }}>
                    You already arrived at store and already get your order ?
                    you can confirm it or let our side to confirm it
                  </Text>
                ) : (
                  <Text
                    style={{
                      lineHeight: 18,
                      fontFamily: medium,
                      color: black,
                      fontSize: RFPercentage(1.5),
                      letterSpacing: 0.3,
                      textAlign: 'center',
                    }}>
                    You have received your order ? you can confirm the order or
                    not to confirm it, if you haven't receive it yet
                  </Text>
                )}
              </View>
              <View style={{flex: 1, width: '100%'}}>
                <Button
                  onPress={() => {
                    try {
                      submitConfirmationFloristOrder();
                    } catch (error) {
                      console.log('Error: ', error);
                      setShowModalConfirmFlorist(false);
                    }
                  }}
                  style={{
                    top: 1,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    height: '100%',
                    marginTop: 0,
                    marginBottom: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    backgroundColor: mainGreen,
                    borderRadius: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      color: white,
                      fontSize: RFPercentage(1.8),
                      letterSpacing: 0.3,
                    }}>
                    CONFIRM
                  </Text>
                  {isLoadingSubmit ? (
                    <ActivityIndicator
                      color={white}
                      size="large"
                      style={{marginLeft: 5}}
                    />
                  ) : null}
                </Button>
              </View>
            </View>
          </View>
        </Modal>
        {params.successRegisterEvent ? (
          <View style={{top: 50}}>
            <SuccessToaster text="RSVP Successfully" />
          </View>
        ) : null}
        <View style={{zIndex: 99}}>
          <Headers
            orderNumber={orderDetailData?.number ? orderDetailData.number : ''}
            onPress={() => popStacking()}
          />
        </View>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={isPullToRefresh}
              onRefresh={settingPullToRefresh}
            />
          }
          contentContainerStyle={{paddingBottom: 15}}>
          <OrderDetailHeader orderDetailData={orderDetailData} />

          {/* Steppers */}
          {orderDetailData?.orderType === 'Florist' ? (
            <>
              {orderDetailData?.orderDeliveryType === 'Delivery' ? (
                <Card
                  style={{
                    elevation: 0,
                    shadowOpacity: 0,
                    borderWidth: 0,
                    borderColor: 'white',
                  }}>
                  <CardItem
                    style={{width: '100%', paddingLeft: 0, paddingRight: 0}}>
                    <FlatList
                      contentContainerStyle={{width: '100%'}}
                      horizontal
                      scrollEnabled={false}
                      data={steppersData}
                      extraData={steppersData}
                      keyExtractor={item => `${item.id}`}
                      renderItem={({item, index}) => {
                        return (
                          <View
                            style={{
                              flexBasis: '25%',
                              flexDirection: 'row',
                            }}>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'space-between',
                                alignItems: 'center',
                              }}>
                              <View
                                style={{flexDirection: 'row', width: '100%'}}>
                                <View
                                  style={{
                                    width: '100%',
                                    alignSelf: 'center',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                  }}>
                                  <View
                                    style={{
                                      width: '37%',
                                      height: 1,
                                      borderBottomWidth: 2,
                                      borderBottomColor:
                                        index === 0
                                          ? 'white'
                                          : item.value ===
                                            orderDetailData.stepsStatus[index]
                                          ? mainRed
                                          : greyLine,
                                    }}
                                  />
                                  <Image
                                    source={
                                      item.value ===
                                      orderDetailData.stepsStatus[index]
                                        ? item.imageRed
                                        : item.imageGrey
                                    }
                                    style={{
                                      width: width / 15,
                                      height: height / 15,
                                    }}
                                    resizeMode="contain"
                                  />
                                  <View
                                    style={{
                                      width: '37%',
                                      height: 1,
                                      borderBottomWidth: 2,
                                      borderBottomColor:
                                        index === steppersData.length - 1
                                          ? 'white'
                                          : orderDetailData.stepsStatus[index]
                                          ? mainRed
                                          : greyLine,
                                    }}
                                  />
                                </View>
                              </View>
                              <View
                                style={{
                                  marginTop: 5,
                                  height: 50,
                                  width: '100%',
                                  justifyContent: 'flex-start',
                                  alignItems: 'center',
                                  paddingLeft: 10,
                                  paddingRight: 10,
                                }}>
                                <Text
                                  style={{
                                    lineHeight: 18,
                                    textAlign: 'center',
                                    fontFamily: book,
                                    color: black,
                                    fontSize: RFPercentage(1.5),
                                  }}>
                                  {item.title}
                                </Text>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                    />
                  </CardItem>
                </Card>
              ) : null}

              {orderDetailData?.orderStatus === 'Cart' ? null : (
                <Card
                  style={{
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    elevation: 0,
                    shadowOpacity: 0,
                    borderBottomColor: headerBorderBottom,
                    borderBottomWidth: 1,
                  }}>
                  <CardItem
                    style={{
                      paddingBottom: 0,
                      width: '100%',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(1.8),
                        letterSpacing: 0.3,
                      }}>
                      {orderDetailData?.orderDeliveryType &&
                        orderDetailData.orderDeliveryType.toUpperCase()}{' '}
                      AT
                    </Text>
                  </CardItem>
                  <CardItem style={{width: '100%'}}>
                    <MapView
                      width={70}
                      height={70}
                      forMapAddress={
                        orderDetailData?.orderDeliveryAddress?.address
                      }
                    />
                    <View
                      style={{
                        flexBasis: '75%',
                        height: '100%',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                          bottom: 8,
                        }}>
                        <Image
                          source={
                            orderDetailData?.orderDeliveryType === 'Delivery'
                              ? charDelivery
                              : charSelfPickup
                          }
                          style={{
                            right: 5,
                            width: width / 13,
                            height: height / 28,
                          }}
                          resizeMode="contain"
                        />
                        <Text
                          style={{
                            marginLeft: 5,
                            fontFamily: medium,
                            fontSize: RFPercentage(1.8),
                            color: mainRed,
                            letterSpacing: 0.3,
                          }}>
                          {orderDetailData?.orderDeliveryType}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: '100%',
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.7),
                            lineHeight: 18,
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          {orderDetailData?.orderDeliveryAddress?.address},{' '}
                          {orderDetailData?.orderDeliveryAddressDetail
                            ?.streetAndBlock
                            ? `Block ${orderDetailData?.orderDeliveryAddressDetail?.streetAndBlock}`
                            : ''}{' '}
                          {orderDetailData?.orderDeliveryAddressDetail
                            ?.floorAndAppartment
                            ? `Unit ${orderDetailData?.orderDeliveryAddressDetail?.floorAndAppartment}`
                            : ''}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderTopColor: headerBorderBottom,
                      borderTopWidth: 1,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(1.8),
                        letterSpacing: 0.3,
                      }}>
                      {orderDetailData?.orderDeliveryAddress &&
                        orderDetailData.orderDeliveryType.toUpperCase()}{' '}
                      DATE & TIME
                    </Text>
                  </CardItem>
                  <CardItem style={{paddingTop: 0}}>
                    <View style={{flex: 0.1}}>
                      <Icon
                        type="Feather"
                        name="calendar"
                        style={{fontSize: RFPercentage(2.5), color: black}}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={{
                          fontFamily: medium,
                          color: black,
                          fontSize: RFPercentage(1.7),
                          letterSpacing: 0.3,
                        }}>{`${
                        orderDetailData?.orderDeliveryDate
                          ? moment(orderDetailData.orderDeliveryDate)
                              .utc()
                              .local()
                              .format('dd, MMM DD YYYY')
                          : 'N/A'
                      }, ${
                        orderDetailData?.orderDeliveryTimeslot?.timeslot
                          ? orderDetailData.orderDeliveryTimeslot.timeslot
                          : 'N/A'
                      }`}</Text>
                    </View>
                  </CardItem>
                </Card>
              )}
              <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                <CardItem style={{width: '100%', paddingBottom: 0}}>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        fontFamily: medium,
                        color: '#999999',
                        fontSize: RFPercentage(2),
                        letterSpacing: 0.3,
                      }}>
                      Your Order
                    </Text>
                  </View>
                  <View style={{flex: 0.4}} />
                </CardItem>
                <FlatList
                  disableVirtualization
                  scrollEnabled={false}
                  data={
                    orderDetailData?.productsOrder
                      ? orderDetailData.productsOrder
                      : []
                  }
                  extraData={
                    orderDetailData?.productsOrder
                      ? orderDetailData.productsOrder
                      : []
                  }
                  keyExtractor={item => `${item.id}`}
                  renderItem={({item, index}) => {
                    console.log('KONTOL ITEM: ', item);

                    const {
                      upsize,
                      itemPrice,
                      orderQty,
                      featuredImageDynamicURL,
                      featuredImageURL,
                      discount,
                      floristPriceAfterDiscount,
                    } = item;

                    const source = featuredImageDynamicURL
                      ? {uri: `${featuredImageDynamicURL}=h500`}
                      : {uri: `${featuredImageURL}`};
                    return (
                      <CardItem style={{width: '100%'}}>
                        <View style={{width: '100%'}}>
                          <View style={{width: '100%', flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 0.2,
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                                paddingTop: 3,
                              }}>
                              <View
                                style={{
                                  width: 25,
                                  height: 25,
                                  backgroundColor: '#E8E8E8',
                                  borderRadius: 25 / 2,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    fontSize: RFPercentage(1.4),
                                    color: black,
                                    letterSpacing: 0.3,
                                  }}>
                                  {index + 1}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: 25,
                                  height: 25,
                                  backgroundColor: 'transparent',
                                  borderRadius: 25 / 2,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}
                              />
                            </View>
                            <View style={{flex: 0.5}}>
                              <View style={{width: '100%', height: height / 8}}>
                                <AsyncImage
                                  source={source}
                                  style={{
                                    flex: 1,
                                    height: '100%',
                                    width: '100%',
                                  }}
                                  resizeMode={'cover'}
                                  placeholderColor="white"
                                  loaderStyle={{
                                    width: width / 7,
                                    height: height / 7,
                                  }}
                                />
                              </View>
                            </View>
                            <View
                              style={{
                                flex: 0.5,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                                paddingTop: 3,
                                paddingLeft: 10,
                              }}>
                              <View style={upsize ? {marginBottom: 10} : {}}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {item?.name ? item.name : 'N/A'}
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: book,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {upsize
                                    ? `Upsize: +${upsize.size} Inches`
                                    : null}
                                </Text>
                                {discount === 0 ||
                                discount === null ||
                                discount === undefined ? null : (
                                  <View>
                                    <Text
                                      style={{
                                        fontStyle: 'italic',
                                        fontFamily: book,
                                        color: greyLine,
                                        fontSize: RFPercentage(1.6),
                                        letterSpacing: 0.3,
                                        lineHeight: 18,
                                      }}>
                                      Discount {discount}%
                                    </Text>
                                  </View>
                                )}
                              </View>
                            </View>
                            <View
                              style={{
                                flex: 0.2,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingTop: 3,
                              }}>
                              <View style={{marginBottom: 10}}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {item?.orderQty}
                                </Text>
                              </View>
                              <View />
                            </View>
                            <View
                              style={{
                                flex: 0.4,
                                justifyContent: 'space-between',
                                alignItems: 'flex-end',
                              }}>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {floristPriceAfterDiscount !== 0 ||
                                  floristPriceAfterDiscount !== null ||
                                  floristPriceAfterDiscount !== undefined
                                    ? `$${floristPriceAfterDiscount.toFixed(2)}`
                                    : 'N/A'}
                                </Text>
                                {discount === 0 ||
                                discount === null ||
                                discount === undefined ? null : (
                                  <Text
                                    style={{
                                      textDecorationLine: 'line-through',
                                      fontFamily: medium,
                                      color: greyLine,
                                      fontSize: RFPercentage(1.6),
                                      letterSpacing: 0.3,
                                      lineHeight: 18,
                                    }}>
                                    {itemPrice
                                      ? `$${itemPrice.toFixed(2)}`
                                      : 'N/A'}
                                  </Text>
                                )}
                              </View>
                              <View>
                                {/* <Text
                                  style={{
                                    fontFamily: medium,
                                    color: black,
                                    fontSize: RFPercentage(1.6),
                                    letterSpacing: 0.3,
                                    lineHeight: 18,
                                  }}>
                                  {item?.itemPrice
                                    ? `$${item?.itemPrice.toFixed(2)}`
                                    : 'N/A'}
                                </Text> */}
                              </View>
                            </View>
                          </View>
                          <View
                            style={{
                              borderBottomWidth: 1,
                              borderBottomColor: headerBorderBottom,
                              width: '100%',
                              height: 0.5,
                              marginVertical: 5,
                              marginTop: 15,
                            }}
                          />
                        </View>
                      </CardItem>
                    );
                  }}
                />
              </Card>
              <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                <SpacedCardContent
                  title={'Subtotal'}
                  value={`$${orderDetailData?.subTotal.toFixed(2)}`}
                />
                <SpacedCardContent
                  title={'Taxes'}
                  value={`$${orderDetailData?.gstTax.toFixed(2)}`}
                />
                {orderDetailData?.orderDeliveryType === 'Delivery' ? (
                  <SpacedCardContent
                    title={'Delivery Fee'}
                    value={`$${orderDetailData?.deliveryFee.toFixed(2)}`}
                  />
                ) : null}
                <SpacedCardContent
                  title={'Total'}
                  value={`$${orderDetailData?.total.toFixed(2)}`}
                />
              </Card>

              <>
                {orderDetailData?.orderDeliveryType === 'Delivery' ? (
                  <Card
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      elevation: 0,
                      shadowOpacity: 0,
                    }}>
                    <CardItem style={{width: '100%'}}>
                      <View style={{flex: 1}}>
                        <Text
                          style={{
                            fontFamily: medium,
                            color: '#999999',
                            fontSize: RFPercentage(2),
                            letterSpacing: 0.3,
                          }}>
                          Progress{' '}
                          {orderDetailData?.productOrderStatus?.length === 0
                            ? '(No progress yet)'
                            : null}
                        </Text>
                      </View>
                      <View style={{flex: 0.4}} />
                    </CardItem>
                    <FlatList
                      scrollEnabled={false}
                      data={
                        orderDetailData?.productOrderStatus
                          ? orderDetailData.productOrderStatus
                          : []
                      }
                      extraData={
                        orderDetailData?.productOrderStatus
                          ? orderDetailData.productOrderStatus
                          : []
                      }
                      keyExtractor={(item, index) => `${item.id}`}
                      renderItem={({item, index}) => {
                        return (
                          <CardItem style={{paddingTop: 0, paddingBottom: 0}}>
                            <View
                              style={{
                                flex: 0.1,
                                alignItems: 'center',
                              }}>
                              <Image
                                source={orderConfirmedRed}
                                style={{
                                  bottom: 1,
                                  width: width / 25,
                                  height: height / 25,
                                }}
                                resizeMode="contain"
                              />
                              <View
                                style={{
                                  bottom: 1,
                                  width: 1,
                                  flex: 1,
                                  borderWidth: 1,
                                  borderColor: mainRed,
                                }}
                              />
                            </View>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'space-between',
                                alignItems: 'flex-start',
                                padding: 10,
                                paddingTop: 5,
                              }}>
                              <View>
                                <Text
                                  style={{
                                    fontSize: RFPercentage(1.8),
                                    color: black,
                                    fontFamily: medium,
                                    letterSpacing: 0.3,
                                  }}>
                                  {item?.orderStatus}
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontSize: RFPercentage(1.7),
                                    color: black,
                                    fontFamily: book,
                                    letterSpacing: 0.3,
                                  }}>
                                  {item?.note}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  flexWrap: 'wrap',
                                  justifyContent: 'flex-start',
                                  alignItems: 'center',
                                }}>
                                <Icon
                                  type="Feather"
                                  name="clock"
                                  style={{fontSize: 15, color: mainRed}}
                                />
                                <Text
                                  style={{
                                    fontSize: RFPercentage(1.6),
                                    color: black,
                                    fontFamily: book,
                                    letterSpacing: 0.3,
                                    right: 10,
                                  }}>
                                  {item?.createdOn
                                    ? moment(item.createdOn)
                                        .utc()
                                        .local()
                                        .format('ddd, DD MMM YYYY hh:mm A')
                                    : ''}
                                </Text>
                              </View>
                            </View>
                          </CardItem>
                        );
                      }}
                    />
                  </Card>
                ) : null}
              </>
            </>
          ) : null}

          <EventDetail orderDetailData={orderDetailData} />
          <PaymentDetail
            orderDetailData={orderDetailData}
            label="PAYMENT DETAILS"
          />
          {orderDetailData?.orderType !== 'Florist' ? (
            <PaymentHistory
              singlePreview
              {...props}
              orderDetailData={orderDetailData}
              paymentList={orderDetailData.paymentList}
            />
          ) : null}

          {/* For Bridal Only */}
          {orderDetailData?.orderType === 'Bridal' ? (
            <OrderPackageList
              navigation={props?.navigation}
              orderProps={orderDetailData}
              setCollapsePackageList={setCollapsePackageList}
            />
          ) : null}

          {/* For Venue or Hotel only */}
          {orderDetailData?.orderType === 'Venue' ||
          orderDetailData?.orderType === 'Hotel' ? (
            <OrderVenueAndHotelPackageList
              orderProps={orderDetailData}
              onPressPhoto={(photos, index) => {
                turnOnImageViewer(photos, index);
              }}
              setCollapsePackageList={setCollapsePackageList}
            />
          ) : null}

          {/* Interior Only */}
          {orderDetailData?.orderType === 'Interior' ? (
            <InteriorPackageList
              {...props}
              orderDetailData={orderDetailData}
              setCollapseInteriorPackageList={setCollapseInteriorPackageList}
            />
          ) : null}

          <AdditionalItemList
            orderProps={orderDetailData}
            setCollapseAdditionalItem={setCollapseAdditionalItem}
          />
        </Content>
        {orderDetailData?.orderType === 'Interior Design' ? (
          orderDetailData?.paymentList?.length > 0 ? (
            <ButtonReviewProgress
              id={id}
              orderDetailData={orderDetailData}
              {...props}
            />
          ) : (
            <ButtonMakePaymentForInteior
              id={id}
              orderDetailData={orderDetailData}
              {...props}
            />
          )
        ) : null}
        {orderDetailData?.orderType === 'Florist' ? (
          <ButtonAddToCart
            isConfirmed={
              orderDetailData?.stepsStatus?.length === 4 ? true : false
            }
            isLoading={isLoadingSubmit}
            onPress={() => {
              // submitConfirmationFloristOrder();
              setShowModalConfirmFlorist(true);
            }}
            title={'CONFIRM ORDER'}
            disabled={orderDetailData?.stepsStatus?.length === 4 ? true : false}
          />
        ) : null}
      </Container>
    );
  } else {
    // show error to refresh
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
          settingPullToRefresh();
        }}
      />
    );
  }
};

export const ButtonAddToCart = props => {
  const {onPress, title, disabled, isLoading, isConfirmed} = props;
  return (
    <Footer>
      <FooterTab
        style={{backgroundColor: isConfirmed ? headerBorderBottom : mainGreen}}>
        <View
          style={{
            backgroundColor: isConfirmed ? headerBorderBottom : mainGreen,
            width: '100%',
          }}>
          <TouchableOpacity
            disabled={disabled}
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: isConfirmed ? greyLine : white,
                letterSpacing: 0.3,
              }}>
              {isConfirmed ? 'Confirmed' : `${title}`}
            </Text>
            {isLoading ? (
              <ActivityIndicator
                size="large"
                color="white"
                style={{marginLeft: 10}}
              />
            ) : null}
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const SpacedCardContent = props => {
  const {title, value, forBottom} = props;

  return (
    <CardItem style={{width: '100%', paddingTop: forBottom ? 20 : 0}}>
      <View
        style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(1.8),
            letterSpacing: 0.3,
          }}>
          {value}
        </Text>
      </View>
    </CardItem>
  );
};

export const Headers = props => {
  const {orderNumber, onPress} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor="white"
      style={{
        backgroundColor: 'white',
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.4,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          transparent
          onPress={onPress}
          style={{
            alignSelf: 'flex-end',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: regular,
            color: black,
            letterSpacing: 0.3,
          }}>
          Order Detail
        </Text>
        <Text
          style={{
            lineHeight: 18,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: '#999999',
            letterSpacing: 0.3,
          }}>
          {orderNumber}
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

export const ButtonMakePaymentForInteior = props => {
  const {orderDetailData, navigation} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('MakePayment', {
                paymentDetail: {
                  order: orderDetailData,
                  merchant: orderDetailData.merchant,
                  totalPaid: orderDetailData.totalPaid,
                  balance: orderDetailData.balance,
                  paymentList: orderDetailData.paymentList,
                },
              });
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              MAKE DOWN PAYMENT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const ButtonReviewProgress = props => {
  const {navigation, id} = props;
  const goToProgressScreen = () => {
    try {
      navigation.navigate('InteriorProgress', {
        id,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => goToProgressScreen()}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              REVIEW PROGRESS
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(OrderDetail);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
