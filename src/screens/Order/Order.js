import React, {useState, useEffect, useMemo, useCallback} from 'react';
import {
  Text,
  View,
  FlatList,
  ScrollView,
  StatusBar,
  RefreshControl,
  Dimensions,
  Animated,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import NoLoginYet from '../../components/CommonScreens/NoLoginYetOrder';
import getAllOrdersGroupByDateQuery from '../../graphql/queries/getAllOrdersGroupByDate';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import NoTransactions from '../../components/CommonScreens/NoTransactionOrder';
import OrderList from '../../components/Cards/Orders/OrderList';
import {CommonActions} from '@react-navigation/native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, white, transparent, greyLine, mainRed, headerBorderBottom} =
  Colors;
const {regular} = FontSize;
const {book, medium} = FontType;

const {height} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

const listOrderType = [
  {
    id: 1,
    orderType: 'Event',
  },
  {
    id: 2,
    orderType: 'Bridal',
  },
  {
    id: 3,
    orderType: 'Interior Design',
  },
  {
    id: 4,
    orderType: 'Florist',
  },
  {
    id: 5,
    orderType: 'Venue',
  },
  {
    id: 6,
    orderType: 'Hotel',
  },
];

export const ListOrderTypeFilter = props => {
  const {settingOrderType, orderType, isPullToRefresh} = props;
  return (
    <FlatList
      contentContainerStyle={{
        // marginTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        paddingBottom: 10,
      }}
      showsHorizontalScrollIndicator={false}
      data={listOrderType}
      extraData={listOrderType}
      keyExtractor={(item, index) => `${item.id} OrderType`}
      listKey={(item, index) => `${item.id} OrderType`}
      horizontal
      renderItem={({item, index}) => {
        return (
          <TouchableOpacity
            disabled={isPullToRefresh ? true : false}
            onPress={() => settingOrderType(item.orderType)}
            style={{
              backgroundColor: orderType === item.orderType ? mainRed : 'white',
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginRight: 10,
              borderWidth: 1,
              borderRadius: 25,
              borderColor: mainRed,
              justifyContent: 'center',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 8,
              paddingBottom: 8,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: orderType === item.orderType ? 'white' : mainRed,
                letterSpacing: 0.3,
              }}>
              {item.orderType}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

export const OrderFlatList = props => {
  console.log('OrderFlatList: ', props);
  const {
    orders,
    orderType,
    settingOrderType,
    orderTabRef,
    pullToRefresh,
    isPullToRefresh,
    isLoadMore,
    settingIsLoadMore,
    totalCount,
    initialPageSize,
    onChangeOpacity,
  } = props;

  const keyExt = (item, _) => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderParentList = () => {
    if (orders) {
      return (
        <View style={{flex: 1}}>
          <FlatList
            ListHeaderComponent={() => {
              return (
                <ListOrderTypeFilter
                  isPullToRefresh={isPullToRefresh}
                  orderType={orderType}
                  settingOrderType={type => settingOrderType(type)}
                />
              );
            }}
            onScrollBeginDrag={() => onChangeOpacity(false)}
            onScrollEndDrag={() => onChangeOpacity(true)}
            ref={orderTabRef}
            refreshControl={
              <RefreshControl
                title={isPullToRefresh ? 'Refreshing...' : 'Pull To Refresh'}
                onRefresh={() => pullToRefresh()}
                refreshing={isPullToRefresh}
              />
            }
            contentContainerStyle={{
              paddingTop: 16,
              paddingBottom: 110,
            }}
            ListFooterComponent={() => {
              if (isLoadMore) {
                return (
                  <View
                    style={{
                      width: '100%',
                      height: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 5,
                    }}>
                    <Text>Load More...</Text>
                  </View>
                );
              } else {
                return null;
              }
            }}
            onEndReached={() => {
              if (initialPageSize <= totalCount) {
                settingIsLoadMore(true);
              } else {
                settingIsLoadMore(false);
              }
            }}
            onEndReachedThreshold={0.5}
            getItemLayout={getItemLayout}
            data={orders}
            extraData={orders}
            keyExtractor={keyExt}
            listKey={keyExt}
            renderItem={({item, index}) => {
              const date = item.date;
              const orderListItem = item.listOrder;
              return (
                <Card transparent style={{marginLeft: 0, marginRight: 0}}>
                  <CardItem style={{backgroundColor: transparent}}>
                    <Text
                      style={{
                        top: 5,
                        fontFamily: medium,
                        fontSize: RFPercentage(1.7),
                        color: '#757575',
                        letterSpacing: 0.3,
                        lineHeight: 18,
                        left: 5,
                      }}>
                      {date}
                    </Text>
                  </CardItem>
                  <OrderList
                    orderListItem={orderListItem}
                    parentIndex={index}
                    {...props}
                  />
                </Card>
              );
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  };

  if (orders) {
    return RenderParentList();
  } else {
    return null;
  }
};

export const Headers = props => {
  return (
    <Header
      translucent={false}
      androidStatusBarColor={white}
      iosBarStyle={'dark-content'}
      style={{
        backgroundColor: white,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <Left style={{flex: 0.1}} />
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          Orders
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Order = props => {
  console.log('Order Props: ', props);
  const {navigation, client, orderTabRef, positionYBottomNav} = props;

  const [orderType, setOrderType] = useState('');
  const initialPageNumber = 1;
  const [isLoading, setIsLoading] = useState(true);
  const [orders, setOrder] = useState([]);
  const [noConnection, setNoConnection] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [initialPageSize, setInitialPageSize] = useState(10);
  const [totalCount, setTotalCount] = useState(0);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [isPullToRefresh, setIsPullToRefresh] = useState(false);

  useEffect(() => {
    StatusBarSetting();
    // navigationOptions();
    fetchOrder();
    onChangeOpacity(true);
    const subscriber = navigation.addListener('focus', () => {
      console.log('Subscriber Order');
      StatusBarSetting();
      onChangeOpacity(true);
      pullToRefresh();
    });
    return () => {
      subscriber();
    };
  }, [orderType, pullToRefresh]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goToLogin = () => {
    try {
      navigation.dispatch(
        CommonActions.navigate({name: 'Login', params: {showXIcon: true}}),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  const StatusBarSetting = () => {
    if (Platform?.OS === 'android') {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setTranslucent(false);
    }
    StatusBar.setBarStyle('dark-content');
  };

  const settingIsLoadMore = async value => {
    await setIsLoadMore(value);
    let oldPageSize = initialPageSize;
    let newPageSize = (oldPageSize += 10);
    await setInitialPageSize(newPageSize);
    await fetchOrder();
  };

  const pullToRefresh = async () => {
    StatusBarSetting();
    await setIsPullToRefresh(true);
    await setInitialPageSize(10);
    await fetchOrder();
  };

  const fetchOrder = () => {
    console.log('orderType CUKKKKSSSS: ', orderType);
    client
      .query({
        query: getAllOrdersGroupByDateQuery,
        variables: {
          orderType: String(orderType),
          pageSize: initialPageSize,
          pageNumber: initialPageNumber,
        },
        ssr: false,
        fetchPolicy: 'no-cache',
        notifyOnNetworkStatusChange: true,
      })
      .then(async response => {
        console.log('Response: ', response);
        const {data} = response;
        const {getAllOrdersGroupByDate, errors} = data;

        if (errors === undefined) {
          const {
            data: orderListBaseOnDate,
            totalCount: orderTotalCount,
            error,
          } = getAllOrdersGroupByDate;

          if (error === null) {
            await setOrder([...orderListBaseOnDate]);
            // setOrder([]);
            await setTotalCount(orderTotalCount);
            await setNoConnection(false);
            await setIsLogin(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setIsPullToRefresh(false);
          } else {
            // login card show
            await setNoConnection(false);
            await setIsLogin(false);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setIsPullToRefresh(false);
          }
        } else {
          // refresh
          await setNoConnection(true);
          await setIsLogin(false);
          await setIsLoading(false);
          await setIsLoadMore(false);
          await setIsPullToRefresh(false);
        }
      })
      .catch(async error => {
        console.log('Error: ', error);
        await setNoConnection(true);
        await setIsLogin(false);
        await setIsLoading(false);
        await setIsLoadMore(false);
        await setIsPullToRefresh(false);
      });
  };

  // const navigationOptions = () => {
  //   navigation.setOptions({
  //     headerTitle: 'Orders',
  //     headerTitleAlign: 'center',
  //     headerTitleStyle: {
  //       fontFamily: medium,
  //       fontSize: regular,
  //       color: black,
  //     },
  //     headerStyle: {
  //       borderBottomWidth: 0.5,
  //       borderBottomColor: greyLine,
  //       elevation: 0,
  //       shadowOpacity: 0,
  //     },
  //   });
  // };

  const settingOrderType = async type => {
    try {
      if (orderType === type) {
        await setIsPullToRefresh(true);
        await setOrderType('');
      } else {
        await setIsPullToRefresh(true);
        await setOrderType(type);
      }
    } catch (error) {
      console.log('Erroor: ', error);
    }
  };

  if (
    noConnection === false &&
    isLoading === false &&
    isLogin === true &&
    orders.length === 0
  ) {
    // show no trtansaction card
    return (
      <Container style={{backgroundColor: 'white'}}>
        <StatusBar barStyle="dark-content" translucent={true} />
        <Headers />
        {orderType === '' ? null : (
          <Card
            transparent
            style={{
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
              marginTop: 0,
            }}>
            <CardItem
              style={{
                paddingLeft: 0,
                paddingRight: 0,
                marginLeft: 0,
                marginRight: 0,
              }}>
              <ListOrderTypeFilter
                isPullToRefresh={isPullToRefresh}
                orderType={orderType}
                settingOrderType={type => settingOrderType(type)}
              />
            </CardItem>
          </Card>
        )}
        <ScrollView
          refreshControl={
            <RefreshControl
              onRefresh={() => pullToRefresh()}
              refreshing={isPullToRefresh}
            />
          }
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <NoTransactions
            titleText="No Orders yet."
            subText="Your orders will appear here once you've made purchase."
          />
        </ScrollView>
      </Container>
    );
  } else if (
    noConnection === false &&
    isLoading === false &&
    isLogin === false &&
    orders.length === 0
  ) {
    // show no login card
    return (
      <Container style={{backgroundColor: 'white'}}>
        <StatusBar barStyle="dark-content" />
        <Headers />
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <NoLoginYet
            titleText="No Orders yet."
            subText="Your orders will appear here once you've made purchase. Login to view and manage them with ease."
            onPress={() => {
              // Login
              goToLogin();
            }}
          />
        </ScrollView>
      </Container>
    );
  } else if (
    noConnection === false &&
    isLoading === false &&
    isLogin === true &&
    orders.length !== 0
  ) {
    // show list data
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <Headers />
        <View style={{flex: 1}}>
          <OrderFlatList
            orderType={orderType}
            settingOrderType={type => settingOrderType(type)}
            onChangeOpacity={e => onChangeOpacity(e)}
            orders={orders}
            orderTabRef={orderTabRef}
            pullToRefresh={() => pullToRefresh()}
            isPullToRefresh={isPullToRefresh}
            isLoadMore={isLoadMore}
            settingIsLoadMore={e => settingIsLoadMore(e)}
            totalCount={totalCount}
            initialPageSize={initialPageSize}
            {...props}
          />
        </View>
      </Container>
    );
  } else if (
    noConnection === false &&
    isLoading === true &&
    isLogin === false &&
    orders.length === 0
  ) {
    // show loader
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else {
    // show error to refrsh
    return (
      <ErrorScreen
        message="Refresh"
        onPress={() => {
          // refetch function
          fetchOrder();
        }}
      />
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = dispatch => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(Order);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
