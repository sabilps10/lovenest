import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  Image,
  Dimensions,
  ActivityIndicator,
  Modal,
  PermissionsAndroid,
  Animated,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Footer,
  FooterTab,
  Button,
} from 'native-base';
import {CameraKitCameraScreen} from 'react-native-camera-kit';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import _ from 'lodash';
import {QRCode} from '../../utils/Themes/Images';
import fetchProduct from '../../graphql/queries/fetchProduct';
import Colors from '../../utils/Themes/Colors';
import {charImage} from '../../utils/Themes/Images';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncToken from '../../utils/AsyncstorageDataStructure/index';
import {CommonActions} from '@react-navigation/native';
import QUERY_GETCUSTOMER from '../../graphql/queries/getCustomer';
import {launchImageLibrary} from 'react-native-image-picker';
import {RFPercentage} from 'react-native-responsive-fontsize';
import RNQRGenerator from 'rn-qr-generator';
import {ReactNativeFile} from 'apollo-upload-client';

const {asyncToken} = AsyncToken;
const {extraLarge, regular} = FontSize;
const {medium, book} = FontType;
const {overlayDim, black, lightSalmon, mainGreen, white} = Colors;
const {charNoQR} = charImage;
const {QRCodeFrame: QrOverLay} = QRCode;

const {height} = Dimensions.get('window');

class QrCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      productId: null,
      showModal: false,
    };
  }

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentDidMount() {
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(false);
    }
    this.props.navigation.addListener('didFocus', () => {
      this.setState({
        isLoading: false,
        productId: null,
        showModal: false,
      });
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(false);
      }
    });
  }

  componentWillUnmount() {
    this.setState({
      isLoading: false,
      productId: null,
      showModal: false,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('masuk didUpdate bosss 1111', prevState);
    if (this.state.productId !== prevState.productId) {
      console.log('masuk didUpdate bosss 2222');
      this.getProductDetail();
    }
  }

  getProductDetail = () => {
    const {props} = this;
    const {client} = props;

    if (this.state.productId !== null || this.state.productId !== undefined) {
      client
        .query({
          query: fetchProduct,
          variables: {
            id: parseInt(this.state.productId, 10),
          },
        })
        .then(response => {
          console.log('response QR get product: ', response);
          const {data} = response;
          const {fetchProduct: fetching} = data;
          const {data: productDetailData} = fetching;

          if (productDetailData !== undefined || productDetailData !== null) {
            const {id: productId, publicProduct} = productDetailData;
            if (publicProduct) {
              this.setState(
                prevState => ({
                  ...prevState,
                  isLoading: false,
                  productId: null,
                  showModal: false,
                }),
                () => {
                  this.props.navigation.navigate('ProductDetail', {
                    productId: productId,
                  });
                },
              );
            } else {
              this.setState(prevState => ({
                ...prevState,
                isLoading: false,
                showModal: true,
              }));
            }
          } else {
            this.setState(prevState => ({
              ...prevState,
              isLoading: false,
              showModal: false,
            }));
          }
        })
        .catch(error => {
          console.log('Error get product QR: ', error);
          this.setState(prevState => ({
            ...prevState,
            isLoading: false,
            showModal: false,
          }));
        });
    }
  };

  handlereadQRCode = event => {
    // console.log('QRCODE DATA: ', event?.nativeEvent);
    try {
      if (event) {
        const {nativeEvent} = event;
        if (nativeEvent) {
          const {codeStringValue} = nativeEvent;
          console.log('codeStringValue: ', codeStringValue);

          const re = new RegExp('^(http|https)://', 'i');
          const match = re.test(codeStringValue);

          if (match) {
            // this is will check Product
            if (codeStringValue) {
              this.setState(
                prevState => ({
                  ...prevState,
                  isLoading: true,
                }),
                () => {
                  const uri = codeStringValue;
                  const spliting = uri.split('/').pop();
                  const anotherSpliting = spliting.split('?')[0];
                  console.log('anotherSpliting: ', anotherSpliting);

                  if (anotherSpliting) {
                    const id = parseInt(anotherSpliting, 10);
                    console.log('ID>>>>>>>>>> ', id);

                    if (isNaN(id)) {
                      this.setState(prevState => ({
                        ...prevState,
                        isLoading: false,
                      }));
                    } else {
                      this.setState(
                        prevState => ({
                          ...prevState,
                          productId: id,
                        }),
                        () => {
                          // this.getProductDetail(this.state.productId);
                        },
                      );
                      // this.getProductDetail(id);
                      // this.setState(
                      //   prevState => ({
                      //     ...prevState,
                      //     isLoading: false,
                      //   }),
                      //   () => {
                      //     // this.props.navigation.navigate('ProductDetail', { id });
                      //   }
                      // );
                    }
                  } else {
                    this.setState(prevState => ({
                      ...prevState,
                      isLoading: false,
                      showModal: false,
                    }));
                  }
                },
              );
            }
          } else {
            // this is will check for photo booths
            console.log('Masuk Ke Photo Booths');
            this.setState(
              prevState => ({
                ...prevState,
                isLoading: true,
              }),
              async () => {
                const token = await AsyncStorage.getItem(asyncToken);
                if (token) {
                  await this.props.client
                    .query({
                      query: QUERY_GETCUSTOMER,
                      fetchPolicy: 'no-cache',
                      notifyOnNetworkStatusChange: true,
                    })
                    .then(response => {
                      console.log('Response User: ', response);
                      const {data, errors} = response;
                      const {getCustomer} = data;
                      if (errors) {
                        this.setState(prevState => ({
                          ...prevState,
                          isLoading: false,
                        }));
                      } else {
                        const {name, profileImage, partnerName} =
                          getCustomer[0];
                        this.setState(
                          prevState => ({
                            ...prevState,
                            isLoading: false,
                          }),
                          () => {
                            this.props.navigation.navigate('PhotoBooths', {
                              hashId: codeStringValue,
                              profile: {
                                name,
                                profileImage,
                                partnerName,
                              },
                            });
                          },
                        );
                      }
                    })
                    .catch(error => {
                      console.log('Error get user: ', error);
                      this.setState(prevState => ({
                        ...prevState,
                        isLoading: false,
                      }));
                    });
                } else {
                  this.setState(
                    prevState => ({
                      ...prevState,
                      isLoading: false,
                    }),
                    () => {
                      this.props.navigation.dispatch(
                        CommonActions.navigate({
                          name: 'Login',
                          params: {showXIcon: true},
                        }),
                      );
                    },
                  );
                }
              },
            );
          }
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        isLoading: false,
        showModal: false,
      }));
    }
  };

  openLocalImage = async () => {
    try {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          ],
          {
            title: 'Photo Gallery',
            message:
              'Love Nest App needs access to your gallery to save the photo ',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        console.log('granted >>>>> ', granted);
        if (granted) {
          launchImageLibrary({mediaType: 'photo', includeBase64: true}, res => {
            if (res.didCancel) {
              console.log('Cancel', res.didCancel);
            } else if (res.error) {
              console.log('Error Open Gallery', res.error);
            } else {
              console.log('Res Open Image Gallery: ', res);
              if (res?.assets[0]?.base64) {
                // found local image uri
                RNQRGenerator.detect({
                  base64: res?.assets[0]?.base64, // local path of the image. Can be skipped if base64 is passed.
                })
                  .then(response => {
                    console.log('QR GENERATOR LOCAL IMAGE: ', response);
                    const {values} = response; // Array of detected QR code values. Empty if nothing found.
                    if (values?.length > 0) {
                      // Image contain QR code
                      const dataStruct = {
                        nativeEvent: {
                          codeStringValue: values[0],
                        },
                      };

                      this.handlereadQRCode(dataStruct);
                    } else {
                      // Image does not contain QR Code
                      throw new Error('Image does not contain QR Code !');
                    }
                  })
                  .catch(error =>
                    console.log('Cannot detect QR code in image', error),
                  );
              } else {
                // cant find uri of local image
              }
            }
          });
        }
      } else {
        launchImageLibrary({mediaType: 'photo', includeBase64: true}, res => {
          if (res.didCancel) {
            console.log('Cancel', res.didCancel);
          } else if (res.error) {
            console.log('Error Open Gallery', res.error);
          } else {
            console.log('Res Open Image Gallery: ', res);
            if (res?.assets[0]?.base64) {
              // found local image uri
              RNQRGenerator.detect({
                base64: res?.assets[0]?.base64, // local path of the image. Can be skipped if base64 is passed.
              })
                .then(response => {
                  console.log('QR GENERATOR LOCAL IMAGE: ', response);
                  const {values} = response; // Array of detected QR code values. Empty if nothing found.
                  if (values?.length > 0) {
                    // Image contain QR code
                    const dataStruct = {
                      nativeEvent: {
                        codeStringValue: values[0],
                      },
                    };

                    this.handlereadQRCode(dataStruct);
                  } else {
                    // Image does not contain QR Code
                    throw new Error('Image does not contain QR Code !');
                  }
                })
                .catch(error =>
                  console.log('Cannot detect QR code in image', error),
                );
            } else {
              // cant find uri of local image
            }
          }
        });
      }
    } catch (error) {
      console.log('Error Open Local Image: ', error);
    }
  };

  render() {
    return (
      <Container style={{backgroundColor: 'transparent'}}>
        <View
          style={{position: 'relative', zIndex: 99, flexDirection: 'column'}}>
          {Platform.OS === 'ios' ? (
            <View style={{position: 'relative', zIndex: 999}}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                style={{
                  borderRadius: 32 / 2,
                  width: 32,
                  height: 32,
                  zIndex: 9999999,
                  position: 'absolute',
                  top: 36,
                  left: 20,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="MaterialIcons"
                  name="chevron-left"
                  style={{
                    fontSize: Platform.OS === 'ios' ? 25 : 25,
                    color: '#555555',
                    // left: Platform.OS === 'ios' ? 2 : 2,
                    top: Platform.OS === 'ios' ? 1 : 0,
                  }}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                style={{
                  borderRadius: 32 / 2,
                  width: 32,
                  height: 32,
                  zIndex: 9999999,
                  position: 'absolute',
                  top: 36,
                  left: 20,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  type="MaterialIcons"
                  name="chevron-left"
                  style={{
                    fontSize: Platform.OS === 'ios' ? 25 : 25,
                    color: '#555555',
                    // left: Platform.OS === 'ios' ? 2 : 2,
                    top: Platform.OS === 'ios' ? 1 : 0,
                  }}
                />
              </TouchableOpacity>
            </View>
          )}
          <Image
            source={QrOverLay}
            style={{
              flex: 1,
              position: 'absolute',
              zIndex: 1,
              width: '100%',
              height,
              overflow: 'hidden',
            }}
          />
          {this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                position: 'absolute',
                zIndex: 99,
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'red',
                width: '100%',
                height,
                // overflow: 'hidden',
              }}>
              <View
                style={{
                  bottom: 50,
                  width: 150,
                  height: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}>
                <ActivityIndicator size="large" color="pink" style={{}} />
                <Text style={{textAlign: 'center', marginVertical: 5}}>
                  Processing...
                </Text>
              </View>
            </View>
          ) : null}
          <CameraKitCameraScreen
            cameraOptions={{
              flashMode: 'auto', // on/off/auto(default)
              focusMode: 'on', // off/on(default)
              zoomMode: 'on', // off/on(default)
              // ratioOverlay: '1:1',            // optional, ratio overlay on the camera and crop the image seamlessly
              // ratioOverlayColor: '#00000077' // optional
            }}
            style={{height: 1200, bottom: Platform.OS === 'ios' ? 200 : 0}}
            actions={{rightButtonText: 'Done', leftButtonText: 'Cancel'}}
            // onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
            scanBarcode
            // laserColor="blue"
            // frameColor={"yellow"}

            onReadCode={event => this.handlereadQRCode(event)} // optional
            hideControls={false} // (default false) optional, hide buttons and additional controls on top and bottom of screen
            // showFrame={true}   //(default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
            // offsetForScannerFrame={10}   //(default 30) optional, offset from left and right side of the screen
            // heightForScannerFrame={300}  //(default 200) optional, change height of the scanner frame
            // colorForScannerFrame={'red'} //(default white) optional, change colot of the scanner frame
          />
        </View>
        <Card
          style={{
            elevation: 0,
            shadowOpacity: 0,
            borderWidth: 0,
            zIndex: 999,
            position: 'absolute',
            bottom: 0,
            left: -2,
            right: 0,
            width: '100%',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginBottom: 0,
            paddingBottom: 10,
          }}>
          <CardItem
            style={{
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
            }}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                // paddingLeft: 20,
                // paddingRight: 20,
                minheight: Platform.OS === 'ios' ? 88 : 100,
                padding: 12,
                paddingLeft: Platform.OS === 'ios' ? 26 : 36,
                paddingRight: Platform.OS === 'ios' ? 26 : 36,
                overflow: 'hidden',
              }}>
              <Text
                style={{
                  color: '#2a2a2a',
                  fontSize: 21,
                  textAlign: 'center',
                  fontFamily: 'Gotham-Medium',
                  paddingBottom: 10,
                }}>
                Get to know the product
              </Text>
              <Text
                style={{
                  color: '#2a2a2a',
                  fontSize: 14,
                  textAlign: 'center',
                  lineHeight: 20,
                  letterSpacing: 0.3,
                  fontFamily: 'Gotham-Book',
                }}>
                You can scan the QR Code on the tag of the product to see it
                details and recommendations.
              </Text>
            </View>
          </CardItem>
          <CardItem style={{width: '100%', backgroundColor: 'white'}}>
            <Button
              onPressIn={() => this.openLocalImage()}
              style={{
                borderRadius: 4,
                width: '100%',
                backgroundColor: mainGreen,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  color: white,
                  fontSize: RFPercentage(1.5),
                  letterSpacing: 0.3,
                }}>
                Scan From Local Image
              </Text>
            </Button>
          </CardItem>
        </Card>
        <Modal visible={this.state.showModal} transparent>
          <View style={{flex: 1, backgroundColor: overlayDim}}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  isLoading: false,
                  productId: null,
                  showModal: false,
                });
              }}
              style={{flex: 1}}
            />
            <View
              style={{
                flex: 2,
                backgroundColor: 'white',
                flexDirection: 'column',
              }}>
              <Container>
                <Content>
                  <Card
                    transparent
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginLeft: 0,
                      marginRight: 0,
                    }}>
                    <CardItem cardBody>
                      <Image
                        source={charNoQR}
                        style={{flex: 1, width: null, height: 286}}
                        resizeMode="contain"
                      />
                    </CardItem>
                    <CardItem
                      style={{
                        flexDirection: 'column',
                        paddingLeft: 25,
                        paddingRight: 25,
                      }}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontFamily: medium,
                          fontSize: extraLarge,
                          color: black,
                          letterSpacing: 0.4,
                          lineHeight: 25,
                        }}>
                        You’re scanning an exclusive product
                      </Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          marginVertical: 10,
                          fontFamily: book,
                          fontSize: regular,
                          color: black,
                          letterSpacing: 0.3,
                          lineHeight: 20,
                        }}>
                        Contact the Love Nest Administrator to know the details
                      </Text>
                    </CardItem>
                  </Card>
                </Content>
                <Footer>
                  <FooterTab
                    style={{
                      height: '100%',
                      width: '100%',
                      backgroundColor: lightSalmon,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      activeOpacity={0.9}
                      style={{width: '100%', flexWrap: 'wrap'}}
                      onPress={() => {
                        this.setState({
                          isLoading: false,
                          productId: null,
                          showModal: false,
                        });
                      }}>
                      <View
                        style={{
                          width: '100%',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Gotham-Medium',
                            fontSize: 14,
                            textAlign: 'center',
                            color: '#ffffff',
                          }}>
                          SCAN ANOTHER PRODUCT
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </FooterTab>
                </Footer>
              </Container>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(QrCode);

const Wrapper = compose(withApollo)(ConnectedComponent);

export default props => {
  return <Wrapper {...props} />;
};
