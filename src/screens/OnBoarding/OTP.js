import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
  Modal,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Button,
  Left,
} from 'native-base';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import moment from 'moment';
import AsyncData from '../../utils/AsyncstorageDataStructure';
import {onBoarding} from '../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {width, height} = Dimensions.get('window');
const {shield} = onBoarding;
const {asyncToken, asyncCountryCode, asyncPhone, asyncConcatedPhoneNumber} =
  AsyncData;
const {
  lightSalmon,
  black,
  white,
  transparent,
  overlayDim,
  greyLine,
  mainGreen,
} = Colors;
const {superExtraLarge, regular, tiny} = FontSize;
const {medium, book, bold} = FontType;

// mutations
import verifyCustomerOTPVer2 from '../../graphql/mutations/verifyCustomerOTPVer2';
import verifyCustomerPhoneVer2 from '../../graphql/mutations/verifyCustomerPhoneVer2';

class OTP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countryCode: '',
      phoneNumber: '',
      initialTime: 60,
      code: '',
      errorMessage: '',
      isLoading: true,
      openModalLoading: false,
    };
  }

  setPhoneNumber = props => {
    try {
      if (
        props?.route?.params?.countryCode !== undefined &&
        props?.route?.params?.phoneNumber !== undefined
      ) {
        const {route} = props;
        const {params} = route;
        const {countryCode, phoneNumber} = params;
        this.setState(
          prevState => ({
            ...prevState,
            countryCode,
            phoneNumber,
          }),
          () => {
            this.setState(prevState => ({
              ...prevState,
              isLoading: false,
            }));
          },
        );
      }
    } catch (error) {
      console.log('setphoneNumber Error: ', error);
      this.setState(
        prevState => ({
          ...prevState,
          countryCode: '',
          phoneNumber: '',
          initialTime: 60,
          code: '',
          errorMessage: '',
          isLoading: true,
        }),
        () => {
          this.goBackToLogin();
        },
      );
    }
  };

  componentDidMount() {
    const {props} = this;
    this.startTimer();
    this.setPhoneNumber(props);
  }

  componentWillUnmount() {
    this.startTimer();
  }

  startTimer = () => {
    let settingInterval = setInterval(() => {
      if (this.state.initialTime === 0) {
        clearInterval(settingInterval);
      } else {
        this.setState(prevState => ({
          ...prevState,
          initialTime: prevState.initialTime - 1,
        }));
      }
    }, 1000);
  };

  setTimeToDefault = () => {
    this.setState(prevState => ({
      ...prevState,
      initialTime: 60,
    }));
    this.startTimer();
    // need resend OTP code from API
  };

  goBackToLogin = () => {
    try {
      const {navigation} = this.props;
      navigation.dispatch({
        ...CommonActions.goBack(),
      });
    } catch (error) {
      console.log('goBackTologin Error: ', error);
      this.props.navigation.goBack(null);
    }
  };

  resendOTPCode = () => {
    try {
      this.setTimeToDefault();
      console.log('Props Resend OTP: ', this.props);
      console.log('State Resend OTP: ', this.state);
      const {client} = this.props;
      const {countryCode, phoneNumber: phone, initialTime} = this.state;
      if (initialTime === 0) {
        client
          .mutate({
            mutation: verifyCustomerPhoneVer2,
            variables: {
              countryCode: parseInt(countryCode.replace('+', ''), 10),
              phone,
            },
          })
          .then(response => {
            console.log('Response: ', response);
          })
          .catch(error => {
            console.log('Error API Resend OTP: ', error);
          });
      }
    } catch (error) {
      console.log('Error resend OTP: ', error);
    }
  };

  sendingOTP = code => {
    try {
      const {props, state} = this;
      const {client, navigation, route} = props;
      const {params} = route;
      const {countryCode, phoneNumber, initialTime} = state;
      this.setState(
        prevState => ({
          ...prevState,
          openModalLoading: true,
          errorMessage: '',
        }),
        () => {
          if (initialTime === 0) {
            this.setState(prevState => ({
              ...prevState,
              errorMessage: 'Verification code is expired. Try resend the code',
              openModalLoading: false,
            }));
          } else {
            const newCountryCode = countryCode.replace('+', '');
            client
              .mutate({
                mutation: verifyCustomerOTPVer2,
                variables: {
                  countryCode: parseInt(newCountryCode, 10),
                  phone: phoneNumber,
                  smsOTP: code,
                },
              })
              .then(async response => {
                console.log('Response OTP: ', response);
                const {data} = response;
                const {verifyCustomerOTPVer2: detailResponseOTP} = data;
                const {token, error} = detailResponseOTP;
                const {isCustomerExist} = params;

                await AsyncStorage.setItem(asyncToken, token);
                await AsyncStorage.setItem(asyncCountryCode, countryCode);
                await AsyncStorage.setItem(asyncPhone, phoneNumber);
                await AsyncStorage.setItem(
                  asyncConcatedPhoneNumber,
                  `${countryCode}${phoneNumber}`,
                );

                if (error === null) {
                  if (isCustomerExist) {
                    // indicate registered user, SEND TO HOME
                    this.setState(
                      prevState => ({
                        ...prevState,
                        errorMessage: '',
                        openModalLoading: false,
                        initialTime: 0,
                      }),
                      () => {
                        navigation.reset({
                          routes: [
                            {
                              name: 'MainHome',
                              params: {
                                countryCode,
                                phoneNumber,
                                token,
                                isCustomerExist,
                                specialCustomer: null,
                              },
                            },
                          ],
                        });
                      },
                    );
                  } else {
                    // indicated new user, SEND TO REGISTER
                    this.setState(
                      prevState => ({
                        ...prevState,
                        errorMessage: '',
                        openModalLoading: false,
                      }),
                      () => {
                        navigation.reset({
                          routes: [
                            {
                              name: 'RegisterMainAccount',
                              params: {
                                countryCode,
                                phoneNumber,
                                token,
                                isCustomerExist,
                                specialCustomer: null,
                              },
                            },
                          ],
                        });
                      },
                    );
                  }
                } else {
                  this.setState(prevState => ({
                    ...prevState,
                    errorMessage: error,
                    openModalLoading: false,
                  }));
                }
              })
              .catch(error => {
                throw error;
              });
          }
        },
      );
    } catch (error) {
      console.log('Sending OTP Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        errorMessage: error,
        openModalLoading: false,
      }));
    }
  };

  render() {
    const {
      errorMessage,
      initialTime,
      countryCode,
      phoneNumber,
      openModalLoading,
    } = this.state;
    const timer = moment.utc(initialTime * 1000).format('mm:ss');
    return (
      <Container>
        <StatusBar
          translucent={false}
          animated
          barStyle="dark-content"
          backgroundColor={Platform.OS === 'ios' ? transparent : 'white'}
        />
        <Content>
          <Card transparent>
            <CardItem style={header.cardItem}>
              <Left>
                <Button
                  transparent
                  style={header.button}
                  onPress={() => this.goBackToLogin()}>
                  <Icon
                    type="Feather"
                    name="chevron-left"
                    style={header.icon}
                  />
                </Button>
              </Left>
            </CardItem>
            <CardItem>
              <Image
                source={shield}
                style={{
                  width: width / 4,
                  height: height / 9,
                  aspectRatio: 1.15,
                }}
                resizeMode="contain"
              />
            </CardItem>
            <CardItem style={title.cardItem}>
              <Text style={title.titlText}>Account Verification</Text>
            </CardItem>
            <CardItem
              style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
              <Text style={subTitle.subText}>
                You only have to enter the verification code we sent via SMS to
                you registered phone number{'\n'}{' '}
              </Text>
              <Text
                style={{
                  color: black,
                  fontFamily: bold,
                  fontSize: RFPercentage(1.8),
                }}>{`${countryCode}${phoneNumber}`}</Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <OTPInputView
                style={otpStyles.otpViewStyle}
                pinCount={4}
                // code={this.state.otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged={code => {
                  console.log('CODEMAN', code);
                  this.setState(prevState => ({
                    ...prevState,
                    code,
                  }));
                }}
                placeholderCharacter={'*'}
                placeholderTextColor={greyLine}
                autoFocusOnLoad
                codeInputFieldStyle={otpForm.underlineStyleBase}
                codeInputHighlightStyle={otpForm.underlineStyleHighLighted}
                onCodeFilled={code => {
                  console.log('Code >>>>>> ', code);
                  console.log(`Code is ${code}, you are good to go!`);
                  this.sendingOTP(code);
                }}
              />
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={errorMessageStyles.text}>{errorMessage}</Text>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {initialTime === 0 ? (
                <Button
                  // onPress={this.setTimeToDefault}
                  onPress={this.resendOTPCode}
                  style={timerStyles.buttonResend}>
                  <Text style={timerStyles.textResend}>Resend Code</Text>
                </Button>
              ) : (
                <React.Fragment>
                  <ActivityIndicator
                    size="small"
                    style={timerStyles.loaderTimer}
                  />
                  <Text>{timer}</Text>
                </React.Fragment>
              )}
            </CardItem>
          </Card>
        </Content>
        <Modal visible={openModalLoading} transparent>
          <View style={modalStyles.container}>
            <View style={modalStyles.boxLoader}>
              {errorMessage === '' ? (
                <ActivityIndicator size="large" color={black} />
              ) : (
                <Icon
                  type="Feather"
                  name="alert-circle"
                  style={modalStyles.icon}
                />
              )}
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const otpStyles = StyleSheet.create({
  left: {flexBasis: '40%'},
  otpViewStyle: {width: '80%', height: 40},
});

const modalStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: overlayDim,
  },
  boxLoader: {
    borderRadius: 5,
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: white,
  },
  icon: {fontSize: 25, color: black},
});

const timerStyles = StyleSheet.create({
  body: {flexBasis: '35%'},
  rightContainer: {
    flexBasis: '25%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonResend: {
    width: '100%',
    backgroundColor: transparent,
    elevation: 0,
    shadowOpacity: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textResend: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    lineHeight: 15,
    fontWeight: 'bold',
  },
  loaderTimer: {right: 10},
});

const header = StyleSheet.create({
  cardItem: {
    paddingLeft: 9,
    paddingRight: 9,
    paddingBottom: 20,
  },
  icon: {
    fontSize: superExtraLarge,
    color: black,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 36,
    height: 36,
  },
});

const title = StyleSheet.create({
  cardItem: {
    paddingBottom: 0,
  },
  titlText: {
    fontFamily: medium,
    fontSize: RFPercentage(2.5),
    color: black,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
});

const subTitle = StyleSheet.create({
  subText: {
    fontFamily: book,
    fontSize: RFPercentage(1.8),
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
    fontWeight: '300',
  },
});

const otpForm = StyleSheet.create({
  underlineStyleBase: {
    width: 50,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: black,
  },
  underlineStyleHighLighted: {
    borderColor: mainGreen,
  },
});

const errorMessageStyles = StyleSheet.create({
  text: {
    fontFamily: book,
    fontSize: tiny,
    color: lightSalmon,
  },
});

const Wrapper = compose(withApollo)(OTP);

export default props => <Wrapper {...props} />;
