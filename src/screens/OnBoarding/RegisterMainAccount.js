import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Platform,
  Image,
  Dimensions,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Left,
  Body,
} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import ButtonFooter from '../../components/Button/onBoardingButton';
import Picker from '../../components/Picker/pickerSelect';
import DatePicker from '../../components/Picker/dateAndTimePicker';
import moment from 'moment';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import EmailValidator from '../../utils/EmailValidator';
import {onBoarding} from '../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

const {width, height} = Dimensions.get('window');
const {bannerMainAccount} = onBoarding;
const {
  black,
  veryLightPinkTwo,
  lightSalmon,
  brownGrey,
  white,
  mainGreen,
  greyLine,
  transparent,
} = Colors;
const {extraLarge, regular} = FontSize;
const {medium, book} = FontType;
const items = [
  {label: 'Female', value: 'FEMALE'},
  {label: 'Male', value: 'MALE'},
];

/*
  PARAMS:
    countryCode,
    phoneNumber,
    token,
    isCustomerExist,
    specialCustomer: null,
*/

class RegisterMainAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideButtonFooter: false,
      showAddButtonOfAdditionalWeddingDate: true,
      showWeddingDatePicker: false,
      showOptionalWeddingDatePicker: false,
      countryCode: '',
      phoneNumber: '',
      name: '',
      gender: 'Female',
      email: '',
      defaultWeddingDate: new Date(),
      weddingDate: '',
      optionalWeddingDate: '',
      errorMessage: '',
      openPickerIOS: true,
      openPickerAndroid: false,
    };
  }

  componentDidMount() {
    const {props} = this;
    this.setCountryCodeAndPhoneNumber(props);
  }

  setCountryCodeAndPhoneNumber = props => {
    try {
      const {route} = props;
      const {params} = route;
      const {countryCode, phoneNumber} = params;

      if (countryCode !== undefined && phoneNumber !== undefined) {
        this.setState(prevState => ({
          ...prevState,
          countryCode,
          phoneNumber,
        }));
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  onChange = (label, value) => {
    this.setState(prevState => ({
      ...prevState,
      [label]: value,
    }));
  };

  hideButton = () => {
    this.setState(prevState => ({
      ...prevState,
      hideButtonFooter: true,
    }));
  };

  showButton = () => {
    this.setState(prevState => ({
      ...prevState,
      hideButtonFooter: false,
    }));
  };

  onNext = () => {
    try {
      const {props, state} = this;
      const {navigation, route} = props;
      const {params} = route;
      const {token} = params;
      const {
        name,
        gender,
        email,
        weddingDate,
        optionalWeddingDate,
        countryCode,
        phoneNumber,
      } = state;
      console.log('name: ', name);
      console.log('email: ', email);
      console.log('weddingDate: ', weddingDate);
      this.setState(
        prevState => ({
          ...prevState,
          errorMessage: '',
        }),
        async () => {
          if (name === '' || email === '' || weddingDate === '') {
            this.setState(prevState => ({
              ...prevState,
              errorMessage: 'Check your input please!',
            }));
          } else {
            const validatingEmail = await EmailValidator(email);
            if (validatingEmail) {
              // email is good
              navigation.navigate('RegisterPartnerAccount', {
                name,
                gender: gender.toUpperCase(),
                email,
                countryCode,
                phoneNumber,
                weddingDate,
                optionalWeddingDate,
                token,
              });
            } else {
              // wrong email format
              this.setState(prevState => ({
                ...prevState,
                errorMessage: 'Wrong email format!',
              }));
            }
          }
        },
      );
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        errorMessage: 'Internal server error!',
      }));
    }
  };

  doneDatePicker = (label, selectedDate) => {
    if (label === 'weddingDate') {
      this.setState(prevState => ({
        ...prevState,
        weddingDate: selectedDate,
        showWeddingDatePicker: false,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        optionalWeddingDate: selectedDate,
        showOptionalWeddingDatePicker: false,
      }));
    }
  };

  cancelDatePicker = label => {
    if (label === 'weddingDate') {
      this.setState(prevState => ({
        ...prevState,
        showWeddingDatePicker: false,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        showOptionalWeddingDatePicker: false,
      }));
    }
  };

  render() {
    console.log('Regis Main Account Props: ', this.props);
    console.log('Regis Main Account State: ', this.state);
    const {
      name,
      email,
      weddingDate,
      optionalWeddingDate,
      showWeddingDatePicker,
      showOptionalWeddingDatePicker,
      showAddButtonOfAdditionalWeddingDate,
      hideButtonFooter,
      errorMessage,
    } = this.state;
    return (
      <Container>
        <StatusBar
          translucent={false}
          animated
          barStyle="dark-content"
          backgroundColor={Platform.OS === 'ios' ? transparent : 'white'}
        />
        <Content contentContainerStyle={{paddingBottom: 25}}>
          <Card transparent style={{paddingTop: 25}}>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={bannerMainAccount}
                style={{
                  width: width / 1.02,
                  height: height / 3.5,
                  aspectRatio: width / 1.02 / (height / 3.5),
                }}
              />
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginRight: 10,
                  }}>
                  <Text
                    style={{
                      fontFamily: book,
                      color: greyLine,
                      fontSize: RFPercentage(1.5),
                    }}>
                    STEP 1 - Your Profile
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    borderBottomWidth: 0.5,
                    height: 1,
                    borderBottomColor: greyLine,
                  }}
                />
              </View>
            </CardItem>
            <CardItem>
              <Text style={titleStyles.title}>
                Let’s get to know you better!
              </Text>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                  paddingBottom: Platform.OS === 'ios' ? 10 : 0,
                }}>
                <Text style={styles.labelSection}>Name*</Text>
                <TextInput
                  onFocus={this.hideButton}
                  onBlur={this.showButton}
                  returnKeyType="done"
                  underlineColorAndroid={white}
                  style={{paddingLeft: 1}}
                  value={name}
                  placeholder="Your Name"
                  onChangeText={e => this.onChange('name', e)}
                />
              </View>
            </CardItem>
            <CardItem style={{backgroundColor: 'transparent'}}>
              <View
                style={{
                  width: '103%',
                  ...styles.cardItem,
                  paddingBottom: Platform.OS === 'ios' ? 10 : 0,
                }}>
                <Text style={styles.labelSection}>Gender*</Text>
                <View style={{width: '100%'}}>
                  <Picker
                    disables={false}
                    items={items}
                    useNativeAndroidPickerStyle={true}
                    placeholder={{label: 'Select Gender', value: ''}}
                    onChange={e => this.onChange('gender', e)}
                  />
                </View>
              </View>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                  paddingBottom: Platform.OS === 'ios' ? 10 : 0,
                }}>
                <Text style={styles.labelSection}>Email*</Text>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="email-address"
                  onFocus={this.hideButton}
                  onBlur={this.showButton}
                  returnKeyType="done"
                  underlineColorAndroid={white}
                  style={{paddingLeft: 1}}
                  value={email}
                  placeholder="Your Email"
                  onChangeText={e => {
                    let lowerCase = e.toLowerCase();
                    this.onChange('email', lowerCase);
                  }}
                />
              </View>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                }}>
                <Text style={styles.labelCalendar}>Wedding Date*</Text>
                <Button
                  transparent
                  onPress={() =>
                    this.setState(prevState => ({
                      ...prevState,
                      showWeddingDatePicker: true,
                    }))
                  }>
                  <Text
                    style={{
                      left: 1,
                      color: weddingDate === '' ? brownGrey : black,
                    }}>
                    {weddingDate === ''
                      ? 'Select Your Wedding Date'
                      : moment(weddingDate).format('DD MMMM YYYY')}
                  </Text>
                </Button>
                {showWeddingDatePicker ? (
                  <DatePicker
                    openStatus={showWeddingDatePicker}
                    minDate={new Date()}
                    value={weddingDate}
                    mode="date"
                    display="spinner"
                    done={selectedDate =>
                      this.doneDatePicker('weddingDate', selectedDate)
                    }
                    cancel={() => this.cancelDatePicker('weddingDate')}
                  />
                ) : null}
              </View>
            </CardItem>
            {showAddButtonOfAdditionalWeddingDate ? (
              <CardItem>
                <TouchableOpacity
                  style={styles.addButtonAdditionalWeddingDate}
                  onPress={() =>
                    this.setState(prevState => ({
                      ...prevState,
                      showAddButtonOfAdditionalWeddingDate: false,
                    }))
                  }>
                  <Left style={styles.leftIconAdditionalWeddingDate}>
                    <Icon
                      type="Feather"
                      name="plus-square"
                      style={{...styles.additionalIcon, color: lightSalmon}}
                    />
                  </Left>
                  <Body style={styles.buttonAdditionalWeddingDate}>
                    <Text
                      style={{
                        ...styles.textOfOptionalWeddingDate,
                        color: lightSalmon,
                      }}>
                      Add additional wedding date
                    </Text>
                  </Body>
                </TouchableOpacity>
              </CardItem>
            ) : (
              <CardItem style={{flexDirection: 'row'}}>
                <View
                  style={{
                    width: '90%',
                    ...styles.cardItem,
                  }}>
                  <Text style={{...styles.labelCalendar}}>
                    Additional Wedding Date
                  </Text>
                  <Button
                    transparent
                    onPress={() =>
                      this.setState(prevState => ({
                        ...prevState,
                        showOptionalWeddingDatePicker: true,
                      }))
                    }>
                    <Text
                      style={{
                        left: 1,
                        color: optionalWeddingDate === '' ? brownGrey : black,
                      }}>
                      {optionalWeddingDate === ''
                        ? 'Select Your Additional Wedding Date'
                        : moment(optionalWeddingDate).format('DD MMMM YYYY')}
                    </Text>
                  </Button>
                  {showOptionalWeddingDatePicker ? (
                    <DatePicker
                      openStatus={showOptionalWeddingDatePicker}
                      minDate={new Date()}
                      value={optionalWeddingDate}
                      mode="date"
                      display="spinner"
                      done={selectedDate =>
                        this.doneDatePicker('optionalWeddingDate', selectedDate)
                      }
                      cancel={() =>
                        this.cancelDatePicker('optionalWeddingDate')
                      }
                    />
                  ) : null}
                </View>
                <View style={styles.removeAdditionalWeddingDateViewWrapper}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(prevState => ({
                        ...prevState,
                        showAddButtonOfAdditionalWeddingDate: true,
                        optionalWeddingDate: '',
                      }))
                    }
                    style={styles.removeIconAdditionalWeddingDateButton}>
                    <Icon
                      type="Feather"
                      name="x-square"
                      style={styles.iconRemoveAdditionalWeddingDate}
                    />
                  </TouchableOpacity>
                </View>
              </CardItem>
            )}
            <CardItem>
              <Text style={styles.errorMessageStyles}>{errorMessage}</Text>
            </CardItem>
          </Card>
        </Content>
        {hideButtonFooter ? null : (
          <View style={FooterStyles.Footer}>
            <ButtonFooter
              transparentType={false}
              text="NEXT"
              onPressed={this.onNext}
            />
          </View>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  cardItem: {
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderBottomColor: veryLightPinkTwo,
  },
  pickerSelect: {height: 50, width: '100%', left: -7},
  addButtonAdditionalWeddingDate: {width: '100%', flexDirection: 'row'},
  leftIconAdditionalWeddingDate: {flexBasis: '10%'},
  additionalIcon: {fontSize: 25, color: black},
  buttonAdditionalWeddingDate: {flexBasis: '90%', justifyContent: 'center'},
  removeAdditionalWeddingDateViewWrapper: {width: '10%'},
  removeIconAdditionalWeddingDateButton: {
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconRemoveAdditionalWeddingDate: {fontSize: 25, color: black, left: 4},
  labelSection: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    letterSpacing: 0.3,
    marginBottom: Platform.OS === 'ios' ? 15 : 0,
  },
  labelCalendar: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    // fontWeight: 'bold',
    letterSpacing: 0.3,
    marginBottom: Platform.OS === 'ios' ? 5 : 0,
  },
  textOfOptionalWeddingDate: {
    fontFamily: book,
    fontSize: regular,
    letterSpacing: 0.3,
  },
  errorMessageStyles: {
    fontFamily: book,
    fontSize: regular,
    color: lightSalmon,
  },
});

const titleStyles = StyleSheet.create({
  title: {
    fontFamily: medium,
    fontSize: extraLarge,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
});

const FooterStyles = StyleSheet.create({
  Footer: {
    flexDirection: 'column',
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? (hasNotch() ? 13 : 0) : 0,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 16,
    backgroundColor: 'transparent',
    height: 80,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Wrapper = compose(withApollo)(RegisterMainAccount);

export default props => <Wrapper {...props} />;
