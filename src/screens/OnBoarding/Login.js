import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  StatusBar,
  Platform,
  Image,
  Dimensions,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Button, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonNext from '../../components/Button/onBoardingButton';
import CountryCodeListModal from '../../components/Modal/onBoardingModal/countryCodeListModal';
import verifyCustomerPhoneVer2 from '../../graphql/mutations/verifyCustomerPhoneVer2';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncLabel from '../../utils/AsyncstorageDataStructure';
import {onBoarding} from '../../utils/Themes/Images';
import {hasNotch} from 'react-native-device-info';
import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';

const {width, height} = Dimensions.get('window');
const {hand} = onBoarding;
const {asyncToken, asyncPhone, asyncConcatedPhoneNumber, asyncCountryCode} =
  AsyncLabel;
const {
  transparent,
  veryLightPinkTwo,
  black,
  brownGrey,
  lightSalmon,
  mainGreen,
} = Colors;
const {extraLarge, regular, tiny} = FontSize;
const {medium, book} = FontType;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      countryFlag: '🇸🇬',
      countryCode: '+65',
      phoneNumber: '',
      errorMessage: '',
      buttonText: 'NEXT',
      scrollEnabled: false,
      buttonIsLoading: false,
    };
  }

  componentDidMount() {
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(false);
    }

    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(false);
      }
    });
  }

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentWillUnmount() {
    this.setState(prevState => ({
      ...prevState,
      openModal: false,
      countryFlag: '🇸🇬',
      countryCode: '+65',
      phoneNumber: '',
      errorMessage: '',
      buttonText: 'Next',
      scrollEnabled: false,
    }));
  }

  onChangePhoneNumber = e => {
    this.setState(prevState => ({
      ...prevState,
      phoneNumber: e,
    }));
  };

  navigateToHome = async () => {
    try {
      const {props} = this;
      const {navigation} = props;
      await AsyncStorage.clear();
      await navigation.dispatch(
        CommonActions.reset({
          routes: [
            {name: 'MainHome', params: {greeting: 'Welcome To Love Nest'}},
          ],
        }),
      );
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  navigateToOTP = () => {
    const {props, state} = this;
    const {navigation} = props;
    const {countryCode, phoneNumber} = state;
    try {
      this.setState(
        prevState => ({
          ...prevState,
          buttonIsLoading: true,
        }),
        () => {
          if (phoneNumber === '') {
            this.setState(prevState => ({
              ...prevState,
              errorMessage: 'Please fill your phone number!',
              buttonIsLoading: false,
            }));
          } else {
            this.setState(
              prevState => ({
                ...prevState,
                errorMessage: '',
              }),
              () => {
                const newCountryCode = countryCode.replace('+', '');
                props.client
                  .mutate({
                    mutation: verifyCustomerPhoneVer2,
                    variables: {
                      countryCode: parseInt(newCountryCode, 10),
                      phone: phoneNumber,
                    },
                  })
                  .then(async res => {
                    console.log('res verifyCustomerPhoneVer2: >>>', res);
                    const {data} = res;
                    const {verifyCustomerPhoneVer2: responRequestOTP} = data;
                    const {
                      isCustomerExist,
                      specialCustomer,
                      token, // if this token !== '' so this is for apple developer account
                      error,
                    } = responRequestOTP;

                    if (error === null) {
                      if (isCustomerExist && specialCustomer && token !== '') {
                        // for apple developer account testing
                        await AsyncStorage.setItem(asyncToken, token);
                        await AsyncStorage.setItem(
                          asyncCountryCode,
                          countryCode,
                        );
                        await AsyncStorage.setItem(asyncPhone, phoneNumber);
                        await AsyncStorage.setItem(
                          asyncConcatedPhoneNumber,
                          `${countryCode}${phoneNumber}`,
                        );
                        this.setState(
                          prevState => ({
                            ...prevState,
                            buttonIsLoading: false,
                          }),
                          () => {
                            navigation.dispatch(
                              CommonActions.reset({
                                routes: [
                                  {
                                    name: 'MainHome',
                                    params: {
                                      newCountryCode,
                                      countryCode,
                                      phoneNumber,
                                      token,
                                      isCustomerExist,
                                      specialCustomer,
                                    },
                                  },
                                ],
                              }),
                            );
                          },
                        );
                      } else if (
                        isCustomerExist &&
                        !specialCustomer &&
                        token === ''
                      ) {
                        // for user has been fully register
                        this.setState(
                          prevState => ({
                            ...prevState,
                            buttonIsLoading: false,
                          }),
                          () => {
                            navigation.navigate('OTP', {
                              countryCode,
                              phoneNumber,
                              token,
                              isCustomerExist,
                              specialCustomer,
                            });
                          },
                        );
                      } else if (
                        !isCustomerExist &&
                        !specialCustomer &&
                        token === ''
                      ) {
                        // new user
                        this.setState(
                          prevState => ({
                            ...prevState,
                            buttonIsLoading: false,
                          }),
                          () => {
                            navigation.navigate('OTP', {
                              countryCode,
                              phoneNumber,
                              token,
                              isCustomerExist,
                              specialCustomer,
                            });
                          },
                        );
                      }
                    } else {
                      throw error;
                    }
                  })
                  .catch(error => {
                    throw error;
                  });
              },
            );
          }
        },
      );
    } catch (error) {
      console.log('Error to navigate to OTP screen: ', error);
      this.setState(prevState => ({
        ...prevState,
        errorMessage: error,
        buttonIsLoading: false,
      }));
    }
  };

  onFocusTextInput = () => {
    this.setState(prevState => ({
      ...prevState,
      scrollEnabled: true,
    }));
  };

  onBlurTextInput = () => {
    this.setState(prevState => ({
      ...prevState,
      scrollEnabled: false,
    }));
  };

  openModal = () => {
    this.setState(prevState => ({
      ...prevState,
      openModal: !prevState.openModal,
    }));
  };

  closeModal = modalProps => {
    if (modalProps === undefined) {
      this.setState(prevState => ({
        ...prevState,
        openModal: false,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        openModal: false,
        countryFlag: modalProps.flag,
        countryCode: modalProps.dial_code,
      }));
    }
  };

  render() {
    console.log('LOGIN Props: ', this.props);
    const {
      openModal,
      countryFlag,
      countryCode,
      phoneNumber,
      scrollEnabled,
      buttonText,
      errorMessage,
      buttonIsLoading,
    } = this.state;
    return (
      <Container>
        <StatusBar
          translucent={false}
          animated
          barStyle="dark-content"
          backgroundColor={Platform.OS === 'ios' ? transparent : 'white'}
        />
        {this.props.route?.params?.showXIcon ? (
          <Button
            onPress={() => {
              this.props.navigation.goBack(null);
            }}
            style={{
              position: 'absolute',
              top: hasNotch() ? 45 : 25,
              right: 25,
              zIndex: 99,
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              elevation: 0,
              shadowOpacity: 0,
              backgroundColor: 'transparent',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        ) : null}
        <Content scrollEnabled={scrollEnabled}>
          <Card transparent style={styles.card}>
            <CardItem>
              <Image
                source={hand}
                style={{
                  width: width / 4,
                  height: height / 9,
                  aspectRatio: 1.15,
                }}
                resizeMode="contain"
              />
            </CardItem>
            <CardItem style={styles.textCardItem}>
              <Text style={styles.title}>Hello!</Text>
              <Text style={styles.text}>
                You must verify your phone number to{'\n'}create a Love Nest
                account. By tapping Next,{'\n'}you agree to the Love Nest’s
                Terms{'\n'}and and Conditions of Use and Privacy Policy.
              </Text>
            </CardItem>
            <CardItem style={cardItemForm.cardItem}>
              <View style={cardItemForm.countryCodeButton}>
                <Button
                  style={cardItemForm.countryCode}
                  onPress={this.openModal}>
                  <View>
                    <Text style={cardItemForm.countryFlag}>{countryFlag}</Text>
                  </View>
                  <View>
                    <Text style={cardItemForm.textCountryCode}>
                      {countryCode}
                    </Text>
                  </View>
                </Button>
              </View>
              <View style={cardItemForm.phoneNumberForm}>
                <TextInput
                  onFocus={this.onFocusTextInput}
                  onBlur={this.onBlurTextInput}
                  value={phoneNumber}
                  placeholder="63346789"
                  returnKeyType="done"
                  keyboardType="phone-pad"
                  style={cardItemForm.textInput}
                  onChangeText={this.onChangePhoneNumber}
                />
              </View>
            </CardItem>
            <CardItem style={cardItemForm.cardItemErrorMessage}>
              <Text style={cardItemForm.errorMessage}>{errorMessage}</Text>
            </CardItem>
          </Card>
        </Content>
        {openModal ? (
          <CountryCodeListModal
            modalVisibility={openModal}
            closeModal={this.closeModal}
          />
        ) : null}
        {scrollEnabled ? null : (
          <View style={Footer.nextButton}>
            {this.props.route?.params?.showXIcon ? (
              <ButtonNext
                fullyTransparent={true}
                transparentType
                text="SKIP"
                onPressed={() => {}}
              />
            ) : (
              <ButtonNext
                transparentType
                text="SKIP"
                onPressed={this.navigateToHome}
              />
            )}

            <ButtonNext
              text={buttonText}
              onPressed={this.navigateToOTP}
              isLoading={buttonIsLoading}
            />
          </View>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    paddingTop: 25,
    elevation: 0,
    shadowOpacity: 0,
  },
  textCardItem: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  title: {
    marginVertical: 10,
    textAlign: 'left',
    fontFamily: medium,
    fontSize: extraLarge,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.45,
  },
  text: {
    textAlign: 'left',
    fontFamily: book,
    fontSize: regular,
    color: black,
    lineHeight: 20,
    letterSpacing: 0.3,
  },
});

const cardItemForm = StyleSheet.create({
  cardItem: {
    backgroundColor: transparent,
    flexDirection: 'row',
  },
  countryCodeButton: {
    flexBasis: '22%',
  },
  phoneNumberForm: {
    height: 32,
    paddingLeft: 10,
    flexBasis: '75%',
  },
  countryCode: {
    minWidth: '100%',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 0,
    backgroundColor: veryLightPinkTwo,
    height: 32,
    width: '100%',
    elevation: 0,
    shadowOpacity: 0,
    flexDirection: 'row',
  },
  textInput: {
    padding: 5,
    paddingBottom: 7,
    height: 32,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: brownGrey,
  },
  countryFlag: {right: 2, fontSize: 20, bottom: 2.5},
  textCountryCode: {top: 1, left: 2},
  errorMessage: {
    fontFamily: book,
    fontSize: tiny,
    color: lightSalmon,
    lineHeight: 12,
    letterSpacing: 0.21,
  },
  cardItemErrorMessage: {
    paddingTop: 0,
    paddingBottom: 0,
  },
});

const Footer = StyleSheet.create({
  nextButton: {
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? (hasNotch() ? 13 : 0) : 0,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 16,
    backgroundColor: transparent,
    height: 120,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const Connector = connect(mapToState, mapToDispatch)(Login);

const Wrapper = compose(withApollo)(Connector);

export default props => <Wrapper {...props} />;
