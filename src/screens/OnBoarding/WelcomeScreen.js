import React from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import {onBoarding} from '../../utils/Themes/Images';
import Banner from '../../components/Onboarding/Banner';
import {hasNotch} from 'react-native-device-info';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';

const {mainGreen} = Colors;
const {onBoarding0} = onBoarding;
const {width, height} = Dimensions.get('window');
const {medium, bold} = FontType;
const imgWidth = width / 1.5;
const imgHeight = height / 8;

const WelcomeScreen = props => {
  const {navigation} = props;
  return (
    <Container>
      <StatusBar translucent backgroundColor="transparent" animated />
      <View
        style={{
          zIndex: 3,
          left: 0,
          right: 0,
          top: hasNotch() ? 100 : 80,
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={onBoarding0}
          style={{
            width: imgWidth,
            height: imgHeight,
            aspectRatio: imgWidth / imgHeight,
          }}
          resizeMode="contain"
        />
      </View>
      <Banner timer={3000} />
      <View
        style={{
          paddingLeft: 15,
          paddingRight: 15,
          width,
          height: 48,
          zIndex: 5,
          left: 0,
          right: 0,
          bottom: Platform.OS === 'ios' ? (hasNotch() ? 25 : 10) : 10,
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: mainGreen,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: 'white',
              letterSpacing: 1.25,
            }}>
            GET STARTED
          </Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

const Wrapper = compose(withApollo)(WelcomeScreen);

export default props => <Wrapper {...props} />;
