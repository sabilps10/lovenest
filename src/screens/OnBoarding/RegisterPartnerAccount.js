import React, {Component} from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  TextInput,
  Platform,
  Dimensions,
  Image,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Picker from '../../components/Picker/pickerSelect';
import {Container, Content, Card, CardItem, Icon, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import CountryCodeListModal from '../../components/Modal/onBoardingModal/countryCodeListModal';
import ButtonNext from '../../components/Button/onBoardingButton';
import CLoader from '../../components/Loader/circleLoader';
import registerCustomerVer2 from '../../graphql/mutations/registerCustomerVer2';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncType from '../../utils/AsyncstorageDataStructure/index';
import {CommonActions} from '@react-navigation/native';
import moment from 'moment';
import {onBoarding} from '../../utils/Themes/Images';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

const {asyncToken} = AsyncType;
const {width, height} = Dimensions.get('window');
const {bannerPartnerAccount} = onBoarding;

/*
  PROPS ROUTE PARAMS
  name,
  gender: gender.toUpperCase(),
  email,
  countryCode,
  phoneNumber,
  weddingDate,
  optionalWeddingDate,
  token,
*/

const {
  black,
  veryLightPinkTwo,
  lightSalmon,
  brownGrey,
  white,
  transparent,
  mainGreen,
  greyLine,
} = Colors;
const {extraLarge, regular, tiny} = FontSize;
const {medium, book, bookItalic} = FontType;
const FEMALE = [{label: 'Female', value: 'FEMALE'}];
const MALE = [{label: 'Male', value: 'MALE'}];

class RegisterPartnerAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonIsLoading: false,
      hideButton: false,
      openModal: false,
      partnerCountryCode: '+65',
      partnerPhoneNumber: '',
      partnerGender: '',
      partnerEmail: '',
      partnerName: '',
      isLoading: true,
      onMessageError: '',
    };
  }

  componentDidMount() {
    const {props} = this;
    console.log('PROPS JANCUK: ', props);
    this.setGenderPartnerProps(props);
  }

  setGenderPartnerProps = props => {
    // const {route} = props;
    // const {params} = route;
    // const {gender} = params;

    if (props?.route?.params?.gender) {
      this.setState(
        prevState => ({
          ...prevState,
          partnerGender:
            props.route.params.gender === 'FEMALE' ? 'MALE' : 'FEMALE',
        }),
        () => {
          this.setState(prevState => ({
            ...prevState,
            isLoading: false,
          }));
        },
      );
    } else {
      this.setState(prevState => ({
        ...prevState,
        isLoading: false,
      }));
    }
  };

  onChange = (label, e) => {
    this.setState(prevState => ({
      ...prevState,
      [label]: e,
    }));
  };

  openModal = () => {
    this.setState(prevState => ({
      ...prevState,
      openModal: !prevState.openModal,
    }));
  };

  closeModal = modalProps => {
    if (modalProps === undefined) {
      this.setState(prevState => ({
        ...prevState,
        openModal: false,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        openModal: false,
        partnerCountryCode: modalProps.dial_code,
      }));
    }
  };

  hideButton = () => {
    this.setState(prevState => ({
      ...prevState,
      hideButton: true,
    }));
  };

  showButton = () => {
    this.setState(prevState => ({
      ...prevState,
      hideButton: false,
    }));
  };

  navigateBack = () => {
    try {
      const {navigation} = this.props;
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  onSumbit = () => {
    try {
      this.setState(
        prevState => ({
          ...prevState,
          buttonIsLoading: true,
          onMessageError: '',
        }),
        async () => {
          const {props, state} = this;
          const {navigation, route, client} = props;
          const {params} = route;
          const {
            name,
            gender,
            email,
            countryCode,
            phoneNumber: phone,
            weddingDate: marriageDate,
            optionalWeddingDate: marriageDate2,
            token,
          } = params;
          await AsyncStorage.setItem(asyncToken, token);
          const {
            partnerName,
            partnerGender,
            partnerCountryCode,
            partnerPhoneNumber: partnerPhone,
            partnerEmail,
          } = state;

          if (
            partnerName !== '' &&
            partnerEmail === '' &&
            partnerPhone === ''
          ) {
            this.setState(prevState => ({
              ...prevState,
              onMessageError: 'Complete your partner information!',
              buttonIsLoading: false,
            }));
          }

          if (
            partnerName === '' &&
            partnerEmail !== '' &&
            partnerPhone === ''
          ) {
            this.setState(prevState => ({
              ...prevState,
              onMessageError: 'Complete your partner information!',
              buttonIsLoading: false,
            }));
          }

          if (
            partnerName === '' &&
            partnerEmail === '' &&
            partnerPhone !== ''
          ) {
            this.setState(prevState => ({
              ...prevState,
              onMessageError: 'Complete your partner information!',
              buttonIsLoading: false,
            }));
          }

          const variables = {
            name,
            gender,
            email: email.toLowerCase(),
            countryCode,
            phone,
            marriageDate: moment(marriageDate).format('DD MMMM YYYY'),
            marriageDate2:
              marriageDate2 === ''
                ? ''
                : moment(marriageDate2).format('DD MMMM YYYY'),
            partnerName,
            partnerGender,
            partnerEmail: partnerEmail === '' ? '' : partnerEmail.toLowerCase(),
            partnerCountryCode,
            partnerPhone,
          };

          console.log('variables: ', variables);

          client
            .mutate({
              mutation: registerCustomerVer2,
              variables,
            })
            .then(async response => {
              console.log('response: ', response);
              const {data} = response;
              const {registerCustomerVer2: registerUser} = data;
              const {error} = registerUser;

              if (error === null) {
                this.setState(
                  prevState => ({
                    ...prevState,
                    onMessageError: '',
                    buttonIsLoading: false,
                  }),
                  () => {
                    navigation.dispatch(
                      CommonActions.reset({
                        routes: [
                          {
                            name: 'MainHome',
                            params: {user: 'jane'},
                          },
                        ],
                      }),
                    );
                  },
                );
              } else {
                this.setState(prevState => ({
                  ...prevState,
                  buttonIsLoading: false,
                  onMessageError: 'Check your input!',
                }));
              }
            })
            .catch(error => {
              console.log('Error: ', error);
              this.setState(prevState => ({
                ...prevState,
                buttonIsLoading: false,
                onMessageError: 'Internal Server Error',
              }));
            });
        },
      );
    } catch (error) {
      console.log('Error: onSubmit ', error);
      this.setState(prevState => ({
        ...prevState,
        buttonIsLoading: false,
        onMessageError: 'Internal Server Error',
      }));
    }
  };

  render() {
    console.log('Partner Props: ', this.props);
    const {
      hideButton,
      buttonIsLoading,
      openModal,
      partnerCountryCode,
      partnerPhoneNumber,
      partnerName,
      partnerEmail,
      partnerGender,
      isLoading,
      onMessageError,
    } = this.state;

    if (isLoading) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CLoader />
        </View>
      );
    }

    return (
      <Container>
        <StatusBar
          translucent={false}
          animated
          barStyle="dark-content"
          backgroundColor={Platform.OS === 'ios' ? transparent : 'white'}
        />
        <Content>
          <Card transparent style={{marginBottom: 100, paddingTop: 25}}>
            <CardItem style={{width: '100%'}}>
              <Button
                onPress={() => {
                  try {
                    this.props.navigation.goBack(null);
                  } catch (error) {
                    console.log('Error back partner screen: ', error);
                    this.props.navigation.dispatch(
                      CommonActions.reset({
                        routes: [
                          {
                            name: 'Main',
                            params: {user: 'jane'},
                          },
                        ],
                      }),
                    );
                  }
                }}
                transparent
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'absolute',
                  bottom: 0,
                  right: 0,
                }}>
                <Icon
                  type="Feather"
                  name="x"
                  style={{
                    fontSize: RFPercentage(3.5),
                    color: black,
                  }}
                />
              </Button>
            </CardItem>
            <CardItem
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={bannerPartnerAccount}
                style={{
                  width: width / 1.02,
                  height: height / 3.5,
                  aspectRatio: width / 1.02 / (height / 3.5),
                }}
              />
            </CardItem>
            <CardItem style={{paddingTop: 0, marginTop: 0}}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginRight: 10,
                  }}>
                  <Text
                    style={{
                      fontFamily: book,
                      color: greyLine,
                      fontSize: RFPercentage(1.5),
                    }}>
                    STEP 2 - Your Partner (Optional)
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    borderBottomWidth: 0.5,
                    height: 1,
                    borderBottomColor: greyLine,
                  }}
                />
              </View>
            </CardItem>
            <CardItem>
              <Text style={titleStyles.title}>
                Let’s get to know you better!
              </Text>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                }}>
                <Text style={styles.labelSection}>Name</Text>
                <TextInput
                  onFocus={this.hideButton}
                  onBlur={this.showButton}
                  returnKeyType="done"
                  underlineColorAndroid={white}
                  style={{paddingLeft: 1}}
                  value={partnerName}
                  placeholder="Your Partner Name"
                  onChangeText={e => this.onChange('partnerName', e)}
                />
              </View>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '103%',
                  ...styles.cardItem,
                }}>
                <Text style={styles.labelSection}>Gender</Text>
                <View style={{width: '100%'}}>
                  <Picker
                    disabled
                    items={partnerGender === 'FEMALE' ? FEMALE : MALE}
                    useNativeAndroidPickerStyle={true}
                    placeholder={
                      partnerGender === 'FEMALE' ? FEMALE[0] : MALE[0]
                    }
                    onChange={e => this.onChange('gender', e)}
                  />
                </View>
              </View>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                }}>
                <Text style={styles.labelSection}>Email</Text>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="email-address"
                  onFocus={this.hideButton}
                  onBlur={this.showButton}
                  returnKeyType="done"
                  underlineColorAndroid={white}
                  style={{paddingLeft: 1}}
                  value={partnerEmail}
                  placeholder="Your Partner Email"
                  onChangeText={e => {
                    let lowerCase = e.toLowerCase();
                    this.onChange('partnerEmail', lowerCase);
                  }}
                />
              </View>
            </CardItem>
            <CardItem>
              <View
                style={{
                  width: '100%',
                  ...styles.cardItem,
                }}>
                <Text style={styles.labelSection}>Phone Number</Text>
                <View style={{flexDirection: 'row'}}>
                  <View style={cardItemForm.countryCodeButton}>
                    <Button
                      style={cardItemForm.countryCode}
                      onPress={this.openModal}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={cardItemForm.textCountryCode}>
                          {partnerCountryCode}
                        </Text>
                        <Icon
                          type="Feather"
                          name="chevron-down"
                          style={{right: 5, fontSize: 15, color: black}}
                        />
                      </View>
                    </Button>
                  </View>
                  <View style={cardItemForm.phoneNumberForm}>
                    <TextInput
                      onFocus={this.hideButton}
                      onBlur={this.showButton}
                      value={partnerPhoneNumber}
                      placeholder="Your Partner Phone Number"
                      returnKeyType="done"
                      keyboardType="phone-pad"
                      style={cardItemForm.textInput}
                      onChangeText={e => this.onChange('partnerPhoneNumber', e)}
                    />
                  </View>
                </View>
              </View>
            </CardItem>
            <CardItem>
              <Text style={errorStyles.errorMessageStyles}>
                {onMessageError}
              </Text>
            </CardItem>
          </Card>
        </Content>
        {openModal ? (
          <CountryCodeListModal
            modalVisibility={openModal}
            closeModal={this.closeModal}
          />
        ) : null}
        {hideButton ? null : (
          <View style={Footer.nextButton}>
            <ButtonNext
              transparentType
              text="BACK"
              onPressed={this.navigateBack}
            />
            <ButtonNext
              text="SAVE"
              onPressed={this.onSumbit}
              isLoading={buttonIsLoading}
            />
          </View>
        )}
      </Container>
    );
  }
}

const errorStyles = StyleSheet.create({
  errorMessageStyles: {
    fontFamily: book,
    fontSize: regular,
    color: lightSalmon,
  },
});

const titleStyles = StyleSheet.create({
  title: {
    fontFamily: medium,
    color: black,
    fontSize: extraLarge,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
  italicStyle: {
    fontFamily: bookItalic,
    color: brownGrey,
    fontSize: extraLarge,
    lineHeight: 25,
    letterSpacing: 0.4,
  },
});

const styles = StyleSheet.create({
  cardItem: {
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderBottomColor: veryLightPinkTwo,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  labelSection: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    letterSpacing: 0.3,
    marginBottom: Platform.OS === 'ios' ? 15 : 0,
  },
  pickerSelect: {height: 50, width: '100%', left: -7},
});

const cardItemForm = StyleSheet.create({
  cardItem: {
    backgroundColor: transparent,
    flexDirection: 'row',
  },
  countryCodeButton: {
    flexWrap: 'wrap',
    flexBasis: '20%',
  },
  phoneNumberForm: {
    height: 32,
    flexBasis: '85%',
  },
  countryCode: {
    minWidth: '100%',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderRadius: 0,
    backgroundColor: transparent,
    height: 32,
    width: '100%',
    elevation: 0,
    shadowOpacity: 0,
    flexDirection: 'row',
  },
  textInput: {
    padding: 5,
    paddingLeft: 25,
    paddingBottom: 7,
    height: 32,
    width: '100%',
    borderLeftWidth: 1,
    borderLeftColor: veryLightPinkTwo,
  },
  textCountryCode: {bottom: 0.5, left: 2},
  errorMessage: {
    fontFamily: book,
    fontSize: tiny,
    color: lightSalmon,
    lineHeight: 12,
    letterSpacing: 0.21,
  },
  cardItemErrorMessage: {
    paddingTop: 0,
    paddingBottom: 0,
  },
});

const Footer = StyleSheet.create({
  nextButton: {
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? (hasNotch() ? 13 : 0) : 0,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 16,
    backgroundColor: 'transparent',
    height: 120,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Wrapper = compose(withApollo)(RegisterPartnerAccount);

export default props => <Wrapper {...props} />;
