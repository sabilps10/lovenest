import React, {useEffect, useState} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  View,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Header from '../../components/Header/Common/index';
import {Container, Card, CardItem, Button, Icon} from 'native-base';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncImage from '../../components/Image/AsyncImage';
import moment from 'moment';

// Query
import GET_ALL_PROMOTIONS from '../../graphql/queries/getAllPromotionWithPagination';

const {book, medium} = FontType;
const {white, black, mainRed, mainGreen, greyLine} = Colors;
const {width, height} = Dimensions.get('window');
const AnimatedCard = Animated.createAnimatedComponent(Card);

const SpecialOffers = props => {
  const {client, navigation, positionYBottomNav} = props;

  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [totalCount, setTotalCount] = useState(0);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const pageNumber = 1;
  const [itemDisplayed, setItemDisplayed] = useState(10);

  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    StatusBarFunc();
    fetch();

    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subs = navigation.addListener('focus', () => {
      StatusBarFunc();
      fetch();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return () => {
      subs();
      onLoadMore();
    };
  }, [isLoadMore, onLoadMore, onRefresh]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        let oldPageSize = itemDisplayed;
        let newPageSize = (oldPageSize += 10);
        await setItemDisplayed(newPageSize);
        await fetch();
      }
    } catch (error) {
      console.log('Error onLoadMore: ', error);
      setIsLoadMore(false);
      setItemDisplayed(10);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await setItemDisplayed(10);
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_ALL_PROMOTIONS,
          variables: {
            upcoming: true,
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response fetch all promotions: ', response);
          const {data, errors} = response;
          const {getAllPromotions} = data;
          const {data: dataList, error, totalData} = getAllPromotions;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsLoadMore(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            } else {
              await setTotalCount(totalData);
              await setList([...dataList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsLoadMore(false);
              await setRefreshing(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsLoadMore(false);
          await setRefreshing(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setIsLoadMore(false);
      await setRefreshing(false);
    }
  };

  const StatusBarFunc = () => {
    StatusBar.setTranslucent(false);
    StatusBar.setBackgroundColor(white);
    StatusBar.setBarStyle('dark-content');
  };

  const buttonLeft = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Header
          title={'Special Offers'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Header
          title={'Special Offers'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            onPressIn={() => onRefresh()}
            style={{
              backgroundColor: mainGreen,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: white}}>Refresh</Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <Header
          title={'Special Offers'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, zIndex: -1}}>
          <ListPromo
            {...props}
            setIsLoadMore={async () => await setIsLoadMore(true)}
            isLoadMore={isLoadMore}
            onLoadMore={() => onLoadMore()}
            data={list}
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        </View>
      </Container>
    );
  }
};

export const ListPromo = props => {
  const {
    data,
    refreshing,
    onRefresh,
    isLoadMore,
    onLoadMore,
    setIsLoadMore,
  } = props;
  console.log('DATA LIST PROMO: ', props);

  return (
    <FlatList
      ListFooterComponent={() => {
        if (isLoadMore) {
          return (
            <ActivityIndicator
              size="small"
              color={mainGreen}
              style={{marginVertical: 15}}
            />
          );
        } else {
          return null;
        }
      }}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      onEndReached={({distanceFromEnd}) => {
        if (distanceFromEnd < 0) {
          onLoadMore();
        }
      }}
      onMomentumScrollBegin={() => setIsLoadMore()}
      onEndReachedThreshold={0.1}
      legacyImplementation
      contentContainerStyle={{padding: 15, paddingTop: 5}}
      data={data}
      extraData={data}
      keyExtractor={item => `${item.id}`}
      renderItem={({item, index}) => {
        return <PromoCard {...props} item={item} index={index} />;
      }}
    />
  );
};

export const PromoCard = props => {
  const {item, navigation} = props;

  const {
    id,
    name,
    merchantName,
    promoImageURL,
    promoImageDynamicURL,
    startDate,
    endDate,
  } = item;

  const start = moment(startDate)
    .utc()
    .format('ddd, DD MMM YYYY');
  const end = moment(endDate)
    .utc()
    .format('ddd, DD MMM YYYY');
  const isSame = moment(startDate).isSame(endDate);

  const period = isSame ? end : `${start} - ${end}`;

  const logoSource = promoImageDynamicURL
    ? {uri: `${promoImageDynamicURL}=h500`}
    : {uri: promoImageURL};

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  if (item) {
    return (
      <TouchableOpacity
        style={{marginTop: 10}}
        onPress={() => {
          // navigate to special promo detail
          navigation.navigate('SpecialOfferDetail', {id});
        }}
        activeOpacity={1}
        onPressIn={pressIn}
        onPressOut={pressOut}>
        <AnimatedCard style={{borderRadius: 4, transform: [{scale}]}}>
          <CoverImage logoSource={logoSource} />
          <TitleAndPeriod title={name} period={period} />
          <SponsoredBy merchantName={merchantName} />
        </AnimatedCard>
      </TouchableOpacity>
    );
  } else {
    return null;
  }
};

export const SponsoredBy = props => {
  const {merchantName} = props;
  return (
    <CardItem style={{paddingTop: 0, backgroundColor: 'transparent'}}>
      <Text
        style={{
          fontSize: RFPercentage(1.4),
          fontFamily: book,
          color: 'grey',
          letterSpacing: 0.3,
          lineHeight: 18,
        }}>
        By <Text style={{fontFamily: medium}}>{merchantName}</Text>
      </Text>
    </CardItem>
  );
};

export const TitleAndPeriod = props => {
  const {title, period} = props;
  return (
    <CardItem style={{paddingBottom: 5, paddingTop: 10}}>
      <View style={{width: '100%'}}>
        <Text
          style={{
            fontSize: RFPercentage(1.8),
            fontFamily: medium,
            color: black,
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          {title}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
            marginVertical: 5,
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="clock"
            style={{
              fontSize: RFPercentage(1.5),
              color: 'grey',
              marginLeft: 0,
              marginRight: 0,
            }}
          />
          <Text
            style={{
              left: -10,
              fontSize: RFPercentage(1.4),
              fontFamily: medium,
              color: mainRed,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            {period}
          </Text>
        </View>
      </View>
    </CardItem>
  );
};

export const CoverImage = props => {
  const {logoSource} = props;

  return (
    <CardItem cardBody style={{backgroundColor: 'transparent'}}>
      <AsyncImage
        resizeMode={'cover'}
        style={{
          flex: 1,
          width: '100%',
          height: height / 5.2,
          borderTopLeftRadius: 4,
          borderTopRightRadius: 4,
        }}
        loaderStyle={{
          width: width / 7,
          height: height / 7,
        }}
        source={logoSource}
        placeholderColor={greyLine}
      />
    </CardItem>
  );
};

const Wrapper = compose(withApollo)(SpecialOffers);

export default props => <Wrapper {...props} />;
