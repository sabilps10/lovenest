import React, {useState, useEffect} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  Animated,
  ScrollView,
  Platform,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';
import moment from 'moment';

// Header Default
import Headers from '../../components/Header/Common/index';

// Query get Promo Detail by Promo Id
import GET_PROMO_DETAIL from '../../graphql/queries/getPromotion';

// Common Component
import BackButton from '../../components/Event/Button/BackButton';
import HeaderSpecialPromo from '../../components/Event/Header/index';
import AnimatedHeaderImage from '../../components/Event/Header/HeaderImage';
import NonelepsisDescription from '../../components/Event/EventDescription/nonElepsisDescription';
import {TouchableOpacity} from 'react-native-gesture-handler';

const {
  white,
  black,
  transparent,
  overlayDim,
  mainGreen,
  mainRed,
  greyLine,
  headerBorderBottom,
  newContainerColor,
} = Colors;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

const SpecialOfferDetail = props => {
  console.log('Special Offer Detail Props: ', props);
  const {navigation, client, route, positionYBottomNav} = props;
  const {params} = route;
  const {id} = params;

  const [detail, setDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor(transparent);
    StatusBar.setBarStyle('dark-content');
    fetch();
    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subs = navigation.addListener('focus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBackgroundColor(transparent);
      StatusBar.setBarStyle('dark-content');
      fetch();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subs;
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_PROMO_DETAIL,
          variables: {
            id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response Special Promo Detail: ', response);
          const {data, errors} = response;
          const {getPromotion} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            await setDetail([...getPromotion]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const onRefreshButton = async () => {
    try {
      await setIsError(false);
      await setIsLoading(true);
      await setRefreshing(false);
      await fetch();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return <Loader goBack={() => goBack()} />;
  } else if (!isLoading && isError) {
    return <RefreshScreen goback={goBack} refetch={onRefreshButton} />;
  } else {
    const IMAGE_HEIGHT = 188;
    const HEADER_HEIGHT =
      Platform.OS === 'android' ? (hasNotch() ? 80 : 70) : hasNotch() ? 80 : 70;
    const SCROLL_HEIGHT = IMAGE_HEIGHT - HEADER_HEIGHT;

    const TOP_BUTTON_DEFAULT_POSITION = hasNotch() ? 35 : 30;
    const TOP_BUTTON_POSITION_MINUS = hasNotch() ? 40 : 35;

    const scrollY = new Animated.Value(0);

    const headerOpacity = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [0.0, 0.5, 1.0],
    });

    const headerImageOpacity = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [1.0, 0.9, 0],
    });

    const imageScale = scrollY.interpolate({
      inputRange: [-25, 0],
      outputRange: [1.1, 1],
      extrapolateRight: 'clamp',
    });

    const buttonPosition = scrollY.interpolate({
      inputRange: [0, HEADER_HEIGHT],
      outputRange: [
        TOP_BUTTON_DEFAULT_POSITION,
        HEADER_HEIGHT - TOP_BUTTON_POSITION_MINUS,
      ],
      extrapolate: 'clamp',
    });

    const promoDetail = detail[0];
    const {
      id,
      promoImageURL,
      promoImageDynamicURL,
      promoImageBlobKey,
      promoLinkURL,
      merchantId,
      merchantName,
      createdOn,
      updatedOn,
      name,
      startDate,
      endDate,
      description,
    } = promoDetail;

    const imageSource = promoImageDynamicURL
      ? {uri: `${promoImageDynamicURL}=h500`}
      : {uri: promoImageURL};

    const start = moment(startDate)
      .utc()
      .format('ddd, DD MMM YYYY');
    const end = moment(endDate)
      .utc()
      .format('ddd, DD MMM YYYY');
    const isSame = moment(startDate).isSame(endDate);

    const period = isSame ? end : `${start} - ${end}`;

    return (
      <Container>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          animated
          barStyle="dark-content"
        />
        <BackButton buttonPosition={buttonPosition} onPress={() => goBack()} />
        <HeaderSpecialPromo
          headerOpacity={headerOpacity}
          name={detail[0]?.merchantName}
          headerHeight={HEADER_HEIGHT}
        />
        <Animated.ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          contentContainerStyle={{paddingBottom: 15, backgroundColor: white}}
          scrollEventThrottle={Platform.OS === 'android' ? 16 : 1}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}>
          <AnimatedHeaderImage
            containerStyle={{
              opacity: headerImageOpacity,
              transform: [
                {translateY: Animated.multiply(scrollY, 0.65)},
                {scale: imageScale},
              ],
            }}
            imageStyle={{
              aspectRatio: Dimensions.get('window').width / 188,
              width: Dimensions.get('window').width,
              height: 188,
              opacity: headerImageOpacity,
              transform: [{scale: imageScale}],
            }}
            imageSource={imageSource}
            placeholderColor={headerBorderBottom}
          />
          <Card
            style={{
              height: '100%',
              marginLeft: 0,
              marginRight: 0,
              paddingTop: 15,
              borderWidth: 0,
              borderColor: white,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <TitleAndPeriod title={name} period={period} />
            <SponsoredBy merchantName={merchantName} />
            <CardItem style={{paddingTop: 15}}>
              <NonelepsisDescription
                smallText={true}
                description={description}
              />
            </CardItem>
          </Card>
        </Animated.ScrollView>
        <ButtonFooter
          onPress={() => navigation.navigate('SpecialOfferForm', {id})}
        />
      </Container>
    );
  }
};

export const ButtonFooter = props => {
  const {onPress} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => onPress()}
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
                lineHeight: 18,
                fontFamily: medium,
              }}>
              SEND OFFER
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const SponsoredBy = props => {
  const {merchantName} = props;
  return (
    <CardItem style={{paddingTop: 0}}>
      <Text
        style={{
          fontSize: RFPercentage(1.6),
          fontFamily: book,
          color: 'grey',
          letterSpacing: 0.3,
          lineHeight: 18,
        }}>
        By <Text style={{fontFamily: medium}}>{merchantName}</Text>
      </Text>
    </CardItem>
  );
};

export const TitleAndPeriod = props => {
  const {title, period} = props;
  return (
    <CardItem style={{paddingBottom: 5, paddingTop: 10}}>
      <View style={{width: '100%'}}>
        <Text
          style={{
            fontSize: RFPercentage(2.1),
            fontFamily: medium,
            color: black,
            letterSpacing: 0.3,
            lineHeight: 18,
          }}>
          {title}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
            marginVertical: 10,
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Icon
            type="Feather"
            name="clock"
            style={{
              fontSize: RFPercentage(1.8),
              color: 'grey',
              marginLeft: 0,
              marginRight: 0,
            }}
          />
          <Text
            style={{
              left: -10,
              fontSize: RFPercentage(1.7),
              fontFamily: medium,
              color: mainRed,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            {period}
          </Text>
        </View>
      </View>
    </CardItem>
  );
};

export const RefreshScreen = props => {
  const {goBack, refetch} = props;
  return (
    <Container>
      <StatusBar
        translucent={false}
        backgroundColor="white"
        // animated
        barStyle="dark-content"
      />
      <Headers
        title={'Special Offer'}
        buttonLeft={() => {
          goBack();
        }}
        buttonRight={() => {}}
        showRightButton={false}
      />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button
          onPress={() => refetch()}
          style={{
            minWidth: width / 2.5,
            borderRadius: 4,
            padding: 10,
            paddingTop: 5,
            paddingBottom: 5,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: mainGreen,
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: white,
              letterSpacing: 0.3,
              lineHeight: 18,
              fontSize: RFPercentage(1.8),
            }}>
            Refresh
          </Text>
        </Button>
      </View>
    </Container>
  );
};

export const Loader = props => {
  const {goBack} = props;
  return (
    <Container>
      <StatusBar
        translucent={false}
        backgroundColor="white"
        // animated
        barStyle="dark-content"
      />
      <Headers
        title={'Special Offer'}
        buttonLeft={() => {
          goBack();
        }}
        buttonRight={() => {}}
        showRightButton={false}
      />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    </Container>
  );
};

const Wrapper = compose(withApollo)(SpecialOfferDetail);

export default props => <Wrapper {...props} />;
