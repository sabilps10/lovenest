import React, {useState, useEffect} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Headers from '../../components/Header/Common/index';
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Card,
  CardItem,
  Icon,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncData from '../../utils/AsyncstorageDataStructure/index';

import Colors from '../../utils/Themes/Colors';
import {FontType, FontSize} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

import Form from '../../components/FormInput/FormInput';
import FormPhone from '../../components/FormInput/PhoneNumberInput';
import DateAndTimePicker from '../../components/FormInput/DateAndTimeForm';
import CustomPicker from '../../components/FormInput/CustomPickerEnquiry';
import ModalCountryCode from '../../components/Modal/onBoardingModal/countryCodeListModal';

// Query
import GET_CUSTOMER from '../../graphql/queries/getCustomer';

// Mutation
import SUBMIT_OFFER from '../../graphql/mutations/submitPromotionEnquiry';

const {asyncToken} = AsyncData;
const {
  black,
  white,
  lightSalmon,
  greyLine,
  newContainerColor,
  mainGreen,
  overlayDim,
} = Colors;
const {regular, small} = FontSize;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');

const SendOffer = props => {
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [name, setName] = useState('');
  const [countryCode, setCountryCode] = useState('+65');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [modalPhonePickerVisible, setModalPhonePickerVisible] = useState(false);

  const [buttonLoader, setButtonLoader] = useState(false);

  const [showModal, setShowModal] = useState(false);
  const [isSuccessSubmit, setIsSuccessSubmit] = useState(false);
  const [isErrorSubmit, setIsErrorSubmit] = useState(false);
  const [errMsg, setErrMsg] = useState('');

  useEffect(() => {
    fetchGetCustomer();
    const subs = navigation.addListener('focus', () => {
      fetchGetCustomer();
    });

    return subs;
  }, []);

  const fetchGetCustomer = () => {
    try {
      AsyncStorage.getItem(asyncToken)
        .then(res => {
          if (res) {
            // any Token here
            client
              .query({
                query: GET_CUSTOMER,
                fetchPolicy: 'no-cache',
                ssr: false,
              })
              .then(async response => {
                console.log('Response Get Customer Enquiry: ', response);
                const {data, errors} = response;
                const {getCustomer} = data;

                if (errors) {
                  await setIsError(false);
                  await setIsLoading(false);
                } else {
                  if (getCustomer) {
                    const {
                      name: userName,
                      countryCode: userCountryCode,
                      phone: userPhone,
                      email: userEmail,
                    } = getCustomer[0];
                    await setName(userName);
                    await setCountryCode(userCountryCode);
                    await setPhone(userPhone);
                    await setEmail(userEmail);
                    await setIsError(false);
                    await setIsLoading(false);
                  } else {
                    await setIsError(false);
                    await setIsLoading(false);
                  }
                }
              })
              .catch(error => {
                console.log('Error: ', error);
                setIsError(false);
                setIsLoading(false);
              });
          } else {
            // no token =>>> not login yet
            setIsError(false);
            setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error Async Token 1: ', error);
          setIsError(false);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Async Token');
      setIsError(false);
      setIsLoading(false);
    }
  };

  const submit = async () => {
    try {
      console.log({
        name,
        countryCode,
        phone,
        email,
        message,
      });

      await setButtonLoader(true);
      await setShowModal(true);
      if (name === '' || phone === '' || email === '' || message === '') {
        await setIsErrorSubmit(true);
        await setErrMsg('Fill up empty form!');

        setTimeout(async () => {
          await setButtonLoader(false);
          await setIsErrorSubmit(false);
          await setErrMsg('');
          await setShowModal(false);
          await setIsSuccessSubmit(false);
        }, 2000);
      } else {
        // check email validation format
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(email).toLowerCase());
        console.log('ISVALID EMAIL: ', isValid);

        if (isValid) {
          // email is valid
          const submitSpecialOfferEnquiryParams = {
            specialOfferId: parseInt(id, 10),
            name,
            email,
            code: countryCode,
            phone,
            message,
          };
          console.log(
            'submitSpecialOfferEnquiryParams: ',
            submitSpecialOfferEnquiryParams,
          );

          client
            .mutate({
              mutation: SUBMIT_OFFER,
              variables: {
                input: submitSpecialOfferEnquiryParams,
              },
            })
            .then(async response => {
              console.log('Response Submit: ', response);
              const {data, errors} = response;
              const {submitSpecialOfferEnquiry} = data;
              const {error} = submitSpecialOfferEnquiry;

              if (errors) {
                await setIsErrorSubmit(true);
                await setErrMsg(errors.message);

                setTimeout(async () => {
                  await setIsErrorSubmit(false);
                  await setErrMsg('');
                  await setShowModal(false);
                  await setIsSuccessSubmit(false);
                  await setButtonLoader(false);
                }, 2000);
              } else {
                if (error) {
                  await setIsErrorSubmit(true);
                  await setErrMsg(error);

                  setTimeout(async () => {
                    await setIsErrorSubmit(false);
                    await setErrMsg('');
                    await setShowModal(false);
                    await setIsSuccessSubmit(false);
                    await setButtonLoader(false);
                  }, 2000);
                } else {
                  await setIsSuccessSubmit(true);

                  setTimeout(async () => {
                    await setIsErrorSubmit(false);
                    await setErrMsg('');
                    await setShowModal(false);
                    await setIsSuccessSubmit(false);
                    await setButtonLoader(false);
                  }, 2000);
                }
              }
            })
            .catch(async error => {
              console.log('Error: ', error);
              await setIsErrorSubmit(true);
              await setErrMsg(error.message);

              setTimeout(async () => {
                await setIsErrorSubmit(false);
                await setErrMsg('');
                await setShowModal(false);
                await setIsSuccessSubmit(false);
                await setButtonLoader(false);
              }, 2000);
            });
        } else {
          // email invalid
          await setIsErrorSubmit(true);
          await setErrMsg('Invalid Email Format!');

          setTimeout(async () => {
            await setIsErrorSubmit(false);
            await setErrMsg('');
            await setShowModal(false);
            await setIsSuccessSubmit(false);
            await setButtonLoader(false);
          }, 2000);
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsErrorSubmit(true);
      await setErrMsg('Failed to submit');

      setTimeout(async () => {
        await setIsErrorSubmit(false);
        await setErrMsg('');
        await setShowModal(false);
        await setIsSuccessSubmit(false);
        await setButtonLoader(false);
      }, 2000);
    }
  };

  if (isLoading) {
    return (
      <Container>
        <StatusBar backgroundColor="#ffffff" animated translucent={false} />
        <Headers
          title={'Send Offer'}
          buttonLeft={() => {
            navigation.goBack(null);
          }}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Gathering Detail...</Text>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        {buttonLoader ? (
          <Modal visible={buttonLoader} animationType="fade" transparent>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: overlayDim,
                padding: 15,
                paddingLeft: 25,
                paddingRight: 25,
              }}>
              {!isSuccessSubmit && !isErrorSubmit ? (
                <View
                  style={{
                    width: width / 1.2,
                    height: height / 4.5,
                    padding: 10,
                    backgroundColor: white,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 4,
                  }}>
                  <ActivityIndicator
                    size="large"
                    color={mainGreen}
                    style={{marginBottom: 10}}
                  />
                  <Text>Submitting Offer...</Text>
                </View>
              ) : isSuccessSubmit ? (
                <View
                  style={{
                    width: width / 1.2,
                    height: height / 4.5,
                    padding: 10,
                    backgroundColor: white,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 4,
                  }}>
                  <Icon
                    type="Feather"
                    name="check"
                    style={{
                      fontSize: 26,
                      color: 'green',
                      marginBottom: 10,
                    }}
                  />
                  <Text>Successfully Submit</Text>
                </View>
              ) : (
                <View
                  style={{
                    width: width / 1.2,
                    height: height / 4.5,
                    padding: 10,
                    backgroundColor: white,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 4,
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{
                      fontSize: 26,
                      color: 'red',
                      marginBottom: 10,
                    }}
                  />
                  <Text>{errMsg === '' ? 'Failed to submit' : errMsg}</Text>
                </View>
              )}
            </View>
          </Modal>
        ) : null}
        <StatusBar backgroundColor="#ffffff" animated translucent={false} />
        <Headers
          title={'Send Offer'}
          buttonLeft={() => {
            navigation.goBack(null);
          }}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <Content>
          <Card
            transparent
            style={{
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <Form
              type="Free Text"
              isError={false}
              required={true}
              isLoading={null}
              numberOfLines={1}
              multiline={true}
              value={name}
              onChangeText={async e => {
                await setName(e);
              }}
              title={'Name'}
              placeholder={'Your name...'}
            />
            <FormPhone
              title={'Phone Number'}
              required={true}
              value={phone}
              placeholder={'Your phone number...'}
              countryCode={countryCode}
              onChange={e => setPhone(e)}
              onChangeCountryCode={e => setCountryCode(e)}
            />
            <Form
              type="Email"
              isError={false}
              required={true}
              isLoading={null}
              numberOfLines={1}
              multiline={true}
              value={email}
              onChangeText={async e => {
                await setEmail(e);
              }}
              title={'Email'}
              placeholder={'Your email...'}
            />
            <Form
              type="Free Text"
              isError={false}
              required={true}
              isLoading={null}
              numberOfLines={3}
              multiline={true}
              value={message}
              onChangeText={async e => {
                await setMessage(e);
              }}
              title={'Message'}
              placeholder={'Your message...'}
            />
          </Card>
        </Content>
        {modalPhonePickerVisible ? (
          <ModalCountryCode
            modalVisibility={modalPhonePickerVisible}
            closeModal={async e => {
              console.log('EA: >>>>>>>>>>>', e);
              if (e) {
                const {dial_code} = e;
                await setCountryCode(dial_code);
                await setModalPhonePickerVisible(false);
              } else {
                setModalPhonePickerVisible(false);
              }
            }}
          />
        ) : null}
        <Footer>
          <FooterTab style={{backgroundColor: mainGreen}}>
            <View style={{backgroundColor: mainGreen, width: '100%'}}>
              <TouchableOpacity
                activeOpacity={buttonLoader ? 1 : 0.6}
                disabled={buttonLoader ? true : false}
                onPress={() => {
                  submit();
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                {buttonLoader ? (
                  <ActivityIndicator
                    color="white"
                    size="large"
                    style={{marginRight: 8}}
                    animating={buttonLoader}
                  />
                ) : null}
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  SUBMIT OFFER
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
};

const Wrapper = compose(withApollo)(SendOffer);

export default props => <Wrapper {...props} />;
