import React, {useState, useEffect, useRef} from 'react';
import {View, Animated, Text} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import compose from 'lodash/fp/compose';
import {withApollo} from 'react-apollo';
import Loader from '../../components/Loader/circleLoader';
import getAppointmentSlotByYearQuery from '../../graphql/queries/getAppointmentSlotByYear';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import Colors from '../../utils/Themes/Colors';
import moment from 'moment';
import Legend from '../../components/Calendar/YearlyCalendar/Legend';
import AppointmentHeader from '../../components/Header/AppointmentHeader/YearlyCalendarHeader';
import YearNavigation from '../../components/Calendar/YearlyCalendar/YearNavigation';
import LoaderWithContainer from '../../components/Loader/LoaderWithContainer';
import AppointmentSlotNotFound from '../../components/Calendar/YearlyCalendar/AppointmentSlotNotFound';
import DateContainer from '../../components/Calendar/YearlyCalendar/MonthList';
import GetCustomerDetailQuery from '../../graphql/queries/getCustomer';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import ButtonBack from '../../components/Button/buttonBack';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {book, medium} = FontType;
const {regular, small} = FontSize;
const {black, newContainerColor, greyLine, superGrey, white} = Colors;

const usePrevious = value => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const Calendar = props => {
  console.log('CALENDAR SCREEN: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {
    id,
    itemName,
    activity,
    // selectedAddressId // this is deprecated from sprint 23
  } = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingCalendarOnly, setIsLoadingCalendarOnly] = useState(false);
  const [currentYear, setCurrentYear] = useState(moment().format('YYYY'));
  const [calendar, setCalendar] = useState([]);
  const [connectionError, setConnectionError] = useState(false);
  const [errorStatus, setErrorStatus] = useState('');
  const [opacity] = useState(new Animated.Value(0));
  const [marriageDate, setMarriageDate] = useState('');

  // Prev state
  const prevCurrentYear = usePrevious(currentYear);

  useEffect(() => {
    getCalendarDataAPI();
    // navigationOptions();
    const subscribe = navigation.addListener('focus', () => {
      getCalendarDataAPI();
      // navigationOptions();
    });
    return subscribe;
  }, [
    isLoading,
    connectionError,
    currentYear,
    isLoadingCalendarOnly,
    errorStatus,
    navigation,
  ]);

  const loadAnimationCalendarShow = () => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true,
      }),
    ]).start();
  };

  const loadAnimatedCalendarFadeOut = () => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 0,
        duration: 3000,
        useNativeDriver: true,
      }),
    ]).start();
  };

  const fetchMarriageDate = () => {
    client
      .query({
        query: GetCustomerDetailQuery,
        fetchPolicy: 'no-cache',
      })
      .then(async response => {
        const {data} = response;
        const {getCustomer, errors} = data;
        const {marriageDate: weddingDate} = getCustomer[0];
        // console.log('response: ', response);
        if (errors === undefined) {
          await setMarriageDate(weddingDate);
        } else {
          await setIsLoading(false);
          await setConnectionError(true);
        }
      })
      .catch(error => {
        console.log('Error: ', error);
        setIsLoading(false);
        setConnectionError(true);
      });
  };

  const getCalendarDataAPI = async () => {
    if (id !== undefined) {
      await fetchMarriageDate();
      await client
        .query({
          query: getAppointmentSlotByYearQuery,
          variables: {
            year: currentYear,
            appointmentId: parseInt(id, 10),
            // addressId: parseInt(selectedAddressId, 10), // sprint 23 backend will handle this validation about address
          },
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Calendar Response API: ', response);
          const {data, errors} = response;
          if (errors === undefined) {
            const {getAppointmentSlotByYear} = data;
            const {allMonths, error} = getAppointmentSlotByYear;
            if (error === null) {
              await setCalendar(allMonths);
              await setIsLoading(false);
              await setIsLoadingCalendarOnly(false);
              await loadAnimationCalendarShow();
            } else {
              await setErrorStatus(error);
              await setIsLoading(false);
              await setIsLoadingCalendarOnly(false);
            }
          } else {
            await setIsLoading(false);
            await setConnectionError(true);
            await setIsLoadingCalendarOnly(false);
          }
        })
        .catch(error => {
          console.log('Error get Calendar Data API: ', error);
          setIsLoading(false);
          setConnectionError(true);
          setIsLoadingCalendarOnly(false);
        });
    } else {
      await setErrorStatus('Appointment Slot Not Found!');
      await setIsLoading(false);
      await setIsLoadingCalendarOnly(false);
    }
  };

  const popStacking = async () => {
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: false,
    });
    navigation.pop();
  };

  const onRefresh = () => {
    setErrorStatus('');
    setIsLoading(true);
    setIsLoadingCalendarOnly(false);
    setConnectionError(false);
    getCalendarDataAPI();
  };

  const backButtonYearNav = () => {
    try {
      console.log('prevCurrentYear: ', prevCurrentYear);
      console.log('currentYear: ', currentYear);
      loadAnimatedCalendarFadeOut();
      setIsLoadingCalendarOnly(true);
      const substractedYear = moment(currentYear)
        .subtract(1, 'y')
        .format('YYYY');
      setCurrentYear(substractedYear);
      getCalendarDataAPI();
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingCalendarOnly(false);
    }
  };

  const rightButtonYearNav = () => {
    try {
      loadAnimatedCalendarFadeOut();
      setIsLoadingCalendarOnly(true);
      const addedYear = moment(currentYear)
        .add(1, 'y')
        .format('YYYY');
      setCurrentYear(addedYear);
      getCalendarDataAPI();
    } catch (error) {
      console.log('Error: ', error);
      setIsLoadingCalendarOnly(false);
    }
  };

  if (isLoading && !connectionError) {
    return <LoaderWithContainer />;
  } else if (!isLoading && connectionError) {
    return <ErrorScreen message="Refresh" onPress={() => onRefresh()} />;
  } else if (!isLoading && errorStatus !== '') {
    return <AppointmentSlotNotFound onPress={() => onRefresh()} />;
  }

  return (
    <Container style={{backgroundColor: white}}>
      <Header
        iosBarStyle="dark-content"
        androidStatusBarColor="white"
        translucent={false}
        style={{
          backgroundColor: 'white',
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <Left style={{flex: 0.1}}>
          <Button
            onPress={() => popStacking()}
            style={{
              elevation: 0,
              backgroundColor: 'transparent',
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: regular,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            Appointment
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {activity ? activity.toUpperCase() : ''}
          </Text>
        </Body>
        <Right style={{flex: 0.1}} />
      </Header>
      <Content contentContainerStyle={{paddingTop: 16, paddingBottom: 16}}>
        <Card
          transparent
          style={{
            marginLeft: 0,
            marginRight: 0,
            shadowOpacity: 0,
            elevation: 0,
          }}>
          <YearNavigation
            year={currentYear}
            backButton={() => backButtonYearNav()}
            rightButton={() => rightButtonYearNav()}
          />
          <Legend footer={true} />
          <CardItem style={{paddingLeft: 0, paddingRight: 0}}>
            {isLoadingCalendarOnly ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Loader />
              </View>
            ) : (
              <DateContainer
                {...props}
                activity={activity}
                appointmentId={id}
                itemName={itemName}
                weddingDate={marriageDate}
                opacity={opacity}
                calendar={calendar}
              />
            )}
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Calendar);

export default props => <Wrapper {...props} />;
