import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Platform,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Icon,
  Header,
  Button,
} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import moment from 'moment';
import Legend from '../../components/Calendar/YearlyCalendar/Legend';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {book, medium} = FontType;
const {regular} = FontSize;
const {black, disableCalendar, white, greyLine, superGrey} = Colors;
const DAY = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

const MonthlyCalendar = props => {
  console.log('MonthlyCalendar: ', props);

  const {navigation, route} = props;
  const {params} = route;
  const {
    appointmentId,
    index,
    calendar,
    weddingDate,
    itemName,
    activity,
  } = params;

  const currentDate = moment().format('DD-MMMM-YYYY');
  const marriageDate = moment(weddingDate).format('DD-MMMM-YYYY');

  const [updatedIndex, setUpdatedIndex] = useState(index);

  useEffect(() => {
    // navigationOptions();
    const subscribe = navigation.addListener('focus', () => {
      // navigationOptions();
    });
    return subscribe;
  }, [navigation]);

  const popStacking = async () => {
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: false,
    });
    navigation.pop();
  };

  const nextMonth = () => {
    if (updatedIndex < calendar.length - 1) {
      setUpdatedIndex(updatedIndex + 1);
    }
  };

  const prevMonth = () => {
    if (updatedIndex > 0) {
      setUpdatedIndex(updatedIndex - 1);
    }
  };

  return (
    <Container style={{backgroundColor: white}}>
      <Header
        iosBarStyle="dark-content"
        androidStatusBarColor="white"
        translucent={false}
        style={{
          backgroundColor: 'white',
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <Left style={{flex: 0.1}}>
          <Button
            onPress={() => popStacking()}
            style={{
              elevation: 0,
              backgroundColor: 'transparent',
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: regular,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            Appointment
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {activity ? activity.toUpperCase() : ''}
          </Text>
        </Body>
        <Right style={{flex: 0.1}} />
      </Header>
      <Content>
        <Card
          transparent
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <CardItem style={{paddingBottom: 0}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: regular,
                lineHeight: 20,
                letterSpacing: 0.3,
                color: '#545454',
              }}>
              Choose your appointment date
            </Text>
          </CardItem>
          <CardItem style={{paddingBottom: 0}}>
            <Left style={{flex: 1}}>
              {updatedIndex === 0 ? null : (
                <TouchableOpacity
                  onPress={() => prevMonth()}
                  style={{
                    width: '100%',
                    height: 30,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Icon
                    type="Feather"
                    name="chevron-left"
                    style={{fontSize: 20, color: black}}
                  />
                </TouchableOpacity>
              )}
            </Left>
            <Body
              style={{
                flex: 2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontWeight: 'bold', color: black}}>
                {calendar[updatedIndex].month}{' '}
                {moment(calendar[updatedIndex].monthYear).format('YYYY')}
              </Text>
            </Body>
            <Right style={{flex: 1}}>
              {updatedIndex === calendar.length - 1 ? null : (
                <TouchableOpacity
                  onPress={() => nextMonth()}
                  style={{
                    width: '100%',
                    height: 30,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Icon
                    type="Feather"
                    name="chevron-right"
                    style={{fontSize: 20, color: black}}
                  />
                </TouchableOpacity>
              )}
            </Right>
          </CardItem>
          <CardItem>
            <FlatList
              scrollEnabled={false}
              data={DAY}
              listKey={(item, i) => `${i}Dates`}
              numColumns={7}
              columnWrapperStyle={{
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
              renderItem={({item}) => {
                return (
                  <View
                    style={{
                      height: 42,
                      width: Dimensions.get('window').width / 9,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 11,
                    }}>
                    <Text style={{fontWeight: 'bold', color: black}}>
                      {item}
                    </Text>
                  </View>
                );
              }}
            />
          </CardItem>
        </Card>
        <Card
          transparent
          style={{
            // paddingTop: 33,
            height: Dimensions.get('window').height / 1.5,
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <CardItem>
            <FlatList
              scrollEnabled={false}
              data={calendar[updatedIndex].datesInMonth}
              listKey={(item, i) => `${i}Dates`}
              numColumns={7}
              columnWrapperStyle={{
                justifyContent: 'space-between',
                alignItems: 'center',
                marginBottom: 10,
              }}
              contentContainerStyle={{marginBottom: 39}}
              renderItem={({item}) => {
                const theDate = moment(item.date).format('DD-MMMM-YYYY');
                const monthStart = moment(calendar[updatedIndex].monthYear)
                  .startOf('month')
                  .format();
                const isSame = moment(theDate).isSame(monthStart, 'month');
                const isBefore = moment(theDate).isBefore(currentDate);
                const fullBooked = item.isFullBooked;
                const isSameDay = moment(theDate).isSame(currentDate);
                const isExceedWeddingDate = moment(theDate).isBefore(
                  marriageDate,
                );

                if (!isSame) {
                  return (
                    <>
                      <TouchableOpacity
                        disabled
                        style={{
                          height: 42,
                          width: Dimensions.get('window').width / 9,
                          borderRadius: 11,
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: white,
                        }}
                      />
                    </>
                  );
                } else {
                  if (fullBooked) {
                    return (
                      <>
                        <TouchableOpacity
                          disabled
                          style={{
                            height: 42,
                            width: Dimensions.get('window').width / 9,
                            borderRadius: 11,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: disableCalendar,
                          }}
                        />
                      </>
                    );
                  } else {
                    if (isBefore) {
                      return (
                        <>
                          <TouchableOpacity
                            disabled
                            style={{
                              height: 42,
                              width: Dimensions.get('window').width / 9,
                              borderRadius: 11,
                              justifyContent: 'center',
                              alignItems: 'center',
                              backgroundColor: disableCalendar,
                            }}
                          />
                        </>
                      );
                    } else if (isSameDay) {
                      return (
                        <>
                          <TouchableOpacity
                            disabled
                            style={{
                              height: 42,
                              width: Dimensions.get('window').width / 9,
                              borderRadius: 11,
                              justifyContent: 'center',
                              alignItems: 'center',
                              backgroundColor: disableCalendar,
                            }}
                          />
                        </>
                      );
                    } else if (!isExceedWeddingDate) {
                      return (
                        <>
                          <TouchableOpacity
                            disabled
                            style={{
                              height: 42,
                              width: Dimensions.get('window').width / 9,
                              borderRadius: 11,
                              justifyContent: 'center',
                              alignItems: 'center',
                              backgroundColor: disableCalendar,
                            }}
                          />
                        </>
                      );
                    } else {
                      return (
                        <>
                          <TouchableOpacity
                            onPress={() => {
                              navigation.navigate('DailyCalendar', {
                                appointmentId,
                                itemName,
                                activity,
                                calendarData:
                                  calendar[updatedIndex].datesInMonth,
                                dateSelected: moment(item.date).format(
                                  'DD-MMMM-YYYY',
                                ),
                                weddingDate: marriageDate,
                              });
                            }}>
                            {Platform.OS === 'android' ? (
                              <LinearGradient
                                start={{x: 0.0, y: 1.8}}
                                end={{x: 1.1, y: 0.0}}
                                locations={[0, 0.5, 0.78]}
                                colors={['#fa878c', '#fa878c', '#fcc0c3']}
                                style={{
                                  height: 42,
                                  width: Dimensions.get('window').width / 9,
                                  borderRadius: 11,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}>
                                <Text
                                  style={{color: white, fontWeight: 'bold'}}>
                                  {moment(item.date).format('D')}
                                </Text>
                              </LinearGradient>
                            ) : (
                              <LinearGradient
                                start={{x: 0.15, y: 1.15}}
                                end={{x: 0.95, y: 0.19}}
                                locations={[0.6, 0.3, 0.2]}
                                colors={[
                                  '#fa878c',
                                  '#fa878c',
                                  '#fa878c',
                                  '#fcc0c3',
                                ]}
                                style={{
                                  height: 42,
                                  width: Dimensions.get('window').width / 9,
                                  borderRadius: 11,
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                }}>
                                <Text
                                  style={{color: white, fontWeight: 'bold'}}>
                                  {moment(item.date).format('D')}
                                </Text>
                              </LinearGradient>
                            )}
                          </TouchableOpacity>
                        </>
                      );
                    }
                  }
                }
              }}
            />
          </CardItem>
          <Legend footer={false} />
        </Card>
      </Content>
    </Container>
  );
};

const Wrapper = compose(withApollo)(MonthlyCalendar);

export default props => <Wrapper {...props} />;
