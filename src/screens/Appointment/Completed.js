import React, {useEffect, useState, useMemo} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  Dimensions,
  TouchableOpacity,
  RefreshControl,
  Image,
  Animated,
} from 'react-native';
import {Container, Content, Card, CardItem} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {withApollo} from 'react-apollo';
import {connect} from 'react-redux';
import compose from 'lodash/fp/compose';
import Loader from '../../components/Loader/circleLoader';
import getInCompleteAppointment from '../../graphql/queries/getCustomerAppointments';
import {CommonActions} from '@react-navigation/native';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncCredential from '../../utils/AsyncstorageDataStructure/index';
import AsyncStorage from '@react-native-community/async-storage';
import {appIcon} from '../../utils/Themes/Images';
import moment from 'moment';

const {confirmedIcon} = appIcon;
const {asyncToken} = AsyncCredential;
const {medium, book} = FontType;
const {white, superGrey, black} = Colors;

const {height, width} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

// screens
import NoTransactions from '../../components/CommonScreens/NoTransactions';
import NoLoginYet from '../../components/CommonScreens/NoLoginYet';

export const ListInCompleteCard = props => {
  const {completedAppointment, goToAppointmentDetail} = props;

  const keyExt = (item, _) => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderListCard = () =>
    useMemo(() => {
      return (
        <FlatList
          ListEmptyComponent={() => {
            return (
              <NoTransactions
                titleText="No complete appointments yet."
                subText="Your appointment will appear here once you've made purchase and approved by our side."
              />
            );
          }}
          contentContainerStyle={{
            paddingTop: 5,
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 80,
          }}
          data={completedAppointment}
          extraData={completedAppointment}
          keyExtractor={keyExt}
          disableVirtualization
          legacyImplementation
          getItemLayout={getItemLayout}
          renderItem={({item}) => {
            const {id, status, activity, merchantDetails} = item;
            return (
              <>
                {status === 'Confirmed' ? (
                  <TouchableOpacity
                    style={{marginBottom: 15}}
                    onPress={() => goToAppointmentDetail(id)}>
                    <Card
                      style={{borderRadius: 4, elevation: 0, shadowOpacity: 0}}>
                      <CardItem style={{backgroundColor: 'transparent'}}>
                        <View style={{flexDirection: 'column', width: '100%'}}>
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              justifyContent: 'flex-start',
                              alignItems: 'center',
                            }}>
                            {item?.updatedOn ? (
                              <Text
                                style={{
                                  fontStyle: 'italic',
                                  marginBottom: 15,
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.3),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                {moment(item?.updatedOn)
                                  .local()
                                  .format('DD MMMM YYYY, hh:mm A')
                                  ? moment(item?.updatedOn)
                                      .local()
                                      .format('DD MMMM YYYY, hh:mm A')
                                  : ''}
                              </Text>
                            ) : null}
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 1,
                                flexDirection: 'column',
                              }}>
                              <Text
                                style={{
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.5),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                {status} Appointment
                              </Text>
                              <Text
                                style={{
                                  marginVertical: 5,
                                  fontFamily: medium,
                                  fontSize: RFPercentage(1.9),
                                  color: black,
                                  letterSpacing: 0.3,
                                  lineHeight: 20,
                                }}>
                                {activity}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.5),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                With{' '}
                                {merchantDetails?.name
                                  ? merchantDetails.name
                                  : 'N/A'}
                              </Text>
                            </View>
                            <View
                              style={{
                                flex: 0.25,
                                alignItems: 'flex-end',
                              }}>
                              <Image
                                source={confirmedIcon}
                                style={{width: width / 10, height: height / 18}}
                                resizeMode="contain"
                              />
                            </View>
                          </View>
                        </View>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                ) : null}
              </>
            );
          }}
        />
      );
    }, [completedAppointment]);

  if (completedAppointment) {
    return RenderListCard();
  } else {
    return null;
  }
};

const Completed = props => {
  console.log('Completed: ', props);
  const {navigation, client, positionYBottomNav} = props;

  const [paramsType, setParamsType] = useState('Completed');

  const [completedAppointment, setCompletedAppointment] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLogin, setIsLogin] = useState(false);
  const [isError, setIsError] = useState(false);
  const [refreshing, onRefresh] = useState(false);

  useEffect(() => {
    fetch();
    onChangeOpacity(true);
    const subScribeTab = navigation.addListener('tabPress', () => {
      console.log('TAB PRESSED');
      refetch();
    });

    const focusSubscribe = navigation.addListener('focus', () => {
      console.log('Focus');
      refetch();
      onChangeOpacity(true);
    });
    return () => {
      subScribeTab;
      focusSubscribe;
    };
  }, [navigation, refetch, paramsType, onChangeParamsType]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const checkLogin = () => {
    return new Promise(async resolve => {
      try {
        const checkingToken = await AsyncStorage.getItem(asyncToken);

        if (checkingToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve(false);
      }
    });
  };

  const goToLogin = () => {
    try {
      navigation.dispatch(
        CommonActions.navigate({name: 'Login', params: {showXIcon: true}}),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  const goToAppointmentDetail = id => {
    try {
      console.log('ID Bro: ', id);
      navigation.navigate('AppointmentDetail', {id});
    } catch (error) {
      console.log('Error goToAppointmentDetail: ', error);
    }
  };

  const fetch = async () => {
    try {
      console.log('Params Type Ma Bruh: ', paramsType);
      const checkingToken = await checkLogin();
      console.log('checkingToken >>>>>>>>> ', checkingToken);
      if (checkingToken) {
        await client
          .query({
            query: getInCompleteAppointment,
            variables: {
              status: paramsType,
            },
            fetchPolicy: 'no-cache',
            notifyOnNetworkStatusChange: true,
          })
          .then(async response => {
            console.log('Response Completed Appointments: ', response);
            const {data, errors} = response;
            const {getCustomerAppointments} = data;
            const {data: appointmentList, error} = getCustomerAppointments;

            if (errors) {
              await setIsError(true);
              await setIsLogin(false);
              await setIsLoading(false);
              await onRefresh(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLogin(false);
                await setIsLoading(false);
                await onRefresh(false);
              } else {
                await setCompletedAppointment([...appointmentList]);
                // await setCompletedAppointment([]);
                await setIsError(false);
                await setIsLogin(true);
                await setIsLoading(false);
                await onRefresh(false);
              }
            }
          })
          .catch(async error => {
            console.log('Error: ', error);
            await setIsError(true);
            await setIsLogin(false);
            await setIsLoading(false);
            await onRefresh(false);
          });
      } else {
        await setIsLogin(false);
        await setIsError(false);
        await setIsLoading(false);
        await onRefresh(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLogin(false);
      await setIsLoading(false);
      await onRefresh(false);
    }
  };

  const refetch = async () => {
    try {
      await onRefresh(true);
      await setIsError(false);
      await fetch();
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await onRefresh(false);
    }
  };

  const onChangeParamsType = async type => {
    try {
      if (type === paramsType) {
        await onRefresh(true);
        await setParamsType('Completed');
      } else {
        await onRefresh(true);
        await setParamsType(type);
      }
    } catch (error) {
      await onRefresh(false);
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError && !isLogin) {
    // Loading Screen
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else if (!isLoading && isError && !isLogin) {
    // Errror Screen
    return <ErrorScreen message="Refresh" onPress={() => refetch()} />;
  } else if (!isLoading && !isError && isLogin) {
    // show List
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <Content
          contentContainerStyle={{paddingTop: 15}}
          onScrollBeginDrag={() => onChangeOpacity(false)}
          onScrollEndDrag={() => onChangeOpacity(true)}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => refetch()}
            />
          }>
          <ListInCompleteCard
            completedAppointment={completedAppointment}
            goToAppointmentDetail={id => goToAppointmentDetail(id)}
          />
        </Content>
      </Container>
    );
  } else {
    // Not login Yet
    return (
      <Container>
        <StatusBar barStyle="dark-content" />
        <Content>
          <NoLoginYet
            titleText="No completed appointments yet."
            subText="Your appointment will appear here once you've made purchase. Login to view and manage them with ease."
            onPress={() => goToLogin()}
          />
        </Content>
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(Completed);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
