import React, {useEffect, useState, useMemo} from 'react';
import {
  Text,
  View,
  StatusBar,
  FlatList,
  Dimensions,
  TouchableOpacity,
  RefreshControl,
  Image,
  Animated,
} from 'react-native';
import {Container, Content, Card, CardItem, Icon} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {withApollo} from 'react-apollo';
import {connect} from 'react-redux';
import compose from 'lodash/fp/compose';
import Loader from '../../components/Loader/circleLoader';
import getInCompleteAppointment from '../../graphql/queries/getCustomerAppointments';
import {CommonActions} from '@react-navigation/native';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {FontType} from '../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncCredential from '../../utils/AsyncstorageDataStructure/index';
import AsyncStorage from '@react-native-community/async-storage';
import {appIcon} from '../../utils/Themes/Images';
import moment from 'moment';

const {rescheduledIcon, haventbookIcon, sentIcon} = appIcon;
const {asyncToken} = AsyncCredential;
const {medium, book} = FontType;
const {white, mainRed, superGrey, black} = Colors;

const {height, width} = Dimensions.get('window');
const ITEM_HEIGHT = height * 0.25;

const appointmentTypeParams = [
  {
    id: 1,
    type: "Haven't Book",
  },
  {
    id: 2,
    type: 'Reschedule',
  },
  {
    id: 3,
    type: 'Sent',
  },
];

// screens
import NoTransactions from '../../components/CommonScreens/NoTransactions';
import NoLoginYet from '../../components/CommonScreens/NoLoginYet';

export const ListInCompleteCard = props => {
  const {inCompleteAppointment, goToAppointmentDetail, navigation} = props;

  const keyExt = (item, _) => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    };
  };

  const RenderListCard = () =>
    useMemo(() => {
      return (
        <FlatList
          ListEmptyComponent={() => {
            return (
              <NoTransactions
                titleText="No incomplete appointments yet."
                subText="Your appointment will appear here once you've made purchase."
              />
            );
          }}
          contentContainerStyle={{
            paddingTop: 5,
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 80,
          }}
          data={inCompleteAppointment}
          extraData={inCompleteAppointment}
          keyExtractor={keyExt}
          disableVirtualization
          legacyImplementation
          getItemLayout={getItemLayout}
          renderItem={({item}) => {
            console.log('ITEM INCLOMPLETE: ', item);
            const {id, status, activity, merchantDetails} = item;
            const source =
              status === "Haven't Book"
                ? haventbookIcon
                : status === 'Sent'
                ? sentIcon
                : rescheduledIcon;
            return (
              <>
                {status === 'Confirm' ? null : (
                  <TouchableOpacity
                    style={{marginBottom: 15}}
                    onPress={() => goToAppointmentDetail(id)}>
                    <Card
                      style={{borderRadius: 4, elevation: 0, shadowOpacity: 0}}>
                      <CardItem style={{backgroundColor: 'transparent'}}>
                        <View style={{flexDirection: 'column', width: '100%'}}>
                          <View
                            style={{
                              flexDirection: 'row',
                              width: '100%',
                              justifyContent: 'flex-start',
                              alignItems: 'center',
                            }}>
                            {item?.updatedOn ? (
                              <Text
                                style={{
                                  fontStyle: 'italic',
                                  marginBottom: 15,
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.3),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                {moment(item?.updatedOn)
                                  .local()
                                  .format('DD MMMM YYYY, hh:mm A')
                                  ? moment(item?.updatedOn)
                                      .local()
                                      .format('DD MMMM YYYY, hh:mm A')
                                  : ''}
                              </Text>
                            ) : null}
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 1,
                                flexDirection: 'column',
                              }}>
                              <Text
                                style={{
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.5),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                {status} Appointment
                              </Text>
                              <Text
                                style={{
                                  marginVertical: 5,
                                  fontFamily: medium,
                                  fontSize: RFPercentage(1.9),
                                  color: black,
                                  letterSpacing: 0.3,
                                  lineHeight: 20,
                                }}>
                                {activity}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: book,
                                  fontSize: RFPercentage(1.5),
                                  color: superGrey,
                                  letterSpacing: 0.3,
                                }}>
                                With{' '}
                                {merchantDetails?.name
                                  ? merchantDetails.name
                                  : 'N/A'}
                              </Text>
                            </View>
                            <View
                              style={{
                                flex: 0.25,
                                alignItems: 'flex-end',
                              }}>
                              <Image
                                source={source}
                                style={{width: width / 10, height: height / 18}}
                                resizeMode="contain"
                              />
                            </View>
                          </View>
                          {status === "Haven't Book" ||
                          status === 'Reschedule' ? (
                            <View
                              style={{
                                marginTop: 10,
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent:
                                  item?.activity === 'Pre/Post Select Photos' ||
                                  item?.activity ===
                                    'Pre/Post Gown Selection' ||
                                  item?.activity === 'Pre/Post Final Fitting' ||
                                  item?.activity === 'Viewing of Layout' ||
                                  item?.activity ===
                                    'Album & Frame Collection' ||
                                  item?.activity === 'AD Gown Selection' ||
                                  item?.activity === 'Trial' ||
                                  item?.activity ===
                                    'AD Final Fitting & Pick Up'
                                    ? 'space-between'
                                    : 'flex-end',
                                alignItems: 'center',
                              }}>
                              <TouchableOpacity
                                onPress={() => {
                                  try {
                                    // for Prewed Appointment Guide
                                    if (
                                      item?.activity ===
                                        'Pre/Post Select Photos' ||
                                      item?.activity ===
                                        'Pre/Post Gown Selection' ||
                                      item?.activity ===
                                        'Pre/Post Final Fitting' ||
                                      item?.activity === 'Viewing of Layout' ||
                                      item?.activity ===
                                        'Album & Frame Collection'
                                    ) {
                                      navigation.navigate(
                                        'AppointmentGuidePdfPreview',
                                        {
                                          id: 2,
                                          name: 'Pre Wedding Appointment Guide',
                                          data: 'https://storage.googleapis.com/love-nest-233803.appspot.com/a94027e0dc60ceb6824015943598b38b_files.pdf',
                                        },
                                      );
                                    } else if (
                                      item?.activity === 'AD Gown Selection' ||
                                      item?.activity === 'Trial' ||
                                      item?.activity ===
                                        'AD Final Fitting & Pick Up'
                                    ) {
                                      navigation.navigate(
                                        'AppointmentGuidePdfPreview',
                                        {
                                          id: 1,
                                          name: 'Actual Day Gown Appointment Guide',
                                          data: 'https://storage.googleapis.com/love-nest-233803.appspot.com/fa24458d6f9c60d025538342d0d2e167_files.pdf',
                                        },
                                      );
                                    } else {
                                      //  do nothing
                                    }
                                  } catch (error) {
                                    console.log('Error: ', error);
                                  }
                                }}
                                style={{
                                  display:
                                    item?.activity ===
                                      'Pre/Post Select Photos' ||
                                    item?.activity ===
                                      'Pre/Post Gown Selection' ||
                                    item?.activity ===
                                      'Pre/Post Final Fitting' ||
                                    item?.activity === 'Viewing of Layout' ||
                                    item?.activity ===
                                      'Album & Frame Collection' ||
                                    item?.activity === 'AD Gown Selection' ||
                                    item?.activity === 'Trial' ||
                                    item?.activity ===
                                      'AD Final Fitting & Pick Up'
                                      ? 'flex'
                                      : 'none',
                                  paddingTop: 5,
                                  paddingBottom: 5,
                                  borderWidth: 0,
                                }}>
                                {item?.activity === 'Pre/Post Select Photos' ||
                                item?.activity === 'Pre/Post Gown Selection' ||
                                item?.activity === 'Pre/Post Final Fitting' ||
                                item?.activity === 'Viewing of Layout' ||
                                item?.activity === 'Album & Frame Collection' ||
                                item?.activity === 'AD Gown Selection' ||
                                item?.activity === 'Trial' ||
                                item?.activity ===
                                  'AD Final Fitting & Pick Up' ? (
                                  <Text
                                    style={{
                                      textDecorationLine: 'underline',
                                      fontFamily: medium,
                                      fontSize: RFPercentage(1.3),
                                      color: mainRed,
                                    }}>
                                    Appointment Guide
                                  </Text>
                                ) : null}
                              </TouchableOpacity>

                              <TouchableOpacity
                                onPress={() => goToAppointmentDetail(id)}
                                style={{
                                  flexDirection: 'row',
                                  flexWrap: 'wrap',
                                  padding: 5,
                                  borderWidth: 1,
                                  borderColor: mainRed,
                                  alignItems: 'center',
                                }}>
                                <Text
                                  style={{
                                    fontFamily: medium,
                                    fontSize: RFPercentage(1.6),
                                    color: mainRed,
                                    letterSpacing: 0.3,
                                  }}>
                                  {status === "Haven't Book"
                                    ? 'Book Appointment'
                                    : 'Rebook Appointment'}
                                </Text>
                                <View
                                  style={{
                                    marginLeft: 5,
                                    width: 20,
                                    height: 20,
                                    borderRadius: 25 / 2,
                                    borderWidth: 1,
                                    borderColor: mainRed,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}>
                                  <Icon
                                    type="Feather"
                                    name="arrow-right"
                                    style={{
                                      top: 1,
                                      left: 10,
                                      fontSize: 15,
                                      color: mainRed,
                                    }}
                                  />
                                </View>
                              </TouchableOpacity>
                            </View>
                          ) : null}
                        </View>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                )}
              </>
            );
          }}
        />
      );
    }, [inCompleteAppointment]);

  if (inCompleteAppointment) {
    return RenderListCard();
  } else {
    return null;
  }
};

export const ListParamsType = props => {
  const {paramsType, onChangeParamsType} = props;
  const keyExt = item => `${String(item.id)}`;
  return (
    <FlatList
      data={appointmentTypeParams}
      extraData={appointmentTypeParams}
      keyExtractor={keyExt}
      listKey={keyExt}
      horizontal
      contentContainerStyle={{
        marginTop: 10,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        paddingBottom: 10,
      }}
      renderItem={({item}) => {
        return (
          <TouchableOpacity
            onPress={() => {
              onChangeParamsType(item.type);
            }}
            style={{
              backgroundColor: item.type === paramsType ? mainRed : white,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginRight: 10,
              borderWidth: 1,
              borderRadius: 25,
              borderColor: mainRed,
              justifyContent: 'center',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 8,
              paddingBottom: 8,
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: paramsType === item.type ? white : mainRed,
                letterSpacing: 0.3,
              }}>
              {item.type}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const InCompleted = props => {
  console.log('InCompleted: ', props);
  const {navigation, client, positionYBottomNav} = props;

  const [paramsType, setParamsType] = useState('Incompleted');

  const [inCompleteAppointment, setInCompleteAppointment] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLogin, setIsLogin] = useState(false);
  const [isError, setIsError] = useState(false);
  const [refreshing, onRefresh] = useState(false);

  useEffect(() => {
    fetch();
    onChangeOpacity(true);

    const focusSubscribe = navigation.addListener('focus', () => {
      console.log('Focus');
      refetch();
      onChangeOpacity(true);
    });
    return () => {
      // subScribeTab;
      focusSubscribe();
    };
  }, [navigation, refetch, paramsType, onChangeParamsType]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const checkLogin = () => {
    return new Promise(async resolve => {
      try {
        const checkingToken = await AsyncStorage.getItem(asyncToken);

        if (checkingToken) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (error) {
        console.log('Error: ', error);
        resolve(false);
      }
    });
  };

  const goToLogin = () => {
    try {
      navigation.dispatch(
        CommonActions.navigate({name: 'Login', params: {showXIcon: true}}),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  const goToAppointmentDetail = id => {
    try {
      console.log('ID Bro: ', id);
      navigation.navigate('AppointmentDetail', {id});
    } catch (error) {
      console.log('Error goToAppointmentDetail: ', error);
    }
  };

  const fetch = async () => {
    try {
      console.log('Params Type Ma Bruh: ', paramsType);
      const checkingToken = await checkLogin();
      console.log('checkingToken >>>>>>>>> ', checkingToken);
      if (checkingToken) {
        await client
          .query({
            query: getInCompleteAppointment,
            variables: {
              status: paramsType,
            },
            fetchPolicy: 'no-cache',
            notifyOnNetworkStatusChange: true,
          })
          .then(async response => {
            console.log('Response InComplete Appointments: ', response);
            const {data, errors} = response;
            const {getCustomerAppointments} = data;
            const {data: appointmentList, error} = getCustomerAppointments;

            if (errors) {
              await setIsError(true);
              await setIsLogin(false);
              await setIsLoading(false);
              await onRefresh(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLogin(false);
                await setIsLoading(false);
                await onRefresh(false);
              } else {
                await setInCompleteAppointment([...appointmentList]);
                // await setInCompleteAppointment([]);
                await setIsError(false);
                await setIsLogin(true);
                await setIsLoading(false);
                await onRefresh(false);
              }
            }
          })
          .catch(async error => {
            console.log('Error: ', error);
            await setIsError(true);
            await setIsLogin(false);
            await setIsLoading(false);
            await onRefresh(false);
          });
      } else {
        await setIsLogin(false);
        await setIsError(false);
        await setIsLoading(false);
        await onRefresh(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLogin(false);
      await setIsLoading(false);
      await onRefresh(false);
    }
  };

  const refetch = async () => {
    try {
      await onRefresh(true);
      await setIsError(false);
      await fetch();
    } catch (error) {
      await setIsError(true);
      await setIsLoading(false);
      await onRefresh(false);
    }
  };

  const onChangeParamsType = async type => {
    try {
      if (type === paramsType) {
        await onRefresh(true);
        await setParamsType('Incompleted');
      } else {
        await onRefresh(true);
        await setParamsType(type);
      }
    } catch (error) {
      await onRefresh(false);
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError && !isLogin) {
    // Loading Screen
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <StatusBar barStyle="dark-content" />
        <Loader />
      </View>
    );
  } else if (!isLoading && isError && !isLogin) {
    // Errror Screen
    return <ErrorScreen message="Refresh" onPress={() => refetch()} />;
  } else if (!isLoading && !isError && isLogin) {
    // show List
    return (
      <Container style={{backgroundColor: white}}>
        <StatusBar barStyle="dark-content" />
        <Content
          onScrollBeginDrag={() => onChangeOpacity(false)}
          onScrollEndDrag={() => onChangeOpacity(true)}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => refetch()}
            />
          }>
          {paramsType === 'Incompleted' &&
          inCompleteAppointment.length === 0 ? null : (
            <ListParamsType
              paramsType={paramsType}
              onChangeParamsType={type => onChangeParamsType(type)}
            />
          )}
          <ListInCompleteCard
            navigation={props?.navigation}
            inCompleteAppointment={inCompleteAppointment}
            goToAppointmentDetail={id => goToAppointmentDetail(id)}
          />
        </Content>
      </Container>
    );
  } else {
    // Not login Yet
    return (
      <Container>
        <StatusBar barStyle="dark-content" />
        <Content>
          <NoLoginYet
            titleText="No incomplete appointments yet."
            subText="Your appointment will appear here once you've made purchase. Login to view and manage them with ease."
            onPress={() => goToLogin()}
          />
        </Content>
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(InCompleted);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
