import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  Animated,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import BigCard from '../../components/Cards/Appointments/bigCard';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Button,
} from 'native-base';
import Loader from '../../components/Loader/circleLoader';
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import queryAppointmentDetail from '../../graphql/queries/getAppointmentDetails';
import Buttons from '../../components/Button/buttonFixToBottom';
import OrderPackageList from '../../components/Cards/OrderPackageList';
import AdditionalItemList from '../../components/Cards/AdditionalItemList';
import {CommonActions} from '@react-navigation/native';

// Helpers
import PackageListHelper from '../Order/Helpers/PackageListHelper';
import AdditionalItemHelper from '../Order/Helpers/AdditionalItems';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, newContainerColor, greyLine, mainRed, superGrey} = Colors;
const {medium} = FontType;
const {regular} = FontSize;

const yellowStatus = "Haven't Book";
const redStatus = 'Reschedule';

const redButtonText = 'REBOOK APPOINTMENT';
const yellowButtonText = 'BOOK APPOINTMENT';
const anotherButtonText = 'VIEW ORDER DETAIL';

const {width, height} = Dimensions?.get('screen');

// Appointment Guide AD Gown
import adgown1 from '../../static/adgown/1.jpg';
import adgown2 from '../../static/adgown/2.jpg';
import adgown3 from '../../static/adgown/3.jpg';
import adgown4 from '../../static/adgown/4.jpg';
import adgown5 from '../../static/adgown/5.jpg';
import adgown6 from '../../static/adgown/6.jpg';
import adgown7 from '../../static/adgown/6.jpg';

// Appointment Guide Pre Wedding
import prewed1 from '../../static/prewed/1.jpg';
import prewed2 from '../../static/prewed/2.jpg';
import prewed3 from '../../static/prewed/3.jpg';
import prewed4 from '../../static/prewed/4.jpg';
import prewed5 from '../../static/prewed/5.jpg';
import prewed6 from '../../static/prewed/6.jpg';
import prewed7 from '../../static/prewed/7.png';

export const ViewOrderDetail = props => {
  const {orderId, navigation} = props;

  const goToOrderDetail = () => {
    try {
      navigation.dispatch(
        CommonActions.navigate({
          // this commonAction to navigate un related screen
          name: 'Orders',
          params: {
            screen: 'OrderDetail', // property should named as [screen]
            params: {
              id: orderId,
            },
          },
        }),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };
  return (
    <Card
      transparent
      style={{
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <CardItem
        style={{backgroundColor: 'transparent', paddingTop: 0}}
        button
        onPress={() => {
          console.log('View Order Detail: ', orderId);
          goToOrderDetail();
        }}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              top: 1,
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: mainRed,
              letterSpacing: 0.3,
            }}>
            View Detail Order
          </Text>
          <Icon
            type="Feather"
            name="arrow-right"
            style={{marginLeft: 5, fontSize: 17, color: mainRed}}
          />
        </View>
      </CardItem>
    </Card>
  );
};

export const ViewOrderDetailValidation = props => {
  const {appointmentDetail, cardStatus} = props;
  const orderId =
    appointmentDetail !== null || appointmentDetail !== undefined
      ? appointmentDetail.order !== null ||
        appointmentDetail.order !== undefined
        ? appointmentDetail.order.id !== null ||
          appointmentDetail.order.id !== undefined
          ? appointmentDetail.order.id
          : null
        : null
      : null;
  if (orderId === null) {
    return null;
  } else {
    if (cardStatus === yellowStatus) {
      return <ViewOrderDetail {...props} orderId={orderId} />;
    } else if (cardStatus === redStatus) {
      return <ViewOrderDetail {...props} orderId={orderId} />;
    } else {
      return null;
    }
  }
};

const AppointmentDetail = props => {
  console.log('AppointmentDetail Props: ', props);
  const {navigation, client, route, positionYBottomNav} = props;
  const {params} = route;
  const {id} = params;

  let [appId, setAppId] = useState(null);
  let [appointmentDetail, setAppointmentDetail] = useState(null);
  let [isLoading, setIsLoading] = useState(true);
  let [networkError, setNetWorkError] = useState(false);
  let [errorMessage, setErrorMessage] = useState('');
  let [cardStatus, setCardStatus] = useState(null);
  let [bookButtonIsLoading, setBookButtonIsLoading] = useState(false);

  useEffect(() => {
    // navigationOptions();
    StatusBar.setTranslucent(false);
    settingAppId(id);
    getAppointmentDetail();

    if (positionYBottomNav) {
      onChangeOpacity(false);
    }

    const subscribe = navigation.addListener('focus', () => {
      console.log('Masuk Sini Kleng!');
      // navigationOptions();
      StatusBar.setTranslucent(false);
      settingAppId(id);
      getAppointmentDetail();

      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });
    return subscribe;
  }, [navigation, isLoading, cardStatus, appId]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const getAppointmentDetail = () => {
    try {
      client
        .query({
          query: queryAppointmentDetail,
          variables: {
            id: appId,
          },
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(async res => {
          console.log('getAppointmentDetail Res: ', res);
          const {data, errors} = res;
          if (errors === undefined) {
            const {getAppointmentDetails: detail} = data;
            const {data: detailApp, error} = detail;
            const {order} = detailApp;

            if (error === null) {
              const {status} = detailApp;

              const packageLists = await PackageListHelper(order);
              detailApp.order.packageLists = packageLists;

              const additionalItems = await AdditionalItemHelper(order);
              detailApp.order.additionalItems = additionalItems;

              await settingAppointmentDetail(detailApp);
              await settingCardStatus(status);
              await settingIsLoading(false);
            } else {
              await settingErrorMessage(error);
              await settingNetworkError(true);
              await settingIsLoading(false);
            }
          } else {
            await settingNetworkError(true);
            await settingIsLoading(false);
          }
        })
        .catch(async error => {
          console.log('Error Query: ', error);
          await settingNetworkError(true);
          await settingIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      settingNetworkError(true);
      settingIsLoading(false);
    }
  };

  const setCollapsePackageList = async index => {
    try {
      console.log('Index collapse >>> ', index);
      let oldData = {...appointmentDetail};
      oldData.order.packageLists[index].collapse =
        !oldData.order.packageLists[index].collapse;
      console.log('oldData >>>>>>>> ', oldData);
      await settingAppointmentDetail(oldData);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const setCollapseAdditionalItem = async index => {
    try {
      let oldData = {...appointmentDetail};
      oldData.order.additionalItems[index].collapse =
        !oldData.order.additionalItems[index].collapse;

      await settingAppointmentDetail(oldData);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const settingAppId = value => {
    appId = value;
    setAppId(appId);
  };

  const settingNetworkError = value => {
    networkError = value;
    setNetWorkError(networkError);
  };

  const settingErrorMessage = value => {
    errorMessage = value;
    setErrorMessage(errorMessage);
  };

  const settingAppointmentDetail = value => {
    appointmentDetail = value;
    setAppointmentDetail(appointmentDetail);
  };

  const settingIsLoading = value => {
    isLoading = value;
    setIsLoading(isLoading);
  };

  const settingCardStatus = value => {
    cardStatus = value;
    setCardStatus(cardStatus);
  };

  const onRefresh = () => {
    settingIsLoading(true);
    settingNetworkError(false);
    getAppointmentDetail();
  };

  const popStacking = () => {
    try {
      // await navigation.dangerouslyGetParent().setOptions({
      //   tabBarVisible: true,
      // });
      navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const BookingAppointment = async () => {
    try {
      console.log('Appointment Detail Pressed button: ', appointmentDetail);
      await setBookButtonIsLoading(true);
      await setBookButtonIsLoading(false);
      navigation.navigate('Calendar', {
        id: appointmentDetail.id,
        itemName: appointmentDetail.packageName,
        activity: appointmentDetail?.activity ? appointmentDetail.activity : '',
        selectedAddress: null,
      });
    } catch (error) {
      await setBookButtonIsLoading(false);
    }
  };

  const ImageStep = () => {
    if (!appointmentDetail) {
      return null;
    } else {
      if (appointmentDetail?.activity === 'Pre/Post Select Photos') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={prewed5}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'Pre/Post Gown Selection') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={prewed2}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'Pre/Post Final Fitting') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={prewed3}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'Viewing of Layout') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={prewed6}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'Album & Frame Collection') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={prewed7}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'AD Gown Selection') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={adgown2}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'Trial') {
        return (
          <Card transparent>
            <CardItem style={{width: '100%', height: width * 0.5}}>
              <Image
                source={adgown3}
                style={{flex: 1, width: null, height: width * 0.5}}
              />
            </CardItem>
          </Card>
        );
      } else if (appointmentDetail?.activity === 'AD Final Fitting & Pick Up') {
        return (
          <View style={{flexDirection: 'row', borderWidth: 0}}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal
              contentContainerStyle={{
                paddingTop: 20,
                paddingBottom: 8,
                paddingLeft: 10,
                paddingRight: 10,
              }}
              pagingEnabled>
              <View
                style={{
                  marginHorizontal: 10,
                  height: width * 0.5,
                  width: width * 0.8,
                  borderWidth: 0,
                }}>
                <Image
                  source={adgown4}
                  style={{flex: 1, width: null, height: width * 0.5}}
                />
              </View>
              <View
                style={{
                  marginHorizontal: 10,
                  height: width * 0.5,
                  width: width * 0.8,
                  borderWidth: 0,
                }}>
                <Image
                  source={adgown5}
                  style={{flex: 1, width: null, height: width * 0.5}}
                />
              </View>
              <View
                style={{
                  marginHorizontal: 10,
                  height: width * 0.5,
                  width: width * 0.8,
                  borderWidth: 0,
                }}>
                <Image
                  source={adgown6}
                  style={{flex: 1, width: null, height: width * 0.5}}
                />
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return null;
      }
    }
  };

  if (isLoading && !networkError) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loader />
        </Content>
      </Container>
    );
  } else if (!isLoading && networkError) {
    return <ErrorScreen message="Refresh" onPress={() => onRefresh()} />;
  }

  return (
    <Container style={{backgroundColor: 'white'}}>
      <Header
        iosBarStyle="dark-content"
        androidStatusBarColor="white"
        translucent={false}
        style={{
          elevation: 0,
          shadowOpacity: 0,
          backgroundColor: 'white',
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <Left style={{flex: 0.1}}>
          <Button
            onPress={() => popStacking()}
            style={{
              elevation: 0,
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <Icon
              type="Feather"
              name="chevron-left"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            Appointment Detail
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {appointmentDetail?.status
              ? appointmentDetail.status.toUpperCase()
              : ''}
          </Text>
        </Body>
        <Right style={{flex: 0.1}} />
      </Header>
      <Content contentContainerStyle={{paddingTop: 5, paddingBottom: 15}}>
        {appointmentDetail === null ? null : (
          <BigCard
            appointmentData={appointmentDetail}
            cardStatus={cardStatus}
          />
        )}
        <ImageStep />
        {!appointmentDetail ? null : (
          <OrderPackageList
            singlePreview={true}
            orderProps={
              appointmentDetail?.order ? appointmentDetail.order : null
            }
            setCollapsePackageList={setCollapsePackageList}
          />
        )}
        {!appointmentDetail ? null : (
          <AdditionalItemList
            singlePreview={true}
            orderProps={
              appointmentDetail?.order ? appointmentDetail.order : null
            }
            setCollapseAdditionalItem={setCollapseAdditionalItem}
          />
        )}
        {appointmentDetail === null ||
        appointmentDetail === undefined ? null : (
          <ViewOrderDetailValidation
            appointmentDetail={appointmentDetail}
            cardStatus={cardStatus}
            {...props}
          />
        )}
      </Content>
      <Buttons
        disable={bookButtonIsLoading === true ? true : false}
        isLoading={bookButtonIsLoading}
        text={() => {
          if (cardStatus === yellowStatus) {
            return yellowButtonText;
          } else if (cardStatus === redStatus) {
            return redButtonText;
          } else {
            return anotherButtonText;
          }
        }}
        onPress={() => {
          try {
            if (cardStatus === yellowStatus) {
              BookingAppointment(appointmentDetail);
            } else if (cardStatus === redStatus) {
              BookingAppointment(appointmentDetail);
            } else {
              // GO TO ORDER DETAIL
              const orderId =
                appointmentDetail !== null || appointmentDetail !== undefined
                  ? appointmentDetail.order !== null ||
                    appointmentDetail.order !== undefined
                    ? appointmentDetail.order.id !== null ||
                      appointmentDetail.order.id !== undefined
                      ? appointmentDetail.order.id
                      : null
                    : null
                  : null;

              if (orderId !== null || orderId !== undefined) {
                navigation.navigate('Orders', {
                  screen: 'OrderDetail',
                  params: {id: orderId},
                });
              }
            }
          } catch (error) {
            console.log('Appointment Detail button Error: ', error);
            setBookButtonIsLoading(false);
          }
        }}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(AppointmentDetail);

export default props => <Wrapper {...props} />;
