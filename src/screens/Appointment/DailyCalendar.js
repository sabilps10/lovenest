import React, {useState, useEffect, useRef, useMemo} from 'react';
import {Text, Dimensions, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Card,
  CardItem,
  Body,
  Header,
  Left,
  Right,
  Button,
  Icon,
} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import moment from 'moment';
import LoaderWithContainer from '../../components/Loader/LoaderWithContainer';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Buttons from '../../components/Button/buttonFixToBottom';
import ButtonBack from '../../components/Button/buttonBack';
import TabBar from '../../components/Calendar/DailyCalendar/TabBar';
import TimeSlot from '../../components/Calendar/DailyCalendar/TimeSlot';
import bookAppointmentMutation from '../../graphql/mutations/bookAppointment';
import ErrorToaster from '../../components/Toaster/errorToaster';
import SuccessToaster from '../../components/Toaster/successToaster';
import {CommonActions} from '@react-navigation/native';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, superGrey, white, greyLine} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const Tab = createMaterialTopTabNavigator();

const DailyCalendar = props => {
  console.log('DailyCalendar Props: ', props);
  let tabRef = useRef();

  const {navigation, route, client} = props;
  const {params} = route;
  const {
    appointmentId,
    dateSelected,
    itemName,
    calendarData,
    weddingDate,
    activity,
  } = params;
  const inTheMonth = moment(dateSelected).format('DD-MMMM-YYYY');

  const [calendar, setCalendar] = useState(null);
  const [indexes, setIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedTimeSlot, setSelectedTimeSlot] = useState(null);
  const [buttonIsLoading, setButtonIsLoading] = useState(false);
  const [failedBook, setFailedBook] = useState(false);
  const [failedTextBook, setFailedTextBook] = useState('');
  const [successBook, setSuccessBook] = useState(false);
  const [successTextBook, setSuccessTextBook] = useState('');

  useEffect(() => {
    // navigationOptions();
    filteringInMonthCalendar();

    if (!isLoading) {
      if (indexes !== null || indexes !== -1) {
        if (tabRef?.current?.scrollToIndex !== undefined) {
          tabRef.current.scrollToIndex({
            animated: true,
            index: indexes === -1 ? 0 : indexes,
            viewOffset: Dimensions.get('window').width / 2.5,
          });
        }
      }
    }

    const unsubscribe = navigation.addListener('tabPress', e => {
      console.log('TAB PRESS DAILY: ', e);
      // Prevent default behavior
      e.preventDefault();

      // Do something manually
      // ...

      if (!isLoading) {
        if (indexes !== null || indexes !== -1) {
          if (tabRef?.current?.scrollToIndex !== undefined) {
            tabRef.current.scrollToIndex({
              animated: true,
              index: indexes === -1 ? 0 : indexes,
              viewOffset: Dimensions.get('window').width / 2.5,
            });
          }
        }
      }
    });

    return unsubscribe;
  }, [navigation, isLoading]);

  const settingSelectedTimeSlot = selectedSlotTime => {
    console.log('Dialy Calendar selectedSlot: ', selectedSlotTime);
    setSelectedTimeSlot(selectedSlotTime);
  };

  const manipulateCalendar = () => {
    return new Promise(async (resolve, reject) => {
      try {
        let newCalendar = [];
        await calendarData.map(d => {
          const theDate = moment(d?.date).format('DD-MMMM-YYYY');
          if (moment(theDate).isSame(inTheMonth, 'month')) {
            newCalendar.push(d);
          }
        });
        if (
          newCalendar.length === 31 ||
          newCalendar.length === 30 ||
          newCalendar.length === 29 ||
          newCalendar.length === 28
        ) {
          resolve({data: newCalendar, error: null});
        }
      } catch (error) {
        reject({
          data: null,
          error,
        });
      }
    });
  };

  const filteringInMonthCalendar = async () => {
    const newCalendar = await manipulateCalendar();

    const getIndex = await Promise.all(
      newCalendar?.data
        .map((e, i) => {
          const anu = moment(e?.date).format('YYYY-MM-DD');
          const newIntheMonth = moment(inTheMonth).format('YYYY-MM-DD');
          const sama = moment(anu).isSame(newIntheMonth);
          console.log('SAMA GA ? ', sama);
          if (sama) {
            return {index: i};
          } else {
            return null;
          }
        })
        .filter(Boolean),
    );
    console.log('getIndex >>>>>> ', getIndex);
    if (newCalendar && getIndex) {
      await setIndex(getIndex[0]?.index ? getIndex[0]?.index : 0);
      await setCalendar(newCalendar.data);
      await setIsLoading(false);
    }
  };

  const popStacking = async () => {
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: false,
    });
    navigation.pop();
  };
  const updatingSelectedIndex = index => {
    console.log('indexes 1: ', indexes);
    setIndex(index);
    console.log('indexes 2: ', indexes);
  };

  const settingButtonLoading = value => {
    setButtonIsLoading(value);
  };

  const bookAppointmentFunc = async () => {
    try {
      const finalSelectedCalendar = moment(calendar[indexes].date).format(
        'DD MMMM YYYY',
      );
      const finalSelectedTimeSlot = selectedTimeSlot;
      const finalAppointmentId = appointmentId;

      await setFailedBook(false);
      await setFailedTextBook('');
      await setSuccessBook(false);
      await setSuccessTextBook('');
      await settingButtonLoading(true);

      if (finalSelectedTimeSlot === null) {
        // show Toaster Error
        settingButtonLoading(false);
        setFailedBook(true);
        setFailedTextBook('Time slot not selected!');
      } else {
        client
          .mutate({
            mutation: bookAppointmentMutation,
            variables: {
              date: finalSelectedCalendar,
              time: finalSelectedTimeSlot,
              appointmentId: finalAppointmentId,
            },
          })
          .then(async response => {
            console.log('Response Book: ', response);
            const {data} = response;
            const {bookAppointment} = data;
            const {error} = bookAppointment;

            if (error === null) {
              await setFailedBook(false);
              await setFailedTextBook('');
              await setSuccessBook(true);
              await setSuccessTextBook('Success Book');
              await settingButtonLoading(false);

              if (!buttonIsLoading) {
                setTimeout(() => {
                  navigation.dispatch(
                    CommonActions.navigate({
                      name: 'AppointmentDetail',
                      params: {id: appointmentId},
                    }),
                  );
                }, 3000);
              }
            } else {
              await setFailedBook(true);
              await setFailedTextBook(error);
              await setSuccessBook(false);
              await setSuccessTextBook('');
              await settingButtonLoading(false);
            }
          })
          .catch(async error => {
            console.log('Error Catch Promise: ', error);
            await settingButtonLoading(false);
            await setFailedBook(true);
            await setFailedTextBook('Failed to book');
          });
      }
    } catch (error) {
      console.log('Error: ', error);
      await settingButtonLoading(false);
      await setFailedBook(true);
      await setFailedTextBook('Failed to book');
    }
  };

  if (isLoading && calendar === null) {
    return <LoaderWithContainer />;
  }

  return (
    <Container style={{backgroundColor: 'white'}}>
      {failedBook ? (
        <View
          style={{
            backgroundColor: 'red',
            width: '100%',
            position: 'absolute',
            zIndex: 99,
            justifyContent: 'center',
            alignItems: 'center',
            top: 80,
          }}>
          <ErrorToaster text={failedTextBook} />
        </View>
      ) : null}
      {successBook ? (
        <View
          style={{
            backgroundColor: 'red',
            width: '100%',
            position: 'absolute',
            zIndex: 99,
            justifyContent: 'center',
            alignItems: 'center',
            top: 80,
          }}>
          <SuccessToaster text={successTextBook} />
        </View>
      ) : null}
      <Header
        iosBarStyle="dark-content"
        androidStatusBarColor="white"
        translucent={false}
        style={{
          backgroundColor: 'white',
          elevation: 0,
          shadowOpacity: 0,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
        }}>
        <Left style={{flex: 0.1}}>
          <Button
            onPress={() => popStacking()}
            style={{
              elevation: 0,
              backgroundColor: 'transparent',
              alignSelf: 'center',
              paddingTop: 0,
              paddingBottom: 0,
              height: 35,
              width: 35,
              justifyContent: 'center',
            }}>
            <Icon
              type="Feather"
              name="x"
              style={{
                marginLeft: 0,
                marginRight: 0,
                fontSize: 24,
                color: black,
              }}
            />
          </Button>
        </Left>
        <Body
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: regular,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            Appointment
          </Text>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.4),
              color: superGrey,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {activity ? activity.toUpperCase() : ''}
          </Text>
        </Body>
        <Right style={{flex: 0.1}} />
      </Header>
      <Card
        style={{
          marginTop: 0,
          marginLeft: 0,
          marginRight: 0,
          marginBottom: 0,
          elevation: 0,
          shadowOpacity: 0,
          borderWidth: 0,
          borderColor: white,
        }}>
        <CardItem>
          <Body
            style={{
              paddingTop: 10,
              paddingBottom: 10,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: black,
                fontWeight: '500',
              }}>
              {moment(dateSelected).format('MMMM YYYY')}
            </Text>
          </Body>
        </CardItem>
      </Card>
      {/* <Text>{JSON.stringify(calendar)}</Text> */}
      {/* <Text>{JSON.stringify(indexes)}</Text> */}
      <Tab.Navigator
        initialRouteName={
          calendar[indexes]?.date
            ? moment(calendar[indexes]?.date).format('ddd, DD MMM YYYY')
            : moment().format('ddd, DD MMM YYYY')
        }
        swipeEnabled={false}
        lazy
        tabBarOptions={{scrollEnabled: true}}
        tabBar={tabProps => (
          <TabBar
            {...tabProps}
            indexes={indexes}
            tabRef={tabRef}
            setIndex={i => {
              setFailedBook(false);
              setFailedTextBook('');
              setSuccessBook(false);
              setSuccessTextBook('');
              settingButtonLoading(false);
              settingSelectedTimeSlot(null); // to reset selected time for each tab changes
              updatingSelectedIndex(i);
            }}
          />
        )}>
        {calendar.map((data, i) => {
          return (
            <Tab.Screen
              listeners={() => {
                // hidden features of RN V5
              }}
              key={String(i) + 'TabScreen'}
              name={
                data?.date
                  ? moment(data?.date).format('ddd, DD MMM YYYY')
                  : moment().format('ddd, DD MMM YYYY')
              }
              options={{
                tabBarLabel: data?.date
                  ? moment(data?.date).format('ddd, DD MMM YYYY')
                  : moment().format('ddd, DD MMM YYYY'),
              }}
              children={screenProps => {
                const {route: screenRoute} = screenProps;
                const {params: screenParams} = screenRoute;

                if (screenParams === undefined) {
                  if (
                    moment(calendar[indexes]?.date).format(
                      'ddd, DD MMM YYYY',
                    ) === screenRoute.name
                  ) {
                    return (
                      <TimeSlot
                        {...props}
                        {...screenProps}
                        indexes={indexes}
                        calendar={calendar}
                        appointmentId={appointmentId}
                        name={moment(calendar[indexes]?.date).format(
                          'ddd, DD MMM YYYY',
                        )}
                        weddingDate={weddingDate}
                        settingSelectedTimeSlot={async time => {
                          await setFailedBook(false);
                          await setFailedTextBook('');
                          await setSuccessBook(false);
                          await setSuccessTextBook('');
                          await settingButtonLoading(false);
                          await settingSelectedTimeSlot(time);
                        }}
                      />
                    );
                  } else {
                    return null;
                  }
                } else {
                  const {index: screenIndex} = screenParams;
                  if (screenIndex === indexes) {
                    return (
                      <TimeSlot
                        {...props}
                        {...screenProps}
                        indexes={indexes}
                        calendar={calendar}
                        appointmentId={appointmentId}
                        name={moment(calendar[indexes]?.date).format(
                          'ddd, DD MMM YYYY',
                        )}
                        weddingDate={weddingDate}
                        settingSelectedTimeSlot={async time => {
                          await setFailedBook(false);
                          await setFailedTextBook('');
                          await setSuccessBook(false);
                          await setSuccessTextBook('');
                          await settingButtonLoading(false);
                          await settingSelectedTimeSlot(time);
                        }}
                      />
                    );
                  } else {
                    return null;
                  }
                }
              }}
            />
          );
        })}
      </Tab.Navigator>
      <Buttons
        disable={buttonIsLoading ? true : false}
        isLoading={buttonIsLoading}
        text={'BOOK APPOINTMENT'}
        onPress={() => bookAppointmentFunc()}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(DailyCalendar);

export default props => <Wrapper {...props} />;
