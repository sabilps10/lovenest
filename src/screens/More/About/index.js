import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Linking,
  Dimensions,
  Platform,
  Animated,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Icon,
} from 'native-base';
import Map from '../../../components/Map';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import ButtonBack from '../../../components/Button/buttonBack';
import {commonImage} from '../../../utils/Themes/Images';
import ButtonMap from '../../../components/Button/buttonMap';
import {socialMediaIcon} from '../../../utils/Themes/Images';
import ButtonSocialMedia from '../../../components/Button/buttonSocialMedia';
import Geocoding from '../../../utils/Geocoding';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {hasNotch} from 'react-native-device-info';

const {newContainerColor, black, transparent, veryLightPinkTwo} = Colors;
const {medium, book} = FontType;
const {regular} = FontSize;
const {lnTextBlack} = commonImage;
const {facebook, youtube, globe} = socialMediaIcon;

const {width, height} = Dimensions.get('window');

class AboutUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: `City Gate, 371 Beach Road, ${'\n'}${'#02-16 Singapore 199597'}`,
      email: 'contact@lovenest.com.sg',
      phone: '+65 6455 2291',
      facebook:
        'https://www.facebook.com/pg/Love-Nest-2361019000885809/about/?ref=page_internal',
      youtube: 'https://www.youtube.com/channel/UCNZxsMtnpjgNR5OVKKVDEFg',
      website: 'https://www.lovenest.com.sg',
      coordinate: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
    };
  }

  popStacking = async () => {
    const {navigation} = this.props;
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: true,
    });
    navigation.pop();
  };

  navigationOptions = props => {
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'About Us',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: RFPercentage(1.9),
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={this.popStacking} />;
      },
    });
  };

  handleOpenURL = url => {
    if (url !== '') {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log(`Don't know how to open URI: ${url}`);
        }
      });
    }
  };

  getLatLngLocationFromAddress = async address => {
    try {
      const getLocationDetail = await Geocoding.detailLocation(address);
      console.log('getLocationDetail: ', getLocationDetail);

      if (getLocationDetail) {
        const ASPECT_RATIO = width / height;

        const {lat} = getLocationDetail.location;
        const {lng} = getLocationDetail.location;
        const latDelta =
          getLocationDetail.viewport.northeast.lat -
          getLocationDetail.viewport.southwest.lat;
        const lngDelta = latDelta * ASPECT_RATIO;

        const coordinate = {
          latitude: lat,
          longitude: lng,
          latitudeDelta: latDelta,
          longitudeDelta: lngDelta,
        };

        console.log('coordinate: ', coordinate);

        if (coordinate) {
          this.setState({
            coordinate,
          });
        }
      }
    } catch (error) {
      console.log('error: ', error);
    }
  };

  openMap = () => {
    const {latitude} = this.state.coordinate;
    const {longitude} = this.state.coordinate;
    const label = `${this.state.address}`;

    const url = Platform.select({
      ios: `https://www.google.com/maps/search/?api=1&query=${label}&center=${latitude},${longitude}`,
      android: `geo:${latitude},${longitude}?q=${label}`,
    });
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          const browser_url = `https://www.google.de/maps/@${latitude},${longitude}?q=${label}`;
          return Linking.openURL(browser_url);
        }
        return Linking.openURL(url);
      })
      .catch(err => console.log('error', err));
  };

  openCall = () => {
    try {
      Linking.openURL(`tel:${this.state.phone}`);
    } catch (error) {
      console.log('Error on Call: ', error);
    }
  };

  sendEmail = () => {
    try {
      Linking.openURL(`mailto:${this.state.email}`);
    } catch (error) {
      console.log('Send Email Error: ', error);
    }
  };

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentDidMount() {
    const {props} = this;
    const {navigation} = props;
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(false);
    }
    this.getLatLngLocationFromAddress(this.state.address);
    this._unsubscribe = navigation.addListener('focus', () => {
      this.navigationOptions(props);
      this.getLatLngLocationFromAddress(this.state.address);
      console.log('More ADD LISTENER DID MOUNT');
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(false);
      }
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content contentContainerStyle={styles.content}>
          <Card transparent style={styles.card}>
            <CardItem style={styles.cardItemLogo}>
              <Image source={lnTextBlack} style={styles.lnLogo} />
            </CardItem>
            <CardItem style={styles.cardItemMap}>
              <Map forMapAddress={this.state.address} />
              <ButtonMap onPress={() => this.openMap()} />
            </CardItem>
            <CardItem
              button
              onPress={() => this.openMap()}
              style={{
                ...styles.commonCardItem,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    // borderWidth: 1,
                    width: '100%',
                    flexDirection: 'row',
                    // flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    alignItems: 'flex-start',
                  }}>
                  <View
                    style={{
                      flex: 0.1,
                      // borderWidth: 1,
                      height: '100%',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignItems: 'flex-end',
                    }}>
                    <Icon
                      type="Feather"
                      name="map-pin"
                      style={
                        Platform.OS === 'ios'
                          ? hasNotch()
                            ? {
                                ...styles.icon,
                                marginLeft: 0,
                                marginRight: 0,
                                top: 5,
                                left: '160%',
                              }
                            : {
                                ...styles.icon,
                                marginLeft: 0,
                                marginRight: 0,
                                top: 5,
                                left: '160%',
                              }
                          : {
                              ...styles.icon,
                              marginLeft: 0,
                              marginRight: 0,
                              top: 3,
                              left: '160%',
                            }
                      }
                    />
                  </View>
                  <View style={{flex: 1, paddingLeft: 5}}>
                    <Text
                      style={{
                        ...styles.text,
                        textAlign: 'center',
                        lineHeight: 21,
                        right:
                          Platform.OS === 'ios' ? (hasNotch() ? 15 : 15) : 15,
                        // left: '50%',
                      }}>
                      {this.state.address}
                    </Text>
                  </View>
                </View>
              </View>
            </CardItem>
            <CardItem
              button
              onPress={() => this.sendEmail()}
              style={{
                ...styles.commonCardItem,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    // borderWidth: 1,
                    width: '85%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Icon
                    type="Feather"
                    name="mail"
                    style={{...styles.icon, marginTop: 3}}
                  />
                  <View>
                    <Text style={{...styles.text, right: 10}}>
                      {this.state.email}
                    </Text>
                  </View>
                </View>
              </View>
            </CardItem>
            <CardItem
              button
              onPress={() => this.openCall()}
              style={{
                ...styles.commonCardItem,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    // borderWidth: 1,
                    width: '85%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Icon
                    type="Feather"
                    name="phone"
                    style={{...styles.icon, marginTop: 3}}
                  />
                  <Text style={{...styles.text, right: 10}}>
                    {this.state.phone}
                  </Text>
                </View>
              </View>
            </CardItem>
            <CardItem
              style={{
                ...styles.commonCardItem,
                ...styles.separatorCardItem,
              }}>
              <View style={styles.lineSeparator} />
            </CardItem>
            <CardItem
              style={{
                ...styles.commonCardItem,
                ...styles.socialMediaCardItem,
              }}>
              <ButtonSocialMedia
                iconType={facebook}
                onPress={() => this.handleOpenURL(this.state.facebook)}
              />
              <ButtonSocialMedia
                iconType={youtube}
                onPress={() => this.handleOpenURL(this.state.youtube)}
              />
              <ButtonSocialMedia
                iconType={globe}
                onPress={() => this.handleOpenURL(this.state.website)}
              />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: 'white'},
  content: {flex: 1, paddingTop: 16},
  card: {
    elevation: 0,
    shadowOpacity: 0,
    width: '100%',
    marginLeft: 0,
    marginRight: 0,
    paddingTop: 24,
  },
  cardItemLogo: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 24,
  },
  lnLogo: {width: 163.64, height: 38},
  cardItemMap: {
    marginBottom: 25,
    paddingLeft: 0,
    paddingRight: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commonCardItem: {backgroundColor: transparent},
  separatorCardItem: {
    paddingTop: 25,
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialMediaCardItem: {
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lineSeparator: {
    height: 1,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: veryLightPinkTwo,
  },
  body: {
    flexBasis: '90%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  left: {
    flexBasis: '10%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  icon: {fontSize: 15, color: black},
  text: {
    textAlign: 'center',
    fontFamily: book,
    color: black,
    // fontSize: regular,
    fontSize: RFPercentage(1.8),
    lineHeight: 20,
    letterSpacing: 0.3,
  },
});

export default AboutUs;
