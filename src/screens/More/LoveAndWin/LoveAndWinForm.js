import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  View,
  Animated,
  SafeAreaView,
  TextInput,
  Platform,
  ScrollView,
  Keyboard,
  Modal,
  FlatList,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import DatePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import DocumentPicker from 'react-native-document-picker';
import {ReactNativeFile} from 'apollo-upload-client';
import {CameraKitCameraScreen} from 'react-native-camera-kit';

// Map
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geocoding from '../../../utils/Geocoding';

//  Query
import GET_SERVICET_YPES from '../../../graphql/queries/reviewGetServiceType';
import MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';
import GET_MERCHANT_BY_SERVICE_TYPE from '../../../graphql/queries/getMerchantByServiceType';
import GET_STAFF from '../../../graphql/queries/getAllStaffMobile';

// Mutation
import UPLOAD_FILE from '../../../graphql/mutations/uploadMobileAttatchment';
import CREATE_SUBMISSION from '../../../graphql/mutations/customerSubmission';

const {width} = Dimensions?.get('screen');
const {
  white,
  black,
  mainGreen,
  overlayDim,
  mainRed,
  headerBorderBottom,
  greyLine,
} = Colors;
const {book, medium} = FontType;

const LoveAndWinForm = props => {
  console.log('LoveAndWinForm Props: ', props);
  const {positionYBottomNav} = props;

  const [service, setService] = useState(null);
  const [merchant, setMerchant] = useState(null);

  const [location, setLocation] = useState(null);
  const [dateOfPurchase, setDateOfPurchase] = useState(null);

  const [totalAmount, setTotalAmount] = useState('');
  const [depositAmount, setDepositAmount] = useState('');
  const [staff, setStaff] = useState(null);

  const [showModalService, setShowModalService] = useState(false);
  const [showModalMerchant, setShowModalMerchant] = useState(false);
  const [showModalStaff, setShowModalStaff] = useState(false);
  const [showModalDatePicker, setShowModalDatePicker] = useState(false);
  const [showModalUpload, setShowModalUpload] = useState(false);

  const [listServiceType, setListServiceType] = useState([]);
  const [listMerchant, setListMerchant] = useState([]);
  const [listStaff, setListStaff] = useState([]);

  // For File PDF
  const [listFile, setListFile] = useState([]);

  // For Image File
  const [listImages, setListImages] = useState([]);

  const [loaderSubmit, setLoaderSubmit] = useState(false);
  const [message, setMessage] = useState('');

  useEffect(() => {
    onChangeOpacity(false);

    if (showModalService) {
      fetchServiceType();
    }

    if (showModalMerchant && service) {
      fetchMerchant();
    }

    if (merchant && !showModalMerchant) {
      fetchMerchantDetailAfterMerchantSelected();
    }

    if (showModalStaff && merchant) {
      fetchStaff();
    }

    if (loaderSubmit) {
      onSubmit();
    }
  }, [
    showModalService,
    showModalMerchant,
    service,
    merchant,
    showModalStaff,
    loaderSubmit,
  ]);

  const fetchStaff = useCallback(() => {
    try {
      props?.client
        ?.query({
          query: GET_STAFF,
          variables: {
            id: merchant?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('Res Staff: ', res);
          const {data, errors} = res;
          const {getAllStaffMobile} = data;
          const {data: result, error} = getAllStaffMobile;

          if (!errors || !error) {
            setListStaff([...result]);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {}
  }, [merchant, showModalStaff]);

  const fetchMerchantDetailAfterMerchantSelected = useCallback(() => {
    try {
      props?.client
        ?.query({
          query: MERCHANT_DETAIL,
          variables: {
            id: merchant?.id,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('fetch merchant detail: ', res);
          const {data, errors} = res;
          const {getMerchantVer2} = data;

          if (!errors) {
            if (getMerchantVer2) {
              if (getMerchantVer2[0]?.addresses?.length > 0) {
                setLocation(`${getMerchantVer2[0]?.addresses[0]?.address}`);
              } else {
                setLocation(`${getMerchantVer2[0]?.address}`);
              }
            }
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {}
  }, [merchant]);

  const fetchMerchant = useCallback(() => {
    try {
      props?.client
        ?.query({
          query: GET_MERCHANT_BY_SERVICE_TYPE,
          variables: {
            serviceType: service,
            itemDisplayed: 100,
            pageNumber: 1,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('Fetch merchant: ', res);
          const {data, errors} = res;
          const {getMerchantsByServiceType} = data;
          const {data: result, error} = getMerchantsByServiceType;

          if (!errors || !error) {
            setListMerchant([...result]);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error fetch merchant: ', error);
    }
  }, [service, showModalMerchant]);

  const fetchServiceType = useCallback(() => {
    try {
      props?.client
        ?.query({
          query: GET_SERVICET_YPES,
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(res => {
          console.log('Res fetch service type: ', res);
          const {data, errors} = res;
          const {serviceTypes} = data;
          const {data: result, error} = serviceTypes;

          if (!errors || !error) {
            setListServiceType([...result]);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error fetch service type: ', error);
    }
  }, [showModalService]);

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      props?.navigation?.goBack(null);
    } catch (error) {}
  };

  const openModalUpload = () => {
    try {
      setShowModalUpload(!showModalUpload);
    } catch (error) {
      console.log('Open File Error: ', error);
    }
  };

  const onSubmit = useCallback(async () => {
    try {
      console.log('ON SUBMIT: ', {
        loveWinId: props?.route?.params?.id,
        service,
        merchant,
        dateOfPurchase,
        totalAmount,
        depositAmount,
        staff,
        listFile,
        listImages,
      });

      if (
        !service ||
        !merchant ||
        !dateOfPurchase ||
        totalAmount === '' ||
        depositAmount === '' ||
        !staff ||
        (listImages?.length === 0 && listFile?.length === 0)
      ) {
        // form not filled
        await setMessage(String('Please fill all form'));
        await setTimeout(() => {
          setMessage(String(''));
          setLoaderSubmit(false);
        }, 1500);
      } else {
        if (!props?.route?.params?.id) {
          await setMessage(String('Failed to Submit'));
          await setTimeout(() => {
            setMessage(String(''));
            setLoaderSubmit(false);
          }, 1500);
        }
        // form completely filled
        const attachmentIds = await mergingAttachment(listFile, listImages);
        const variables = {
          loveWinId: props?.route?.params?.id,
          serviceType: service,
          merchantId: merchant?.id,
          datePurchase: moment(dateOfPurchase).utc().format(),
          totalAmount: parseFloat(totalAmount),
          depositAmount: parseFloat(depositAmount),
          staff: {
            ...staff,
            id: parseInt(staff?.id, 10),
          },
          attachmentIds,
        };
        console.log('variables: ', variables);

        props?.client
          ?.mutate({
            mutation: CREATE_SUBMISSION,
            variables,
          })
          .then(async res => {
            console.log('Res Submit Submission: ', res);
            const {data, errors} = res;
            const {customerSubmission} = data;
            const {error} = customerSubmission;

            if (errors) {
              await setMessage(
                String(errors?.message ? errors?.message : 'Failed to Submit'),
              );
              await setTimeout(() => {
                setMessage(String(''));
                setLoaderSubmit(false);
              }, 1500);
            } else {
              if (error) {
                await setMessage(
                  String(error?.message ? error?.message : 'Failed to Submit'),
                );
                await setTimeout(() => {
                  setMessage(String(''));
                  setLoaderSubmit(false);
                }, 1500);
              } else {
                await setMessage(String('Submit Successfuly'));
                await setTimeout(() => {
                  setMessage(String(''));
                  setLoaderSubmit(false);
                  props?.navigation?.goBack(null);
                }, 1500);
              }
            }
          })
          .catch(error => {
            console.log('Error: ', error);
            throw error;
          });
      }
    } catch (error) {
      await setMessage(String('Failed to Submit'));
      await setTimeout(() => {
        setMessage(String(''));
        setLoaderSubmit(false);
      }, 1500);
    }
  }, [loaderSubmit]);

  const mergingAttachment = (files, images) => {
    return new Promise(async (resolve, reject) => {
      try {
        const merge = files.concat(images);
        const getCleanIdOnly = await Promise.all(
          merge?.map((d, i) => {
            return parseInt(d?.id, 10);
          }),
        );

        if (merge?.length === getCleanIdOnly?.length) {
          resolve(getCleanIdOnly);
        } else {
          reject('Failed to get attachment');
        }
      } catch (error) {
        reject('Failed to get attachment');
      }
    });
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: white}}>
      <ModalLoaderSubmit visible={loaderSubmit} message={message} />
      <ModalService
        listServiceType={listServiceType}
        visible={showModalService}
        onClose={() => setShowModalService(false)}
        onSelectServiceType={serviceTypeSelected => {
          setMerchant(null);
          setService(serviceTypeSelected);
          setShowModalService(false);
        }}
      />
      <ModalMerchant
        listMerchant={listMerchant}
        visible={showModalMerchant}
        onClose={() => setShowModalMerchant(false)}
        onSelectMerchant={selectedMerchant => {
          setMerchant(selectedMerchant);
          setShowModalMerchant(false);
        }}
      />
      <ModalStaff
        listStaff={listStaff}
        visible={showModalStaff}
        onClose={() => setShowModalStaff(false)}
        onSelectStaff={selectedStaff => {
          console.log('Kontol Staff: ', selectedStaff);
          setStaff(selectedStaff);
          setShowModalStaff(false);
        }}
      />
      {showModalDatePicker ? (
        <ModalDatePicker
          visible={showModalDatePicker}
          onClose={() => setShowModalDatePicker(false)}
          value={
            dateOfPurchase ? new Date(moment(dateOfPurchase).valueOf()) : null
          }
          onChange={e => {
            try {
              if (Platform?.OS === 'ios') {
                console.log('Date Selected IOS: ', e);
                setDateOfPurchase(new Date(moment(e).valueOf()));
                setShowModalDatePicker(false);
              } else {
                console.log('Date Selected Android: ', e);
                const updatedDate = new Date(moment(e).valueOf());
                setShowModalDatePicker(false);
                setDateOfPurchase(updatedDate);
              }
            } catch (error) {
              console.log('error modal date picker: ', error);
            }
          }}
        />
      ) : null}
      <ModalUpload
        client={props?.client}
        visible={showModalUpload}
        onClose={() => setShowModalUpload(!showModalUpload)}
        onChange={result => {
          console.log('Result File Uploaded: ', result);
          if (result) {
            if (result?.mimetype === 'application/pdf') {
              setListFile([...listFile, {...result}]);
              setShowModalUpload(false);
            } else {
              setListImages([...listImages, {...result}]);
              setShowModalUpload(false);
            }
          } else {
            //  Do nothing
          }
        }}
      />
      <Headers title="Love And Win Cash" goBack={goBack} />
      <KeyboardAvoidingView
        keyboardVerticalOffset={20}
        behavior={Platform?.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <ScrollView
          keyboardDismissMode={'on-drag'}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{padding: 15}}>
          {/* Service */}
          <FormButton
            title={'Service'}
            value={service}
            placeholder={'Select service'}
            onOpen={() => setShowModalService(true)}
          />
          {/* Merchant */}
          <FormButton
            title={'Merchant'}
            value={merchant}
            placeholder={'Select merchant'}
            onOpen={() => {
              if (service) {
                setShowModalMerchant(true);
              }
            }}
          />
          {/* Map when merchant selected */}
          {merchant ? (
            <View style={{paddingTop: 10, paddingBottom: 10}}>
              <GoogleMap
                address={location}
                widthMap={width * 0.92}
                heightMap={width * 0.5}
              />
            </View>
          ) : null}
          {/* Location */}
          {merchant ? (
            <Form
              required={true}
              numberOfLines={1}
              title={'Location'}
              value={location}
              placeholder={'merchant location'}
              onChange={e => {
                console.log('Location: ', e);
              }}
            />
          ) : null}
          {/* Staff */}
          {merchant ? (
            <FormButton
              title={'Staff'}
              value={staff}
              placeholder={'Select staff'}
              onOpen={() => setShowModalStaff(true)}
            />
          ) : null}
          {/* Total Amount */}
          <Form
            required={true}
            numberOfLines={1}
            title={'Total Amount'}
            value={totalAmount}
            placeholder={'input your total amount'}
            onChange={e => {
              console.log('Total Amount: ', e);
              setTotalAmount(e);
            }}
          />
          {/* Deposit Amount */}
          <Form
            required={true}
            numberOfLines={1}
            title={'Deposit Amount'}
            value={depositAmount}
            placeholder={'input your deposit amount'}
            onChange={e => {
              console.log('Deposit Amount: ', e);
              setDepositAmount(e);
            }}
          />
          {/* Date Of Purchase */}
          <FormDate
            value={
              dateOfPurchase
                ? moment(dateOfPurchase).format('DD MMMM YYYY')
                : null
            }
            title={'Date of Purchase'}
            placeholder={'DD MMMM YYYY'}
            onOpen={() => setShowModalDatePicker(true)}
          />
          <View style={{width: '100%', padding: 8}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
              }}>
              <Text style={{color: mainRed}}>*</Text>Attatchment(s)
            </Text>
            <TouchableOpacity
              onPress={() => openModalUpload()}
              style={{
                right: 4,
                marginTop: 10,
                flexDirection: 'row',
                padding: 5,
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Icon
                type={'Feather'}
                name={'plus-square'}
                style={{fontSize: RFPercentage(2.2), color: mainRed}}
              />
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: mainRed,
                  letterSpacing: 0.3,
                }}>
                Upload File
              </Text>
            </TouchableOpacity>
            {/* PDF FILE LIST */}
            {listFile?.length === 0 ? null : (
              <FlatList
                contentContainerStyle={{paddingTop: 25, paddingBottom: 15}}
                ListHeaderComponent={() => {
                  if (listFile?.length === 0) {
                    return null;
                  } else {
                    return (
                      <View
                        style={{
                          alignSelf: 'flex-start',
                          borderWidth: 1,
                          borderColor: mainGreen,
                          borderRadius: 15,
                          padding: 6,
                          paddingLeft: 25,
                          paddingRight: 25,
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.6),
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          PDF FILE
                        </Text>
                      </View>
                    );
                  }
                }}
                data={listFile}
                extraData={listFile}
                keyExtractor={(item, index) => `${item?.id}`}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        marginVertical: 10,
                        borderRadius: 5,
                        flexDirection: 'row',
                        padding: 10,
                        borderWidth: 1,
                        borderColor: mainGreen,
                        backgroundColor: mainGreen,
                      }}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          paddingRight: 5,
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.6),
                            color: white,
                            letterSpacing: 0.3,
                          }}>
                          {item?.filename}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 0.1,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            try {
                              let oldList = listFile;
                              oldList?.splice(index, 1);
                              setListFile([...listFile]);
                            } catch (error) {
                              console.log('Error remove file array: ', error);
                            }
                          }}
                          style={{
                            padding: 6,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Icon
                            type="Feather"
                            name="x"
                            style={{fontSize: RFPercentage(2), color: white}}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  );
                }}
              />
            )}
            {/* List Images */}
            {listImages?.length === 0 ? null : (
              <FlatList
                contentContainerStyle={{paddingTop: 25, paddingBottom: 15}}
                ListHeaderComponent={() => {
                  if (listImages?.length === 0) {
                    return null;
                  } else {
                    return (
                      <View
                        style={{
                          marginBottom: 8,
                          alignSelf: 'flex-start',
                          borderWidth: 1,
                          borderColor: mainGreen,
                          borderRadius: 15,
                          padding: 6,
                          paddingLeft: 25,
                          paddingRight: 25,
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.6),
                            color: black,
                            letterSpacing: 0.3,
                          }}>
                          PHOTO
                        </Text>
                      </View>
                    );
                  }
                }}
                data={listImages}
                extraData={listImages}
                keyExtractor={(item, index) => `${item?.id}`}
                numColumns={3}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        right: 7,
                        flex: 1 / 3,
                        height: width * 0.45,
                        padding: 5,
                        borderWidth: 0,
                      }}>
                      <View
                        style={{flex: 1, backgroundColor: headerBorderBottom}}>
                        <TouchableOpacity
                          onPress={() => {
                            try {
                              let oldList = listImages;
                              oldList?.splice(index, 1);
                              setListImages([...listFile]);
                            } catch (error) {
                              console.log('Error remove file array: ', error);
                            }
                          }}
                          style={{
                            borderRadius: 50,
                            padding: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: white,
                            position: 'absolute',
                            zIndex: 99,
                            top: 0,
                            right: 0,
                          }}>
                          <Icon
                            type="Feather"
                            name="x"
                            style={{fontSize: RFPercentage(1.8), color: black}}
                          />
                        </TouchableOpacity>
                        <Image
                          source={{uri: item?.url}}
                          style={{
                            zIndex: 1,
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                          }}
                        />
                      </View>
                    </View>
                  );
                }}
              />
            )}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <SubmitButton
        onPress={() => {
          setLoaderSubmit(true);
        }}
      />
    </SafeAreaView>
  );
};

export const ModalLoaderSubmit = props => {
  const {visible, message} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          backgroundColor: overlayDim,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 15,
          paddingLeft: 35,
          paddingRight: 35,
        }}>
        <View
          style={{
            backgroundColor: white,
            borderRadius: 5,
            padding: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {message === '' ? (
            <ActivityIndicator size="large" color={mainGreen} />
          ) : message === 'Submit Successfuly' ? (
            <Icon
              type="Feather"
              name="check"
              style={{
                fontSize: RFPercentage(2.5),
                color: mainGreen,
                marginBottom: 10,
              }}
            />
          ) : (
            <Icon
              type="Feather"
              name="x"
              style={{
                fontSize: RFPercentage(2.5),
                color: mainRed,
                marginBottom: 10,
              }}
            />
          )}
          {message === '' ? (
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Submitting...
            </Text>
          ) : (
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {message}
            </Text>
          )}
        </View>
      </View>
    </Modal>
  );
};

export const SubmitButton = props => {
  const {onPress} = props;

  return (
    <View style={{position: 'relative', bottom: 0, left: 0, right: 0}}>
      <TouchableOpacity
        onPress={() => onPress()}
        style={{
          backgroundColor: mainGreen,
          width: '100%',
          padding: 10,
          paddingTop: 20,
          paddingBottom: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: white,
            letterSpacing: 0.3,
          }}>
          SUBMIT
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export const ModalUpload = props => {
  const {visible, onClose, onChange, client} = props;

  const [openCamera, setOpenCamera] = useState(false);
  const [imagePreview, setImagePreview] = useState(null);

  const [fileUpload, setFileUpload] = useState(null);
  const [fileName, setFileName] = useState('');
  const [isLoadingUploadGetUrl, setIsLoadingUploadGetUrl] = useState(false);

  useEffect(() => {
    if (isLoadingUploadGetUrl) {
      executeUpload();
    }
  }, [isLoadingUploadGetUrl]);

  const executeUpload = useCallback(() => {
    try {
      if (fileUpload) {
        client
          ?.mutate({
            mutation: UPLOAD_FILE,
            variables: {
              file: fileUpload,
              type: 'LoveWin',
            },
          })
          .then(res => {
            console.log('Res Execute Upload: ', res);
            const {data, errors} = res;
            const {uploadMobileAttachment} = data;
            const {data: result, error} = uploadMobileAttachment;

            if (errors || error) {
              setIsLoadingUploadGetUrl(false);
            } else {
              onChange(result);
              setFileName('');
              setFileUpload(null);
              setImagePreview(null);
              setIsLoadingUploadGetUrl(false);
            }
          })
          .catch(error => {
            throw error;
          });
      } else {
        setIsLoadingUploadGetUrl(false);
      }
    } catch (error) {
      console?.log('Error Execute upload: ', error);
      setIsLoadingUploadGetUrl(false);
    }
  });

  const openFile = async () => {
    try {
      const res = await DocumentPicker.pickSingle({
        type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
      });
      console.log('res: ', res);
      if (res) {
        const file = new ReactNativeFile({
          uri: res?.uri,
          name: res?.name,
          type: res?.type,
        });

        if (file) {
          setFileUpload(file);
          setImagePreview(null);
          setFileName(res?.name);
        }
      }
    } catch (error) {
      console.log('Error openFile: ', error);
      if (DocumentPicker.isCancel(error)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        // throw error
      }
    }
  };

  return (
    <Modal visible={visible} transparent animationType="fade">
      <SafeAreaView
        style={{
          flex: 1,
        }}>
        {openCamera ? (
          <CameraKitCameraScreen
            cameraOptions={{
              flashMode: 'auto', // on/off/auto(default)
              focusMode: 'on', // off/on(default)
              zoomMode: 'on', // off/on(default)
              //   ratioOverlay: '1:1', // optional, ratio overlay on the camera and crop the image seamlessly
              //   ratioOverlayColor: '#00000077', // optional
            }}
            actions={{leftButtonText: 'Cancel'}}
            hideControls={false}
            onBottomButtonPressed={event => {
              console.log('EVENT CAMERA: ', event);
              if (event?.type === 'capture') {
                const file = new ReactNativeFile({
                  uri:
                    Platform?.OS === 'ios'
                      ? event?.image?.uri
                      : `file://${event?.image?.uri}`,
                  name: event?.image?.name,
                  type: 'image/*',
                });
                console.log('file: ', file);

                if (file) {
                  setImagePreview(
                    event?.image?.uri
                      ? Platform?.OS === 'ios'
                        ? event?.image?.uri
                        : `file://${event?.image?.uri}`
                      : null,
                  );
                  setFileUpload(file);
                  setFileName(event?.image?.name ? event?.image?.name : '');
                  setOpenCamera(!openCamera);
                }
              } else if (event?.type === 'left') {
                setOpenCamera(!openCamera);
              }
            }}
            captureButtonImage={require('../../../static/images/cameraButton.png')}
            saveToCameraRoll={false}
          />
        ) : (
          <View
            style={{
              flex: 1,
              backgroundColor: overlayDim,
              padding: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flex: 1,
                width: '100%',
                backgroundColor: white,
                borderRadius: 5,
                borderWidth: 0,
              }}>
              {/* Header */}
              <HeaderUpload onClose={() => onClose()} title={'Upload'} />
              <ScrollView
                keyboardDismissMode="interactive"
                keyboardShouldPersistTaps="handled">
                <View style={{flex: 1, padding: 15}}>
                  {/* Button File for PDF */}
                  <ButtonFile
                    isLoadingUpload={isLoadingUploadGetUrl}
                    onPress={() => {
                      openFile();
                    }}
                  />
                  <Text
                    style={{
                      marginVertical: 15,
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    OR
                  </Text>
                  {imagePreview ? (
                    <TouchableOpacity
                      onPress={() => setOpenCamera(true)}
                      style={{
                        borderRadius: 5,
                        marginBottom: 10,
                        padding: 10,
                        backgroundColor: headerBorderBottom,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: width * 0.3,
                        height: width * 0.3,
                      }}>
                      <Image
                        source={{uri: imagePreview}}
                        style={{
                          position: 'absolute',
                          top: 0,
                          right: 0,
                          bottom: 0,
                          left: 0,
                        }}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => setOpenCamera(true)}
                      style={{
                        borderRadius: 5,
                        marginBottom: 10,
                        padding: 10,
                        backgroundColor: headerBorderBottom,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: width * 0.3,
                        height: width * 0.3,
                      }}>
                      <Icon
                        type="Feather"
                        name="camera"
                        style={{fontSize: RFPercentage(3), color: greyLine}}
                      />
                      <Text
                        style={{
                          color: greyLine,
                          fontFamily: book,
                          fontSize: RFPercentage(1.6),
                          letterSpacing: 0.3,
                          marginTop: 10,
                        }}>
                        Take a photo
                      </Text>
                    </TouchableOpacity>
                  )}
                  {/* File name form */}
                  <Form
                    required={true}
                    numberOfLines={1}
                    title={'File Name'}
                    value={fileName}
                    placeholder={'input your file name'}
                    onChange={e => {
                      console.log('Onchange file name: ', e);
                      setFileName(e);
                    }}
                  />
                </View>
              </ScrollView>
              <Bottom
                isLoadingUpload={isLoadingUploadGetUrl}
                onClose={() => onClose()}
                onUpload={() => setIsLoadingUploadGetUrl(true)}
              />
            </View>
          </View>
        )}
      </SafeAreaView>
    </Modal>
  );
};

export const Bottom = props => {
  const {isLoadingUpload, onClose, onUpload} = props;

  return (
    <View
      style={{
        paddingBottom: 15,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <TouchableOpacity
        onPress={() => onClose()}
        style={{
          flex: 1,
          marginHorizontal: 10,
          borderWidth: 1,
          borderColor: mainGreen,
          backgroundColor: white,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: mainGreen,
            letterSpacing: 0.3,
          }}>
          Cancel
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onUpload()}
        style={{
          flex: 1,
          marginHorizontal: 10,
          borderWidth: 1,
          borderColor: mainGreen,
          backgroundColor: mainGreen,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: white,
            letterSpacing: 0.3,
          }}>
          {isLoadingUpload ? 'UPLOADING...' : 'UPLOAD'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export const ButtonFile = props => {
  const {onPress, isLoadingUpload} = props;
  return (
    <View>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: black,
          letterSpacing: 0.3,
        }}>
        Select File
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => onPress()}
          style={{
            marginTop: 10,
            flexDirection: 'row',
            backgroundColor: mainGreen,
            padding: 7,
            paddingLeft: 10,
            paddingRight: 10,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'flex-start',
          }}>
          <Icon
            type="Feather"
            name="file"
            style={{
              fontSize: RFPercentage(2.2),
              color: white,
              marginRight: 10,
            }}
          />
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: white,
              letterSpacing: 0.3,
            }}>
            Choose file
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const HeaderUpload = props => {
  const {title, onClose} = props;

  return (
    <View
      style={{
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
      }}>
      <View style={{flex: 0.1}} />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View style={{flex: 0.1}}>
        <TouchableOpacity onPress={() => onClose()} style={{padding: 6}}>
          <Icon
            type="Feather"
            name="x"
            style={{fontSize: RFPercentage(2.2), color: black}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const ModalStaff = props => {
  const {visible, onClose, listStaff, onSelectStaff} = props;

  return (
    <Modal visible={visible} transparent animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <HeaderModal onClose={onClose} title={'Select Staff'} />
          <FlatList
            contentContainerStyle={{padding: 15}}
            legacyImplementation={true}
            initialNumToRender={5}
            disableVirtualization={true}
            data={listStaff}
            extraData={listStaff}
            keyExtractor={(item, index) => `${item?.id}`}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() => onSelectStaff(item)}
                  style={{
                    marginBottom: 15,
                    paddingTop: 10,
                    paddingBottom: 15,
                    borderBottomWidth: 1,
                    borderBottomColor: headerBorderBottom,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item?.name}
                  </Text>
                </TouchableOpacity>
              );
            }}
            ListEmptyComponent={() => {
              return (
                <View style={{justifyContent: 'center', paddingTop: 15}}>
                  <ActivityIndicator size="large" color={mainGreen} />
                </View>
              );
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export const ModalDatePicker = props => {
  console.log('ModalDatePicker Props: ', props);
  const {value, visible, onClose, onChange} = props;

  const [date, setDate] = useState(new Date());

  const [showAndroid, setShowAndroid] = useState(false);

  useEffect(() => {
    showingAndroid(visible);
    if (value) {
      setDate(value);
    }
  }, []);

  const showingAndroid = status => {
    setShowAndroid(status);
  };

  if (Platform?.OS === 'ios') {
    return (
      <Modal visible={visible} transparent animationType="fade">
        <SafeAreaView style={{flex: 1}}>
          <TouchableOpacity
            onPress={onClose}
            style={{flex: 1, backgroundColor: overlayDim}}
          />
          <View
            style={{
              flex: 0.5,
              backgroundColor: white,
            }}>
            <View
              style={{
                width: '100%',
                padding: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row',
                borderWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => {
                  onClose();
                }}
                style={{
                  padding: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: mainRed}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  onChange(date);
                }}
                style={{
                  padding: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: mainRed}}>Done</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: headerBorderBottom,
              }}>
              {visible ? (
                <DatePicker
                  style={{
                    width: '100%',
                    height: width * 0.5,
                  }}
                  value={date ? date : new Date()}
                  mode={'date'}
                  display={'spinner'}
                  onChange={(event, selectedDate) => {
                    try {
                      setDate(selectedDate);
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                />
              ) : null}
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    );
  } else {
    // Android
    return (
      <React.Fragment>
        {showAndroid ? (
          <DatePicker
            style={{
              zIndex: 999,
              display: visible ? 'flex' : 'none',
              width: '100%',
              height: width * 0.5,
            }}
            value={date ? date : new Date()}
            mode={'date'}
            display={'spinner'}
            onChange={(event, selectedDate) => {
              try {
                console.log('Android Date Picker: ', {event, selectedDate});
                if (event?.type === 'set') {
                  setShowAndroid(false);
                  setDate(selectedDate);
                  onChange(selectedDate);
                } else {
                  setShowAndroid(false);
                  onClose();
                }
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
          />
        ) : null}
      </React.Fragment>
    );
  }
};

export const FormDate = props => {
  const {value, title, placeholder, onOpen} = props;

  return (
    <View style={{width: '100%'}}>
      <View style={{width: '100%', paddingBottom: 10}}>
        <Text>
          <Text
            style={{
              fontFamily: medium,
              color: mainRed,
              fontSize: RFPercentage(1.8),
            }}>
            *
          </Text>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
        </Text>
        <TouchableOpacity
          onPress={() => onOpen()}
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 3,
            paddingRight: 3,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              fontFamily: book,
              color: value ? black : greyLine,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              marginRight: 10,
            }}>
            {value === '' || !value ? placeholder : value}
          </Text>
          <Icon
            type="Feather"
            name="chevron-down"
            style={{fontSize: RFPercentage(2.2), color: greyLine}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const GoogleMap = props => {
  console.log('Google Map Props: ', props);
  const {address, widthMap, heightMap} = props;

  const [location, setLocation] = useState(null);
  const [coordinate, setCoordinate] = useState({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  });

  useEffect(() => {
    if (address) {
      manipulateAddress();
    }
  }, [address]);

  const getCoordinateByAddress = detailLocation => {
    return new Promise(async (resolve, reject) => {
      try {
        let coordinates = null;
        const getDetail = await Geocoding?.detailLocation(detailLocation);
        console.log('getDetail: ', getDetail);
        if (getDetail) {
          const ASPECT_RATIO = widthMap / heightMap;
          console.log('ASPECT_RATIO: ', ASPECT_RATIO);

          const {lat} = getDetail.location;
          const {lng} = getDetail.location;
          const latDelta =
            getDetail.viewport.northeast.lat - getDetail.viewport.southwest.lat;
          const lngDelta = latDelta * ASPECT_RATIO;

          coordinates = {
            latitude: lat,
            longitude: lng,
            latitudeDelta: latDelta,
            longitudeDelta: lngDelta,
          };

          if (coordinates) {
            resolve(coordinates);
          }
        } else {
          resolve({
            latitude: 0,
            longitude: 0,
            latitudeDelta: 0,
            longitudeDelta: 0,
          });
        }
      } catch (error) {
        resolve({
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0,
          longitudeDelta: 0,
        });
      }
    });
  };

  const manipulateAddress = async () => {
    try {
      const detailCoordinate = await getCoordinateByAddress(address);
      console.log('COORDINATE: ', detailCoordinate);
      if (detailCoordinate) {
        await setCoordinate({...detailCoordinate});
        await setLocation(address);
      }
    } catch (error) {}
  };

  if (!location) {
    return (
      <View
        style={{
          width: '100%',
          height: width * 0.5,
          backgroundColor: headerBorderBottom,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 1,
        }}>
        <Text
          style={{
            fontSize: RFPercentage(1.8),
            fontFamily: medium,
            color: 'gray',
            letterSpacing: 0.3,
          }}>
          Map
        </Text>
        <Text
          style={{
            fontStyle: 'italic',
            marginTop: 15,
            fontSize: RFPercentage(1.4),
            fontFamily: book,
            color: 'gray',
            letterSpacing: 0.3,
          }}>
          Please select merchant first to load the location
        </Text>
      </View>
    );
  } else {
    return (
      <View style={{flex: 1}}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{width: widthMap, height: heightMap}}
          region={coordinate}>
          <Marker
            coordinate={{
              latitude: coordinate?.latitude,
              longitude: coordinate?.longitude,
            }}
            title={location}
          />
        </MapView>
      </View>
    );
  }
};

export const ModalMerchant = props => {
  const {visible, onClose, listMerchant, onSelectMerchant} = props;

  return (
    <Modal visible={visible} transparent animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <HeaderModal onClose={onClose} title={'Select Merchant'} />
          <FlatList
            contentContainerStyle={{padding: 15}}
            legacyImplementation={true}
            initialNumToRender={5}
            disableVirtualization={true}
            data={listMerchant}
            extraData={listMerchant}
            keyExtractor={(item, index) => `${item?.id}`}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() => onSelectMerchant(item)}
                  style={{
                    marginBottom: 15,
                    paddingTop: 10,
                    paddingBottom: 15,
                    borderBottomWidth: 1,
                    borderBottomColor: headerBorderBottom,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item?.name}
                  </Text>
                </TouchableOpacity>
              );
            }}
            ListEmptyComponent={() => {
              return (
                <View style={{justifyContent: 'center', paddingTop: 15}}>
                  <ActivityIndicator size="large" color={mainGreen} />
                </View>
              );
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export const ModalService = props => {
  const {visible, onClose, listServiceType, onSelectServiceType} = props;

  return (
    <Modal visible={visible} transparent animationType="slide">
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={onClose}
          style={{flex: 1, backgroundColor: overlayDim}}
        />
        <View
          style={{
            width: '100%',
            height: width * 1.5,
            backgroundColor: white,
          }}>
          <HeaderModal onClose={onClose} title={'Select Service'} />
          <FlatList
            contentContainerStyle={{padding: 15}}
            data={listServiceType}
            extraData={listServiceType}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() => onSelectServiceType(item)}
                  style={{
                    marginBottom: 15,
                    paddingTop: 10,
                    paddingBottom: 15,
                    borderBottomWidth: 1,
                    borderBottomColor: headerBorderBottom,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    {item}
                  </Text>
                </TouchableOpacity>
              );
            }}
            ListEmptyComponent={() => {
              return (
                <View style={{justifyContent: 'center', paddingTop: 15}}>
                  <ActivityIndicator size="large" color={mainGreen} />
                </View>
              );
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export const HeaderModal = props => {
  const {onClose, title} = props;

  return (
    <View
      style={{
        backgroundColor: white,
        bottom: 13,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomColor: headerBorderBottom,
        borderBottomWidth: 1,
        width: '100%',
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={onClose} style={{padding: 5}}>
          <Icon
            type="Feather"
            name="x"
            style={{fontSize: RFPercentage(2.4), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: medium,
            color: black,
            fontSize: RFPercentage(2),
            letterSpacing: 0.3,
            textAlign: 'center',
          }}>
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
      />
    </View>
  );
};

export const FormButton = props => {
  const {title, value, placeholder, onOpen} = props;

  return (
    <View style={{width: '100%'}}>
      <View style={{width: '100%', paddingBottom: 10}}>
        <Text>
          <Text
            style={{
              fontFamily: medium,
              color: mainRed,
              fontSize: RFPercentage(1.8),
            }}>
            *
          </Text>
          <Text
            style={{
              fontFamily: medium,
              color: black,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
            }}>
            {title}
          </Text>
        </Text>
        <TouchableOpacity
          onPress={() => onOpen()}
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 3,
            paddingRight: 3,
            borderBottomColor: headerBorderBottom,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              fontFamily: book,
              color: value ? black : greyLine,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              marginRight: 10,
            }}>
            {title === 'Service'
              ? value
                ? value
                : placeholder
              : value
              ? value?.name
              : placeholder}
          </Text>
          <Icon
            type="Feather"
            name="chevron-down"
            style={{fontSize: RFPercentage(2.2), color: greyLine}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const Form = props => {
  const {title, value, onChange, placeholder, numberOfLines, required} = props;

  return (
    <View style={{width: '100%', marginVertical: 10}}>
      <Text>
        <Text
          style={{
            color: required ? mainRed : 'transparent',
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          *
        </Text>
        <Text
          style={{
            color: black,
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
          }}>
          {title}
        </Text>
      </Text>
      <View style={{width: '100%'}}>
        <TextInput
          keyboardType="numeric"
          numberOfLines={numberOfLines}
          underlineColorAndroid={'transparent'}
          placeholderTextColor={greyLine}
          style={
            Platform?.OS === 'android'
              ? {
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  borderBottomColor: headerBorderBottom,
                  borderBottomWidth: 1,
                  letterSpacing: 0.3,
                }
              : {
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  borderBottomColor: headerBorderBottom,
                  borderBottomWidth: 1,
                  letterSpacing: 0.3,
                  height: 45,
                  paddingLeft: 4,
                }
          }
          value={value}
          placeholder={placeholder}
          onChangeText={e => {
            onChange(e);
          }}
        />
      </View>
    </View>
  );
};

export const Headers = props => {
  const {goBack, title} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
      />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(LoveAndWinForm);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
