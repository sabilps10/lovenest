import React, {useEffect, useState, useRef, useCallback} from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  ImageBackground,
  View,
  Animated,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {charImage} from '../../../utils/Themes/Images';
import moment from 'moment';

const {width, height} = Dimensions?.get('screen');
const {
  white,
  black,
  mainGreen,
  mainRed,
  headerBorderBottom,
  greyLine,
  overlayDim,
  lightSalmon,
} = Colors;
const {book, medium} = FontType;
const {
  charLoveAndWinBg,
  charRibbonTopTime,
  charRibbonDateDraw,
  charRibbonToWin,
  charChanceCard,
  charChanceCardRibbon,
  charLoveWinEmpty,
} = charImage;

import GRAND_DRAW from '../../../graphql/queries/loveAndWin';

const LoveAndWin = props => {
  const {positionYBottomNav} = props;

  const [loveAndWinID, setLoveAndWinID] = useState(null);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  const [totalGrandDraw, setTotalGrandDraw] = useState(1500);
  const [address, setAddress] = useState('');

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [drawNumber, setDrawNumber] = useState(null);
  const [tnc, setTnc] = useState('');

  const [merchantParticipant, setMerchantParticipant] = useState([]);

  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    onChangeOpacity(false);
    fetch();
    const focus = props?.navigation?.addListener('focus', () => {
      fetch();
    });

    if (refreshing) {
      onRefresh();
    }

    return focus;
  }, [refreshing]);

  const onRefresh = useCallback(() => {
    try {
      fetch();
    } catch (error) {
      setRefreshing(false);
    }
  }, [refreshing]);

  const fetch = () => {
    try {
      props?.client
        ?.query({
          query: GRAND_DRAW,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async res => {
          console.log('Result Fetch Grand Draw: ', res);
          const {data, errors} = res;
          const {loveAndWin} = data;
          const {data: result, error} = loveAndWin;

          if (errors || error) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else if (!result) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            //  success get data
            await setTnc(result?.terms);
            const start = moment(result?.startDate).format(
              'DD MMMM YYYY hh:mm',
            );
            const end = moment(result?.endDate).format('DD MMMM YYYY hh:mm');
            await setLoveAndWinID(result?.id ? result?.id : null);
            await setTotalGrandDraw(result?.totalCash ? result?.totalCash : 0);
            await setStartDate(start);
            await setEndDate(end);
            await setAddress(
              result?.location
                ? `${result?.location?.locationName}, ${result?.location?.address}`
                : 'LOVE NEST, CITY GATE SUPER, #02-16 199597',
            );
            await setDrawNumber(
              result?.submission ? result?.submission?.chanceNumber : null,
            );
            await setMerchantParticipant([...result?.merchants]);
            await setIsError(false);
            await setIsLoading(false);
            await setRefreshing(false);
          }
        })
        .catch(error => {
          throw error;
        });
    } catch (error) {
      console.log('Error fetch grandraw: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      props?.navigation?.goBack(null);
    } catch (error) {}
  };

  const goToHistory = () => {
    try {
      props?.navigation?.navigate('LoveAndWinHistory');
    } catch (error) {}
  };

  const goToSubmit = () => {
    try {
      props?.navigation?.navigate('LoveAndWinForm', {id: loveAndWinID});
    } catch (error) {}
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <Headers
          title="Love & Win Cash"
          goBack={goBack}
          goToHistory={goToHistory}
        />
        <View
          style={{
            flex: 1,
            padding: 15,
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <Headers
          title="Love & Win Cash"
          goBack={goBack}
          goToHistory={goToHistory}
        />
        <View
          style={{
            flex: 1,
            zIndex: -99,
          }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }
            contentContainerStyle={{padding: 15}}>
            <View
              style={{
                width: '100%',
                height: width * 1.6,
                borderWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={charLoveWinEmpty}
                style={{width: '100%', height: width * 0.5, marginBottom: 15}}
              />
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(2.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                No Event Yet.
              </Text>
              <Text
                style={{
                  marginTop: 15,
                  textAlign: 'center',
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Stay tune for the Love & Win Cash Event
              </Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <View style={{flex: 1}}>
          <Headers
            title="Love & Win Cash"
            goBack={goBack}
            goToHistory={goToHistory}
          />
          <View style={{flex: 1, backgroundColor: '#FADCDA'}}>
            <ScrollView
              bounces={false}
              contentContainerStyle={{paddingBottom: 15}}>
              <ImageBackground
                source={charLoveAndWinBg}
                style={{
                  flex: 1,
                  width: null,
                  minHeight: width * 1.6,
                  borderWidth: 0,
                }}>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    flex: 1,
                    paddingTop: 260,
                  }}>
                  <CountDown
                    navigation={props?.navigation}
                    startDate={startDate}
                    endDate={endDate}
                  />
                  <GrandDrawPrice
                    address={address}
                    endDate={endDate}
                    totalGrandDraw={totalGrandDraw}
                  />
                </View>
              </ImageBackground>
              {drawNumber ? (
                <ChanceCardGrandDraw drawNumber={drawNumber} />
              ) : null}
              <Submit
                title={drawNumber ? 'SUBMIT MORE' : 'SUBMIT'}
                disabled={drawNumber ? false : false}
                onPress={() => {
                  goToSubmit();
                }}
              />
              <ListMerchant data={merchantParticipant} />
              <View
                style={{
                  marginTop: 25,
                  width: '100%',
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginBottom: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(2),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    TERMS & CONDITIONS
                  </Text>
                </View>
                <TNC data={tnc} />
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
};

export const ListMerchant = props => {
  const {data} = props;

  return (
    <FlatList
      ListHeaderComponent={() => {
        if (data?.length === 0) {
          return null;
        } else {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                marginBottom: 15,
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(2),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                PARTICIPATING MERCHANTS
              </Text>
            </View>
          );
        }
      }}
      ListFooterComponent={() => {
        if (data?.length === 0) {
          return null;
        } else {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  marginTop: 30,
                  width: '30%',
                  height: 1,
                  borderWidth: 1,
                  borderColor: white,
                }}
              />
            </View>
          );
        }
      }}
      data={data}
      extraData={data}
      keyExtractor={(item, index) => `${item?.id}`}
      numColumns={3}
      columnWrapperStyle={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      renderItem={({item, index}) => {
        return (
          <View style={{flex: 1 / 3, padding: 10}}>
            <View
              style={{
                width: '100%',
                height: width * 0.2,
                backgroundColor: '#FADCDA',
              }}>
              <Image
                source={{uri: item?.logoImageUrl}}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 0,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 1,
                  },
                  shadowOpacity: 0.22,
                  shadowRadius: 2.22,
                  elevation: 3,
                }}
                resizeMode={'contain'}
              />
            </View>
          </View>
        );
      }}
    />
  );
};

export const TNC = props => {
  console.log('TNC Props: ', props);

  const {data} = props;

  if (data !== 0) {
    const split = String(data).split('\n');
    return (
      <React.Fragment>
        {split?.map((d, i) => {
          return (
            <View style={{width: '100%', flexDirection: 'row'}}>
              <Text>{'\u2022'}</Text>
              <Text style={{flex: 1, paddingLeft: 5}}>{`${d}\n`}</Text>
            </View>
          );
        })}
      </React.Fragment>
    );
  } else {
    return (
      <ActivityIndicator
        size="small"
        color={mainGreen}
        style={{alignSelf: 'center'}}
      />
    );
  }
};

export const ChanceCardGrandDraw = props => {
  const {drawNumber} = props;
  return (
    <View style={{width: '100%', marginBottom: 35, bottom: 50, borderWidth: 0}}>
      <Text
        style={{
          marginBottom: 25,
          fontFamily: medium,
          fontSize: RFPercentage(2.3),
          color: '#401B13',
          letterSpacing: 0.3,
          textAlign: 'center',
          alignSelf: 'center',
        }}>
        Thanks For Submit
      </Text>
      <Text
        style={{
          marginBottom: 15,
          fontFamily: medium,
          fontSize: RFPercentage(1.4),
          color: '#401B13',
          letterSpacing: 0.3,
          textAlign: 'center',
          alignSelf: 'center',
        }}>
        LOVE & WIN CHANCE CARD
      </Text>
      <ImageBackground
        source={charChanceCard}
        style={{
          alignSelf: 'center',
          width: width * 0.65,
          minHeight: width * 0.35,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}>
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            width: '100%',
            left: 0,
            right: 0,
            borderWidth: 0,
            paddingLeft: 10,
            paddingRight: 10,
          }}>
          <Image
            source={charChanceCardRibbon}
            style={{
              top: 7,
              zIndex: 99,
              width: width * 0.25,
              height: width * 0.08,
              alignSelf: 'flex-end',
            }}
          />
          <View
            style={{
              zIndex: 1,
              padding: 8,
              paddingTop: 12,
              paddingBottom: 12,
              width: '100%',
              backgroundColor: white,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.4),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              {drawNumber ? drawNumber : 'NO DRAW NUMBER'}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export const Submit = props => {
  const {onPress, disabled, title} = props;

  return (
    <View
      style={{
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 40,
      }}>
      <TouchableOpacity
        disabled={disabled}
        onPress={onPress}
        style={{
          padding: 15,
          paddingLeft: 85,
          paddingRight: 85,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: disabled ? headerBorderBottom : mainGreen,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

          elevation: 5,
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.6),
            color: disabled ? greyLine : white,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </TouchableOpacity>
      <View
        style={{
          marginTop: 25,
          width: '30%',
          height: 1,
          borderWidth: 1,
          borderColor: white,
        }}
      />
    </View>
  );
};

export const GrandDrawPrice = props => {
  const {address, endDate, totalGrandDraw} = props;

  return (
    <View
      style={{
        borderWidth: 0,
        bottom: 35,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 0,
        paddingBottom: 5,
      }}>
      <ImageBackground
        source={charRibbonToWin}
        style={{
          zIndex: 10,
          top: 35,
          width: width * 0.6,
          height: width * 0.2,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 5,
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(3.2),
            color: white,
            letterSpacing: 0.3,
            textShadowOffset: {width: 1, height: 1},
            textShadowRadius: 1,
            textShadowColor: mainRed,
            bottom: 2,
          }}>
          $ {totalGrandDraw}
        </Text>
      </ImageBackground>
      <ImageBackground
        source={charRibbonDateDraw}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 20,
          paddingBottom: 5,
          zIndex: 1,
          width: width * 0.8,
          height: width * 0.25,
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1),
            color: black,
            letterSpacing: 0.3,
          }}>
          LOVE & WIN CASH GRAND DRAW IS ON
        </Text>
        <Text
          style={{
            top: 1,
            marginTop: 3,
            marginBottom: 8,
            fontFamily: medium,
            fontSize: RFPercentage(2.5),
            color: white,
            letterSpacing: 0.3,
            textShadowOffset: {width: 1, height: 1},
            textShadowRadius: 1,
            textShadowColor: mainRed,
          }}>
          {moment(endDate).format('DD MMMM YYYY')}
        </Text>
        <Text
          style={{
            bottom: 2,
            fontFamily: medium,
            fontSize: RFPercentage(1),
            color: black,
            letterSpacing: 0.3,
            textAlign: 'center',
            lineHeight: 10,
          }}>
          {moment(endDate).format('hh:mm A')} | {address}
        </Text>
      </ImageBackground>
    </View>
  );
};

export const CountDown = props => {
  console.log('CountDown Props >>>> ', props);
  const {startDate, endDate} = props;

  const timerRef = useRef(null);

  const [isExpired, setIsExpired] = useState(false);
  const [days, setDays] = useState(NaN);
  const [hours, setHours] = useState(NaN);

  const [distance, setDistance] = useState(null);

  useEffect(() => {
    timerRef.current = setInterval(() => {
      const end = moment(endDate).utc();
      console.log('isi end', end);
      const now = moment().utc();
      console.log('isi now', now);

      const distances = moment(end - now);
      console.log('DISTANCE HEART BEAT: ', distances);
      setDistance(distances);
      const dataHours = Math.floor(
        (Math.abs(distance) % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
      );
      // Math.floor(Math.abs(distance) / (1000 * 60 * 60 * 24));
      console.log('isi dataHours', dataHours);

      const dataDays = Math.floor(Math.abs(distance) / (1000 * 60 * 60 * 24));

      setHours(dataHours);
      setDays(dataDays);
    }, 1000);
    if (distance < 0) {
      clearInterval(timerRef.current);
      setIsExpired(!isExpired);
    }

    const onBlur = props?.navigation?.addListener('blur', () => {
      clearInterval(timerRef.current);
    });

    return () => {
      onBlur;
      clearInterval(timerRef.current);
    };
  }, [distance]);

  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0,
        paddingTop: 15,
      }}>
      <ImageBackground
        resizeMode="cover"
        source={charRibbonTopTime}
        style={{
          zIndex: 10,
          justifyContent: 'center',
          alignItems: 'center',
          width: width * 0.7,
          height: width * 0.18,
          borderWidth: 0,
        }}>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {isExpired ? (
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
                bottom: 5,
              }}>
              EXPIRED
            </Text>
          ) : (
            <Text
              style={{
                lineHeight: 18,
                textAlign: 'center',
                bottom: Number?.isNaN(days) && Number?.isNaN(hours) ? 0 : 5,
              }}>
              {Number?.isNaN(days) ? (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator size="small" color={mainGreen} />
                  <Text
                    style={{
                      marginHorizontal: 5,
                      fontFamily: book,
                      fontSize: RFPercentage(1.3),
                      color: black,
                    }}>
                    {' DAY(s) : '}
                  </Text>
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                  }}>
                  {days}
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.3),
                      color: black,
                    }}>
                    {' DAY(s) : '}
                  </Text>
                </Text>
              )}
              {Number?.isNaN(hours) ? (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator size="small" color={mainGreen} />
                  <Text
                    style={{
                      marginHorizontal: 5,
                      fontFamily: book,
                      fontSize: RFPercentage(1.3),
                      color: black,
                    }}>
                    {' HOUR(s) : '}
                  </Text>
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                  }}>
                  {hours}
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: RFPercentage(1.3),
                      color: black,
                    }}>
                    {' HOUR(s) : '}
                  </Text>
                </Text>
              )}
            </Text>
          )}
        </View>
      </ImageBackground>
      <View
        style={{
          zIndex: 1,
          minHeight: width * 0.15,
          bottom: 5,
          width: width * 0.65,
          backgroundColor: white,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 13,
          paddingBottom: 5,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

          elevation: 5,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.5),
            color: black,
            letterSpacing: 0.3,
          }}>
          PERIOD
        </Text>
        <Text
          style={{
            marginTop: 8,
            fontFamily: medium,
            fontSize: RFPercentage(1.5),
            color: '#644640',
            letterSpacing: 0.3,
          }}>
          {moment(startDate)?.format('DD MMMM YYYY')} -{' '}
          {moment(endDate)?.format('DD MMMM YYYY')}
        </Text>
      </View>
    </View>
  );
};

export const Headers = props => {
  const {goBack, title, goToHistory} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => goToHistory()}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="MaterialCommunityIcons"
            name="history"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(LoveAndWin);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
