import React, {useCallback, useEffect, useState} from 'react';
import {
  Text,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Image,
  View,
  Animated,
  SafeAreaView,
  ActivityIndicator,
  ScrollView,
  FlatList,
  RefreshControl,
  Modal,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Header, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import _ from 'lodash';
import moment from 'moment';

import {charImage} from '../../../utils/Themes/Images';

const {charLoveAndWinHostoryIcon, charLoveWinEmpty} = charImage;
const {width} = Dimensions?.get('screen');
const {
  white,
  black,
  mainGreen,
  mainRed,
  headerBorderBottom,
  greyLine,
  overlayDim,
} = Colors;
const {book, medium} = FontType;

// Query
import GET_SUBMISSIONS from '../../../graphql/queries/getSubmissions';

// Mutation
import DELETE_SUBMISSION_BY_ID from '../../../graphql/mutations/customerSubmissionDelete';

const LoveAndWin = props => {
  const {positionYBottomNav} = props;

  const itemDisplayed = 3;
  const [list, setList] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  const [showModalDelete, setShowModalDelete] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const [message, setMessage] = useState('');
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);

  useEffect(() => {
    onChangeOpacity(false);
    fetch();

    const focus = props?.navigation?.addListener('focus', () => {
      setRefreshing(true);
    });

    if (refreshing) {
      onRefresh();
    }

    if (isLoadMore) {
      onLoadMore();
    }

    return () => focus;
  }, [refreshing, isLoadMore]);

  const onLoadMore = useCallback(() => {
    try {
      setPageNumber(pageNumber + 1);
      fetch();
    } catch (error) {
      console.log('OnLoadMore Error: ', error);
      setIsLoadMore(false);
    }
  }, [isLoadMore]);

  const onRefresh = useCallback(() => {
    try {
      setPageNumber(1);
      fetch();
    } catch (error) {
      console.log('Error refreshing: ', error);
      setRefreshing(false);
    }
  }, [refreshing]);

  const fetch = () => {
    try {
      props?.client
        ?.query({
          query: GET_SUBMISSIONS,
          variables: {
            itemDisplayed,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(res => {
          console.log('Res fetch submissions list: ', res);
          const {data, errors} = res;
          const {getSubmissions} = data;
          const {data: result, error, totalCount} = getSubmissions;

          if (errors || error) {
            setIsError(true);
            setIsLoading(false);
            setRefreshing(false);
            setIsLoadMore(false);
          } else {
            setTotalData(totalCount);
            let oldData = list;
            let mergerData = [...oldData, ...result];
            const cleanDuplicated = _.uniqBy(mergerData, 'id');
            console.log('cleanDuplicated: ', cleanDuplicated);
            setList(pageNumber === 1 ? [...result] : [...cleanDuplicated]);
            setIsError(false);
            setIsLoading(false);
            setRefreshing(false);
            setIsLoadMore(false);
          }
        })
        .catch(error => {
          console.log('Error ANu: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error ANu: ', error);
      setIsError(true);
      setIsLoading(false);
      setRefreshing(false);
      setIsLoadMore(false);
    }
  };

  // Bottom Nav Bar set to hidden
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const goBack = () => {
    try {
      props?.navigation?.goBack(null);
    } catch (error) {}
  };

  const deleteSubmissionById = () => {
    try {
      setIsLoadingDelete(true);
      props?.client
        ?.mutate({
          mutation: DELETE_SUBMISSION_BY_ID,
          variables: {
            id: selectedItem?.id,
          },
        })
        .then(res => {
          const {data, errors} = res;
          const {customerSubmissionDelete} = data;
          const {error} = customerSubmissionDelete;

          if (errors || error) {
            setMessage('Failed to delete history');
            setIsLoadingDelete(false);

            setTimeout(() => {
              setMessage('');
              setIsLoadingDelete(false);
              setShowModalDelete(false);
            }, 1000);
          } else {
            setIsLoadingDelete(false);
            setMessage('Successfully delete');

            setTimeout(() => {
              setMessage('');
              setIsLoadingDelete(false);
              setShowModalDelete(false);
              setShowModalDelete(false);
              setRefreshing(true);
            }, 1000);
          }
        })
        .catch(error => {
          throw new Error(
            String(
              error?.message ? error?.message : 'Failed to delete history',
            ),
          );
        });
    } catch (error) {
      setMessage('Failed to delete history');
      setIsLoadingDelete(false);

      setTimeout(() => {
        setMessage('');
        setIsLoadingDelete(false);
        setShowModalDelete(false);
      }, 1000);
    }
  };

  if (isLoading && !isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <Headers title="History" goBack={goBack} />
        <View style={{flex: 1}}>
          <ActivityIndicator
            size="large"
            color={mainGreen}
            style={{marginTop: 10}}
          />
        </View>
      </SafeAreaView>
    );
  } else if (!isLoading && isError) {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <Headers title="History" goBack={goBack} />
        <View style={{flex: 1}}>
          <ScrollView
            contentContainerStyle={{paddingLeft: 15, paddingRight: 15}}>
            <View
              style={{
                width: '100%',
                height: width * 1.6,
                borderWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={charLoveWinEmpty}
                style={{width: '100%', height: width * 0.5, marginBottom: 25}}
              />
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(2.8),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                You're not submit the Grand Draw yet.
              </Text>
              <Text
                style={{
                  marginTop: 15,
                  textAlign: 'center',
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Submit your data to win Love & Win Cash
              </Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: white}}>
        <Headers title="History" goBack={goBack} />
        <ModalConfirmation
          message={message}
          loading={isLoadingDelete}
          chanceNumber={selectedItem ? selectedItem?.chanceNumber : ''}
          visible={showModalDelete}
          onCancel={() => setShowModalDelete(false)}
          onDelete={() => {
            try {
              console.log('Item Selected: ', selectedItem);
              deleteSubmissionById();
            } catch (error) {
              setSelectedItem(null);
              setShowModalDelete(false);
              console.log('Error onDelete Modal: ', error);
            }
          }}
        />
        <View style={{flex: 1, zIndex: -99}}>
          <FlatList
            ListEmptyComponent={() => {
              return (
                <View
                  style={{
                    width: '100%',
                    height: width * 1.6,
                    borderWidth: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={charLoveWinEmpty}
                    style={{
                      width: '100%',
                      height: width * 0.5,
                      marginBottom: 25,
                    }}
                  />
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: medium,
                      fontSize: RFPercentage(2.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    You're not submit the Grand Draw yet.
                  </Text>
                  <Text
                    style={{
                      marginTop: 15,
                      textAlign: 'center',
                      fontFamily: book,
                      fontSize: RFPercentage(1.6),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    Submit your data to win Love & Win Cash
                  </Text>
                </View>
              );
            }}
            onEndReached={() => {
              try {
                if (list?.length < totalData) {
                  setIsLoadMore(true);
                } else {
                  setIsLoadMore(false);
                }
              } catch (error) {
                console.log('Error On End Reach: ', error);
                setIsLoadMore(false);
              }
            }}
            onEndReachedThreshold={0.05}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => setRefreshing(true)}
              />
            }
            contentContainerStyle={{padding: 15}}
            data={list}
            extraData={list}
            keyExtractor={(item, index) => `${item?.id}`}
            renderItem={({item, index}) => {
              console.log('ITEM MA BRU: ', item);
              return (
                <HistoryCard
                  item={item}
                  onEdit={() => {
                    try {
                      props?.navigation?.navigate('LoveAndWinHistoryDetail', {
                        id: item?.id,
                        loveWinId: item?.loveWinId,
                      });
                    } catch (error) {
                      console.log('Error: ', error);
                    }
                  }}
                  onDelete={() => {
                    setSelectedItem(item);
                    setShowModalDelete(true);
                  }}
                />
              );
            }}
            ListFooterComponent={() => {
              if (isLoadMore) {
                return (
                  <View
                    style={{
                      paddingTop: 10,
                      paddingRight: 10,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size="large" color={mainGreen} />
                  </View>
                );
              } else {
                return (
                  <View
                    style={{
                      paddingTop: 10,
                      paddingRight: 10,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size="large" color={white} />
                  </View>
                );
              }
            }}
          />
        </View>
      </SafeAreaView>
    );
  }
};

export const ModalConfirmation = props => {
  const {visible, onCancel, onDelete, chanceNumber, message, loading} = props;

  return (
    <Modal visible={visible} transparent animationType="fade">
      <SafeAreaView
        style={{
          flex: 1,
        }}>
        <View
          style={{
            flex: 1,
            backgroundColor: overlayDim,
            justifyContent: 'center',
            alignItems: 'center',
            paddingLeft: 25,
            paddingRight: 25,
          }}>
          <View
            style={{
              backgroundColor: white,
              borderRadius: 5,
              padding: 10,
              paddingTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {loading ? (
              <ActivityIndicator
                size="small"
                color={mainGreen}
                style={{marginBottom: 10}}
              />
            ) : (
              <Text
                style={{
                  marginBottom: 10,
                  fontSize: RFPercentage(1.8),
                  textAlign: 'center',
                }}>
                Are you sure want to delete
                <Text
                  style={{
                    fontFamily: medium,
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  {` ${chanceNumber}`}
                </Text>
              </Text>
            )}
            <View
              style={{
                marginTop: 15,
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => onCancel()}
                style={{
                  flex: 1,
                  borderWidth: 1,
                  borderColor: mainGreen,
                  backgroundColor: mainGreen,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                  marginRight: 15,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: white,
                    letterSpacing: 0.3,
                    fontSize: RFPercentage(1.6),
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onDelete()}
                style={{
                  flex: 1,
                  marginLeft: 15,
                  borderWidth: 1,
                  borderColor: mainGreen,
                  backgroundColor: white,
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: mainGreen,
                    letterSpacing: 0.3,
                    fontSize: RFPercentage(1.6),
                  }}>
                  {loading ? 'Deleting...' : 'Delete'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export const HistoryCard = props => {
  const {item, onEdit, onDelete} = props;
  return (
    <View
      style={{
        marginBottom: 15,
        backgroundColor: white,
        borderWidth: 1,
        borderColor: headerBorderBottom,
        width: '100%',
        padding: 10,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.06,
        shadowRadius: 2.22,
        elevation: 3,
      }}>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          borderBottomWidth: 1,
          borderBottomColor: headerBorderBottom,
          paddingBottom: 15,
        }}>
        <View
          style={{
            flex: 0.15,
            borderWidth: 0,
            borderRadius: 5,
            backgroundColor: headerBorderBottom,
          }}>
          <Image
            resizeMode="cover"
            source={charLoveAndWinHostoryIcon}
            style={{
              borderRadius: 5,
              width: '100%',
              height: width * 0.12,
              borderWidth: 0,
            }}
          />
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 5,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.5),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.chanceNumber}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 5,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.2),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.createdOn
                ? moment(item?.createdOn).format('DD MMM YYYY')
                : 'N/A'}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 5,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.2),
                color: black,
                letterSpacing: 0.3,
              }}>
              {item?.createdOn
                ? moment(item?.createdOn).format('hh:mm A')
                : 'N/A'}
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          paddingTop: 10,
          paddingBottom: 5,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}>
            AMOUNT OF PACKAGE
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <Text>
            {item?.totalAmount ? `$ ${item?.totalAmount.toFixed(2)}` : 'N/A'}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          paddingTop: 10,
          paddingBottom: 5,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}>
            DEPOSIT AMOUNT
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <Text>
            {item?.depositAmount
              ? `$ ${item?.depositAmount.toFixed(2)}`
              : 'N/A'}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}>
            MERCHANT
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <Text>{item?.merchant?.name}</Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          paddingTop: 5,
          paddingBottom: 10,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.5),
              color: black,
              letterSpacing: 0.3,
            }}>
            SERVICE
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <Text>{item?.merchant?.serviceType}</Text>
        </View>
      </View>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: headerBorderBottom,
          width: '100%',
          height: 1,
          marginVertical: 5,
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 10,
          width: '100%',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => onEdit()}
          style={{
            padding: 10,
            paddingLeft: 25,
            paddingRight: 25,
            backgroundColor: mainGreen,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.5),
              color: white,
              letterSpacing: 0.3,
            }}>
            EDIT
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onDelete()}
          style={{
            padding: 10,
            paddingLeft: 15,
            paddingRight: 15,
            backgroundColor: white,
            borderWidth: 1,
            borderColor: mainGreen,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.5),
              color: mainGreen,
              letterSpacing: 0.3,
            }}>
            DELETE
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const Headers = props => {
  const {goBack, title} = props;

  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: headerBorderBottom,
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
      }}>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={goBack}
          style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: RFPercentage(2.5), color: black}}
          />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.8),
            color: black,
            letterSpacing: 0.3,
          }}>
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 0.1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
      />
    </Header>
  );
};

const mapToState = state => {
  console.log('mapToState: ', state);
  const {positionYBottomNav} = state;
  return {
    positionYBottomNav,
  };
};

const mapToDispatch = () => {
  return {};
};

const ConnectingComponent = connect(mapToState, mapToDispatch)(LoveAndWin);

const Wrapper = compose(withApollo)(ConnectingComponent);

export default props => <Wrapper {...props} />;
