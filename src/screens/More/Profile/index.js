import React from 'react';
import {View, Text, Image, StyleSheet, StatusBar, Animated} from 'react-native';
import {Container, Content, Card, CardItem, Icon, Button} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {profileIcon} from '../../../utils/Themes/Images';
import Loader from '../../../components/Loader/circleLoader';
import NotLoginYet from '../../../components/ErrorScreens/NotLoginYet';
import {CommonActions, StackActions} from '@react-navigation/native';
import AsyncImage from '../../../components/Image/AsyncImageProfile';

//GraphQL Queries
import GETUSERDETAIL from '../../../graphql/queries/getCustomer';

// Button Nav
import ButtonBack from '../../../components/Button/buttonBack';
import ButtonEdit from '../../../components/Button/buttonText';

const {userIcon} = profileIcon;
const {
  newContainerColor,
  black,
  veryLightPinkTwo,
  transparent,
  disableText,
  greyLine,
} = Colors;
const {regular, exregular, large} = FontSize;
const {medium, bold, book} = FontType;

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      mainAccount: null,
      partnerAccount: null,
      marriageDate: '',
      marriageDate2: '',
      keyCollectionDate: '',
      tokenNotFound: false,
      errorMessage: '',
    };
  }

  popStacking = async () => {
    const {navigation} = this.props;
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: true,
    });
    navigation.dispatch(StackActions.pop());
  };

  goToEditProfile = () => {
    const {navigation} = this.props;
    navigation.push('EditProfile');
  };

  navigationOptions = props => {
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'My Account',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        fontSize: regular,
        color: black,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={this.popStacking} />;
      },
      headerRight: () => {
        if (
          this.state.mainAccount === null &&
          this.state.tokenNotFound &&
          this.state.errorMessage === 'You seems not login yet!' &&
          !this.state.isLoading
        ) {
          return null;
        } else {
          return (
            <ButtonEdit
              {...props}
              onPress={this.goToEditProfile}
              text={'Edit'}
            />
          );
        }
      },
    });
  };

  getUserDetail = props => {
    const {client} = props;
    client
      .query({
        query: GETUSERDETAIL,
        fetchPolicy: 'no-cache',
        notifyOnNetworkStatusChange: true,
      })
      .then(response => {
        console.log('response GET USER DETAIL: ', response);
        const {data, errors} = response;

        if (errors) {
          // error status
          this.setState(prevState => ({
            ...prevState,
            tokenNotFound: true,
            errorMessage: 'You seems not login yet!',
            isLoading: false,
          }));
        } else {
          // get data
          if (data?.getCustomer?.length === 0) {
            // no data, and it the same as error
            this.setState(prevState => ({
              ...prevState,
              tokenNotFound: true,
              errorMessage: 'You seems not login yet!',
              isLoading: false,
            }));
          } else {
            if (
              !data?.getCustomer[0]?.partnerName ||
              data?.getCustomer[0]?.partnerName === ''
            ) {
              // no partnet data
              this.setState(
                prevState => ({
                  ...prevState,
                  mainAccount: {
                    ...prevState.mainAccount,
                    id: data?.getCustomer[0]?.id,
                    name: data?.getCustomer[0]?.name,
                    countryCode: data?.getCustomer[0]?.countryCode,
                    mobile: data?.getCustomer[0]?.mobile, // +6583938
                    phone: data?.getCustomer[0]?.phone, // 6557746
                    email: data?.getCustomer[0]?.email,
                    gender: data?.getCustomer[0]?.gender,
                    profileImage:
                      data?.getCustomer[0]?.profileImage === null ||
                      data?.getCustomer[0]?.profileImage === ''
                        ? ''
                        : data?.getCustomer[0]?.profileImage,
                  },
                  marriageDate: data?.getCustomer[0]?.marriageDate,
                  marriageDate2:
                    data?.getCustomer[0]?.marriageDate2 === null
                      ? ''
                      : data?.getCustomer[0]?.marriageDate2,
                  keyCollectionDate:
                    data?.getCustomer[0]?.keyCollectionDate === null
                      ? ''
                      : data?.getCustomer[0]?.keyCollectionDate,
                }),
                () => {
                  this.setState(prevState => ({
                    ...prevState,
                    isLoading: false,
                  }));
                },
              );
            } else {
              // with partner data
              this.setState(
                prevState => ({
                  ...prevState,
                  mainAccount: {
                    ...prevState.mainAccount,
                    id: data?.getCustomer[0]?.id,
                    name: data?.getCustomer[0]?.name,
                    countryCode: data?.getCustomer[0]?.countryCode,
                    mobile: data?.getCustomer[0]?.mobile, // +6583938
                    phone: data?.getCustomer[0]?.phone, // 6557746
                    email: data?.getCustomer[0]?.email,
                    gender: data?.getCustomer[0]?.gender,
                    profileImage:
                      data?.getCustomer[0]?.profileImage === null ||
                      data?.getCustomer[0]?.profileImage === ''
                        ? ''
                        : data?.getCustomer[0]?.profileImage,
                  },
                  partnerAccount: {
                    ...prevState.partnerAccount,
                    partnerName: data?.getCustomer[0]?.partnerName,
                    partnerCountryCode:
                      data?.getCustomer[0]?.partnerCountryCode,
                    partnerMobile: data?.getCustomer[0]?.partnerMobile, // +658748938
                    partnerPhone: data?.getCustomer[0]?.partnerPhone, // 775874
                    partnerGender: data?.getCustomer[0]?.partnerGender,
                    partnerEmail: data?.getCustomer[0]?.partnerEmail,
                  },
                  marriageDate: data?.getCustomer[0]?.marriageDate,
                  marriageDate2:
                    data?.getCustomer[0]?.marriageDate2 === null
                      ? ''
                      : data?.getCustomer[0]?.marriageDate2,
                  keyCollectionDate:
                    data?.getCustomer[0]?.keyCollectionDate === null
                      ? ''
                      : data?.getCustomer[0]?.keyCollectionDate,
                }),
                () => {
                  this.setState(prevState => ({
                    ...prevState,
                    isLoading: false,
                  }));
                },
              );
            }
          }
        }
      })
      .catch(error => {
        console.log('Error: ', error);
        this.setState(prevState => ({
          ...prevState,
          tokenNotFound: false,
          errorMessage: 'Internal Server Error',
          isLoading: false,
        }));
      });
  };

  onRefresh = () => {
    console.log('Masuk Onrefresh');
    const {props} = this;
    this.setState(
      prevState => ({
        ...prevState,
        isLoading: true,
      }),
      () => {
        this.getUserDetail(props);
      },
    );
  };

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentDidMount() {
    const {props} = this;
    const {navigation} = props;
    this.navigationOptions(props);
    this.getUserDetail(props);
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(false);
    }
    this._unsubscribe = navigation.addListener('focus', () => {
      this.navigationOptions(props);
      this.setState(
        prevState => ({
          ...prevState,
          isLoading: true,
        }),
        () => {
          this.getUserDetail(props);
        },
      );
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(false);
      }
      console.log('My Account Main ADD LISTENER DID MOUNT');
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  goToLogin = () => {
    try {
      const {props} = this;
      const {navigation} = props;

      navigation.dispatch(
        CommonActions.navigate({name: 'Login', params: {showXIcon: true}}),
      );
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  render() {
    console.log('Profile Props: ', this.props);
    console.log('Profile State: ', this.state);
    const {state} = this;
    const {isLoading, mainAccount, partnerAccount} = state;

    if (isLoading) {
      return (
        <Container style={styles.container}>
          <StatusBar barStyle="dark-content" backgroundColor={transparent} />
          <Content
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{bottom: 30}}>
              <Loader />
            </View>
          </Content>
        </Container>
      );
    }

    if (
      this.state.mainAccount === null &&
      this.state.tokenNotFound &&
      this.state.errorMessage === 'You seems not login yet!' &&
      !this.state.isLoading
    ) {
      return <NotLoginYet onPress={() => this.goToLogin()} message="Login" />;
    }

    if (
      this.state.mainAccount === null &&
      !this.state.tokenNotFound &&
      this.state.errorMessage === 'Internal Server Error' &&
      !this.state.isLoading
    ) {
      return <NotLoginYet onPress={() => this.onRefresh()} message="Refresh" />;
    }
    return (
      <Container style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={transparent} />
        <Content contentContainerStyle={{paddingTop: 7}}>
          <Card style={styles.card}>
            <CardItem>
              {/* <Image
                source={
                  this.state.mainAccount.profileImage === '' ||
                  this.state.mainAccount.profileImage === undefined
                    ? userIcon
                    : {uri: this.state.mainAccount.profileImage}
                }
                style={styles.image}
              /> */}
              <AsyncImage
                style={styles.image}
                source={{uri: this.state.mainAccount.profileImage}}
                loaderStyle={{width: 98 / 2, height: 98 / 2}}
                placeholderColor={newContainerColor}
              />
            </CardItem>
            {/* =============================================== */}
            <CardItem>
              <Text style={styles.name}>{mainAccount.name}</Text>
            </CardItem>
            <CardItem>
              <Text style={styles.usertype}>
                {mainAccount.gender === 'MALE' ? 'Groom' : 'Bride'}
              </Text>
            </CardItem>
            <CardItem style={styles.cardItem}>
              <View style={styles.viewIcon}>
                <Icon type="Feather" name="mail" style={styles.icon} />
              </View>
              <View style={styles.viewText}>
                <Text style={styles.text}>{mainAccount.email}</Text>
              </View>
            </CardItem>
            <CardItem style={styles.cardItem}>
              <View style={styles.viewIcon}>
                <Icon type="Feather" name="phone" style={styles.icon} />
              </View>
              <View style={styles.viewText}>
                <Text style={styles.text}>{mainAccount.mobile}</Text>
              </View>
            </CardItem>
            <CardItem style={styles.cardItem}>
              <View style={styles.viewIcon}>
                <Icon
                  type="FontAwesome"
                  name={mainAccount.gender === 'MALE' ? 'mars' : 'venus'}
                  style={{left: 5, ...styles.icon}}
                />
              </View>
              <View style={styles.viewText}>
                <Text style={styles.text}>
                  {mainAccount.gender === 'MALE' ? 'Male' : 'Female'}
                </Text>
              </View>
            </CardItem>
            {/* =============================================== */}
            {partnerAccount !== null ? (
              <React.Fragment>
                <CardItem style={{paddingTop: 20, paddingBottom: 20}}>
                  <View
                    style={{
                      width: '90%',
                      height: 0.5,
                      borderWidth: 0.7,
                      borderColor: veryLightPinkTwo,
                    }}
                  />
                </CardItem>
                <CardItem>
                  <Text style={styles.name}>{partnerAccount.partnerName}</Text>
                </CardItem>
                <CardItem>
                  <Text style={styles.usertype}>
                    {partnerAccount.partnerGender === 'MALE'
                      ? 'Groom'
                      : 'Bride'}
                  </Text>
                </CardItem>
                <CardItem style={styles.cardItem}>
                  <View style={styles.viewIcon}>
                    <Icon type="Feather" name="mail" style={styles.icon} />
                  </View>
                  <View style={styles.viewText}>
                    <Text style={styles.text}>
                      {partnerAccount.partnerEmail}
                    </Text>
                  </View>
                </CardItem>
                <CardItem style={styles.cardItem}>
                  <View style={styles.viewIcon}>
                    <Icon type="Feather" name="phone" style={styles.icon} />
                  </View>
                  <View style={styles.viewText}>
                    <Text style={styles.text}>
                      {partnerAccount.partnerMobile}
                    </Text>
                  </View>
                </CardItem>
                <CardItem style={styles.cardItem}>
                  <View style={styles.viewIcon}>
                    <Icon
                      type="FontAwesome"
                      name={
                        partnerAccount.partnerGender === 'MALE'
                          ? 'mars'
                          : 'venus'
                      }
                      style={{left: 5, ...styles.icon}}
                    />
                  </View>
                  <View style={styles.viewText}>
                    <Text style={styles.text}>
                      {partnerAccount.partnerGender === 'MALE'
                        ? 'Male'
                        : 'Female'}
                    </Text>
                  </View>
                </CardItem>
              </React.Fragment>
            ) : null}
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: newContainerColor,
  },
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 0,
    marginRight: 0,
    paddingTop: 24,
    paddingBottom: 24,
    shadowOpacity: 0,
    elevation: 0,
  },
  image: {width: 98, height: 98, borderRadius: 98 / 2},
  name: {
    fontFamily: bold,
    fontSize: exregular,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  usertype: {
    fontFamily: book,
    fontSize: regular,
    color: disableText,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  cardItem: {
    flexDirection: 'row',
    paddingLeft: 100,
    paddingRight: 100,
  },
  viewIcon: {flexBasis: '15%', height: '100%'},
  icon: {top: 0, fontSize: 20, color: black},
  text: {
    fontFamily: book,
    fontSize: regular,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
  viewText: {flexBasis: '85%'},
});

const Wrapper = compose(withApollo)(Profile);

export default props => <Wrapper {...props} />;
