import React from 'react';
import {
  Alert,
  View,
  Text,
  Image,
  StatusBar,
  PermissionsAndroid,
} from 'react-native';
import {Container, Content, Card, CardItem, Button} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import moment from 'moment';
import {launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {ReactNativeFile} from 'apollo-upload-client';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import ActionSheet from 'react-native-actionsheet';
import EmailValidator from '../../../utils/EmailValidator';
import AsyncImage from '../../../components/Image/AsyncImageProfile';
import {StackActions} from '@react-navigation/native';

import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {profileIcon} from '../../../utils/Themes/Images';

// Button Nav
import ButtonBack from '../../../components/Button/buttonBack';
import ButtonEdit from '../../../components/Button/buttonText';
import ButtonImagePicker from '../../../components/Button/buttonImagePicker';

// Text Input Custom
import TextInput from '../../../components/Input/TextInputCustoms';
import GenderPicker from '../../../components/Input/TextInputGenderPicker';
import DatePicker from '../../../components/Input/TextInputDatePicker';
import TextInputPhonePicker from '../../../components/Input/TextInputPhonePicker';
import ModalCountryCode from '../../../components/Modal/onBoardingModal/countryCodeListModal';
import ModalCountryCodePartner from '../../../components/Modal/onBoardingModal/countryCodeListModal';
import Loader from '../../../components/Loader/circleLoader';
import JustLoader from '../../../components/Loader/JustLoader';
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';
import SuccessToaster from '../../../components/Toaster/successToaster';
import ErrorToaster from '../../../components/Toaster/errorToaster';

// GraphQL Query
import userDetailQuery from '../../../graphql/queries/getCustomer';

// GraphQL Mutations
import uploadProfileImage from '../../../graphql/mutations/uploadProfileImage';
import regisCustomer from '../../../graphql/mutations/registerCustomerVer2';

const {userIcon} = profileIcon;
const {newContainerColor, black, transparent, lightSalmon, greyLine} = Colors;
const {regular, exregular, small} = FontSize;
const {medium, bold, book} = FontType;
const FEMALE = [{label: 'Female', value: 'FEMALE'}];
const MALE = [{label: 'Male', value: 'MALE'}];

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      oldProfile: {
        id: '',
        name: '',
        mobile: '',
        email: '',
        gender: '',
        partnerName: '',
        partnerMobile: '',
        partnerPhone: '',
        partnerEmail: '',
        partnerGender: '',
        partnerCountryCode: '+65',
        marriageDate: '',
        marriageDate2: '',
        profileImage: '',
        countryCode: '+65',
        phone: '',
        keyCollectionDate: '',
      },
      profile: {
        id: '',
        name: '',
        mobile: '',
        email: '',
        gender: '',
        partnerName: '',
        partnerMobile: '',
        partnerPhone: '',
        partnerEmail: '',
        partnerGender: '',
        partnerCountryCode: '+65',
        marriageDate: '',
        marriageDate2: '',
        profileImage: '',
        countryCode: '+65',
        phone: '',
        keyCollectionDate: '',
      },
      openWeddingDate: false,
      optionalWeddingDate: false,
      keyCollectionDate: false,
      openModalCountryCode: false,
      openModalCountryCodePartner: false,
      imageUploadLoader: false,
      errors: false,
      errorsMessage: '',
      errorToasterMessage: '',
      errorToasterStatus: false,
      successToasterMessage: '',
      successToasterStatus: false,
      saveIsLoading: false,
    };
    this.contentRef = React.createRef();
  }

  popStacking = async () => {
    if (
      JSON.stringify(this.state.profile) ===
      JSON.stringify(this.state.oldProfile)
    ) {
      console.log('SAMA');
      const {navigation} = this.props;
      await navigation.dangerouslyGetParent().setOptions({
        tabBarVisible: false,
      });
      navigation.dispatch(StackActions.pop());
    } else {
      console.log('Tidak SAMA');
      this.showActionSheet();
    }
  };

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  navigationOptions = (props, state) => {
    console.log('State >>>>>> ', props);
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'My Account',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        fontSize: regular,
        color: black,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={this.popStacking} />;
      },
      headerRight: () => {
        console.log('this.state.saveIsLoading: ', this.state.saveIsLoading);
        return this.loadButtonSave(props);
      },
    });
  };

  loadButtonSave = props => {
    const {state} = this;
    if (state.saveIsLoading) {
      return (
        <View style={{width: 60, height: 60}}>
          <JustLoader />
        </View>
      );
    } else {
      return (
        <ButtonEdit {...props} onPress={this.updateProfile} text={'Save'} />
      );
    }
  };

  updateProfile = async () => {
    try {
      console.log('Masuk Update Func');
      this.setState(
        prevState => ({
          ...prevState,
          saveIsLoading: true,
        }),
        () => {
          const {props, state} = this;
          const {profile} = state;
          const {client} = props;
          const {
            name,
            email,
            gender,
            partnerName,
            partnerPhone,
            partnerEmail,
            partnerGender,
            partnerCountryCode,
            marriageDate,
            marriageDate2,
            profileImage,
            countryCode,
            phone,
            keyCollectionDate,
          } = profile;

          console.log(
            'marriageDate KONTOL: >>>>>> ',
            moment(marriageDate).format('DD MMMM YYYY'),
          );

          if (name === '' || email === '' || phone === '') {
            console.log(0, 'Main account is empty');
            this.setState(prevState => ({
              ...prevState,
              errorToasterMessage: 'Please check your input!',
              errorToasterStatus: true,
              saveIsLoading: false,
            }));
            setTimeout(() => {
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: '',
                errorToasterStatus: false,
              }));
            }, 5000);
          } else {
            if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName === '' || partnerName === null) &&
              (partnerEmail === '' || partnerEmail === null) &&
              (partnerPhone === '' || partnerPhone === null)
            ) {
              // Send To API without partner data
              console.log(1, 'partner empty');
              const params = {
                name,
                email,
                gender,
                marriageDate,
                marriageDate2,
                profileImage,
                countryCode,
                phone,
                keyCollectionDate,
              };
              console.log('Params: ', params);
              EmailValidator(email)
                .then(emailValid => {
                  console.log('Email Valid Status: ', emailValid);
                  if (emailValid) {
                    // is valid
                    client
                      .mutate({
                        mutation: regisCustomer,
                        variables: params,
                      })
                      .then(registerCustomerResponse => {
                        console.log(
                          'registerCustomerResponse: ',
                          registerCustomerResponse,
                        );
                        const {data} = registerCustomerResponse;
                        const {registerCustomerVer2} = data;
                        const {error} = registerCustomerVer2;
                        if (error === null) {
                          this.getUserDetail(props);
                          this.setState(prevState => ({
                            ...prevState,
                            successToasterMessage: 'Successful update',
                            successToasterStatus: true,
                            saveIsLoading: false,
                          }));
                          setTimeout(() => {
                            this.setState(prevState => ({
                              ...prevState,
                              successToasterMessage: '',
                              successToasterStatus: false,
                            }));
                          }, 5000);
                          setTimeout(() => {
                            this.props.navigation.goBack(null);
                          }, 3000);
                        } else {
                          this.setState(prevState => ({
                            ...prevState,
                            errorToasterMessage: 'Failed to update',
                            errorToasterStatus: true,
                            saveIsLoading: false,
                          }));
                          setTimeout(() => {
                            this.setState(prevState => ({
                              ...prevState,
                              errorToasterMessage: '',
                              errorToasterStatus: false,
                            }));
                          }, 5000);
                        }
                      })
                      .catch(error => {
                        console.log('Error: ', error);
                        this.setState(prevState => ({
                          ...prevState,
                          errorToasterMessage: 'Failed to update',
                          errorToasterStatus: true,
                          saveIsLoading: false,
                        }));
                        setTimeout(() => {
                          this.setState(prevState => ({
                            ...prevState,
                            errorToasterMessage: '',
                            errorToasterStatus: false,
                          }));
                        }, 5000);
                      });
                  } else {
                    this.setState(prevState => ({
                      ...prevState,
                      errorToasterMessage: 'Email format is not valid',
                      errorToasterStatus: true,
                      saveIsLoading: false,
                    }));
                    setTimeout(() => {
                      this.setState(prevState => ({
                        ...prevState,
                        errorToasterMessage: '',
                        errorToasterStatus: false,
                      }));
                    }, 5000);
                  }
                })
                .catch(errorEmail => {
                  console.log('errorEmail 0: ', errorEmail);
                  this.setState(prevState => ({
                    ...prevState,
                    errorToasterMessage: 'Email Validator Error',
                    errorToasterStatus: true,
                    saveIsLoading: false,
                  }));
                  setTimeout(() => {
                    this.setState(prevState => ({
                      ...prevState,
                      errorToasterMessage: '',
                      errorToasterStatus: false,
                    }));
                  }, 5000);
                });
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName !== '' || partnerName !== null) &&
              (partnerEmail === '' || partnerEmail === null) &&
              (partnerPhone === '' || partnerPhone === null)
            ) {
              console.log(2, 'partnerEmail & partnerPhone are empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Email & Phone are empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName === '' || partnerName === null) &&
              (partnerEmail !== '' || partnerEmail !== null) &&
              (partnerPhone === '' || partnerPhone === null)
            ) {
              console.log(3, 'partnerName & partnerPhone are empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Name & Phone are empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName === '' || partnerName === null) &&
              (partnerEmail === '' || partnerEmail === null) &&
              (partnerPhone !== '' || partnerPhone !== null)
            ) {
              console.log(4, 'partnerName & partnerEmail are empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Name & Email are Empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName !== '' || partnerName !== null) &&
              (partnerEmail !== '' || partnerEmail !== null) &&
              (partnerPhone === '' || partnerPhone === null)
            ) {
              console.log(5, 'Partnert Phone is empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Phone is empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName === '' || partnerName === null) &&
              (partnerEmail !== '' || partnerEmail !== null) &&
              (partnerPhone !== '' || partnerPhone !== null)
            ) {
              console.log(6, 'partnerName is empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Name is empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName !== '' || partnerName !== null) &&
              (partnerEmail === '' || partnerEmail === null) &&
              (partnerPhone !== '' || partnerPhone !== null)
            ) {
              console.log(7, 'partnerEmail is empty');
              this.setState(prevState => ({
                ...prevState,
                errorToasterMessage: 'Partner Email is Empty',
                errorToasterStatus: true,
                saveIsLoading: false,
              }));
              setTimeout(() => {
                this.setState(prevState => ({
                  ...prevState,
                  errorToasterMessage: '',
                  errorToasterStatus: false,
                }));
              }, 5000);
            } else if (
              name !== '' &&
              email !== '' &&
              phone !== '' &&
              (partnerName !== '' || partnerName !== null) &&
              (partnerEmail !== '' || partnerEmail !== null) &&
              (partnerPhone !== '' || partnerPhone !== null)
            ) {
              // Send to API with all data
              console.log(8, 'All filled');
              const params = {
                name,
                email,
                gender,
                partnerName,
                partnerPhone,
                partnerEmail,
                partnerGender,
                partnerCountryCode,
                marriageDate,
                marriageDate2,
                profileImage,
                countryCode,
                phone,
                keyCollectionDate,
              };
              console.log('Params: ', params);

              EmailValidator(email)
                .then(mainAccountEmailValid => {
                  if (mainAccountEmailValid) {
                    EmailValidator(partnerEmail)
                      .then(partnerEmailValid => {
                        if (partnerEmailValid) {
                          client
                            .mutate({
                              mutation: regisCustomer,
                              variables: params,
                            })
                            .then(async registerCustomerResponse => {
                              console.log(
                                'registerCustomerResponse: ',
                                registerCustomerResponse,
                              );
                              const {data} = registerCustomerResponse;
                              const {registerCustomerVer2} = data;
                              const {error} = registerCustomerVer2;
                              if (error === null) {
                                await this.getUserDetail(props);
                                this.setState(prevState => ({
                                  ...prevState,
                                  successToasterMessage: 'Successful update',
                                  successToasterStatus: true,
                                  saveIsLoading: false,
                                }));
                                setTimeout(() => {
                                  this.setState(prevState => ({
                                    ...prevState,
                                    successToasterMessage: '',
                                    successToasterStatus: false,
                                  }));
                                }, 5000);
                                setTimeout(() => {
                                  this.props.navigation.goBack(null);
                                }, 3000);
                              } else {
                                this.setState(prevState => ({
                                  ...prevState,
                                  errorToasterMessage: 'Failed to update',
                                  errorToasterStatus: true,
                                  saveIsLoading: false,
                                }));
                                setTimeout(() => {
                                  this.setState(prevState => ({
                                    ...prevState,
                                    errorToasterMessage: '',
                                    errorToasterStatus: false,
                                  }));
                                }, 5000);
                              }
                            })
                            .catch(error => {
                              console.log('Error: ', error);
                              this.setState(prevState => ({
                                ...prevState,
                                errorToasterMessage: 'Internal Server Error',
                                errorToasterStatus: true,
                                saveIsLoading: false,
                              }));
                              setTimeout(() => {
                                this.setState(prevState => ({
                                  ...prevState,
                                  errorToasterMessage: '',
                                  errorToasterStatus: false,
                                }));
                              }, 5000);
                            });
                        } else {
                          console.log('Partner Email is not valid');
                          this.setState(prevState => ({
                            ...prevState,
                            errorToasterMessage: 'Partner Email is not valid',
                            errorToasterStatus: true,
                            saveIsLoading: false,
                          }));
                          setTimeout(() => {
                            this.setState(prevState => ({
                              ...prevState,
                              errorToasterMessage: '',
                              errorToasterStatus: false,
                            }));
                          }, 5000);
                        }
                      })
                      .catch(errorPartnerEmailValid => {
                        console.log(
                          'Error Partner Email Valid: ',
                          errorPartnerEmailValid,
                        );
                        this.setState(prevState => ({
                          ...prevState,
                          errorToasterMessage: 'Partner Email Validator Error',
                          errorToasterStatus: true,
                          saveIsLoading: false,
                        }));
                        setTimeout(() => {
                          this.setState(prevState => ({
                            ...prevState,
                            errorToasterMessage: '',
                            errorToasterStatus: false,
                          }));
                        }, 5000);
                      });
                  } else {
                    console.log('Main Account Email is not valid');
                    this.setState(prevState => ({
                      ...prevState,
                      errorToasterMessage: 'Email is format is not valid',
                      errorToasterStatus: true,
                      saveIsLoading: false,
                    }));
                    setTimeout(() => {
                      this.setState(prevState => ({
                        ...prevState,
                        errorToasterMessage: '',
                        errorToasterStatus: false,
                      }));
                    }, 5000);
                  }
                })
                .catch(errorEmailMainAcoount => {
                  console.log(
                    'Error Email Main Account: ',
                    errorEmailMainAcoount,
                  );
                  this.setState(prevState => ({
                    ...prevState,
                    errorToasterMessage: 'Email Validator Error',
                    errorToasterStatus: true,
                    saveIsLoading: false,
                  }));
                  setTimeout(() => {
                    this.setState(prevState => ({
                      ...prevState,
                      errorToasterMessage: '',
                      errorToasterStatus: false,
                    }));
                  }, 5000);
                });
            }
          }
        },
      );
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        errorToasterMessage: 'Server Internal Error',
        errorToasterStatus: true,
        saveIsLoading: false,
      }));
      setTimeout(() => {
        this.setState(prevState => ({
          ...prevState,
          errorToasterMessage: '',
          errorToasterStatus: false,
        }));
      }, 5000);
    }
  };

  getUserDetail = props => {
    try {
      const {client} = props;
      client
        .query({
          query: userDetailQuery,
          fetchPolicy: 'no-cache',
          notifyOnNetworkStatusChange: true,
        })
        .then(response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          if (errors === undefined) {
            const {getCustomer} = data;
            if (getCustomer) {
              if (getCustomer.length) {
                const userDetail = getCustomer[0];
                const marriageDate =
                  getCustomer[0].marriageDate === null
                    ? ''
                    : new Date(
                        moment.utc(getCustomer[0].marriageDate).valueOf(),
                      );
                const marriageDate2 =
                  getCustomer[0].marriageDate2 === null
                    ? ''
                    : new Date(
                        moment.utc(getCustomer[0].marriageDate2).valueOf(),
                      );
                const keyCollectionDate =
                  getCustomer[0].keyCollectionDate === null
                    ? ''
                    : new Date(
                        moment.utc(getCustomer[0].keyCollectionDate).valueOf(),
                      );
                const partnerCountryCode =
                  getCustomer[0].partnerCountryCode === null
                    ? '+65'
                    : getCustomer[0].partnerCountryCode;
                const partnerGender =
                  getCustomer[0].gender === 'MALE' ? 'FEMALE' : 'MALE';
                this.setState(
                  prevState => ({
                    ...prevState,
                    oldProfile: {
                      ...prevState.oldProfile,
                      ...userDetail,
                      partnerCountryCode,
                      partnerGender,
                      marriageDate,
                      marriageDate2,
                      keyCollectionDate,
                    },
                    profile: {
                      ...prevState.profile,
                      ...userDetail,
                      partnerCountryCode,
                      partnerGender,
                      marriageDate,
                      marriageDate2,
                      keyCollectionDate,
                    },
                  }),
                  () => {
                    console.log('NEW STATE: ', this.state);
                    this.setState(prevState => ({
                      ...prevState,
                      isLoading: false,
                    }));
                  },
                );
              } else {
                console.log('Error getting userDetail');
                this.setState(prevState => ({
                  ...prevState,
                  errors: true,
                  errorsMessage: 'Internal Server Error',
                  isLoading: false,
                }));
              }
            } else {
              console.log('Error getting userDetail');
              this.setState(prevState => ({
                ...prevState,
                errors: true,
                errorsMessage: 'Internal Server Error',
                isLoading: false,
              }));
            }
          } else {
            console.log('GrapQL Error: ', errors);
            this.setState(prevState => ({
              ...prevState,
              errors: true,
              errorsMessage: 'Not Login Yet!',
              isLoading: false,
            }));
          }
        })
        .catch(error => {
          console.log('Error: jjjjj', error);
          this.setState(prevState => ({
            ...prevState,
            errors: true,
            errorsMessage: 'Internal Server Error',
            isLoading: false,
          }));
        });
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        errors: true,
        errorsMessage: 'Internal Server Error',
        isLoading: false,
      }));
    }
  };

  onRefresh = () => {
    const {props} = this;
    this.setState(
      prevState => ({
        ...prevState,
        errors: false,
        isLoading: true,
        errorsMessage: '',
      }),
      () => {
        this.getUserDetail(props);
      },
    );
  };

  componentDidMount() {
    const {props, state} = this;
    this.navigationOptions(props, state);
    this.getUserDetail(props);
    this.subscriber = this.props.navigation.addListener('focus', () => {
      this.getUserDetail(props);
    });
  }

  componentWillUnmount() {
    const {props, state} = this;
    this.navigationOptions(props, state);
    this.subscriber();
  }

  openImage = () => {
    try {
      console.log('Masuk Open Image');
      const {props} = this;
      const {client} = props;

      launchImageLibrary(
        {
          mediaType: 'photo',
          selectionLimit: 1,
        },
        res => {
          console.log('res image picker: ', res);
          if (res.didCancel) {
            console.log('Upload image canceled');
          } else if (res.error) {
            console.log('image upload error: ', res.error);
          } else {
            const {uri, type} = res?.assets[0];
            const dateFileGenerated = moment().format('DDMMYYYY');
            const nameFile = `imageProfile${dateFileGenerated}.jpg`;

            ImagePicker.openCropper({
              path: uri,
              width: 300,
              height: 400,
            })
              .then(image => {
                console.log('Cropper Result: ', image);
                const {path} = image;

                if (path) {
                  const file = new ReactNativeFile({
                    uri: path,
                    name: nameFile,
                    type,
                  });
                  console.log('FILE >>> ', file);
                  if (file) {
                    this.setState(
                      prevState => ({
                        ...prevState,
                        imageUploadLoader: true,
                      }),
                      () => {
                        client
                          .mutate({
                            mutation: uploadProfileImage,
                            variables: {
                              imagefile: file,
                            },
                          })
                          .then(response => {
                            console.log('response mutate: ', response);
                            const {data} = response;
                            const {uploadProfileImage: uploadProfileImageAPI} =
                              data;
                            const {error, imageUrl} = uploadProfileImageAPI;

                            if (error === '') {
                              this.setState(
                                prevState => ({
                                  ...prevState,
                                  profile: {
                                    ...prevState.profile,
                                    profileImage: imageUrl,
                                  },
                                }),
                                () => {
                                  this.setState(prevState => ({
                                    ...prevState,
                                    imageUploadLoader: false,
                                  }));
                                },
                              );
                            } else {
                              this.setState(prevState => ({
                                ...prevState,
                                imageUploadLoader: false,
                              }));
                            }
                          })
                          .catch(error => {
                            throw error;
                          });
                      },
                    );
                  }
                }
              })
              .catch(errorCropper => {
                throw errorCropper;
              });
          }
        },
      );
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        imageUploadLoader: false,
      }));
    }
  };

  onChangeText = (label, e) => {
    this.setState(prevState => ({
      ...prevState,
      profile: {
        ...prevState.profile,
        [label]: e,
      },
    }));
  };

  openDatePicker = label => {
    if (label === 'weddingDate') {
      this.setState(prevState => ({
        ...prevState,
        openWeddingDate: !prevState.openWeddingDate,
      }));
    } else if (label === 'optionalWeddingDate') {
      this.setState(prevState => ({
        ...prevState,
        optionalWeddingDate: !prevState.optionalWeddingDate,
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        keyCollectionDate: !prevState.keyCollectionDate,
      }));
    }
  };

  setData = (label, value) => {
    if (label === 'weddingDate') {
      this.setState(
        prevState => ({
          ...prevState,
          profile: {
            ...prevState.profile,
            marriageDate: value,
          },
        }),
        () => {
          this.setState(prevState => ({
            ...prevState,
            openWeddingDate: !prevState.openWeddingDate,
          }));
        },
      );
    } else if (label === 'optionalWeddingDate') {
      this.setState(
        prevState => ({
          ...prevState,
          profile: {
            ...prevState.profile,
            marriageDate2: value,
          },
        }),
        () => {
          this.setState(prevState => ({
            ...prevState,
            optionalWeddingDate: !prevState.optionalWeddingDate,
          }));
        },
      );
    } else {
      this.setState(
        prevState => ({
          ...prevState,
          profile: {
            ...prevState.profile,
            keyCollectionDate: value,
          },
        }),
        () => {
          this.setState(prevState => ({
            ...prevState,
            keyCollectionDate: !prevState.keyCollectionDate,
          }));
        },
      );
    }
  };

  bride = () => {
    this.setState(prevState => ({
      ...prevState,
      openModalCountryCode: !prevState.openModalCountryCode,
    }));
  };

  groom = () => {
    this.setState(prevState => ({
      ...prevState,
      openModalCountryCodePartner: !prevState.openModalCountryCodePartner,
    }));
  };

  clearDate = label => {
    this.setState(prevState => ({
      ...prevState,
      profile: {
        ...prevState.profile,
        [label]: '',
      },
    }));
  };

  goToLogin = () => {
    try {
      const {props} = this;
      const {navigation} = props;

      navigation.dispatch(CommonActions.navigate({name: 'Login'}));
    } catch (error) {
      console.log('Error go to login: ', error);
    }
  };

  logout = async () => {
    try {
      const {props} = this;
      const {navigation, client} = props;
      await AsyncStorage.clear();
      await client
        .resetStore()
        .then(() => {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: 'Main',
                },
              ],
            }),
          );
        })
        .catch(error => {
          console.log('Error: ', error);
        });
    } catch (error) {
      console.log('Error Logout: ', error);
    }
  };

  render() {
    console.log('State Edit Profile: ', this.state);
    if (this.state.isLoading) {
      return (
        <Container style={{backgroundColor: newContainerColor}}>
          <StatusBar barStyle="dark-content" backgroundColor={transparent} />
          <Content
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{bottom: 30}}>
              <Loader />
            </View>
          </Content>
        </Container>
      );
    }

    if (
      !this.state.isLoading &&
      this.state.errors &&
      this.state.errorsMessage === 'Not Login Yet!'
    ) {
      return (
        <ErrorScreen
          onPress={() => this.goToLogin()}
          message="Not Login Yet!"
        />
      );
    }

    if (
      !this.state.isLoading &&
      this.state.errors &&
      this.state.errorsMessage === 'Internal Server Error'
    ) {
      return <ErrorScreen onPress={() => this.onRefresh()} message="Refresh" />;
    }

    console.log('Error Toaster Status: ', this.state.errorToasterStatus);
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        {this.state.successToasterStatus ? (
          <SuccessToaster text={this.state.successToasterMessage} />
        ) : null}
        {this.state.errorToasterStatus ? (
          <ErrorToaster text={this.state.errorToasterMessage} />
        ) : null}
        <Content
          contentContainerStyle={{
            paddingTop: 24,
            paddingBottom: 80,
          }}>
          <Card
            transparent
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 0,
              marginRight: 0,
              shadowOpacity: 0,
              elevation: 0,
            }}>
            <CardItem
              style={{
                flexDirection: 'column',
                backgroundColor: transparent,
                paddingBottom: 0,
              }}>
              <AsyncImage
                style={{width: 98, height: 98, borderRadius: 98 / 2}}
                source={{uri: this.state.profile.profileImage}}
                loaderStyle={{width: 98 / 2, height: 98 / 2}}
                placeholderColor={newContainerColor}
              />
              <ButtonImagePicker
                onPress={this.openImage}
                isLoading={this.state.imageUploadLoader}
              />
            </CardItem>
          </Card>

          {/* BRIDE */}
          <Card
            transparent
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}>
            <CardItem
              style={{
                paddingLeft: 25,
                paddingRight: 25,
                backgroundColor: transparent,
              }}>
              <Text>
                Bride
                {this.state.profile.gender === 'FEMALE' ? (
                  '*'
                ) : (
                  <Text> (Optional)</Text>
                )}
              </Text>
            </CardItem>
          </Card>
          <Card
            style={{
              shadowOpacity: 0,
              elevation: 0,
            }}>
            <TextInput
              value={
                this.state.profile.gender === 'FEMALE'
                  ? this.state.profile.name
                  : this.state.profile.partnerName
              }
              title={this.state.profile.gender === 'FEMALE' ? 'Name*' : 'Name'}
              placeholder={
                this.state.profile.gender === 'FEMALE'
                  ? 'Your Name'
                  : 'Your Partner Name'
              }
              onChangeText={e => {
                const label =
                  this.state.profile.gender === 'FEMALE'
                    ? 'name'
                    : 'partnerName';
                this.onChangeText(label, e);
              }}
            />
            <GenderPicker
              title={
                this.state.profile.gender === 'FEMALE' ? 'Gender*' : 'Gender'
              }
              items={FEMALE}
              placeholder={FEMALE[0]}
              disabled
              useNativeAndroid={true}
              onChange={e => {
                const label =
                  this.state.profile.gender === 'FEMALE'
                    ? 'gender'
                    : 'partnerGender';
                this.onChangeText(label, e);
              }}
            />
            <TextInput
              value={
                this.state.profile.gender === 'FEMALE'
                  ? this.state.profile.email
                  : this.state.profile.partnerEmail
              }
              title={
                this.state.profile.gender === 'FEMALE' ? 'Email*' : 'Email'
              }
              placeholder={
                this.state.profile.gender === 'FEMALE'
                  ? 'Your Email'
                  : 'Your Partner Email'
              }
              onChangeText={e => {
                const label =
                  this.state.profile.gender === 'FEMALE'
                    ? 'email'
                    : 'partnerEmail';
                const normalize = e.toLowerCase();
                this.onChangeText(label, normalize);
              }}
            />
            <TextInputPhonePicker
              title={this.state.profile.gender === 'FEMALE' ? 'Phone Number*' : 'Phone Number'}
              value={
                this.state.profile.gender === 'FEMALE'
                  ? this.state.profile.phone
                  : this.state.profile.partnerPhone
              }
              placeholder={
                this.state.profile.gender === 'FEMALE'
                  ? 'Your Phone Number'
                  : 'Your Partner Phone'
              }
              countryCode={
                this.state.profile.gender === 'FEMALE'
                  ? this.state.profile.countryCode
                  : this.state.profile.partnerCountryCode
              }
              modalVisibility={this.state.openModalCountryCode}
              openModal={() => {
                const label =
                  this.state.profile.gender === 'FEMALE'
                    ? 'openModalCountryCode'
                    : 'openModalCountryCodePartner';
                this.setState(prevState => ({
                  ...prevState,
                  [label]: true,
                }));
              }}
              onChange={e => {
                const label =
                  this.state.profile.gender === 'FEMALE'
                    ? 'phone'
                    : 'partnerPhone';
                this.onChangeText(label, e);
              }}
            />
          </Card>

          {/* GROOMS */}
          <Card
            transparent
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}>
            <CardItem
              style={{
                paddingLeft: 25,
                paddingRight: 25,
                backgroundColor: transparent,
              }}>
              <Text>
                Groom
                {this.state.profile.gender === 'MALE' ? (
                  '*'
                ) : (
                  <Text> (Optional)</Text>
                )}
              </Text>
            </CardItem>
          </Card>
          <Card
            style={{
              shadowOpacity: 0,
              elevation: 0,
            }}>
            <TextInput
              value={this.state.profile.name}
              title={this.state.profile.gender === 'MALE' ? 'Name*' : 'Name'}
              placeholder={
                this.state.profile.gender === 'MALE'
                  ? 'Your Name'
                  : 'Your Partner Name'
              }
              onChangeText={e => {
                const label =
                  this.state.profile.gender === 'MALE' ? 'name' : 'partnerName';
                this.onChangeText(label, e);
              }}
            />
            <GenderPicker
              title={this.state.profile.gender === 'MALE' ? 'Gender*' : 'Gender'}
              items={MALE}
              placeholder={MALE[0]}
              disabled
              useNativeAndroid={true}
              onChange={e => {
                const label =
                  this.state.profile.gender === 'MALE'
                    ? 'gender'
                    : 'partnerGender';
                this.onChangeText(label, e);
              }}
            />
            <TextInput
              value={
                this.state.profile.gender === 'MALE'
                  ? this.state.profile.email
                  : this.state.profile.partnerEmail
              }
              title={this.state.profile.gender === 'MALE' ? 'Email*' : 'Email'}
              placeholder={
                this.state.profile.gender === 'MALE'
                  ? 'Your Email'
                  : 'Your Partner Email'
              }
              onChangeText={e => {
                const label =
                  this.state.profile.gender === 'MALE'
                    ? 'email'
                    : 'partnerEmail';
                const normalize = e.toLowerCase();
                this.onChangeText(label, normalize);
              }}
            />
            <TextInputPhonePicker
              title={this.state.profile.gender === 'MALE' ? 'Phone Number*' : 'Phone Number'}
              placeholder={
                this.state.profile.gender === 'MALE'
                  ? 'Your Phone Number'
                  : 'Your Partner Phone'
              }
              countryCode={
                this.state.profile.gender === 'MALE'
                  ? this.state.profile.countryCode
                  : this.state.profile.partnerCountryCode
              }
              value={
                this.state.profile.gender === 'MALE'
                  ? this.state.profile.phone
                  : this.state.profile.partnerPhone
              }
              modalVisibility={this.state.openModalCountryCode}
              openModal={() => {
                const label =
                  this.state.profile.gender === 'MALE'
                    ? 'openModalCountryCode'
                    : 'openModalCountryCodePartner';
                this.setState(prevState => ({
                  ...prevState,
                  [label]: true,
                }));
              }}
              onChange={e => {
                const label =
                  this.state.profile.gender === 'MALE'
                    ? 'phone'
                    : 'partnerPhone';
                this.onChangeText(label, e);
              }}
            />
          </Card>

          {/* IMPORTANT DATES */}
          <Card
            transparent
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}>
            <CardItem
              style={{
                paddingLeft: 25,
                paddingRight: 25,
                backgroundColor: transparent,
              }}>
              <Text>Important Dates</Text>
            </CardItem>
          </Card>
          <Card
            style={{
              shadowOpacity: 0,
              elevation: 0,
            }}>
            <DatePicker
              placeholder="Select Your Wedding Date"
              openStatus={this.state.openWeddingDate}
              openPicker={() => this.openDatePicker('weddingDate')}
              title="Wedding Date*"
              value={this.state.profile.marriageDate}
              done={e => {
                this.setData('weddingDate', e);
              }}
              cancel={() => this.openDatePicker('weddingDate')}
            />
            <DatePicker
              clearDate={() => this.clearDate('marriageDate2')}
              placeholder="Select Optional Wedding Date"
              openStatus={this.state.optionalWeddingDate}
              openPicker={() => this.openDatePicker('optionalWeddingDate')}
              title="Optional Wedding Date"
              value={this.state.profile.marriageDate2}
              done={e => {
                this.setData('optionalWeddingDate', e);
              }}
              cancel={() => this.openDatePicker('optionalWeddingDate')}
            />
            <DatePicker
              clearDate={() => this.clearDate('keyCollectionDate')}
              placeholder="Select Your Key Collection Date"
              openStatus={this.state.keyCollectionDate}
              openPicker={() => this.openDatePicker('keyCollectionDate')}
              title="Key Collection"
              value={this.state.profile.keyCollectionDate}
              done={e => {
                this.setData('keyCollectionDate', e);
              }}
              cancel={() => this.openDatePicker('keyCollectionDate')}
            />
          </Card>
          <Card transparent>
            <CardItem
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: transparent,
              }}>
              <Button
                onPress={this.logout}
                transparent
                style={{
                  width: '50%',
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: regular,
                    color: lightSalmon,
                    lineHeight: 15,
                  }}>
                  Logout
                </Text>
              </Button>
            </CardItem>
          </Card>
        </Content>
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={'Discard all changes?'}
          options={['Discard', 'cancel']}
          cancelButtonIndex={1}
          destructiveButtonIndex={0}
          onPress={async index => {
            /* do something */
            console.log('Action Sheet Index: ', index);
            if (index === 0) {
              const {navigation} = this.props;
              await navigation.dangerouslyGetParent().setOptions({
                tabBarVisible: false,
              });
              navigation.pop();
            } else {
            }
          }}
        />
        {this.state.openModalCountryCode ? (
          <ModalCountryCode
            modalVisibility={this.state.openModalCountryCode}
            closeModal={e => {
              console.log('EA: ', e);
              const label =
                this.state.profile.gender === 'MALE'
                  ? 'countryCode'
                  : 'partnerCountryCode';
              if (e === undefined) {
                this.setState(prevState => ({
                  ...prevState,
                  openModalCountryCode: !prevState.openModalCountryCode,
                }));
              } else {
                this.setState(
                  prevState => ({
                    ...prevState,
                    profile: {
                      ...prevState.profile,
                      [label]: e.dial_code,
                    },
                  }),
                  () => {
                    this.setState(prevState => ({
                      ...prevState,
                      openModalCountryCode: !prevState.openModalCountryCode,
                    }));
                  },
                );
              }
            }}
          />
        ) : null}
        {this.state.openModalCountryCodePartner ? (
          <ModalCountryCodePartner
            modalVisibility={this.state.openModalCountryCodePartner}
            closeModal={e => {
              const label =
                this.state.profile.gender === 'FEMALE'
                  ? 'countryCode'
                  : 'partnerCountryCode';
              if (e === undefined) {
                this.setState(prevState => ({
                  ...prevState,
                  openModalCountryCodePartner:
                    !prevState.openModalCountryCodePartner,
                }));
              } else {
                this.setState(
                  prevState => ({
                    ...prevState,
                    profile: {
                      ...prevState.profile,
                      [label]: e.dial_code,
                    },
                  }),
                  () => {
                    this.setState(prevState => ({
                      ...prevState,
                      openModalCountryCodePartner:
                        !prevState.openModalCountryCodePartner,
                    }));
                  },
                );
              }
            }}
          />
        ) : null}
      </Container>
    );
  }
}

const Wrapper = compose(withApollo)(EditProfile);

export default props => <Wrapper {...props} />;
