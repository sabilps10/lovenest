import React, {useCallback, useEffect} from 'react';
import {Text, View, FlatList, Animated} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Card, CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonBack from '../../../components/Button/buttonBack';
import {StackActions} from '@react-navigation/native';
import {connect} from 'react-redux';

const {headerBorderBottom, greyLine, black} = Colors;
const {medium} = FontType;
const listPdf = [
  {
    id: 1,
    name: 'Actual Day Gown Appointment Guide',
    file: `https://storage.googleapis.com/love-nest-233803.appspot.com/fa24458d6f9c60d025538342d0d2e167_files.pdf`,
  },
  {
    id: 2,
    name: 'Pre Wedding Appointment Guide',
    file: `https://storage.googleapis.com/love-nest-233803.appspot.com/a94027e0dc60ceb6824015943598b38b_files.pdf`,
  },
];

const AppointmentGuide = props => {
  const {navigation, positionYBottomNav} = props;

  useEffect(() => {
    navigationOptions();
    onChangeOpacity();
  }, []);
  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const popStacking = async () => {
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: true,
    });
    navigation.dispatch(StackActions.pop());
  };

  const navigationOptions = () => {
    navigation.setOptions({
      headerTitle: 'Appointment Guide',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        fontSize: RFPercentage(1.8),
        color: black,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={popStacking} />;
      },
    });
  };

  const keyExt = useCallback(item => {
    return `${item?.id}`;
  }, []);

  const renderItem = useCallback(({item, index}) => {
    return <CardItemPdf {...props} item={item} index={index} />;
  }, []);

  return (
    <View style={{flex: 1, paddingTop: 15}}>
      <Card
        style={{marginLeft: 0, marginRight: 0, elevation: 0, shadowOpacity: 0}}>
        <FlatList
          data={listPdf}
          extraData={listPdf}
          keyExtractor={keyExt}
          renderItem={renderItem}
        />
      </Card>
    </View>
  );
};

export const CardItemPdf = props => {
  const {navigation, item, index} = props;

  const goToPdfPreview = () => {
    try {
      navigation.navigate('AppointmentGuidePdfPreview', {
        id: item.id,
        name: item?.name,
        data: item?.file,
      });
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <CardItem
      button
      onPress={() => {
        goToPdfPreview();
      }}
      style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
            }}>
            {item?.name}
          </Text>
          <Icon
            type="Feather"
            name="chevron-right"
            style={{color: greyLine, fontSize: RFPercentage(3)}}
          />
        </View>
        {index === listPdf?.length - 1 ? null : (
          <View
            style={{
              marginTop: 15,
              top: 5,
              height: 1,
              width: '100%',
              borderBottomColor: headerBorderBottom,
              borderBottomWidth: 1,
            }}
          />
        )}
      </View>
    </CardItem>
  );
};

const mapToState = state => {
  const {review} = state;
  const {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  } = review;
  return {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  };
};

const mapToDispatch = () => {
  return {};
};

const connected = connect(mapToState, mapToDispatch)(AppointmentGuide);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
