import React, {useEffect} from 'react';
import {
  Text,
  StatusBar,
  Dimensions,
  Image,
  View,
  StyleSheet,
  Animated
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Pdf from 'react-native-pdf';
import ButtonBack from '../../../components/Button/buttonBack';
import {CommonActions, StackActions} from '@react-navigation/native';
import ADGownPdf from '../../../static/pdf/adgown.pdf';
import PreWeddingPdf from '../../../static/pdf/prewedding.pdf';
import {connect} from 'react-redux';

const {white, headerBorderBottom, greyLine, black} = Colors;
const {medium, book} = FontType;

const {width, height} = Dimensions.get('screen')

const Components = props => {
  console.log('PDF PRevew Props: ', props);
  const {navigation, positionYBottomNav} = props;

  useEffect(() => {
    navigationOptions(props?.route?.params?.name);
    onChangeOpacity();
  }, []);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const popStacking = async () => {
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: true,
    });
    navigation.dispatch(StackActions.pop());
  };

  const navigationOptions = name => {
    navigation.setOptions({
      headerTitle: name,
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        fontSize: RFPercentage(1.8),
        color: black,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={popStacking} />;
      },
    });
  };
  return (
    <View style={{flex: 1}}>
      <Pdf
        scale={3.0}
        source={{uri: props?.route?.params?.data}}
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`number of pages: ${numberOfPages}`);
        }}
        onPageChanged={(page, numberOfPages) => {
          console.log(`current page: ${page}`);
        }}
        onError={error => {
          console.log(error);
        }}
        onPressLink={uri => {
          console.log(`Link presse: ${uri}`);
        }}
        style={{flex: 1, width, height}}
      />
    </View>
  );
};

const mapToState = state => {
  const {review} = state;
  const {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  } = review;
  return {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  };
};

const mapToDispatch = () => {
  return {};
};

const connected = connect(mapToState, mapToDispatch)(Components);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
