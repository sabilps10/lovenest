import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList, Animated} from 'react-native';
import {Container, Content, Card, CardItem} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import TCData from '../../../utils/TC_data/TermAndConditionData';
import ButtonBack from '../../../components/Button/buttonBack';
import {StackActions} from '@react-navigation/native';
import {RFPercentage} from 'react-native-responsive-fontsize'

const {black, newContainerColor: offWhite} = Colors;
const {small, regular} = FontSize;
const {book, medium} = FontType;

const styles = StyleSheet.create({
  card: {
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  title: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.39,
  },
  text: {
    fontFamily: book,
    fontSize: small,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
});

class TermCondition extends Component {
  constructor(props) {
    super(props);
  }

  popStacking = async () => {
    const {navigation} = this.props;
    const parent = await navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: true,
    });
    navigation.dispatch(StackActions.pop());
  };

  navigationOptions = props => {
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'Terms & Condition',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: RFPercentage(1.9),
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={this.popStacking} />;
      },
    });
  };

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentDidMount() {
    const {props} = this;
    const {navigation} = props;
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(false);
    }
    this._unsubscribe = navigation.addListener('focus', () => {
      this.navigationOptions(props);
      console.log('Terms and Conditions ADD LISTENER DID MOUNT');
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(false);
      }
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  subDataList = (value, i) => {
    return (
      <React.Fragment key={`${i}theParent`}>
        {!value ? null : (
          <FlatList
            data={value}
            keyExtractor={(item, index) => `${i + index}subData`}
            renderItem={({item, index}) => {
              const numberingSubData = (index += 1);
              return (
                <CardItem style={{flexDirection: 'row'}}>
                  <View style={{flexBasis: '8%', height: '100%'}}>
                    <Text style={styles.text}>{numberingSubData}.</Text>
                  </View>
                  <View style={{flexBasis: '92%'}}>
                    <Text style={styles.text}>{item.sub}</Text>
                  </View>
                </CardItem>
              );
            }}
          />
        )}
      </React.Fragment>
    );
  };

  dataList = (value, i) => {
    return (
      <FlatList
        data={value}
        keyExtractor={(item, index) => String(`${index}Data`)}
        renderItem={({item, index}) => {
          const numbering = (index += 1);
          const subData = item.subData;
          return (
            <CardItem style={{flexDirection: 'row'}}>
              <View style={{flexBasis: '8%', height: '100%'}}>
                <Text style={styles.text}>{`${numbering}`}.</Text>
              </View>
              <View style={{flexBasis: '92%', flexDirection: 'column'}}>
                {item.nextPage ? (
                  <Text style={styles.text}>
                    {item.data}
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate('PrivacyAndPolicy')
                      }
                      style={{...styles.text, fontWeight: 'bold'}}>
                      {' please click here.'}
                    </Text>
                  </Text>
                ) : (
                  <Text style={styles.text}>{item.data}</Text>
                )}
                {this.subDataList(subData, index)}
              </View>
            </CardItem>
          );
        }}
      />
    );
  };

  render() {
    console.log('Props Term Component: ', this.props);
    return (
      <Container style={{backgroundColor: 'white'}}>
        <Content contentContainerStyle={{paddingTop: 20}}>
          <Card transparent style={styles.card}>
            <FlatList
              data={TCData}
              keyExtractor={(item, index) => String(`${index}header`)}
              renderItem={({item, index}) => {
                const itemDatas = item.data;
                return (
                  <View style={{flexDirection: 'column'}}>
                    <CardItem>
                      <Text style={styles.title}>{item.title}</Text>
                    </CardItem>
                    {this.dataList(itemDatas, index)}
                  </View>
                );
              }}
            />
          </Card>
        </Content>
      </Container>
    );
  }
}

const Wrapper = compose(withApollo)(TermCondition);

export default props => {
  return <Wrapper {...props} />;
};
