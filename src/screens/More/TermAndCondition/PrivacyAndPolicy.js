import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {Container, Content, Card, CardItem} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import PPData from '../../../utils/TC_data/PrivacyAndPolicy';
import ButtonBack from '../../../components/Button/buttonBack';
import {StackActions} from '@react-navigation/native';

const {black, newContainerColor: offWhite} = Colors;
const {small, regular} = FontSize;
const {book, medium} = FontType;

const styles = StyleSheet.create({
  card: {
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  title: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    lineHeight: 25,
    letterSpacing: 0.39,
  },
  text: {
    fontFamily: book,
    fontSize: small,
    color: black,
    lineHeight: 18,
    letterSpacing: 0.3,
  },
});

class PrivacyAndPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  popStacking = async () => {
    const {navigation} = this.props;
    await navigation.dangerouslyGetParent().setOptions({
      tabBarVisible: true,
    });
    navigation.dispatch(StackActions.pop());
  };

  navigationOptions = props => {
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'Privacy & Polcicy',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={this.popStacking} />;
      },
    });
  };

  componentDidMount() {
    const {props} = this;
    const {navigation} = props;

    this._unsubscribe = navigation.addListener('focus', () => {
      this.navigationOptions(props);
      console.log('Privacy and Policy ADD LISTENER DID MOUNT');
    });
  }

  subDataList = (value, i) => {
    return (
      <React.Fragment key={`${i}theParent`}>
        {!value ? null : (
          <FlatList
            data={value}
            keyExtractor={(item, index) => `${i + index}subData`}
            renderItem={({item, index}) => {
              const numberingSubData = (index += 1);
              return (
                <CardItem style={{flexDirection: 'row'}}>
                  <View style={{flexBasis: '8%', height: '100%'}}>
                    <Text style={styles.text}>{numberingSubData}.</Text>
                  </View>
                  <View style={{flexBasis: '92%'}}>
                    <Text style={styles.text}>{item.sub}</Text>
                  </View>
                </CardItem>
              );
            }}
          />
        )}
      </React.Fragment>
    );
  };

  dataList = (value, i) => {
    return (
      <FlatList
        data={value}
        keyExtractor={(item, index) => String(`${index}Data`)}
        renderItem={({item, index}) => {
          const numbering = (index += 1);
          const subData = item.subData;
          return (
            <CardItem style={{flexDirection: 'row'}}>
              <View style={{flexBasis: '8%', height: '100%'}}>
                <Text style={styles.text}>{`${numbering}`}.</Text>
              </View>
              <View style={{flexBasis: '92%', flexDirection: 'column'}}>
                {item.nextPage ? (
                  <Text style={styles.text}>
                    {item.data}
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate('PrivacyAndPolicy')
                      }
                      style={{...styles.text, fontWeight: 'bold'}}>
                      {' please click here.'}
                    </Text>
                  </Text>
                ) : (
                  <Text style={styles.text}>{item.data}</Text>
                )}
                {this.subDataList(subData, index)}
              </View>
            </CardItem>
          );
        }}
      />
    );
  };

  render() {
    return (
      <Container style={{backgroundColor: offWhite}}>
        <Content contentContainerStyle={{paddingTop: 20}}>
          <Card style={styles.card}>
            <FlatList
              data={PPData}
              keyExtractor={(item, index) => String(`${index}header`)}
              renderItem={({item, index}) => {
                const itemDatas = item.data;
                return (
                  <View style={{flexDirection: 'column'}}>
                    <CardItem>
                      <Text style={styles.title}>{item.title}</Text>
                    </CardItem>
                    {this.dataList(itemDatas, index)}
                  </View>
                );
              }}
            />
          </Card>
        </Content>
      </Container>
    );
  }
}

const Wrapper = compose(withApollo)(PrivacyAndPolicy);

export default props => {
  return <Wrapper {...props} />;
};
