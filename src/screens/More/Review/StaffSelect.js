import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Header,
  Left,
  Body,
  Right,
  Icon,
  Container,
  Content,
  Card,
  CardItem,
  Footer,
  FooterTab,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {connect} from 'react-redux';

// redux
import ReduxStaffSelect from '../../../redux/thunk/ReviewSelectedStaffThunk';

const {white, black, greyLine, mainGreen} = Colors;
const {regular, small} = FontSize;
const {book, medium} = FontType;

// Query
import QUERY_GET_STAFF_REVIEW from '../../../graphql/queries/getAllStaffMobile';
import reviewSelectStaffThunk from '../../../redux/thunk/ReviewSelectedStaffThunk';

export const HeaderReview = props => {
  const {onPress} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        shadowRadius: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.6,
        borderBottomColor: 'rgb(231,231,231)',
        flexDirection: 'row',
        backgroundColor: 'white',
      }}>
      <Left style={{flex: 0.2}}>
        <TouchableOpacity onPress={() => onPress()}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: 25, color: black}}
          />
        </TouchableOpacity>
      </Left>
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
          Select Staff
        </Text>
      </Body>
      <Right style={{flex: 0.2, height: '100%'}} />
    </Header>
  );
};

const StaffSelect = props => {
  console.log('MerchantSelect Props: ', props);
  const {navigation, client, merchantId, reviewStaff, selectedStaff} = props;

  const [staffList, setStaffList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    validateFetch();
  }, [selectedStaff, merchantId]);

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const validateFetch = async () => {
    if (selectedStaff?.length === 0) {
      fetch();
    } else {
      await setStaffList([...selectedStaff]);
      await setIsError(false);
      await setIsLoading(false);
    }
  };

  const fetch = async () => {
    try {
      if (merchantId) {
        await client
          .query({
            query: QUERY_GET_STAFF_REVIEW,
            variables: {
              merchantId: parseInt(merchantId, 10),
            },
            fetchPolicy: 'no-cache',
            ssr: false,
          })
          .then(async response => {
            console.log('Response: ', response);
            const {data, errors} = response;
            const {getAllStaffMobile} = data;
            const {data: listStaff, error} = getAllStaffMobile;

            if (errors) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              if (error) {
                await setIsError(true);
                await setIsLoading(false);
              } else {
                const manipulatedStaff = await Promise.all(
                  listStaff.map((d, i) => {
                    return {
                      ...d,
                      selected: false,
                    };
                  }),
                );

                if (manipulatedStaff) {
                  await setStaffList([...manipulatedStaff]);
                  await setIsError(false);
                  await setIsLoading(false);
                } else {
                  await setIsError(true);
                  await setIsLoading(false);
                }
              }
            }
          })
          .catch(error => {
            console.log('Error: ', error);
          });
      } else {
        await setIsError(true);
        await setIsLoading(false);
      }
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>No Staff related to merchant you selected</Text>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <Content>
          <Card
            style={{
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 25,
              elevation: 0,
              shadowOpacity: 0,
            }}>
            <CardItem
              style={{
                width: '100%',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: regular,
                  color: black,
                  letterSpacing: 0.3,
                }}>
                Select Staff
              </Text>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: small,
                  color: greyLine,
                  letterSpacing: 0.3,
                }}>
                You may select more than one
              </Text>
            </CardItem>
            <FlatList
              contentContainerStyle={{paddingBottom: 25}}
              data={staffList}
              extraData={staffList}
              listKey={(item, index) => `${String(index)} Parent`}
              renderItem={({item, index}) => {
                return (
                  <CardItem
                    button
                    onPress={async () => {
                      const oldListStaff = staffList;
                      oldListStaff[index].selected = !oldListStaff[index]
                        .selected;
                      await setStaffList([...oldListStaff]);
                    }}
                    style={{width: '100%'}}>
                    <View
                      style={{
                        flex: 0.1,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        minHeight: 30,
                      }}>
                      {item.selected ? (
                        <View
                          style={{
                            width: 20,
                            height: 20,
                            borderWidth: 1,
                            borderRadius: 3,
                            borderColor: black,
                            backgroundColor: black,
                          }}>
                          <Icon
                            type="Feather"
                            name="check"
                            style={{fontSize: 15, color: white}}
                          />
                        </View>
                      ) : (
                        <View
                          style={{
                            minWidth: 20,
                            maxWidth: 20,
                            minHeight: 20,
                            maxHeight: 20,
                            borderWidth: 1,
                            borderRadius: 3,
                            borderColor: black,
                            backgroundColor: white,
                          }}
                        />
                      )}
                    </View>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        minHeight: 30,
                      }}>
                      <Text
                        style={{
                          top: 2,
                          fontFamily: book,
                          fontSize: regular,
                          color: black,
                          letterSpacing: 0.3,
                        }}>
                        {item.name}
                      </Text>
                    </View>
                  </CardItem>
                );
              }}
            />
          </Card>
        </Content>
        <Footer>
          <FooterTab style={{backgroundColor: mainGreen, width: '100%'}}>
            <View style={{width: '100%', backgroundColor: mainGreen}}>
              <TouchableOpacity
                onPress={async () => {
                  await reviewStaff(staffList);
                  navigation.goBack(null);
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    color: white,
                    fontSize: regular,
                    letterSpacing: 0.3,
                  }}>
                  SUBMIT
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('Staff Review State: ', state);
  const {review} = state;
  const {selectedMerchant, selectedStaff} = review;
  return {
    merchantId: selectedMerchant,
    selectedStaff,
  };
};

const mapToDispatch = dispatch => {
  return {
    reviewStaff: listStaff => dispatch(ReduxStaffSelect(listStaff)),
  };
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(StaffSelect);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
