import React from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Header,
  Left,
  Body,
  Right,
  Icon,
} from 'native-base';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import ImageViewerSlider from '../../../components/Image/ImageViewer';
import ImagePreview from './Components/ImagePreview';

const {book, medium} = FontType;
const {regular, small} = FontSize;
const {
  black,
  white,
  greyLine: darkerGreyCalendar,
  lightSalmon,
  containerColor,
} = Colors;

const defaultImage =
  'https://erekrut.panaceaebizz.com/wp-content/uploads/2020/05/blank-profile-picture-973460_1280.png';

class ReviewAndRatingDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      images: [],
      prevImages: [],
      isLoading: true,
      openImageViewer: false,
    };
  }

  async componentDidMount() {
    const {route, navigation} = this.props;
    const {params} = route;
    const {items} = params;

    StatusBar.setBackgroundColor('white');
    StatusBar.setBarStyle('dark-content');

    this.subscriber = navigation.addListener('focus', () => {
      StatusBar.setBackgroundColor('white');
      StatusBar.setBarStyle('dark-content');
    });

    if (items) {
      const {reviewGallery} = items;
      const images = !reviewGallery
        ? []
        : await reviewGallery.map(d => {
            return d.url;
          });
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState(prevState => ({
        ...prevState,
        items: {...items},
        images,
        prevImages: [...reviewGallery],
        isLoading: false,
      }));
    }
  }

  componentDidUpdate(prevState) {
    if (this.state.openImageViewer) {
      StatusBar.setBackgroundColor('black');
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor('white');
      StatusBar.setBarStyle('dark-content');
    }
  }

  componentWillUnmount() {
    this.subscriber();
  }

  render() {
    console.log('Props Review Detail: ', this.props);
    const {items, isLoading} = this.state;
    console.log('State Review Detail: ', this.state);

    if (isLoading) {
      return (
        <Container style={{backgroundColor: 'white'}}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator />
          </View>
        </Container>
      );
    }

    const {
      id,
      customer,
      createdOn,
      serviceRating,
      productRating,
      testimonial,
      reviewGallery,
    } = items;
    const {name, profileImage} = customer;
    const serviceRatingLeft = serviceRating === null ? null : 5 - serviceRating;
    const productRatingLeft = productRating === null ? null : 5 - productRating;

    return (
      <Container style={{backgroundColor: 'white'}}>
        <Header
          translucent={false}
          androidStatusBarColor={'white'}
          iosBarStyle="dark-content"
          style={{
            backgroundColor: 'white',
            elevation: 0,
            shadowOpacity: 0,
            borderBottomColor: darkerGreyCalendar,
            borderBottomWidth: 0.5,
          }}>
          <Left style={{flex: 0.1}}>
            <Icon
              onPress={() => {
                this.props.navigation.goBack(null);
              }}
              type="Feather"
              name="chevron-left"
              style={{fontSize: 25, color: black}}
            />
          </Left>
          <Body
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: regular,
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Review Detail
            </Text>
          </Body>
          <Right style={{flex: 0.1}} />
        </Header>
        <ImagePreview
          onPress={() => {
            this.setState(prevState => ({
              ...prevState,
              openImageViewer: !prevState.openImageViewer,
            }));
          }}
          images={this.state.prevImages}
          visible={this.state.openImageViewer}
        />
        <Content>
          {/* <ImageViewerSlider
            imageData={this.state.images}
            openStatus={this.state.openImageViewer}
            closeImageViewer={() => {
              this.setState(prevState => ({
                ...prevState,
                openImageViewer: false,
              }));
            }}
          /> */}
          <Card
            style={{
              elevation: 0,
              shadowOpacity: 0,
              borderTopWidth: 0,
              borderRightWidth: 0,
              borderLeftWidth: 0,
              borderBottomWidth: 0.5,
              borderTopColor: 'transparent',
              borderRightColor: 'transparent',
              borderLeftColor: 'transparent',
            }}>
            <CardItem style={{width: '100%'}}>
              <Image
                source={{
                  uri:
                    !profileImage || profileImage === ''
                      ? defaultImage
                      : profileImage,
                }}
                style={{width: 40, height: 40, borderRadius: 40 / 2}}
                resizeMode="cover"
              />
              <View style={{flex: 1, flexDirection: 'column', paddingLeft: 10}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: regular,
                    color: black,
                    letterSpacing: 0.3,
                  }}>{`${name.substr(0, 5)}***`}</Text>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: small,
                    color: darkerGreyCalendar,
                    letterSpacing: 0.3,
                  }}>
                  {moment.utc(createdOn).format('MMMM YYYY')}
                </Text>
              </View>
            </CardItem>
            <CardItem>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: regular,
                  color: black,
                  letterSpacing: 0.3,
                  lineHeight: 18,
                }}>
                {testimonial}
              </Text>
            </CardItem>
            {/* {testimonial.length > 220 ? (
        <CardItem style={{ marginTop: 0, paddingTop: 0 }}>
          <Text>Read More</Text>
        </CardItem>
      ) : null} */}
            {(serviceRating === null && productRating === null) ||
            (serviceRating === 0 && productRating === 0) ? null : (
              <CardItem>
                {serviceRating === null || serviceRating === 0 ? null : (
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{marginBottom: 10}}>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: regular,
                          color: darkerGreyCalendar,
                          letterSpacing: 0.3,
                        }}>
                        Service
                      </Text>
                    </View>
                    <View
                      style={{
                        flexBasis: '40%',
                        flexDirection: 'row',
                        paddingRight: 20,
                      }}>
                      {new Array(serviceRating).fill(null).map((_, i) => {
                        return (
                          <View style={{width: 20, height: 20, left: -5}}>
                            <Icon
                              type="MaterialCommunityIcons"
                              name="star"
                              style={{
                                fontSize: 20,
                                color: '#FFC95A',
                                marginLeft: 0,
                                marginRight: 0,
                                paddingLeft: 0,
                                paddingRight: 0,
                              }}
                            />
                          </View>
                        );
                      })}
                      {serviceRatingLeft === 0 || serviceRatingLeft === null
                        ? null
                        : new Array(serviceRatingLeft)
                            .fill(null)
                            .map((_, i) => {
                              return (
                                <View style={{width: 20, height: 20, left: -5}}>
                                  <Icon
                                    type="MaterialCommunityIcons"
                                    name="star"
                                    style={{
                                      fontSize: 20,
                                      color: '#D5D5D5',
                                      marginLeft: 0,
                                      marginRight: 0,
                                      paddingLeft: 0,
                                      paddingRight: 0,
                                    }}
                                  />
                                </View>
                              );
                            })}
                    </View>
                  </View>
                )}
                {productRating === null || productRating === 0 ? null : (
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{marginBottom: 10}}>
                      <Text
                        style={{
                          fontFamily: book,
                          fontSize: regular,
                          color: darkerGreyCalendar,
                          letterSpacing: 0.3,
                        }}>
                        Product
                      </Text>
                    </View>
                    <View
                      style={{
                        flexBasis: '40%',
                        flexDirection: 'row',
                        paddingRight: 20,
                      }}>
                      {new Array(productRating).fill(null).map((_, i) => {
                        return (
                          <View style={{width: 20, height: 20, left: -5}}>
                            <Icon
                              type="MaterialCommunityIcons"
                              name="star"
                              style={{
                                fontSize: 20,
                                color: '#FFC95A',
                                marginLeft: 0,
                                marginRight: 0,
                                paddingLeft: 0,
                                paddingRight: 0,
                              }}
                            />
                          </View>
                        );
                      })}
                      {productRatingLeft === 0 || productRatingLeft === null
                        ? null
                        : new Array(productRatingLeft)
                            .fill(null)
                            .map((_, i) => {
                              return (
                                <View style={{width: 20, height: 20, left: -5}}>
                                  <Icon
                                    type="MaterialCommunityIcons"
                                    name="star"
                                    style={{
                                      fontSize: 20,
                                      color: '#D5D5D5',
                                      marginLeft: 0,
                                      marginRight: 0,
                                      paddingLeft: 0,
                                      paddingRight: 0,
                                    }}
                                  />
                                </View>
                              );
                            })}
                    </View>
                  </View>
                )}
              </CardItem>
            )}
            <CardItem style={{width: '100%'}}>
              <FlatList
                contentContainerStyle={
                  {
                    // borderWidth: 1,
                    // justifyContent: 'space-between',
                  }
                }
                scrollEnabled={false}
                legacyImplementation
                disableVirtualization
                data={reviewGallery}
                extraData={reviewGallery}
                numColumns={3}
                columnWrapperStyle={{
                  width: '100%',
                  // justifyContent: 'space-between',
                  marginBottom: 10,
                }}
                keyExtractor={(item, index) => String(item.id)}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => {
                        this.setState(prevState => ({
                          ...prevState,
                          openImageViewer: true,
                        }));
                      }}
                      style={{
                        borderRadius: 4,
                        width: 100,
                        height: 100,
                        marginRight: 10,
                      }}>
                      <Image
                        source={{uri: item.url}}
                        style={{
                          borderRadius: 4,
                          width: 100,
                          height: 100,
                          zIndex: 1,
                          position: 'absolute',
                          top: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          backgroundColor: darkerGreyCalendar,
                        }}
                      />
                    </TouchableOpacity>
                  );
                }}
              />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const Wrapper = compose(withApollo)(ReviewAndRatingDetail);

export default props => <Wrapper {...props} />;
