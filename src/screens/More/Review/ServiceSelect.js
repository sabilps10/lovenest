import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Header,
  Left,
  Body,
  Right,
  Icon,
  Container,
  Content,
  Button,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {connect} from 'react-redux';

//  Query
import GET_SERVICETYPES from '../../../graphql/queries/reviewGetServiceType';

// Redux
import ReviewServiceThunk from '../../../redux/thunk/ReviewSelectServiceThunk';
import ReviewMerchantThunk from '../../../redux/thunk/ReviewSelectedMerchantThunk';
import ReviewStaffThunk from '../../../redux/thunk/ReviewSelectedStaffThunk';

const {white, black, greyLine, disableCalendar, mainGreen} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;
const {width, height} = Dimensions.get('window');
const dummyListService = [
  {
    id: 1,
    serviceName: 'Bridal',
  },
  {
    id: 2,
    serviceName: 'Jewellery',
  },
  {
    id: 3,
    serviceName: 'Venue',
  },
  {
    id: 4,
    serviceName: 'Interior Design',
  },
  {
    id: 5,
    serviceName: 'Smart Home',
  },
];

export const HeaderReview = props => {
  const {onPress} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        shadowRadius: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.6,
        borderBottomColor: 'rgb(231,231,231)',
        flexDirection: 'row',
        backgroundColor: 'white',
      }}>
      <Left style={{flex: 0.2}}>
        <TouchableOpacity onPress={() => onPress()}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: 25, color: black}}
          />
        </TouchableOpacity>
      </Left>
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
          Select Sevices
        </Text>
      </Body>
      <Right style={{flex: 0.2, height: '100%'}} />
    </Header>
  );
};

const ServiceSelect = props => {
  console.log('ServiceSelect Props: ', props);
  const {
    client,
    navigation,
    reviewServiceSelected,
    reviewMerchantSelected,
    reviewStaffSelected,
  } = props;

  const [list, setList] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetchServiceType();
  }, [navigation, refreshing]);

  const fetchServiceType = async () => {
    try {
      await client
        .query({
          query: GET_SERVICETYPES,
          ssr: false,
          fetchPolicy: 'no-cache',
        })
        .then(async response => {
          console.log('fetchServiceType: ', response);
          const {data, errors} = response;
          const {serviceTypes} = data;
          const {data: arrayList, error} = serviceTypes;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
            } else {
              const finalList = await Promise.all(
                arrayList.map((d, i) => {
                  return {
                    id: i + 1,
                    serviceName: d,
                  };
                }),
              );

              if (finalList?.length === arrayList?.length) {
                await setList([...finalList]);
                await setIsError(false);
                await setIsLoading(false);
                await setRefreshing(false);
              }
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          throw error;
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
    }
  };

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <Content
          style={{flex: 1}}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => setRefreshing(true)}
            />
          }>
          <View
            style={{
              flex: 1,
              height: height / 1.5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>Failed to load data</Text>
          </View>
        </Content>
      </Container>
    );
  } else {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <FlatList
          contentContainerStyle={{
            paddingTop: 15,
            paddingBottom: 15,
            backgroundColor: 'white',
          }}
          data={list}
          extraData={list}
          legacyImplementation
          disableVirtualization
          keyExtractor={(item, _) => `${String(item.id)}`}
          renderItem={({item, _}) => {
            return (
              <TouchableOpacity
                onPress={async () => {
                  await reviewServiceSelected(item.serviceName);
                  await reviewMerchantSelected(null, '');
                  await reviewStaffSelected([]);
                  await goBack();
                }}
                style={{
                  backgroundColor: 'transparent',
                  width: '100%',
                  padding: 10,
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: 40,
                    borderBottomColor: greyLine,
                    borderBottomWidth: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text>{item.serviceName}</Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('Redux State: ', state);
  return {};
};

const mapToDispatch = dispatch => {
  return {
    reviewServiceSelected: service => dispatch(ReviewServiceThunk(service)),
    reviewMerchantSelected: (merchantId, merchantName) =>
      dispatch(ReviewMerchantThunk(merchantId, merchantName)),
    reviewStaffSelected: staffs => dispatch(ReviewStaffThunk(staffs)),
  };
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(ServiceSelect);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
