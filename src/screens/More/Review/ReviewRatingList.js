import React from 'react';
import {
  View,
  Text,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import moment from 'moment';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {charImage} from '../../../utils/Themes/Images';

// Query
import QueryReview from '../../../graphql/queries/getCustomerTestimonial';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {charNoOrder} = charImage;
const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  lightSalmon,
  greyLine: darkerGreyCalendar,
  containerColor,
} = Colors;
const {book, medium} = FontType;
const {small, regular} = FontSize;

const defaultImage = `https://erekrut.panaceaebizz.com/wp-content/uploads/2020/05/blank-profile-picture-973460_1280.png`;

export const ReviewCard = props => {
  const {items, navigation} = props;
  const {
    id,
    customer,
    createdOn,
    serviceRating,
    productRating,
    testimonial,
    reviewGallery,
  } = items;
  const {name, profileImage} = customer;
  const serviceRatingLeft = serviceRating === null ? null : 5 - serviceRating;
  const productRatingLeft = productRating === null ? null : 5 - productRating;
  const gallery =
    reviewGallery.length > 3 ? reviewGallery.slice(0, 3) : reviewGallery;
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('ReviewRatingAndDetail', {items});
      }}>
      <Card
        style={{
          elevation: 0,
          shadowOpacity: 0,
          borderTopWidth: 0,
          borderRightWidth: 0,
          borderLeftWidth: 0,
          borderBottomWidth: 0.5,
          borderTopColor: 'transparent',
          borderRightColor: 'transparent',
          borderLeftColor: 'transparent',
          paddingBottom: 20,
        }}>
        <CardItem style={{width: '100%'}}>
          <Image
            source={{
              uri:
                profileImage === null || profileImage == ''
                  ? defaultImage
                  : profileImage,
            }}
            style={{width: 40, height: 40, borderRadius: 40 / 2}}
            resizeMode="cover"
          />
          <View style={{flex: 1, flexDirection: 'column', paddingLeft: 10}}>
            <Text
              style={{
                fontFamily: book,
                fontSize: regular,
                color: black,
                letterSpacing: 0.3,
              }}>{`${name.substr(0, 5)}***`}</Text>
            <Text
              style={{
                fontFamily: book,
                fontSize: small,
                color: darkerGreyCalendar,
                letterSpacing: 0.3,
              }}>
              {moment.utc(createdOn).format('MMMM YYYY')}
            </Text>
          </View>
        </CardItem>
        <CardItem>
          <Text
            style={{
              fontFamily: book,
              fontSize: regular,
              color: black,
              letterSpacing: 0.3,
              lineHeight: 18,
            }}>
            {testimonial.length > 220
              ? `${testimonial.substr(0, 220)}...`
              : testimonial}
          </Text>
        </CardItem>
        {/* {testimonial.length > 220 ? (
        <CardItem style={{ marginTop: 0, paddingTop: 0 }}>
          <Text>Read More</Text>
        </CardItem>
      ) : null} */}
        {(serviceRating === null && productRating === null) ||
        (serviceRating === 0 && productRating === 0) ? null : (
          <CardItem>
            {serviceRating === null || serviceRating === 0 ? null : (
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{marginBottom: 10}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: regular,
                      color: darkerGreyCalendar,
                      letterSpacing: 0.3,
                    }}>
                    Service
                  </Text>
                </View>
                <View
                  style={{
                    flexBasis: '40%',
                    flexDirection: 'row',
                    paddingRight: 20,
                  }}>
                  {new Array(serviceRating).fill(null).map((_, i) => {
                    return (
                      <View
                        key={String(i)}
                        style={{width: 20, height: 20, left: -5}}>
                        <Icon
                          type="MaterialCommunityIcons"
                          name="star"
                          style={{
                            fontSize: 20,
                            color: '#FFC95A',
                            marginLeft: 0,
                            marginRight: 0,
                            paddingLeft: 0,
                            paddingRight: 0,
                          }}
                        />
                      </View>
                    );
                  })}
                  {serviceRatingLeft === 0 || serviceRatingLeft === null
                    ? null
                    : new Array(serviceRatingLeft).fill(null).map((_, i) => {
                        return (
                          <View
                            key={String(i)}
                            style={{width: 20, height: 20, left: -5}}>
                            <Icon
                              type="MaterialCommunityIcons"
                              name="star"
                              style={{
                                fontSize: 20,
                                color: '#D5D5D5',
                                marginLeft: 0,
                                marginRight: 0,
                                paddingLeft: 0,
                                paddingRight: 0,
                              }}
                            />
                          </View>
                        );
                      })}
                </View>
              </View>
            )}
            {productRating === null || productRating === 0 ? null : (
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{marginBottom: 10}}>
                  <Text
                    style={{
                      fontFamily: book,
                      fontSize: regular,
                      color: darkerGreyCalendar,
                      letterSpacing: 0.3,
                    }}>
                    Product
                  </Text>
                </View>
                <View
                  style={{
                    flexBasis: '40%',
                    flexDirection: 'row',
                    paddingRight: 20,
                  }}>
                  {new Array(productRating).fill(null).map((_, i) => {
                    return (
                      <View
                        key={String(i)}
                        style={{width: 20, height: 20, left: -5}}>
                        <Icon
                          type="MaterialCommunityIcons"
                          name="star"
                          style={{
                            fontSize: 20,
                            color: '#FFC95A',
                            marginLeft: 0,
                            marginRight: 0,
                            paddingLeft: 0,
                            paddingRight: 0,
                          }}
                        />
                      </View>
                    );
                  })}
                  {productRatingLeft === 0 || productRatingLeft === null
                    ? null
                    : new Array(productRatingLeft).fill(null).map((_, i) => {
                        return (
                          <View
                            key={String(i)}
                            style={{width: 20, height: 20, left: -5}}>
                            <Icon
                              type="MaterialCommunityIcons"
                              name="star"
                              style={{
                                fontSize: 20,
                                color: '#D5D5D5',
                                marginLeft: 0,
                                marginRight: 0,
                                paddingLeft: 0,
                                paddingRight: 0,
                              }}
                            />
                          </View>
                        );
                      })}
                </View>
              </View>
            )}
          </CardItem>
        )}
        <CardItem style={{width: '100%'}}>
          <FlatList
            contentContainerStyle={{zIndex: -10}}
            scrollEnabled={false}
            legacyImplementation
            disableVirtualization
            data={gallery}
            extraData={gallery}
            numColumns={3}
            columnWrapperStyle={{
              width: '100%',
              //   justifyContent: 'space-between',
            }}
            keyExtractor={(item, index) => String(item.id)}
            renderItem={({item, index}) => {
              return (
                <View
                  style={
                    index === gallery.length - 1
                      ? {width: 100, height: 100, borderRadius: 4}
                      : {
                          borderRadius: 4,
                          width: 100,
                          height: 100,
                          marginRight: 10,
                        }
                  }>
                  {reviewGallery.length > 0 && reviewGallery.length > 3 ? (
                    index === gallery.length - 1 ? (
                      <View
                        style={{
                          borderRadius: 4,
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                          opacity: 0.6,
                          backgroundColor: 'rgba(0, 0, 0, 0.5)',
                          zIndex: 10,
                          position: 'absolute',
                          top: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                        }}>
                        {/* <Icon type="Feather" name="plus" style={{ fontSize: 25, color: 'white' }} /> */}
                        <Text
                          style={{
                            color: 'white',
                            fontSize: 15,
                            fontWeight: 'bold',
                          }}>
                          See More
                        </Text>
                      </View>
                    ) : null
                  ) : null}
                  <Image
                    source={{uri: item.url}}
                    style={{
                      borderRadius: 4,
                      width: 100,
                      height: 100,
                      zIndex: 1,
                      position: 'absolute',
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      backgroundColor: darkerGreyCalendar,
                    }}
                  />
                </View>
              );
            }}
          />
        </CardItem>
      </Card>
    </TouchableOpacity>
  );
};

class ReviewAndRatingList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      isLoading: true,
      isError: false,
      pageSize: 4,
      pageNumber: 1,
      totalCount: 0,
      dummyData: [1, 2, 3, 4, 5, 6, 7, 8],
      heightState: null,
      isPullOnRefresh: false,
      isLoadMore: false,
    };
  }

  componentDidMount() {
    this.fetch();
  }

  setLoadMore = status => {
    this.setState(
      prevState => ({
        ...prevState,
        isLoadMore: status,
        pageSize: prevState.pageSize + 10,
      }),
      () => {
        this.fetch();
      },
    );
  };

  setPullRefresh = () => {
    this.setState(
      prevState => ({
        ...prevState,
        isPullOnRefresh: true,
        pageSize: 4,
      }),
      () => {
        this.fetch();
      },
    );
  };

  fetch = () => {
    try {
      console.log('LIST: ', this.props);
      const {client, route} = this.props;
      const {params} = route;
      const {id} = params;
      client
        .query({
          query: QueryReview,
          variables: {
            merchantId: parseInt(id, 10),
            searchBy: '',
            itemDisplayed: this.state.pageSize,
            pageNumber: this.state.pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(responseReview => {
          console.log('responseReview: ', responseReview);
          const {data, errors} = responseReview;
          const {getCustomerTestimonial} = data;
          const {data: listReview, error, totalCount} = getCustomerTestimonial;

          if (errors) {
            this.setState(prevState => ({
              ...prevState,
              isError: true,
              isLoading: false,
              isPullOnRefresh: false,
              isLoadMore: false,
            }));
          } else {
            if (error) {
              this.setState(prevState => ({
                ...prevState,
                isError: false,
                isLoading: false,
                isPullOnRefresh: false,
                isLoadMore: false,
              }));
            } else {
              this.setState(
                prevState => ({
                  ...prevState,
                  list: listReview,
                  // list: [],
                  totalCount,
                }),
                () => {
                  this.setState(prevState => ({
                    ...prevState,
                    isError: false,
                    isLoading: false,
                    isPullOnRefresh: false,
                    isLoadMore: false,
                  }));
                },
              );
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          this.setState(prevState => ({
            ...prevState,
            isError: true,
            isLoading: false,
            isPullOnRefresh: false,
            isLoadMore: false,
          }));
        });
    } catch (error) {
      console.log('Error: ', error);
      this.setState(prevState => ({
        ...prevState,
        isError: true,
        isLoading: false,
        isPullOnRefresh: false,
        isLoadMore: false,
      }));
    }
  };

  keyExt = (item, i) => `${String(i)}`;

  render() {
    const {isLoading, isError, list, totalCount} = this.state;

    if (isLoading && !isError) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: containerColor,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator />
        </View>
      );
    }

    if (!isLoading && isError) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: containerColor,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            onPress={() => {
              this.setState(
                prevState => ({
                  ...prevState,
                  isLoading: true,
                  isError: false,
                }),
                () => {
                  this.fetch();
                },
              );
            }}>
            Refresh
          </Text>
        </View>
      );
    }

    return (
      <Container style={{backgroundColor: containerColor}}>
        <Header
          translucent={false}
          androidStatusBarColor={'white'}
          iosBarStyle="dark-content"
          style={{
            backgroundColor: 'white',
            elevation: 0,
            shadowOpacity: 0,
            borderBottomColor: darkerGreyCalendar,
            borderBottomWidth: 0.5,
            zIndex: 999,
          }}>
          <Left style={{flex: 0.3}}>
            <TouchableOpacity
              onPress={() => {
                try {
                  this.props.navigation.goBack(null);
                } catch (error) {
                  //
                }
              }}
              style={{padding: 5}}>
              <Icon
                type="Feather"
                name="chevron-left"
                style={{fontSize: RFPercentage(3.5), color: black}}
              />
            </TouchableOpacity>
          </Left>
          <Body
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
                letterSpacing: 0.3,
                textAlign: 'center',
              }}>
              Reviews
            </Text>
          </Body>
          <Right style={{flex: 0.3}} />
        </Header>
        <View style={{flex: 1, zIndex: -99, backgroundColor: 'white'}}>
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.isPullOnRefresh}
                onRefresh={this.setPullRefresh}
              />
            }
            scrollEnabled
            data={list}
            onEndReachedThreshold={0.01}
            onEndReached={() => {
              if (this.state.pageSize >= this.state.totalCount) {
                this.setLoadMore(false);
              } else {
                this.setLoadMore(true);
              }
            }}
            contentContainerStyle={{
              paddingLeft: 10,
              paddingRight: 10,
              paddingBottom: 25,
            }}
            ListEmptyComponent={() => {
              return (
                <Card
                  style={{width: '100%', height: width * 1.5, paddingTop: 55}}
                  transparent>
                  <CardItem cardBody style={{paddingTop: 100}}>
                    <Image
                      source={charNoOrder}
                      style={{width: '100%', height: width * 0.5, flex: 1}}
                      resizeMode="contain"
                    />
                  </CardItem>
                  <CardItem
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        top: 15,
                        fontFamily: medium,
                        fontSize: 21,
                        color: black,
                        letterSpacing: 0.45,
                        textAlign: 'center',
                        marginBottom: 10,
                      }}>
                      No Review Yet.
                    </Text>
                  </CardItem>
                  <CardItem
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: book,
                        fontSize: regular,
                        color: black,
                        letterSpacing: 0.3,
                        textAlign: 'center',
                        lineHeight: 20,
                      }}>
                      Let’s share your thought about our services
                    </Text>
                  </CardItem>
                </Card>
              );
            }}
            ListHeaderComponent={() => {
              if (list.length === 0) {
                return null;
              } else {
                return (
                  <Card transparent>
                    <CardItem>
                      <Text>
                        Total {totalCount}{' '}
                        {totalCount > 1 ? 'Reviews' : 'Review'}
                      </Text>
                    </CardItem>
                  </Card>
                );
              }
            }}
            ListFooterComponent={() => {
              if (this.state.isLoadMore) {
                return (
                  <Card transparent>
                    <CardItem
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text>Loading...</Text>
                    </CardItem>
                  </Card>
                );
              } else {
                return null;
              }
            }}
            keyExtractor={this.keyExt}
            renderItem={({item, index}) => {
              return (
                <ReviewCard
                  items={item}
                  parentIndex={index}
                  navigation={this.props.navigation}
                />
              );
            }}
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  console.log('State Redux: ', state);
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReviewAndRatingList);

const Wrapper = compose(withApollo)(ConnectedComponent);

export default props => {
  return <Wrapper {...props} />;
};
