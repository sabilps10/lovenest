import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Header, Left, Body, Right, Icon, Container, Content} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {connect} from 'react-redux';
import _ from 'lodash';

// redux
import ReviewMerchantRedux from '../../../redux/thunk/ReviewSelectedMerchantThunk';
import ReviewStaffThunk from '../../../redux/thunk/ReviewSelectedStaffThunk';

const {white, black, greyLine, disableCalendar, mainGreen} = Colors;
const {regular} = FontSize;
const {book, medium} = FontType;

// Query
import QUERY_GET_MERCHANT_REVIEW from '../../../graphql/queries/getMerchantForReview';

export const HeaderReview = props => {
  const {onPress} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        shadowRadius: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.6,
        borderBottomColor: 'rgb(231,231,231)',
        flexDirection: 'row',
        backgroundColor: 'white',
      }}>
      <Left style={{flex: 0.2}}>
        <TouchableOpacity onPress={() => onPress()}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: 25, color: black}}
          />
        </TouchableOpacity>
      </Left>
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
          Select Merchants
        </Text>
      </Body>
      <Right style={{flex: 0.2, height: '100%'}} />
    </Header>
  );
};

const MerchantSelect = props => {
  console.log('MerchantSelect Props: ', props);
  const {
    navigation,
    client,
    serviceSelected,
    reduxMerchantSelect,
    reduxStaffSelect,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [merchants, setMerchants] = useState([]);

  useEffect(() => {
    fetch();
  }, []);

  const goBack = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const grouping = listMerchant => {
    return new Promise((resolve, reject) => {
      try {
        let newArray = [];
        let counter = 0;
        const group = _.groupBy(listMerchant, 'serviceType');
        console.log('group length: ', group);
        if (group) {
          // eslint-disable-next-line no-unused-vars
          for (const [key, value] of Object.entries(group)) {
            newArray.push({
              label: key,
              data: value,
            });

            counter++;
          }
        }
        console.log('newArray: ', newArray);
        if (counter === newArray.length) {
          resolve(newArray);
        }
      } catch (error) {
        reject('error grouping');
      }
    });
  };

  const fetch = () => {
    try {
      console.log('serviceSelected >>> ', serviceSelected);
      client
        .query({
          query: QUERY_GET_MERCHANT_REVIEW,
          variables: {
            service: serviceSelected,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          const {getAllMerchants} = data;
          const {data: listMerchant, error} = getAllMerchants;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              const merchantGrouping = await grouping(listMerchant);
              console.log('merchantGrouping >>> ', merchantGrouping);
              if (merchantGrouping) {
                const filtering = await merchantGrouping
                  .map(d => {
                    if (d.label === serviceSelected) {
                      return [...d.data];
                    } else {
                      return null;
                    }
                  })
                  .filter(Boolean);
                console.log('filtering >>> ', filtering);
                if (filtering) {
                  await setMerchants([...filtering[0]]);
                  await setIsError(false);
                  await setIsLoading(false);
                } else {
                  await setIsError(true);
                  await setIsLoading(false);
                }
              } else {
                await setIsError(true);
                await setIsLoading(false);
              }
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>No Merchant related to service you selected</Text>
        </View>
      </Container>
    );
  } else {
    console.log('MERCHANTS: ', merchants);
    return (
      <Container>
        <HeaderReview
          {...props}
          onPress={() => {
            goBack();
          }}
        />
        <FlatList
          data={merchants}
          extraData={merchants}
          keyExtractor={(item, index) => `${item.id} MerchantsID`}
          listKey={(item, index) => `${item.id} MerchantsID`}
          renderItem={({item, index}) => {
            console.log('ITEM >>> :', item);
            return (
              <TouchableOpacity
                onPress={async () => {
                  await reduxMerchantSelect(item.id, item.name);
                  await reduxStaffSelect([]);
                  await goBack();
                }}
                style={{
                  backgroundColor: 'transparent',
                  width: '100%',
                  padding: 10,
                  paddingLeft: 15,
                  paddingRight: 15,
                }}>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: 40,
                    borderBottomColor: greyLine,
                    borderBottomWidth: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text>{item.name}</Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </Container>
    );
  }
};

const mapToState = state => {
  console.log('mapToState Merchant Select: ', state);
  const {review} = state;
  const {serviceSelected} = review;
  return {
    serviceSelected,
  };
};

const mapToDispatch = dispatch => {
  return {
    reduxMerchantSelect: (merchantId, merchantName) =>
      dispatch(ReviewMerchantRedux(merchantId, merchantName)),
    reduxStaffSelect: staffs => dispatch(ReviewStaffThunk(staffs)),
  };
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(MerchantSelect);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
