import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Dimensions,
  Image,
  RefreshControl,
  FlatList,
  PermissionsAndroid,
  Platform,
  Animated,
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Rating, AirbnbRating} from 'react-native-elements';
import {launchImageLibrary} from 'react-native-image-picker';
import {ReactNativeFile} from 'apollo-upload-client';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncToken from '../../../utils/AsyncstorageDataStructure/index';

// Mutations
import MUTATION_REVIEW from '../../../graphql/mutations/customerReviewMobile';
import MUTATION_UPLOAD_IMAGE from '../../../graphql/mutations/uploadImagesMobile';

// Redux
import ReduxServiceSelect from '../../../redux/thunk/ReviewSelectServiceThunk';
import ReduxMerchantSelect from '../../../redux/thunk/ReviewSelectedMerchantThunk';
import ReduxStaffSelect from '../../../redux/thunk/ReviewSelectedStaffThunk';

const {asyncToken} = AsyncToken;
const {width, height} = Dimensions.get('window');
const {
  white,
  black,
  greyLine,
  mainGreen,
  disableCalendar,
  mainRed,
  overlayDim,
} = Colors;
const {book, medium} = FontType;
const {regular} = FontSize;

export const HeaderReview = props => {
  const {onPress} = props;
  return (
    <Header
      translucent={false}
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        shadowRadius: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderBottomWidth: 0.6,
        borderBottomColor: 'rgb(231,231,231)',
        flexDirection: 'row',
        backgroundColor: 'white',
      }}>
      <Left style={{flex: 0.2}}>
        <TouchableOpacity onPress={() => onPress()}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{fontSize: 25, color: black}}
          />
        </TouchableOpacity>
      </Left>
      <Body
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <Text style={{fontFamily: medium, fontSize: regular, color: black}}>
          Review
        </Text>
      </Body>
      <Right style={{flex: 0.2, height: '100%'}} />
    </Header>
  );
};

const Review = props => {
  console.log('Review Props: ', props);
  const {
    navigation,
    client,
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
    serviceSelect,
    merchantSelect,
    staffSelect,
    positionYBottomNav,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, setIsRefreshing] = useState(false);

  const [service, setService] = useState('');
  const [merchant, setMerchant] = useState('');
  const [staff, setStaff] = useState([]);
  const [message, setMessage] = useState('');

  const [serviceRating, setServiceRating] = useState(0);
  const [productRating, setProductRating] = useState(0);
  const [appContentRating, setAppContentRating] = useState(0);
  const [bridalConsultantRating, setBridalConsultantRating] = useState(0);
  const [outStandingRating, setOutStandingRating] = useState(0);

  const [image, setImage] = useState([]);
  const [previewImage, setPreviewImage] = useState([]);
  const [statusUploadImage, setStatusUploadImage] = useState('');
  const [statusReviewSubmit, setStatusReviewSubmit] = useState('');
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isFailed, setIsFailed] = useState(false);

  useEffect(() => {
    validateReduxState();

    if (positionYBottomNav) {
      onChangeOpacity(false);
    }
    const subscriber = navigation.addListener('focus', () => {
      validateReduxState();
      if (positionYBottomNav) {
        onChangeOpacity(false);
      }
    });

    return subscriber;
  }, [serviceSelected, selectedMerchant, selectedMerchantName, selectedStaff]);

  const onChangeOpacity = status => {
    if (status) {
      Animated.timing(positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const validateReduxState = async () => {
    await setIsRefreshing(true);
    setTimeout(async () => {
      await setServiceSelected();
      await setSelectedMerchant();
      await setSelectedStaff();
      await setIsRefreshing(false);
    }, 500);
  };

  const setServiceSelected = async () => {
    await setService(serviceSelected ? serviceSelected : '');
  };

  const setSelectedMerchant = async () => {
    await setMerchant(selectedMerchantName ? selectedMerchantName : '');
  };

  const setSelectedStaff = async () => {
    const concatedText = await selectedStaff
      .map((data, i) => {
        if (data.selected) {
          return data.name;
        } else {
          return null;
        }
      })
      .filter(Boolean);

    if (concatedText) {
      await setStaff([...concatedText]);
    }
  };

  const goBack = async () => {
    try {
      await serviceSelect('');
      await merchantSelect(null, '');
      await staffSelect([]);
      navigation.goBack();
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const settingRating = async (endValue, typeRating) => {
    try {
      if (typeRating === 'Service') {
        await setServiceRating(endValue);
      } else if (typeRating === 'Product') {
        await setProductRating(endValue);
      } else if (typeRating === 'AppContent') {
        await setAppContentRating(endValue);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const settingRatingWithTitle = async (endValue, typeRating) => {
    try {
      if (typeRating === 'BridalConsultant') {
        await setBridalConsultantRating(endValue);
      } else if (typeRating === 'Outstanding') {
        await setOutStandingRating(endValue);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const pickImage = async () => {
    try {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'We need your permission',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
          const options = {
            mediaType: 'photo',
          };
          launchImageLibrary(options, async response => {
            if (response.didCancel) {
              console.log('Cancel upload: ', response.didCancel);
            } else if (response.error) {
              console.log('Error Upload: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
              const {uri, type, fileName} = response;
              const source = {uri};

              if (source) {
                const file = new ReactNativeFile({
                  uri,
                  name: fileName,
                  type,
                });
                console.log('FILE >>> ', file);
                if (file) {
                  await setImage([...image, file]);
                  await setPreviewImage([...previewImage, source]);
                }
              }
            }
          });
        }
      } else {
        const options = {
          mediaType: 'photo',
        };
        launchImageLibrary(options, async response => {
          if (response.didCancel) {
            console.log('Cancel upload: ', response.didCancel);
          } else if (response.error) {
            console.log('Error Upload: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const {uri, type, fileName} = response;
            const source = {uri};

            if (source) {
              const file = new ReactNativeFile({
                uri,
                name: fileName,
                type,
              });
              console.log('FILE >>> ', file);
              if (file) {
                await setImage([...image, file]);
                await setPreviewImage([...previewImage, source]);
              }
            }
          }
        });
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const uploadingImage = () => {
    return new Promise((resolve, reject) => {
      try {
        console.log('IMAGESSSS TO UPLOAD: ', image);
        client
          .mutate({
            mutation: MUTATION_UPLOAD_IMAGE,
            variables: {
              imagefiles: image,
              type: 'Review',
            },
          })
          .then(async response => {
            console.log('Response Upload Image: ', response);
            const {data, errors} = response;
            const {uploadImagesMobile} = data;
            const {data: listFinalImage, error} = uploadImagesMobile;

            if (errors) {
              resolve({
                data: null,
                status: 'failed',
              });
            } else {
              if (error) {
                resolve({
                  data: null,
                  status: 'failed',
                });
              } else {
                const manipulated = await Promise.all(
                  listFinalImage
                    .map((d, i) => {
                      return d.id;
                    })
                    .filter(Boolean),
                );
                console.log('manipulated >>>>> ', manipulated);
                if (manipulated) {
                  resolve({
                    data: manipulated,
                    status: 'success',
                  });
                }
              }
            }
            resolve({
              data: [],
              status: 'success',
            });
          })
          .catch(error => {
            console.log('Error Upload Image: ', error);
            resolve({
              data: null,
              status: 'failed',
            });
          });
      } catch (error) {
        resolve({
          data: null,
          status: 'failed',
        });
      }
    });
  };

  const filteredStaff = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const staffFilter = await selectedStaff
          .map((d, i) => {
            if (d.selected) {
              return {
                id: parseInt(d.id, 10),
                name: d.name,
                role: d.role,
              };
            } else {
              return null;
            }
          })
          .filter(Boolean);

        if (staffFilter) {
          resolve({
            data: staffFilter,
            error: false,
          });
        } else {
          resolve({
            data: null,
            error: true,
          });
        }
      } catch (error) {
        resolve({
          data: null,
          error: true,
        });
      }
    });
  };

  const submitReview = async () => {
    try {
      await setIsLoadingSubmit(true);
      await setStatusUploadImage('');
      await setStatusReviewSubmit('');
      await setIsSuccess(false);
      await setIsFailed(false);
      if (
        service === '' &&
        merchant === '' &&
        staff.length === 0 &&
        message === '' &&
        serviceRating === 0 &&
        productRating === 0
      ) {
        await setIsFailed(true);
        setTimeout(async () => {
          await setIsFailed(false);
          await setIsLoadingSubmit(false);
        }, 2000);
      } else {
        if (image.length > 0) {
          // with images
          await setStatusUploadImage('Uploading image...');
          const uploading = await uploadingImage();
          console.log('submit uploading: ', uploading);
          if (uploading.status === 'success') {
            await setStatusUploadImage('');
            await setStatusReviewSubmit('Submiting Review...');
            const staffFilteredData = await filteredStaff();
            if (!staffFilteredData.error) {
              const variables = {
                merchantId: parseInt(selectedMerchant, 10),
                staff: staffFilteredData.data,
                serviceRating: parseInt(serviceRating, 10),
                productRating: parseInt(productRating, 10),
                appRating: parseInt(appContentRating, 10),
                review1: parseInt(bridalConsultantRating, 10),
                review2: parseInt(outStandingRating, 10),
                reviewGallery: JSON.stringify(uploading.data),
                testimonial: message,
              };
              console.log('VARIABLES SUBMIT: ', variables);

              client
                .mutate({
                  mutation: MUTATION_REVIEW,
                  variables,
                })
                .then(async response => {
                  console.log('response SUBMIT REVIEW: ', response);
                  await setStatusReviewSubmit('');
                  await setIsSuccess(true);
                  setTimeout(async () => {
                    await setIsLoadingSubmit(false);
                    goBack();
                  }, 2000);
                })
                .catch(async error => {
                  console.log('Error SUBMIT REVIEW: ', error);
                  await setStatusReviewSubmit('');
                  await setIsFailed(true);
                  setTimeout(async () => {
                    await setIsFailed(false);
                    await setIsLoadingSubmit(false);
                  }, 2000);
                });
            }
          } else {
            // failed to upload image
            await setStatusReviewSubmit('');
            await setIsFailed(true);
            setTimeout(async () => {
              await setIsFailed(false);
              await setIsLoadingSubmit(false);
            }, 2000);
          }
        } else {
          // without images
          await setStatusUploadImage('');
          await setStatusReviewSubmit('Submiting Review...');
          const staffFilteredData = await filteredStaff();
          if (!staffFilteredData.error) {
            const variables = {
              merchantId: parseInt(selectedMerchant, 10),
              staff: staffFilteredData.data,
              serviceRating: parseInt(serviceRating, 10),
              productRating: parseInt(productRating, 10),
              appRating: parseInt(appContentRating, 10),
              review1: parseInt(bridalConsultantRating, 10),
              review2: parseInt(outStandingRating, 10),
              testimonial: message,
            };
            console.log('VARIABLES SUBMIT: ', variables);

            client
              .mutate({
                mutation: MUTATION_REVIEW,
                variables,
              })
              .then(async response => {
                console.log('response SUBMIT REVIEW: ', response);
                await setStatusReviewSubmit('');
                await setIsSuccess(true);
                setTimeout(async () => {
                  await setIsLoadingSubmit(false);
                  goBack();
                }, 2000);
              })
              .catch(async error => {
                console.log('Error SUBMIT REVIEW: ', error);
                await setStatusReviewSubmit('');
                await setIsFailed(true);
                setTimeout(async () => {
                  await setIsFailed(false);
                  await setIsLoadingSubmit(false);
                }, 2000);
              });
          }
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setStatusReviewSubmit('');
      await setIsFailed(true);
      setTimeout(async () => {
        await setIsFailed(false);
        await setIsLoadingSubmit(false);
      }, 2000);
    }
  };

  const submitReviewOnlyForFlorist = async () => {
    try {
      await setIsLoadingSubmit(true);
      await setStatusUploadImage('');
      await setStatusReviewSubmit('');
      await setIsSuccess(false);
      await setIsFailed(false);
      if (
        service === '' &&
        merchant === '' &&
        message === '' &&
        serviceRating === 0 &&
        productRating === 0
      ) {
        await setIsFailed(true);
        setTimeout(async () => {
          await setIsFailed(false);
          await setIsLoadingSubmit(false);
        }, 2000);
      } else {
        if (image.length > 0) {
          // with images
          await setStatusUploadImage('Uploading image...');
          const uploading = await uploadingImage();
          console.log('submit uploading: ', uploading);
          if (uploading.status === 'success') {
            await setStatusUploadImage('');
            await setStatusReviewSubmit('Submiting Review...');
            const staffFilteredData = await filteredStaff();
            if (!staffFilteredData.error) {
              const variables = {
                merchantId: parseInt(selectedMerchant, 10),
                staff: staffFilteredData.data,
                serviceRating: parseInt(serviceRating, 10),
                productRating: parseInt(productRating, 10),
                appRating: parseInt(appContentRating, 10),
                review1: parseInt(bridalConsultantRating, 10),
                review2: parseInt(outStandingRating, 10),
                reviewGallery: JSON.stringify(uploading.data),
                testimonial: message,
              };
              console.log('VARIABLES SUBMIT: ', variables);

              client
                .mutate({
                  mutation: MUTATION_REVIEW,
                  variables,
                })
                .then(async response => {
                  console.log('response SUBMIT REVIEW: ', response);
                  await setStatusReviewSubmit('');
                  await setIsSuccess(true);
                  setTimeout(async () => {
                    await setIsLoadingSubmit(false);
                    goBack();
                  }, 2000);
                })
                .catch(async error => {
                  console.log('Error SUBMIT REVIEW: ', error);
                  await setStatusReviewSubmit('');
                  await setIsFailed(true);
                  setTimeout(async () => {
                    await setIsFailed(false);
                    await setIsLoadingSubmit(false);
                  }, 2000);
                });
            }
          } else {
            // failed to upload image
            await setStatusReviewSubmit('');
            await setIsFailed(true);
            setTimeout(async () => {
              await setIsFailed(false);
              await setIsLoadingSubmit(false);
            }, 2000);
          }
        } else {
          // without images
          await setStatusUploadImage('');
          await setStatusReviewSubmit('Submiting Review...');
          const staffFilteredData = await filteredStaff();
          if (!staffFilteredData.error) {
            const variables = {
              merchantId: parseInt(selectedMerchant, 10),
              staff: staffFilteredData.data,
              serviceRating: parseInt(serviceRating, 10),
              productRating: parseInt(productRating, 10),
              appRating: parseInt(appContentRating, 10),
              review1: parseInt(bridalConsultantRating, 10),
              review2: parseInt(outStandingRating, 10),
              testimonial: message,
            };
            console.log('VARIABLES SUBMIT: ', variables);

            client
              .mutate({
                mutation: MUTATION_REVIEW,
                variables,
              })
              .then(async response => {
                console.log('response SUBMIT REVIEW: ', response);
                await setStatusReviewSubmit('');
                await setIsSuccess(true);
                setTimeout(async () => {
                  await setIsLoadingSubmit(false);
                  goBack();
                }, 2000);
              })
              .catch(async error => {
                console.log('Error SUBMIT REVIEW: ', error);
                await setStatusReviewSubmit('');
                await setIsFailed(true);
                setTimeout(async () => {
                  await setIsFailed(false);
                  await setIsLoadingSubmit(false);
                }, 2000);
              });
          }
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      await setStatusReviewSubmit('');
      await setIsFailed(true);
      setTimeout(async () => {
        await setIsFailed(false);
        await setIsLoadingSubmit(false);
      }, 2000);
    }
  };

  return (
    <Container>
      <HeaderReview onPress={() => goBack()} />
      {isLoadingSubmit ? (
        <View
          style={{
            flex: 1,
            zIndex: 99,
            backgroundColor: overlayDim,
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: width / 1.5,
              height: height / 6,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              padding: 20,
            }}>
            {isSuccess ? (
              <Icon
                type="Feather"
                name="check"
                style={{fontSize: 30, color: 'green'}}
              />
            ) : isFailed ? (
              <Icon
                type="Feather"
                name="x"
                style={{fontSize: 30, color: mainRed}}
              />
            ) : (
              <ActivityIndicator size="large" color={mainGreen} />
            )}
            <Text style={{marginVertical: 10}}>
              {statusUploadImage !== ''
                ? statusUploadImage
                : statusReviewSubmit !== ''
                ? statusReviewSubmit
                : isSuccess
                ? 'Success'
                : 'Failed To Submit'}
            </Text>
          </View>
        </View>
      ) : null}
      <Content
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => validateReduxState()}
          />
        }>
        <Card transparent style={{marginBottom: 25}}>
          <OpeningText />
          <DropDownForm
            title="Service"
            value={service}
            typeForm="Service"
            {...props}
            navigateTo={() => navigation.navigate('ReviewServiceSelect')}
          />
          <DropDownForm
            title="Merchant"
            value={merchant}
            typeForm="Merchant"
            {...props}
            navigateTo={() => navigation.navigate('ReviewMerchantSelect')}
          />
          {serviceSelected === 'Florist' ? null : (
            <DropDownForm
              title="Staff"
              value={staff}
              typeForm="Staff"
              {...props}
              navigateTo={() => {
                navigation.navigate('ReviewStaffSelect');
              }}
            />
          )}
          <CardItem style={{width: '100%'}}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                letterSpacing: 0.3,
                color: black,
              }}>
              Wich Do You Enjoy The Most?*
            </Text>
          </CardItem>
          <RatingSection
            typeRating="Service"
            value={serviceRating}
            onChange={(endValue, typeRating) =>
              settingRating(endValue, typeRating)
            }
          />
          <RatingSection
            typeRating="Product"
            value={productRating}
            onChange={(endValue, typeRating) =>
              settingRating(endValue, typeRating)
            }
          />
          <RatingSection
            typeRating="App Content"
            value={appContentRating}
            onChange={(endValue, typeRating) =>
              settingRating(endValue, typeRating)
            }
          />
          {/* Gap */}
          {serviceSelected === 'Florist' ? null : (
            <RatingSectionLongText
              title={'Did you find our consultant helpful?'}
              typeRating="BridalConsultant"
              value={bridalConsultantRating}
              onChange={(endValue, typeRating) =>
                settingRatingWithTitle(endValue, typeRating)
              }
            />
          )}

          {serviceSelected === 'Florist' ? null : (
            <RatingSectionLongText
              title={'Did this person do an outstanding job?'}
              typeRating="Outstanding"
              value={outStandingRating}
              onChange={(endValue, typeRating) =>
                settingRatingWithTitle(endValue, typeRating)
              }
            />
          )}
          <ReviewMessageForm
            title="Review"
            value={message}
            onChange={async e => {
              await setMessage(e);
            }}
          />
          <CardItem style={{width: '100%'}}>
            <View style={{width: '100%'}}>
              <View style={{marginBottom: 15}}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Image(s)
                </Text>
              </View>
              <View
                style={{width: '100%', flexDirection: 'row', flexWrap: 'wrap'}}>
                {previewImage.map((d, i) => {
                  return (
                    <View
                      style={{
                        width: width / 5,
                        height: width / 5,
                        marginRight: 15,
                        marginBottom: 15,
                        backgroundColor: greyLine,
                      }}>
                      <TouchableOpacity
                        onPress={async () => {
                          let oldImages = image;
                          let oldPrevImage = previewImage;

                          oldImages.splice(i, 1);
                          oldPrevImage.splice(i, 1);

                          await setImage([...oldImages]);
                          await setPreviewImage([...oldPrevImage]);
                        }}
                        style={{
                          width: 20,
                          height: 20,
                          backgroundColor: mainRed,
                          borderRadius: 20 / 2,
                          position: 'absolute',
                          top: -5,
                          right: -5,
                          zIndex: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Icon
                          type="Feather"
                          name="x"
                          style={{left: 9, fontSize: 15, color: white}}
                        />
                      </TouchableOpacity>
                      <Image
                        source={d}
                        style={{
                          zIndex: 1,
                          flex: 1,
                          width: null,
                          height: width / 5,
                        }}
                        resizeMode="cover"
                      />
                    </View>
                  );
                })}
                {image.length === 5 ? null : (
                  <TouchableOpacity
                    onPress={() => pickImage()}
                    style={{
                      width: width / 5,
                      height: height / 10,
                      backgroundColor: disableCalendar,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="plus"
                      style={{
                        fontSize: 35,
                        color: greyLine,
                        marginLeft: 0,
                        marginRight: 0,
                        right: 2,
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </CardItem>
        </Card>
      </Content>
      <FooterButton
        onPress={() => {
          if (serviceSelected === 'Florist') {
            submitReviewOnlyForFlorist();
          } else {
            submitReview();
          }
        }}
        isLoading={isLoading}
      />
    </Container>
  );
};

export const FooterButton = props => {
  const {onPress, isLoading} = props;
  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen, width: '100%'}}>
        <View style={{width: '100%', backgroundColor: mainGreen}}>
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                color: white,
                fontSize: regular,
                letterSpacing: 0.3,
              }}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

export const ReviewMessageForm = props => {
  const {title, value, onChange} = props;
  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
            }}>
            {title}*
          </Text>
        </View>
        <View>
          <TextInput
            numberOfLines={5}
            keyboardType="default"
            returnKeyType="done"
            onBlur={() => {
              Keyboard.dismiss();
            }}
            value={value}
            placeholder="Write your review here..."
            onChangeText={e => {
              onChange(e);
            }}
            multiline
            style={{
              top: 5,
              textAlignVertical: 'top',
              minHeight: 60,
              fontSize: regular,
              fontFamily: book,
              color: black,
              letterSpacing: 0.3,
              lineHeight: 25,
            }}
          />
        </View>
        <View
          style={{
            marginTop: 10,
            flex: 1,
            height: 1,
            borderBottomColor: greyLine,
            borderBottomWidth: 1,
          }}
        />
      </View>
    </CardItem>
  );
};

export const RatingSectionLongText = props => {
  const {typeRating, onChange, title} = props;

  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 20,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              color: black,
            }}>
            {title}
          </Text>
        </View>
        <View style={{width: '100%'}}>
          <Rating
            showRating={false}
            minValue={0}
            startingValue={0}
            onFinishRating={endValue => {
              onChange(endValue, typeRating);
            }}
            style={{paddingVertical: 10}}
          />
        </View>
      </View>
    </CardItem>
  );
};

export const RatingSection = props => {
  const {typeRating, onChange} = props;

  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              letterSpacing: 0.3,
              color: black,
            }}>
            {typeRating}
          </Text>
        </View>
        <View style={{width: '100%'}}>
          <Rating
            showRating={false}
            minValue={0}
            startingValue={0}
            onFinishRating={endValue => {
              onChange(endValue, typeRating);
            }}
            style={{paddingVertical: 10}}
          />
        </View>
      </View>
    </CardItem>
  );
};

export const DropDownForm = props => {
  const {title, value, navigateTo, typeForm} = props;

  return (
    <CardItem style={{width: '100%'}}>
      <View style={{width: '100%'}}>
        <View style={{marginBottom: 20}}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.7),
              color: black,
              letterSpacing: 0.3,
            }}>
            {title}*
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigateTo();
          }}
          style={{width: '100%', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              height: '100%',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 18,
              }}>
              {!value || value === ''
                ? `Select ${typeForm}`
                : typeForm === 'Staff'
                ? value?.length === 0
                  ? `Select ${typeForm}`
                  : value.join(', ')
                : value}
            </Text>
          </View>
          <View
            style={{
              flex: 0.15,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'flex-start',
            }}>
            <Icon
              type="Feather"
              name="chevron-down"
              style={{
                bottom: 1.5,
                color: black,
                fontSize: RFPercentage(2.3),
                marginLeft: 0,
                marginRight: 0,
              }}
            />
          </View>
        </TouchableOpacity>
        <View
          style={{
            marginTop: 10,
            flex: 1,
            height: 1,
            borderBottomColor: greyLine,
            borderBottomWidth: 1,
          }}
        />
      </View>
    </CardItem>
  );
};

export const OpeningText = () => {
  return (
    <CardItem>
      <Text
        style={{
          fontFamily: medium,
          fontSize: RFPercentage(1.8),
          color: black,
          letterSpacing: 0.3,
        }}>
        Share with us your thoughts about Love Nest service
      </Text>
    </CardItem>
  );
};

const mapToState = state => {
  const {review} = state;
  const {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  } = review;
  return {
    serviceSelected,
    selectedMerchant,
    selectedMerchantName,
    selectedStaff,
  };
};

const mapToDispatch = dispatch => {
  return {
    serviceSelect: service => dispatch(ReduxServiceSelect(service)),
    merchantSelect: (merchantId, merchantName) =>
      dispatch(ReduxMerchantSelect(merchantId, merchantName)),
    staffSelect: staffs => dispatch(ReduxStaffSelect(staffs)),
  };
};

const connected = connect(
  mapToState,
  mapToDispatch,
)(Review);

const Wrapper = compose(withApollo)(connected);

export default props => <Wrapper {...props} />;
