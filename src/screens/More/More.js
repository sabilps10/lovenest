import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Animated,
  Image,
  Dimensions,
} from 'react-native';
import Colors from '../../utils/Themes/Colors';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Icon,
} from 'native-base';
import {FontSize, FontType} from '../../utils/Themes/Fonts';
import {getVersion} from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncToken from '../../utils/AsyncstorageDataStructure';
import {charImage} from '../../utils/Themes/Images';

const {asyncToken} = AsyncToken;

const version = getVersion();

const {
  lightSalmon,
  black,
  veryLightPinkTwo,
  transparent,
  newContainerColor,
  greyLine,
} = Colors;
const {regular, small} = FontSize;
const {medium, book} = FontType;
const {charLoveAndWinIcon} = charImage;
const {width} = Dimensions?.get('screen');

class More extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
    };
  }

  navigationOptions = props => {
    const {navigation} = props;
    navigation.setOptions({
      headerTitle: 'More',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        fontSize: regular,
        color: black,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <View style={{display: 'none'}} />;
      },
    });
  };

  checkIsLogin = async () => {
    try {
      const userToken = await AsyncStorage.getItem(asyncToken);

      if (userToken) {
        this.setState(prevState => ({
          ...prevState,
          isLogin: true,
        }));
      } else {
        this.setState(prevState => ({
          ...prevState,
          isLogin: true,
        }));
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  onChangeOpacity = status => {
    if (status) {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(this?.props?.positionYBottomNav, {
        toValue: 300,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  componentDidMount() {
    const {props} = this;
    const {navigation} = props;
    this.checkIsLogin();
    this.navigationOptions(props);
    if (this?.props?.positionYBottomNav) {
      this.onChangeOpacity(true);
    }
    this._unsubscribe = navigation.addListener('focus', () => {
      this.checkIsLogin();
      this.navigationOptions(props);
      console.log('More ADD LISTENER DID MOUNT');
      if (this?.props?.positionYBottomNav) {
        this.onChangeOpacity(true);
      }
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  onProfile = label => {
    try {
      const {props} = this;
      const {navigation} = props;
      const parent = props.navigation.dangerouslyGetParent();
      parent.setOptions({
        tabBarVisible: false,
      });
      navigation.push(label);
    } catch (error) {
      console.log(`Error On : ${label}`, error);
    }
  };

  render() {
    console.log('Props More: ', this.props);
    return (
      <Container style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={transparent} />
        <Content contentContainerStyle={styles.contentContainerStyle}>
          <Card transparent style={styles.card}>
            <CardItem
              button
              activeOpacity={0.5}
              onPress={() => this.onProfile('Profile')}
              style={styles.cardItem}>
              <View style={styles.viewContainer}>
                <Left style={styles.left}>
                  <Icon type="Feather" name="users" style={styles.iconLeft} />
                </Left>
                <Body style={styles.body}>
                  <Text style={styles.text}>My Account</Text>
                </Body>
                <Right style={styles.right}>
                  <Icon
                    type="Feather"
                    name="chevron-right"
                    style={styles.iconRight}
                  />
                </Right>
              </View>
            </CardItem>
            {this.state.isLogin ? (
              <CardItem
                button
                activeOpacity={0.5}
                onPress={() => this.onProfile('Review')}
                style={styles.cardItem}>
                <View style={styles.viewContainer}>
                  <Left style={styles.left}>
                    <Icon type="Feather" name="star" style={styles.iconLeft} />
                  </Left>
                  <Body style={styles.body}>
                    <Text style={styles.text}>Review</Text>
                  </Body>
                  <Right style={styles.right}>
                    <Icon
                      type="Feather"
                      name="chevron-right"
                      style={styles.iconRight}
                    />
                  </Right>
                </View>
              </CardItem>
            ) : null}
            <CardItem
              button
              activeOpacity={0.5}
              onPress={() => this.onProfile('AboutUs')}
              style={styles.cardItem}>
              <View style={styles.viewContainer}>
                <Left style={styles.left}>
                  <Icon
                    type="Feather"
                    name="alert-circle"
                    style={styles.iconLeft}
                  />
                </Left>
                <Body style={styles.body}>
                  <Text style={styles.text}>About Love Nest</Text>
                </Body>
                <Right style={styles.right}>
                  <Icon
                    type="Feather"
                    name="chevron-right"
                    style={styles.iconRight}
                  />
                </Right>
              </View>
            </CardItem>
            <CardItem
              button
              activeOpacity={0.5}
              onPress={() => this.onProfile('AppointmentGuide')}
              style={styles.cardItem}>
              <View style={styles.viewContainer}>
                <Left style={styles.left}>
                  <Icon
                    type="Feather"
                    name="calendar"
                    style={styles.iconLeft}
                  />
                </Left>
                <Body style={styles.body}>
                  <Text style={styles.text}>Appointment Guide</Text>
                </Body>
                <Right style={styles.right}>
                  <Icon
                    type="Feather"
                    name="chevron-right"
                    style={styles.iconRight}
                  />
                </Right>
              </View>
            </CardItem>
            {this.state.isLogin ? (
              <CardItem
                button
                activeOpacity={0.5}
                onPress={() => this.onProfile('LoveAndWin')}
                style={styles.cardItem}>
                <View style={styles.viewContainer}>
                  <Left style={styles.left}>
                    <Image
                      resizeMode="contain"
                      source={charLoveAndWinIcon}
                      style={{width: width * 0.05, height: width * 0.05}}
                    />
                  </Left>
                  <Body style={styles.body}>
                    <Text style={styles.text}>Love & Win Cash</Text>
                  </Body>
                  <Right style={styles.right}>
                    <Icon
                      type="Feather"
                      name="chevron-right"
                      style={styles.iconRight}
                    />
                  </Right>
                </View>
              </CardItem>
            ) : null}
            <CardItem
              button
              activeOpacity={0.5}
              onPress={() => this.onProfile('TermAndConditionStack')}
              style={styles.cardItem}>
              <View style={{...styles.viewContainer2}}>
                <Left style={styles.left}>
                  <Icon
                    type="Feather"
                    name="clipboard"
                    style={styles.iconLeft}
                  />
                </Left>
                <Body style={styles.body}>
                  <Text style={styles.text}>Terms & Conditions</Text>
                </Body>
                <Right style={styles.right}>
                  <Icon
                    type="Feather"
                    name="chevron-right"
                    style={styles.iconRight}
                  />
                </Right>
              </View>
            </CardItem>
          </Card>
          <Card transparent style={styles.cardVersion}>
            <CardItem style={styles.cardItemVersion}>
              <Text style={styles.versionText}>Version {version}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {backgroundColor: 'white'},
  contentContainerStyle: {},
  card: {
    elevation: 0,
    shadowOpacity: 0,
    marginLeft: 0,
    marginRight: 0,
    paddingBottom: 10,
  },
  cardVersion: {paddingTop: 50, elevation: 0, shadowOpacity: 0},
  versionText: {
    fontFamily: book,
    fontSize: small,
    color: black,
    lineHeight: 14,
    letterSpacing: 0.2,
  },
  cardItemVersion: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: transparent,
  },
  cardItem: {height: 48, backgroundColor: transparent, flexDirection: 'column'},
  viewContainer: {
    flexDirection: 'row',
    width: '100%',
    borderBottomColor: veryLightPinkTwo,
    borderBottomWidth: 1,
    paddingBottom: 10,
    height: 40,
    paddingTop: 5,
  },
  viewContainer2: {
    flexDirection: 'row',
    width: '100%',
    borderBottomColor: veryLightPinkTwo,
    borderBottomWidth: 0,
    paddingBottom: 10,
    height: 40,
    paddingTop: 5,
  },
  left: {
    flexBasis: '10%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  iconLeft: {fontSize: 20, color: black},
  body: {flexBasis: '80%', justifyContent: 'center', alignItems: 'flex-start'},
  text: {
    fontFamily: medium,
    fontSize: regular,
    color: black,
    letterSpacing: 0.3,
    lineHeight: 18,
  },
  right: {flexBasis: '10%', justifyContent: 'center', alignItems: 'flex-end'},
  iconRight: {fontSize: 20, color: black},
});

export default More;
