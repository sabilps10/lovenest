import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Card, CardItem, Button} from 'native-base';
import Colors from '../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../utils/Themes/Fonts';
import Header from '../../components/Header/Common/index';
import CardBenefit from '../../components/Home/Benefit/CardBenefit';

// Query
import GET_ALL_BENEFIT from '../../graphql/queries/banners';

const {white, black, mainGreen, mainRed} = Colors;
const {book, medium} = FontType;

const AllBenefit = props => {
  const {navigation, client} = props;

  const [list, setList] = useState([]);

  const [totalCount, setTotalCount] = useState(0);
  const [itemDisplayed, setItemDisplayed] = useState(16);
  const pageNumber = 1;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [refreshing, setRefreshing] = useState(false);

  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    fetch();

    const subs = navigation.addListener('focus', () => {
      fetch();
    });

    return () => {
      subs();
    };
  }, [isLoadMore, onLoadMore, onRefresh]);

  const onLoadMore = async () => {
    try {
      if (isLoadMore) {
        let oldPageSize = itemDisplayed;
        let newPageSize = (oldPageSize += 10);
        await setItemDisplayed(newPageSize);
        await fetch();
      }
    } catch (error) {
      console.log('Error onLoadMore: ', error);
      setIsLoadMore(false);
      setItemDisplayed(10);
    }
  };

  const onRefresh = async () => {
    try {
      await setRefreshing(true);
      await setItemDisplayed(10);
      await fetch();
    } catch (error) {
      await setRefreshing(false);
    }
  };

  const fetch = async () => {
    try {
      await client
        .query({
          query: GET_ALL_BENEFIT,
          variables: {
            tagId: 3,
            itemDisplayed,
            pageNumber,
            searchBy: '',
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response get benefit list: ', response);
          const {data, errors} = response;
          const {bannersMobile} = data;
          const {
            data: benefitList,
            error,
            totalCount: totalData,
          } = bannersMobile;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setList([...benefitList]);
              await setIsError(false);
              await setIsLoading(false);
              await setRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setRefreshing(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      await setIsError(true);
      await setIsLoading(false);
      await setRefreshing(false);
      await setIsLoadMore(false);
    }
  };

  const buttonLeft = () => {
    try {
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container>
        <Header
          title={'Love Nest Benefits'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      </Container>
    );
  } else if (!isLoading && isError) {
    return (
      <Container>
        <Header
          title={'Love Nest Benefits'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Button
            style={{
              backgroundColor: mainGreen,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: white}}>Refresh</Text>
          </Button>
        </View>
      </Container>
    );
  } else {
    return (
      <Container>
        <Header
          title={'Love Nest Benefits'}
          buttonLeft={() => buttonLeft()}
          buttonRight={() => {}}
          showRightButton={false}
        />
        <View style={{flex: 1, zIndex: -99}}>
          <ListBenefits
            {...props}
            setIsLoadMore={async () => await setIsLoadMore(true)}
            isLoadMore={isLoadMore}
            onLoadMore={() => onLoadMore()}
            data={list}
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        </View>
      </Container>
    );
  }
};

export const ListBenefits = props => {
  const {
    data,
    refreshing,
    onRefresh,
    isLoadMore,
    onLoadMore,
    setIsLoadMore,
  } = props;

  return (
    <FlatList
      ListFooterComponent={() => {
        if (isLoadMore) {
          return (
            <ActivityIndicator
              size="small"
              color={mainGreen}
              style={{marginVertical: 15}}
            />
          );
        } else {
          return null;
        }
      }}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      onEndReached={({distanceFromEnd}) => {
        if (distanceFromEnd < 0) {
          onLoadMore();
        }
      }}
      onMomentumScrollBegin={() => setIsLoadMore()}
      onEndReachedThreshold={0.1}
      legacyImplementation
      contentContainerStyle={{
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 10,
        marginBottom: 80,
      }}
      numColumns={2}
      columnWrapperStyle={{
        marginBottom: 8,
        justifyContent: 'space-between',
      }}
      data={data}
      extraData={data}
      listKey={(_, index) => `${String(index)} LN Benefits`}
      renderItem={({item, index}) => {
        return <CardBenefit data={item} />;
      }}
    />
  );
};

const Wrapper = compose(withApollo)(AllBenefit);

export default props => <Wrapper {...props} />;
