import React, {useState, useEffect} from 'react';
import {Text, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, CardItem, Card} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Common Screen
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import EmptyTabScreen from '../../../components/Cards/Merchants/EmptyTabScreen';
import ProductCard from '../../../components/Cards/Merchants/ProductCard';
import ButtonExploreAll from '../../../components/Button/buttonExploreAll';

// Shimmer
import ProductShimmer from '../../../components/Loader/Merchant/ProductShimmer';

// Product Query
import PRODUCT_BY_MERCHANT_QUERY from '../../../graphql/queries/productByMerchant';

const {greyLine} = Colors;
const {medium} = FontType;

const Product = props => {
  console.log('Product Props: ', props);
  const {settingHeightTab, heights, navigation, client, route} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoadingProduct, setIsLoadingProduct] = useState(true);
  const [isErrorProduct, setIsErrorProduct] = useState(false);

  const [isLoadingAccessories, setIsLoadingAccessories] = useState(true);
  const [isErrorAccessories, setIsErrorAccessories] = useState(false);

  const [productList, setProductList] = useState([]);
  const [accessoriesList, setAccessoriesList] = useState([]);
  const [totalProduct, setTotalProduct] = useState(0);
  const [totalAccessories, setTotalAccessories] = useState(0);

  useEffect(() => {
    fetchAll();
    const subscriber = navigation.addListener('tabPress', () => {
      fetchAll();
    });
    return () => {
      subscriber();
    };
  }, [navigation, heights]);

  const fetchAll = () => {
    fetchProduct();
    fetchAccessories();
  };

  const fetchProduct = () => {
    try {
      client
        .query({
          query: PRODUCT_BY_MERCHANT_QUERY,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType: String(serviceType),
            bridalType: [],
            bridalCategory: [],
            bridalColor: [],
            jewelleryType: [],
            smartHomeCategory: [],
            pageSize: 4,
            pageNumber: 1,
            accsCategory: [],
            accsDetails: [],
            accsRange: [],
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response Get Product: ', response);
          const {data, errors} = response;
          const {productsByMerchant} = data;
          const {data: products, error, totalData} = productsByMerchant;

          if (errors) {
            await setIsErrorProduct(true);
            await setIsLoadingProduct(false);
          } else {
            if (error) {
              await setIsErrorProduct(true);
              await setIsLoadingProduct(false);
            } else {
              await setTotalProduct(totalData);
              await setProductList([...products]);
              // await setProductList([]);
              await setIsErrorProduct(false);
              await setIsLoadingProduct(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorProduct(true);
          await setIsLoadingProduct(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorProduct(true);
      setIsLoadingProduct(false);
    }
  };

  const fetchAccessories = () => {
    try {
      client
        .query({
          query: PRODUCT_BY_MERCHANT_QUERY,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType: 'Accessories',
            bridalType: [],
            bridalCategory: [],
            bridalColor: [],
            jewelleryType: [],
            smartHomeCategory: [],
            pageSize: 4,
            pageNumber: 1,
            accsCategory: [],
            accsDetails: [],
            accsRange: [],
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response Get Product: ', response);
          const {data, errors} = response;
          const {productsByMerchant} = data;
          const {data: products, error, totalData} = productsByMerchant;

          if (errors) {
            await setIsErrorAccessories(true);
            await setIsLoadingAccessories(false);
          } else {
            if (error) {
              await setIsErrorAccessories(true);
              await setIsLoadingAccessories(false);
            } else {
              await setTotalAccessories(totalData);
              await setAccessoriesList([...products]);
              // await setAccessoriesList([]);
              await setIsErrorAccessories(false);
              await setIsLoadingAccessories(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorAccessories(true);
          await setIsLoadingAccessories(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorAccessories(true);
      setIsLoadingAccessories(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height: layoutHeight} = nativeEvent;
    settingHeightTab(layoutHeight);
  };

  const reload = async () => {
    await setIsErrorAccessories(false);
    await setIsErrorProduct(false);
    await setIsLoadingAccessories(true);
    await setIsLoadingAccessories(true);
    await fetchAll();
  };

  if (isErrorProduct && isErrorAccessories) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  if (
    !isLoadingAccessories &&
    !isLoadingProduct &&
    !isErrorAccessories &&
    !isErrorProduct &&
    productList.length === 0 &&
    accessoriesList.length === 0
  ) {
    return (
      <EmptyTabScreen
        heights={heights}
        onLayout={onLayout}
        message="Currently no products available. Don’t worry we’ll notify you soon!"
      />
    );
  }

  return (
    <Container style={{height: heights}}>
      {!isErrorProduct ? (
        isLoadingProduct ? (
          <Content
            onLayout={onLayout}
            contentContainerStyle={{
              padding: 15,
              paddingTop: 25,
              paddingBottom: 25,
            }}>
            <Card transparent>
              <ProductShimmer />
            </Card>
          </Content>
        ) : (
          <ProductListCard
            merchantId={id}
            serviceType={serviceType}
            label={'Product'}
            total={totalProduct}
            onLayout={onLayout}
            productList={productList}
            navigation={navigation}
          />
        )
      ) : null}
      {!isErrorAccessories ? (
        isLoadingAccessories ? (
          <Content
            onLayout={onLayout}
            contentContainerStyle={{
              padding: 15,
              paddingTop: 25,
              paddingBottom: 25,
            }}>
            <Card transparent>
              <ProductShimmer />
            </Card>
          </Content>
        ) : (
          <ProductListCard
            merchantId={id}
            serviceType={serviceType}
            label={'Accessories'}
            total={totalAccessories}
            onLayout={onLayout}
            productList={accessoriesList}
            navigation={navigation}
          />
        )
      ) : null}
    </Container>
  );
};

export const ProductListCard = props => {
  const {
    merchantId,
    serviceType,
    onLayout,
    productList,
    navigation,
    total,
    label,
  } = props;
  const goToProducts = () => {
    if (label === 'Product') {
      navigation.navigate('Products', {id: merchantId, serviceType});
    } else {
      // Go to Accessories List Screen
      navigation.navigate('Accessories', {
        id: merchantId,
        serviceType: 'Accessories',
      });
    }
  };
  return (
    <>
      {productList.length === 0 ? null : (
        <Card transparent>
          <CardItem>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: greyLine,
                letterSpacing: 0.3,
              }}>
              {total > 1 ? `${total} Products` : `${total} Product`}
            </Text>
          </CardItem>
        </Card>
      )}
      <FlatList
        disableVirtualization
        ListFooterComponent={() => {
          if (productList.length === 0) {
            return null;
          } else {
            return <ButtonExploreAll onPress={goToProducts} />;
          }
        }}
        scrollEnabled={false}
        onLayout={onLayout}
        data={productList}
        numColumns={2}
        contentContainerStyle={{
          padding: 15,
          paddingBottom: 15,
          paddingTop: 0,
        }}
        columnWrapperStyle={{marginBottom: 10, justifyContent: 'space-between'}}
        listKey={(_, index) => `${String(index)} Product`}
        renderItem={({item}) => {
          const {
            featuredImageDynamicURL,
            featuredImageURL,
            name,
            id: productId,
          } = item;
          const source =
            featuredImageDynamicURL === null
              ? {uri: featuredImageURL}
              : {uri: `${featuredImageDynamicURL}=h500`};
          return (
            <ProductCard
              label={label}
              name={name}
              source={source}
              merchantId={merchantId}
              productId={productId}
              navigation={navigation}
            />
          );
        }}
      />
    </>
  );
};

const Wrapper = compose(withApollo)(Product);

export default props => <Wrapper {...props} />;
