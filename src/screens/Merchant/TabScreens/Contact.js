import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, FlatList, Linking, Platform} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem, Icon} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import {socialMediaIcon} from '../../../utils/Themes/Images';
import GeocodeByAddress from '../../../utils/Geocoding';
import Loader from '../../../components/Loader/JustLoader';

//Query Get Merchant Detail
import MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

// Common screens
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import Map from '../../../components/Map/MapWithNavigationButton';
import ButtonSocialMedia from '../../../components/Button/buttonSocialMedia';

const {white, black, greyLine} = Colors;
const {book, medium} = FontType;
const {facebook, youtube, instagram, pinterest} = socialMediaIcon;

const Contact = props => {
  console.log('Product Props: ', props);
  const {navigation, settingHeightTab, heights, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [merchantDetail, setMerchantDetail] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetch();
  }, [navigation, heights]);

  const fetch = () => {
    try {
      client
        .query({
          query: MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Fetch Merchant Detail Contact: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;
          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (getMerchantVer2) {
              await setMerchantDetail(getMerchantVer2);
              await setIsError(false);
              await setIsLoading(false);
            } else {
              await setIsError(true);
              await setIsLoading(false);
            }
          }
        })
        .catch(error => {
          console.log('Promise Error Fetch Contact: ', error);
          setIsError(true);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Fetch Contact: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height} = nativeEvent;
    settingHeightTab(height);
  };

  const reload = async () => {
    await setIsLoading(true);
    await setIsError(false);
    await fetch();
  };

  const openMapByAddress = async address => {
    try {
      const getLocationDetail = await GeocodeByAddress.detailLocation(address);
      console.log('getLocationDetail: ', getLocationDetail);

      if (getLocationDetail) {
        const lat = getLocationDetail.location.lat;
        const lng = getLocationDetail.location.lng;

        const latitude = lat;
        const longitude = lng;
        const label = `${address}`;

        const splitingAddress = label.replace(',', ' ');
        const replaceAddressWithReservedSymbol = splitingAddress.replace(
          '#',
          '',
        );
        console.log('splitingAddress: ', replaceAddressWithReservedSymbol);

        const url = Platform.select({
          ios: `https://www.google.com/maps/search/?api=1&query=${replaceAddressWithReservedSymbol}&center=${latitude},${longitude}`,
          android: `geo:${latitude},${longitude}?q=${replaceAddressWithReservedSymbol}`,
        });
        Linking.canOpenURL(url)
          .then(supported => {
            if (!supported) {
              const browser_url = `https://www.google.de/maps/@${latitude},${longitude}?q=${label}`;
              return Linking.openURL(browser_url);
            }
            return Linking.openURL(url);
          })
          .catch(err => console.log('error', err));
      }
    } catch (error) {
      console.log('error: ', error);
    }
  };

  const openMap = item => {
    try {
      const label = `${item.locationName}, ${item.address}, ${item.country}`;

      const url = Platform.select({
        ios: `https://www.google.com/maps/search/?api=1&query=${label}&center=${
          item.latitude
        },${item.longitude}`,
        android: `geo:${item.latitude},${item.longitude}?q=${label}`,
      });
      Linking.canOpenURL(url)
        .then(supported => {
          if (!supported) {
            const browser_url = `https://www.google.de/maps/@${item.latitude},${
              item.longitude
            }?q=${label}`;
            return Linking.openURL(browser_url);
          } else {
            return Linking.openURL(url);
          }
        })
        .catch(error => console.log('Error: ', error));
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const sendEmail = email => {
    try {
      Linking.openURL(`mailto:${email}`);
    } catch (error) {
      console.log('Send Email Error: ', error);
    }
  };

  const openCall = phoneNumber => {
    try {
      Linking.openURL(`tel:${phoneNumber}`);
    } catch (error) {
      console.log('Error on Call: ', error);
    }
  };

  const sosmedOpenLink = url => {
    if (url !== '') {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log(`Don't know how to open URI: ${url}`);
        }
      });
    }
  };

  if (isLoading && !isError) {
    return (
      <Container style={{height: heights}}>
        <Content
          onLayout={onLayout}
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: 60,
              height: 60,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Loader />
          </View>
        </Content>
      </Container>
    );
  }

  if (!isLoading && isError) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <Content
        onLayout={onLayout}
        contentContainerStyle={{
          padding: 0,
          paddingBottom: 25,
        }}>
        <ListMapAddress
          merchantDetail={merchantDetail}
          openCall={openCall}
          openMap={openMap}
        />
        <AddressCardNonList
          merchantDetail={merchantDetail}
          openCall={openCall}
          openMapByAddress={openMapByAddress}
          sosmedOpenLink={sosmedOpenLink}
          sendEmail={sendEmail}
        />
      </Content>
    </Container>
  );
};

export const AddressCardNonList = props => {
  const {
    merchantDetail,
    openCall,
    openMapByAddress,
    sosmedOpenLink,
    sendEmail,
  } = props;

  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card transparent style={{marginTop: 0}}>
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openMapByAddress(merchantDetail[0].address);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].address}`}</Text>
              </View>
            </CardItem>
          ) : null}
          {merchantDetail[0].addresses.length === 0 ? (
            <CardItem
              button
              onPress={() => {
                openCall(
                  `${merchantDetail[0].countryCode}${merchantDetail[0].phone}`,
                );
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${merchantDetail[0].countryCode}${
                  merchantDetail[0].phone
                }`}</Text>
              </View>
            </CardItem>
          ) : null}
          <CardItem
            button
            onPress={() => {
              sendEmail(merchantDetail[0].email);
            }}
            style={{width: '100%'}}>
            <View
              style={{
                flex: 0.1,
                height: '100%',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 3,
              }}>
              <Icon
                type="Feather"
                name="mail"
                style={{fontSize: RFPercentage(2), color: black}}
              />
            </View>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {merchantDetail[0].email}
              </Text>
            </View>
          </CardItem>
          <CardItem style={{width: '100%'}}>
            <View
              style={{
                height: 1,
                width: '100%',
                borderBottomColor: greyLine,
                borderBottomWidth: 0.5,
              }}
            />
          </CardItem>
          <CardItem>
            {merchantDetail[0].facebookLink ? (
              <ButtonSocialMedia
                iconType={facebook}
                onPress={() => sosmedOpenLink(merchantDetail[0].facebookLink)}
              />
            ) : null}
            {merchantDetail[0].instagramLink ? (
              <ButtonSocialMedia
                iconType={instagram}
                onPress={() => sosmedOpenLink(merchantDetail[0].instagramLink)}
              />
            ) : null}
            {merchantDetail[0].pinterestLink ? (
              <ButtonSocialMedia
                iconType={pinterest}
                onPress={() => sosmedOpenLink(merchantDetail[0].pinterestLink)}
              />
            ) : null}
            {merchantDetail[0].youtubeLink ? (
              <ButtonSocialMedia
                iconType={youtube}
                onPress={() => sosmedOpenLink(merchantDetail[0].youtubeLink)}
              />
            ) : null}
          </CardItem>
        </Card>
      );
    }, [merchantDetail]);

  if (merchantDetail) {
    return RenderCard();
  } else {
    return null;
  }
};

export const ListMapAddress = props => {
  const {merchantDetail, openCall, openMap} = props;
  const keyExt = item => `${String(item.id)}`;
  const RenderList = () =>
    useMemo(() => {
      return (
        <FlatList
          scrollEnabled={false}
          legacyImplementation
          disableVirtualization
          data={merchantDetail[0].addresses}
          extraData={merchantDetail[0].addresses}
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item}) => {
            return (
              <MapAddressCard
                item={item}
                openCall={openCall}
                openMap={openMap}
              />
            );
          }}
        />
      );
    }, [merchantDetail]);
  if (merchantDetail) {
    return RenderList();
  } else {
    return null;
  }
};

export const MapAddressCard = props => {
  const {item, openCall, openMap} = props;
  const RenderCard = () =>
    useMemo(() => {
      return (
        <>
          <Card transparent style={{marginTop: 0}}>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>{`${item.locationName}`}</Text>
            </CardItem>
          </Card>
          <Map
            withDirectionText={false}
            forMapAddresses={`${item.locationName}, ${item.address}, ${
              item.country
            }`}
            onNavigate={() => openMap(item)}
          />
          <Card transparent>
            <CardItem
              button
              onPress={() => {
                openMap(item);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="map-pin"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.locationName}, ${item.address}, ${
                  item.country
                }`}</Text>
              </View>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                openCall(`${item.countryCode}${item.phone}`);
              }}
              style={{width: '100%'}}>
              <View
                style={{
                  flex: 0.1,
                  height: '100%',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  paddingTop: 3,
                }}>
                <Icon
                  type="Feather"
                  name="phone"
                  style={{fontSize: RFPercentage(2), color: black}}
                />
              </View>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: book,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    lineHeight: 18,
                    letterSpacing: 0.3,
                  }}>{`${item.countryCode}${item.phone}`}</Text>
              </View>
            </CardItem>
            <CardItem style={{width: '100%'}}>
              <View
                style={{
                  height: 1,
                  width: '100%',
                  borderBottomColor: greyLine,
                  borderBottomWidth: 0.5,
                }}
              />
            </CardItem>
          </Card>
        </>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(Contact);

export default props => <Wrapper {...props} />;
