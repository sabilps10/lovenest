import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  Animated,
  ScrollView,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, List, Item, Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import AsyncImage from '../../../components/Image/AsyncImage';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

//Query Get Merchant Detail
import MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

// Common screens
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import AboutShimmer from '../../../components/Loader/Merchant/AboutShimmer';

const {book, medium} = FontType;
const {white, newContainerColor, greyLine, black} = Colors;
const {width, height} = Dimensions.get('window');
const AnimatedCard = Animated.createAnimatedComponent(Card);

const About = props => {
  console.log('About Screen Tab Props: ', props);
  const {navigation, settingHeightTab, heights, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [about, setAbout] = useState('');
  const [relatedMerchant, setRelatedMerchant] = useState([]);

  useEffect(() => {
    fetch();

    const subsriber = navigation.addListener('tabPress', () => {
      fetch();
    });

    return () => {
      subsriber();
    };
  }, [heights, navigation]);

  const fetch = () => {
    try {
      client
        .query({
          query: MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response Fetch About Merchant: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (getMerchantVer2) {
              const {
                description,
                relatedMerchant: relatedMerchants,
              } = getMerchantVer2[0];
              await setAbout(description);
              await setRelatedMerchant(
                relatedMerchants === null || relatedMerchants === undefined
                  ? []
                  : relatedMerchants.length === 0
                  ? []
                  : [...relatedMerchants],
              );
              await setIsError(false);
              await setIsLoading(false);
            } else {
              await setIsError(true);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error Fetch About Merchant: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error About Tab Fetch: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height: layoutHeight} = nativeEvent;
    settingHeightTab(layoutHeight);
  };

  const reload = async () => {
    await setIsLoading(true);
    await setIsError(false);
    await fetch();
  };

  if (isLoading && !isError) {
    return (
      <Container style={{height: heights}}>
        <Content onLayout={onLayout} scrollEnabled={false}>
          <Card transparent>
            <CardItem>
              <AboutShimmer />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }

  if (!isLoading && isError) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <Content
        scrollEnabled={false}
        onLayout={onLayout}
        contentContainerStyle={{
          padding: 15,
          paddingTop: 10,
          paddingBottom: 25,
          paddingLeft: 0,
          paddingRight: 0,
        }}>
        <Card
          transparent
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
          }}>
          <CardItem>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: black,
              }}>
              Description
            </Text>
          </CardItem>
          <CardItem>
            <Text
              style={{
                letterSpacing: 0.3,
                lineHeight: 18,
                fontFamily: book,
                fontSize: RFPercentage(1.7),
                color: black,
              }}>
              {about}
            </Text>
          </CardItem>
        </Card>
        <ListRelatedMerchant data={relatedMerchant} navigation={navigation} />
      </Content>
    </Container>
  );
};

export const ListRelatedMerchant = props => {
  const {data, navigation} = props;
  if (data) {
    const keyExt = (item, index) => `${String(item.id)} Related Merchant`;
    return (
      <>
        {data.length === 0 ? null : (
          <Card
            transparent
            style={{
              marginBottom: 0,
              paddingBottom: 0,
            }}>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                }}>
                Related Merchants
              </Text>
            </CardItem>
          </Card>
        )}
        <FlatList
          scrollEnabled
          contentContainerStyle={{
            paddingLeft: 15,
            paddingTop: 5,
            paddingBottom: 5,
          }}
          data={data}
          extraData={data}
          horizontal
          showsHorizontalScrollIndicator={false}
          legacyImplementation
          disableVirtualization
          decelerationRate="fast"
          keyExtractor={keyExt}
          listKey={keyExt}
          renderItem={({item, index}) => {
            return <RelatedMerchantCard navigation={navigation} item={item} />;
          }}
        />
      </>
    );
  } else {
    return null;
  }
};

export const RelatedMerchantCard = props => {
  const {item, navigation} = props;
  const {logoImageUrl: source, name, serviceType} = item;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const RenderCard = () =>
    useMemo(() => {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            navigation.push('MerchantDetail', {id: item.id, serviceType});
          }}
          onPressIn={pressIn}
          onPressOut={pressOut}
          style={{marginRight: 15}}>
          <AnimatedCard
            transparent
            style={{width: width / 4, transform: [{scale}]}}>
            <CardItem cardBody style={{backgroundColor: 'transparent'}}>
              <AsyncImage
                style={{
                  width: width / 4,
                  height: height / 8,
                  aspectRatio: width / 4 / (height / 8),
                  borderRadius: 4,
                  borderWidth: 0.5,
                  borderColor: greyLine,
                  backgroundColor: 'white',
                }}
                source={{uri: source}}
                placeholderColor={newContainerColor}
                loaderStyle={{
                  width: width / 7,
                  height: height / 7,
                }}
                resizeMode="cover"
              />
            </CardItem>
            <CardItem
              style={{
                backgroundColor: 'transparent',
                paddingLeft: 5,
                width: '100%',
                flexDirection: 'column',
                flexWrap: 'wrap',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  marginBottom: 5,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.5),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {name}
              </Text>
              <Text
                style={{
                  textAlign: 'left',
                  fontFamily: book,
                  fontSize: RFPercentage(1.5),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {serviceType}
              </Text>
            </CardItem>
          </AnimatedCard>
        </TouchableOpacity>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(About);

export default props => <Wrapper {...props} />;
