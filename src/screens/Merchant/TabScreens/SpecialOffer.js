import React, {useState, useEffect} from 'react';
import {FlatList, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

// QUERY GET Special Offer from Query Get Merchant Detail
import GET_MERCHANT_DETAIL from '../../../graphql/queries/getMerchantVer2';

// Common Screen
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import EmptyTabScreen from '../../../components/Cards/Merchants/EmptyTabScreen';
import SpecialOfferCard from '../../../components/Cards/Merchants/SpecialOfferCard';

// Shimmer
import SpecialOfferShimmer from '../../../components/Loader/Merchant/SpecialOfferShimmer';

const {white} = Colors;
const {width} = Dimensions.get('window');

const SpecialOffer = props => {
  console.log('SpecialOffer Props: ', props);
  const {settingHeightTab, heights, route, navigation, client} = props;
  const {params} = route;
  const {id} = params;

  const [isLoadingSpecialOffer, setIsLoadingSpecialOffer] = useState(true);
  const [isErrorSpecialOffer, setIsErrorSpecialOffer] = useState(false);

  const [specialOfferList, setSpecialOfferList] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('tabPress', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, heights]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('SPEICAL OFFER >> Merchant Detail Response: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors === undefined) {
            const {promotions} = getMerchantVer2[0];
            if (promotions) {
              await setSpecialOfferList([...promotions]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            } else {
              await setSpecialOfferList([]);
              await setIsErrorSpecialOffer(false);
              await setIsLoadingSpecialOffer(false);
            }
          } else {
            await setIsErrorSpecialOffer(true);
            await setIsLoadingSpecialOffer(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorSpecialOffer(true);
          await setIsLoadingSpecialOffer(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorSpecialOffer(true);
      setIsLoadingSpecialOffer(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height: layoutHeight} = nativeEvent;
    settingHeightTab(layoutHeight);
  };

  const extKey = item => `${String(item.id)} Parent`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 1.1,
      offset: (width / 1.1) * index,
      index,
    };
  };

  const reload = async () => {
    await setIsErrorSpecialOffer(false);
    await setIsLoadingSpecialOffer(true);
    await fetchMerchantDetail();
  };

  if (isLoadingSpecialOffer && !isErrorSpecialOffer) {
    return <SpecialOfferShimmer />;
  }

  if (!isLoadingSpecialOffer && isErrorSpecialOffer) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  if (
    !isLoadingSpecialOffer &&
    !isErrorSpecialOffer &&
    specialOfferList.length === 0
  ) {
    return (
      <EmptyTabScreen
        heights={heights}
        onLayout={onLayout}
        message="Currently no special offer available. Don’t worry we’ll notify you soon!"
      />
    );
  }

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <FlatList
        scrollEnabled={false}
        disableVirtualization
        legacyImplementation
        onLayout={onLayout}
        data={specialOfferList}
        keyExtractor={extKey}
        listKey={extKey}
        getItemLayout={getItemLayout}
        contentContainerStyle={{padding: 15, alignItems: 'center'}}
        renderItem={({item}) => {
          const {promoImageURL, promoImageDynamicURL, promoLinkURL} = item;
          const source = promoImageDynamicURL
            ? {uri: `${promoImageDynamicURL}=h150`}
            : {uri: promoImageURL};
          const disabled = promoLinkURL && promoLinkURL !== '' ? false : true;
          return (
            <SpecialOfferCard
              source={source}
              disabled={disabled}
              promoLinkURL={promoLinkURL}
              navigation={navigation}
            />
          );
        }}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(SpecialOffer);

export default props => <Wrapper {...props} />;