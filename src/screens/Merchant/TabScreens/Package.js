// This oly for Venue

import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';

// Product Query (this will be show o Venue service)
import PRODUCT_BY_MERCHANT_QUERY from '../../../graphql/queries/productByMerchant';

// Common Screen
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import ButtonExploreAll from '../../../components/Button/buttonExploreAll';
import PackageCard from '../../../components/Cards/Merchants/PackageCard';
import EmptyTabScreen from '../../../components/Cards/Merchants/EmptyTabScreen';

// Shimmer
import PackageShimmer from '../../../components/Loader/Merchant/SpecialOfferShimmer';

const {white} = Colors;

const Package = props => {
  console.log('Package Props: ', props);
  const {
    settingHeightTab,
    heights,
    client,
    route,
    navigation,
    activeTab,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [list, setList] = useState([]);
  const pageSize = 4;
  const pageNumber = 1;

  useEffect(() => {
    fetchPackageList();
    const subscriber = navigation.addListener('tabPress', () => {
      fetchPackageList();
    });
    return subscriber;
  }, [navigation, heights, activeTab, onLayout]);

  const fetchPackageList = () => {
    try {
      client
        .query({
          query: PRODUCT_BY_MERCHANT_QUERY,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType: String(serviceType),
            bridalType: [],
            bridalCategory: [],
            bridalColor: [],
            jewelleryType: [],
            smartHomeCategory: [],
            pageSize,
            pageNumber,
            accsCategory: [],
            accsDetails: [],
            accsRange: [],
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Package Response: ', response);
          const {data, errors} = response;
          const {productsByMerchant} = data;
          const {data: listPackage, error} = productsByMerchant;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setList([...listPackage]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error Response Package: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height: heightContent} = nativeEvent;
    settingHeightTab(heightContent);
  };

  const reload = async () => {
    await setIsError(false);
    await setIsLoading(true);
    await fetchPackageList();
  };

  const extKey = item => `${String(item.id)} - Parent`;

  if (isLoading && !isError) {
    return <PackageShimmer />;
  }

  if (!isLoading && isError) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  if (!isLoading && !isError && list.length === 0) {
    return (
      <EmptyTabScreen
        heights={heights}
        onLayout={onLayout}
        message="Currently no package offer available. Don’t worry we’ll notify you soon!"
      />
    );
  }

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <FlatList
        onLayout={onLayout}
        ListFooterComponent={() => {
          if (list.length === 0) {
            return null;
          } else {
            return (
              <Card transparent>
                <CardItem>
                  <ButtonExploreAll
                    onPress={() => {
                      navigation.navigate('Package', {id, serviceType});
                    }}
                  />
                </CardItem>
              </Card>
            );
          }
        }}
        contentContainerStyle={{
          alignItems: 'center',
          paddingTop: 15,
          paddingBottom: 15,
        }}
        disableVirtualization
        legacyImplementation
        scrollEnabled={false}
        data={list}
        extraData={list}
        keyExtractor={extKey}
        listKey={extKey}
        renderItem={({item}) => {
          return (
            <PackageCard
              navigation={navigation}
              item={item}
              label={'Packages'}
              merchantId={id}
            />
          );
        }}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(Package);

export default props => <Wrapper {...props} />;
