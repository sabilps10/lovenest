import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, Animated, FlatList} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType} from '../../../utils/Themes/Fonts';

// Query Project
import GET_ALL_PROJECT from '../../../graphql/queries/getAllProjects';

// Common Screen
import ReloadScreen from '../../../components/Cards/Merchants/ReloadTabScreen';
import ButtonExploreAll from '../../../components/Button/buttonExploreAll';
import EmptyTabScreen from '../../../components/Cards/Merchants/EmptyTabScreen';

// Highlight Card
/*
  Thing that has Highlight is:
  Bridal,
  Venue,
  Interior Design
*/
import HighlightBridalCard from '../../../components/Cards/Merchants/HighlightBridal';
import HighlightVenueCard from '../../../components/Cards/Merchants/HighlightVenue';
import HighlightInteriorCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';

// Shimmer
import PackageShimmer from '../../../components/Loader/Merchant/SpecialOfferShimmer';

const {white, black} = Colors;
const {medium, book} = FontType;

const Hightlight = props => {
  console.log('Highlight Tab Props: ', props);
  const {settingHeightTab, heights, navigation, client, route} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [list, setList] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const pageSize = 4;
  const pageNumber = 1;

  useEffect(() => {
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return subscriber;
  }, [navigation]);

  const fetch = () => {
    try {
      client
        .query({
          query: GET_ALL_PROJECT,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType,
            itemDisplayed: parseInt(pageSize, 10),
            pageNumber,
          },
        })
        .then(async response => {
          console.log('highlight tab response: ', response);
          const {data, errors} = response;
          const {getAllProjects} = data;
          const {data: projectList, error} = getAllProjects;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setList([...projectList]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const onLayout = ({nativeEvent}) => {
    console.log('DATA LAYOUT: ', nativeEvent);
    const {height} = nativeEvent;
    settingHeightTab(height);
  };

  const reload = async () => {
    await setIsError(false);
    await setIsLoading(true);
    await fetch();
  };

  const extKey = item => `${String(item.id)} - Parent`;

  if (isLoading && !isError) {
    return <PackageShimmer />;
  }

  if (!isLoading && isError) {
    return (
      <ReloadScreen heights={heights} reload={reload} onLayout={onLayout} />
    );
  }

  if (!isLoading && !isError && list.length === 0) {
    return (
      <EmptyTabScreen
        heights={heights}
        onLayout={onLayout}
        message="Currently no highlight offer available. Don’t worry we’ll notify you soon!"
      />
    );
  }

  return (
    <Container style={{backgroundColor: white, height: heights}}>
      <FlatList
        data={list}
        extraData={list}
        listKey={extKey}
        keyExtractor={extKey}
        legacyImplementation
        disableVirtualization
        scrollEnabled={false}
        onLayout={onLayout}
        contentContainerStyle={{
          alignItems: 'center',
          paddingTop: 15,
          paddingBottom: 15,
        }}
        ListFooterComponent={() => {
          if (list.length === 0) {
            return null;
          } else {
            return (
              <Card transparent>
                <CardItem>
                  <ButtonExploreAll
                    onPress={() => {
                      navigation.navigate('Highlight', {id, serviceType});
                    }}
                  />
                </CardItem>
              </Card>
            );
          }
        }}
        renderItem={({item, _}) => {
          if (serviceType === 'Bridal') {
            return (
              <HighlightBridalCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else if (serviceType === 'Venue') {
            return (
              <HighlightVenueCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else if (serviceType === 'Interior Design') {
            return (
              <HighlightInteriorCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else {
            return null;
          }
        }}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(Hightlight);

export default props => <Wrapper {...props} />;
