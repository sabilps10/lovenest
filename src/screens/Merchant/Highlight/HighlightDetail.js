import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Animated,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  Platform,
  Image,
  RefreshControl,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Footer,
  FooterTab,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AsyncImage from '../../../components/Image/AsyncImage';
import Colors from '../../../utils/Themes/Colors';
import Loader from '../../../components/Loader/LoaderWithContainer';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {FontType} from '../../../utils/Themes/Fonts';
import {hasNotch} from 'react-native-device-info';
import {charImage} from '../../../utils/Themes/Images';

const {
  white,
  black,
  greyLine,
  newContainerColor,
  lightSalmon,
  mainGreen,
} = Colors;
const {width, height} = Dimensions.get('window');
const {book, medium} = FontType;
const heightSlider = height / 1.58;
const {charRightShuriken} = charImage;

const AnimatedCard = Animated.createAnimatedComponent(Card);

// Query Get Project (Highlight)
import GET_PROJECT from '../../../graphql/queries/getProject';
import SimilarProject from './RelatedProject';
import SpecialOffer from '../../Menu/Bridal/BridalDetail/Components/SpecialOffer';

const HighlightDetail = props => {
  console.log('HighlightDetail Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {productId, merchantId} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [refreshing, onRefresh] = useState(false);

  const [dataHighlight, setDataHighlight] = useState(null);

  useEffect(() => {
    fetchHighlight();
  }, [navigation]);

  const pullOnRefresh = async () => {
    await onRefresh(true);
    await fetchHighlight();
  };

  const fetchHighlight = () => {
    try {
      client
        .query({
          query: GET_PROJECT,
          variables: {
            id: parseInt(productId, 10),
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('GET HIGHLIGH DETAIL RESPONSE: ', response);
          const {data, errors} = response;
          const {getProject} = data;
          const {data: highlight, error} = getProject;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await onRefresh(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await onRefresh(false);
            } else {
              await setDataHighlight(highlight);
              await setIsError(false);
              await setIsLoading(false);
              await onRefresh(false);
            }
          }
        })
        .catch(error => {
          console.log('Error fetch highlight: ', error);
          setIsError(true);
          setIsLoading(false);
          onRefresh(false);
        });
    } catch (error) {
      console.log('Error fetch highlight: ', error);
      setIsError(true);
      setIsLoading(false);
      onRefresh(false);
    }
  };

  if (isLoading && !isError) {
    return <Loader />;
  }

  return (
    <Container style={{backgroundColor: 'white'}}>
      <StatusBar translucent={false} backgroundColor={newContainerColor} />
      <CricleButtonBack navigation={navigation} />
      <Content
        contentContainerStyle={{paddingBottom: 15}}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => pullOnRefresh()}
          />
        }>
        <ProductImageSlider gallery={dataHighlight?.galleries} {...props} />
        <HeaderTitleAndDescription data={dataHighlight} />
        {dataHighlight.type === 'Interior Design' ? (
          <ContentHighlightInterior data={dataHighlight} />
        ) : null}
        {/* <ListRelatedMerchant
          data={dataHighlight.featuredMerchant}
          navigation={navigation}
        /> */}
        {dataHighlight.type === 'Interior Design' ? (
          <SimilarProject
            projectId={dataHighlight.id}
            merchantName={dataHighlight.merchantDetails.name}
            isLoadingParent={isLoading}
            serviceType={dataHighlight.merchantDetails.serviceType}
            merchantId={dataHighlight.merchantDetails.id}
            data={dataHighlight}
            {...props}
          />
        ) : null}
        {/* <SpecialOffer
          isLoading={refreshing}
          {...props}
          id={dataHighlight.merchantDetails.id}
        /> */}
        <MerchantBaseOnProduct {...props} data={dataHighlight} />
      </Content>
      <FooterEnquiry dataProduct={dataHighlight} navigation={navigation} />
    </Container>
  );
};

export const MerchantBaseOnProduct = props => {
  console.log('MerchantBaseOnProduct: ', props);
  const {data, navigation} = props;
  const {merchantDetails} = data;
  const source = merchantDetails?.logoImageDynamicUrl
    ? {uri: `${merchantDetails.logoImageDynamicUrl}=h500`}
    : {uri: merchantDetails.logoImageUrl};
  const name = merchantDetails?.name ? merchantDetails.name : 'N/A';
  const tagline = merchantDetails?.tagline ? merchantDetails.tagline : 'N/A';
  return (
    <View>
      <LinearGradient
        start={{x: 0.0, y: 0.3}}
        end={{x: 0.0, y: 8}}
        locations={[0.0, 0.2]}
        colors={['#CAB77C', 'white']}
        style={{
          marginTop: 15,
          marginBottom: 15,
          // minHeight: height / 5.5,
          width: '100%',
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingTop: 15,
          paddingBottom: 15,
        }}>
        <Image
          source={charRightShuriken}
          style={{
            width: width / 10,
            height: height / 17,
            position: 'absolute',
            right: 10,
            top: 20,
            zIndex: 1,
          }}
        />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            paddingLeft: 15,
            paddingRight: 15,
            paddingTop: 5,
            paddingBottom: 5,
          }}>
          <View
            style={{
              flex: 0.39,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <View
              style={{
                width: width / 4.5,
                height: height / 9.8,
                backgroundColor: 'white',
                borderRadius: 4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={source}
                style={{width: '100%', height: height / 13}}
                resizeMode="contain"
              />
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flexWrap: 'wrap',
                marginBottom: 5,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: 'white',
                  letterSpacing: 0.3,
                }}>
                {name}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flexWrap: 'wrap',
                marginBottom: 5,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: book,
                  fontSize: RFPercentage(1.6),
                  color: 'white',
                  letterSpacing: 0.3,
                }}>
                {tagline}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                position: 'absolute',
                bottom: 0,
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (merchantDetails.serviceType === 'Bridal') {
                    navigation.push('BridalDetail', {
                      id: merchantDetails.id,
                      serviceType: merchantDetails.serviceType,
                    });
                  } else if (merchantDetails.serviceType === 'Jewellery') {
                    navigation.push('JewelleryMerchantDetail', {
                      id: merchantDetails.id,
                      serviceType: merchantDetails.serviceType,
                    });
                  } else if (merchantDetails.serviceType === 'Venue') {
                    navigation.push('VenueMerchantDetail', {
                      id: merchantDetails.id,
                      serviceType: merchantDetails.serviceType,
                    });
                  } else if (
                    merchantDetails.serviceType === 'Interior Design'
                  ) {
                    navigation.push('InteriorMerchantDetail', {
                      id: merchantDetails.id,
                      serviceType: merchantDetails.serviceType,
                    });
                  }
                }}
                style={{
                  // minWidth: width / 5,
                  // minHeight: height / 20,
                  borderWidth: 1,
                  borderColor: 'white',
                  flexDirection: 'row',
                  padding: 5,
                  paddingLeft: 10,
                  paddingRight: 10,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.5),
                    color: 'white',
                    letterSpacing: 0.3,
                    marginRight: 3,
                  }}>
                  Explore
                </Text>
                <View
                  style={{
                    marginLeft: 5,
                    borderWidth: 1,
                    borderColor: 'white',
                    borderRadius: 20 / 2,
                    height: 20,
                    width: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    type="Feather"
                    name="arrow-right"
                    style={{fontSize: 13, color: 'white'}}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export const ContentHighlightInterior = props => {
  console.log('ContentHighlightInterior: ', props);
  const {data} = props;
  const {
    interiorRoomType,
    interiorRoomSize,
    interiorRoomStyle,
    interiorWorkDuration,
  } = data;
  return (
    <Card
      transparent
      style={{
        marginTop: 10,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          Specification
        </Text>
      </CardItem>
      {/* <CardItem
        style={{
          width: '100%',
          backgroundColor: 'transparent',
          justifyContent: 'flex-start',
          alignItems: 'center',
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}>
        <View style={{width: 25}}>
          <Icon
            type="Feather"
            name="tag"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: RFPercentage(2),
              color: black,
            }}
          />
        </View>
        <View
          style={{
            borderRightWidth: 1,
            borderRightColor: greyLine,
            paddingRight: 15,
            marginRight: 15,
          }}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {interiorRoomType}
          </Text>
        </View>
        <View style={{width: 25}}>
          <Icon
            type="Feather"
            name="layout"
            style={{
              marginLeft: 0,
              marginRight: 0,
              fontSize: RFPercentage(2),
              color: black,
            }}
          />
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Text
            style={{
              fontFamily: book,
              fontSize: RFPercentage(1.8),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {interiorRoomSize} m
          </Text>
          <Text
            style={{
              bottom: 4,
              right: -2,
              fontFamily: book,
              fontSize: RFPercentage(1.2),
              color: black,
              letterSpacing: 0.3,
            }}>
            2
          </Text>
        </View>
      </CardItem> */}
      <ListItemProduct label={'Category'} content={interiorRoomType} />
      <ListItemProduct label={'Area'} content={`${interiorRoomSize} m`} />
      <ListItemProduct
        label={'Work Duration'}
        content={`${interiorWorkDuration} Weeks`}
      />
      <ListItemProduct label={'Style'} content={interiorRoomStyle} />
    </Card>
  );
};

export const ListItemProduct = props => {
  const {label, content} = props;

  return (
    <CardItem style={{width: '100%', backgroundColor: 'transparent'}}>
      <View
        style={{
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          flex: 0.6,
          height: '100%',
          minHeight: 30,
          paddingBottom: 10,
        }}>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.6),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {label}
        </Text>
      </View>
      <View
        style={{
          minHeight: 30,
          height: '100%',
          flex: 1,
          borderBottomColor: greyLine,
          borderBottomWidth: 0.5,
          justifyContent: 'flex-end',
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingBottom: 10,
        }}>
        {label === 'Area' ? (
          <>
            <Text
              style={{
                fontFamily: book,
                fontSize: RFPercentage(1.6),
                color: black,
                letterSpacing: 0.3,
                lineHeight: 20,
              }}>
              {content}
            </Text>
            <Text
              style={{
                bottom: 4,
                right: -2,
                fontFamily: book,
                fontSize: RFPercentage(1.2),
                color: black,
                letterSpacing: 0.3,
              }}>
              2
            </Text>
          </>
        ) : (
          <Text
            style={{
              textAlign: 'right',
              fontFamily: book,
              fontSize: RFPercentage(1.6),
              color: black,
              letterSpacing: 0.3,
              lineHeight: 20,
            }}>
            {content}
          </Text>
        )}
      </View>
    </CardItem>
  );
};

export const ListRelatedMerchant = props => {
  const {data, navigation} = props;
  if (data) {
    const keyExt = (item, index) => `${String(item.id)} Related Merchant`;
    return (
      <>
        {data.length === 0 ? null : (
          <Card
            style={{
              marginTop: 10,
              marginLeft: 0,
              marginRight: 0,
              shadowOpacity: 0,
              elevation: 0,
              marginBottom: 0,
              paddingBottom: 0,
            }}>
            <CardItem>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: black,
                }}>
                Featured Merchant(s)
              </Text>
            </CardItem>
            <FlatList
              scrollEnabled
              contentContainerStyle={{
                paddingLeft: 15,
                paddingTop: 5,
                paddingBottom: 5,
              }}
              data={data}
              extraData={data}
              horizontal
              initialNumToRender={3}
              showsHorizontalScrollIndicator={false}
              legacyImplementation
              disableVirtualization
              decelerationRate="fast"
              keyExtractor={keyExt}
              listKey={keyExt}
              renderItem={({item, index}) => {
                return (
                  <RelatedMerchantCard navigation={navigation} item={item} />
                );
              }}
            />
          </Card>
        )}
      </>
    );
  } else {
    return null;
  }
};

export const RelatedMerchantCard = props => {
  const {item, navigation} = props;
  const {logoImageUrl: source, name, serviceType} = item;

  const animation = new Animated.Value(0);
  const inputRange = [0, 1];
  const outputRange = [1, 0.8];
  const scale = animation.interpolate({inputRange, outputRange});

  const pressIn = () => {
    Animated.spring(animation, {
      toValue: 0.3,
      useNativeDriver: true,
    }).start();
  };

  const pressOut = () => {
    Animated.spring(animation, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };

  const RenderCard = () =>
    useMemo(() => {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            navigation.push('MerchantDetail', {id: item.id, serviceType});
          }}
          onPressIn={pressIn}
          onPressOut={pressOut}
          style={{marginRight: 15}}>
          <AnimatedCard
            transparent
            style={{width: width / 4, transform: [{scale}]}}>
            <CardItem cardBody style={{backgroundColor: 'transparent'}}>
              <AsyncImage
                style={{
                  width: width / 4,
                  height: height / 8,
                  aspectRatio: width / 4 / (height / 8),
                  borderRadius: 4,
                  borderWidth: 0.5,
                  borderColor: greyLine,
                  backgroundColor: 'white',
                }}
                source={{uri: source}}
                placeholderColor={newContainerColor}
                loaderStyle={{
                  width: width / 7,
                  height: height / 7,
                }}
                resizeMode="cover"
              />
            </CardItem>
            <CardItem
              style={{
                backgroundColor: 'transparent',
                paddingLeft: 5,
                width: '100%',
                flexDirection: 'column',
                flexWrap: 'wrap',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  marginBottom: 5,
                  fontFamily: medium,
                  fontSize: RFPercentage(1.5),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {name}
              </Text>
              <Text
                style={{
                  textAlign: 'left',
                  fontFamily: book,
                  fontSize: RFPercentage(1.5),
                  color: black,
                  lineHeight: 18,
                  letterSpacing: 0.3,
                }}>
                {serviceType}
              </Text>
            </CardItem>
          </AnimatedCard>
        </TouchableOpacity>
      );
    }, [item]);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const HeaderTitleAndDescription = props => {
  const {data} = props;
  const {name, description} = data;
  return (
    <Card
      transparent
      style={{
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        shadowOpacity: 0,
        elevation: 0,
        borderColor: greyLine,
        borderWidth: 0.5,
        borderRadius: 0,
      }}>
      <CardItem>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(2),
            color: black,
            letterSpacing: 0.39,
            lineHeight: 25,
          }}>
          {name}
        </Text>
      </CardItem>
      <CardItem>
        <Text
          style={{
            fontFamily: book,
            fontSize: RFPercentage(1.7),
            color: black,
            letterSpacing: 0.3,
            lineHeight: 20,
          }}>
          {description}
        </Text>
      </CardItem>
    </Card>
  );
};

export const CricleButtonBack = props => {
  const {navigation} = props;

  return (
    <Button
      onPress={() => {
        navigation.goBack(null);
      }}
      style={{
        alignSelf: 'flex-end',
        paddingTop: 0,
        paddingBottom: 0,
        height: 30,
        width: 30,
        justifyContent: 'center',
        borderRadius: 30 / 2,
        backgroundColor: white,
        position: 'absolute',
        top: Platform.OS === 'ios' ? (hasNotch() ? 70 : 35) : 15,
        left: 15,
        zIndex: 999,
      }}>
      <Icon
        type="Feather"
        name="chevron-left"
        style={{marginLeft: 0, marginRight: 0, fontSize: 25, color: black}}
      />
    </Button>
  );
};

export const ProductImageSlider = props => {
  const {gallery, navigation} = props;
  const scrollX = new Animated.Value(0);
  const keyExt = (item, index) => `${String(item.id)} Images`;
  const RenderSlider = () =>
    useMemo(() => {
      return (
        <>
          <FlatList
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      x: scrollX,
                    },
                  },
                },
              ],
              {useNativeDriver: false},
            )}
            initialNumToRender={3}
            legacyImplementation
            disableVirtualization
            decelerationRate="fast"
            data={gallery}
            extraData={gallery}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            pagingEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            initialScrollIndex={0}
            renderItem={({item, index}) => {
              return <CardImage item={item} />;
            }}
          />
          <View
            style={{
              position: 'relative',
              zIndex: 10,
              bottom: 50,
              right: 0,
              left: 0,
              width: '100%',
              flexDirection: 'row',
              paddingLeft: 15,
              paddingRight: 15,
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {gallery.map((_, idx) => {
                let color = Animated.divide(
                  scrollX,
                  Dimensions.get('window').width,
                ).interpolate({
                  inputRange: [idx - 1, idx, idx + 1],
                  outputRange: [greyLine, white, greyLine],
                  extrapolate: 'clamp',
                });
                return (
                  <>
                    {gallery.length > 7 ? (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 7,
                          width: 7,
                          backgroundColor: color,
                          margin: 3,
                          borderRadius: 7 / 2,
                        }}
                      />
                    ) : (
                      <Animated.View
                        key={String(idx)}
                        style={{
                          height: 9,
                          width: 9,
                          backgroundColor: color,
                          margin: 4,
                          borderRadius: 9 / 2,
                        }}
                      />
                    )}
                  </>
                );
              })}
            </View>
            <View
              style={{
                flex: 0.3,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  navigation.navigate('ProductGallery', {
                    images: gallery,
                  });
                }}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: white,
                  paddingLeft: 10,
                  paddingRight: 10,
                  height: height / 25,
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  View All
                </Text>
              </Button>
            </View>
          </View>
        </>
      );
    }, [gallery]);
  if (gallery) {
    return RenderSlider();
  } else {
    return null;
  }
};

export const CardImage = props => {
  const {item} = props;
  const {url, dynamicUrl} = item;
  const logoSource = dynamicUrl ? {uri: `${dynamicUrl}=h800`} : {uri: url};
  const RenderCard = () =>
    useMemo(() => {
      return (
        <Card
          style={{
            elevation: 0,
            shadowOpacity: 0,
            marginLeft: 0,
            marginTop: 0,
            marginBottom: 0,
            marginRight: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            width,
          }}>
          <CardItem cardBody>
            <AsyncImage
              resizeMode={'cover'}
              style={{
                shadowOpacity: 0,
                elevation: 0,
                width,
                height: heightSlider,
                aspectRatio: width / heightSlider,
              }}
              loaderStyle={{
                width: width / 7,
                height: height / 7,
              }}
              source={logoSource}
              placeholderColor={greyLine}
            />
          </CardItem>
        </Card>
      );
    }, [item]);

  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

export const FooterEnquiry = props => {
  const {dataProduct, navigation} = props;

  return (
    <Footer>
      <FooterTab style={{backgroundColor: mainGreen}}>
        <View style={{backgroundColor: mainGreen, width: '100%'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Enquiry', {
                serviceType: dataProduct.type,
                id: dataProduct.merchantDetails.id,
              });
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.8),
                color: white,
                letterSpacing: 0.3,
              }}>
              SUBMIT ENQUIRY
            </Text>
          </TouchableOpacity>
        </View>
      </FooterTab>
    </Footer>
  );
};

const Wrapper = compose(withApollo)(HighlightDetail);

export default props => <Wrapper {...props} />;
