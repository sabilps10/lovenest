import React, {useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'native-base';
import ButtonSeeAllProduct from '../../../components/Home/PublicProduct/ButtonSeeAllProduct';
import SIMILAR_PRODUCT from '../../../graphql/queries/productSimilar';
import Loader from '../../../components/Loader/circleLoader';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';
import {commonImage} from '../../../utils/Themes/Images';
import InteriorCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';

const {mainRed} = Colors;
const {loveCrown} = commonImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const {medium} = FontType;

import GET_PUBLIC_PROJECT from '../../../graphql/queries/projectsPublic';

const SimilarProduct = props => {
  console.log('SimilarProduct Component', props);
  const {
    navigation,
    client,
    merchantId,
    serviceType,
    isLoadingParent,
    merchantName,
    projectId,
  } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [projectList, setProjectList] = useState([]);

  const [interiorType, setInteriorType] = useState([]);
  const [interiorStyle, setInteriorStyle] = useState([]);

  const [pageSize, setPageSize] = useState(2);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalData, setTotalData] = useState(0);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setIsRefreshing] = useState(false);

  useEffect(() => {
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      fetch();
    });

    return () => {
      subscriber();
    };
  }, [navigation, merchantId, serviceType, isLoadingParent]);

  const fetch = () => {
    try {
      client
        .query({
          query: GET_PUBLIC_PROJECT,
          variables: {
            serviceType,
            merchantId,
            venueType: [],
            interiorType: [],
            interiorStyle: [],
            pageSize,
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response: ', response);
          const {data, errors} = response;
          const {projectsPublic} = data;
          const {data: list, error, totalData: totalCount} = projectsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsRefreshing(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsRefreshing(false);
              await setIsLoadMore(false);
            } else {
              await setTotalData(totalCount);
              const removeListWithSameId = await list.filter(item => {
                return item.id !== projectId;
              });
              console.log('removeListWithSameId: ', removeListWithSameId);
              await setProjectList([...removeListWithSameId]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsRefreshing(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(error => {
          console.log('Error: ', error);
          setIsError(true);
          setIsLoading(false);
          setIsRefreshing(false);
          setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsRefreshing(false);
      setIsLoadMore(false);
    }
  };

  const extKey = item => `${String(item.id)} Parent`;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: widthScreen,
          height: heightScreen / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <>
        {projectList.length === 0 ? null : (
          <View
            style={{
            //   width: widthScreen,
            //   minHeight: heightScreen / 2.3,
              paddingBottom: 25,
              marginBottom: 15,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.15}}
              end={{x: 0.0, y: 5}}
              locations={[0.0, 0.15, 0.9]}
              colors={['#FFEDED', 'white', 'white']}
              style={{
                position: 'absolute',
                top: 0,
                zIndex: 0,
                width: '100%',
                height: '100%',
              }}
            />
            <Image
              source={loveCrown}
              style={{
                position: 'absolute',
                top: 10,
                left: 0,
                zIndex: 2,
                width: 43,
                height: 54,
              }}
              resizeMode="contain"
            />
            <ButtonSeeAllProduct
              title={`Other Projects`}
              {...props}
              disableButton={true}
            />
            <View style={{top: 45, zIndex: 4}}>
              {projectList.length === 0 ? null : (
                <FlatList
                  legacyImplementation
                  disableVirtualization
                  contentContainerStyle={{
                    padding: 15,
                    paddingBottom: 15,
                    paddingTop: 0,
                    alignItems: 'center',
                  }}
                  ListFooterComponent={() => {
                    return (
                      <View
                        style={{
                          width: '100%',
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: 10,
                          flexDirection: 'row',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            try {
                              navigation.push('InteriorDesign', {
                                serviceType,
                                merchantId,
                              });
                            } catch (error) {
                              console.log('Error: ', error);
                            }
                          }}
                          style={{
                            width: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              fontFamily: medium,
                              fontSize: RFPercentage(1.7),
                              color: mainRed,
                              letterSpacing: 0.3,
                              marginRight: 5,
                            }}>
                            Explore
                          </Text>
                          <View
                            style={{
                              width: 20,
                              height: 20,
                              borderRadius: 20 / 2,
                              borderWidth: 1,
                              borderColor: mainRed,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Icon
                              type="Feather"
                              name="arrow-right"
                              style={{fontSize: 15, color: mainRed}}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    );
                  }}
                  data={
                    projectList?.length > 1
                      ? projectList.splice(1)
                      : projectList
                  }
                  extraData={
                    projectList?.length > 1
                      ? projectList.splice(1)
                      : projectList
                  }
                  keyExtractor={extKey}
                  renderItem={({item}) => {
                    return (
                      <InteriorCard
                        item={item}
                        label={serviceType}
                        navigation={navigation}
                      />
                    );
                  }}
                />
              )}
            </View>
          </View>
        )}
      </>
    );
  }
};

const Wrapper = compose(withApollo)(SimilarProduct);

export default props => <Wrapper {...props} />;
