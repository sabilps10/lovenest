import React, {useState, useEffect} from 'react';
import {Text, FlatList, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Button, Icon, Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
// Query Get All Project (highlight)
import GET_ALL_PROJECT from '../../../graphql/queries/getAllProjects';

// Component
import ButtonBack from '../../../components/Button/buttonBack';

// Highlight Card
import HighlightBridalCard from '../../../components/Cards/Merchants/HighlightBridal';
import HighlightVenueCard from '../../../components/Cards/Merchants/HighlightVenue';
import HighlightInteriorCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';

// Loader or Shimmer
import HighlightShimmer from '../../../components/Loader/Merchant/SpecialOfferShimmer';

// Commoin Screen
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';

const {greyLine, black, mainRed} = Colors;
const {regular} = FontSize;
const {medium} = FontType;
const {height} = Dimensions.get('window');

const ShortHighlight = props => {
  console.log('ShortHighlight Props: ', props);
  const {navigation, client, route, showShort} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);

  const [pageSize, setPageSize] = useState(3);
  const pageNumber = 1;

  const [, setIsLoadMore] = useState(false);
  const [, setIsPullRefresh] = useState(false);

  const [, setTotalCount] = useState(0);

  useEffect(() => {
    navigationOptions();
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      fetch();
    });

    return subscriber;
  }, [navigation]);

  const fetch = () => {
    try {
      client
        .query({
          query: GET_ALL_PROJECT,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType,
            itemDisplayed: parseInt(pageSize, 10),
            pageNumber,
          },
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('highlight list screen response: ', response);
          const {data, errors} = response;
          const {getAllProjects} = data;
          const {data: projectList, error, totalData} = getAllProjects;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsPullRefresh(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setList([...projectList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsPullRefresh(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsPullRefresh(false);
      setIsLoadMore(false);
    }
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Highlights',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const reload = async () => {
    await setIsError(false);
    await setIsLoading(true);
    await fetch();
  };

  const extKey = item => `${String(item.id)} - Parent`;

  if (isLoading && !isError) {
    return <HighlightShimmer />;
  }

  if (!isLoading && isError) {
    return (
      <Container>
        <ErrorScreen
          message="Refresh"
          onPress={() => {
            reload();
          }}
        />
      </Container>
    );
  }

  return (
    <>
      {showShort ? (
        <>
          {list.length === 0 ? null : (
            <Card transparent style={{marginLeft: 0, marginRight: 0, marginTop: 0}}>
              {list.length > 0 ? (
                <CardItem
                  style={{
                    paddingTop: 0,
                    paddingBottom: 0,
                    width: '100%',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingLeft: 20,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.8),
                      color: black,
                      letterSpacing: 0.3,
                    }}>
                    Highlights
                  </Text>
                  <Button
                    onPress={() => {
                      navigation.navigate('Highlight', {serviceType, id});
                    }}
                    style={{
                      backgroundColor: 'transparent',
                      elevation: 0,
                      shadowOpacity: 0,
                      height: height / 20,
                      paddingLeft: 10,
                      paddingRight: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={{
                        right: -15,
                        fontFamily: medium,
                        fontSize: RFPercentage(1.6),
                        color: mainRed,
                        letterSpacing: 0.3,
                      }}>
                      See All
                    </Text>
                    <Icon
                      type="Feather"
                      name="arrow-right"
                      style={{right: -10, fontSize: 18, color: mainRed}}
                    />
                  </Button>
                </CardItem>
              ) : null}
              <FlatList
                decelerationRate="fast"
                data={list}
                extraData={list}
                listKey={extKey}
                keyExtractor={extKey}
                legacyImplementation
                disableVirtualization
                initialNumToRender={3}
                scrollEnabled={false}
                contentContainerStyle={{
                  alignItems: 'center',
                  paddingBottom: 15,
                }}
                renderItem={({item}) => {
                  if (serviceType === 'Bridal') {
                    return (
                      <HighlightBridalCard
                        item={item}
                        label={serviceType}
                        navigation={navigation}
                      />
                    );
                  } else if (serviceType === 'Venue') {
                    return (
                      <HighlightVenueCard
                        item={item}
                        label={serviceType}
                        navigation={navigation}
                      />
                    );
                  } else if (serviceType === 'Interior Design') {
                    return (
                      <HighlightInteriorCard
                        item={item}
                        label={serviceType}
                        navigation={navigation}
                      />
                    );
                  } else {
                    return null;
                  }
                }}
              />
            </Card>
          )}
        </>
      ) : null}
    </>
  );
};

const Wrapper = compose(withApollo)(ShortHighlight);

export default props => <Wrapper {...props} />;
