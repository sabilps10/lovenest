import React, {useState, useEffect} from 'react';
import {FlatList, RefreshControl, Dimensions} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
// Query Get All Project (highlight)
import GET_ALL_PROJECT from '../../../graphql/queries/getAllProjects';

// Component
import ButtonBack from '../../../components/Button/buttonBack';

// Highlight Card
import HighlightBridalCard from '../../../components/Cards/Merchants/HighlightBridal';
import HighlightVenueCard from '../../../components/Cards/Merchants/HighlightVenue';
import HighlightInteriorCard from '../../../components/Cards/Merchants/HighlightInteriorDesign';

// Loader or Shimmer
import HighlightShimmer from '../../../components/Loader/Merchant/SpecialOfferShimmer';

// Commoin Screen
import ErrorScreen from '../../../components/ErrorScreens/NotLoginYet';

const {greyLine, black} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

const Highlight = props => {
  console.log('Highlight Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);

  const [pageSize, setPageSize] = useState(10);
  const pageNumber = 1;

  const [, setIsLoadMore] = useState(false);
  const [isPullRefresh, setIsPullRefresh] = useState(false);

  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    navigationOptions();
    fetch();
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      fetch();
    });

    return subscriber;
  }, [navigation]);

  const fetch = () => {
    try {
      client
        .query({
          query: GET_ALL_PROJECT,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType,
            itemDisplayed: parseInt(pageSize, 10),
            pageNumber,
          },
        })
        .then(async response => {
          console.log('highlight list screen response: ', response);
          const {data, errors} = response;
          const {getAllProjects} = data;
          const {data: projectList, error, totalData} = getAllProjects;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsPullRefresh(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setList([...projectList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsPullRefresh(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsPullRefresh(false);
      setIsLoadMore(false);
    }
  };

  const settingIsLoadMore = async status => {
    await setIsLoadMore(status);
    await setPageSize(pageSize + 20);
    await fetch();
  };

  const settingPullOnRefresh = async () => {
    await setIsPullRefresh(true);
    await setPageSize(10);
    await fetch();
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Highlights',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const reload = async () => {
    await setIsError(false);
    await setIsLoading(true);
    await fetch();
  };

  const extKey = item => `${String(item.id)} - Parent`;

  if (isLoading && !isError) {
    return <HighlightShimmer />;
  }

  if (!isLoading && isError) {
    return (
      <Container>
        <ErrorScreen
          message="Refresh"
          onPress={() => {
            reload();
          }}
        />
      </Container>
    );
  }

  return (
    <Container style={{backgroundColor: 'white'}}>
      <FlatList
        decelerationRate="fast"
        data={list}
        extraData={list}
        listKey={extKey}
        keyExtractor={extKey}
        legacyImplementation
        disableVirtualization
        initialNumToRender={3}
        scrollEnabled
        refreshControl={
          <RefreshControl
            refreshing={isPullRefresh}
            onRefresh={() => settingPullOnRefresh()}
          />
        }
        onEndReachedThreshold={0.01}
        onEndReached={() => {
          if (pageSize <= totalCount) {
            settingIsLoadMore(true);
          } else {
            settingIsLoadMore(false);
          }
        }}
        contentContainerStyle={{
          alignItems: 'center',
          paddingTop: 15,
          paddingBottom: 15,
        }}
        renderItem={({item}) => {
          if (serviceType === 'Bridal') {
            return (
              <HighlightBridalCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else if (serviceType === 'Venue') {
            return (
              <HighlightVenueCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else if (serviceType === 'Interior Design') {
            return (
              <HighlightInteriorCard
                item={item}
                label={serviceType}
                navigation={navigation}
              />
            );
          } else {
            return null;
          }
        }}
      />
    </Container>
  );
};

const Wrapper = compose(withApollo)(Highlight);

export default props => <Wrapper {...props} />;
