// This Component Deprecated

/*
  HIGHLIGHT is Project
  -Bridal
  -Interior
  -Venue

  PRODUCT
  Merchant that has Product
  1. Bridal (Products)
  2. Venue (Packages) -> Filter Product
  3. Smart Home (Products)
  4. Jewellery (Products)

  Merchant that does not have product
  1. Interior Design

  Cafe still comming soon
*/

import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Platform,
  Animated,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../utils/Themes/Fonts';
import Colors from '../../utils/Themes/Colors';
import {Container, Footer, FooterTab} from 'native-base';
import {hasNotch} from 'react-native-device-info';
import GET_MERCHANT_DETAIL from '../../graphql/queries/getMerchantVer2';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

// Error Screen
import ErrorScreen from '../../components/ErrorScreens/NotLoginYet';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Small Components
import HeaderDeafult from '../../components/Header/Merchant/HeaderDefault';
import HeaderImage from '../../components/Header/Merchant/HeaderImage';
import CircleButton from '../../components/Button/circleButton';
import HeaderTop from '../../components/Header/Merchant/HeaderTop';

// Loader Shimmer
import MerchantDetailShimmer from '../../components/Loader/Merchant/MerchantDetailShimmer';

// Redux
import {connect} from 'react-redux';
import ReduxResetProductFilter from '../../redux/thunk/ResetProductFilterThunk';

// Tab Bar
import TabBar from './TabBar';

// Tab Screens
import Highlight from './TabScreens/Highlight';
import SpecialOffer from './TabScreens/SpecialOffer';
import Product from './TabScreens/Product';
import Package from './TabScreens/Package';
import About from './TabScreens/About';
import Contact from './TabScreens/Contact';

const {white, transparent, newContainerColor, lightSalmon} = Colors;
const {medium} = FontType;

const {width, height} = Dimensions.get('window');
const HEADER_HEIGHT = hasNotch() ? 80 : 60;
const HEADER_CARD_IMAGE_HEIGHT = height / 2.94;
const SCROLL_HEIGHT = HEADER_CARD_IMAGE_HEIGHT - HEADER_HEIGHT;
const defaultHeight = height / 1.25;

const Tab = createMaterialTopTabNavigator();

const MerchantDetail = props => {
  console.log('MerchantDetail Props: ', props);
  const {navigation, client, route, resetProductFilter} = props;
  const {params} = route;
  const {serviceType: active, id} = params;

  const [scrollY] = useState(new Animated.Value(0));

  const [heights, setHeights] = useState(500);
  const [activeTab, setActiveTab] = useState(
    active === 'Venue'
      ? 'Package'
      : active === ' Interior Design'
      ? 'SpecialOffer'
      : 'Product',
  );

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [detail, setDetail] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });
    return subscriber;
  }, [navigation, id, heights, activeTab]);

  const settingActiveTab = index => {
    setActiveTab(index);
  };

  const settingHeightTab = contentTabHeight => {
    console.log('BAZENG: ', contentTabHeight);
    setHeights(contentTabHeight);
  };

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: GET_MERCHANT_DETAIL,
          variables: {
            id: parseInt(id, 10),
          },
          fetchPolicy: 'network-only', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Merchant Detail Response: ', response);
          const {data, errors} = response;
          const {getMerchantVer2} = data;

          if (errors === undefined) {
            await setDetail([...getMerchantVer2]);
            await setIsError(false);
            await setIsLoading(false);
          } else {
            await setIsError(true);
            await setIsLoading(false);
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <HeaderDeafult
          navigation={navigation}
          resetProductFilter={resetProductFilter}
        />
        <MerchantDetailShimmer />
      </Container>
    );
  } else if (!isLoading && !isError) {
    const HEADER_OPACITY = scrollY.interpolate({
      inputRange: [0.0, 0.5, SCROLL_HEIGHT],
      outputRange: [0.0, 0.5, 1.0],
    });

    const tabY = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT, SCROLL_HEIGHT + 1],
      outputRange: [0, 0, 1],
    });

    const imgScale = scrollY.interpolate({
      inputRange: [-25, 0],
      outputRange: [1.1, 1],
      extrapolateRight: 'clamp',
    });
    const {
      coverImageUrl,
      coverImageDynamicUrl,
      logoImageUrl,
      logoImageDynamicUrl,
      name,
      serviceType,
      tagline,
    } = detail[0];

    const imageSource =
      coverImageDynamicUrl === null
        ? {uri: coverImageUrl}
        : {uri: `${coverImageDynamicUrl}=h500`};
    const logoSource =
      logoImageDynamicUrl === null
        ? {uri: logoImageUrl}
        : {uri: `${logoImageDynamicUrl}=h200`};
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <StatusBar translucent backgroundColor={transparent} animated />
        <CircleButton
          navigation={navigation}
          resetProductFilter={resetProductFilter}
        />
        <HeaderTop
          opacity={HEADER_OPACITY}
          name={name}
          headerHeight={HEADER_HEIGHT}
        />
        <Animated.ScrollView
          bounces={true}
          decelerationRate="fast"
          scrollEventThrottle={Platform.OS === 'android' ? 16 : 1}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}
          style={{zIndex: 0}}>
          <HeaderImage
            name={name}
            tagline={tagline}
            imageSource={imageSource}
            logoSource={logoSource}
            containerStyle={{
              zIndex: -5,
              transform: [{translateY: tabY}],
              opacity: 1,
            }}
            imageBannerStyle={{
              width: width,
              height: height / 4,
              aspectRatio: width / (height / 4),
              transform: [{scale: imgScale}],
            }}
          />
          <Tab.Navigator
            lazy
            swipeEnabled={false}
            animationEnabled
            initialRouteName={activeTab}
            tabBar={tabBarProps => {
              return (
                <Animated.View
                  style={{
                    zIndex: 5,
                    position: 'relative',
                    backgroundColor: newContainerColor,
                    paddingTop: 15,
                    transform: [{translateY: tabY}],
                  }}>
                  <TabBar {...tabBarProps} />
                </Animated.View>
              );
            }}>
            {serviceType === 'Venue' ? (
              <Tab.Screen
                name="Package"
                listeners={data => {
                  const {route: tabPressRoute} = data;
                  const {name: routeNamePress} = tabPressRoute;
                  settingActiveTab(routeNamePress);
                }}
                children={childrenProps => (
                  <Package
                    {...childrenProps}
                    {...props}
                    id={id}
                    activeTab={activeTab}
                    serviceType={active}
                    settingHeightTab={settingHeightTab}
                    heights={activeTab === 'Package' ? heights : defaultHeight}
                  />
                )}
                options={{
                  tabBarLabel: 'Packages',
                }}
              />
            ) : serviceType === 'Interior Design' ? null : (
              <Tab.Screen
                name="Product"
                listeners={data => {
                  const {route: tabPressRoute} = data;
                  const {name: routeNamePress} = tabPressRoute;
                  settingActiveTab(routeNamePress);
                }}
                children={childrenProps => (
                  <Product
                    {...childrenProps}
                    {...props}
                    id={id}
                    activeTab={activeTab}
                    serviceType={active}
                    settingHeightTab={settingHeightTab}
                    heights={activeTab === 'Product' ? heights : defaultHeight}
                  />
                )}
                options={{
                  tabBarLabel: 'Products',
                }}
              />
            )}
            <Tab.Screen
              name="SpecialOffer"
              listeners={data => {
                const {route: tabPressRoute} = data;
                const {name: routeNamePress} = tabPressRoute;
                settingActiveTab(routeNamePress);
              }}
              children={childrenProps => (
                <SpecialOffer
                  {...childrenProps}
                  {...props}
                  id={id}
                  activeTab={activeTab}
                  serviceType={active}
                  settingHeightTab={settingHeightTab}
                  heights={
                    activeTab === 'SpecialOffer' ? heights : defaultHeight
                  }
                />
              )}
              options={{
                tabBarLabel: 'Special Offers',
              }}
            />
            {serviceType === 'Bridal' ||
            serviceType === 'Venue' ||
            serviceType === 'Interior Design' ? (
              <Tab.Screen
                name="Highlight"
                listeners={data => {
                  const {route: tabPressRoute} = data;
                  const {name: routeNamePress} = tabPressRoute;
                  settingActiveTab(routeNamePress);
                }}
                children={childrenProps => (
                  <Highlight
                    {...childrenProps}
                    {...props}
                    id={id}
                    activeTab={activeTab}
                    serviceType={active}
                    settingHeightTab={settingHeightTab}
                    heights={
                      activeTab === 'Highlight' ? heights : defaultHeight
                    }
                  />
                )}
                options={{
                  tabBarLabel: 'Highlights',
                }}
              />
            ) : null}
            <Tab.Screen
              name="About"
              listeners={data => {
                const {route: tabPressRoute} = data;
                const {name: routeNamePress} = tabPressRoute;
                settingActiveTab(routeNamePress);
              }}
              children={childrenProps => (
                <About
                  {...childrenProps}
                  {...props}
                  id={id}
                  activeTab={activeTab}
                  serviceType={active}
                  settingHeightTab={settingHeightTab}
                  heights={activeTab === 'About' ? heights : defaultHeight}
                />
              )}
              options={{
                tabBarLabel: 'About',
              }}
            />
            <Tab.Screen
              name="Contact"
              listeners={data => {
                const {route: tabPressRoute} = data;
                const {name: routeNamePress} = tabPressRoute;
                settingActiveTab(routeNamePress);
              }}
              children={childrenProps => (
                <Contact
                  {...childrenProps}
                  {...props}
                  id={id}
                  activeTab={activeTab}
                  serviceType={active}
                  settingHeightTab={settingHeightTab}
                  heights={activeTab === 'Contact' ? heights : defaultHeight}
                />
              )}
              options={{
                tabBarLabel: 'Contact',
              }}
            />
          </Tab.Navigator>
        </Animated.ScrollView>
        <Footer>
          <FooterTab style={{backgroundColor: lightSalmon}}>
            <View style={{backgroundColor: lightSalmon, width: '100%'}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Enquiry', {serviceType: active, id});
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: white,
                    letterSpacing: 0.3,
                  }}>
                  SUBMIT ENQUIRY
                </Text>
              </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  } else {
    return (
      <Container style={{backgroundColor: newContainerColor}}>
        <HeaderDeafult
          navigation={navigation}
          resetProductFilter={resetProductFilter}
        />
        <ErrorScreen
          message="Refresh"
          onPress={() => {
            fetchMerchantDetail();
          }}
        />
      </Container>
    );
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {};
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    resetProductFilter: () => dispatch(ReduxResetProductFilter()),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MerchantDetail);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
