import React, {useRef, useMemo} from 'react';
import {Text, View, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {RFPercentage} from 'react-native-responsive-fontsize';

const {black, greyLine, lightSalmon} = Colors;
const {medium} = FontType;

const TabBarMerchant = props => {
  console.log('TabBarMerchant Props: ', props);
  const {state, descriptors, navigation} = props;
  const {routes, index: activeIndex} = state;
  const tabBarRef = useRef();

  const getItemLayout = (data, index) => {
    // Max 5 items visibles at once
    return {
      length: Dimensions.get('window').width / 3,
      offset: (Dimensions.get('window').width / 3) * index,
      index,
    };
  };

  const keyExt = (_, index) => `${String(index)} Tab Bar`;
  const RenderTabBar = () =>
    useMemo(() => {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 50,
            width: Dimensions.get('window').width,
            backgroundColor: 'white',
            borderBottomColor: greyLine,
            borderBottomWidth: 0,
            borderTopWidth: 0.5,
            borderTopColor: greyLine,
          }}>
          <FlatList
            bounces={false}
            ref={tabBarRef}
            data={routes}
            getItemLayout={getItemLayout}
            snapToAlignment={'center'}
            snapToInterval={Dimensions.get('window').width / 3}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled
            keyExtractor={keyExt}
            listKey={keyExt}
            renderItem={({item: route, index}) => {
              const {options} = descriptors[route.key];
              const label =
                options.tabBarLabel !== undefined
                  ? options.tabBarLabel
                  : options.title !== undefined
                  ? options.title
                  : route.name;

              const isFocused = state.index === index;

              const onPress = () => {
                const event = navigation.emit({
                  type: 'tabPress',
                  target: route.key,
                  canPreventDefault: true,
                });

                if (!isFocused && !event.defaultPrevented) {
                  tabBarRef.current.scrollToIndex({
                    animated: true,
                    index,
                    viewOffset: Dimensions.get('window').width / 2.15,
                  });
                  navigation.navigate(route.name);
                }
              };

              const onLongPress = () => {
                navigation.emit({
                  type: 'tabLongPress',
                  target: route.key,
                });
              };

              const color =
                label === 'Products'
                  ? isFocused
                    ? lightSalmon
                    : black
                  : label === 'Special Offers'
                  ? isFocused
                    ? lightSalmon
                    : black
                  : label === 'Highlights'
                  ? isFocused
                    ? lightSalmon
                    : black
                  : label === 'About'
                  ? isFocused
                    ? lightSalmon
                    : black
                  : label === 'Contact'
                  ? isFocused
                    ? lightSalmon
                    : black
                  : isFocused
                  ? lightSalmon
                  : black;

              const borderBottomWidth =
                label === 'Products'
                  ? isFocused
                    ? 4
                    : 0
                  : label === 'Special Offers'
                  ? isFocused
                    ? 4
                    : 0
                  : label === 'Highlights'
                  ? isFocused
                    ? 4
                    : 0
                  : label === 'About'
                  ? isFocused
                    ? 4
                    : 0
                  : label === 'Contact'
                  ? isFocused
                    ? 4
                    : 0
                  : isFocused
                  ? 4
                  : 0;

              return (
                <TouchableOpacity
                  key={String(route.key)}
                  accessibilityRole={'button'}
                  accessibilityStates={['selected']}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={() => onPress()}
                  onLongPress={() => onLongPress()}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    // width: Dimensions.get('window').width / 4,
                    paddingLeft: 20,
                    paddingRight: 20,
                    borderBottomColor: color,
                    borderBottomWidth: borderBottomWidth,
                  }}>
                  <Text
                    style={{
                      fontFamily: medium,
                      fontSize: RFPercentage(1.65),
                      color: color,
                      letterSpacing: 0.3,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      );
    }, [activeIndex]);
  if (routes) {
    return RenderTabBar();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(TabBarMerchant);

export default props => <Wrapper {...props} />;
