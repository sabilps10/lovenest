import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';

// Redux
import {connect} from 'react-redux';
import ReduxAccessoriesFilter from '../../../redux/thunk/AccessoriesFilterThunk';
import ReduxResetAccessoriesFilter from '../../../redux/thunk/ResetAccessoriesFilterThunk';
import ReduxAccessoriesFilterStatus from '../../../redux/thunk/AccessoriesFilterStatusThunk';

// Query
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';

// Components
import ButtonBack from '../../../components/Button/buttonBack';
import ButtonReset from '../../../components/Button/buttonText';

const {black, greyLine, newContainerColor, white, lightSalmon, mainGreen, mainRed} = Colors;
const {regular, small} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions;

export const LoadingScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    </Container>
  );
};

export const ErrorScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Error</Text>
      </View>
    </Container>
  );
};

export const ListCardItem = props => {
  const {name, checked, parentIndex, checkList, index} = props;

  return (
    <CardItem
      style={{paddingBottom: 2}}
      button
      activeOpacity={1}
      onPress={() => {
        checkList(parentIndex, index);
      }}>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',
          borderBottomWidth: 0.5,
          borderBottomColor: greyLine,
          paddingBottom: 10,
        }}>
        <View style={{flex: 0.1}}>
          {checked ? (
            <View
              style={{
                width: 25,
                height: 25,
                borderWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: black,
                borderRadius: 4,
              }}>
              <Icon
                type="Feather"
                name="check"
                style={{
                  left: 10,
                  fontSize: 12.5,
                  color: white,
                }}
              />
            </View>
          ) : (
            <View
              style={{
                width: 25,
                height: 25,
                borderWidth: 2,
                borderColor: black,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: white,
                borderRadius: 4,
              }}
            />
          )}
        </View>
        <View
          style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
          <Text>{name}</Text>
        </View>
      </View>
    </CardItem>
  );
};

export const ListFilter = props => {
  const {list, checkList, parentIndex} = props;

  const extKey = (_, index) => `${String(index)} Child`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };
  const RenderItem = () =>
    useMemo(() => {
      return (
        <FlatList
          getItemLayout={getItemLayout}
          disableVirtualization
          data={list}
          legacyImplementation
          scrollEnabled={false}
          extraData={list}
          keyExtractor={extKey}
          listKey={extKey}
          renderItem={({item, index}) => {
            const {name, checked} = item;
            return (
              <ListCardItem
                name={name}
                checked={checked}
                parentIndex={parentIndex}
                checkList={(p, c) => checkList(p, c)}
                index={index}
              />
            );
          }}
        />
      );
    }, [list, parentIndex, checkList]);
  if (list) {
    return RenderItem();
  } else {
    return null;
  }
};

const AccessoriesFilter = props => {
  console.log('ProductFilter Props: ', props);
  const {
    navigation,
    client,
    route,
    accessoriesFilter,
    resetAccessoriesFilter,
    accessories,
    accessoriesFilterStatus,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [filterList, setFilterList] = useState([]);

  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  useEffect(() => {
    navigationOptions();
    fetchAllIncludeReduxState();
    const subscriber = navigation.addListener('focus', () => {
      fetchAllIncludeReduxState();
    });

    return subscriber;
  }, [navigation, accessories]);

  const fetchAllIncludeReduxState = async () => {
    if (accessories.data.length === 0) {
      await fetchProductFilterList();
    } else {
      await setFilterList([...accessories.data]);
      await setSelectedType([...accessories.selectedType]);
      await setSelectedCategory([...accessories.selectedCategory]);
      await setSelectedColor([...accessories.selectedColor]);
      await setSelectedRange([...accessories.selectedRange]);
      await setIsError(false);
      await setIsLoading(false);
    }
  };

  const fetchProductFilterList = () => {
    try {
      console.log('Masuk sini bray >>>>>>>>>>');
      client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('FILTER DATA RESPONSE: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: list, error} = productsFilter;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setFilterList([...list]);
              await setSelectedType([]);
              await setSelectedCategory([]);
              await setSelectedColor([]);
              await setSelectedRange([]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const popStacking = () => {
    try {
      navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Accessories Filter',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return (
          <ButtonBack iconX={true} {...props} onPress={() => popStacking()} />
        );
      },
      headerRight: () => {
        return (
          <ButtonReset
            onPress={async () => {
              await await accessoriesFilterStatus(false);
              await resetAccessoriesFilter();
              navigation.pop();
            }}
            text={'Reset'}
          />
        );
      },
    });
  };

  const extKey = (_, index) => `${String(index)}`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };

  const checkList = async (parentIndex, index) => {
    let oldData = filterList;
    oldData[parentIndex].list[index].checked = !filterList[parentIndex].list[
      index
    ].checked;
    await setFilterList([...oldData]);

    if (filterList[parentIndex].name === 'Details') {
      let tempSelectedArray = selectedType;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedType(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Category') {
      let tempSelectedArray = selectedCategory;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedCategory(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Range') {
      let tempSelectedArray = selectedColor;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedColor(tempSelectedArray);
    }
  };

  const applyFilter = async () => {
    if (
      selectedType.length === 0 &&
      selectedCategory.length === 0 &&
      selectedColor.length === 0 &&
      selectedRange.length === 0
    ) {
      await accessoriesFilterStatus(false);
      await accessoriesFilter([], [], [], [], []);
    } else {
      await accessoriesFilterStatus(true);
      await accessoriesFilter(
        filterList,
        selectedType,
        selectedCategory,
        selectedColor,
        selectedRange,
      );
    }
    navigation.pop();
  };

  if (isLoading && !isError) {
    return <LoadingScreen />;
  }

  if (!isLoading && isError) {
    return <ErrorScreen />;
  }

  return (
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1}}>
        <Card
          style={{
            marginLeft: 0,
            marginRight: 0,
            elevation: 0,
            shadowOpacity: 0,
            borderWidth: 0,
            borderColor: white,
          }}>
          <FlatList
            decelerationRate="normal"
            legacyImplementation
            contentContainerStyle={{paddingBottom: 10}}
            data={filterList}
            extraData={filterList}
            keyExtractor={extKey}
            listKey={extKey}
            getItemLayout={getItemLayout}
            renderItem={({item, index}) => {
              const {name: label, list} = item;
              return (
                <>
                  <CardItem>
                    <Text>{label}</Text>
                  </CardItem>
                  <ListFilter
                    parentIndex={index}
                    list={list}
                    checkList={(parent, i) => checkList(parent, i)}
                  />
                </>
              );
            }}
          />
        </Card>
      </View>
      <Footer>
        <FooterTab style={{backgroundColor: mainGreen}}>
          <View style={{backgroundColor: mainGreen, width: '100%'}}>
            <TouchableOpacity
              onPress={() => applyFilter()}
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                APPLY FILTER
              </Text>
            </TouchableOpacity>
          </View>
        </FooterTab>
      </Footer>
    </Container>
  );
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    accessories: state.accessories,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    accessoriesFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxAccessoriesFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    resetAccessoriesFilter: () => dispatch(ReduxResetAccessoriesFilter()),
    accessoriesFilterStatus: status =>
      dispatch(ReduxAccessoriesFilterStatus(status)),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AccessoriesFilter);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
