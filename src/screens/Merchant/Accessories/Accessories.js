import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
  RefreshControl,
  Platform,
} from 'react-native';
import {hasNotch} from 'react-native-device-info';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {
  Container,
  Footer,
  FooterTab,
  Card,
  CardItem,
  Icon,
  Button,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ReduxAccessoriesFilter from '../../../redux/thunk/AccessoriesFilterThunk';
import ReduxAccessoriesResetFilter from '../../../redux/thunk/ResetAccessoriesFilterThunk';

// Components
import ButtonBack from '../../../components/Button/buttonBack';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';
import EmptyData from '../../../components/Cards/Merchants/EmptyTabScreen';

// Query
import GET_PRODUCTS from '../../../graphql/queries/productsPublic';

const {
  newContainerColor,
  white,
  greyLine,
  lightSalmon,
  black,
  transparent,
  mainGreen,
  mainRed,
} = Colors;
const {medium, book} = FontType;
const {regular} = FontSize;
const {width, height} = Dimensions.get('window');

export const LoadingScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    </Container>
  );
};

export const ErrorScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Error</Text>
      </View>
    </Container>
  );
};

const Accessories = props => {
  console.log('Accessories Props: >>>>', props);
  const {
    navigation,
    client,
    route,
    accessories,
    resetFilterAccessories,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoadingProducts, setIsLoadingProduct] = useState(true);
  const [isErrorProduct, setIsErrorProduct] = useState(false);

  const [productList, setProductList] = useState([]);

  const [pageSize, setPageSize] = useState(10);
  const [pageNumber] = useState(1);
  const [totalCount, settotalCount] = useState(0);

  const [isPullOnRefresh, setIsPullOnRefresh] = useState(false);

  useEffect(() => {
    navigationOptions();
    runSelectionAPI();
    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      refetchAfterFilter();
    });

    return () => {
      subscriber();
    };
  }, [navigation, isPullOnRefresh]);

  const runSelectionAPI = () => {
    if (id) {
      fetchAllProduct();
    } else {
      fetchAllProduct();
    }
  };

  const settingPullOnRefresh = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await refetchAfterFilter();
  };

  const refetchAfterFilter = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await fetchAllProduct();
  };

  const fetchAllProduct = () => {
    try {
      const variables = {
        // merchantId: null,
        serviceType,
        bridalType: [],
        bridalCategory: [],
        bridalColor: [],
        jewelleryType: [],
        smartHomeCategory: [],
        pageSize,
        pageNumber,
        accsCategory:
          accessories.selectedCategory.length === 0
            ? []
            : accessories.selectedCategory,
        accsDetails:
          accessories.selectedType.length === 0 ? [] : accessories.selectedType,
        accsRange:
          accessories.selectedColor.length === 0
            ? []
            : accessories.selectedColor,
      };
      client
        .query({
          query: GET_PRODUCTS,
          variables,
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
        })
        .then(async response => {
          console.log('Response GET Product List: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error, totalData} = productsPublic;

          if (errors) {
            await setIsErrorProduct(true);
            await setIsLoadingProduct(false);
            await setIsPullOnRefresh(false);
          } else {
            if (error) {
              await setIsErrorProduct(true);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
            } else {
              await settotalCount(totalData);
              await setProductList([...list]);
              await setIsErrorProduct(false);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorProduct(true);
          await setIsLoadingProduct(false);
          await setIsPullOnRefresh(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorProduct(true);
      setIsLoadingProduct(false);
      setIsPullOnRefresh(false);
    }
  };

  const popStacking = async () => {
    try {
      await resetFilterAccessories();
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Accessories',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const settingIsLoadMore = async () => {
    await setPageSize(pageSize + 100);
    await fetchAllProduct();
  };

  if (isLoadingProducts && !isErrorProduct) {
    return <LoadingScreen />;
  }

  if (!isLoadingProducts && isErrorProduct) {
    return <ErrorScreen />;
  }

  return (
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1}}>
        <Card transparent>
          <CardItem
            style={{
              backgroundColor: transparent,
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: medium,
                fontSize: RFPercentage(1.7),
                color: greyLine,
                letterSpacing: 0.3,
              }}>
              {totalCount === 0
                ? ' 0 Product'
                : totalCount > 1
                ? `${totalCount} Products`
                : `${totalCount} Product`}
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('AccessoriesFilter', {id, serviceType});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                }}>
                <Icon
                  type="Feather"
                  name="filter"
                  style={{color: black, fontSize: RFPercentage(1.8)}}
                />
                <Text
                  style={{
                    right: 10,
                    fontFamily: medium,
                    fontSize: RFPercentage(1.7),
                    color: black,
                    letterSpacing: 0.3,
                  }}>
                  Filter
                </Text>
                {accessories.status === false ? null : (
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 20 / 2,
                      backgroundColor: lightSalmon,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      type="Feather"
                      name="check"
                      style={{left: 12, color: white, fontSize: 10}}
                    />
                  </View>
                )}
              </View>
            </TouchableOpacity>
          </CardItem>
        </Card>
        <ListingProduct
          isPullOnRefresh={isPullOnRefresh}
          settingPullOnRefresh={() => settingPullOnRefresh()}
          productList={productList}
          pageSize={pageSize}
          settingIsLoadMore={() => settingIsLoadMore()}
          navigation={navigation}
          id={id}
          totalCount={totalCount}
        />
        <View
          style={{
            width: Dimensions.get('window').width,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: Platform.OS === 'ios' ? (hasNotch() ? 25 : 15) : 20,
          }}>
          <Button
            activeOpacity={0.9}
            onPress={() => {
              navigation.navigate('AccessoriesFilter', {id, serviceType});
            }}
            style={{
              width: Dimensions.get('window').width / 3,
              borderRadius: 25,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: white,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
            }}>
            <Icon
              type="Feather"
              name="filter"
              style={{
                right: accessories.status === false ? 10 : 0,
                color: black,
                fontSize: RFPercentage(1.8),
              }}
            />
            <Text
              style={{
                right: accessories.status === false ? 10 : 0,
                fontFamily: medium,
                fontSize: RFPercentage(1.6),
                color: mainGreen,
                letterSpacing: 0.25,
              }}>
              Filter
            </Text>
            {accessories.status === false ? null : (
              <Icon
                type="Feather"
                name="check"
                style={{color: lightSalmon, fontSize: 18}}
              />
            )}
          </Button>
        </View>
      </View>
    </Container>
  );
};

export const ListingProduct = props => {
  const {
    productList,
    pageSize,
    settingIsLoadMore,
    navigation,
    id,
    totalCount,
    isPullOnRefresh,
    settingPullOnRefresh,
  } = props;

  const extKey = (item, _) => `${String(item.id)} Product`;

  if (productList) {
    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isPullOnRefresh}
            onRefresh={() => settingPullOnRefresh()}
          />
        }
        ListEmptyComponent={() => {
          return (
            <View
              style={{
                flex: 1,
                height: height / 1.2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(2.3),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                Accessories Not Found, Please try with another filter
              </Text>
            </View>
          );
        }}
        data={productList}
        extraData={productList}
        keyExtractor={extKey}
        disableVirtualization
        legacyImplementation
        decelerationRate="fast"
        numColumns={2}
        contentContainerStyle={{
          padding: 15,
          paddingBottom: 15,
          paddingTop: 0,
        }}
        columnWrapperStyle={{
          marginBottom: 0,
          justifyContent: 'space-between',
        }}
        onEndReached={() => {
          if (pageSize <= totalCount) {
            settingIsLoadMore(true);
          } else {
            settingIsLoadMore(false);
          }
        }}
        onEndReachedThreshold={6}
        renderItem={({item, index}) => {
          const {
            featuredImageDynamicURL,
            featuredImageURL,
            name,
            id: productId,
          } = item;
          const source =
            featuredImageDynamicURL === null
              ? {uri: featuredImageURL}
              : {uri: `${featuredImageDynamicURL}=h500`};
          return (
            <CardProduct
              label={'Product'}
              name={name}
              source={source}
              merchantId={id}
              productId={productId}
              navigation={navigation}
            />
          );
        }}
      />
    );
  } else {
    return null;
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    accessories: state.accessories,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    accessoriesFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxAccessoriesFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    resetFilterAccessories: () => dispatch(ReduxAccessoriesResetFilter()),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Accessories);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
