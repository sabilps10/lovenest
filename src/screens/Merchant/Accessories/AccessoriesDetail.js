import React from 'react';
import {Text, View} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';

const AccessoriesDetail = props => {
  console.log('AccessoriesDetail Props: ', props);
  return (
    <View>
      <Text>AccessoriesDetail</Text>
    </View>
  );
};

const Wrapper = compose(withApollo)(AccessoriesDetail);

export default props => <Wrapper {...props} />;
