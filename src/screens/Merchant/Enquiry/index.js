import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {
  Container,
  Content,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Button,
} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonBack from '../../../components/Button/buttonBack';
import ModalCountryCode from '../../../components/Modal/onBoardingModal/countryCodeListModal';
import Picker from '../../../components/Picker/pickerSelect';
import moment from 'moment';
import DatePicker from '../../../components/Picker/dateAndTimePicker';
import AsyncStorage from '@react-native-community/async-storage';
import AsyncType from '../../../utils/AsyncstorageDataStructure/index';

// Query And Mutation
import SUBMIT_ENQUIRY from '../../../graphql/mutations/sendRequestAppointment';
import GET_CUSTOMER from '../../../graphql/queries/getCustomer';

// Pickers
import TextInputPhonePicker from '../../../components/Input/TextinputPhonePickerWithoutLabel';

// Toaster
import ErrorToaster from '../../../components/Toaster/errorToaster';
import SuccessToaster from '../../../components/Toaster/successToaster';

const {
  black,
  white,
  lightSalmon,
  greyLine,
  newContainerColor,
  mainGreen,
} = Colors;
const {regular, small} = FontSize;
const {book, medium} = FontType;
const {asyncToken} = AsyncType;

const renovationBudgetItems = [
  {
    label: 'Below $10,000',
    value: 'Below $10,000',
  },
  {
    label: '$10,001 to $20,000',
    value: '$10,001 to $20,000',
  },
  {
    label: '$20,001 to $30,000',
    value: '$20,001 to $30,000',
  },
  {
    label: '$30,001 to $40,000',
    value: '$30,001 to $40,000',
  },
  {
    label: '$40,001 to $60,000',
    value: '$40,001 to $60,000',
  },
  {
    label: '$60,001 to $80,000',
    value: '$60,001 to $80,000',
  },
  {
    label: '$80,001 to $100,000',
    value: '$80,001 to $100,000',
  },
  {
    label: 'Above $100,001',
    value: 'Above $100,001',
  },
];

const keyCollectionDateItem = [
  {
    label: 'Key is collected',
    value: 'Key is collected',
  },
  {
    label: 'In 1 to 3 months',
    value: 'In 1 to 3 months',
  },
  {
    label: 'In 3 to 6 months',
    value: 'In 3 to 6 months',
  },
  {
    label: 'In 6 to 9 months',
    value: 'In 6 to 9 months',
  },
  {
    label: 'In 9 to 12 months',
    value: 'In 9 to 12 months',
  },
  {
    label: 'Above 12 months',
    value: 'Above 12 months',
  },
];

const typeOfHomeItem = [
  {
    label: 'HDB 1-2 room',
    value: 'HDB 1-2 room',
  },
  {
    label: 'HDB 3 room',
    value: 'HDB 3 room',
  },
  {
    label: 'HDB 4 room',
    value: 'HDB 4 room',
  },
  {
    label: 'HDB 5 room',
    value: 'HDB 5 room',
  },
  {
    label: 'HDB Executive/ Masonette',
    value: 'HDB Executive/ Masonette',
  },
  {
    label: 'Condominium',
    value: 'Condominium',
  },
  {
    label: 'Terrace',
    value: 'Terrace',
  },
  {
    label: 'Semi-detached',
    value: 'Semi-detached',
  },
  {
    label: 'Bungalow',
    value: 'Bungalow',
  },
  {
    label: 'Others',
    value: 'Others',
  },
];

import DateTimePicker from '@react-native-community/datetimepicker';
import Form from '../../../components/FormInput/FormInput';
import FormPhone from '../../../components/FormInput/PhoneNumberInput';
import DateAndTimePicker from '../../../components/FormInput/DateAndTimeForm';
import CustomPicker from '../../../components/FormInput/CustomPickerEnquiry';

const Enquiry = props => {
  console.log('Enquiry Props: ', props);
  const {navigation, route, client} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [name, setName] = useState('');
  const [countryCode, setCountryCode] = useState('+65');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [appointmentDate, setAppointmentDate] = useState('');
  const [appointmentTime, setAppointmentTime] = useState('');
  const [message, setMessage] = useState('');

  // Special Cases for Interior Design
  const [renovationBudget, setRenovationBudget] = useState('');
  const [keyCollectionDate, setKeyCollectionDate] = useState('');
  const [typeOfHome, setTypeOfHome] = useState('');

  const [modalPhonePickerVisible, setModalPhonePickerVisible] = useState(false);
  const [showAppointmenntDatePicker, setShowAppointmentDatePicker] = useState(
    false,
  );
  const [showAppointmentTimePicker, setShowAppointmentTimePicker] = useState(
    false,
  );

  // Toaster
  const [showErrorToaster, setShowErrorToaster] = useState(false);
  const [errorToasterMessage, setErrorToasterMessage] = useState('');

  const [showSuccressToaster, setShowSuccessToaster] = useState(false);
  const [successToasterMessage, setSuccessToasterMessage] = useState('');

  const [buttonLoader, setButtonLoader] = useState(false);

  useEffect(() => {
    StatusBar.setBackgroundColor('white');
    StatusBar.setTranslucent(false);
    StatusBar.setBarStyle('dark-content');
    navigationOptions();
    fetchGetCustomer();
  }, [id, serviceType, navigation]);

  const fetchGetCustomer = () => {
    try {
      AsyncStorage.getItem(asyncToken)
        .then(res => {
          if (res) {
            // any Token here
            client
              .query({
                query: GET_CUSTOMER,
                fetchPolicy: 'no-cache',
                ssr: false,
              })
              .then(async response => {
                console.log('Response Get Customer Enquiry: ', response);
                const {data, errors} = response;
                const {getCustomer} = data;

                if (errors) {
                  await setIsError(false);
                  await setIsLoading(false);
                } else {
                  if (getCustomer) {
                    const {
                      name: userName,
                      countryCode: userCountryCode,
                      phone: userPhone,
                      email: userEmail,
                    } = getCustomer[0];
                    await setName(userName);
                    await setCountryCode(userCountryCode);
                    await setPhone(userPhone);
                    await setEmail(userEmail);
                    await setIsError(false);
                    await setIsLoading(false);
                  } else {
                    await setIsError(false);
                    await setIsLoading(false);
                  }
                }
              })
              .catch(error => {
                console.log('Error: ', error);
                setIsError(false);
                setIsLoading(false);
              });
          } else {
            // no token =>>> not login yet
            setIsError(false);
            setIsLoading(false);
          }
        })
        .catch(error => {
          console.log('Error Async Token 1: ', error);
          setIsError(false);
          setIsLoading(false);
        });
    } catch (error) {
      console.log('Error Async Token');
      setIsError(false);
      setIsLoading(false);
    }
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Submit Enquiry',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return (
          <ButtonBack iconX={true} {...props} onPress={() => popStacking()} />
        );
      },
    });
  };

  const submit = async () => {
    try {
      await setButtonLoader(true);
      await setErrorToasterMessage('');
      await setSuccessToasterMessage('');
      await setShowErrorToaster(false);
      await setShowSuccessToaster(false);
      if (serviceType === 'Interior Design') {
        console.log('Masuk Ke Interior Design');
        if (
          name === '' ||
          email === '' ||
          phone === '' ||
          renovationBudget === '' ||
          keyCollectionDate === '' ||
          typeOfHome === '' ||
          appointmentDate === '' ||
          appointmentTime === '' ||
          message === ''
        ) {
          await setButtonLoader(false);
          await setErrorToasterMessage('Please check your input!');
          await setShowErrorToaster(true);
        } else {
          const submittedData = {
            name,
            email,
            mobile: phone,
            date: appointmentDate,
            time: appointmentTime,
            message,
            merchantId: parseInt(id, 10),
            renovationBudget,
            keyCollectionDate,
            typeOfHome,
          };
          console.log('SUBMITTED DATA: ', submittedData);
          client
            .mutate({
              mutation: SUBMIT_ENQUIRY,
              variables: {
                ...submittedData,
              },
            })
            .then(async response => {
              console.log('Success Submited non Interior: ', response);
              const {data, errors} = response;
              const {sendRequestAppointment} = data;
              const {error} = sendRequestAppointment;

              if (errors) {
                await setButtonLoader(false);
                await setErrorToasterMessage('Failed to submit!');
                await setSuccessToasterMessage('');
                await setShowErrorToaster(true);
                await setShowSuccessToaster(false);
              } else {
                if (error) {
                  await setButtonLoader(false);
                  await setErrorToasterMessage('Failed to submit!');
                  await setSuccessToasterMessage('');
                  await setShowErrorToaster(true);
                  await setShowSuccessToaster(false);
                } else {
                  await setButtonLoader(false);
                  await setErrorToasterMessage('');
                  await setSuccessToasterMessage(
                    'Successfully submit enquiry!',
                  );
                  await setShowErrorToaster(false);
                  await setShowSuccessToaster(true);

                  setTimeout(async () => {
                    await setButtonLoader(false);
                    await setErrorToasterMessage('');
                    await setSuccessToasterMessage('');
                    await setShowErrorToaster(false);
                    await setShowSuccessToaster(false);
                    await navigation.goBack(null);
                  }, 3000);
                }
              }
            })
            .catch(async error => {
              console.log('Error: ', error);
              await setButtonLoader(false);
              await setErrorToasterMessage('Failed to submit!');
              await setSuccessToasterMessage('');
              await setShowErrorToaster(true);
              await setShowSuccessToaster(false);
            });
        }
      } else {
        console.log('Masuk Ke Non Interior Design');
        if (
          name === '' ||
          email === '' ||
          phone === '' ||
          appointmentDate === '' ||
          appointmentTime === '' ||
          message === ''
        ) {
          await setButtonLoader(false);
          await setErrorToasterMessage('Please check your input!');
          await setShowErrorToaster(true);
        } else {
          const submittedData = {
            name,
            email,
            mobile: phone,
            date: appointmentDate,
            time: appointmentTime,
            message,
            merchantId: parseInt(id, 10),
          };
          console.log('SUBMITTED DATA: ', submittedData);
          client
            .mutate({
              mutation: SUBMIT_ENQUIRY,
              variables: {
                ...submittedData,
              },
            })
            .then(async response => {
              console.log('Success Submited non Interior: ', response);
              const {data, errors} = response;
              const {sendRequestAppointment} = data;
              const {error} = sendRequestAppointment;

              if (errors) {
                await setButtonLoader(false);
                await setErrorToasterMessage('Failed to submit!');
                await setSuccessToasterMessage('');
                await setShowErrorToaster(true);
                await setShowSuccessToaster(false);
              } else {
                if (error) {
                  await setButtonLoader(false);
                  await setErrorToasterMessage('Failed to submit!');
                  await setSuccessToasterMessage('');
                  await setShowErrorToaster(true);
                  await setShowSuccessToaster(false);
                } else {
                  await setButtonLoader(false);
                  await setErrorToasterMessage('');
                  await setSuccessToasterMessage(
                    'Successfully submit enquiry!',
                  );
                  await setShowErrorToaster(false);
                  await setShowSuccessToaster(true);

                  setTimeout(async () => {
                    await setButtonLoader(false);
                    await setErrorToasterMessage('');
                    await setSuccessToasterMessage('');
                    await setShowErrorToaster(false);
                    await setShowSuccessToaster(false);
                    await navigation.goBack(null);
                  }, 3000);
                }
              }
            })
            .catch(async error => {
              console.log('Error: ', error);
              await setButtonLoader(false);
              await setErrorToasterMessage('Failed to submit!');
              await setSuccessToasterMessage('');
              await setShowErrorToaster(true);
              await setShowSuccessToaster(false);
            });
        }
      }
    } catch (error) {
      console.log('Error: ', error);
      setButtonLoader(false);
      setErrorToasterMessage('Failed to submit!');
      setSuccessToasterMessage('');
      setShowErrorToaster(true);
      setShowSuccessToaster(false);
    }
  };

  if (isLoading && !isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Checking detail...</Text>
        </View>
      </Container>
    );
  }

  if (!isLoading && isError) {
    return (
      <Container style={{backgroundColor: white}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Error</Text>
        </View>
      </Container>
    );
  }

  return (
    <Container style={{backgroundColor: white}}>
      {showSuccressToaster ? (
        <SuccessToaster text={successToasterMessage} />
      ) : null}
      {showErrorToaster ? <ErrorToaster text={errorToasterMessage} /> : null}
      <Content contentContainerStyle={{paddingTop: 15, paddingBottom: 15}}>
        <Card
          transparent
          style={{
            marginLeft: 0,
            marginRight: 0,
            elevation: 0,
            shadowOpacity: 0,
          }}>
          <Form
            type="Free Text"
            isError={false}
            required={true}
            isLoading={null}
            numberOfLines={1}
            multiline={true}
            value={name}
            onChangeText={async e => {
              await setName(e);
            }}
            title={'Name'}
            placeholder={'Your name...'}
          />
          <FormPhone
            title={'Phone Number'}
            required={true}
            value={phone}
            placeholder={'Your phone number...'}
            countryCode={countryCode}
            onChange={e => setPhone(e)}
            onChangeCountryCode={e => setCountryCode(e)}
          />
          <Form
            type="Email"
            isError={false}
            required={true}
            isLoading={null}
            numberOfLines={1}
            multiline={true}
            value={email}
            onChangeText={async e => {
              await setEmail(e);
            }}
            title={'Email'}
            placeholder={'Your email...'}
          />
          <DateAndTimePicker
            type={'Date'}
            disabled={false}
            isError={false}
            title={'Appointment Date'}
            value={appointmentDate}
            onChange={e => {
              try {
                console.log('Appointment Date Value: ', e);
                setAppointmentDate(e);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            placeholder={'Your appointment date'}
            dateFormat={'DD MMMM YYYY'}
          />
          <DateAndTimePicker
            type={'Time'}
            disabled={false}
            isError={false}
            title={'Appointment Time'}
            value={appointmentTime}
            onChange={e => {
              try {
                console.log('Appointment Time Value: ', e);
                setAppointmentTime(e);
              } catch (error) {
                console.log('Error: ', error);
              }
            }}
            placeholder={'Your appointment time'}
            dateFormat={'hh:mm A'}
          />
          {serviceType === 'Interior Design' ? (
            <CustomPicker
              type={'Picker'}
              data={renovationBudgetItems}
              disabled={false}
              isError={false}
              title={'Renovation Budget'}
              value={renovationBudget}
              onChange={e => {
                try {
                  console.log('renovationBudget Value: ', e);
                  setRenovationBudget(e);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              placeholder={'Select Renovation Budget'}
            />
          ) : null}
          {serviceType === 'Interior Design' ? (
            <CustomPicker
              type={'Picker'}
              data={typeOfHomeItem}
              disabled={false}
              isError={false}
              title={'Type of Home'}
              value={typeOfHome}
              onChange={e => {
                try {
                  console.log('Type Of Home Value: ', e);
                  setTypeOfHome(e);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              placeholder={'Select Type of Home'}
            />
          ) : null}
          {serviceType === 'Interior Design' ? (
            <CustomPicker
              type={'Picker'}
              data={keyCollectionDateItem}
              disabled={false}
              isError={false}
              title={'Key Collection Date'}
              value={keyCollectionDate}
              onChange={e => {
                try {
                  console.log('Type Of Home Value: ', e);
                  setKeyCollectionDate(e);
                } catch (error) {
                  console.log('Error: ', error);
                }
              }}
              placeholder={'Select Key Collection Date'}
            />
          ) : null}
          <Form
            type="Free Text"
            isError={false}
            required={true}
            isLoading={null}
            numberOfLines={3}
            multiline={true}
            value={message}
            onChangeText={async e => {
              await setMessage(e);
            }}
            title={'Message'}
            placeholder={'Your message...'}
          />
        </Card>
      </Content>
      {modalPhonePickerVisible ? (
        <ModalCountryCode
          modalVisibility={modalPhonePickerVisible}
          closeModal={async e => {
            console.log('EA: >>>>>>>>>>>', e);
            if (e) {
              const {dial_code} = e;
              await setCountryCode(dial_code);
              await setModalPhonePickerVisible(false);
            } else {
              setModalPhonePickerVisible(false);
            }
          }}
        />
      ) : null}
      <Footer>
        <FooterTab style={{backgroundColor: mainGreen}}>
          <View style={{backgroundColor: mainGreen, width: '100%'}}>
            <TouchableOpacity
              activeOpacity={buttonLoader ? 1 : 0.6}
              disabled={buttonLoader ? true : false}
              onPress={() => {
                submit();
              }}
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              {buttonLoader ? (
                <ActivityIndicator
                  color="white"
                  size="large"
                  style={{marginRight: 8}}
                  animating={buttonLoader}
                />
              ) : null}
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                SUBMIT ENQUIRY
              </Text>
            </TouchableOpacity>
          </View>
        </FooterTab>
      </Footer>
    </Container>
  );
};

const Wrapper = compose(withApollo)(Enquiry);

export default props => <Wrapper {...props} />;
