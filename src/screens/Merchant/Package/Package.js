import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, FlatList, RefreshControl} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Container, Content, Card, CardItem} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontType, FontSize} from '../../../utils/Themes/Fonts';

const {newContainerColor, greyLine, black} = Colors;
const {regular} = FontSize;
const {medium} = FontType;

// Product Query (this.will be show only on Venue Service)
import PRODUCT_BY_MERCHANT_QUERY from '../../../graphql/queries/productByMerchant';
import PUBLIC_PRODUCT from '../../../graphql/queries/productsPublic';

// Components
import ButtonBack from '../../../components/Button/buttonBack';
import PackageCard from '../../../components/Cards/Merchants/PackageCard';

export const LoadingScreen = () => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    </Container>
  );
};

export const ErrorScreen = props => {
  const {onReload} = props;
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text onPress={() => onReload}>Reload</Text>
      </View>
    </Container>
  );
};

export const EmptyScreen = () => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Data not found</Text>
      </View>
    </Container>
  );
};

const Package = props => {
  console.log('Package Props: ', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [list, setList] = useState([]);

  const [isLoadMore, setIsLoadMore] = useState(false);
  const [isPullRefresh, setIsPullRefresh] = useState(false);

  const [pageSize, setPageSize] = useState(10);
  const pageNumber = 1;
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    navigationOptions();
    API_CAll_WRAPPER();

    const subscriber = navigation.addListener('focus', () => {
      API_CAll_WRAPPER();
    });

    return subscriber;
  }, [navigation]);

  const API_CAll_WRAPPER = () => {
    if (id) {
      fetch();
    } else {
      fetchWithoutMerchantId();
    }
  };

  const fetchWithoutMerchantId = () => {
    try {
      client
        .query({
          query: PUBLIC_PRODUCT,
          variables: {
            serviceType: String(serviceType),
            pageSize,
            pageNumber,
          },
        })
        .then(async response => {
          console.log(
            'Response fetchWithoutMerchantId Fetch Package List: ',
            response,
          );
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: packageList, error, totalData} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsPullRefresh(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setList([...packageList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsPullRefresh(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsPullRefresh(false);
      setIsLoadMore(false);
    }
  };

  const fetch = () => {
    try {
      client
        .query({
          query: PUBLIC_PRODUCT,
          variables: {
            merchantId: parseInt(id, 10),
            serviceType: String(serviceType),
            pageSize,
            pageNumber,
          },
        })
        .then(async response => {
          console.log('Response Fetch Package List: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: packageList, error, totalData} = productsPublic;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
            await setIsPullRefresh(false);
            await setIsLoadMore(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            } else {
              await setTotalCount(totalData);
              await setList([...packageList]);
              await setIsError(false);
              await setIsLoading(false);
              await setIsPullRefresh(false);
              await setIsLoadMore(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
          await setIsPullRefresh(false);
          await setIsLoadMore(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
      setIsPullRefresh(false);
      setIsLoadMore(false);
    }
  };

  const popStacking = async () => {
    try {
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Packages',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
    });
  };

  const settingIsLoadMore = async status => {
    await setIsLoadMore(status);
    await setPageSize(pageSize + 20);
    await API_CAll_WRAPPER();
  };

  const settingPullOnRefresh = async () => {
    await setIsPullRefresh(true);
    await setPageSize(10);
    await API_CAll_WRAPPER();
  };

  if (isLoading && !isError) {
    return <LoadingScreen />;
  }

  if (!isLoading && isError) {
    return (
      <ErrorScreen
        onPress={async () => {
          await setIsError(false);
          await setIsLoading(true);
          await setPageSize(10);
          await fetch();
        }}
      />
    );
  }

  if (!isLoading && !isError && list.length === 0) {
    return <EmptyScreen />;
  }

  return (
    <ListPackages
      id={id}
      list={list}
      totalCount={totalCount}
      pageSize={pageSize}
      isPullRefresh={isPullRefresh}
      isLoadMore={isLoadMore}
      settingPullOnRefresh={settingPullOnRefresh}
      settingIsLoadMore={e => settingIsLoadMore(e)}
      navigation={navigation}
    />
  );
};

export const ListPackages = props => {
  const {
    id,
    list,
    pageSize,
    totalCount,
    isPullRefresh,
    isLoadMore,
    settingIsLoadMore,
    settingPullOnRefresh,
    navigation,
  } = props;
  const extKey = (item, index) => `${String(item.id)} - Parent`;
  const RenderList = () =>
    useMemo(() => {
      return (
        <FlatList
          contentContainerStyle={{
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 15,
          }}
          refreshControl={
            <RefreshControl
              refreshing={isPullRefresh}
              onRefresh={() => settingPullOnRefresh()}
            />
          }
          initialNumToRender={10}
          onEndReachedThreshold={0.01}
          onEndReached={() => {
            if (pageSize <= totalCount) {
              settingIsLoadMore(true);
            } else {
              settingIsLoadMore(false);
            }
          }}
          ListFooterComponent={() => {
            if (isLoadMore) {
              return (
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text>Loading...</Text>
                </View>
              );
            } else {
              return null;
            }
          }}
          decelerationRate="fast"
          data={list}
          extraData={list}
          disableVirtualization
          legacyImplementation
          listKey={extKey}
          keyExtractor={extKey}
          renderItem={({item, index}) => {
            return (
              <PackageCardItem
                item={item}
                label={'Package'}
                merchantId={id}
                navigation={navigation}
              />
            );
          }}
        />
      );
    }, [
      list,
      pageSize,
      isPullRefresh,
      isLoadMore,
      settingIsLoadMore,
      settingPullOnRefresh,
    ]);
  if (list) {
    if (list.length === 0) {
      return null;
    } else {
      return RenderList();
    }
  } else {
    return null;
  }
};

export const PackageCardItem = props => {
  const {item, label, merchantId, navigation} = props;
  const RenderCard = () =>
    useMemo(() => {
      return (
        <PackageCard
          item={item}
          label={label}
          merchantId={merchantId}
          navigation={navigation}
        />
      );
    }, []);
  if (item) {
    return RenderCard();
  } else {
    return null;
  }
};

const Wrapper = compose(withApollo)(Package);

export default props => <Wrapper {...props} />;
