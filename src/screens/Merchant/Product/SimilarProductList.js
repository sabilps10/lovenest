import React, {useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {Container, Header, Left, Right, Body, Button, Icon} from 'native-base';
import ButtonSeeAllProduct from '../../../components/Home/PublicProduct/ButtonSeeAllProduct';
import SIMILAR_PRODUCT from '../../../graphql/queries/productSimilar';
import Loader from '../../../components/Loader/circleLoader';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';
import {commonImage} from '../../../utils/Themes/Images';

const {mainRed, color, white, transparent, black, greyLine} = Colors;
const {loveCrown} = commonImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const {medium} = FontType;

const SimilarProductList = props => {
  console.log('SimilarProductList Screen', props);
  const {navigation, client, route} = props;
  const {params} = route;
  const {id} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);

  const [product, setProduct] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, id]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: SIMILAR_PRODUCT,
          variables: {
            id: parseInt(id, 10),
            pageSize,
            pageNumber,
          },
          networkPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Similar Product List: ', response);
          const {data, errors} = response;
          const {productsSimilar} = data;
          const {data: list, error} = productsSimilar;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProduct([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const extKey = item => `${String(item.id)} Parent`;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <Container>
        <Headers
          onPress={() => {
            try {
              navigation.goBack(null);
            } catch (error) {
              console.log('Error: ', error);
            }
          }}
        />
        <FlatList
          legacyImplementation
          disableVirtualization
          numColumns={2}
          contentContainerStyle={{
            padding: 15,
            paddingBottom: 15,
            paddingTop: 15,
          }}
          columnWrapperStyle={{
            justifyContent: 'space-between',
          }}
          data={product}
          extraData={product}
          keyExtractor={extKey}
          renderItem={({item}) => {
            const {
              featuredImageDynamicURL,
              featuredImageURL,
              name,
              id: productId,
              merchantDetails,
            } = item;
            const source =
              featuredImageDynamicURL === null
                ? {uri: featuredImageURL}
                : {uri: `${featuredImageDynamicURL}=h500`};
            const {id: merchantId} = merchantDetails;
            return (
              <CardProduct
                label={'Product'}
                name={name}
                source={source}
                merchantId={merchantId}
                productId={productId}
                navigation={navigation}
              />
            );
          }}
        />
      </Container>
    );
  }
};

export const Headers = props => {
  const {onPress} = props;
  return (
    <Header
      iosBarStyle="dark-content"
      androidStatusBarColor={white}
      style={{
        backgroundColor: white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
      }}>
      <Left style={{flex: 0.1}}>
        <Button
          onPress={() => onPress()}
          style={{
            backgroundColor: 'transparent',
            elevation: 0,
            shadowOpacity: 0,
            alignSelf: 'center',
            paddingTop: 0,
            paddingBottom: 0,
            height: 35,
            width: 35,
            justifyContent: 'center',
          }}>
          <Icon
            type="Feather"
            name="chevron-left"
            style={{marginLeft: 0, marginRight: 0, fontSize: 24, color: black}}
          />
        </Button>
      </Left>
      <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            fontFamily: medium,
            fontSize: RFPercentage(1.9),
            color: black,
            letterSpacing: 0.3,
          }}>
          Collections
        </Text>
      </Body>
      <Right style={{flex: 0.1}} />
    </Header>
  );
};

const Wrapper = compose(withApollo)(SimilarProductList);

export default props => <Wrapper {...props} />;
