import React, {useEffect, useState} from 'react';
import {Text, View, Modal, StatusBar, ActivityIndicator, SafeAreaView} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {Button, Icon} from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import {RFPercentage} from 'react-native-responsive-fontsize';

const ProductGallery = props => {
  console.log('ProoductAttachment Props: ', props);
  const {route, navigation} = props;
  const {params} = route;
  const {images} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [gallery, setGallery] = useState([]);

  useEffect(() => {
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor('black');
    StatusBar.setBarStyle('dark-content');

    fetch();
  }, []);

  const fetch = async () => {
    try {
      if (images) {
        const manipulated = await Promise.all(
          images.map((d, i) => {
            return {
              ...d,
              url: d?.dynamicUrl ? `${d.dynamicUrl}=h500` : d.url,
            };
          }),
        );

        if (manipulated.length === images.length) {
          await setGallery([...manipulated]);
          await setIsError(false);
          await setIsLoading(false);
        }
      }
    } catch (error) {
      setIsError(false);
      setGallery(false);
    }
  };

  const backPrevScreen = () => {
    try {
      StatusBar.setTranslucent(false);
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('white');
      navigation.goBack(null);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <Modal style={{margin: 0}} visible={true} transparent>
      {isLoading && !isError ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'black',
          }}>
          <ActivityIndicator color="white" size="large" />
        </View>
      ) : !isLoading && isError ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'black',
          }}>
          <Text style={{color: 'white'}}>Error get images</Text>
          <Button
            onPress={backPrevScreen}
            transparent
            style={{
              paddingTop: 0,
              paddingBottom: 0,
              justifyContent: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            <Icon
              type="Feather"
              name="arrow-left"
              style={{
                marginLeft: 0,
                marginRight: 5,
                fontSize: 18,
                color: 'white',
              }}
            />
            <Text style={{fontSize: RFPercentage(1.9), color: 'white'}}>
              Back To Prev Screen
            </Text>
          </Button>
        </View>
      ) : (
        <ImageViewer
          onSwipeDown={backPrevScreen}
          enableSwipeDown
          backgroundColor="black"
          imageUrls={gallery}
          renderHeader={() => {
            return (
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  paddingTop: 15,
                  paddingLeft: 15,
                  zIndex: 999,
                }}>
                <Button
                  onPress={backPrevScreen}
                  transparent
                  style={{
                    top: 18,
                    paddingTop: 0,
                    paddingBottom: 0,
                    height: 35,
                    width: 35,
                    justifyContent: 'center',
                    backgroundColor: 'transparent',
                  }}>
                  <Icon
                    type="Feather"
                    name="x"
                    style={{
                      marginLeft: 0,
                      marginRight: 0,
                      fontSize: 24,
                      color: 'white',
                    }}
                  />
                </Button>
              </View>
            );
          }}
        />
      )}
    </Modal>
  );
};

const Wrapper = compose(withApollo)(ProductGallery);

export default props => <Wrapper {...props} />;
