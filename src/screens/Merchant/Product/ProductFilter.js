import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, FlatList, Dimensions, TouchableOpacity} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import Colors from '../../../utils/Themes/Colors';
import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Footer,
  FooterTab,
} from 'native-base';
import {RFPercentage} from 'react-native-responsive-fontsize';
import LinearGradient from 'react-native-linear-gradient';
// Redux
import {connect} from 'react-redux';
import ReduxProductFilter from '../../../redux/thunk/ProductFilterThunk';
import ReduxResetProductFilter from '../../../redux/thunk/ResetProductFilterThunk';
import ReduxProductFilterStatus from '../../../redux/thunk/ProductFilterStatusThunk';

// Query
import GET_FILTER_LIST from '../../../graphql/queries/productFilter';

// Components
import ButtonBack from '../../../components/Button/buttonBack';
import ButtonReset from '../../../components/Button/buttonText';
import ButtonFiltered from '../../../components/Button/ButtonFilters/Filtered';
import ButtonUnFilter from '../../../components/Button/ButtonFilters/Unfilter';

const {
  black,
  greyLine,
  newContainerColor,
  white,
  mainRed: lightSalmon,
  mainGreen,
} = Colors;
const {regular, small} = FontSize;
const {medium, book} = FontType;
const {width, height} = Dimensions;

export const LoadingScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
    </Container>
  );
};

export const ErrorScreen = props => {
  return (
    <Container style={{backgroundColor: newContainerColor}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Error</Text>
      </View>
    </Container>
  );
};

export const ListCardItem = props => {
  // props indicates
  // name, checked, parentIndex, checkList, index
  const {checked} = props;

  return (
    <>
      {checked ? <ButtonFiltered {...props} /> : <ButtonUnFilter {...props} />}
    </>
  );
};

export const ListFilter = props => {
  const {list, checkList, parentIndex} = props;

  const extKey = (_, index) => `${String(index)} Child`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };
  const RenderItem = () =>
    useMemo(() => {
      return (
        <FlatList
          getItemLayout={getItemLayout}
          disableVirtualization
          data={list}
          legacyImplementation
          scrollEnabled={false}
          extraData={list}
          keyExtractor={extKey}
          listKey={extKey}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            // borderWidth: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '100%',
          }}
          renderItem={({item, index}) => {
            const {name, checked} = item;
            return (
              <ListCardItem
                name={name}
                checked={checked}
                parentIndex={parentIndex}
                checkList={(p, c) => checkList(p, c)}
                index={index}
              />
            );
          }}
        />
      );
    }, [list, parentIndex, checkList]);
  if (list) {
    return RenderItem();
  } else {
    return null;
  }
};

const ProductFilter = props => {
  console.log('ProductFilter Props: ', props);
  const {
    navigation,
    client,
    route,
    productFilter,
    resetProductFilter,
    product,
    productFilterStatus,
  } = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [filterList, setFilterList] = useState([]);

  const [selectedType, setSelectedType] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedColor, setSelectedColor] = useState([]);
  const [selectedRange, setSelectedRange] = useState([]);

  useEffect(() => {
    navigationOptions();
    fetchAllIncludeReduxState();
    const subscriber = navigation.addListener('focus', () => {
      fetchAllIncludeReduxState();
    });

    return () => {
      subscriber();
    };
  }, [navigation, product]);

  const fetchAllIncludeReduxState = async () => {
    if (product.data.length === 0) {
      console.log('Masuk Sini Jos');
      await fetchProductFilterList();
    } else {
      console.log('masuk sana jooss');
      await setFilterList([...product.data]);
      await setSelectedType([...product.selectedType]);
      await setSelectedCategory([...product.selectedCategory]);
      await setSelectedColor([...product.selectedColor]);
      await setSelectedRange([...product.selectedRange]);
      await setIsError(false);
      await setIsLoading(false);
    }
  };

  const fetchProductFilterList = () => {
    try {
      console.log('Masuk sini bray >>>>>>>>>>');
      const merchantId = id ? parseInt(id, 10) : null;
      client
        .query({
          query: GET_FILTER_LIST,
          variables: {
            // merchantId,
            serviceType,
          },
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('FILTER DATA RESPONSE: ', response);
          const {data, errors} = response;
          const {productsFilter} = data;
          const {data: list, error} = productsFilter;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setFilterList([...list]);
              await setSelectedType([]);
              await setSelectedCategory([]);
              await setSelectedColor([]);
              await setSelectedRange([]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const popStacking = () => {
    try {
      navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Product Filter',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return (
          <ButtonBack iconX={true} {...props} onPress={() => popStacking()} />
        );
      },
      headerRight: () => {
        return (
          <ButtonReset
            onPress={async () => {
              await await productFilterStatus(false);
              await resetProductFilter();
              navigation.pop();
            }}
            text={'Reset'}
          />
        );
      },
    });
  };

  const extKey = (_, index) => `${String(index)}`;
  const getItemLayout = (_, index) => {
    return {
      length: width / 3.5,
      offset: (width / 3.5) * index,
      index,
    };
  };

  const checkList = async (parentIndex, index) => {
    let oldData = filterList;
    oldData[parentIndex].list[index].checked = !filterList[parentIndex].list[
      index
    ].checked;
    await setFilterList([...oldData]);

    if (filterList[parentIndex].name === 'Type') {
      let tempSelectedArray = selectedType;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedType(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Category') {
      let tempSelectedArray = selectedCategory;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedCategory(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Color') {
      let tempSelectedArray = selectedColor;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedColor(tempSelectedArray);
    } else if (filterList[parentIndex].name === 'Range') {
      let tempSelectedArray = selectedRange;
      if (filterList[parentIndex].list[index].checked) {
        tempSelectedArray.push(filterList[parentIndex].list[index].name);
      } else {
        const indexes = tempSelectedArray.indexOf(
          filterList[parentIndex].list[index].name,
        );
        tempSelectedArray.splice(indexes, 1);
      }
      await setSelectedRange(tempSelectedArray);
    }
  };

  const applyFilter = async () => {
    if (
      selectedType.length === 0 &&
      selectedCategory.length === 0 &&
      selectedColor.length === 0 &&
      selectedRange.length === 0
    ) {
      await productFilterStatus(false);
    } else {
      await productFilterStatus(true);
    }
    await productFilter(
      filterList,
      selectedType,
      selectedCategory,
      selectedColor,
      selectedRange,
    );
    navigation.pop();
  };

  if (isLoading && !isError) {
    return <LoadingScreen />;
  }

  if (!isLoading && isError) {
    return <ErrorScreen />;
  }

  return (
    <Container style={{backgroundColor: white}}>
      <View style={{flex: 1}}>
        <Card
          style={{
            marginLeft: 0,
            marginRight: 0,
            elevation: 0,
            shadowOpacity: 0,
            borderWidth: 0,
            borderColor: white,
          }}>
          <FlatList
            disableVirtualization
            decelerationRate="normal"
            legacyImplementation
            contentContainerStyle={{paddingBottom: 10}}
            data={filterList}
            extraData={filterList}
            keyExtractor={extKey}
            listKey={extKey}
            getItemLayout={getItemLayout}
            renderItem={({item, index}) => {
              const {name: label, list} = item;
              return (
                <>
                  <CardItem
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginTop: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.8),
                        color: black,
                        letterSpacing: 0.3,
                      }}>
                      {label}
                    </Text>
                    <Text
                      style={{
                        fontFamily: medium,
                        fontSize: RFPercentage(1.4),
                        color: greyLine,
                        letterSpacing: 0.3,
                      }}>
                      You may select more than one
                    </Text>
                  </CardItem>
                  <CardItem
                    style={{
                      width: '100%',
                      flexWrap: 'wrap',
                      flexDirection: 'row',
                    }}>
                    <ListFilter
                      parentIndex={index}
                      list={list}
                      checkList={(parent, i) => checkList(parent, i)}
                    />
                  </CardItem>
                </>
              );
            }}
          />
        </Card>
      </View>
      <Footer>
        <FooterTab style={{backgroundColor: mainGreen}}>
          <View style={{backgroundColor: mainGreen, width: '100%'}}>
            <TouchableOpacity
              onPress={() => applyFilter()}
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: white,
                  letterSpacing: 0.3,
                }}>
                APPLY FILTER
              </Text>
            </TouchableOpacity>
          </View>
        </FooterTab>
      </Footer>
    </Container>
  );
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    product: state.product,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxProductFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    resetProductFilter: () => dispatch(ReduxResetProductFilter()),
    productFilterStatus: status => dispatch(ReduxProductFilterStatus(status)),
  };
};

const ConnectComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductFilter);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
