import React, {useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'native-base';
import ButtonSeeAllProduct from '../../../components/Home/PublicProduct/ButtonSeeAllProduct';
import SIMILAR_PRODUCT from '../../../graphql/queries/productSimilar';
import Loader from '../../../components/Loader/circleLoader';
import {FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Colors from '../../../utils/Themes/Colors';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';
import {commonImage} from '../../../utils/Themes/Images';

const {mainRed} = Colors;
const {loveCrown} = commonImage;
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const {medium} = FontType;

const SimilarProduct = props => {
  console.log('SimilarProduct Component', props);
  const {navigation, client, id, isLoading: loadingParent} = props;

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [pageSize] = useState(2);
  const [pageNumber] = useState(1);

  const [product, setProduct] = useState([]);

  useEffect(() => {
    fetchMerchantDetail();
    const subscriber = navigation.addListener('focus', () => {
      fetchMerchantDetail();
    });

    return () => {
      subscriber();
    };
  }, [navigation, loadingParent, id]);

  const fetchMerchantDetail = () => {
    try {
      client
        .query({
          query: SIMILAR_PRODUCT,
          variables: {
            id: parseInt(id, 10),
            pageSize,
            pageNumber,
          },
          networkPolicy: 'no-cache',
          ssr: false,
          notifyOnNetworkStatusChange: true,
        })
        .then(async response => {
          console.log('Similar Product: ', response);
          const {data, errors} = response;
          const {productsSimilar} = data;
          const {data: list, error} = productsSimilar;

          if (errors) {
            await setIsError(true);
            await setIsLoading(false);
          } else {
            if (error) {
              await setIsError(true);
              await setIsLoading(false);
            } else {
              await setProduct([...list]);
              await setIsError(false);
              await setIsLoading(false);
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsError(true);
          await setIsLoading(false);
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsError(true);
      setIsLoading(false);
    }
  };

  const extKey = item => `${String(item.id)} Parent`;

  if (isLoading && !isError) {
    return (
      <View
        style={{
          width: widthScreen,
          height: heightScreen / 4,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Loader />
      </View>
    );
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <>
        {product.length === 0 ? null : (
          <View
            style={{
              width: widthScreen,
              minHeight: heightScreen / 2.3,
              paddingBottom: 25,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.15}}
              end={{x: 0.0, y: 5}}
              locations={[0.0, 0.15, 0.9]}
              colors={['#FFEDED', 'white', 'white']}
              style={{
                position: 'absolute',
                top: 0,
                zIndex: 0,
                width: '100%',
                height: '100%',
              }}
            />
            <Image
              source={loveCrown}
              style={{
                position: 'absolute',
                top: 10,
                left: 0,
                zIndex: 2,
                width: 43,
                height: 54,
              }}
              resizeMode="contain"
            />
            <ButtonSeeAllProduct
              title={'On The Same Collection'}
              {...props}
              disableButton={true}
            />
            <View style={{top: 45, zIndex: 4}}>
              <FlatList
                legacyImplementation
                disableVirtualization
                numColumns={2}
                contentContainerStyle={{
                  padding: 15,
                  paddingBottom: 15,
                  paddingTop: 0,
                }}
                columnWrapperStyle={{
                  justifyContent: 'space-between',
                }}
                ListFooterComponent={() => {
                  return (
                    <View
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: 10,
                        flexDirection: 'row',
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          try {
                            navigation.push('SimilarProduct', {id});
                          } catch (error) {
                            console.log('Error: ', error);
                          }
                        }}
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: medium,
                            fontSize: RFPercentage(1.7),
                            color: mainRed,
                            letterSpacing: 0.3,
                            marginRight: 5,
                          }}>
                          Explore The Collection
                        </Text>
                        <View
                          style={{
                            width: 20,
                            height: 20,
                            borderRadius: 20 / 2,
                            borderWidth: 1,
                            borderColor: mainRed,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Icon
                            type="Feather"
                            name="arrow-right"
                            style={{fontSize: 15, color: mainRed}}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  );
                }}
                data={product}
                extraData={product}
                keyExtractor={extKey}
                renderItem={({item}) => {
                  const {
                    featuredImageDynamicURL,
                    featuredImageURL,
                    name,
                    id: productId,
                    merchantDetails,
                  } = item;
                  const source =
                    featuredImageDynamicURL === null
                      ? {uri: featuredImageURL}
                      : {uri: `${featuredImageDynamicURL}=h500`};
                  const {id: merchantId} = merchantDetails;
                  return (
                    <CardProduct
                      label={'Product'}
                      name={name}
                      source={source}
                      merchantId={merchantId}
                      productId={productId}
                      navigation={navigation}
                    />
                  );
                }}
              />
            </View>
          </View>
        )}
      </>
    );
  }
};

const Wrapper = compose(withApollo)(SimilarProduct);

export default props => <Wrapper {...props} />;
