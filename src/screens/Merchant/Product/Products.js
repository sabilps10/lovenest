/**
 * id from navigation params in LN v2 will be undefined, so it make validation to check which API will be running off
 */

import React, {useState, useEffect, useCallback} from 'react';
import {
  Text,
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
  RefreshControl,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {withApollo} from 'react-apollo';
import compose from 'lodash/fp/compose';
import {connect} from 'react-redux';
import {Container, Card, CardItem, Icon, Button, Content} from 'native-base';
import Colors from '../../../utils/Themes/Colors';
import {FontSize, FontType} from '../../../utils/Themes/Fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ReduxProductFilter from '../../../redux/thunk/ProductFilterThunk';
import ReduxProductResetFilter from '../../../redux/thunk/ResetProductFilterThunk';
import {CommonActions} from '@react-navigation/native';
import {hasNotch} from 'react-native-device-info';
import _ from 'lodash';

// Components
import ButtonBack from '../../../components/Button/buttonBack';
import CardProduct from '../../../components/Cards/Merchants/ProductCard';

// Query
import GET_PRODUCTS from '../../../graphql/queries/productByMerchant';
import GET_PRODUCT_PUBLIC from '../../../graphql/queries/productsPublic';

const {
  newContainerColor,
  white,
  greyLine,
  lightSalmon,
  black,
  transparent,
  mainGreen,
  headerBorderBottom,
  mainRed,
} = Colors;
const {medium} = FontType;
const {regular} = FontSize;
const {width, height} = Dimensions.get('window');

export const LoadingScreen = () => {
  return (
    <Container style={{backgroundColor: 'white'}}>
      <Content scrollEnabled={false}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: width * 1.5,
          }}>
          <ActivityIndicator size="large" color={mainGreen} />
        </View>
      </Content>
    </Container>
  );
};

export const ErrorScreen = props => {
  const {isPullOnRefresh, settingPullOnRefresh} = props;
  return (
    <Container style={{backgroundColor: 'white'}}>
      <Content
        refreshControl={
          <RefreshControl
            refreshing={isPullOnRefresh}
            onRefresh={settingPullOnRefresh}
          />
        }>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: width * 1.5,
          }}>
          <Text
            style={{
              fontFamily: medium,
              fontSize: RFPercentage(1.8),
              color: greyLine,
              letterSpacing: 0.3,
              textAlign: 'center',
            }}>
            Products Not Found
          </Text>
        </View>
      </Content>
    </Container>
  );
};

const Products = props => {
  console.log('Products Props: >>>>', props);
  const {navigation, client, route, product, resetProductFilter} = props;
  const {params} = route;
  const {id, serviceType} = params;

  const [isLoadingProducts, setIsLoadingProduct] = useState(true);
  const [isErrorProduct, setIsErrorProduct] = useState(false);

  const [productList, setProductList] = useState([]);

  const [pageSize, setPageSize] = useState(40);
  const [pageNumber] = useState(1);
  const [totalCount, settotalCount] = useState(0);

  const [isPullOnRefresh, setIsPullOnRefresh] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);

  useEffect(() => {
    navigationOptions();
    runSelectionAPI();

    if (isLoadMore) {
      refetchCallBackLoadMore();
    }

    const subscriber = navigation.addListener('focus', () => {
      navigationOptions();
      refetchAfterFilter();
      // settingPullOnRefresh();
    });

    return () => {
      subscriber();
    };
  }, [navigation, isPullOnRefresh, isLoadMore]);

  const settingPullOnRefresh = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await refetchAfterFilter();
  };

  const refetchAfterFilter = async () => {
    await setIsPullOnRefresh(true);
    await setPageSize(10);
    await refetchAfterFilterCallBack();
  };

  const refetchAfterFilterCallBack = useCallback(async () => {
    await runSelectionAPI();
  }, [setPageSize, setIsPullOnRefresh]);

  const runSelectionAPI = () => {
    console.log('INI CUKKKK PRODUCT: ', product);
    if (id) {
      fetchAllProduct();
    } else {
      fetchAllPublicProduct();
    }
  };

  const fetchAllPublicProduct = () => {
    try {
      const variables = {
        // merchantId: id,
        serviceType,
        bridalType: serviceType === 'Bridal' ? product.selectedType : [],
        bridalCategory:
          serviceType === 'Bridal' ? product.selectedCategory : [],
        bridalColor: serviceType === 'Bridal' ? product.selectedColor : [],
        jewelleryType: serviceType === 'Jewellery' ? product.selectedType : [],
        smartHomeCategory:
          serviceType === 'Smart Home' ? product.selectedCategory : [],
        pageSize,
        pageNumber,
        accsCategory: [],
        accsDetails: [],
        accsRange: [],
      };

      client
        .query({
          query: GET_PRODUCT_PUBLIC,
          variables,
          fetchPolicy: 'no-cache',
          ssr: false,
        })
        .then(async response => {
          console.log('Response fetch public product: ', response);
          const {errors, data} = response;
          const {productsPublic} = data;
          const {data: list, error, totalData} = productsPublic;

          if (errors) {
            await setIsErrorProduct(true);
            await setIsLoadingProduct(false);
            await setIsPullOnRefresh(false);
            await closeIsLoadMore();
          } else {
            if (error) {
              await setIsErrorProduct(true);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
              await closeIsLoadMore();
            } else {
              await settotalCount(totalData);
              await setProductList([...list]);
              await setIsErrorProduct(false);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
              await closeIsLoadMore();
            }
          }
        })
        .catch(async error => {
          await setIsErrorProduct(true);
          await setIsLoadingProduct(false);
          await setIsPullOnRefresh(false);
          await closeIsLoadMore();
        });
    } catch (error) {
      console.log('Error: Fetch Public Product In Products.js: ', error);
      setIsErrorProduct(true);
      setIsLoadingProduct(false);
      setIsPullOnRefresh(false);
      closeIsLoadMore();
    }
  };

  const fetchAllProduct = () => {
    try {
      const variables = {
        merchantId: id,
        serviceType,
        bridalType: serviceType === 'Bridal' ? product.selectedType : [],
        bridalCategory:
          serviceType === 'Bridal' ? product.selectedCategory : [],
        bridalColor: serviceType === 'Bridal' ? product.selectedColor : [],
        jewelleryType: serviceType === 'Jewellery' ? product.selectedType : [],
        smartHomeCategory:
          serviceType === 'Smart Home' ? product.selectedCategory : [],
        pageSize,
        pageNumber,
        accsCategory: [],
        accsDetails: [],
        accsRange: [],
      };
      client
        .query({
          query: GET_PRODUCT_PUBLIC,
          variables,
          fetchPolicy: 'no-cache', // use no-cache to avoid caching
          notifyOnNetworkStatusChange: true,
          ssr: false,
          pollInterval: 0,
        })
        .then(async response => {
          console.log('Response GET Product List: ', response);
          const {data, errors} = response;
          const {productsPublic} = data;
          const {data: list, error, totalData} = productsPublic;

          if (errors) {
            await setIsErrorProduct(true);
            await setIsLoadingProduct(false);
            await setIsPullOnRefresh(false);
            await closeIsLoadMore();
          } else {
            if (error) {
              await setIsErrorProduct(true);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
              await closeIsLoadMore();
            } else {
              await settotalCount(totalData);
              await setProductList([...list]);
              await setIsErrorProduct(false);
              await setIsLoadingProduct(false);
              await setIsPullOnRefresh(false);
              await closeIsLoadMore();
            }
          }
        })
        .catch(async error => {
          console.log('Error: ', error);
          await setIsErrorProduct(true);
          await setIsLoadingProduct(false);
          await setIsPullOnRefresh(false);
          await closeIsLoadMore();
        });
    } catch (error) {
      console.log('Error: ', error);
      setIsErrorProduct(true);
      setIsLoadingProduct(false);
      setIsPullOnRefresh(false);
      closeIsLoadMore();
    }
  };

  const popStacking = async () => {
    try {
      await resetProductFilter();
      await navigation.pop();
    } catch (error) {
      console.log('ERROR MAMY: ', error);
      navigation.goBack(null);
    }
  };

  const navigationOptions = () => {
    navigation.setOptions({
      tabBarVisible: false,
      headerTitle: 'Products',
      headerTitleAlign: 'center',
      headerTitleStyle: {
        fontFamily: medium,
        color: black,
        fontSize: regular,
      },
      headerStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: greyLine,
        elevation: 0,
        shadowOpacity: 0,
      },
      headerLeft: () => {
        return <ButtonBack {...props} onPress={() => popStacking()} />;
      },
      headerRight: () => {
        return (
          <Button transparent onPress={() => resetToHome()}>
            <Icon
              type="Feather"
              name="home"
              style={{fontSize: 25, color: black}}
            />
          </Button>
        );
      },
    });
  };

  const resetToHome = () => {
    try {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Main'}],
        }),
      );
    } catch (error) {
      console.log('Error reset to home from Products: ', error);
    }
  };

  const settingIsLoadMore = async status => {
    await setIsLoadMore(status);
  };

  const refetchCallBackLoadMore = useCallback(async () => {
    await setPageSize(pageSize + 40);
    await runSelectionAPI();
  }, [isLoadMore]);

  const closeIsLoadMore = useCallback(async () => {
    await setIsLoadMore(false);
  }, [productList]);

  if (isLoadingProducts && !isErrorProduct) {
    return <LoadingScreen />;
  }

  if (!isLoadingProducts && isErrorProduct) {
    return (
      <ErrorScreen
        isPullOnRefresh={isPullOnRefresh}
        settingPullOnRefresh={settingPullOnRefresh}
      />
    );
  } else {
    return (
      <Container style={{backgroundColor: white}}>
        <View style={{flex: 1}}>
          <Card transparent>
            <CardItem
              style={{
                backgroundColor: transparent,
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: medium,
                  fontSize: RFPercentage(1.7),
                  color: greyLine,
                  letterSpacing: 0.3,
                  top: 5,
                }}>
                {totalCount === 0
                  ? ' 0 Product'
                  : totalCount > 1
                  ? `${totalCount} Products`
                  : `${totalCount} Product`}
              </Text>
            </CardItem>
          </Card>
          <ListingProduct
            isLoading={isLoadingProducts}
            isError={isErrorProduct}
            isPullOnRefresh={isPullOnRefresh}
            settingPullOnRefresh={() => settingPullOnRefresh()}
            productList={productList}
            pageSize={pageSize}
            isLoadMore={isLoadMore}
            settingIsLoadMore={async status => await settingIsLoadMore(status)}
            navigation={navigation}
            id={id}
            totalCount={totalCount}
          />
          <View
            style={{
              position: 'absolute',
              bottom:
                Platform.OS === 'android'
                  ? hasNotch()
                    ? 35
                    : 35
                  : hasNotch()
                  ? 35
                  : 20,
              zIndex: 2,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('ProductFilter', {id, serviceType});
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderWidth: 1,
                  borderColor: headerBorderBottom,
                  borderRadius: 25,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  minWidth: width / 16,
                  height: height / 19,
                  flexDirection: 'row',
                  // flexWrap: 'wrap',
                }}>
                {isLoadMore ? (
                  <ActivityIndicator size="small" color={mainGreen} />
                ) : (
                  <Icon
                    style={{fontSize: RFPercentage(1.8), color: black}}
                    type="Feather"
                    name="filter"
                  />
                )}
                <Text
                  style={{
                    fontFamily: medium,
                    fontSize: RFPercentage(1.8),
                    color: black,
                    letterSpacing: 0.3,
                    marginHorizontal: 5,
                  }}>
                  {isLoadMore ? 'Loading more...' : 'Filter'}
                </Text>
                {product?.status ? (
                  isLoadMore ? null : (
                    <View
                      style={{
                        borderRadius: 20 / 2,
                        backgroundColor: mainRed,
                        width: 20,
                        height: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon
                        style={{fontSize: RFPercentage(1.5), color: white}}
                        type="Feather"
                        name="check"
                      />
                    </View>
                  )
                ) : null}
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
};

export const ListingProduct = props => {
  const {
    productList,
    pageSize,
    settingIsLoadMore,
    navigation,
    id,
    totalCount,
    isPullOnRefresh,
    settingPullOnRefresh,
    isLoading,
    isError,
  } = props;

  const extKey = useCallback(
    (item, index) => {
      return `${String(item.id)} Product`;
    },
    [productList],
  );
  const renderItem = useCallback(
    ({item, index}) => {
      const {
        featuredImageDynamicURL,
        featuredImageURL,
        name,
        id: productId,
      } = item;
      const source =
        featuredImageDynamicURL === null
          ? {uri: featuredImageURL}
          : {uri: `${featuredImageDynamicURL}=h500`};
      return (
        <CardProduct
          label={'Product'}
          name={name}
          source={source}
          merchantId={id}
          productId={productId}
          navigation={navigation}
        />
      );
    },
    [productList],
  );

  if (isLoading && !isError) {
    return null;
  } else if (!isLoading && isError) {
    return null;
  } else {
    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isPullOnRefresh}
            onRefresh={() => settingPullOnRefresh()}
          />
        }
        ListEmptyComponent={() => {
          return (
            <View
              style={{
                flex: 1,
                width: width,
                // borderWidth: 1,
                height: width * 1.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  bottom: 45,
                  textAlign: 'center',
                  fontFamily: medium,
                  fontSize: RFPercentage(1.8),
                  color: greyLine,
                  letterSpacing: 0.3,
                  lineHeight: 25,
                }}>
                Products Not Found
              </Text>
            </View>
          );
        }}
        data={productList}
        extraData={productList}
        keyExtractor={extKey}
        disableVirtualization
        legacyImplementation
        initialNumToRender={40}
        decelerationRate="normal"
        contentContainerStyle={{
          paddingTop: 10,
          paddingBottom: 45,
          alignSelf: 'flex-start',
        }}
        numColumns={2}
        columnWrapperStyle={{
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingLeft: 7,
          paddingRight: 10,
        }}
        onEndReached={() => {
          if (pageSize < totalCount) {
            settingIsLoadMore(true);
          } else {
            settingIsLoadMore(false);
          }
        }}
        onEndReachedThreshold={0.6}
        renderItem={renderItem}
      />
    );
  }
};

const mapStateToProps = state => {
  console.log('mapStateToProps: ', state);
  return {
    product: state.product,
  };
};

const mapDispatchToProps = dispatch => {
  console.log('mapDispatchToProps: ', dispatch);
  return {
    productFilter: (
      data,
      selectedtype,
      selectedCategory,
      selectedColor,
      selectedRange,
    ) =>
      dispatch(
        ReduxProductFilter(
          data,
          selectedtype,
          selectedCategory,
          selectedColor,
          selectedRange,
        ),
      ),
    resetProductFilter: () => dispatch(ReduxProductResetFilter()),
  };
};

const ConnectComponent = connect(mapStateToProps, mapDispatchToProps)(Products);

const Wrapper = compose(withApollo)(ConnectComponent);

export default props => <Wrapper {...props} />;
