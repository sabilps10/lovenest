import FLORIST_FILTER_STATUS_ACTION from '../action/FloristFilterStatusAction';

const floristFilterStatusThunk = status => {
  return dispatch => {
    dispatch(FLORIST_FILTER_STATUS_ACTION(status));
  };
};

export default floristFilterStatusThunk;
