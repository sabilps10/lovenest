import FLORIST_SIZE_ACTION from '../action/FloristSizeFilterAction';

const floristSizeFilterThunk = size => {
  return dispatch => {
    dispatch(FLORIST_SIZE_ACTION(size));
  };
};

export default floristSizeFilterThunk;
