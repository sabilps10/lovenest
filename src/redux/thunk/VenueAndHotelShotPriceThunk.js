import VenueAndHotelShortPriceAction from '../action/VenueAndHotelShortPriceAction';

const venueAndHotelShortPriceThunk = shortBy => {
  return dispatch => {
    dispatch(VenueAndHotelShortPriceAction(shortBy));
  };
};

export default venueAndHotelShortPriceThunk;
