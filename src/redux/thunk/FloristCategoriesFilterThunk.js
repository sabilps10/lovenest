import FLORIST_CATEGORIES_ACTION from '../action/FloristCategoriesFilterAction';

const floristCategoriesFilterThunk = (categories, categoryName) => {
  return dispatch => {
    dispatch(FLORIST_CATEGORIES_ACTION(categories, categoryName));
  };
};

export default floristCategoriesFilterThunk;
