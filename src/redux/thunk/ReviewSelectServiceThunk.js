import REVIEW_SERVICE_SELECT from '../action/ReviewSelectServiceAction';

const reviewSelectServiceThunk = service => {
  return dispatch => {
    dispatch(REVIEW_SERVICE_SELECT(service));
  };
};

export default reviewSelectServiceThunk;
