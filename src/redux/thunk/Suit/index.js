import Action from '../../action/Suit/index';

const thunkFilterSuit = list => {
  return dispatch => {
    dispatch(Action(list));
  };
};

export default thunkFilterSuit;
