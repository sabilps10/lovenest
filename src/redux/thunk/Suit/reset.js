import Action from '../../action/Suit/reset';

const thunkResetSuit = () => {
  return dispatch => {
    dispatch(Action());
  };
};

export default thunkResetSuit;
