import VenueAndHotelAvailabilitiesAction from '../action/VenueAndHotelAvailabilityAction';

const venueAndHotelAvailabilitiesThunk = avail => {
  return dispatch => {
    dispatch(VenueAndHotelAvailabilitiesAction(avail));
  };
};

export default venueAndHotelAvailabilitiesThunk;
