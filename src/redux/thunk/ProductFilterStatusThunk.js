import FILTER_PRODUCT_STATUS from '../action/ProductFilterStatus';

const productFilterStatusThunk = status => {
  return dispatch => {
    dispatch(FILTER_PRODUCT_STATUS(status));
  };
};

export default productFilterStatusThunk;
