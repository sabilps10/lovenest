import FILTER_PRODUCT_ACTION from '../action/ProductFilterAction';

const productFilterThunk = (
  data,
  selectedType,
  selectedCategory,
  selectedColor,
  selectedRange,
) => {
  return dispatch => {
    dispatch(
      FILTER_PRODUCT_ACTION(
        data,
        selectedType,
        selectedCategory,
        selectedColor,
        selectedRange,
      ),
    );
  };
};

export default productFilterThunk;
