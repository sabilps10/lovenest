import VenueAndHotelPriceAction from '../action/VenueAndHotelPriceAction';

const venueAndHotelpriceThunk = prices => {
  return dispatch => {
    dispatch(VenueAndHotelPriceAction(prices));
  };
};

export default venueAndHotelpriceThunk;
