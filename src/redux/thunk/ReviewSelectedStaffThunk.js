import REVIEW_STAFF_SELECT from '../action/ReviewSelectedStaffAction';

const reviewSelectStaffThunk = staffs => {
  return dispatch => {
    dispatch(REVIEW_STAFF_SELECT(staffs));
  };
};

export default reviewSelectStaffThunk;
