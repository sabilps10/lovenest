import REVIEW_MERCHANT_SELECT from '../action/ReviewSelectedMerchantAction';

const reviewSelectMerchantThunk = (merchantId, merchantName) => {
  return dispatch => {
    dispatch(REVIEW_MERCHANT_SELECT(merchantId, merchantName));
  };
};

export default reviewSelectMerchantThunk;
