import VenueAndHotelPackagesAction from '../action/VenueAndHotelPackageAction';

const venueAndHotelPackagesThunk = packages => {
  return dispatch => {
    dispatch(VenueAndHotelPackagesAction(packages));
  };
};

export default venueAndHotelPackagesThunk;
