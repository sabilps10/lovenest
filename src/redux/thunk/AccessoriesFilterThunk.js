import FILTER_ACCESSORIES_ACTION from '../action/AccessoriesFilterAction';

const accessoriesFilterThunk = (
  data,
  selectedType,
  selectedCategory,
  selectedColor,
  selectedRange,
) => {
  return dispatch => {
    dispatch(
      FILTER_ACCESSORIES_ACTION(
        data,
        selectedType,
        selectedCategory,
        selectedColor,
        selectedRange,
      ),
    );
  };
};

export default accessoriesFilterThunk;
