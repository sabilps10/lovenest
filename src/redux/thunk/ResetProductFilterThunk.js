import RESET_PRODUCT_FILTER from '../action/ResetProductFilter';

const resetproductFilterThunk = () => {
  return dispatch => {
    dispatch(RESET_PRODUCT_FILTER());
  };
};

export default resetproductFilterThunk;
