import FLORIST_TABSCREEN_ACTION from '../action/FloristTabScreenHeightAction';

const floristTabScreenHeightThunk = height => {
  return dispatch => {
    dispatch(FLORIST_TABSCREEN_ACTION(height));
  };
};

export default floristTabScreenHeightThunk;
