import Action from '../../../action/Bridal/Suit/reset';

const thunkResetSuit = () => {
  return dispatch => {
    dispatch(Action());
  };
};

export default thunkResetSuit;
