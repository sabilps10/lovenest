import Action from '../../../action/Bridal/Suit/index';

const thunkFilterSuit = list => {
  return dispatch => {
    dispatch(Action(list));
  };
};

export default thunkFilterSuit;
