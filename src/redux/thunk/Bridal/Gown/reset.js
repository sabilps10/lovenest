import Action from '../../../action/Bridal/Gown/reset';

const thunkResetGown = () => {
  return dispatch => {
    dispatch(Action());
  };
};

export default thunkResetGown;
