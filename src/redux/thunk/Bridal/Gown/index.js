import Action from '../../../action/Bridal/Gown/index';

const thunkFilterGown = list => {
  return dispatch => {
    dispatch(Action(list));
  };
};

export default thunkFilterGown;
