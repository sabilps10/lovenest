import FILTER_ACCESSORIES_STATUS from '../action/AccessoriesFilterStatus';

const accessoriesFilterStatusThunk = status => {
  return dispatch => {
    dispatch(FILTER_ACCESSORIES_STATUS(status));
  };
};

export default accessoriesFilterStatusThunk;
