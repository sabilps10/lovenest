import RESET_ACCESSORIES_FILTER from '../action/ResetAccessoriesFilter';

const resetproductFilterThunk = () => {
  return dispatch => {
    dispatch(RESET_ACCESSORIES_FILTER());
  };
};

export default resetproductFilterThunk;
