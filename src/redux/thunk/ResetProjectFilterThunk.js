import RESET_PROJECT_FILTER from '../action/ResetProjectFilter';

const resetprojectFilterThunk = () => {
  return dispatch => {
    dispatch(RESET_PROJECT_FILTER());
  };
};

export default resetprojectFilterThunk;
