import FLORIST_THEME_ACTION from '../action/FloristThemeFilterAction';

const floristThemeFilterThunk = themes => {
  return dispatch => {
    dispatch(FLORIST_THEME_ACTION(themes));
  };
};

export default floristThemeFilterThunk;
