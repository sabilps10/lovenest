import FLORIST_COLOR_ACTION from '../action/FloristColorFilterAction';

const floristColorFilterThunk = color => {
  return dispatch => {
    dispatch(FLORIST_COLOR_ACTION(color));
  };
};

export default floristColorFilterThunk;
