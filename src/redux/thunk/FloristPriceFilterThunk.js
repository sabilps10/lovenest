import FLORIST_PRICE_ACTION from '../action/FloristPriceFilterAction';

const floristPriceFilterThunk = price => {
  return dispatch => {
    dispatch(FLORIST_PRICE_ACTION(price));
  };
};

export default floristPriceFilterThunk;
