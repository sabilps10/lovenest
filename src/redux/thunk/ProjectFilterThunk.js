import FILTER_PROJECT_ACTION from '../action/ProjectFilterAction';

const projectFilterThunk = (data, selectedType, selectedStyle) => {
  return dispatch => {
    dispatch(FILTER_PROJECT_ACTION(data, selectedType, selectedStyle));
  };
};

export default projectFilterThunk;