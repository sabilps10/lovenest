import FLORIST_TYPE_ACTION from '../action/FloristTypeFilterAction';

const floristTypeFilterThunk = type => {
  return dispatch => {
    dispatch(FLORIST_TYPE_ACTION(type));
  };
};

export default floristTypeFilterThunk;
