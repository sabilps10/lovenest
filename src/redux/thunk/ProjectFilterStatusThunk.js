import FILTER_PROJECT_STATUS from '../action/ProjectFilterStatus';

const projectFilterStatusThunk = status => {
  return dispatch => {
    dispatch(FILTER_PROJECT_STATUS(status));
  };
};

export default projectFilterStatusThunk;
