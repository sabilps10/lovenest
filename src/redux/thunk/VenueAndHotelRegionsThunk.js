import VenueAndHotelRegionsAction from '../action/VenueAndHotelRegionAction';

const venueAndHotelRegionsThunk = regions => {
  return dispatch => {
    dispatch(VenueAndHotelRegionsAction(regions));
  };
};

export default venueAndHotelRegionsThunk;
