import VenueAndHotelCapacitiesAction from '../action/VenueAndHotelCapacityAction';

const venueAndHotelCapacitiesThunk = capacities => {
  return dispatch => {
    dispatch(VenueAndHotelCapacitiesAction(capacities));
  };
};
export default venueAndHotelCapacitiesThunk;
