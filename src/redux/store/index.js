import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';

import Reducer from '../reducer';
const middleware = thunk;
const composeEnhancers = composeWithDevTools({});
const store = createStore(
  Reducer,
  composeEnhancers(applyMiddleware(middleware)),
);

export default store;
