import {
  REVIEW_SERVICE_SELECT,
  REVIEW_MERCHANT_SELECT,
  REVIEW_STAFF_SELECT,
} from '../../type';

const initialState = {
  serviceSelected: null,
  selectedMerchant: null,
  selectedMerchantName: '',
  selectedStaff: [],
};

const defaultState = {
  serviceSelected: null,
  selectedMerchant: null,
  selectedMerchantName: '',
  selectedStaff: [],
};

const ReviewReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case REVIEW_SERVICE_SELECT:
      return {
        ...state,
        serviceSelected: payload.service,
      };
    case REVIEW_MERCHANT_SELECT:
      return {
        ...state,
        selectedMerchant: payload.merchantId,
        selectedMerchantName: payload.merchantName,
      };
    case REVIEW_STAFF_SELECT:
      return {
        ...state,
        selectedStaff: [...payload.staffs],
      };
    default:
      return initialState;
  }
};

export default ReviewReducer;
