// Type of Action
import {
  FILTER_ACCESSORIES,
  RESET_FILTER_ACCESSORIES,
  FILTER_ACCESSORIES_STATUS,
} from '../../type/index';

const initiateState = {
  data: [],
  status: false,
  selectedType: [],
  selectedCategory: [],
  selectedColor: [],
  selectedRange: [],
};

const defaultState = {
  data: [],
  status: false,
  selectedType: [],
  selectedCategory: [],
  selectedColor: [],
  selectedRange: [],
};

const AccessoriesFilterReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case FILTER_ACCESSORIES:
      return {
        ...state,
        data: payload.data,
        selectedType: payload.selectedType,
        selectedCategory: payload.selectedCategory,
        selectedColor: payload.selectedColor,
        selectedRange: payload.selectedRange,
      };
    case RESET_FILTER_ACCESSORIES:
      return {
        ...state,
        data: initiateState.data,
        status: false,
        selectedType: initiateState.selectedType,
        selectedCategory: initiateState.selectedCategory,
        selectedColor: initiateState.selectedColor,
        selectedRange: initiateState.selectedRange,
      };
    case FILTER_ACCESSORIES_STATUS:
      return {
        ...state,
        status: payload.status,
      };
    default:
      return state;
  }
};

export default AccessoriesFilterReducer;
