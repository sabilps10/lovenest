// Type of Action
import {
  FILTER_PROJECT,
  FILTER_PROJECT_STATUS,
  RESET_FILTER_PROJECT,
} from '../../type/index';

const initiateState = {
  data: [],
  status: false,
  selectedType: [],
  selectedStyle: [],
};

const defaultState = {
  data: [],
  status: false,
  selectedType: [],
  selectedStyle: [],
};

const ProjectFilterReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case FILTER_PROJECT:
      return {
        ...state,
        data: payload.data,
        selectedType: payload.selectedType,
        selectedStyle: payload.selectedStyle,
      };
    case FILTER_PROJECT_STATUS:
      return {
        ...state,
        status: payload.status,
      };
    case RESET_FILTER_PROJECT:
      return {
        ...state,
        data: initiateState.data,
        status: false,
        selectedType: initiateState.selectedType,
        selectedStyle: initiateState.selectedStyle,
      };
    default:
      return state;
  }
};

export default ProjectFilterReducer;
