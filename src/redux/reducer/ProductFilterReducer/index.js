// Type of Action
import {
  FILTER_PRODUCT,
  RESET_FILTER_PRODUCT,
  FILTER_PRODUCT_STATUS,
} from '../../type/index';

const initiateState = {
  data: [],
  status: false,
  selectedType: [],
  selectedCategory: [],
  selectedColor: [],
  selectedRange: [],
};

const defaultState = {
  data: [],
  status: false,
  selectedType: [],
  selectedCategory: [],
  selectedColor: [],
  selectedRange: [],
};

const ProductFilterReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case FILTER_PRODUCT:
      return {
        ...state,
        data: payload.data,
        selectedType: payload.selectedType,
        selectedCategory: payload.selectedCategory,
        selectedColor: payload.selectedColor,
        selectedRange: payload.selectedRange,
      };
    case RESET_FILTER_PRODUCT:
      state = initiateState;
      return {
        ...state,
        // data: initiateState.data,
        // status: false,
        // selectedType: initiateState.selectedType,
        // selectedCategory: initiateState.selectedCategory,
        // selectedColor: initiateState.selectedColor,
        // selectedRange: initiateState.selectedRange,
      };
    case FILTER_PRODUCT_STATUS:
      return {
        ...state,
        status: payload.status,
      };
    default:
      return state;
  }
};

export default ProductFilterReducer;
