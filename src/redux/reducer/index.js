import {combineReducers} from 'redux';
import product from './ProductFilterReducer/index';
import project from './ProjectFilterReducer/index';
import accessories from './AccessoriesFilterReducer/index';
import positionYBottomNav from './BottomTabReducer/BottomTabReducer';
import review from './ReviewReducer';
import florist from './FloristFilterReducer';
import floristTabScreenHeight from './FloristTabScreenHeightReducer';
import venueAndHotel from './VenueAndHotelFilterReducer';

// New Bridal Redux Setup
import bridalGownFilterList from './Bridal/Gown/index';
import bridalSuitFilterList from './Bridal/Suit/index';

// Suit Redux Setup
import suitFilterList from './Suit/index';

export default combineReducers({
  product,
  project,
  accessories,
  positionYBottomNav,
  review,
  florist,
  floristTabScreenHeight,
  venueAndHotel,
  bridalGownFilterList,
  bridalSuitFilterList,
  suitFilterList,
});
