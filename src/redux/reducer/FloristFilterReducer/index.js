import {
  FLORIST_PRICE,
  FLORIST_SIZE,
  FLORIST_CATEGORIES,
  FLORIST_TYPE,
  FLORIST_COLOR,
  FLORIST_FILTER_STATUS,
  FLORIST_THEME,
} from '../../type';

const initiateState = {
  price: [0.0, 0.0],
  size: [0.0, 0.0],
  categories: [],
  selectedCategories: [],
  type: [],
  color: [],
  status: false,
  themes: [],
};

const defaultState = {
  price: [0.0, 0.0],
  size: [0.0, 0.0],
  categories: [],
  selectedCategories: [],
  type: [],
  color: [],
  status: false,
  themes: [],
};

const FloristFilterReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case FLORIST_PRICE:
      return {
        ...state,
        price: payload.prices,
      };
    case FLORIST_SIZE:
      return {
        ...state,
        size: payload.size,
      };
    case FLORIST_CATEGORIES:
      return {
        ...state,
        categories: [...payload.categories],
        selectedCategories: payload.categoryName,
      };
    case FLORIST_TYPE:
      return {
        ...state,
        type: [...payload.type],
      };
    case FLORIST_COLOR:
      return {
        ...state,
        color: [...payload.color],
      };
    case FLORIST_FILTER_STATUS:
      return {
        ...state,
        status: payload.status,
      };
    case FLORIST_THEME:
      return {
        ...state,
        themes: payload.themes,
      };
    default:
      return initiateState;
  }
};

export default FloristFilterReducer;
