import {
  VENUE_AND_HOTEL_PRICE,
  VENUE_AND_HOTEL_SHORT_PRICE,
  VENUE_AND_HOTEL_PACKAGES,
  VENUE_AND_HOTEL_REGION,
  VENUE_AND_HOTEL_CAPACITY,
  VENUE_AND_HOTEL_AVAILABILITY,
} from '../../type';

const initiateState = {
  prices: [0.0, 0.0],
  shortPrices: null, // null is default unselected, and if selected might has LOW and HIGH in string
  packages: [], // cant have more than one data
  regions: [], // cant have more than one data
  capacities: [], // cant have more than one data
  availabilities: [], // cant have more than one data
};

const defaultState = {
  prices: [0.0, 0.0],
  shortPrices: null, // null is default unselected, and if selected might has LOW and HIGH in string
  packages: [], // cant have more than one data
  regions: [], // cant have more than one data
  capacities: [], // cant have more than one data
  availabilities: [], // cant have more than one data
};

const VenueAndHotelReducer = (state = defaultState, action) => {
  const {type, payload} = action;
  switch (type) {
    case VENUE_AND_HOTEL_PRICE:
      return {
        ...state,
        prices: payload?.prices,
      };
    case VENUE_AND_HOTEL_SHORT_PRICE:
      return {
        ...state,
        shortPrices: payload?.shortBy,
      };
    case VENUE_AND_HOTEL_PACKAGES:
      return {
        ...state,
        packages: [...payload?.packages],
      };
    case VENUE_AND_HOTEL_REGION:
      return {
        ...state,
        regions: [...payload?.regions],
      };
    case VENUE_AND_HOTEL_CAPACITY:
      return {
        ...state,
        capacities: [...payload?.capacities],
      };
    case VENUE_AND_HOTEL_AVAILABILITY:
      return {
        ...state,
        availabilities: [...payload?.avail],
      };
    default:
      return initiateState;
  }
};

export default VenueAndHotelReducer;
