import {FLORIST_TAB} from '../../type';

const initiateState = {
  height: 200,
};

const defaultState = {
  height: 200,
};

const FloristTabScreenHeightReducer = (state = defaultState, action) => {
  const {type, payload} = action;

  switch (type) {
    case FLORIST_TAB:
      return {
        ...state,
        height: payload.height,
      };
    default:
      return initiateState;
  }
};

export default FloristTabScreenHeightReducer;
