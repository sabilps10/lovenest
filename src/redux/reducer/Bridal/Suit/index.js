import {
  FILTER_SUIT_PRODUCT,
  FILTER_SUIT_RESET,
} from '../../../type/Bridal/Suit/index';

const initiateState = {
  list: [],
};

const defaultState = {
  list: [],
};

const ReducerSuitFilter = (state = defaultState, action) => {
  switch (action?.type) {
    case FILTER_SUIT_PRODUCT:
      return {
        ...state,
        list: action?.payload?.list,
      };
    case FILTER_SUIT_RESET:
      return {
        ...state,
        list: [],
      };
    default:
      return {
        ...initiateState,
      };
  }
};

export default ReducerSuitFilter;
