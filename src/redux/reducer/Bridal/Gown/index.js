import {
  FILTER_GOWN_PRODUCT,
  FILTER_GOWN_RESET,
} from '../../../type/Bridal/Gown/index';

const initiateState = {
  list: [],
};

const defaultState = {
  list: [],
};

const ReducerGownFilter = (state = defaultState, action) => {
  switch (action?.type) {
    case FILTER_GOWN_PRODUCT:
      return {
        ...state,
        list: action?.payload?.list,
      };
    case FILTER_GOWN_RESET:
      return {
        state,
        list: [],
      };
    default:
      return {
        ...initiateState,
      };
  }
};

export default ReducerGownFilter;
