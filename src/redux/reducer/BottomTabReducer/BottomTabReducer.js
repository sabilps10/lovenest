import {Animated} from 'react-native';
const positionY = new Animated.Value(0);

const BottomNavPosition = () => {
  return positionY;
};

export default BottomNavPosition;
