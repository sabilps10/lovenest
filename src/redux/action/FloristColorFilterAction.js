import {FLORIST_COLOR} from '../type';

const floristColorFilterAction = color => {
  return {
    type: FLORIST_COLOR,
    payload: {
      color,
    },
  };
};

export default floristColorFilterAction;
