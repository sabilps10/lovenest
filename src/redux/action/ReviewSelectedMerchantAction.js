import {REVIEW_MERCHANT_SELECT} from '../type';

const reviewSelectMerchantAction = (merchantId, merchantName) => {
  return {
    type: REVIEW_MERCHANT_SELECT,
    payload: {
      merchantId,
      merchantName,
    },
  };
};

export default reviewSelectMerchantAction;
