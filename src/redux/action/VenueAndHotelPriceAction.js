import {VENUE_AND_HOTEL_PRICE} from '../type';

const venueAndHotelPriceAction = prices => {
  return {
    type: VENUE_AND_HOTEL_PRICE,
    payload: {
      prices,
    },
  };
};

export default venueAndHotelPriceAction;
