import {RESET_FILTER_PRODUCT} from '../type';
const resetProductFilter = () => {
  return {
    type: RESET_FILTER_PRODUCT,
  };
};
export default resetProductFilter;
