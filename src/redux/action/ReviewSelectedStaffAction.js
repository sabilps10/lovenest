import {REVIEW_STAFF_SELECT} from '../type';

const reviewSelectStaffAction = staffs => {
  return {
    type: REVIEW_STAFF_SELECT,
    payload: {
      staffs,
    },
  };
};

export default reviewSelectStaffAction;
