import {FILTER_ACCESSORIES} from '../type';
const filterAccessoriesAction = (
  data,
  selectedType,
  selectedCategory,
  selectedColor,
  selectedRange,
) => {
  return {
    type: FILTER_ACCESSORIES,
    payload: {
      data,
      selectedType,
      selectedCategory,
      selectedColor,
      selectedRange,
    },
  };
};

export default filterAccessoriesAction;
