import {FILTER_PROJECT} from '../type';
const filterProjectAction = (data, selectedType, selectedStyle) => {
  return {
    type: FILTER_PROJECT,
    payload: {
      data,
      selectedType,
      selectedStyle,
    },
  };
};

export default filterProjectAction;