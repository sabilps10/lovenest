import {VENUE_AND_HOTEL_AVAILABILITY} from '../type';

const venueAndHotelAvailabilityAction = avail => {
  return {
    type: VENUE_AND_HOTEL_AVAILABILITY,
    payload: {
      avail,
    },
  };
};

export default venueAndHotelAvailabilityAction;
