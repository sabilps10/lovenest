import {FILTER_PRODUCT} from '../type';
const filterProductAction = (
  data,
  selectedType,
  selectedCategory,
  selectedColor,
  selectedRange,
) => {
  return {
    type: FILTER_PRODUCT,
    payload: {
      data,
      selectedType,
      selectedCategory,
      selectedColor,
      selectedRange,
    },
  };
};

export default filterProductAction;
