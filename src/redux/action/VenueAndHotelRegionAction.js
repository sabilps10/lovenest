import {VENUE_AND_HOTEL_REGION} from '../type';

const venueAndHotelRegionAction = regions => {
  return {
    type: VENUE_AND_HOTEL_REGION,
    payload: {
      regions,
    },
  };
};
export default venueAndHotelRegionAction;
