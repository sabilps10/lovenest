import {FILTER_PRODUCT_STATUS} from '../type';
const filterProductStatusAction = status => {
  return {
    type: FILTER_PRODUCT_STATUS,
    payload: {
      status,
    },
  };
};

export default filterProductStatusAction;
