import {FILTER_PROJECT_STATUS} from '../type';
const filterProjectStatusAction = status => {
  return {
    type: FILTER_PROJECT_STATUS,
    payload: {
      status,
    },
  };
};

export default filterProjectStatusAction;