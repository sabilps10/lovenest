import {REVIEW_SERVICE_SELECT} from '../type';
const reviewSelectServiceAction = service => {
  return {
    type: REVIEW_SERVICE_SELECT,
    payload: {
      service,
    },
  };
};

export default reviewSelectServiceAction;
