import {FILTER_GOWN_RESET} from '../../../type/Bridal/Gown/index';

const actionResetFilterGown = () => {
  return {
    type: FILTER_GOWN_RESET,
    payload: {
      reset: true,
    },
  };
};

export default actionResetFilterGown;
