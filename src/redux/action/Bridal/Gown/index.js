import {FILTER_GOWN_PRODUCT} from '../../../type/Bridal/Gown/index';

const actionFilterGown = list => {
  return {
    type: FILTER_GOWN_PRODUCT,
    payload: {
      list,
    },
  };
};

export default actionFilterGown;
