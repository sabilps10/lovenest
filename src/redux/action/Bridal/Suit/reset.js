import {FILTER_SUIT_RESET} from '../../../type/Bridal/Suit/index';

const actionResetFilterSuit = () => {
  return {
    type: FILTER_SUIT_RESET,
    payload: {
      reset: true,
    },
  };
};

export default actionResetFilterSuit;
