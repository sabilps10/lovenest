import {FLORIST_CATEGORIES} from '../type';

const floristCategoriesFilterAction = (categories, categoryName) => {
  return {
    type: FLORIST_CATEGORIES,
    payload: {
      categories,
      categoryName,
    },
  };
};

export default floristCategoriesFilterAction;
