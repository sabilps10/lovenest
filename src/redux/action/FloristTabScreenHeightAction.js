import {FLORIST_TAB} from '../type';

const floristTabScreenheightAction = height => {
  return {
    type: FLORIST_TAB,
    payload: {
      height,
    },
  };
};

export default floristTabScreenheightAction;
