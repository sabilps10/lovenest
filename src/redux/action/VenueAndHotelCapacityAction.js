import {VENUE_AND_HOTEL_CAPACITY} from '../type';

const venueAndHotelCapacityAction = capacities => {
  return {
    type: VENUE_AND_HOTEL_CAPACITY,
    payload: {
      capacities,
    },
  };
};

export default venueAndHotelCapacityAction;
