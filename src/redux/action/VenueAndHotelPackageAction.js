import {VENUE_AND_HOTEL_PACKAGES} from '../type';

const venueAndHotelPackagesAction = packages => {
  return {
    type: VENUE_AND_HOTEL_PACKAGES,
    payload: {
      packages,
    },
  };
};

export default venueAndHotelPackagesAction;
