import {FLORIST_SIZE} from '../type';

const floristSizeFilterAction = size => {
  return {
    type: FLORIST_SIZE,
    payload: {
      size,
    },
  };
};

export default floristSizeFilterAction;
