import {FILTER_SUIT_PRODUCT} from '../../type/Suit/index';

const actionFilterSuit = list => {
  return {
    type: FILTER_SUIT_PRODUCT,
    payload: {
      list,
    },
  };
};

export default actionFilterSuit;
