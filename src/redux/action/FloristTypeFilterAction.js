import {FLORIST_TYPE} from '../type';

const floristTypeFilterAction = type => {
  return {
    type: FLORIST_TYPE,
    payload: {
      type,
    },
  };
};
export default floristTypeFilterAction;
