import {FLORIST_THEME} from '../type/index';

const floristThemeFilterAction = themes => {
  return {
    type: FLORIST_THEME,
    payload: {
      themes,
    },
  };
};

export default floristThemeFilterAction;
