import {VENUE_AND_HOTEL_SHORT_PRICE} from '../type';

const venueAndHotelShortPriceAction = shortBy => {
  return {
    type: VENUE_AND_HOTEL_SHORT_PRICE,
    payload: {
      shortBy,
    },
  };
};

export default venueAndHotelShortPriceAction;
