import {RESET_FILTER_PROJECT} from '../type';
const resetProjectFilter = () => {
  return {
    type: RESET_FILTER_PROJECT,
  };
};
export default resetProjectFilter;