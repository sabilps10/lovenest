import {FILTER_ACCESSORIES_STATUS} from '../type';
const filterAccessoriesStatusAction = status => {
  return {
    type: FILTER_ACCESSORIES_STATUS,
    payload: {
      status,
    },
  };
};

export default filterAccessoriesStatusAction;
