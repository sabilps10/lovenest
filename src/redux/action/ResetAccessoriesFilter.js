import {RESET_FILTER_ACCESSORIES} from '../type';
const resetProductFilter = () => {
  return {
    type: RESET_FILTER_ACCESSORIES,
  };
};
export default resetProductFilter;
