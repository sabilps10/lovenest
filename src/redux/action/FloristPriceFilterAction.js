import {FLORIST_PRICE} from '../type';

const floristPriceFilterAction = prices => {
  return {
    type: FLORIST_PRICE,
    payload: {
      prices,
    },
  };
};

export default floristPriceFilterAction;
