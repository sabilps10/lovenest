import {FLORIST_FILTER_STATUS} from '../type';

const floristFilterStatusAction = status => {
  return {
    type: FLORIST_FILTER_STATUS,
    payload: {
      status,
    },
  };
};

export default floristFilterStatusAction;
