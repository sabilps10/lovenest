import gql from 'graphql-tag';

const saveTokenNotification = gql`
  query saveTokenNotification($token: String) {
    saveTokenNotification(token: $token) {
      saved
    }
  }
`;

export default saveTokenNotification;
