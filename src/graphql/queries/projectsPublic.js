import gql from 'graphql-tag';

const projectsPublic = gql`
  query projectsPublic(
    $serviceType: String!
    $merchantId: [Int]
    $venueType: [String]
    $interiorType: [String]
    $interiorStyle: [String]
    $pageSize: Int
    $pageNumber: Int
  ) {
    projectsPublic(
      serviceType: $serviceType
      merchantId: $merchantId
      venueType: $venueType
      interiorType: $interiorType
      interiorStyle: $interiorStyle
      pageSize: $pageSize
      pageNumber: $pageNumber
    ) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
        }
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          coverImageBlobKey
          logoImageUrl
          logoImageDynamicUrl
          logoImageBlobKey
          name
          serviceType
          tagline
          tagline
          description
          email
          initial
          facebookLink
          pinterestLink
          youtubeLink
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
            notes
            __typename
          }
          extraPhoto {
            id
            filename
            url
            dynamicUrl
            blobKey
            mimetype
            view
            width
            height
            orientation
            createdOn
            updatedOn
            deleted
          }
        }
        description
        featuredMerchant {
          id
          coverImageUrl
          coverImageDynamicUrl
          coverImageBlobKey
          logoImageUrl
          logoImageDynamicUrl
          logoImageBlobKey
          name
          serviceType
          tagline
          description
          initial
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
            notes
            __typename
          }
          extraPhoto {
            id
            filename
            url
            dynamicUrl
            blobKey
            mimetype
            view
            width
            height
            orientation
            createdOn
            updatedOn
            deleted
          }
          createdOn
        }
        featuredProduct {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          publicProduct
          name
          merchantDetails {
            id
            coverImageUrl
            coverImageDynamicUrl
            coverImageBlobKey
            logoImageUrl
            logoImageDynamicUrl
            logoImageDynamicUrl
            logoImageBlobKey
            name
            serviceType
            tagline
            description
            email
            initial
            facebookLink
            instagramLink
            youtubeLink
            pinterestLink
            createdOn
            addresses {
              id
              locationName
              address
              city
              country
              postCode
              latitude
              longitude
              countryCode
              phone
              notes
              __typename
            }
            extraPhoto {
              id
              filename
              url
              dynamicUrl
              blobKey
              mimetype
              view
              width
              height
              orientation
              createdOn
              updatedOn
              deleted
            }
          }
          description
        }
        bridalTheme
        interiorRoomType
        interiorRoomSize
        interiorRoomStyle
        interiorWorkDuration
        hotelVenueType
        hotelCapacity
        price
        updatedOn
        createdOn
        deleted
      }
      error
      totalData
    }
  }
`;

export default projectsPublic;
