import gql from 'graphql-tag';

const checkTokenExpired = gql`
  query {
    checkTokenExpired {
      status
    }
  }
`;

export default checkTokenExpired;
