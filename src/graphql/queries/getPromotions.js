import gql from 'graphql-tag';

const getPromotions = gql`
  query getPromotions($merchantId: Int) {
    getPromotions(merchantId: $merchantId) {
      id
      merchantId
      merchantName
      promoLinkURL
      promoImageURL
      promoImageDynamicURL
      name
      startDate
      endDate
      description
    }
  }
`;

export default getPromotions;
