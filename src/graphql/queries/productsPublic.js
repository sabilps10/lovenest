import gql from 'graphql-tag';

const productsPublic = gql`
  query productsPublic(
    $serviceType: String!
    $merchantId: [Int]
    $bridalType: [String]
    $bridalCategory: [String]
    $bridalColor: [String]
    $bridalRange: [String]
    $accsCategory: [String]
    $accsDetails: [String]
    $accsRange: [String]
    $jewelleryType: [String]
    $smartHomeCategory: [String]
    $pageSize: Int
    $pageNumber: Int
  ) {
    productsPublic(
      serviceType: $serviceType
      merchantId: $merchantId
      bridalType: $bridalType
      bridalCategory: $bridalCategory
      bridalColor: $bridalColor
      bridalRange: $bridalRange
      accsCategory: $accsCategory
      accsDetails: $accsDetails
      accsRange: $accsRange
      jewelleryType: $jewelleryType
      smartHomeCategory: $smartHomeCategory
      pageSize: $pageSize
      pageNumber: $pageNumber
    ) {
      data {
        id
        type
        isWishlist
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          __typename
        }
        publicProduct
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          coverImageBlobKey
          logoImageUrl
          logoImageDynamicUrl
          logoImageBlobKey
          name
          serviceType
          tagline
          description
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
          }
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          deleted
          updatedOn
          initial
        }
        description
        relatedProjects {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          galleries {
            id
            filename
            url
            dynamicUrl
            blobKey
            featured
            temporary
            createdOn
            __typename
          }
          name
          description
          featuredMerchant {
            id
            coverImageUrl
            coverImageDynamicUrl
            coverImageBlobKey
            logoImageUrl
            logoImageDynamicUrl
            logoImageBlobKey
            name
            serviceType
            tagline
            description
            addresses {
              id
              locationName
              address
              city
              country
              postCode
              latitude
              longitude
              countryCode
              phone
            }
            email
          }
          featuredProduct {
            id
            type
            featuredImageURL
            featuredImageDynamicURL
            publicProduct
            name
            description
          }
          bridalTheme
          interiorRoomType
          interiorRoomSize
          interiorWorkDuration
          hotelVenueType
          hotelCapacity
          price
        }
        createdOn
        updatedOn
        deleted
        productCode
        bridalCategory
        bridalType
        bridalSize
        bridalQuantity
        bridalSpecFabric
        bridalSpecColour
        bridalSpecDetails
        jewelType
        jewelMetal
        jewelSettingType
        jewelWeight
        jewelDiamondInformation {
          cutType
          total
          weight
        }
        jewelGemsInformation {
          gemsType
          total
          weight
        }
        smartHomeCategory
        smartHomePrice
        hotelPrice
        hotelPackageDetails
        hotelCapacity
        makeupLookType
        makeupLookDesc
        artistName
        artistDesc
        photographyThemeName
        photographyThemeDescription
        photographyPhotographerName
        photographyPhotographerDescription
        accsType
        accsCategory
        accsDetails
        accsRange
        __typename
      }
      error
      totalData
    }
  }
`;

export default productsPublic;
