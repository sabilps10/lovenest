import gql from 'graphql-tag';

const projectsFilter = gql`
  query projectsFilter($serviceType: String!, $merchantId: Int) {
    projectsFilter(serviceType: $serviceType, merchantId: $merchantId) {
      data {
        name
        list {
          name
          checked
        }
      }
      error
    }
  }
`;

export default projectsFilter;
