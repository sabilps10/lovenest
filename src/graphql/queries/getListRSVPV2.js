import gql from 'graphql-tag';

const getListRSVPV2 = gql`
  query getInvitationSummaryByTemplateV2(
    $group: Int
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getInvitationSummaryByTemplateV2(
      group: $group
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        templateId
        type
        imageUrl
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
        createdOn
        updatedOn
        totalInvitation
        totalAttendance
      }
      error
      totalCount
      totalInvitation
      totalAttendance
    }
  }
`;

export default getListRSVPV2;
