import gql from 'graphql-tag';

// get List Menu Home PAge
const merchantCategoryMobile = gql`
  query {
    merchantCategoryMobile {
      list1 {
        id
        menuName
        menu
        icon
      }
      list2 {
        id
        menuName
        menu
        icon
      }
      list3 {
        id
        menuName
        menu
        icon
      }
      error
    }
  }
`;

export default merchantCategoryMobile;
