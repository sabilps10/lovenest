import gql from 'graphql-tag';

const productsFilter = gql`
  query productsFilter($merchantId: Int, $serviceType: String!) {
    productsFilter(merchantId: $merchantId, serviceType: $serviceType) {
      data {
        name
        list {
          name
          checked
        }
      }
      error
    }
  }
`;

export default productsFilter;
