import gql from 'graphql-tag';

const venueHotelCapacityFilter = gql`
  query {
    venueHotelCapacity {
      data
      error
    }
  }
`;

export default venueHotelCapacityFilter;
