import gql from 'graphql-tag';

const getPromotion = gql`
  query getPromotion($id: Int) {
    getPromotion(id: $id) {
      id
      promoImageURL
      promoImageDynamicURL
      promoImageBlobKey
      promoLinkURL
      merchantId
      merchantName
      createdOn
      updatedOn
      name
      startDate
      endDate
      description
    }
  }
`;

export default getPromotion;
