import gql from 'graphql-tag';

const getAllStaffMobile = gql`
  query getAllStaffMobile($merchantId: Int) {
    getAllStaffMobile(merchantId: $merchantId) {
      data {
        id
        name
        role
      }
      totalCount
      error
    }
  }
`;

export default getAllStaffMobile;
