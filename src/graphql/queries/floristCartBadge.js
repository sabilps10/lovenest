import gql from 'graphql-tag';

const floristCartBadge = gql`
  query {
    cartBadge {
      totalCount
      error
    }
  }
`;

export default floristCartBadge;
