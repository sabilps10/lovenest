import gql from 'graphql-tag';

const productSizeRange = gql`
  query productSizeRange(
    $type: String!
    $merchantId: Int
    $floristCategory: String
  ) {
    productSizeRange(
      type: $type
      merchantId: $merchantId
      floristCategory: $floristCategory
    ) {
      data {
        max
        min
      }
      error
    }
  }
`;

export default productSizeRange;