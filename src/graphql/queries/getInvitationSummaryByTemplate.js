import gql from 'graphql-tag';

const getListSummaryInvitation = gql`
  query getInvitationSummaryByTemplate(
    $searchBy: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getInvitationSummaryByTemplate(
      searchBy: $searchBy
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        name
        url
        dynamicUrl
        totalInvitation
        totalAttendance
        groomName
        brideName
        weddingDate
        location
        textCollor
        createdOn
      }
      totalInvitation
      totalAttendance
      totalCount
      error
    }
  }
`;

export default getListSummaryInvitation;
