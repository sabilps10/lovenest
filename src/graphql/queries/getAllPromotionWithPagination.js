import gql from 'graphql-tag';

const getAllPromotions = gql`
  query getAllPromotions(
    $itemDisplayed: Int
    $pageNumber: Int
    $upcoming: Boolean
  ) {
    getAllPromotions(
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
      upcoming: $upcoming
    ) {
      data {
        id
        promoLinkURL
        promoImageURL
        promoImageDynamicURL
        merchantId
        merchantName
        name
        startDate
        endDate
        description
      }
      error
      totalData
    }
  }
`;

export default getAllPromotions;
