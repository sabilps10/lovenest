import gql from 'graphql-tag';

const loveAndWin = gql`
  query {
    loveAndWin {
      data {
        id
        startDate
        endDate
        totalCash
        status
        merchants {
          id
          name
          logoImageUrl
        }
        submission {
          id
          chanceNumber
          loveWinId
          serviceType
          merchantId
          totalAmount
          datePurchase
          depositAmount
        }
        location {
          id
          locationName
          address
          city
          postCode
          longitude
          latitude
        }
        terms
      }
      error
    }
  }
`;

export default loveAndWin;
