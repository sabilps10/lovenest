import gql from 'graphql-tag';

const getProject = gql`
  query getProject($id: Int!) {
    getProject(id: $id) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          featured
          temporary
          createdOn
        }
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          description
          address
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        description
        featuredMerchant {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          description
          address
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        featuredProduct {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          publicProduct
          name
          merchantDetails {
            id
            coverImageUrl
            coverImageDynamicUrl
            logoImageUrl
            logoImageDynamicUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
          description
        }
        bridalTheme
        interiorRoomType
        interiorRoomSize
        interiorRoomStyle
        interiorWorkDuration
        hotelVenueType
        hotelCapacity
        updatedOn
        createdOn
        deleted
        floristTheme
        floristType
      }
      error
    }
  }
`;

export default getProject;
