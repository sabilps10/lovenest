import gql from 'graphql-tag';

const venueHotelRegionFilter = gql`
  query {
    venueHotelRegions {
      data
      error
    }
  }
`;
export default venueHotelRegionFilter;
