import gql from 'graphql-tag';

const serviceTypes = gql`
  query {
    serviceTypes {
      data
      error
    }
  }
`;

export default serviceTypes;
