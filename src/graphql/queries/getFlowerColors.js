import gql from 'graphql-tag';

const floristColor = gql`
  query floristSpecColor($floristCategory: String, $merchantId: Int) {
    floristSpecColor(
      floristCategory: $floristCategory
      merchantId: $merchantId
    ) {
      data {
        name
      }
      error
    }
  }
`;

export default floristColor;
