import gql from 'graphql-tag';

const floristThemes = gql`
  query floristThemes($floristCategory: String, $merchantId: Int) {
    floristThemes(floristCategory: $floristCategory, merchantId: $merchantId) {
      data {
        name
      }
      error
    }
  }
`;

export default floristThemes;
