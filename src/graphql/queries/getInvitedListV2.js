import gql from 'graphql-tag';

const getInvitedListV2 = gql`
  query getCustomerWeddingInvitationsV2(
    $templateId: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getCustomerWeddingInvitationsV2(
      templateId: $templateId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        uniqueId
        invitationUrl
        name
        phoneNo
        email
        gender
        paxType
        group {
          id
          name
        }
        note
        tableType
        totalSeat
        sendStatus
        attendances {
          id
          name
          phoneNo
          email
          rsvpStatus
          vaccinatedSwab
          gender
          paxType
          group {
            id
            name
          }
          note
          deleted
          createdOn
        }
        valid
        selected
        deleted
        createdOn
      }
      error
      totalCount
    }
  }
`;

export default getInvitedListV2;
