import gql from 'graphql-tag';

const getCustomerInvitationDraft = gql`
  query getCustomerInvitationDraft($itemDisplayed: Int, $pageNumber: Int) {
    getCustomerInvitationDraft(
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        templateName
        template {
          id
          name
          id
          dynamicUrl
          url
        }
        contactSelected {
          id
          name
          phoneNo
          valid
          selected
        }
        frontImage {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          createdOn
          updatedOn
          deleted
        }
        backImage {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          createdOn
          updatedOn
          deleted
        }
        brideName
        groomName
        weddingDate
        location
        eventTime {
          eventName
          startTime
          endTime
        }
        textCollor
      }
      totalCount
      error
    }
  }
`;

export default getCustomerInvitationDraft;
