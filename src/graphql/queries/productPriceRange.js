import gql from 'graphql-tag';

const productPriceRange = gql`
  query productPriceRange(
    $type: String!
    $merchantId: Int
    $floristCategory: String
  ) {
    productPriceRange(
      type: $type
      merchantId: $merchantId
      floristCategory: $floristCategory
    ) {
      data {
        max
        min
      }
      error
    }
  }
`;

export default productPriceRange;
