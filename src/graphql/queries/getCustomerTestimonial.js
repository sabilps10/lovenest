import gql from 'graphql-tag';

const getCustomerTestimonial = gql`
  query getCustomerTestimonial(
    $merchantId: Int
    $searchBy: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getCustomerTestimonial(
      merchantId: $merchantId
      searchBy: $searchBy
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        source
        merchantId
        testimonial
        serviceRating
        productRating
        appRating
        review1
        review2
        reviewGallery {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
        }
        customer {
          id
          profileImage
          name
        }
        createdOn
      }
      totalCount
      error
    }
  }
`;

export default getCustomerTestimonial;
