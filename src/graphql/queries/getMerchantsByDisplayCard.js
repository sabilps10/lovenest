import gql from 'graphql-tag';

const getMerchantsByDisplayCard = gql`
  query getMerchantsByDisplayCard(
    $serviceType: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getMerchantsByDisplayCard(
      serviceType: $serviceType
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        coverImageUrl
        coverImageDynamicUrl
        logoImageUrl
        logoImageDynamicUrl
        video {
          id
          filename
          url
          mimetype
          encoding
          createdOn
        }
        extraPhoto {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          width
          height
          orientation
          createdOn
          updatedOn
          deleted
        }
        displayCard {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
          merchant {
            id
            name
          }
          deleted
        }
        averageRating
        region
        capacity
        rateCards {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          createdOn
          updatedOn
          deleted
        }
        promotions {
          id
          promoImageURL
          promoImageDynamicURL
          promoImageBlobKey
          promoLinkURL
          merchantId
          merchantName
          createdOn
          updatedOn
          name
          startDate
          endDate
          description
        }
        totalRating
        name
        serviceType
        tagline
        description
        address
        phone
        email
        facebookLink
        instagramLink
        pinterestLink
        youtubeLink
        createdOn
        updatedOn
        deleted
      }
      error
      totalData
    }
  }
`;

export default getMerchantsByDisplayCard;
