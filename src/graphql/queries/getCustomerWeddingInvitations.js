import gql from 'graphql-tag';

const getCustomerWeddingInvitation = gql`
  query getCustomerWeddingInvitations(
    $templateId: Int
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getCustomerWeddingInvitations(
      templateId: $templateId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        name
        phoneNo
        invitationUrl
        attendances {
          id
          name
          phoneNo
          email
          rsvpStatus
          vaccinatedSwab
          deleted
          createdOn
        }
      }
      totalCount
      error
    }
  }
`;

export default getCustomerWeddingInvitation;
