import gql from 'graphql-tag';

const productOrderAddress = gql`
  query productOrderAddress($type: String) {
    productOrderAddress(type: $type) {
      data {
        id
        locationName
        address
        city
        country
        postCode
        latitude
        longitude
        countryCode
        phone
        notes
      }
      error
    }
  }
`;

export default productOrderAddress;
