import gql from 'graphql-tag';

const getOrderPayments = gql`
  query getOrderPayments(
    $pageSize: Int
    $pageNumber: Int
    $isCompleted: Boolean!
  ) {
    getOrderPayments(
      pageSize: $pageSize
      pageNumber: $pageNumber
      isCompleted: $isCompleted
    ) {
      data {
        order {
          id
          number
          referenceOrderNumber
          name
          total
          subTotal
          gstTax
          totalItem
          merchant {
            id
            coverImageUrl
            coverImageDynamicUrl
            logoImageUrl
            logoImageDynamicUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            createdOn
            updatedOn
            deleted
            initial
            __typename
          }
          customer {
            id
            name
            countryCode
            phone
            mobile
            email
            gender
            partnerName
            partnerCountryCode
            partnerPhone
            partnerMobile
            partnerEmail
            partnerGender
            marriageDate
            keyCollectionDate
            profileImage
            isFacebook
            createdOn
            updatedOn
            deleted
          }
          packageLists {
            id
            name
            totalItem
            totalPrice
            settingPackageId
            itemLists {
              id
              packageName
              packageItemName
              price
              quantity
              appointmentInvitation
              subtotal
              description
              appointment
              settingPackageItemId
              appointmentDetails {
                id
                status
                rejectReason
                date
                time
                timeslot
                orderId
                orderNumber
                packageId
                packageName
                packageItemName
                merchantId
                customerId
                activity
                createdOn
                updatedOn
                merchantDetails {
                  id
                  coverImageUrl
                  coverImageDynamicUrl
                  logoImageUrl
                  logoImageDynamicUrl
                  name
                  serviceType
                  tagline
                  description
                  address
                  phone
                  email
                  facebookLink
                  instagramLink
                  pinterestLink
                  youtubeLink
                  createdOn
                  updatedOn
                  deleted
                }
                __typename
              }
              __typename
            }
            __typename
          }
          additionalItems {
            id
            name
            quantity
            price
            subtotal
            description
            createdOn
            updatedOn
            deleted
            __typename
          }
          issuedDate
          createdOn
          updatedOn
          orderType
          ticketQuantity
          eventDetails {
            id
            eventBannerURL
            eventBannerDynamicURL
            name
            description
            venueName
            address1
            address2
            city
            country
            postCode
            latitude
            longitude
            date
            startTime
            endTime
            isFeatured
            freeGift
            retailPrice
            promoPrice
            summary
            __typename
          }
          paymentList {
            id
            orderId
            paymentNumber
            receiptNumber
            referenceNumber
            receiptNumber
            referenceUrl
            paidDate
            amount
            preAmount
            amountDue
            balance
            description
            source
            outlet
            status
            paymentType
            refund {
              id
              amount
              reason
              type
              referenceNumber
              createdOn
              updatedOn
              deleted
            }
            createdOn
            updatedOn
            deleted
          }
          totalPaid
          balance
          status
          __typename
        }
        merchant {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          description
          address
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
          initial
        }
        totalPaid
        balance
        paymentList {
          id
          orderId
          paymentNumber
          receiptNumber
          referenceNumber
          receiptNumber
          referenceUrl
          paidDate
          amount
          preAmount
          amountDue
          balance
          description
          source
          outlet
          status
          paymentType
          refund {
            id
            amount
            reason
            type
            referenceNumber
            createdOn
            updatedOn
            deleted
          }
          createdOn
          updatedOn
          deleted
        }
      }
      error
      totalCount
    }
  }
`;

export default getOrderPayments;
