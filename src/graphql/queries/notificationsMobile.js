import gql from 'graphql-tag';

const notificationsMobile = gql`
  query notificationsMobile(
    $type: String!
    $filter: String
    $pageSize: Int
    $pageNumber: Int
  ) {
    notificationsMobile(
      type: $type
      filter: $filter
      pageSize: $pageSize
      pageNumber: $pageNumber
    ) {
      data {
        photoAlbumActivies {
          id
        }
        id
        type
        createdOn
        message
        merchant {
          id
          serviceType
          logoImageUrl
          logoImageDynamicUrl
          coverImageUrl
          coverImageDynamicUrl
          name
          tagline
        }
        update {
          id
          title
          description
          createdOn
          event {
            id
            eventBannerURL
            eventBannerDynamicURL
            name
            description
          }
          promotion {
            id
            merchantId
            merchantName
            promoLinkURL
            promoImageURL
            promoImageDynamicURL
            name
            description
          }
          others {
            id
            title
            description
            createdOn
          }
          merchant {
            id
            name
            serviceType
            coverImageUrl
            coverImageDynamicUrl
            tagline
          }
          notificationDate
        }
        appointment {
          id
          status
          message
          merchantDetails {
            id
            name
            serviceType
            coverImageUrl
            coverImageDynamicUrl
            tagline
          }
        }
        order {
          id
          name
          status
          orderStatus
          orderType
          merchant {
            id
            name
            serviceType
            description
            tagline
          }
          packageLists {
            id
            name
            totalItem
            totalPrice
            settingPackageId
          }
        }
        isRead
      }
      totalCount
      error
    }
  }
`;

export default notificationsMobile;
