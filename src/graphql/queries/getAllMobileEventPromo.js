import gql from 'graphql-tag';

const getMobileEventPromo = gql`
  query getMobileEventPromo($itemDisplayed: Int, $pageNumber: Int) {
    getMobileEventPromo(
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        date
        notifications {
          title
          description
          createdOn
          event {
            id
            eventBannerURL
            eventBannerDynamicURL
            name
            description
            venueName
            address1
            address2
            city
            country
            postCode
            latitude
            longitude
            date
            endDate
            startTime
            endTime
            isFeatured
            freeGift
            retailPrice
            promoPrice
            summary
            createdOn
            updatedOn
          }
          promotion {
            id
            promoImageURL
            promoImageDynamicURL
            promoLinkURL
            merchantId
            merchantName
            createdOn
            updatedOn
            name
            startDate
            endDate
          }
          others {
            title
            description
            createdOn
          }
        }
      }
      totalCount
      error
    }
  }
`;

export default getMobileEventPromo;
