import gql from 'graphql-tag';

const getCardToken = gql`
    query getCardToken (
        $number: String!
        $expMonth: Int!
        $expYear: Int!
        $cvc: String!
    ) {
        getCardToken(number: $number, expMonth: $expMonth, expYear: $expYear, cvc: $cvc) {
        data{
            id
        }
        error
        }
    }
`;

export default getCardToken;
