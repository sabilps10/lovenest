import gql from 'graphql-tag';

const exportWeddingAttendancePdf = gql`
  query exportWeddingAttendancePdf($templateId: Int) {
    exportWeddingAttendancePdf(templateId: $templateId) {
      data {
        id
        name
        dynamicUrl
      }
      error
    }
  }
`;
export default exportWeddingAttendancePdf;
