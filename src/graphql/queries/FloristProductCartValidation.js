import gql from 'graphql-tag';

const productCardValidation = gql`
  query productCartValidation($productId: Int!) {
    productCartValidation(productId: $productId) {
      validationStatus
      error
    }
  }
`;

export default productCardValidation;
