import gql from 'graphql-tag';

const getAllOrdersGroupByDate = gql`
  query getAllOrdersGroupByDate($orderType: String, $pageSize: Int) {
    getAllOrdersGroupByDate(orderType: $orderType, pageSize: $pageSize) {
      totalCount
      data {
        date
        listOrder {
          id
          number
          name
          total
          createdOn
          interiorPackageList {
            id
            name
            totalItem
            totalPrice
            settingPackageId
            interiorOrderProducts {
              id
              name
              quantity
              attachment {
                id
                filename
                url
                dynamicUrl
                blobKey
                mimetype
              }
              createdOn
              updatedOn
            }
            personInCharges {
              id
              designation
              designationId
              userName
              userId
              userRole
              rate {
                id
                name
                rate
                totalHours
                deleted
              }
              createdOn
            }
            itemLists {
              id
              packageId
              packageName
              packageItemDescription
              location
              area
              price
            }
          }
          merchant {
            id
            coverImageUrl
            coverImageDynamicUrl
            logoImageUrl
            logoImageDynamicUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
          customer {
            id
            name
            countryCode
            phone
            mobile
            email
            gender
            partnerName
            partnerCountryCode
            partnerPhone
            partnerMobile
            partnerEmail
            partnerGender
            marriageDate
            profileImage
            isFacebook
            createdOn
            updatedOn
            deleted
          }
          packageLists {
            id
            name
            totalItem
            totalPrice
            itemLists {
              id
              packageName
              packageItemName
              price
              quantity
              subtotal
              description
              appointment
              appointmentDetails {
                id
                status
                date
                time
                timeslot
                rejectReason
                orderId
                orderNumber
                packageId
                packageName
                packageItemId
                packageItemName
                merchantId
                customerId
                activity
                createdOn
                updatedOn
              }
            }
          }
          orderType
          ticketQuantity
          eventDetails {
            id
            eventBannerURL
            eventBannerDynamicURL
            name
            description
            venueName
            address1
            address2
            city
            country
            postCode
            latitude
            longitude
            date
            startTime
            endTime
            isFeatured
            createdOn
            updatedOn
          }
          additionalItems {
            id
            name
            quantity
            price
            subtotal
            description
            createdOn
            updatedOn
            deleted
            __typename
          }
          paymentList {
            id
            orderId
            paymentNumber
            receiptNumber
            referenceNumber
            receiptNumber
            referenceUrl
            paidDate
            amount
            preAmount
            amountDue
            balance
            description
            source
            outlet
            status
            paymentType
            refund {
              id
              amount
              reason
              type
              referenceNumber
              createdOn
              updatedOn
              deleted
            }
            createdOn
            updatedOn
            deleted
          }
          totalPaid
          balance
          status
          productsOrder {
            id
            type
            orderQty
            featuredImageURL
            featuredImageDynamicURL
            galleries {
              id
              filename
              url
              dynamicUrl
              blobKey
              featured
              temporary
              createdOn
              order
            }
            publicProduct
            name
            merchantDetails {
              id
              coverImageUrl
              coverImageDynamicUrl
              logoImageUrl
              logoImageDynamicUrl
              name
              serviceType
              tagline
              description
              address
              phone
              email
              facebookLink
              instagramLink
              pinterestLink
              youtubeLink
              createdOn
              updatedOn
              deleted
            }
            description
            relatedProjects {
              id
              type
              featuredImageURL
              featuredImageDynamicURL
              name
            }
            createdOn
            updatedOn
            deleted
          }
          productOrderStatus {
            id
            orderStatus
            notes
            createdOn
          }
          orderStatus
          orderDeliveryType
          orderDeliveryDate
          orderDeliveryTime
          orderDeliveryAddress {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
            notes
          }
          orderDeliveryTimeslot {
            id
            timeslot
            orderQty
            type
            status
            availabilty
            updatedOn
          }
          stepsStatus
          interiorHandoverDate
        }
      }
      error
    }
  }
`;

export default getAllOrdersGroupByDate;
