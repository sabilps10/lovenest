import gql from 'graphql-tag';

const floristCategory = gql`
  query floristCategories($merchantId: Int) {
    floristCategories(merchantId: $merchantId) {
      data {
        name
        total
      }
      error
    }
  }
`;

export default floristCategory;
