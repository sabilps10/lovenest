import gql from 'graphql-tag';

const productsPopular = gql`
  query productsPopular(
    $serviceType: String!
    $merchantId: [Int]
    $bridalType: [String]
    $pageSize: Int
    $pageNumber: Int
  ) {
    productsPopular(
      serviceType: $serviceType
      merchantId: $merchantId
      pageSize: $pageSize
      bridalType: $bridalType
      pageNumber: $pageNumber
    ) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        isPopular
        name
        description
        productCode
        floristPrice
        discount
        isWishlist
        itemPrice
        isFreeDelivery
        soldQty
        hotelPrice
        hotelCapacity
        hotelPackageDetails
        hotelPackage
        hotelRegion
        hotelAvailabilty
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          region
        }
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
        }
      }
      totalData
      error
    }
  }
`;

export default productsPopular;
