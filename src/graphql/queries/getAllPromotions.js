import gql from 'graphql-tag';

const getAllPromotions = gql`
  query getAllPromotions(
    $upcoming: Boolean
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getAllPromotions(
      upcoming: $upcoming
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        promoLinkURL
        promoImageURL
        promoImageDynamicURL
        merchantId
        merchantName
        name
        startDate
        endDate
        description
      }
      totalData
      error
    }
  }
`;

export default getAllPromotions;
