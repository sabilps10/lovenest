import gql from 'graphql-tag';

const getCustomerAlbumActivity = gql`
  query getCustomerAlbumActivity($orderId: Int!) {
    getCustomerAlbumActivity(orderId: $orderId) {
      data {
        id
        name
        date
        time
        picId
        picName
        picRole
        albumQty
        albumVersionSize
        albumVersionSizeName
        albumDecorate
        albumDecorateName
        factory
        isCompleted
        isGoToStore
        isPassInspection
        isFailedInspection
        label
        imageFolderHash
        invoice
        totalImage
        remarks
        albumCollectionDate
      }
      error
    }
  }
`;

export default getCustomerAlbumActivity;
