import gql from 'graphql-tag';

const merchantMostSearch = gql`
  query merchantMostSearch($serviceType: [String]!) {
    merchantMostSearch(serviceType: $serviceType) {
      data {
        serviceType
        merchants {
          id
          serviceType
          name
        }
      }
      error
    }
  }
`;
export default merchantMostSearch;
