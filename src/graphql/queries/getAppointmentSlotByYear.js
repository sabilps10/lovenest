import gql from 'graphql-tag';

const getAppointmentSlotByYear = gql`
  query getAppointmentSlotByYear(
    $year: String!
    $appointmentId: Int!
    $addressId: Int
  ) {
    getAppointmentSlotByYear(
      year: $year
      appointmentId: $appointmentId
      addressId: $addressId
    ) {
      year
      allMonths {
        id
        month
        monthYear
        totalDays
        datesInMonth {
          date
          isFullBooked
        }
      }
      error
    }
  }
`;

export default getAppointmentSlotByYear;
