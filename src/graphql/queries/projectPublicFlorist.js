import gql from 'graphql-tag';

const projectPublicFlorist = gql`
  query projectsPublic(
    $serviceType: String!
    $merchantId: [Int]
    $pageSize: Int
    $pageNumber: Int
  ) {
    projectsPublic(
      serviceType: $serviceType
      merchantId: $merchantId
      pageSize: $pageSize
      pageNumber: $pageNumber
    ) {
      data {
        id
        name
        description
        galleries {
          id
          filename
          url
          dynamicUrl
          featured
        }
        featuredImageDynamicURL
        featuredImageURL
        floristTheme
      }
      error
      totalData
    }
  }
`;

export default projectPublicFlorist;
