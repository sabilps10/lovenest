import gql from 'graphql-tag';

const getEventDetail = gql`
  query getEvent($id: Int) {
    getEvent(id: $id) {
      data {
        id
        isFullBooked
        timeslot {
          id
          name
          startTime
          endTime
          pax
          rsvpAmount
          rsvpOrdered
        }
        summary
        description
        eventBannerURL
        eventBannerDynamicURL
        name
        freeGift
        retailPrice
        promoPrice
        venueName
        address1
        address2
        city
        country
        postCode
        latitude
        longitude
        date
        endDate
        startTime
        endTime
        isFeatured
        createdOn
        updatedOn
      }
      totalData
      error
    }
  }
`;

export default getEventDetail;
