import gql from 'graphql-tag';

const getAllUpcomingEvents = gql`
  query getAllUpcomingEvents($pageSize: Int, $pageNumber: Int) {
    getAllUpcomingEvents(pageSize: $pageSize, pageNumber: $pageNumber) {
      data {
        id
        isFullBooked
        timeslot {
          id
          name
          startTime
          endTime
          pax
          rsvpAmount
          rsvpOrdered
        }
        summary
        eventBannerURL
        eventBannerDynamicURL
        name
        freeGift
        retailPrice
        promoPrice
        venueName
        address1
        address2
        city
        country
        postCode
        latitude
        longitude
        date
        endDate
        startTime
        endTime
        isFeatured
        createdOn
        updatedOn
      }
      error
      totalData
    }
  }
`;
export default getAllUpcomingEvents;
