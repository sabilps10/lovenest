import gql from 'graphql-tag';

const floristType = gql`
  query floristTypes($floristCategory: String, $merchantId: Int) {
    floristTypes(floristCategory: $floristCategory, merchantId: $merchantId) {
      data {
        name
      }
      error
    }
  }
`;

export default floristType;
