import gql from 'graphql-tag';

const getAllProjects = gql`
  query getAllProjects(
    $merchantId: Int
    $itemDisplayed: Int
    $pageNumber: Int
    $serviceType: String
  ) {
    getAllProjects(
      merchantId: $merchantId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
      serviceType: $serviceType
    ) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          featured
          temporary
          createdOn
        }
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          description
          address
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
          }
          countryCode
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        description
        featuredMerchant {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          description
          address
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
          }
          countryCode
          email
          facebookLink
          youtubeLink
          createdOn
          deleted
        }
        featuredProduct {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          publicProduct
          name
          merchantDetails {
            id
            coverImageUrl
            coverImageDynamicUrl
            logoImageUrl
            logoImageDynamicUrl
            name
            serviceType
            tagline
            description
            address
            addresses {
              id
              locationName
              address
              city
              country
              postCode
              latitude
              longitude
            }
            countryCode
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
          description
        }
        bridalTheme
        interiorRoomType
        interiorRoomSize
        interiorRoomStyle
        interiorWorkDuration
        hotelVenueType
        hotelCapacity
        price
        updatedOn
        createdOn
        deleted
      }
      error
      totalData
    }
  }
`;

export default getAllProjects;
