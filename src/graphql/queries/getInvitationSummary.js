import gql from 'graphql-tag';

const getInvitationSummary = gql`
  query getInvitationSummary($templateId: Int) {
    getInvitationSummary(templateId: $templateId) {
      rsvp {
        attending
        attendingPerc
        notAttending
        notAttendingPerc
        totalAttendance
      }
      vaccination {
        vaccinated
        vaccinatedPerc
        notVaccinated
        notVaccinatedPerc
        totalAttendance
      }
      totalInvitation
      error
    }
  }
`;

export default getInvitationSummary;
