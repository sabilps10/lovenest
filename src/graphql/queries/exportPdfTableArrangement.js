import gql from 'graphql-tag';

const exportPdfTableArrangement = gql`
  query exportWeddingTablesPdf($templateId: String!) {
    exportWeddingTablesPdf(templateId: $templateId) {
      data {
        id
        name
        dynamicUrl
      }
      error
    }
  }
`;

export default exportPdfTableArrangement;
