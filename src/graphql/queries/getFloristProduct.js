import gql from 'graphql-tag';

const getFloristProduct = gql`
  query productsPublic(
    $serviceType: String!
    $floristPrice: Float
    $floristSize: String
    $floristColor: [String]
    $floristType: [String]
    $floristCategory: [String]
    $pageSize: Int
    $pageNumber: Int
  ) {
    productsPublic(
      serviceType: $serviceType
      floristPrice: $floristPrice
      floristSize: $floristSize
      floristColor: $floristColor
      floristType: $floristType
      floristCategory: $floristCategory
      pageSize: $pageSize
      pageNumber: $pageNumber
    ) {
      data {
        id
        type
        discount
        floristPriceAfterDiscount
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
        }
        publicProduct
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          email
        }
        description
        relatedProjects {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          name
        }
        createdOn
        updatedOn
        deleted
        productCode
        floristSize
        floristType
        floristPrice
        floristColor
        floristCategory
        floristExtraSize {
          id
          size
          price
          selected
        }
        isWishlist
      }
      error
      totalData
    }
  }
`;

export default getFloristProduct;
