import gql from 'graphql-tag';

const getInvitationByLink = gql`
  query invitationByUniqueId($uniqueId: String!) {
    invitationByUniqueId(uniqueId: $uniqueId) {
      data {
        id
        templateId
        contacts {
          id
          uniqueId
          invitationUrl
          name
          phoneNo
          email
          gender
          paxType
          group {
            id
            name
          }
          note
          valid
          selected
        }
        elements {
          id
          text
          fontSize
          color
          fontFamily
          textAlign
          x
          y
        }
        schedules {
          eventName
          startTime
          endTime
        }
        imageUrl
        type
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
      }
      error
    }
  }
`;

export default getInvitationByLink;
