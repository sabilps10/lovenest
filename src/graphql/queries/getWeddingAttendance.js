import gql from 'graphql-tag';

const getWeddingAttendance = gql`
  query getWeddingAttendancesByTemplate(
    $templateId: String!
    $selectedIds: [Int]
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getWeddingAttendancesByTemplate(
      templateId: $templateId
      selectedIds: $selectedIds
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        name
        phoneNo
      }
      error
      totalCount
    }
  }
`;

export default getWeddingAttendance;
