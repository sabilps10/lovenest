import gql from 'graphql-tag';

const banners = gql`
  query bannersMobile(
    $tagId: Int
    $itemDisplayed: Int
    $pageNumber: Int
    $searchBy: String
  ) {
    bannersMobile(
      tagId: $tagId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
      searchBy: $searchBy
    ) {
      data {
        id
        url
        dynamicUrl
        blobKey
        tag {
          id
          ratio
        }
        link
        createdOn
        updatedOn
      }
      error
      totalCount
    }
  }
`;

export default banners;
