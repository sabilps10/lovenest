import gql from 'graphql-tag';

const getCustomer = gql`
  query($mobile: String) {
    getCustomer(mobile: $mobile) {
      id
      name
      mobile
      email
      gender
      partnerName
      partnerMobile
      partnerPhone
      partnerEmail
      partnerGender
      partnerCountryCode
      marriageDate
      marriageDate2
      profileImage
      countryCode
      phone
      keyCollectionDate
    }
  }
`;

export default getCustomer;
