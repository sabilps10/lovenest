import gql from 'graphql-tag';

const getMerchantByCategory = gql`
  query getMerchantsByCategory(
    $category: String
    $pageNumber: Int
    $itemDisplayed: Int
  ) {
    getMerchantsByCategory(
      category: $category
      pageNumber: $pageNumber
      itemDisplayed: $itemDisplayed
    ) {
      data {
        id
        coverImageUrl
        coverImageDynamicUrl
        coverImageBlobKey
        logoImageUrl
        logoImageDynamicUrl
        logoImageBlobKey
        name
        serviceType
        tagline
        description
        email
        initial
        facebookLink
        instagramLink
        pinterestLink
        youtubeLink
        addresses {
          id
          locationName
          address
          city
          country
          postCode
          latitude
          longitude
          countryCode
          phone
          notes
        }
        extraPhoto {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          width
          height
          orientation
          createdOn
          updatedOn
          deleted
        }
        video {
          id
          filename
          url
          mimetype
          encoding
          createdOn
          videothumbnail {
            id
            filename
            url
            dynamicUrl
            blobKey
            mimetype
            view
            width
            height
            orientation
            createdOn
            updatedOn
            deleted
          }
        }
        displayCard {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
          merchant {
            id
            name
          }
          deleted
        }
        products {
          id
          name
        }
        promotion {
          id
          name
        }
        rateCards {
          id
          filename
          dynamicUrl
        }
        region
        capacity
        editorChoice
        premiumMerchant
        createdOn
        updatedOn
        deleted
      }
      error
      totalData
    }
  }
`;

export default getMerchantByCategory;
