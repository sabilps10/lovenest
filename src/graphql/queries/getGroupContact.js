import gql from 'graphql-tag';

const getGroupContact = gql`
  query {
    rsvpGroups {
      data {
        id
        name
      }
      error
    }
  }
`;

export default getGroupContact;
