import gql from 'graphql-tag';

const getPaymentMethods = gql`
  query {
    getPaymentMethods {
      data {
        id
        brand
        country
        expMonth
        expYear
        funding
        last4
        isPrimary
      }
      error
    }
  }
`;

export default getPaymentMethods;
