import gql from 'graphql-tag';

const getMerchantVer2 = gql`
  query getMerchantVer2($id: Int) {
    getMerchantVer2(id: $id) {
      id
      coverImageUrl
      coverImageDynamicUrl
      logoImageUrl
      logoImageDynamicUrl
      name
      serviceType
      tagline
      description
      countryCode
      address
      addresses {
        id
        locationName
        address
        city
        country
        postCode
        latitude
        longitude
        countryCode
        phone
      }
      video {
        id
        filename
        url
        mimetype
        encoding
        videothumbnail {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          width
          height
          orientation
          createdOn
          updatedOn
          deleted
        }
        createdOn
      }
      extraPhoto {
        id
        filename
        url
        dynamicUrl
        blobKey
        mimetype
        view
        width
        height
        orientation
        createdOn
        updatedOn
        deleted
      }
      displayCard {
        id
        filename
        url
        dynamicUrl
        blobKey
        featured
        temporary
        createdOn
        order
        merchant {
          id
          name
        }
        deleted
      }
      phone
      email
      facebookLink
      instagramLink
      pinterestLink
      youtubeLink
      createdOn
      updatedOn
      deleted
      extraPhoto {
        id
        filename
        url
        dynamicUrl
        blobKey
        mimetype
        view
        width
        height
        orientation
        createdOn
        updatedOn
        deleted
      }
      promotions {
        id
        promoImageURL
        promoImageDynamicURL
        promoLinkURL
        merchantId
        merchantId
        merchantName
        createdOn
        updatedOn
        name
        startDate
        endDate
      }
      relatedMerchant {
        id
        coverImageUrl
        logoImageUrl
        logoImageDynamicUrl
        name
        serviceType
        tagline
        description
        address
        phone
        email
        facebookLink
        instagramLink
        pinterestLink
        youtubeLink
        createdOn
        updatedOn
        deleted
      }
      products {
        id
        type
        featuredImageURL
        galleries {
          id
          filename
          url
          featured
          temporary
          createdOn
        }
        publicProduct
        name
        merchantDetails {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          countryCode
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        description
        relatedProjects {
          id
          type
          featuredImageURL
          galleries {
            id
            filename
            url
            featured
            temporary
            createdOn
          }
          name
          description
          merchantDetails {
            id
            coverImageUrl
            logoImageUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
        }
        createdOn
        deleted
        bridalCategory
        bridalType
        bridalSize
        bridalQuantity
        bridalSpecFabric
        bridalSpecColour
        bridalSpecDetails
        jewelType
        jewelMetal
        jewelSettingType
        jewelWeight
        jewelDiamondInformation {
          cutType
          total
          weight
        }
        jewelGemsInformation {
          gemsType
          total
          weight
        }
        smartHomeCategory
        smartHomePrice
        hotelPrice
        hotelCapacity
        hotelPackageDetails
      }
      projects {
        id
        type
        featuredImageURL
        galleries {
          id
          filename
          url
          featured
          temporary
          createdOn
        }
        name
        merchantDetails {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        description
        featuredMerchant {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          phone
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        featuredProduct {
          id
          type
          featuredImageURL
          publicProduct
          name
          merchantDetails {
            id
            coverImageUrl
            logoImageUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            pinterestLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
          description
        }
        bridalTheme
        interiorRoomType
        interiorRoomSize
        interiorRoomStyle
        interiorWorkDuration
        hotelVenueType
        hotelCapacity
        updatedOn
        createdOn
        deleted
      }
    }
  }
`;

export default getMerchantVer2;
