import gql from 'graphql-tag';

const getImageCard = gql`
  query getImageCards($merchantId: Int, $itemDisplayed: Int, $pageNumber: Int) {
    getImageCards(
      merchantId: $merchantId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        filename
        url
        dynamicUrl
      }
      totalCount
      error
    }
  }
`;

export default getImageCard;
