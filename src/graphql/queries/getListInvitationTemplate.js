import gql from 'graphql-tag';

const getListInvitationTemplate = gql`
  query templatesInvitationMobile($itemDisplayed: Int, $pageNumber: Int) {
    templatesInvitationMobile(
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        name
        url
        dynamicUrl
        tag {
          id
          name
          ratio
        }
        status
        link
        createdOn
      }
      error
      totalCount
    }
  }
`;

export default getListInvitationTemplate;
