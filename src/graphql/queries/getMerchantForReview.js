import gql from 'graphql-tag';

const getAllMerchants = gql`
  query getAllMerchants($serviceType: String) {
    getAllMerchants(serviceType: $serviceType) {
      data {
        id
        name
        serviceType
      }
      error
      totalData
    }
  }
`;

export default getAllMerchants;
