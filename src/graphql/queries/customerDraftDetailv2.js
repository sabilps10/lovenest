import gql from 'graphql-tag';

const getDraftDetailV2 = gql`
  query customerDraft($templateId: String!) {
    customerDraft(templateId: $templateId) {
      data {
        templateId
        contacts {
          id
          name
          phoneNo
          email
          gender
          paxType
          group {
            id
            name
          }
          note
          valid
          selected
        }
        elements {
          id
          text
          fontSize
          color
          fontFamily
          fontSize
          textAlign
          x
          y
        }
        schedules {
          eventName
          startTime
          endTime
        }
        imageUrl
        type
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
      }
      error
    }
  }
`;

export default getDraftDetailV2;
