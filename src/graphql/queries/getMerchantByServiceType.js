import gql from 'graphql-tag';

const getMerchantsByServiceType = gql`
  query getMerchantsByServiceType(
    $serviceType: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getMerchantsByServiceType(
      serviceType: $serviceType
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        coverImageUrl
        coverImageDynamicUrl
        logoImageUrl
        logoImageDynamicUrl
        name
        serviceType
        tagline
        description
        address
        phone
        email
        facebookLink
        instagramLink
        pinterestLink
        youtubeLink
        createdOn
        updatedOn
        deleted
        promotion {
          id
          promoImageURL
          promoImageDynamicURL
          promoLinkURL
          merchantId
          merchantName
          createdOn
          updatedOn
        }
      }
      error
      totalData
    }
  }
`;

export default getMerchantsByServiceType;
