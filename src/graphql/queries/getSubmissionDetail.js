import gql from 'graphql-tag';

const getSubmissionDetail = gql`
  query getSubmission($id: Int!) {
    getSubmission(id: $id) {
      data {
        id
        chanceNumber
        loveWinId
        serviceType
        merchantId
        merchant {
          id
          serviceType
          name
        }
        datePurchase
        totalAmount
        depositAmount
        staff {
          id
          name
          role
        }
        attachments {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          createdOn
          deleted
          updatedOn
        }
        createdOn
        updatedOn
        deleted
      }
      error
    }
  }
`;

export default getSubmissionDetail;
