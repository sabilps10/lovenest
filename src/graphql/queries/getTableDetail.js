import gql from 'graphql-tag';

const getTable = gql`
  query getTable($id: Int!) {
    getTable(id: $id) {
      data {
        tableType
        tableName
        tableDescription
        group {
          id
          name
        }
        guestList {
          id
          name
          phoneNo
        }
      }
      error
    }
  }
`;

export default getTable;
