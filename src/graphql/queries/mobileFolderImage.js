import gql from 'graphql-tag';

const mobileFolderImage = gql`
  query mobileFolderImages(
    $hashId: String!
    $searchBy: String
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    mobileFolderImages(
      hashId: $hashId
      searchBy: $searchBy
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        filename
        url
        dynamicUrl
        blobKey
        featured
        temporary
        createdOn
        imageDate
        order
        selected
      }
      error
      totalCount
    }
  }
`;

export default mobileFolderImage;
