import gql from 'graphql-tag';

const getAllNotifications = gql`
  query getAllNotifications($pageSize: Int, $pageNumber: Int) {
    getAllNotifications(pageSize: $pageSize, pageNumber: $pageNumber) {
      totalCount
      data {
        date
        notifications {
          id
          type
          merchantId
          merchantImage
          merchantName
          customerId
          customerName
          message
          isRead
          createdOn
          merchant {
            id
            coverImageUrl
            coverImageDynamicUrl
            logoImageUrl
            logoImageDynamicUrl
            name
            serviceType
            tagline
            description
            address
            phone
            email
            facebookLink
            instagramLink
            pinterestLink
            youtubeLink
            createdOn
            updatedOn
            deleted
          }
          appointmentDetails {
            id
            date
            status
            orderId
            orderNo
            packageId
            packageName
            packageItemId
            packageItemName
            activity
            merchantDetails {
              id
              coverImageUrl
              coverImageDynamicUrl
              logoImageUrl
              logoImageDynamicUrl
              name
              serviceType
              tagline
              description
              address
              phone
              email
              facebookLink
              instagramLink
              youtubeLink
              pinterestLink
              createdOn
              updatedOn
              deleted
            }
            message
            rejectReason
            time
            timeslot
            createdOn
            updatedOn
          }
          orderDetails {
            id
            number
            name
            total
            totalItem
            merchant {
              id
              coverImageUrl
              coverImageDynamicUrl
              logoImageUrl
              logoImageDynamicUrl
              name
              serviceType
              tagline
              description
              address
              phone
              email
              facebookLink
              instagramLink
              youtubeLink
              pinterestLink
              createdOn
              updatedOn
              deleted
            }
            customer {
              id
              name
              countryCode
              phone
            }
            packageLists {
              id
              name
              totalItem
              totalPrice
              itemLists {
                id
                packageName
                packageItemName
                price
                quantity
                subtotal
                description
                appointment
              }
            }
            createdOn
            updatedOn
            orderType
            ticketQuantity
            eventDetails {
              id
              eventBannerURL
              eventBannerDynamicURL
              name
              description
              venueName
              address1
              address2
              city
              country
              postCode
              latitude
              longitude
              date
              startTime
              endTime
              isFeatured
              createdOn
              updatedOn
            }
          }
        }
      }
      error
    }
  }
`;

export default getAllNotifications;
