import gql from 'graphql-tag';

const getOrderFloristTimeSlot = gql`
  query getOrderFloristTimeslot($deliveryDate: String!, $type: DeliveryType!) {
    getOrderFloristTimeslot(deliveryDate: $deliveryDate, type: $type) {
      data {
        id
        timeslot
        orderQty
        type
        status
        availabilty
        updatedOn
      }
      error
    }
  }
`;

export default getOrderFloristTimeSlot;
