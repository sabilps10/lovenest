import gql from 'graphql-tag';

const getInvitationById = gql`
  query getInvitationById($uniqueId: String!) {
    getInvitationById(uniqueId: $uniqueId) {
      data {
        id
        frontImage {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          createdOn
          updatedOn
          deleted
        }
        backImage {
          id
          filename
          url
          dynamicUrl
          blobKey
          mimetype
          view
          createdOn
          updatedOn
          deleted
        }
        uniqueId
        invitationUrl
        name
        phoneNo
        email
        brideName
        groomName
        weddingDate
        weddingTime
        location
        textCollor
        sendStatus
        eventTime {
          eventName
          startTime
          endTime
        }
        template {
          id
          url
          dynamicUrl
          blobKey
          tag {
            id
            name
            ratio
          }
          status
          link
          createdOn
          updatedOn
        }
        attendances {
          id
          name
          phoneNo
          email
          rsvpStatus
          vaccinatedSwab
          deleted
          createdOn
        }
        deleted
        createdOn
      }
      error
    }
  }
`;

export default getInvitationById;
