import gql from 'graphql-tag';

const getPromotionByMerchantId = gql`
  query getAllPromotions(
    $upcoming: Boolean
    $merchantId: Int
    $itemDisplayed: Int
    $pageNumber: Int
  ) {
    getAllPromotions(
      upcoming: $upcoming
      merchantId: $merchantId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        merchantId
        merchantName
        promoLinkURL
        promoImageURL
        promoImageDynamicURL
        name
        startDate
        endDate
        description
      }
      error
      totalData
    }
  }
`;

export default getPromotionByMerchantId;
