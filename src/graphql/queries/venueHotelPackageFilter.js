import gql from 'graphql-tag';

const venueHotelPackageFilter = gql`
  query {
    venueHotelPackages {
      data
      error
    }
  }
`;

export default venueHotelPackageFilter;
