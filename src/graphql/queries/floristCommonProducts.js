import gql from 'graphql-tag';

const productsPublic = gql`
  query productsPublic(
    $serviceType: String!
    $merchantId: [Int]
    $isRandom: Boolean
    $floristPrice: Float
    $floristSize: String
    $floristCategory: [String]
    $floristType: [String]
    $floristColor: [String]
    $floristTheme: [String]
    $pageSize: Int
    $pageNumber: Int
    $priceRange: InputRange
    $sizeRange: InputRange
    $sortPrice: SortPrice
    $hotelPackage: [String]
    $hotelRegion: [String]
    $hotelCapacity: [String]
    $hotelAvailabilty: [String]
  ) {
    productsPublic(
      serviceType: $serviceType
      merchantId: $merchantId
      isRandom: $isRandom
      floristPrice: $floristPrice
      floristSize: $floristSize
      floristCategory: $floristCategory
      floristType: $floristType
      floristColor: $floristColor
      floristTheme: $floristTheme
      pageSize: $pageSize
      pageNumber: $pageNumber
      priceRange: $priceRange
      sizeRange: $sizeRange
      sortPrice: $sortPrice
      hotelPackage: $hotelPackage
      hotelRegion: $hotelRegion
      hotelCapacity: $hotelCapacity
      hotelAvailabilty: $hotelAvailabilty
    ) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        isPopular
        name
        description
        productCode
        floristPrice
        discount
        isWishlist
        itemPrice
        isFreeDelivery
        soldQty
        hotelPrice
        hotelCapacity
        hotelPackageDetails
        hotelPackage
        hotelRegion
        hotelAvailabilty
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
        }
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          logoImageUrl
          logoImageDynamicUrl
          name
          serviceType
          tagline
          region
        }
      }
      totalData
      error
    }
  }
`;

export default productsPublic;
