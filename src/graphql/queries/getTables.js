import gql from 'graphql-tag';

const getTables = gql`
  query getTables($templateId: String!, $itemDisplayed: Int, $pageNumber: Int) {
    getTables(
      templateId: $templateId
      itemDisplayed: $itemDisplayed
      pageNumber: $pageNumber
    ) {
      data {
        id
        tableType
        tableName
        tableDescription
        group {
          id
          name
        }
        guestList {
          id
          name
          phoneNo
        }
      }
      error
      totalCount
    }
  }
`;

export default getTables;
