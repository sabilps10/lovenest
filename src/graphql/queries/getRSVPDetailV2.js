import gql from 'graphql-tag';

const getRSVPDetail = gql`
  query getInvitationSummaryV2($templateId: String) {
    getInvitationSummaryV2(templateId: $templateId) {
      rsvp {
        attending
        attendingPerc
        notAttending
        notAttendingPerc
        totalAttendance
      }
      vaccination {
        vaccinated
        vaccinatedPerc
        notVaccinated
        notVaccinatedPerc
        totalAttendance
      }
      totalInvitation
      error
    }
  }
`;

export default getRSVPDetail;
