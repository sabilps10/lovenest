import gql from 'graphql-tag';

const getCustomerDraftV2 = gql`
  query customerDrafts($itemDisplayed: Int, $pageNumber: Int) {
    customerDrafts(itemDisplayed: $itemDisplayed, pageNumber: $pageNumber) {
      data {
        templateId
        contacts {
          id
          uniqueId
          invitationUrl
          name
          phoneNo
          email
          gender
          paxType
          group {
            id
            name
          }
          note
          valid
          selected
        }
        elements {
          id
          text
          fontSize
          color
          fontFamily
          fontSize
          textAlign
          x
          y
        }
        schedules {
          eventName
          startTime
          endTime
        }
        imageUrl
        type
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
      }
      error
      totalCount
    }
  }
`;

export default getCustomerDraftV2;
