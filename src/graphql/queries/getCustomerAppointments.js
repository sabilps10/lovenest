import gql from 'graphql-tag';

const getCustomerAppointments = gql`
  query getCustomerAppointments($status: String!) {
    getCustomerAppointments(status: $status) {
      data {
        id
        status
        orderId
        orderNo
        packageId
        packageName
        packageItemId
        packageItemName
        activity
        merchantDetails {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
          }
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        message
        rejectReason
        date
        time
        timeslot
        createdOn
        updatedOn
      }
      error
    }
  }
`;

export default getCustomerAppointments;
