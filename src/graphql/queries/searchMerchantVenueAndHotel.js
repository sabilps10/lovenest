import gql from 'graphql-tag';

const searchMerchantVenueAndHotel = gql`
  query getMerchantsByDisplayCard(
    $searchBy: String
    $serviceTypes: [String]
    $pageNumber: Int
    $itemDisplayed: Int
  ) {
    getMerchantsByDisplayCard(
      searchBy: $searchBy
      serviceTypes: $serviceTypes
      pageNumber: $pageNumber
      itemDisplayed: $itemDisplayed
    ) {
      data {
        id
        coverImageUrl
        coverImageDynamicUrl
        logoImageUrl
        logoImageDynamicUrl
        name
        serviceType
        tagline
        description
        extraPhoto {
          id
          filename
          url
          dynamicUrl
          orientation
        }
        video {
          id
          filename
          url
          videothumbnail {
            id
            filename
            url
            dynamicUrl
          }
        }
        displayCard {
          id
          filename
          url
          dynamicUrl
        }
      }
      error
      totalData
    }
  }
`;

export default searchMerchantVenueAndHotel;
