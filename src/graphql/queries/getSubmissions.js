import gql from 'graphql-tag';

const getSubmission = gql`
  query getSubmissions($itemDisplayed: Int, $pageNumber: Int) {
    getSubmissions(itemDisplayed: $itemDisplayed, pageNumber: $pageNumber) {
      data {
        id
        chanceNumber
        loveWinId
        merchant {
          id
          name
          serviceType
          logoImageUrl
          logoImageDynamicUrl
          coverImageUrl
          coverImageDynamicUrl
        }
        merchantId
        datePurchase
        totalAmount
        depositAmount
        staff {
          id
          name
          role
        }
        attachments {
          id
          filename
          url
          dynamicUrl
          mimetype
        }
        createdOn
        deleted
        updatedOn
      }
      error
      totalCount
    }
  }
`;

export default getSubmission;
