import gql from 'graphql-tag';

const getAllFeaturedEvents = gql`
  query {
    getAllFeaturedEvents {
      data {
        id
        summary
        eventBannerURL
        eventBannerDynamicURL
        name
        venueName
        address1
        address2
        city
        country
        postCode
        latitude
        longitude
        date
        endDate
        startTime
        endTime
        freeGift
        retailPrice
        promoPrice
        isFeatured
        createdOn
        updatedOn
      }
      totalData
      error
    }
  }
`;

export default getAllFeaturedEvents;
