import gql from 'graphql-tag';

const productsByMerchant = gql`
  query productsByMerchant(
    $merchantId: Int!
    $serviceType: String!
    $bridalType: [String]
    $bridalCategory: [String]
    $bridalColor: [String]
    $jewelleryType: [String]
    $smartHomeCategory: [String]
    $pageSize: Int
    $pageNumber: Int
    $accsCategory: [String]
    $accsDetails: [String]
    $accsRange: [String]
  ) {
    productsByMerchant(
      merchantId: $merchantId
      serviceType: $serviceType
      bridalType: $bridalType
      bridalCategory: $bridalCategory
      bridalColor: $bridalColor
      jewelleryType: $jewelleryType
      smartHomeCategory: $smartHomeCategory
      pageSize: $pageSize
      pageNumber: $pageNumber
      accsCategory: $accsCategory
      accsDetails: $accsDetails
      accsRange: $accsRange
    ) {
      data {
        id
        type
        featuredImageURL
        featuredImageDynamicURL
        galleries {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          __typename
        }
        publicProduct
        name
        merchantDetails {
          id
          coverImageUrl
          coverImageDynamicUrl
          coverImageBlobKey
          logoImageUrl
          logoImageDynamicUrl
          logoImageBlobKey
          name
          serviceType
          tagline
          description
          addresses {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
          }
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          deleted
          updatedOn
          initial
        }
        description
        relatedProjects {
          id
          type
          featuredImageURL
          featuredImageDynamicURL
          galleries {
            id
            filename
            url
            dynamicUrl
            blobKey
            featured
            temporary
            createdOn
            __typename
          }
          name
          description
          featuredMerchant {
            id
            coverImageUrl
            coverImageDynamicUrl
            coverImageBlobKey
            logoImageUrl
            logoImageDynamicUrl
            logoImageBlobKey
            name
            serviceType
            tagline
            description
            addresses {
              id
              locationName
              address
              city
              country
              postCode
              latitude
              longitude
              countryCode
              phone
            }
            email
          }
          featuredProduct {
            id
            type
            featuredImageURL
            featuredImageDynamicURL
            publicProduct
            name
            description
          }
          bridalTheme
          interiorRoomType
          interiorRoomSize
          interiorWorkDuration
          hotelVenueType
          hotelCapacity
          price
        }
        createdOn
        updatedOn
        deleted
        productCode
        bridalCategory
        bridalType
        bridalSize
        bridalQuantity
        bridalSpecFabric
        bridalSpecColour
        bridalSpecDetails
        jewelType
        jewelMetal
        jewelSettingType
        jewelWeight
        jewelDiamondInformation {
          cutType
          total
          weight
        }
        jewelGemsInformation {
          gemsType
          total
          weight
        }
        smartHomeCategory
        smartHomePrice
        hotelPrice
        hotelPackageDetails
        hotelCapacity
        makeupLookType
        makeupLookDesc
        artistName
        artistDesc
        photographyThemeName
        photographyThemeDescription
        photographyPhotographerName
        photographyPhotographerDescription
        accsType
        accsCategory
        accsDetails
        accsRange
        __typename
      }
      error
      totalData
      __typename
    }
  }
`;

export default productsByMerchant;
