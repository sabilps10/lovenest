import gql from 'graphql-tag';

const getCartFlorist = gql`
  query {
    getCartDetails {
      data {
        items {
          id
          customerId
          productId
          qty
          floristExtraSize
          product {
            id
            name
            featuredImageDynamicURL
            featuredImageURL
            isFreeDelivery
            discount
            floristPriceAfterDiscount
            itemPrice
          }
          upsize {
            id
            size
            price
          }
          itemPrice
          orderStatus
          createdOn
        }
        orderId
        deliveryOption {
          deliveryDate
          deliveryTime
          customerId
          deliveryType
          deliveryAddress {
            id
            locationName
            address
            city
            country
            postCode
            latitude
            longitude
            countryCode
            phone
            notes
          }
          deliveryAddressDetail {
            id
            detailAddreess
            streetAndBlock
            floorAndAppartment
            buildingAndTown
          }
          timeslotDetail {
            id
            timeslot
            orderQty
            type
            status
            availabilty
            updatedOn
          }
        }
        subtotal
        tax
        total
        deliveryFee
      }
      totalCount
      error
    }
  }
`;

export default getCartFlorist;
