import gql from 'graphql-tag';

const getAppointmentSlotByDate = gql`
  query getAppointmentSlotByDate(
    $date: String!
    $appointmentId: Int!
    $addressId: Int
  ) {
    getAppointmentSlotByDate(
      date: $date
      appointmentId: $appointmentId
      addressId: $addressId
    ) {
      date
      timeslot {
        label
        availableSlots
        time {
          timeLabel
          value
          isFullBooked
          availableSlot
        }
      }
      error
    }
  }
`;

export default getAppointmentSlotByDate;
