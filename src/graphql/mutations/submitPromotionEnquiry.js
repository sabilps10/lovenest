import gql from 'graphql-tag';

const submitPromotionEnquiry = gql`
  mutation submitSpecialOfferEnquiry($input: submitSpecialOfferEnquiryParams) {
    submitSpecialOfferEnquiry(input: $input) {
      status
      error
    }
  }
`;

export default submitPromotionEnquiry;