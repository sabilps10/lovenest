import gql from 'graphql-tag';

const productPaymentMobile = gql`
  mutation productPaymentMobile(
    $cardId: String!
    $amount: Float!
    $description: String
  ) {
    productPaymentMobile(
      cardId: $cardId
      amount: $amount
      description: $description
    ) {
      data {
        id
        orderId
        paymentNumber
        receiptNumber
        referenceNumber
        referenceUrl
        paidDate
        amount
      }
      error
    }
  }
`;

export default productPaymentMobile;
