import gql from 'graphql-tag';

const customerWeddingInvitation = gql`
  mutation customerWeddingInvitation(
    $templateId: Int!
    $brideName: String!
    $groomName: String!
    $weddingDate: String!
    $eventTime: [InputEventTime]!
    $location: String!
    $invitations: [InputWeddingInvitations]!
    $textCollor: String!
    $frontImage: Upload
    $backImage: Upload
  ) {
    customerWeddingInvitation(
      templateId: $templateId
      brideName: $brideName
      groomName: $groomName
      weddingDate: $weddingDate
      eventTime: $eventTime
      location: $location
      invitations: $invitations
      textCollor: $textCollor
      frontImage: $frontImage
      backImage: $backImage
    ) {
      data {
        id
        uniqueId
        invitationUrl
        name
      }
      error
    }
  }
`;

export default customerWeddingInvitation;
