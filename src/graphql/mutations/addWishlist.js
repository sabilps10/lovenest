import gql from 'graphql-tag';

const addWishlist = gql`
  mutation($productId: Int!) {
    addWishlist(productId: $productId) {
      data {
        id
        isWishlist
      }
      error
    }
  }
`;

export default addWishlist;
