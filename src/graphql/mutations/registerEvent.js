import gql from 'graphql-tag';

const registerEvent = gql`
  mutation ($eventId: Int, $quantity: Int, $timeslotId: Int) {
    registerEvent(
      eventId: $eventId
      quantity: $quantity
      timeslotId: $timeslotId
    ) {
      data {
        id
        number
        name
        total
        createdOn
        merchant {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        customer {
          id
          name
          countryCode
          phone
          mobile
          email
          gender
          partnerName
          partnerCountryCode
          partnerPhone
          partnerMobile
          partnerEmail
          partnerGender
          marriageDate
          profileImage
          isFacebook
          createdOn
          updatedOn
          deleted
        }
        packageLists {
          id
          name
          totalItem
          totalPrice
          itemLists {
            id
            packageName
            packageItemName
            price
            quantity
            subtotal
            description
            appointment
            appointmentDetails {
              id
              status
              date
              time
              timeslot
              rejectReason
              orderId
              orderNumber
              packageId
              packageName
              packageItemId
              packageItemName
              merchantId
              customerId
              activity
              createdOn
              updatedOn
            }
          }
        }
        orderType
        ticketQuantity
        eventDetails {
          id
          eventBannerURL
          name
          description
          venueName
          address1
          address2
          city
          country
          postCode
          latitude
          longitude
          date
          startTime
          endTime
          isFeatured
          createdOn
          updatedOn
        }
      }
      error
    }
  }
`;

export default registerEvent;
