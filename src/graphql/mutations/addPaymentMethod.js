import gql from 'graphql-tag';

const addPaymentMethod = gql`
  mutation addPaymentMethod($token: String!, $isPrimary: Boolean!) {
    addPaymentMethod(token: $token, isPrimary: $isPrimary) {
      data {
        id
        brand
        country
        expMonth
        expYear
        funding
        last4
        isPrimary
        __typename
      }
      error
      __typename
    }
  }
`;

export default addPaymentMethod;
