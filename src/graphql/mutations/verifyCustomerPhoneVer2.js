import gql from 'graphql-tag';

const verifyCustomerPhoneVer2 = gql`
  mutation($countryCode: Int, $phone: String) {
    verifyCustomerPhoneVer2(countryCode: $countryCode, phone: $phone) {
      isCustomerExist
      specialCustomer
      token
      error
    }
  }
`;

export default verifyCustomerPhoneVer2;
