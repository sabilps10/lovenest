import gql from 'graphql-tag';

const uploadMobileAttatchment = gql`
  mutation uploadMobileAttachment(
    $file: Upload
    $image: Upload
    $type: String!
  ) {
    uploadMobileAttachment(file: $file, image: $image, type: $type) {
      data {
        id
        filename
        url
        dynamicUrl
        blobKey
        mimetype
      }
      error
    }
  }
`;

export default uploadMobileAttatchment;
