import gql from 'graphql-tag';

const productDeliveryDetail = gql`
  mutation productDeliveryDetails(
    $addressId: Int
    $address: AddressInput
    $addressDetail: AddressDetailsInput
    $deliveryType: DeliveryType
    $deliveryDate: String
    $deliveryTime: String
    $deliveryTimeslot: Int
  ) {
    productDeliveryDetails(
      addressId: $addressId
      address: $address
      addressDetail: $addressDetail
      deliveryType: $deliveryType
      deliveryDate: $deliveryDate
      deliveryTime: $deliveryTime
      deliveryTimeslot: $deliveryTimeslot
    ) {
      data {
        deliveryDate
        deliveryTime
        customerId
        deliveryType
        deliveryAddress {
          address
          latitude
          longitude
          locationName
          id
          city
          country
          countryCode
        }
        deliveryAddressDetail {
          id
          detailAddreess
          streetAndBlock
          floorAndAppartment
          buildingAndTown
        }
      }
      error
    }
  }
`;

export default productDeliveryDetail;
