import gql from 'graphql-tag';

const uploadImagesMobile = gql`
  mutation uploadImagesMobile($imagefiles: [Upload!], $type: String!) {
    uploadImagesMobile(imagefiles: $imagefiles, type: $type) {
      data {
        id
        filename
        url
        dynamicUrl
        blobKey
        featured
        temporary
        createdOn
      }
      error
    }
  }
`;

export default uploadImagesMobile;
