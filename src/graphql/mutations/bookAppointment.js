import gql from 'graphql-tag';

const bookAppointment = gql`
  mutation bookAppointment(
    $date: String
    $time: String
    $appointmentId: Int
    $addressId: Int
  ) {
    bookAppointment(
      date: $date
      time: $time
      appointmentId: $appointmentId
      addressId: $addressId
    ) {
      data {
        activity
        createdOn
        date
        id
        merchantDetails {
          id
          coverImageUrl
          logoImageUrl
          name
          serviceType
          tagline
          description
          address
          addresses {
            id
            locationName
            address
            city
            postCode
            latitude
            longitude
            countryCode
            phone
            __typename
          }
          phone
          email
          facebookLink
          instagramLink
          pinterestLink
          youtubeLink
          createdOn
          updatedOn
          deleted
        }
        message
        orderId
        orderNo
        packageId
        packageItemId
        packageItemName
        packageName
        rejectReason
        status
        time
        timeslot
        updatedOn
      }
      error
    }
  }
`;
export default bookAppointment;
