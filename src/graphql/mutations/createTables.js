import gql from 'graphql-tag';

const createTable = gql`
  mutation createTables(
    $templateId: String!
    $tableArrangement: [TableArrangementInput]
  ) {
    createTables(templateId: $templateId, tableArrangement: $tableArrangement) {
      data {
        id
        tableType
        tableName
        tableDescription
        group {
          id
          name
        }
        guestList {
          id
          name
          phoneNo
        }
        deleted
      }
      error
    }
  }
`;

export default createTable;
