import gql from 'graphql-tag';

const removeFromCart = gql`
  mutation removeFromCart($cartId: Int!) {
    removeFromCart(cartId: $cartId) {
      data {
        id
        customerId
        productId
        qty
        floristExtraSize
        orderStatus
        createdOn
      }
      error
    }
  }
`;

export default removeFromCart;
