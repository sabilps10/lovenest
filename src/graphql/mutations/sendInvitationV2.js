import gql from 'graphql-tag';

const sendInvitaionV2 = gql`
  mutation sendInvitationV2(
    $templateId: String!
    $contacts: [ContactDraftInput]
    $elements: [ElementDraftInput]
    $schedules: [ScheduleDraftInput]
    $imageUrl: String
    $type: String
    $canvasImgUrl: String
    $customCoverImageUrl: String
    $coverImageUrl: String
  ) {
    sendInvitationV2(
      templateId: $templateId
      contacts: $contacts
      elements: $elements
      schedules: $schedules
      imageUrl: $imageUrl
      type: $type
      canvasImgUrl: $canvasImgUrl
      customCoverImageUrl: $customCoverImageUrl
      coverImageUrl: $coverImageUrl
    ) {
      data {
        templateId
        contacts {
          id
          name
          phoneNo
          valid
          selected
        }
        elements {
          id
          text
          fontSize
          color
          fontFamily
          textAlign
          x
          y
        }
        schedules {
          eventName
          startTime
          endTime
        }
        imageUrl
        type
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
      }
      error
    }
  }
`;

export default sendInvitaionV2;
