import gql from 'graphql-tag';

const uploadProfileImage = gql`
  mutation uploadProfileImage($imagefile: Upload!) {
    uploadProfileImage(imagefile: $imagefile) {
      imageUrl
      error
    }
  }
`;
export default uploadProfileImage;
