import gql from 'graphql-tag';

const createPaymentMobile = gql`
  mutation createPaymentMobile(
    $orderId: Int!
    $cardId: String!
    $amount: Float!
    $description: String
  ) {
    createPaymentMobile(
      orderId: $orderId
      cardId: $cardId
      amount: $amount
      description: $description
    ) {
      data {
        id
        orderId
        paymentNumber
        receiptNumber
        paidDate
        amount
        description
        outlet
        status
        paymentType
        createdOn
        updatedOn
        deleted
      }
      error
    }
  }
`;

export default createPaymentMobile;
