import gql from 'graphql-tag';

const customerReviewMobile = gql`
  mutation customerReviewMobile(
    $merchantId: Int!
    $staff: [StaffData]!
    $serviceRating: Int
    $productRating: Int
    $appRating: Int
    $review1: Int
    $review2: Int
    $reviewGallery: String
    $testimonial: String!
  ) {
    customerReviewMobile(
      merchantId: $merchantId
      staff: $staff
      serviceRating: $serviceRating
      productRating: $productRating
      appRating: $appRating
      review1: $review1
      review2: $review2
      reviewGallery: $reviewGallery
      testimonial: $testimonial
    ) {
      data {
        id
        createdOn
      }
      error
    }
  }
`;

export default customerReviewMobile;
