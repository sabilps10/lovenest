import gql from 'graphql-tag';

const customerDraftTemplate = gql`
  mutation customerDraftTemplate(
    $templateId: Int!
    $templateName: String!
    $brideName: String!
    $groomName: String!
    $weddingDate: String!
    $eventTime: [InputEventTime]!
    $location: String!
    $textCollor: String!
    $frontImage: Upload
    $backImage: Upload
    $contactSelected: [InputWeddingInvitations]
  ) {
    customerDraftTemplate(
      templateId: $templateId
      templateName: $templateName
      brideName: $brideName
      groomName: $groomName
      weddingDate: $weddingDate
      eventTime: $eventTime
      location: $location
      textCollor: $textCollor
      frontImage: $frontImage
      backImage: $backImage
      contactSelected: $contactSelected
    ) {
      data {
        id
        templateName
        brideName
        groomName
        weddingDate
        location
        eventTime {
          endTime
          startTime
          eventName
        }
        template {
          id
          name
          url
          dynamicUrl
        }
        textCollor
        deleted
        createdOn
      }
      error
    }
  }
`;

export default customerDraftTemplate;
