import gql from 'graphql-tag';

const sendRequestAppointment = gql`
  mutation sendRequestAppointment(
    $name: String!
    $email: String!
    $mobile: String!
    $date: String!
    $time: String!
    $message: String!
    $merchantId: Int!
    $renovationBudget: String
    $keyCollectionDate: String
    $typeOfHome: String
  ) {
    sendRequestAppointment(
      name: $name
      email: $email
      mobile: $mobile
      date: $date
      time: $time
      message: $message
      merchantId: $merchantId
      renovationBudget: $renovationBudget
      keyCollectionDate: $keyCollectionDate
      typeOfHome: $typeOfHome
    ) {
      data {
        customerName
        customerEmail
        customerMobile
        appointmentDate
        appointmentTime
        message
        merchantId
        merchantName
        merchantEmail
        renovationBudget
        keyCollectionDate
        typeOfHome
      }
      error
    }
  }
`;

export default sendRequestAppointment;
