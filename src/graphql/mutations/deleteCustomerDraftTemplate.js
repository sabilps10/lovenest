import gql from 'graphql-tag';

const deleteTemplateDraft = gql`
  mutation deleteCustomerDraftTemplate($draftId: Int!) {
    deleteCustomerDraftTemplate(draftId: $draftId) {
      data {
        id
        templateName
      }
      error
    }
  }
`;

export default deleteTemplateDraft;
