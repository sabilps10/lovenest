import gql from 'graphql-tag';

const registerCustomerVer2 = gql`
  mutation registerCustomerVer2(
    $name: String!
    $countryCode: String!
    $phone: String!
    $email: String!
    $gender: UserGender!
    $partnerName: String
    $partnerCountryCode: String
    $partnerPhone: String
    $partnerEmail: String
    $partnerGender: UserGender
    $marriageDate: String!
    $marriageDate2: String
    $profileImage: String
    $keyCollectionDate: String
  ) {
    registerCustomerVer2(
      name: $name
      countryCode: $countryCode
      phone: $phone
      email: $email
      gender: $gender
      partnerName: $partnerName
      partnerCountryCode: $partnerCountryCode
      partnerPhone: $partnerPhone
      partnerEmail: $partnerEmail
      partnerGender: $partnerGender
      marriageDate: $marriageDate
      marriageDate2: $marriageDate2
      profileImage: $profileImage
      keyCollectionDate: $keyCollectionDate
    ) {
      status
      message
      data {
        id
        name
        countryCode
        phone
        mobile
        email
        gender
        partnerName
        partnerCountryCode
        partnerPhone
        partnerMobile
        partnerEmail
        partnerGender
        marriageDate
        marriageDate2
        profileImage
        createdOn
        updatedOn
        deleted
        keyCollectionDate
      }
      error
    }
  }
`;

export default registerCustomerVer2;
