import gql from 'graphql-tag';

const markReadNotification = gql`
  mutation markReadNotification($notificationId: Int) {
    markReadNotification(notificationId: $notificationId) {
      status
      message
      data {
        id
        appointmentId
        appointmentStatus
        merchantId
        merchantImage
        merchantName
        customerId
        customerName
        message
        isRead
        createdOn
      }
    }
  }
`;

export default markReadNotification;
