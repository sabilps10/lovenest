import gql from 'graphql-tag';

const submitRsvpV2 = gql`
  mutation customerWeddingAttendanceV2(
    $invitationId: String!
    $tableType: String
    $totalSeat: Int
    $attendances: [InputWeddingAttendance]!
  ) {
    customerWeddingAttendanceV2(
      invitationId: $invitationId
      tableType: $tableType
      totalSeat: $totalSeat
      attendances: $attendances
    ) {
      data {
        id
        name
        phoneNo
        email
        rsvpStatus
        vaccinatedSwab
        gender
        paxType
        group {
          id
          name
        }
        note
        deleted
        createdOn
      }
      error
    }
  }
`;

export default submitRsvpV2;
