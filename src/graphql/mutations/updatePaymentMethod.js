import gql from 'graphql-tag';

const updatePaymentMethod = gql`
  mutation updatePaymentMethod($cardId: String!) {
    updatePaymentMethod(cardId: $cardId) {
      data {
        id
        brand
        country
        expMonth
        expYear
        funding
        last4
        isPrimary
        __typename
      }
      error
    }
  }
`;

export default updatePaymentMethod;
