import gql from 'graphql-tag';

const deleteInvitationByTemplateId = gql`
  mutation deleteInvitationByTemplate($templateId: String!) {
    deleteInvitationByTemplate(templateId: $templateId) {
      data {
        id
      }
      error
    }
  }
`;

export default deleteInvitationByTemplateId;
