import gql from 'graphql-tag';

const verifyCustomerOTPVer2 = gql`
  mutation($countryCode: Int, $phone: String, $smsOTP: String) {
    verifyCustomerOTPVer2(
      countryCode: $countryCode
      phone: $phone
      smsOTP: $smsOTP
    ) {
      status
      token
      error
    }
  }
`;

export default verifyCustomerOTPVer2;
