import gql from 'graphql-tag';

const customerSubmissionDelete = gql`
  mutation customerSubmissionDelete($id: Int!) {
    customerSubmissionDelete(id: $id) {
      data {
        id
      }
      error
    }
  }
`;

export default customerSubmissionDelete;
