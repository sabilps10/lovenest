import gql from 'graphql-tag';

const reviewStageProgress = gql`
  mutation reviewStageProgress(
    $progressId: Int!
    $status: ReviewStageProgressStatus!
  ) {
    reviewStageProgress(progressId: $progressId, status: $status) {
      data {
        id
        note
        status
        userId
        userName
        userRole
        gallery {
          id
          filename
          url
          dynamicUrl
          blobKey
          featured
          temporary
          createdOn
          order
        }
        createdOn
      }
      error
    }
  }
`;

export default reviewStageProgress;
