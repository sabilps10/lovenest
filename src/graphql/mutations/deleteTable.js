import gql from 'graphql-tag';

const deleteTable = gql`
  mutation deleteTable($id: Int!) {
    deleteTable(id: $id) {
      data {
        id
        tableType
        tableName
        tableDescription
        group {
          id
          name
        }
        guestList {
          id
          name
          phoneNo
        }
      }
      error
    }
  }
`;

export default deleteTable;
