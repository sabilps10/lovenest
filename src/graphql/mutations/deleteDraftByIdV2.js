import gql from 'graphql-tag';

const deleteDraftByIdV2 = gql`
  mutation customerDeleteDraftV2($templateId: String!) {
    customerDeleteDraftV2(templateId: $templateId) {
      data {
        templateId
      }
      error
    }
  }
`;

export default deleteDraftByIdV2;
