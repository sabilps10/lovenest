import gql from 'graphql-tag';

const uploadImageMobile = gql`
  mutation uploadImageMobile($imagefile: Upload!, $type: String!) {
    uploadImageMobile(imagefile: $imagefile, type: $type) {
      data {
        id
        filename
        url
        dynamicUrl
        blobKey
        featured
        temporary
        createdOn
        order
      }
      error
    }
  }
`;

export default uploadImageMobile;
