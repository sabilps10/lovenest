import gql from 'graphql-tag';

const deleteInvitationHistoryByTempId = gql`
  mutation deleteCustomerHistoryInvitation($templateId: Int!) {
    deleteCustomerHistoryInvitation(templateId: $templateId) {
      status
      message
      error
    }
  }
`;

export default deleteInvitationHistoryByTempId;
