import gql from 'graphql-tag';

const createDraftV2 = gql`
  mutation customerDraftTemplateV2(
    $templateId: String!
    $contacts: [ContactDraftInput]
    $elements: [ElementDraftInput]
    $schedules: [ScheduleDraftInput]
    $imageUrl: String
    $type: String
    $canvasImgUrl: String
    $customCoverImageUrl: String
    $coverImageUrl: String
  ) {
    customerDraftTemplateV2(
      templateId: $templateId
      contacts: $contacts
      elements: $elements
      schedules: $schedules
      imageUrl: $imageUrl
      type: $type
      canvasImgUrl: $canvasImgUrl
      customCoverImageUrl: $customCoverImageUrl
      coverImageUrl: $coverImageUrl
    ) {
      data {
        templateId
        contacts {
          id
          name
          phoneNo
          valid
          selected
        }
        elements {
          id
          text
          fontSize
          color
          fontFamily
          textAlign
          x
          y
        }
        schedules {
          eventName
          startTime
          endTime
        }
        imageUrl
        type
        canvasImgUrl
        customCoverImageUrl
        coverImageUrl
      }
      error
    }
  }
`;

export default createDraftV2;
