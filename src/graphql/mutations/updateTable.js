import gql from 'graphql-tag';

const updateTable = gql`
  mutation updateTable($id: Int!, $tableArrangement: TableArrangementInput) {
    updateTable(id: $id, tableArrangement: $tableArrangement) {
      data {
        id
        tableType
        tableName
        tableDescription
        group {
          id
          name
        }
        guestList {
          id
          name
          phoneNo
        }
      }
      error
    }
  }
`;

export default updateTable;
