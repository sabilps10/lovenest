import gql from 'graphql-tag';

const productOrderStatusConfirmation = gql`
  mutation productOrderStatus($orderId: Int!, $orderStatus: String!) {
    productOrderStatus(orderId: $orderId, orderStatus: $orderStatus) {
      data {
        id
        orderStatus
        notes
        createdOn
      }
      error
    }
  }
`;

export default productOrderStatusConfirmation;
