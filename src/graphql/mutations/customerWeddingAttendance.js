import gql from 'graphql-tag';

const customerWeddingAttendance = gql`
  mutation customerWeddingAttendance(
    $invitationId: String!
    $attendances: [InputWeddingAttendance]!
  ) {
    customerWeddingAttendance(
      invitationId: $invitationId
      attendances: $attendances
    ) {
      data {
        id
        name
      }
      error
    }
  }
`;

export default customerWeddingAttendance;
