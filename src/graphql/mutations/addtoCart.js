import gql from 'graphql-tag';

const addToCart = gql`
  mutation addToCart($productId: Int!, $qty: Int, $options: CartOptions) {
    addToCart(productId: $productId, qty: $qty, options: $options) {
      data {
        id
        customerId
        qty
        productId
        floristExtraSize
        orderStatus
        createdOn
      }
      error
    }
  }
`;

export default addToCart;
