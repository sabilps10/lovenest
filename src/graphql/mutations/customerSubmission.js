import gql from 'graphql-tag';

const customerSubmission = gql`
  mutation customerSubmission(
    $loveWinId: Int!
    $serviceType: String!
    $merchantId: Int!
    $datePurchase: String!
    $totalAmount: Float!
    $depositAmount: Float!
    $staff: InputStaff
    $attachmentIds: [Int]
  ) {
    customerSubmission(
      loveWinId: $loveWinId
      serviceType: $serviceType
      merchantId: $merchantId
      datePurchase: $datePurchase
      totalAmount: $totalAmount
      depositAmount: $depositAmount
      staff: $staff
      attachmentIds: $attachmentIds
    ) {
      data {
        id
        chanceNumber
        loveWinId
        serviceType
        merchantId
        merchant {
          id
          name
          serviceType
        }
        datePurchase
        totalAmount
        depositAmount
        staff {
          id
          name
          role
        }
        attachments {
          id
          filename
          url
          dynamicUrl
          mimetype
        }
      }
      error
    }
  }
`;

export default customerSubmission;
