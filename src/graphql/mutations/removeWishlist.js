import gql from 'graphql-tag';

const removeWishlist = gql`
  mutation($productId: Int!) {
    removeWishlist(productId: $productId) {
      data {
        id
        isWishlist
      }
      error
    }
  }
`;

export default removeWishlist;
