import gql from 'graphql-tag';

const deletePaymentMethod = gql`
  mutation deletePaymentMethod($cardId: String!) {
    deletePaymentMethod(cardId: $cardId) {
      data {
        id
        brand
        country
        expMonth
        expYear
        funding
        last4
        isPrimary
      }
      error
    }
  }
`;

export default deletePaymentMethod;
